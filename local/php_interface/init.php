<?
//Функция вывода массивов
function pr($item, $show_for = false) {
	global $USER;
	if ($USER->IsAdmin() || $show_for == 'all') {
		if ( ! $item) {
			echo ' <br />пусто <br />';
		} elseif (is_array($item) && empty($item)) {
			echo '<br />массив пуст <br />';
		} else {
			echo ' <pre>' . print_r($item, true) . ' </pre>';
		}
	}
}

/**
 * Определяем мобильное устройство
 * @return bool
 */
function isMobileDevice() {
	return (
		preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $_SERVER['HTTP_USER_AGENT']) ||
		preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($_SERVER['HTTP_USER_AGENT'], 0, 4))
	);
}


/**
 * Склонение слов по падежам. С использованием api Яндекса
 * @var string $text - текст
 * @var integer $numForm - нужный падеж. Число от 0 до 5
 *
 * @return - вернет false при неудаче. При успехе вернет нужную форму слова
 */
function getNewFormText($text, $numForm) {
	$urlXml = "http://export.yandex.ru/inflect.xml?name=" . urlencode($text);
	$result = @simplexml_load_file($urlXml);
	if ($result) {
		$arrData = array();
		foreach ($result->inflection as $one) {
			$arrData[] = (string) $one;
		}

		return $arrData[ $numForm ];
	}

	return false;
}

/**
 * Склонение числительных
 *
 * @param $number - число
 * @param $after - варианты написания array('тур', "тура", "туров")
 */
function plural_form($number, $after) {
	$cases = array(2, 0, 1, 1, 1, 2);
	echo $number . ' ' . $after[ ($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[ min($number % 10, 5) ] ];
}


/**
 * Функция возвращающая наличие доступа к секции инфоблока
 * $ObjectName - название объекта пользотельского поля
 * $IDSection  - Код секции инфоблока
 */
function UserAccess($ObjectName, $IDSection) {
	//По умолчанию доступа нету
	$UseAccess = false;
	global $USER;

	// Получаем код пользователя
	$CurUserID = $USER->GetID();

	$arUserFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields($ObjectName, $IDSection, LANGUAGE_ID);
	foreach ($arUserFields as $FIELD_NAME => $arUserField) {
		foreach ($arUserField["VALUE"] as $UserID => $arProperty) {
			if ($CurUserID == $arProperty) {
				$UseAccess = true;
			}
		}
	}

	return $UseAccess;
}


/**
 * Функция преобразования валют
 *
 */
function SpecTableFormatCurrency($val, $cur, $htplace = "") {
	switch (substr(trim(strip_tags($htplace)), 0, 3)) {
		case "DBL":
			$val /= 2;
			break;
		case "TRP":
			$val /= 3;
			break;
		case "QDP":
			$val /= 4;
			break;
	}

	return CurrencyFormat(floor(CCurrencyRates::ConvertCurrency($val, $cur, "RUB")), "RUB");
}

//Служебная функция конверитрования объекта в массив
function objectToArr($obj) {
	$tmp = array();
	foreach ($obj as $key => $value) {
		if ( ! is_array($value) && ! is_object($value)) {
			if (is_bool($value)) {
				$tmp[ (string) $key ] = $value ? 1 : 0;
			}                // можно заменить на true и false
			else {
				$tmp[ (string) $key ] = (string) $value;
			}
		} else {
			$tmp[ (string) $key ] = objectToArr($value);
		}
	}

	return $tmp;
}

//Очищаем строку с номером телефона
function clearPhoneString($phoneString = '') {
	if (strlen($phoneString) > 0) {
		$phoneString = str_replace(' ', '', $phoneString);
		$phoneString = str_replace('(', '', $phoneString);
		$phoneString = str_replace(')', '', $phoneString);
		$phoneString = str_replace('-', '', $phoneString);
	}

	return $phoneString;
}

//Закаччивание сторонних скриптов
function downloadJs() {

	$arScripts = array(
		array('PATH' => 'https://mc.yandex.ru/metrika/watch.js', 'NAME' => 'watch.js'),
		array('PATH' => 'https://www.google-analytics.com/analytics.js', 'NAME' => 'analytics.js'),
		array('PATH' => 'http://cdn.callbackhunter.com/cbh.js', 'NAME' => 'cbh.js'),
	);

	foreach ($arScripts as $script) {
		$content = file_get_contents($script['PATH']);
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/js/" . $script['NAME'], $content);
	}

	return "downloadJs();";
}


include_once 'simai_image.php';
include_once 'constants.php';
include_once 'events.php';

?>
