<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Sale;

global $USER;

\Bitrix\Main\Loader::includeModule('newtravel.search');
$requset = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

?>

<?
$db_sales = CSaleOrderUserProps::GetList(
	array("DATE_UPDATE" => "DESC"),
	array("USER_ID" => $USER->GetID())
);

if ($ar_sales = $db_sales->GetNext()) {
	$arUserProps = array();
// Выведем все свойства профиля покупателя с кодом $ID
	$db_propVals = CSaleOrderUserPropsValue::GetList(array("ID" => "ASC"), Array("USER_PROPS_ID" => $ar_sales['ID']));
	while ($arPropVals = $db_propVals->GetNext()) {
		$arUserProps[ $arPropVals['PROP_CODE'] ] = $arPropVals['VALUE'];
	}
}

if (strlen($requset->get('ORDER_ID') > 0)) {
	$arUserProps['ORDER_ID'] = $requset->get('ORDER_ID');
}


?>
<p>Вы можете оформить заявку на кредит самостоятельно, за 1 минуту. </p>
<p>Для этого воспользуйтесь <strong><a hreg='#' onclick="$('#credit-instruction').slideToggle()">инструкцией<i class="fa fa-chevron-down"></i></a></strong> <br></p>
<div id="credit-instruction" style="display: none;">
	<h3>Инструкция по заполенению формы оформления кредита</h3>

	<ul>
		<li>
			1. Заполните только те поля которые указаны на изображении <br>
			<img src="/upload/faq/Credit-instruction.jpg" style="width: 100%" alt="">
		</li>
	</ul>
	<br>
	<br>
</div>
<p>
	<a class='btn btn-blue' href='https://www.onlinecredit.ru/sites/phone/sendmail.php?HS=0KPQvNC90YvQtSDRgtGD0YDQuNGB0YLRiy7QoNGEXjMzNzc2Ml5iYzcyYjgxOWI3MDhkZWE1ZmRlMmI3OTg3MTQyYmVlYw==&HH=02697f41fcd05cd39e8123835340dc6b' target='_blank'>
		Оформить заявку самостоятельно за 1 минуту
	</a>
</p>
<p>Либо при помощи нашего специалиста по телефону <a class='btn btn-blue' href='javascript:void(0)' onclick="sendCreditRequest(this, <?=CUtil::PhpToJSObject($arUserProps)?>)">Заказать звонок</a></p>
