<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Sale;

global $USER;

\Bitrix\Main\Loader::includeModule('newtravel.search');
$requset = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
?>

<?
$db_sales = CSaleOrderUserProps::GetList(
	array("DATE_UPDATE" => "DESC"),
	array("USER_ID" => $USER->GetID())
);

if ($ar_sales = $db_sales->GetNext()) {
	$arUserProps = array();
// Выведем все свойства профиля покупателя с кодом $ID
	$db_propVals = CSaleOrderUserPropsValue::GetList(array("ID" => "ASC"), Array("USER_PROPS_ID" => $ar_sales['ID']));
	while ($arPropVals = $db_propVals->GetNext()) {
		$arUserProps[ $arPropVals['PROP_CODE'] ] = $arPropVals['VALUE'];
	}
}

if (strlen($requset->get('ORDER_ID') > 0)) {
	$arUserProps['ORDER_ID'] = $requset->get('ORDER_ID');
	$arPaySys                = CSalePaySystem::GetByID($arUserProps['ORDER_ID']);
}

?>

<div class="row">
	<div class="col-12">
		<p>В приложений Альфа-клик нажимаем <strong>Оплатить и перевести</strong> - <strong>В Другой банк</strong> – <strong>По номеру счета</strong></p>
		<a class="btn btn-outline-primary text-primary" hreg='#' onclick="$('#alfaclick-instruction').slideToggle()">ИНСТРУКЦИЯ <i class="fa fa-chevron-down"></i></a>
	</div>
</div>
<div class="row" id="alfaclick-instruction" style="display: none;">
	<div class="col-4"><img class="img-thumbnail" src="/local/php_interface/include/sale_payment/newtravel.alfaclick/img/1.jpg"></div>
	<div class="col-4"><img class="img-thumbnail" src="/local/php_interface/include/sale_payment/newtravel.alfaclick/img/2.jpg"></div>
	<div class="col-4"><img class="img-thumbnail" src="/local/php_interface/include/sale_payment/newtravel.alfaclick/img/3.jpg"></div>

	<div class="col-12 mt-3">
		<ul class="list-unstyled">
			<li>Наименование получателя: <strong>Умные туристы</strong></li>
			<li>БИК банка получателя: <strong>042202824</strong></li>
			<li>После заполнения БИК поля <strong>Банк получателя и Кор. счет банка получателя</strong> подгружаются автоматический.</li>
			<li>ИНН получателя: <strong>0275085104</strong></li>
			<li>КПП получателя: <strong>027501001</strong></li>
			<li>Номер счета получателя: <strong>40702810429300001663</strong></li>
			<li>Код(): <strong>не заполняется</strong></li>
			<li>Назначение перевода: <strong>За туристическую путевку</strong></li>
			<li>Сумма перевода: <strong>указываете необходимую сумму</strong></li>
		</ul>
	</div>

	<div class="col-4"><img class="img-thumbnail" src="/local/php_interface/include/sale_payment/newtravel.alfaclick/img/4.jpg"></div>
	<div class="col-4"><img class="img-thumbnail" src="/local/php_interface/include/sale_payment/newtravel.alfaclick/img/5.jpg"></div>
	<div class="col-4"><img class="img-thumbnail" src="/local/php_interface/include/sale_payment/newtravel.alfaclick/img/6.jpg"></div>
</div>

