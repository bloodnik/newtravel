<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Sale;

global $USER;

\Bitrix\Main\Loader::includeModule('newtravel.search');
$requset = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
?>

<?
$db_sales = CSaleOrderUserProps::GetList(
	array("DATE_UPDATE" => "DESC"),
	array("USER_ID" => $USER->GetID())
);

if ($ar_sales = $db_sales->GetNext()) {
	$arUserProps = array();
// Выведем все свойства профиля покупателя с кодом $ID
	$db_propVals = CSaleOrderUserPropsValue::GetList(array("ID" => "ASC"), Array("USER_PROPS_ID" => $ar_sales['ID']));
	while ($arPropVals = $db_propVals->GetNext()) {
		$arUserProps[ $arPropVals['PROP_CODE'] ] = $arPropVals['VALUE'];
	}
}

if (strlen($requset->get('ORDER_ID') > 0)) {
	$arUserProps['ORDER_ID'] = $requset->get('ORDER_ID');
	$arPaySys = CSalePaySystem::GetByID($arUserProps['ORDER_ID']);
}

?>

<div class="row">
	<div class="col-md-12">
		<?=$arPaySys['DESCRIPTION']?>
		<img src="/upload/faq/2gismap.png" style="width: 100%;" alt="" class="img-responsive">
	</div>

</div>