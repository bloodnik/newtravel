<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
use Bitrix\Sale;

global $USER;
\Bitrix\Main\Loader::includeModule('newtravel.search');
$requset = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

$db_sales = CSaleOrderUserProps::GetList(
	array("DATE_UPDATE" => "DESC"),
	array("USER_ID" => $USER->GetID())
);

if ($ar_sales = $db_sales->GetNext()) {
	$arUserProps = array();
// Выведем все свойства профиля покупателя с кодом $ID
	$db_propVals = CSaleOrderUserPropsValue::GetList(array("ID" => "ASC"), Array("USER_PROPS_ID" => $ar_sales['ID']));
	while ($arPropVals = $db_propVals->GetNext()) {
		$arUserProps[ $arPropVals['PROP_CODE'] ] = $arPropVals['VALUE'];
	}
}

$orderPrice = 0;
if (strlen($requset->get('ORDER_ID') > 0)) {
	$arUserProps['ORDER_ID'] = $requset->get('ORDER_ID');

	$order = Sale\Order::load($arUserProps['ORDER_ID']);
	$orderPrice = $order->getPrice();
}else {
	$arUserProps['ORDER_ID'] = $GLOBALS['SALE_INPUT_PARAMS']["ORDER"]["ID"];
	$order = Sale\Order::load($arUserProps['ORDER_ID']);
	$orderPrice = $order->getPrice();
}
$arUserProps['PRICE'] = $orderPrice;

?>


<div class="row">
	<div class="col-md-12">
		<a class="btn btn-primary" id="vsegda-da-buy" onclick="showHomeCreditPopup(<?=CUtil::PhpToJSObject($arUserProps)?>)" href="javascript:void(0)">Заявка на кредит</a>
	</div>
</div>

<script>

//    KreditOtdel.Widget.attach({
//        widgetType: KreditOtdel.Widget.widgetTypes.VsegdaDa,
//        elementId: 'vsegda-da-buy',
//        autoStart: true,
//        data: {
//            hostname: 'widget-test.l-kredit.ru', //hostname replaced widget-test.l-kredit.ru
//            //shopId: 123,
//            goods: [{name: "Туристическая путевка", price: <?//=$orderPrice?>//, count: 1}],
//            lastName: "<?//=$arUserProps['LAST_NAME']?>//",
//            firstName: "<?//=$arUserProps['NAME']?>//",
//            middleName: '',
//            phone: "<?//=$arUserProps['PHONE']?>//",
//            email: "<?//=$arUserProps['EMAIL']?>//",
//            discount: 0,
//            orderId: "<?//=$arUserProps['ORDER_ID']?>//"
//        }
//    });
</script>


