<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Sale;

global $USER;

\Bitrix\Main\Loader::includeModule('newtravel.search');
$requset = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

?>

<?
$db_sales = CSaleOrderUserProps::GetList(
	array("DATE_UPDATE" => "DESC"),
	array("USER_ID" => $USER->GetID())
);

if ($ar_sales = $db_sales->GetNext()) {
	$arUserProps = array();
// Выведем все свойства профиля покупателя с кодом $ID
	$db_propVals = CSaleOrderUserPropsValue::GetList(array("ID" => "ASC"), Array("USER_PROPS_ID" => $ar_sales['ID']));
	while ($arPropVals = $db_propVals->GetNext()) {
		$arUserProps[ $arPropVals['PROP_CODE'] ] = $arPropVals['VALUE'];
	}
}

if (strlen($requset->get('ORDER_ID') > 0)) {
	$arUserProps['ORDER_ID'] = $requset->get('ORDER_ID');
} else {
	$arUserProps['ORDER_ID'] = $GLOBALS['SALE_INPUT_PARAMS']["ORDER"]["ID"];
}

$order = Sale\Order::load($arUserProps['ORDER_ID']);

?>
<p>Вы можете оставить заявку на кредит самостоятельно, за 1 минуту. </p>
<p>Для этого нажмите на кнопку "Заявка на кредит" и заполните форму </p>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ceb_credit_modal_<?=$arUserProps['ORDER_ID']?>">
	Заявка на кредит
</button>

<!-- Modal -->
<div class="modal fade" id="ceb_credit_modal_<?=$arUserProps['ORDER_ID']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Заявка на кредит от Credit EuropeBank</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="" method="post" onsubmit="sendCreditRequestCEB(event)">
					<div class="alert alert-danger d-none">

					</div>
					<table class="table table-sm">
						<thead>
						<tr>
							<th class="text-center" colspan="2"><img class="img-thumbnail w-25" src="/local/php_interface/include/sale_payment/newtravel.crediteuropa/img/crediteurop.jpg"></th>
						</tr>
						<tr>
							<th class="table-primary text-center" colspan="2">Заявка на кредит</th>
						</tr>
						<tr>
							<th class="table-warning text-center" colspan="2">Необходимо заполнить все поля</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>ФИО(полностью) *</td>
							<td><input class="form-control form-control-sm" type="text" name="FIO" value="<?=$arUserProps['CONTACT_PERSON']?>" required></td>
						</tr>
						<tr>
							<td>Мобильный телефон</td>
							<td><input class="form-control form-control-sm" type="text" name="PHONE" value="<?=$arUserProps['PHONE']?>" required></td>
						</tr>
						<tr>
							<td>Домашний стационарный телефон</td>
							<td><input class="form-control form-control-sm" type="text" name="PHONE_2" value=""></td>
						</tr>
						<tr>
							<td>Рабочий стационарный телефон</td>
							<td><input class="form-control form-control-sm" type="text" name="PHONE_3" required></td>
						</tr>
						<tr>
							<td>Телефон контактного лица</td>
							<td><input class="form-control form-control-sm" type="text" name="PHONE_4" required></td>
						</tr>
						<tr>
							<td class="table-warning text-center" colspan="2">Данные по кредиту(рассрочке)</td>
						</tr>
						<tr>
							<td>Стоимость</td>
							<td><input class="form-control form-control-sm" type="text" name="PRICE" value="<?=round($order->getPrice())?>" required></td>
						</tr>
						<tr>
							<td>Первоначальный взнос</td>
							<td><input class="form-control" type="number" name="INITIAL_FEE" required></td>
						</tr>
						<tr>
							<td>Срок кредитования</td>
							<td>
								<select class="form-control form-control-sm" name="CREDIT_PERIOD" required>
									<?for ($i = 5; $i <= 24; $i++):?>
										<option value="<?=$i?>"><?=$i?> мес.</option>
									<?endfor;?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="table-warning text-center" colspan="2">Файлы</td>
						</tr>
						<tr>
							<td>
								<label>Первая страница паспорта</label>
								<input class="form-control form-control-sm" name="FILE_1" type="file" required>
							</td>
							<td>
								<label>Страница паспорта с пропиской</label>
								<input class="form-control form-control-sm" name="FILE_2" type="file" required>
							</td>
						</tr>
						<tr>
							<td class="text-right" colspan="2">
								<button class="btn btn-primary" name="submit">Отправить</button>
							</td>
						</tr>
						</tbody>
					</table>

					<input type="hidden" name="ORDER_ID" value="<?=$arUserProps['ORDER_ID']?>">
				</form>
			</div>
		</div>
	</div>
</div>

