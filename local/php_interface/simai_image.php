<?
/**
 * SimaiResizeImage: функция cоздает изображение, приведенное к прямоугольнику заданного размера c произвольным выравниванием
 * Нe вызывается напрямую.
 * @param str $img - путь к файлу относительно корня сайта или id рисунка в системе Битрикс
 * @param int $h - высота (если false или 0 или не задано, то высчитывается пропорционально оригинальному изображению)
 * @param int $w - ширина (если false или 0 или не задано, то высчитывается пропорционально оригинальному изображению)
 * @param str $align - выравнивание center, [right, left]
 * @param str $flip - залипание auto, [vertical, horizontal]
 */
function SimaiResizeImage($img, &$w=0, &$h=0, $align='center', $flip='auto')
{
	if(!is_numeric($w) || !is_numeric($h) || $w < 0 || $h < 0 || ($w == 0 && $h == 0))
		return false;
	
	if(is_numeric($img))
	{
		if($img <= 0)
			return false;
		$file = CFile::GetByID(IntVal($img));
		if(!$file = $file->Fetch())
			return false;
		$url = '/upload/'.$file['SUBDIR'].'/'.$file['FILE_NAME'];
	}
	elseif(!is_bool($img) && !is_object($img) && !is_array($img))
		$url = $img;
	else
		return false;
	$path = pathinfo($url);
	if(strlen($path['basename']) == 0)
		return false;
	$name = $_SERVER['DOCUMENT_ROOT'].$url;
	list($sw, $sh, $t) = getimagesize($name);
	$sd = $sw/$sh;
	$w = ($w > 0?$w:$h*$sd);
	$h = ($h > 0?$h:$w/$sd);
	list($dx, $dy, $sx, $sy, $dw, $dh) = array(0, 0, 0, 0, $w, $h);
	$newfilename = '.thumb_'.md5($img.'_'.$w.'_'.$h.'_'.$align.'_'.$flip).($t==1?'.gif':($t==2?'.jpg':'.png'));
	$newname = $_SERVER['DOCUMENT_ROOT'].$path["dirname"].'/'.$newfilename;
	$newurl = $path["dirname"].'/'.$newfilename;
	if(file_exists($newname) && !isset($_GET["clear_cache"]))
		return $newurl;
	$dd = $w/$h;
	$source = ($t==1?imagecreatefromgif($name):($t==2?imagecreatefromjpeg($name):imagecreatefrompng($name)));
	$thumb = imagecreatetruecolor($w, $h);
	if($flip == 'auto')
		$flip = ($sd >= 1?'vertical':'horizontal');
	elseif($flip == 'filled')
		$flip = ($sd < 1?'vertical':'horizontal');;
	if($flip=='vertical')
	{
		if($dd > $sd)
			$dx = ($align=='left'?0:($align=='right'?$dw - $dh*$sd:($dw - $dh*$sd)/2));
		else
			$sx = ($align=='left'?0:($align=='right'?$sw - $sh*$dd:($sw - $sh*$dd)/2));
		if($dw < $sw)
			$sw = $sh*$dd;
		else
			$dw = $dh*$sd;
	}
	else
	{
		if($dd < $sd)
			$dy = ($align=='left'?0:($align=='right'?$dh - $dw/$sd:($dh - $dw/$sd)/2));
		else
			$sy = ($align=='left'?0:($align=='right'?$sh - $sw/$dd:($sh - $sw/$dd)/2));
		if($dh < $sh)
			$sh = $sw/$dd;
		else
			$dh = $dw/$sd;
	}
	switch($t) {
		case 1:
			$color = imagecolorallocate($thumb, 0, 0, 0);
			imagecolortransparent($thumb, $color);
			break;
		case 2:
			$color = imagecolorallocate($thumb, 0, 0, 0);
			imagefill($thumb, 0, 0, $color);
			break;
		case 3:
			imagealphablending($thumb, false);
			$color = imagecolortransparent($thumb, imagecolorallocatealpha($thumb, 0, 0, 0, 127));
			imagefill($thumb, 0, 0, $color);
			imagesavealpha($thumb, true);
			break;
	}
	if(!imagecopyresampled($thumb, $source, $dx, $dy, $sx, $sy, $dw, $dh, $sw, $sh)) return false;
	switch($t) {
		case 1: imagegif($thumb, $newname); break;
		case 2: imagejpeg($thumb, $newname); break;
		case 3: imagepng($thumb, $newname);
	}
	if(is_resource($thumb)) imagedestroy($thumb);
	return $newurl;
}
/**
 * SimaiShowImage: шаблонная фукнция вывода изображения.
 * @param int $img - ссылка на id рисунка в системе Битрикс
 * @param int $h - высота (если false или 0 или не задано, то высчитывается пропорционально оригинальному изображению)
 * @param int $w - ширина (если false или 0 или не задано, то высчитывается пропорционально оригинальному изображению)
 * @param str $params - добавочная строка к тегу ссылке a напр: 'rel="shadowbox[sub_banners]"' 'class="nyroModal"'
 * @param str $params1 - добавочная строка к тегу изозбражения img напр: 'alt="Подпись к рисунку"'
 * @param str $align - выравнивание center, [right, left]
 * @param str $flip - залипание auto, [vertical, horizontal]
 */
function SimaiShowImage($img, $w, $h, $params='', $align='center', $flip='vertical') {
	$src = SimaiResizeImage($img, $w, $h, $align, $flip);
	return '<img src="'.$src.'" width="'.$w.'" height="'.$h.'"'.($params?' '.trim($params):'').' />';
}
/**
 * SimaiShow2Images: шаблонная фукнция вывода миниатюры изображения и ссылки на оригинальное изображение.
 * Можно использовать для реализации всплывающих (модальных) окон. nyroModal, showbox и тд
 * @param int $img - ссылка на id рисунка в системе Битрикс
 * @param int $h - высота (если false или 0 или не задано, то высчитывается пропорционально оригинальному изображению)
 * @param int $w - ширина (если false или 0 или не задано, то высчитывается пропорционально оригинальному изображению)
 * @param str $params - добавочная строка к тегу ссылке a напр: 'rel="shadowbox[sub_banners]"' 'class="nyroModal"'
 * @param str $params1 - добавочная строка к тегу изозбражения img напр: 'alt="Подпись к рисунку"'
 * @param str $align - выравнивание center, [right, left]
 * @param str $flip - залипание auto, [vertical, horizontal]
 */
function SimaiShow2Images($img, $w, $h, $params='', $params1='', $align='center', $flip='vertical') {
	$src = SimaiResizeImage($img, $w, $h, $align, $flip);
	return '<a href="'.CFile::GetPath($img).'"'.($params?' '.trim($params):'').'><img src="'.$src.'" width="'.$w.'" height="'.$h.'"'.($params1?' '.trim($params1):'').' /></a>';
}
/**
 * SimaiShow2ImagesEx: шаблонная фукнция вывода миниатюры изображения и ссылки на предварительно модифицированное изображение.
 * Можно использовать для реализации всплывающих (модальных) окон. nyroModal, showbox и тд
 * @param int $img - ссылка на id рисунка в системе Битрикс
 * @param int $h - высота (если false или 0 или не задано, то высчитывается пропорционально оригинальному изображению)
 * @param int $w - ширина (если false или 0 или не задано, то высчитывается пропорционально оригинальному изображению)
 * @param str $params - добавочная строка к тегу ссылке a напр: 'rel="shadowbox[sub_banners]"' 'class="nyroModal"'
 * @param str $params1 - добавочная строка к тегу изозбражения img напр: 'alt="Подпись к рисунку"'
 * @param str $align - выравнивание center, [right, left]
 * @param str $flip - залипание auto, [vertical, horizontal]
 */
function SimaiShow2ImagesEx($img, $w, $h, $w1, $h1, $params='', $params1='', $align='center', $flip='vertical', $align1='center', $flip1='vertical') {
	$src = SimaiResizeImage($img, $w, $h, $align, $flip);
	$src1 = SimaiResizeImage($img, $w1, $h1, $align1, $flip1);
	return '<a href="'.$src1.'"'.($params?' '.trim($params):'').'><img src="'.$src.'" width="'.$w.'" height="'.$h.'"'.($params1?' '.trim($params1):'').' /></a>';
}

function SimaiGetImageSrc($img, $w, $h, $align='center', $flip='vertical') {
	return SimaiResizeImage($img, $w, $h, $align, $flip);
}

/*
 * Старые функции (для совместимости)
 */

/**
 * Генерация миниатюры с ограничением по максимальной стороне
 * $img - изображение
 * $s - требуемая наиб. сторона миниатюры
 */
function SimaiImageFixedSize($img,$s)
{
	$img = IntVal($img);
	$s = IntVal($s);
	$file = CFile::MakeFileArray($img);
	$oldname = $file["tmp_name"];
	$size = $file["size"];
	$sw = $size[0];
	$sh = $size[1];
	if ($sw >= $sh)
	return SimaiImageFixedWidth($img,$s);
	else
	return SimaiImageFixedHeight($img,$s);
}

/**
 * функция, формирующая изображение с приведением к заданной ширине
 */
function SimaiImageFixedWidth($img,$s)
{
	$img = IntVal($img);
	$s = IntVal($s);
	$file = CFile::GetByID($img);
	if ($file = $file->Fetch())
	{
		$path = $_SERVER["DOCUMENT_ROOT"].'/upload/'.$file[SUBDIR].'/';
		$oldname = $path.$file[FILE_NAME];
		$oldurl = '/upload/'.$file[SUBDIR].'/'.$file[FILE_NAME];
		$size = getimagesize($oldname);
		$sh = $size[1];
		$sw = $size[0];
		$type = $size[2];
		$ext = "";
		if ($type == 1)
		$ext = ".gif";
		elseif ($type == 3)
		$ext = ".png";
		elseif ($type == 2)
		$ext = ".jpg";
		$newname = $path.$img."_w_".$s.$ext;
		$newurl = '/upload/'.$file[SUBDIR].'/'.$img."_w_".$s.$ext;
		if (($sw != $s) and ($ext != ""))
		{
			if (!file_exists($newname))
			{
				if ($ext == '.gif')
				$source = imagecreatefromgif($oldname);
				elseif ($ext == '.png')
				$source = imagecreatefrompng($oldname);
				else
				$source = imagecreatefromjpeg($oldname);
				$d = $s / $sw;
				$thumb = @imagecreatetruecolor($sw*$d, $sh*$d);
				imagecopyresampled($thumb, $source, 0, 0, 0, 0, $sw*$d, $sh*$d, $sw, $sh);
				if ($ext == '.gif')
				imagegif($thumb,$newname);
				elseif ($ext == '.png')
				imagepng($thumb,$newname);
				else
				imagejpeg($thumb,$newname);
			}
			return $newurl;
		}
		else
		return $oldurl;
	}
	else
	return false;
}

/**
 * функция, формирующая изображение с приведением к заданной высоте
 */
function SimaiImageFixedHeight($img,$s)
{
	$img = IntVal($img);
	$s = IntVal($s);
	$file = CFile::GetByID($img);
	if ($file = $file->Fetch())
	{
		$path = $_SERVER["DOCUMENT_ROOT"].'/upload/'.$file[SUBDIR].'/';
		$oldname = $path.$file[FILE_NAME];
		$oldurl = '/upload/'.$file[SUBDIR].'/'.$file[FILE_NAME];
		$size = getimagesize($oldname);
		$sh = $size[1];
		$sw = $size[0];
		$type = $size[2];
		$ext = "";
		if ($type == 1)
		$ext = ".gif";
		elseif ($type == 3)
		$ext = ".png";
		elseif ($type == 2)
		$ext = ".jpg";
		$newname = $path.$img."_h_".$s.$ext;
		$newurl = '/upload/'.$file[SUBDIR].'/'.$img."_h_".$s.$ext;
		if (($sh != $s) and ($ext != ""))
		{
			if (!file_exists($newname))
			{
				if ($ext == '.gif')
				$source = imagecreatefromgif($oldname);
				elseif ($ext == '.png')
				$source = imagecreatefrompng($oldname);
				else
				$source = imagecreatefromjpeg($oldname);
				$d = $s / $sh;
				$thumb = @imagecreatetruecolor($sw*$d, $sh*$d);
				imagecopyresampled($thumb, $source, 0, 0, 0, 0, $sw*$d, $sh*$d, $sw, $sh);
				if ($ext == '.gif')
				imagegif($thumb,$newname);
				elseif ($ext == '.png')
				imagepng($thumb,$newname);
				else
				imagejpeg($thumb,$newname);
			}
			return $newurl;
		}
		else
		return $oldurl;
	}
	else
	return false;
}

/**
 * Функция ShowCustomSizeImage(): генерация миниатюры с зад. размерами
 * Параметры:
 *   $img          - ID изображения
 *   $w            - ширина в пикселях
 *   $h            - высота в пикселях
 */
function ShowCustomSizeImage($img,$w,$h) {
	$site=CSite::GetByID(SITE_ID);
	$arSite=$site->Fetch();
	$file=CFile::GetByID($img);
	$file=$file->Fetch();

	$filename=substr($file[FILE_NAME],0,strrpos($file[FILE_NAME],'.'));
	$ext=substr($file[FILE_NAME],strrpos($file[FILE_NAME],'.'),strlen($file[FILE_NAME]));
	$path=$arSite['ABS_DOC_ROOT'].'/upload/';
	$newurl=$arSite[DIR].'upload/'.$file[SUBDIR].'/'.$filename.'_cu'.$w.'_'.$h.$ext;
	$newname=$path.'/'.$file[SUBDIR].'/'.$filename.'_cu'.$w.'_'.$h.$ext;

	$oldname=$path.'/'.$file[SUBDIR].'/'.$filename.$ext;

	$size=getimagesize($oldname);
	$sh=$size[1];
	$sw=$size[0];

	if (($sw!=$w) or ($sh!=$h)) {
		if (!file_exists($newname)) {
			if (strpos($ext,'gif')) {
				$source = imagecreatefromgif($oldname);
			} elseif (strpos($ext,'png')) {
				$source = imagecreatefrompng($oldname);
			} else {
				$source = imagecreatefromjpeg($oldname);
			}

			if (($sh / $sw) > ($h / $w))
			{
				$d=$w/$sw;
				$sy = ($sh-$h/$d)/2;
				$sx = 0;
				$ds = $sw*$d;
				$ssw = $sw;
				$ssh = $h/$d;
			}
			else
			{
				$d=$h/$sh;
				$sx = ($sw-$w/$d)/2;
				$sy = 0;
				$ds = $sh*$d;
				$ssw = $w/$d;
				$ssh = $sh;
			}

			$thumb = @imagecreatetruecolor($w, $h);

			imagecopyresampled($thumb, $source, 0, 0, $sx, $sy, $w, $h, $ssw, $ssh);
			if (strpos($ext,'gif')) {
				imagegif($thumb,$newname);
			} elseif (strpos($ext,'png')) {
				imagepng($thumb,$newname);
			} else {
				imagejpeg($thumb,$newname);
			}
		}
		return $newurl;
	} else {
		return '/upload/'.$file[SUBDIR].'/'.$file[FILE_NAME];
	}
}

/**
 * функция вывода изображения, приведенного к квадрату заданного размера
 */
function SimaiSquareImage($img,$s)
{
	$img = IntVal($img);
	$s = IntVal($s);
	$file = CFile::GetByID($img);
	if ($file = $file->Fetch())
	{
		$path = $_SERVER["DOCUMENT_ROOT"].'/upload/'.$file[SUBDIR].'/';
		$oldname = $path.$file[FILE_NAME];
		$oldurl = '/upload/'.$file[SUBDIR].'/'.$file[FILE_NAME];
		$size = getimagesize($oldname);
		$sh = $size[1];
		$sw = $size[0];
		$type = $size[2];
		$ext = "";
		if ($type == 1)
		$ext = ".gif";
		elseif ($type == 3)
		$ext = ".png";
		elseif ($type == 2)
		$ext = ".jpg";
		$newname = $path.$img."_sq_".$s.$ext;
		$newurl = '/upload/'.$file[SUBDIR].'/'.$img."_sq_".$s.$ext;
		if (($sw != $s) and ($sh != $s) and ($ext != ""))
		{
			if (!file_exists($newname))
			{
				if ($ext == '.gif')
				$source = imagecreatefromgif($oldname);
				elseif ($ext == '.png')
				$source = imagecreatefrompng($oldname);
				else
				$source = imagecreatefromjpeg($oldname);
				if($sh > $sw)
				{
					$d = $s / $sw;
					$sy = ($sh - $sw)/2;
					$sx = 0;
					$ds = $sw * $d;
					$ss = $sw;
				}
				else
				{
					$d = $s / $sh;
					$sx = ($sw - $sh)/2;
					$sy = 0;
					$ds = $sh * $d;
					$ss = $sh;
				}
				$thumb = @imagecreatetruecolor($s, $s);
				imagecopyresampled($thumb, $source, 0, 0, $sx, $sy, $ds, $ds, $ss, $ss);
				if ($ext == '.gif')
				imagegif($thumb,$newname);
				elseif ($ext == '.png')
				imagepng($thumb,$newname);
				else
				imagejpeg($thumb,$newname);
			}
			return $newurl;
		}
		else
		return $oldurl;
	}
	else
	return false;
}

/**
 * Функция AddOrderUserPropsImages(): добавляет произвольное множество рисунков из формы к профилю покупателя
 * Параметры:
 * 	$UserPropsID	- ID профиля покупателя
 * 	$ImageFields	- Массив названий полей содержащих рисунки
 */
function AddOrderUserPropsImages($UserPropsID, &$arResult, &$errorMessage, $ImageFields=array("RF"=>"PASS_RF_PHOTOS", "ZG"=>"PASS_ZG_PHOTOS"))
{
	global $_FILES, $_POST, $USER;
	$arResult["IMAGE_FIELDS"] = $ImageFields;

	if(!isset($UserPropsID) && !IntVal($UserPropsID) > 0)
	return false;

	if (!CModule::IncludeModule("sale"))
	{
		ShowError(GetMessage("SALE_MODULE_NOT_INSTALL"));
		return false;
	}
	if (!$USER->IsAuthorized())
	{
		$APPLICATION->AuthForm(GetMessage("SALE_ACCESS_DENIED"));
		return false;
	}
	$dbOrderUserProps = CSaleOrderUserProps::GetList(
	array("DATE_UPDATE" => "DESC"),
	array(
			"ID" => $UserPropsID,
			"USER_ID" => IntVal($USER->GetID())
	),
	false,
	false,
	array("PERSON_TYPE_ID", "ID")
	);
	if ($arOrderUserPropVals = $dbOrderUserProps->Fetch())
	{

		$dbOrderProps = CSaleOrderProps::GetList(
		array(),
		array(
				"PERSON_TYPE_ID" => $arOrderUserPropVals["PERSON_TYPE_ID"],
				"CODE" => array_values($ImageFields)
		),
		false,
		false,
		array("CODE", "ID", "NAME")
		);
		while ($arOrderPropsVals = $dbOrderProps->Fetch())
		{
			$dbOrderUserPropsValue = CSaleOrderUserPropsValue::GetList(
			array(),
			array(
					"USER_PROPS_ID"=>$UserPropsID,
					"ORDER_PROPS_ID"=>$arOrderPropsVals["ID"]
			),
			false,
			false,
			array("ID", "NAME", "VALUE")
			);
			if ($arOrderUserPropsValueVals = $dbOrderUserPropsValue->Fetch())
			{
				$arFileOldIDs = explode(",", $arOrderUserPropsValueVals["VALUE"]);
			}
			else
			{
				$arFileOldIDs = array('','','');
				CSaleOrderUserPropsValue::Add(array(
				"USER_PROPS_ID" => $UserPropsID,
				"ORDER_PROPS_ID" => $arOrderPropsVals["ID"],
				"NAME" => $arOrderPropsVals["NAME"],
				"VALUE" => implode(",", $arFileOldIDs)
				));
			}
			$arFileIDs = array();
			if(!empty($_FILES[$arOrderPropsVals["CODE"]]))
			{
				$arImagesTmp = array();
				foreach ($_FILES[$arOrderPropsVals["CODE"]] as $arKey => $arValues)
				foreach ($arValues as $key => $value)
				$arImagesTmp[$key][$arKey] = $value;

				foreach ($arImagesTmp as $key => $arImage)
				if (!empty($arImage["name"]))
				$arImagesTmp[$key]["del"] = "Y";

				foreach ($arImagesTmp as $key => $arImage)
				{
					$res = CFile::CheckImageFile($arImage);
					if (strlen($res) == 0)
					{
						if(strlen($arImage["name"]) > 0 && !empty($arFileOldIDs[$key]))
						{
							CFile::Delete($arFileOldIDs[$key]);
							$arFileOldIDs[$key] = '';
						}
						if(empty($arFileOldIDs[$key]) && !empty($arImage["name"]))
						{
							$fid = CFile::SaveFile($arImage, "passport_scans");
							if (intval($fid) > 0)
							{
								$arFileIDs[] = $fid;
							}
							else
							{
								$arFileIDs[] = "";
								$errorMessage.= "Error on save image file: ".$arImage["name"]."<br>";
							}
						}
						else
						{
							if($_POST[$arOrderPropsVals["CODE"]."_DEL_".$key] == "on")
							{
								CFile::Delete($arFileOldIDs[$key]);
								$arFileIDs[$key] = '';
							}
							else
							{
								$arFileIDs[$key] = $arFileOldIDs[$key];
							}
						}
					}
					else
					{
						$errorMessage.= $res."<br>";
					}
				}
			}
			if(empty($arFileIDs))
			{
				$arResult[$arOrderPropsVals["CODE"]] = $arFileOldIDs;
			}
			else
			{
				$arResult[$arOrderPropsVals["CODE"]] = $arFileIDs;
				CSaleOrderUserPropsValue::Delete(IntVal($arOrderUserPropsValueVals["ID"]));
				CSaleOrderUserPropsValue::Add(array(
				"USER_PROPS_ID" => $UserPropsID,
				"ORDER_PROPS_ID" => $arOrderPropsVals["ID"],
				"NAME" => $arOrderPropsVals["NAME"],
				"VALUE" => implode(",", $arFileIDs)
				));
			}
		}
	}
}

?>