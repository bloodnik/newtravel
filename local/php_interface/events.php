<?php

use Bitrix\Main;


//Устанавливаем свой формат цены
AddEventHandler("currency", "CurrencyFormat", "myFormat");
function myFormat($fSum, $strCurrency) {
	return number_format($fSum, 0, '.', ' ') . ' Р';
}

//Модифицируем письмо о новом заказе
AddEventHandler("sale", "OnOrderNewSendEmail", "bxModifySaleMails");
function bxModifySaleMails($orderID, &$eventName, &$arFields) {

	\Bitrix\Main\Loader::includeModule('sale');
	\Bitrix\Main\Loader::includeModule('catalog');
	$order      = \Bitrix\Sale\Order::load($orderID);
	$product    = $order->getBasket()->getOrderableItems();
	$product_id = 0;
	foreach ($product as $basketItem) {
		$product_id = $basketItem->getField('PRODUCT_ID');
	}

	$ar_res = CCatalogProduct::GetByIDEx($product_id);

	$props         = $ar_res['PROPERTIES'];
	$strOrderProps = '<ul style="list-style-type: none; padding: 0;">';
	$strOrderProps .= '<li>Название заказа: ' . $ar_res["NAME"] . '</li>';
	if ($props['USE_DISCOUNT']['VALUE'] == "Y") {
		$strOrderProps .= '<li>ТУР СО СКИДКОЙ</li>';
	}
	if ($props['tourname']['VALUE']) {
		$strOrderProps .= '<li>Название тура: ' . $props['tourname']['VALUE'] . '</li>';
	}
	if ($props['operatorname']['VALUE']) {
		$strOrderProps .= '<li>Туроператор: ' . $props['operatorname']['VALUE'] . '</li>';
	}
	if ($props['departurename']['VALUE']) {
		$strOrderProps .= '<li>Город вылета: ' . $props['departurename']['VALUE'] . '</li>';
	}
	if ($props['countryname']['VALUE']) {
		$strOrderProps .= '<li>Страна: ' . $props['countryname']['VALUE'] . '</li>';
	}
	if ($props['hotelregionname']['VALUE']) {
		$strOrderProps .= '<li>Курорт: ' . $props['hotelregionname']['VALUE'] . '</li>';
	}
	if ($props['flydate']['VALUE']) {
		$strOrderProps .= '<li>Дата вылета: ' . $props['flydate']['VALUE'] . '</li>';
	}
	if ($props['hotelname']['VALUE']) {
		$strOrderProps .= '<li>Отель: ' . $props['hotelname']['VALUE'] . ' ' . $props['hotelstars']['VALUE'] . '*</li>';
	}
	if ($props['nights']['VALUE']) {
		$strOrderProps .= '<li>Ночей: ' . $props['nights']['VALUE'] . '</li>';
	}
	if ($props['room']['VALUE']) {
		$strOrderProps .= '<li>Номер: ' . $props['room']['VALUE'] . '</li>';
	}
	if ($props['meal']['VALUE']) {
		$strOrderProps .= '<li>Питание: ' . $props['meal']['VALUE'] . '</li>';
	}
	if ($props['placement']['VALUE']) {
		$strOrderProps .= '<li>Размещение: ' . $props['placement']['VALUE'] . '</li>';
	}
	if ($props['adults']['VALUE']) {
		$strOrderProps .= '<li>Взрослых: ' . $props['adults']['VALUE'] . '</li>';
	}
	if ($props['child']['VALUE']) {
		$strOrderProps .= '<li>Детей: ' . $props['child']['VALUE'] . '</li>';
	}
	if ($props['childage1']['VALUE']) {
		$strOrderProps .= '<li>Возраст ребенка 1: ' . $props['childage1']['VALUE'] . '</li>';
	}
	if ($props['childage2']['VALUE']) {
		$strOrderProps .= '<li>Возраст ребенка 2: ' . $props['childage2']['VALUE'] . '</li>';
	}
	if ($props['childage3']['VALUE']) {
		$strOrderProps .= '<li>Возраст ребенка 3: ' . $props['childage3']['VALUE'] . '</li>';
	}
	$strOrderProps .= '<li>Данные авиарейса: ' . $order->getPropertyCollection()->getItemByOrderPropertyId(50)->getValue() . '</li>';

	//Получает название выбранного способа оплаты
	$paySystemName = "";
	foreach ($order->getPaymentCollection() as $payment) {
		$paySystemName = $payment->getPaymentSystemName();
	}
	$strOrderProps .= '<li>Способ оплаты: ' . $paySystemName . '</li>';


	//Получает название выбранного способа оплаты
	$deliverySystemName = "";
	/**
	 * @var $shipment \Bitrix\Sale\Shipment
	 */
	foreach ($order->getShipmentCollection() as $shipment) {
		$deliverySystemName = $shipment->getDeliveryName();
	}
	$strOrderProps .= '<li>Способ получения документов: ' . $deliverySystemName . '</li>';


	$strOrderProps .= '</ul>';

	$arFields['ORDER_PROPS'] = $strOrderProps;

	$strUserProps = '<ul style="list-style-type: none; padding: 0;">';
	$strUserProps .= '<li>ФИО покупателя: ' . $order->getPropertyCollection()->getPayerName()->getValue() . '</li>';
	$strUserProps .= '<li>Телефон покупателя: ' . $order->getPropertyCollection()->getPhone()->getValue() . '</li>';

	if ($order->getPropertyCollection()->getItemByOrderPropertyId(45)->getValue()) {
		$strUserProps .= '<li>Паспортные данные 1: ' . $order->getPropertyCollection()->getItemByOrderPropertyId(45)->getValue() . '</li>';
	}
	if ($order->getPropertyCollection()->getItemByOrderPropertyId(46)->getValue()) {
		$strUserProps .= '<li>Паспортные данные 2: ' . $order->getPropertyCollection()->getItemByOrderPropertyId(46)->getValue() . '</li>';
	}
	if ($order->getPropertyCollection()->getItemByOrderPropertyId(47)->getValue()) {
		$strUserProps .= '<li>Паспортные данные 3: ' . $order->getPropertyCollection()->getItemByOrderPropertyId(47)->getValue() . '</li>';
	}
	if ($order->getPropertyCollection()->getItemByOrderPropertyId(48)->getValue()) {
		$strUserProps .= '<li>Паспортные данные 4: ' . $order->getPropertyCollection()->getItemByOrderPropertyId(48)->getValue() . '</li>';
	}
	if ($order->getPropertyCollection()->getItemByOrderPropertyId(56)->getValue()) {
		$strUserProps .= '<li>Ребенок 1: ' . $order->getPropertyCollection()->getItemByOrderPropertyId(56)->getValue() . '</li>';
	}
	if ($order->getPropertyCollection()->getItemByOrderPropertyId(57)->getValue()) {
		$strUserProps .= '<li>Ребенок 2: ' . $order->getPropertyCollection()->getItemByOrderPropertyId(57)->getValue() . '</li>';
	}
	if ($order->getPropertyCollection()->getItemByOrderPropertyId(58)->getValue()) {
		$strUserProps .= '<li>Ребенок 3: ' . $order->getPropertyCollection()->getItemByOrderPropertyId(58)->getValue() . '</li>';
	}

	$strUserProps .= '<li>Комментарий покупателя: ' . $order->getField('USER_DESCRIPTION') . '</li>';
	$strUserProps .= '</ul>';

	$arFields['USER_PROPS'] = $strUserProps;

}

//Приводим номера телефонов в формат без пробелов, тире и скобок
AddEventHandler('form', 'onBeforeResultAdd', 'NTonBeforeResultAdd');
function NTonBeforeResultAdd($WEB_FORM_ID, &$arFields, &$arrVALUES) {
	if ($WEB_FORM_ID == 9) {
		$arrVALUES['form_text_75'] = clearPhoneString($arrVALUES['form_text_75']);
	}
	if ($WEB_FORM_ID == 6) {
		$arrVALUES['form_text_58'] = clearPhoneString($arrVALUES['form_text_58']);
	}
	if ($WEB_FORM_ID == 1) {
		$arrVALUES['form_text_84'] = clearPhoneString($arrVALUES['form_text_84']);
	}
}


//Обработчик после добавления результата веб формы
AddEventHandler('form', 'onAfterResultAdd', 'my_onAfterResultAddUpdate');
function my_onAfterResultAddUpdate($WEB_FORM_ID, $RESULT_ID) {
	// действие обработчика распространяется только на форму с ID=6
	if ($WEB_FORM_ID == 9) {

	}
}


//Отправляем письмо о успешно оплате заказа
AddEventHandler('sale', 'OnSaleOrderPaid', 'OnSaleOrderPaidHandler');
/**
 * @param \Bitrix\Sale\Order $order
 */
function OnSaleOrderPaidHandler($order) {
	$bPaid = ($order->isPaid() ? true : false);

	if ($bPaid) {
		$arFields = array(
			"ORDER_ID" => $order->getId(),
			"FIO"      => $order->getPropertyCollection()->getPayerName()->getValue(),
			"PHONE"    => $order->getPropertyCollection()->getPhone()->getValue(),
		);
		CEvent::SendImmediate(
			"ORDER_PAYED_NOTIF",
			's1',
			$arFields,
			$Duplicate = "Y"
		);
	}
}

//// Добавляем новые макросы в почтовый шаблон
AddEventHandler("main", "OnBeforeEventAdd", array("MyClass", "OnBeforeEventAddHandler"));

class MyClass {
	function OnBeforeEventAddHandler(&$event, &$lid, &$arFields, &$message_id, &$files) {
		global $APPLICATION;
		$arFields["CUR_PATH"] = idn_to_utf8($_SERVER['HTTP_HOST']) . $APPLICATION->GetCurPage();

		AddMessage2Log(\Bitrix\Main\Web\Json::encode($files));

		if (strpos($event, 'FORM_FILLING_') !== false || strpos($event, 'SEND_CREDIT_CEB_CALLBACK') !== false) {
			if ( ! is_array($files)) {
				$files = [];
			}

			foreach ($arFields as $key => $field) {
				if ($link = self::getLinkFromField($field)) {
					if ($arFile = self::getFileFromLink($link)) {
						$files[] = $arFile['FILE_ID'];
					}
				}
			}
		}
	}

	static function getLinkFromField($field) {
		preg_match("/href=[\"'](.*form_show_file.*)[\"']/", $field, $out);

		return ($out[1] ?: false);
	}

	static function getFileFromLink($link) {
		$uri = new \Bitrix\Main\Web\Uri($link);
		parse_str($uri->getQuery(), $query);

		return CFormResult::GetFileByHash($query["rid"], $query["hash"]);
	}
}

// регистрируем обработчик
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("CIblockElementAdd", "OnAfterIBlockElementAddHandler"));

class CIblockElementAdd {
	// создаем обработчик события "OnAfterIBlockElementAdd"
	function OnAfterIBlockElementAddHandler(&$arFields) {

		//Уведомление о новом отзыве
		if ($arFields["IBLOCK_ID"] == 71) {
			$arFields = array(
				"REVIEW_TEXT" => $arFields["PREVIEW_TEXT"],
				"IBLOCK_ID"   => $arFields["IBLOCK_ID"],
				"ELEMENT_ID"  => $arFields["ID"],
			);
			if ($arFields["ID"] > 0) {
				CEvent::SendImmediate(
					"NEW_REVIEW_ADDED",
					's1',
					$arFields,
					$Duplicate = "Y"
				);
			}

		}
	}
}

/*Маска для генерации купона*/
\Bitrix\Main\EventManager::getInstance()->addEventHandler('sale', 'onGenerateCoupon', 'myFunction');
function myFunction(Main\Event $event)
{
	$check = $event->getParameter('CHECK');

	$prefix = "UMNYE-TURISTY-";

	if ($check)
	{
		do {
			$myCoupon = $prefix . randString(4, array("ABCDEFGHIJKLNMOPQRSTUVWXYZ1234567890"));; // тут генеришь код купона
		} while (\Bitrix\Sale\DiscountCouponsManager::isExist($myCoupon));
    }else {
		$myCoupon = $prefix . randString(4, array("ABCDEFGHIJKLNMOPQRSTUVWXYZ1234567890"));; // тут генеришь код купона
	}

	$result = new \Bitrix\Main\EventResult(\Bitrix\Main\EventResult::SUCCESS, $myCoupon);

	return $result;
}