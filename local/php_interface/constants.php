<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
global $APPLICATION;


define("IS_INDEX", ($APPLICATION->GetCurDir()==SITE_DIR)?(true):(false));
define("IS_TOURS", ($APPLICATION->GetCurDir()=="/tours/")?(true):(false));
define("IS_HOTELS", (stripos($APPLICATION->GetCurDir(), "/hotel/") !== false)?(true):(false));
define("IS_PROMO", (stripos($APPLICATION->GetCurDir(), "/promo/") !== false)?(true):(false));

//Оборачиваем ли контент в контейнер
$show_container = true;
if($APPLICATION->GetCurDir() == '/faq/')
	$show_container = false;
define("SHOW_CONTAINER", $show_container);
