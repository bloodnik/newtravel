<?
class CSimaiTravelSletatSoap
{
	/**
	 * Ссылка на описание сервиса
	 * @var string
	 */
	private $WSDL = 'http://module.sletat.ru/XmlGate.svc?wsdl';

	/**
	 * Логин пользователя sletat.ru
	 * @var string
	 */
	private $login = 'login_example';

	/**
	 * Пароль пользователя sletat.ru
	 * @var string
	 */
	private $pass = 'password_example';

	/**
	 * Клиент SOAP
	 * @var SoapClient
	 */
	public $client = false;

	/**
	 * Конструктор объекта
	 * Enter description here ...
	 * @param unknown_type $login
	 * @param unknown_type $pass
	 */
	public function __construct($login, $pass, $WSDL = false)
	{
		if ($WSDL!==false)
			$this->WSDL = $WSDL;
		$this->login = $login;
		$this->pass = $pass;
	}

	/**
	 * Вызов метода SOAP-сервиса
	 * @param string $methodName - название метода сервиса
	 * @param array $param - набор параметров ключ -> значение
	 */
	public function getMethod($methodName, $param = array())
	{
		if(!is_object($this->client))
		{
			$this->client = new SoapClient($this->WSDL);
			$header = new SoapHeader("urn:SletatRu:DataTypes:AuthData:v1", "AuthInfo", array("Login"=>$this->login, 'Password'=>$this->pass));
			$this->client->__setSoapHeaders($header);
		}

		if(	!is_object($this->client) ) echo 'soap';
		$result = $this->client->$methodName($param);

		if (!is_soap_fault($result))
		{
			return $this->objectToArr($result);
		}
		else return 'Soap Fault. Use __getLastRequest() and __getLastResponse';
	}

	/**
	 * Преобразует объект в массив
	 * @param object|array $obj - объект или массив
	 * @return array - массив
	 */
	private function objectToArr($obj)
	{
		$tmp = array();
		foreach ($obj as $key => $value)
			if (!is_array($value) && !is_object($value))
			{
				if (is_bool($value))
					$tmp[(string)$key]  = $value?1:0;				// можно заменить на true и false
				else
					$tmp[(string)$key] = (string)$value;
			}
			else
				$tmp[(string)$key] = $this->objectToArr($value);
		return $tmp;
	}


}
?>