<?

/***************************************
				Ответы
***************************************/

class CTourVisor
{

	public static $login = "lili";
	public static $pass = "271000";


	private function GetData ($methodName, $param = array()) {
		if(self::$soap == false) {
			self::$soap = new CSimaiTravelSletatSoap(self::$login, self::$pass);
		}
		
		/*$w = @fopen ($_SERVER['DOCUMENT_ROOT']."/upload/soap_log.csv", "at");
		@fwrite($w, $methodName."; ".implode(", ",$param)."; ".$_SERVER['REMOTE_ADDR']."; ".date("d-m-Y H:i:s")."; ".$_SERVER['REQUEST_URI']."\n");			
		@fclose($w);*/

		return self::$soap->getMethod($methodName, $param);
	}


	//
	// Функции обертки для получения данных
	//


	public function GetHotelInformation($hotelId=0) {
		$params = array();
		if($hotelId > 0) $params["hotelId"] = $hotelId;

		 $cssStylesheet= "http://localhost:6448".SITE_TEMPLATE_PATH."/css/style.css";
		 
		 $params["cssStylesheet"] = $cssStylesheet;
		 
		$data = self::GetData("GetHotelInformation",$params);

		if(is_array($data["GetHotelInformationResult"]))
			return $data["GetHotelInformationResult"];
		return array();
	}


	// Список городов вылета
	public function GetDepartCities() {
		$data = self::GetData("GetDepartCities");
		if(is_array($data["GetDepartCitiesResult"]["City"]))
			return $data["GetDepartCitiesResult"]["City"];
		return array();
	}

	// Список стран
	public function GetCountries($townFromId = 0) {
		$params = array();
		if($townFromId > 0) $params["townFromId"] = $townFromId;
		$data =  self::GetData("GetCountries", $params);

		if(is_array($data["GetCountriesResult"]["Country"]))
			return $data["GetCountriesResult"]["Country"];
		return array();
	}

	// Список курортов (обязательно нужно указать страну назначения)
	public function GetCities($countryId = 0) {
		$params = array();
		if($countryId > 0) $params["countryId"] = $countryId;
		$data =  self::GetData("GetCities", $params);
		if(is_array($data["GetCitiesResult"]["City"]))
			return $data["GetCitiesResult"]["City"];
		return array();
	}

	// Список категорий отелей (обязательно нужно указать страну назначения)
	public function GetHotelStars($countryId = 0, $towns = array()) {
		$params = array();
		if($countryId > 0) $params["countryId"] = $countryId;
		if($towns > 0) $params["towns"] = $towns;
		$data =  self::GetData("GetHotelStars", $params);
		if(is_array($data["GetHotelStarsResult"]["HotelStars"])) {
			if(empty($data["GetHotelStarsResult"]["HotelStars"]["Id"]) and empty($data["GetHotelStarsResult"]["HotelStars"]["Name"]))
				return $data["GetHotelStarsResult"]["HotelStars"];
			else
				return array($data["GetHotelStarsResult"]["HotelStars"]);
		}
		return array();
	}

	// Список видов питания
	public function GetMeals() {
		$data =  self::GetData("GetMeals", $params);
		if(is_array($data["GetMealsResult"]["Meal"])) {
			return $data["GetMealsResult"]["Meal"];
		}
		return array();
	}

	// Список отелей
	public function GetHotels ($countryId=0, $towns=array(), $stars=array(), $filter="", $count=-1) {
		$params = array();
		if($countryId > 0) $params["countryId"] = $countryId;
		$params["towns"] = $towns;
		$params["stars"] = $stars;
		$params["count"] = $count;
		if($filter != "") $params["filter"] = $filter;
		$data =  self::GetData("GetHotels", $params);

		if(is_array($data["GetHotelsResult"]["Hotel"])) {
			if(empty($data["GetHotelsResult"]["Hotel"]["Id"]) and empty($data["GetHotelsResult"]["Hotel"]["Name"]))
				return $data["GetHotelsResult"]["Hotel"];
			else
				return array($data["GetHotelsResult"]["Hotel"]);
		}
		return array();

	}
	
	// Запрос тура
	public function CreateRequest($params) {

		// Переопределяем общие параметры
		$params['requestId'] = 0;
		$params['pageSize'] = 30;
		$params['pageNumber'] = 1;

		$params['hotelIsNotInStop'] = true;
		$params['hasTickets'] = true; //false;
		$params['ticketsIncluded'] = true; //false;

		$params['updateResult'] = true;
		$params['useFilter'] = false;
		$params['f_to_id'] = array();
		$params['useTree'] = false;
		$params['groupBy'] = false;
		$params['includeDescriptions'] = true;


		$data = self::GetData("CreateRequest", $params);
		if(!empty($data["CreateRequestResult"])) {
			return $data["CreateRequestResult"];
		}
		return false;
	}

	// Запрос статуса обработки тура
	public function GetRequestState($id) {

		// Смотрим есть ли запись в кеше
		global $DB;
		$sql = "SELECT * FROM `b_simai_sletat_cache` WHERE `request`=".$id;
		$r = $DB->Query($sql, true, $err_mess.__LINE__);
		if ($row = $r->GetNext()) {
			return unserialize($row["~status"]);
		}


		$data = self::GetData("GetRequestState", array('requestId'=>$id));
		if(is_array($data["GetRequestStateResult"]["OperatorLoadState"])) {

			// Преобразовываем в массив если всего один оператор
			if (isset($data["GetRequestStateResult"]["OperatorLoadState"]["Id"])) {
				$temp = $data["GetRequestStateResult"]["OperatorLoadState"];
				unset($data["GetRequestStateResult"]["OperatorLoadState"]);
				$data["GetRequestStateResult"]["OperatorLoadState"][] = $temp;
			}

			$isLoad = true;
			$IsError = $IsSkipped = $IsTimeout = $IsProcessed = 0;
			foreach ($data['GetRequestStateResult']['OperatorLoadState'] as $n => $info)
			{
				if (!$info['IsProcessed'])
					$isLoad = false;
				else
					$IsProcessed++;
				if ($info['IsError']) $IsError++;
				if ($info['IsSkipped']) $IsSkipped++;
				if ($info['IsTimeout']) $IsTimeout++;



			}
			return array(
				"status" => $isLoad,
				"count" => count($data['GetRequestStateResult']['OperatorLoadState']),
				"load" => $IsProcessed,
				"error" => $IsError,
				"skipped" => $IsSkipped,
				"timeout" => $IsTimeout,
			);
		}
		return false;
	}

	// Выдача результатов
	public function GetRequestResult($id) {
		global $DB;

		// Смотрим есть ли запись в кеше
		$sql = "SELECT * FROM `b_simai_sletat_cache` WHERE `request`=".$id;
		$r = $DB->Query($sql, true, $err_mess.__LINE__);
		if ($row = $r->GetNext()) {
			//var_dump(unserialize($row["~answer"]));
			//echo $row["~answer"];
			//echo "длина ответа".strlen($row["~answer"]);
			return unserialize($row["~answer"]);
		}

		// Если нет то получаем результат

        try {
            $data = self::GetData("GetRequestResult", array('requestId'=>$id));
        } catch (Exception $e) {
            echo 'Чтобы посмотреть результаты начните поиск';
        }

		// Если результат не нулевой то пишем его в кеш
		if(count($data["GetRequestResultResult"]["Rows"]["XmlTourRecord"]) > 0) {

			// Статус тоже пишем, так как он может пригодиться
			$status = self::GetRequestState($id);
			if($status != false) {

				$sql = "INSERT INTO `b_simai_sletat_cache` (`request`, `answer`, `status`) VALUES (".$id.", '".str_replace("'", "\'", serialize($data))."', '".serialize($status)."')";
				$r = $DB->Query($sql, true, $err_mess.__LINE__);

				// Примитивная система очистки кеша
				if(rand(666, 888) == 777) {
					$sql = "DELETE FROM `b_simai_sletat_cache` WHERE date < DATE_ADD(CURDATE(), INTERVAL -1 DAY)";
					$r = $DB->Query($sql, true, $err_mess.__LINE__);
				}
			}
		}

		return $data;
	}
	
	// Запрос сохранения заказа тура в системе
	public function SaveTourOrder($params) {
		$data = self::GetData("SaveTourOrder", $params);
		return false;
	}	
}
?>