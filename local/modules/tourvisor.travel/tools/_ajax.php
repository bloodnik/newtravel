<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

// Кеширование
$obCache = new CPageCache;
$life_time = 60*60; // кешируем на 1 час
$cache_id = serialize($_REQUEST).rand(1,99999999);

// инициализируем буферизирование вывода
if($obCache->StartDataCache($life_time, $cache_id, "/")):

	// Подключение модуля
	CModule::IncludeModule("simai.travel");

	// Страны назначения
	if($_REQUEST["type"] == "country") {
		$results = array();
		$results["results"] = CSimaiTravelSletat::GetCountries($_REQUEST["from"]);
		echo json_encode($results);
	}

	// Курорты
	if($_REQUEST["type"] == "resort") {
		$results = array();
		$results["results"] = CSimaiTravelSletat::GetCities($_REQUEST["country"]);
		echo json_encode($results);
	}

	// Категории отелей
	if($_REQUEST["type"] == "stars") {
		$resorts = array();
		if(!empty($_REQUEST["resorts"])) {
			$resorts = explode(",", substr($_REQUEST["resorts"], 0, -1));
		}
		$results = array();
		$results["results"] = CSimaiTravelSletat::GetHotelStars($_REQUEST["country"], $resorts);
		echo json_encode($results);
	}

	// Список отелей
	if($_REQUEST["type"] == "hotels") {
		$hotels = array();
		$resorts = array();
		if(!empty($_REQUEST["resorts"])) {
			$resorts = explode(",", substr($_REQUEST["resorts"], 0, -1));
		}
		$stars = array();
		if(!empty($_REQUEST["stars"])) {
			$stars = explode(",", substr($_REQUEST["stars"], 0, -1));
		}
		$filter = (!empty($_REQUEST["filter"])) ? $_REQUEST["filter"] : "";
		$count = (!empty($_REQUEST["count"])) ? $_REQUEST["count"] : 50;

		$results = array();
		$results["results"] = CSimaiTravelSletat::GetHotels($_REQUEST["country"], $resorts, $stars, $filter, $count);
		echo json_encode($results);
	}

	// Запрос данных по турам
	if($_REQUEST["type"] == "search") {

		// Обрабатываем детей
		$kidsAges = array();
		for($i=1; $i<=$_POST["kids"]; $i++) {
			$kidsAges[] = $_POST["kid".$i];
		}
		// Параметры запроса
		$params = array(
			'requestId'=>0,
			'pageSize'=>30,
			'pageNumber'=>1,
			"countryId"=>$_POST["countryId"],
			"cityFromId"=>$_POST["cityFromId"],
			"cities"=>(!empty($_POST["resortsAny"]) or !is_array($_POST["resortsList"])) ? array() : $_POST["resortsList"],
			"meals"=>(!empty($_POST["mealsAny"]) or !is_array($_POST["mealsList"])) ? array() : $_POST["mealsList"],
			"stars"=>(!empty($_POST["starsAny"]) or !is_array($_POST["starsList"])) ? array() : $_POST["starsList"],
			"hotels"=> (!empty($_POST["hotelsAny"]) or !is_array($_POST["hotelsList"])) ? array() : $_POST["hotelsList"],
			"adults"=>$_POST["adults"],
			"kids"=>$_POST["kids"],
			"kidsAges"=>$kidsAges,
			"nightsMin" => $_POST["nightsMin"],
			"nightsMax" => $_POST["nightsMax"],
			"priceMin" => $_POST["priceMin"],
			"priceMax" => $_POST["priceMax"],
			"currencyAlias" => $_POST["currencyAlias"],
			"departFrom" => $_POST["departFrom"],
			"departTo" => $_POST["departTo"],
			'hotelIsNotInStop'=>true,

			'hasTickets'=>false,
			'ticketsIncluded'=>true, //false,

			'updateResult'=>true,
			'useFilter'=>false,
			'f_to_id'=>array(),
			'useTree'=>false,
			'groupBy'=>false,
			'includeDescriptions'=>true
		);

/*
		echo "<pre>";
		var_dump($_POST);
		var_dump($params);
		echo "</pre>";
*/
		$id = CSimaiTravelSletat::CreateRequest($params);
		$_SESSION["travel"]["request"][$id] = $params;
		echo $id;
	}

    // записываем предварительно буферизированный вывод в файл кеша
    $obCache->EndDataCache();

endif;


//
// Без кеширования
//

// Запрос статуса обработки данных
if($_REQUEST["type"] == "searchStatus") {
	CModule::IncludeModule("simai.travel");
	$answer = CSimaiTravelSletat::GetRequestState($_REQUEST["id"]);
	/*
	echo "<pre>".rand(1, 99);
	var_dump($answer);
	echo "</pre>";
	*/
	echo ($answer["status"] == true) ? "load"  : "wait";
}


// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>