<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$template = isset($_REQUEST["template"]) ? $_REQUEST["template"] : "full_data_redisign";

// Подключение компонента вывода результата
$APPLICATION->IncludeComponent("tourvisor.travel:result", $template, array(
	"REQUEST_ID" => $_REQUEST["requestid"],
	"IS_FINISH" => $_REQUEST["isfinish"], //Завершен ли поиск
    "OFFICE_PERCENT" => "5",//Процент наценки для покупки в офисе
    "SALE_PERCENT" => "11",//Процент наценки
    "PAGE" => $_REQUEST["page"],//текущая страница
    "ONPAGE" => $_REQUEST["onpage"]//На странице
	//"OFFER_ID" => $_REQUEST["offerId"], //09.07.2014 добавлен параметр offerId
	),
	false
);

// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>