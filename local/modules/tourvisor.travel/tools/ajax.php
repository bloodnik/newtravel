<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

// Кеширование
$obCache   = new CPageCache;
$life_time = 60 * 60; // кешируем на 1 час
$cache_id  = serialize($_REQUEST) . rand(1, 99999999);

// инициализируем буферизирование вывода
if ($obCache->StartDataCache($life_time, $cache_id, "/")):


	$login = "lili";
	$pass  = "271000";

	/**
	 * Получаем данные по отелю
	 *
	 * @param hotelcode - код отеля
	 */
	if ($_REQUEST["type"] == "fullhotel") {
		if (isset($_REQUEST["hotelcode"])) {
			$param = "&hotelcode=" . $_REQUEST["hotelcode"];
		}

		$url   = "http://tourvisor.ru/xml/hotel.php?format=json" . $param . "&authlogin=" . $login . "&authpass=" . $pass;
		$arRes = makeQuery($url);
		echo $arRes;
	}


	// departure - список городов вылета
	//Выдает полный список городов вылета, дополнительных параметров нет.
	if ($_REQUEST["type"] == "departure") {
		$url   = "http://tourvisor.ru/xml/list.php?format=json&type=departure&authlogin=" . $login . "&authpass=" . $pass;
		$arRes = makeQuery($url);
		echo $arRes;
	}

	/* country - список стран
	 * Без параметров выдается полный список стран (все страны, которые есть в системе).
	 *   Параметры:
	 *   cndep – город вылета. Выдается список стран, куда есть вылеты из указанного города
	 */
	if ($_REQUEST["type"] == "country") {
		if (isset($_REQUEST["cndep"])) {
			$param = "&cndep=" . $_REQUEST["cndep"];
		}

		$url   = "http://tourvisor.ru/xml/list.php?format=json&type=country" . $param . "&authlogin=" . $login . "&authpass=" . $pass;
		$arRes = makeQuery($url);
		echo $arRes;
	}

	/* region - список курортов
	*  Без параметров выдается полный список курортов (с кодами стран).
	*   Параметры:
	*   - regcountry – страна, по которой нужно получить список курортов.
	 */
	if ($_REQUEST["type"] == "region") {
		if (isset($_REQUEST["regcountry"])) {
			$param = "&regcountry=" . $_REQUEST["regcountry"];
		}

		$url   = "http://tourvisor.ru/xml/list.php?format=json&type=region" . $param . "&authlogin=" . $login . "&authpass=" . $pass;
		$arRes = makeQuery($url);
		echo $arRes;
	}
	/* region - список курортов
	*  Без параметров выдается полный список курортов (с кодами стран).
	*   Параметры:
	*   - regcountry – страна, по которой нужно получить список курортов.
	 */
	if ($_REQUEST["type"] == "subregion") {
		if (isset($_REQUEST["regcountry"])) {
			$param = "&regcountry=" . $_REQUEST["regcountry"];
		}
		if (isset($_REQUEST["parentregion"])) {
			$param = "&parentregion=" . $_REQUEST["parentregion"];
		}

		$url   = "http://tourvisor.ru/xml/list.php?format=json&type=subregion" . $param . "&authlogin=" . $login . "&authpass=" . $pass;
		$arRes = makeQuery($url);
		echo $arRes;
	}


	/* meal - список типов питания
	*  Без параметров
	 */
	if ($_REQUEST["type"] == "meal") {

		$url   = "http://tourvisor.ru/xml/list.php?format=json&type=meal" . $param . "&authlogin=" . $login . "&authpass=" . $pass;
		$arRes = makeQuery($url);
		echo $arRes;
	}

	/* stars - категорий отелей (звездность)
	*  Без параметров
	 */
	if ($_REQUEST["type"] == "stars") {

		$url   = "http://tourvisor.ru/xml/list.php?format=json&type=stars" . $param . "&authlogin=" . $login . "&authpass=" . $pass;
		$arRes = makeQuery($url);
		echo $arRes;
	}

	/* operator - список туроператоров
	*  Без параметров
	 */
	if ($_REQUEST["type"] == "operator") {

		$url   = "http://tourvisor.ru/xml/list.php?format=json&type=operator" . $param . "&authlogin=" . $login . "&authpass=" . $pass;
		$arRes = makeQuery($url);
		echo $arRes;
	}

	/* operator - список дат вылета
		Выдает список доступных дат вылета для выбранного города вылета и страны (используется в календарике).
		Параметры (все обязательны):
		- flydeparture – код города вылета (обязательно!).
		- flycountry – код страны (обязательно!)
	 */
	if ($_REQUEST["type"] == "flydate") {
		if (isset($_REQUEST["flydeparture"])) {
			$param = "&flydeparture=" . $_REQUEST["flydeparture"];
		}
		if (isset($_REQUEST["flycountry"])) {
			$param .= "&flycountry=" . $_REQUEST["flycountry"];
		}

		$url   = "http://tourvisor.ru/xml/list.php?format=json&type=flydate" . $param . "&authlogin=" . $login . "&authpass=" . $pass;
		$arRes = makeQuery($url);
		echo $arRes;
	}

	/* hotel - список отелей
		Выдает список отелей. Обязательно нужно указать хотя бы код страны (hotcountry)
		Параметры:
			- hotcountry – код страны, по которой нужно получить список отелей (ОБЯЗАТЕЛЬНО!)
			- hotregion – код курорта (можно несколько через запятую)
			- hotstars - звездность (и выше)
			- hotrating - рейтинг отеля (выше или равно которому будет отображаться)
			- hotactive - только отели "Активный"
			- hotrelax - только отели "Спокойный"
			- hotfamily - только отели "Семейный"
			- hothealth - только отели "Здоровье"
			- hotcity - только отели "Городской"
			- hotbeach - только отели "Пляжный"
			- hotdeluxe - только отели "Люкс (VIP)"
	 */
	if ($_REQUEST["type"] == "hotel") {
		if (isset($_REQUEST["hotcountry"])) {
			$param = "&hotcountry=" . $_REQUEST["hotcountry"];
		}
		if (isset($_REQUEST["hotregion"])) {
			$param .= "&hotregion=" . $_REQUEST["hotregion"];
		}
		if (isset($_REQUEST["hotstars"])) {
			$param .= "&hotstars=" . $_REQUEST["hotstars"];
		}
		if (isset($_REQUEST["hotrating"])) {
			$param .= "&hotrating=" . $_REQUEST["hotrating"];
		}
		if (isset($_REQUEST["hotactive"])) {
			$param .= "&hotactive=" . $_REQUEST["hotactive"];
		}
		if (isset($_REQUEST["hotrelax"])) {
			$param .= "&hotrelax=" . $_REQUEST["hotrelax"];
		}
		if (isset($_REQUEST["hotfamily"])) {
			$param .= "&hotfamily=" . $_REQUEST["hotfamily"];
		}
		if (isset($_REQUEST["hothealth"])) {
			$param .= "&hothealth=" . $_REQUEST["hothealth"];
		}
		if (isset($_REQUEST["hotcity"])) {
			$param .= "&hotcity=" . $_REQUEST["hotcity"];
		}
		if (isset($_REQUEST["hotbeach"])) {
			$param .= "&hotbeach=" . $_REQUEST["hotbeach"];
		}
		if (isset($_REQUEST["hotdeluxe"])) {
			$param .= "&hotdeluxe=" . $_REQUEST["hotdeluxe"];
		}

		$url   = "http://tourvisor.ru/xml/list.php?format=json&type=hotel" . $param . "&authlogin=" . $login . "&authpass=" . $pass;
		$arRes = makeQuery($url);
		echo $arRes;
	}

	/*
	 * Отправляем запрос на поиск тура
	 * Возварщается параметр requestid - ID запроса
	 * */
	if ($_REQUEST["type"] == "search") {
		if (isset($_REQUEST["departure"])) {
			$param = "&departure=" . $_REQUEST["departure"];
		}
		if (isset($_REQUEST["country"])) {
			$param .= "&country=" . $_REQUEST["country"];
		}
		if (isset($_REQUEST["datefrom"])) {
			$param .= "&datefrom=" . $_REQUEST["datefrom"];
		}
		if (isset($_REQUEST["dateto"])) {
			$param .= "&dateto=" . $_REQUEST["dateto"];
		}
		if (isset($_REQUEST["nightsfrom"])) {
			$param .= "&nightsfrom=" . $_REQUEST["nightsfrom"];
		}
		if (isset($_REQUEST["nightsto"])) {
			$param .= "&nightsto=" . $_REQUEST["nightsto"];
		}
		if (isset($_REQUEST["adults"])) {
			$param .= "&adults=" . $_REQUEST["adults"];
		}
		if (isset($_REQUEST["child"])) {
			$param .= "&child=" . $_REQUEST["child"];
		}
		if (isset($_REQUEST["childage1"])) {
			$param .= "&childage1=" . $_REQUEST["childage1"];
		}
		if (isset($_REQUEST["childage2"])) {
			$param .= "&childage2=" . $_REQUEST["childage2"];
		}
		if (isset($_REQUEST["childage3"])) {
			$param .= "&childage3=" . $_REQUEST["childage3"];
		}
		if (isset($_REQUEST["stars"])) {
			$param .= "&stars=" . $_REQUEST["stars"];
		}
		if (isset($_REQUEST["meal"])) {
			$param .= "&meal=" . $_REQUEST["meal"];
		}
		if (isset($_REQUEST["rating"])) {
			$param .= "&rating=" . $_REQUEST["rating"];
		}
		if (isset($_REQUEST["hotels"])) {
			$param .= "&hotels=" . $_REQUEST["hotels"];
		}
		if (isset($_REQUEST["regions"])) {
			$param .= "&regions=" . $_REQUEST["regions"];
		}
		if (isset($_REQUEST["subregions"])) {
			$param .= "&subregions=" . $_REQUEST["subregions"];
		}
		if (isset($_REQUEST["pricefrom"])) {
			$param .= "&pricefrom=" . $_REQUEST["pricefrom"];
		}
		if (isset($_REQUEST["priceto"])) {
			$param .= "&priceto=" . $_REQUEST["priceto"];
		}
		if (isset($_REQUEST["operators"])) {
			$param .= "&operators=" . $_REQUEST["operators"];
		}

		$urlParams = $param;

		$url = "http://tourvisor.ru/xml/search.php?format=json" . $param . "&authlogin=" . $login . "&authpass=" . $pass;
		//@$urlParams - нужен чтобы обновить строку URL в браузере
		$arRes = makeQuery($url, $urlParams);
		echo $arRes;
	}

	/*
	 * Проверяем статус запроса
	 *
	 * */
	if ($_REQUEST["type"] == "status") {
		if (isset($_REQUEST["requestid"])) {
			$param = "&type=status";
			$param .= "&requestid=" . $_REQUEST["requestid"];
			$param .= "&page=1";
			$param .= "&nodescription=1";

			$url   = "http://tourvisor.ru/xml/result.php?format=json" . $param . "&authlogin=" . $login . "&authpass=" . $pass;
			$arRes = makeQuery($url);
			echo $arRes;
		}
	}

	/*
	 * Проверяем статус запроса
	 *
	 * */
	if ($_REQUEST["type"] == "actualize") {
		if (isset($_REQUEST["tourid"])) {
			$param .= "&tourid=" . $_REQUEST["tourid"];
			$param .= "&flights=1";
			$param .= "&request=0";

			$url   = "http://tourvisor.ru/xml/actualize.php?format=json" . $param . "&authlogin=" . $login . "&authpass=" . $pass;
			$arRes = makeQuery($url);
			echo $arRes;
		}
	}
	// записываем предварительно буферизированный вывод в файл кеша
	$obCache->EndDataCache();

endif;


//
//Добавление в корзину дополнительных услуг
//
if ($_REQUEST["type"] == "addExtras") {
	if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
		if (isset($_REQUEST['id']) && (isset($_REQUEST['quantity']))) {
			$ID       = intval($_REQUEST['id']);
			$QUANTITY = intval($_POST['quantity']);
			$result   = Add2BasketByProductID(
				$ID,
				$QUANTITY,
				false
			);
			echo $result;
		} else {
			echo "Нет параметров ";
		}
	}
}

//
//Удаление из корзины дополнительных услуг
//
if ($_REQUEST["type"] == "delExtras") {
	if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
		if (isset($_REQUEST['id'])) {
			CSaleBasket::Delete($_REQUEST["id"]);
		} else {
			echo "Нет параметров ";
		}
	}
}

//Функция выполнения get-запроса через CURl
function makeQuery($url, $urlParams = "") {
	if ($curl = curl_init()) {
		curl_setopt($curl, CURLOPT_URL, $url);
	}
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$arRes = curl_exec($curl);

	if ( ! empty($urlParams)) {
		$arRes        = objectToArr(json_decode($arRes));
		$arRes["url"] = $urlParams;
		$arRes        = json_encode($arRes);
	}
	curl_close($curl);

	return $arRes;
}

// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>