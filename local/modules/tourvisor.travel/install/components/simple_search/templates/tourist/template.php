<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form action="<?=$arParams["ACTION"]?>" name="searchTour" method="GET" class="jClever" <?if($arParams["NEW"] == "Y"):?>target="new"<?endif;?>>
<div class="filter-main">
              <div class="filter-wrap">
<input type="hidden" name="start" value="Y">
<input type="hidden" name="count" value="<?=$arParams["COUNT"]?>">

 <div class="f-lcol">
 <div class="select-box">
					<select name="cityFromId" id="travelCityFrom">
						<option value="0">Откуда</option>
						<?foreach($arResult["from"] as $city):?>
							<option value="<?=$city["Id"]?>" <?if($arParams["CITY"] == $city["Id"]):?>SELECTED<?endif;?>><?=$city["Name"]?></option>
						<?endforeach?>
					</select>
					<select name="countryId" id="travelCountryTo">
						<option value="0">Куда</option>
						<?foreach($arResult["to"] as $country):?>
							<option value="<?=$country["Id"]?>" <?if($arParams["COUNTRY"] == $country["Id"]):?>SELECTED<?endif;?>><?=$country["Name"]?></option>
						<?endforeach?>
					</select>
</div>
					<div class="select-box">
						<select id="i-nightlist" class="sCombo" name="night">
							<option value="7-7">на 7 ночей</option>
							<option value="10-10">на 10 ночей</option>
							<option value="14-14">на 14 ночей</option>
							<option value="2-6">на 2-6 ночей</option>
							<option value="7-10">на 7-10 ночей</option>
							<option value="11-14">на 11-14 ночей</option>
							<option value="15-30">на 14 ночей</option>
						</select>
					</div>
				
</div>
<div class="f-rcol">
			<div class="date-pick simple">
				<input placeholder="Вылет с" id="dateAir" value="<?=date("d.m.Y")?>" class="from" name="date" />
			</div>
			<div class="c-row simple"><input type="checkbox" id="i-3day" name="3day" CHECKED><span>±3 дня</span></div>
					
		<input class="oform simple" type="submit" value="ИСКАТЬ ТУР">
</div>
<div class="big-form"><a href="/tours/">Перейти на расширенную форму поиска</a></div>
</div>
</div>
</form>