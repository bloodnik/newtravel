<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form action="<?=$arParams["ACTION"]?>" name="searchTour" method="GET" <?if($arParams["NEW"] == "Y"):?>target="new"<?endif;?>>
<input type="hidden" name="start" value="Y">
<input type="hidden" name="count" value="<?=$arParams["COUNT"]?>">
	<div>
		<table class="form_mini">
			<tbody><tr>
				<td>
					<p class="caption">Откуда</p>
					<select name="cityFromId" id="travelCityFrom">
						<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>
						<?foreach($arResult["from"] as $city):?>
							<option value="<?=$city["Id"]?>" <?if($arParams["CITY"] == $city["Id"]):?>SELECTED<?endif;?>><?=$city["Name"]?></option>
						<?endforeach?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<p class="caption">Куда</p>
					<select name="countryId" id="travelCountryTo">
						<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>
						<?foreach($arResult["to"] as $country):?>
							<option value="<?=$country["Id"]?>" <?if($arParams["COUNTRY"] == $country["Id"]):?>SELECTED<?endif;?>><?=$country["Name"]?></option>
						<?endforeach?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<div>
						<p class="caption">Продолжительность</p>
						<select id="i-nightlist" class="sCombo" name="night">
							<option value="7-7">7 ночей</option>
							<option value="10-10">10 ночей</option>
							<option value="14-14">14 ночей</option>
							<option value="2-6">2-6 ночей</option>
							<option value="7-10">7-10 ночей</option>
							<option value="11-14">11-14 ночей</option>
							<option value="15-30">&gt; 14 ночей</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<p class="caption">Дата вылета</p>
					<div id="i-date">
						<?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
						     "SHOW_INPUT" => "Y",
						     "FORM_NAME" => "searchTour",
						     "INPUT_NAME" => "date",
						     "INPUT_VALUE" => date("d.m.Y", time()+60*60*24*$arParams["DAY"]),
						     "SHOW_TIME" => "N",
						     "HIDE_TIMEBAR" => "Y"
							)
						);?>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="sCheck threedaycheck checked"><label><input type="checkbox" id="i-3day" name="3day" CHECKED><span>±3 дня</span></label></div>
				</td>
			</tr>
		</tbody></table>
	</div>
<input type="submit" value="Найти">
</form>