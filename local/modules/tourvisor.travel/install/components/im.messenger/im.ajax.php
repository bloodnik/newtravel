<?

if (!defined('IM_AJAX_INIT'))
{
	define("IM_AJAX_INIT", true);
	define("PUBLIC_AJAX_MODE", true);
	define("NO_KEEP_STATISTIC", "Y");
	define("NO_AGENT_STATISTIC","Y");
	define("NO_AGENT_CHECK", true);
	define("DisableEventsCheck", true);
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
}
header('Content-Type: application/x-javascript; charset='.LANG_CHARSET);

// NOTICE
// Before execute next code, execute file /module/im/ajax_hit.php
// for skip onBeforeProlog events

if (!CModule::IncludeModule("im"))
{
	echo CUtil::PhpToJsObject(Array('ERROR' => 'IM_MODULE_NOT_INSTALLED'));
	die();
}
if (check_bitrix_sessid())
{
	if ($_POST['IM_UPDATE_STATE'] == 'Y')
	{
		$bOpenMessenger = isset($_POST['OPEN_MESSENGER']) && intval($_POST['OPEN_MESSENGER']) == 1? true: false;
		$bOpenContactList = isset($_POST['OPEN_CONTACT_LIST']) && intval($_POST['OPEN_CONTACT_LIST']) == 1? true: false;

		// Online
		$arOnline = Array();
		if ($bOpenMessenger || $bOpenContactList)
		{
			$CIMContactList = new CIMContactList();
			$arOnline = $CIMContactList->GetStatus();
			CIMMessenger::SetOpenTabs($_POST['TABS']);
		}

		// Counters
		$arResult["COUNTERS"] = Array();
		if (CModule::IncludeModule("socialnetwork"))
			$arResult["COUNTERS"] = CSocNetLogCounter::GetCodeValuesByUserID($USER->GetID(), $_POST['SITE_ID']);

		// Exchange
		$arResult["MAIL_COUNTER"] = 0;
		if ($_POST['UPDATE_ACTIVITY'] == 'Y')
		{
			if (CModule::IncludeModule("dav"))
			{
				$ar = CDavExchangeMail::GetTicker($GLOBALS["USER"]);
				if ($ar !== null)
					$arResult["MAIL_COUNTER"] = intval($ar["numberOfUnreadMessages"]);
			}
			// SET LAST ACTIVITY
			CUser::SetLastActivityDate($USER->GetId());
		}

		$arSend = Array(
			'USER_ID' => $USER->GetId(),
			'ONLINE' => !empty($arOnline)? $arOnline['users']: array(),
			'COUNTERS' => $arResult["COUNTERS"],
			'MAIL_COUNTER' => $arResult["MAIL_COUNTER"],
			'ERROR' => "NO_NEED_LOAD"
		);
		
		$CIMMessage = new CIMMessage();
		$arMessage = $CIMMessage->GetUnreadMessage();
		if ($arMessage['result'])
		{
			CIMMessage::GetFlashMessage($arMessage['unreadMessage']);
			$readMessageUserId = $_POST['TAB'];
			if ($bOpenMessenger && intval($readMessageUserId) > 0)
			{
				$CIMMessage->SetReadMessage($readMessageUserId);
				CIMMessenger::SetCurrentTab($readMessageUserId);
			}

			$arSend['MESSAGE'] = $arMessage['message'];
			$arSend['UNREAD_MESSAGE'] = $arMessage['unreadMessage'];
			$arSend['USERS_MESSAGE'] = $arMessage['usersMessage'];
			$arSend['USERS'] = $arMessage['users'];
			$arSend['USER_IN_GROUP'] = $arMessage['userInGroup'];
			$arSend['WO_USER_IN_GROUP'] = $arMessage['woUserInGroup'];
			$arSend['ERROR'] = '';
		}

		// Notify
		$CIMNotify = new CIMNotify();
		$arNotify = $CIMNotify->GetUnreadNotify();
		if ($arNotify['result'])
		{
			$arSend['NOTIFY'] = $arNotify['notify'];
			$arSend['UNREAD_NOTIFY'] = $arNotify['unreadNotify'];
			$arSend['FLASH_NOTIFY'] = CIMNotify::GetFlashNotify($arNotify['unreadNotify']);
			$arSend['ERROR'] = '';
		}
		echo CUtil::PhpToJsObject($arSend);
	}
	else if ($_POST['IM_NOTIFY_LOAD'] == 'Y')
	{
		$CIMNotify = new CIMNotify();
		$arNotify = $CIMNotify->GetUnreadNotify(false);
		if ($arNotify['result'])
		{
			$arSend['NOTIFY'] = $arNotify['notify'];
			$arSend['UNREAD_NOTIFY'] = $arNotify['unreadNotify'];
			$arSend['FLASH_NOTIFY'] = CIMNotify::GetFlashNotify($arNotify['unreadNotify']);
			$arSend['ERROR'] = '';

			if ($arNotify['maxNotify'] > 0)
				$CIMNotify->MarkNotifyRead($arNotify['maxNotify'], true);
		}
		echo CUtil::PhpToJsObject($arSend);
	}
	else if ($_POST['IM_SEND_MESSAGE'] == 'Y')
	{
		CUtil::decodeURIComponent($_POST);

		$tmpID = $_POST['ID'];

		$ar = Array(
			"TO_USER_ID" => intval($_POST['RECIPIENT_ID']),
			"MESSAGE" 	 => $_POST['MESSAGE'],
		);

		$errorMessage = "";
		$CIMMessage = new CIMMessage();
		if(!($insertID = $CIMMessage->Add($ar)))
		{
			if ($e = $GLOBALS["APPLICATION"]->GetException())
				$errorMessage = $e->GetString();
			if (StrLen($errorMessage) == 0)
				$errorMessage = GetMessage('IM_UNKNOWN_ERROR');
		}

		$arMessage = $CIMMessage->GetUnreadMessage();
		CIMMessage::GetFlashMessage($arMessage['unreadMessage']);

		$CCTP = new CTextParser();
		$CCTP->allow = array("HTML" => "N", "ANCHOR" => "Y", "BIU" => "N", "IMG" => "N", "QUOTE" => "N", "CODE" => "N", "FONT" => "N", "LIST" => "N", "SMILES" => "N", "NL2BR" => "Y", "VIDEO" => "N", "TABLE" => "N", "CUT_ANCHOR" => "N", "ALIGN" => "N");
		echo CUtil::PhpToJsObject(Array(
			'TMP_ID' => $tmpID,
			'ID' => $insertID,
			'SEND_DATE' => time()+CTimeZone::GetOffset(),
			'SEND_MESSAGE' => $CCTP->convertText(htmlspecialchars($ar['MESSAGE'])),
			'RECIPIENT_ID' => intval($_POST['RECIPIENT_ID']),
			'MESSAGE' => $arMessage['message'],
			'UNREAD_MESSAGE' => $arMessage['unreadMessage'],
			'USERS_MESSAGE' => $arMessage['usersMessage'],
			'ERROR' => $errorMessage
		));

		CIMMessenger::SetCurrentTab(intval($_POST['TAB']));
		CIMMessenger::SetOpenTabs($_POST['TABS']);
	}
	else if ($_POST['IM_UPDATE_TABS'] == 'Y')
	{
		$errorMessage = "";

		CIMMessenger::SetCurrentTab(intval($_POST['TAB']));
		CIMMessenger::SetOpenTabs($_POST['TABS']);

		echo CUtil::PhpToJsObject(Array(
			'ERROR' => $errorMessage
		));
	}
	else if ($_POST['IM_READ_MESSAGE'] == 'Y')
	{
		$errorMessage = "";

		$CIMMessage = new CIMMessage();
		$CIMMessage->SetReadMessage($_POST['USER_ID']);

		echo CUtil::PhpToJsObject(Array(
			'USER_ID' => intval($_POST['USER_ID']),
			'ERROR' => $errorMessage
		));
	}
	else if ($_POST['IM_LOAD_LAST_MESSAGE'] == 'Y')
	{
		$CIMHistory = new CIMHistory();
		$arMessage = $CIMHistory->GetLastMessage(intval($_POST['USER_ID']));
		CIMMessage::GetFlashMessage($arMessage['unreadMessage']);

		echo CUtil::PhpToJsObject(Array(
			'MESSAGE' => $arMessage['message'],
			'UNREAD_MESSAGE' => $arMessage['unreadMessage'],
			'USERS_MESSAGE' => $arMessage['usersMessage'],
			'USER_ID' => intval($_POST['USER_ID']),
			'ERROR' => ''
		));

		$CIMMessage = new CIMMessage();
		$CIMMessage->SetReadMessage(intval($_POST['TAB']));

		CIMMessenger::SetOpenTabs($_POST['TABS']);
		CIMMessenger::SetCurrentTab(intval($_POST['TAB']));
	}
	else if ($_POST['IM_HISTORY_LOAD'] == 'Y')
	{
		$CIMHistory = new CIMHistory();
		$arMessage = $CIMHistory->GetLastMessage(intval($_POST['USER_ID']));

		$arUserData = Array('users' => Array(), 'userInGroup' => Array(), 'woUserInGroup' => Array());
		if ($_POST['USER_LOAD'] == 'Y')
			$arUserData = CIMContactList::GetUserData(Array('ID' => $_POST['USER_ID']));

		echo CUtil::PhpToJsObject(Array(
			'MESSAGE' => $arMessage['message'],
			'USERS_MESSAGE' => $arMessage['usersMessage'],
			'USERS' => $arUserData['users'],
			'USER_IN_GROUP' => $arUserData['userInGroup'],
			'WO_USER_IN_GROUP' => $arUserData['woUserInGroup'],
			'ERROR' => ''
		));
	}
	else if ($_POST['IM_HISTORY_LOAD_MORE'] == 'Y')
	{
		$CIMHistory = new CIMHistory();
		$arMessage = $CIMHistory->GetMoreMessage(intval($_POST['MESSAGE_ID']), intval($_POST['USER_ID']));

		echo CUtil::PhpToJsObject(Array(
			'MESSAGE' => $arMessage['message'],
			'USERS_MESSAGE' => $arMessage['usersMessage'],
			'ERROR' => ''
		));
	}
	else if ($_POST['IM_HISTORY_REMOVE_ALL'] == 'Y')
	{
		$errorMessage = "";

		$CIMHistory = new CIMHistory();
		$CIMHistory->RemoveAllMessage($_POST['USER_ID']);

		echo CUtil::PhpToJsObject(Array(
			'USER_ID' => intval($_POST['USER_ID']),
			'ERROR' => $errorMessage
		));
	}
	else if ($_POST['IM_HISTORY_REMOVE_MESSAGE'] == 'Y')
	{
		$errorMessage = "";

		$CIMHistory = new CIMHistory();
		$CIMHistory->RemoveMessage($_POST['MESSAGE_ID']);

		echo CUtil::PhpToJsObject(Array(
			'MESSAGE_ID' => intval($_POST['MESSAGE_ID']),
			'ERROR' => $errorMessage
		));
	}
	else if ($_POST['IM_HISTORY_SEARCH'] == 'Y')
	{
		CUtil::decodeURIComponent($_POST);

		$CIMHistory = new CIMHistory();
		$arMessage = $CIMHistory->SearchMessage($_POST['SEARCH'], intval($_POST['USER_ID']));

		echo CUtil::PhpToJsObject(Array(
			'MESSAGE' => $arMessage['message'],
			'USERS_MESSAGE' => $arMessage['usersMessage'],
			'USER_ID' => intval($_POST['USER_ID']),
			'ERROR' => ''
		));
	}
	else if ($_POST['IM_CONTACT_LIST'] == 'Y')
	{
		$CIMContactList = new CIMContactList();
		$arContactList = $CIMContactList->GetList();

		echo CUtil::PhpToJsObject(Array(
			'USER_ID' => $USER->GetId(),
			'USERS' => $arContactList['users'],
			'GROUPS' => $arContactList['groups'],
			'USER_IN_GROUP' => $arContactList['userInGroup'],
			'WO_GROUPS' => $arContactList['woGroups'],
			'WO_USER_IN_GROUP' => $arMessage['woUserInGroup'],
			'ERROR' => ''
		));
	}
	else if ($_POST['IM_NOTIFY_VIEWED'] == 'Y')
	{
		$errorMessage = "";

		$CIMNotify = new CIMNotify();
		$CIMNotify->MarkNotifyRead($_POST['MAX_ID'], true);

		echo CUtil::PhpToJsObject(Array(
			'ERROR' => $errorMessage
		));
	}
	else if ($_POST['IM_NOTIFY_HISTORY_LOAD_MORE'] == 'Y')
	{
		$errorMessage = "";

		$CIMNotify = new CIMNotify();
		$arNotify = $CIMNotify->GetNotifyList(Array('PAGE' => $_POST['PAGE']));

		echo CUtil::PhpToJsObject(Array(
			'NOTIFY' => $arNotify,
			'ERROR' => $errorMessage
		));
	}
	else if ($_POST['IM_NOTIFY_CONFIRM'] == 'Y')
	{
		$errorMessage = "";

		$CIMNotify = new CIMNotify();
		$CIMNotify->Confirm($_POST['NOTIFY_ID'], $_POST['NOTIFY_VALUE']);

		echo CUtil::PhpToJsObject(Array(
			'NOTIFY_ID' => intval($_POST['NOTIFY_ID']),
			'NOTIFY_VALUE' => $_POST['NOTIFY_VALUE'],
			'ERROR' => $errorMessage
		));
	}
	else if ($_POST['IM_NOTIFY_REMOVE'] == 'Y')
	{
		$errorMessage = "";

		$CIMNotify = new CIMNotify();
		$CIMNotify->Delete($_POST['NOTIFY_ID']);

		echo CUtil::PhpToJsObject(Array(
			'NOTIFY_ID' => intval($_POST['NOTIFY_ID']),
			'ERROR' => $errorMessage
		));
	}
	else if ($_POST['IM_NOTIFY_GROUP_REMOVE'] == 'Y')
	{
		$errorMessage = "";

		$CIMNotify = new CIMNotify();
		if ($arNotify = $CIMNotify->GetNotify($_POST['NOTIFY_ID']))
			CIMNotify::DeleteByTag($arNotify['NOTIFY_TAG']);

		echo CUtil::PhpToJsObject(Array(
			'NOTIFY_ID' => intval($_POST['NOTIFY_ID']),
			'ERROR' => $errorMessage
		));
	}
	else
	{
		echo CUtil::PhpToJsObject(Array('ERROR' => 'UNKNOWN_ERROR'));
	}
}
else
{
	echo CUtil::PhpToJsObject(Array('ERROR' => 'SESSION_ERROR'));
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>