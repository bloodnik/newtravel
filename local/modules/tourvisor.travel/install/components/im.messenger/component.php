<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (isset($_REQUEST['AJAX_CALL']) && $_REQUEST['AJAX_CALL'] == 'Y')
	return;

if (defined('IM_COMPONENT_INIT'))
	return;
else
	define("IM_COMPONENT_INIT", true);

if (intval($USER->GetID()) <= 0)
	return;

if (!CModule::IncludeModule('im'))
	return;

$arResult = Array();

// Counters
$arResult["COUNTERS"] = Array();
if (CModule::IncludeModule("socialnetwork"))
	$arResult["COUNTERS"] = CSocNetLogCounter::GetCodeValuesByUserID($USER->GetID(), SITE_ID);

// Exchange
$arResult["PATH_TO_USER_MAIL"] = "";
$arResult["MAIL_COUNTER"] = 0;
if (CModule::IncludeModule("dav"))
{
	$ar = CDavExchangeMail::GetTicker($GLOBALS["USER"]);
	if ($ar !== null)
	{
		$arResult["PATH_TO_USER_MAIL"] = $ar["exchangeMailboxPath"];
		$arResult["MAIL_COUNTER"] = intval($ar["numberOfUnreadMessages"]);
	}
}

// Message & Notify

$arSettings = CIMMessenger::GetSettings();

$CIMNotify = new CIMNotify();
$arResult['NOTIFY'] = $CIMNotify->GetUnreadNotify();
$arResult['NOTIFY']['flashNotify'] = CIMNotify::GetFlashNotify($arResult['NOTIFY']['unreadNotify']);
$arResult['NOTIFY']['countNotify'] = $CIMNotify->GetNotifyCounter($arResult['NOTIFY']);
$arResult["NOTIFY_COUNTER"] = $arResult['NOTIFY']['countNotify']; // legacy

$CIMContactList = new CIMContactList();
$arResult['CONTACT_LIST'] = $CIMContactList->GetList();

// Модификация
global $USER;
if(!CSite::InGroup(array(8))) {

	// Создаем группу пользователей
	$arResult['CONTACT_LIST']["groups"]["consult"] = array(
		"id" => "consult",
		"status" => "open",
		"name" => "Консультанты"
	);

	$arResult['CONTACT_LIST']["woGroups"]["consult"] = array(
		"id" => "consult",
		"status" => "open",
		"name" => "Консультанты"
	);

	// Список пользователей-консультантов
	$listUser = array();
	$filter = Array (
	    "ACTIVE"              => "Y",
	    "GROUPS_ID"           => Array(8)
	);
	$rsUsers = CUser::GetList(($by="name"), ($order="desc"), $filter);
	//$is_filtered = $rsUsers->is_filtered; // отфильтрована ли выборка ?
	//$rsUsers->NavStart(50); // разбиваем постранично по 50 записей
	//while($rsUsers->NavNext(true, "f_")) :
	while($user = $rsUsers->GetNext()) :

		// Сохраняем пользователей в контакт-листе

		$status = CSocNetUser::IsOnLine($user["ID"]) == true ? "online" : "offline";
		$arResult['CONTACT_LIST']["users"][$user["ID"]] = array(
			"id" => $user["ID"],
			"name" => $user["NAME"]." ".$user["LAST_NAME"],
			"avatar" => "/bitrix/js/im/images/blank.gif",
			"status" => $status,
			"profile" => "/people/user/".$user["ID"]."/"
		);

		$listUser[] = $user["ID"];

	endwhile;

	// Добавляем пользователей в группу
	$arResult['CONTACT_LIST']["userInGroup"]["consult"] = array(
		"id" => "consult",
		"users" => $listUser
	);

	$arResult['CONTACT_LIST']["woUserInGroup"]["consult"] = array(
		"id" => "consult",
		"users" => $listUser
	);

}
// -- Модификация

$CIMMessage = new CIMMessage();
$arResult['MESSAGE'] = $CIMMessage->GetUnreadMessage();
$arResult['MESSAGE']['flashMessage'] = CIMMessage::GetFlashMessage($arResult['MESSAGE']['unreadMessage']);
$arResult['MESSAGE']['countMessage'] = $CIMMessage->GetMessageCounter($arResult['MESSAGE']);
$arResult["MESSAGE_COUNTER"] = $arResult['MESSAGE']['countMessage']; // legacy

// Merge message users with contact list
if (isset($arResult['MESSAGE']['users']) && !empty($arResult['MESSAGE']['users']))
{
	foreach ($arResult['MESSAGE']['users'] as $arUser)
		$arResult['CONTACT_LIST']['users'][$arUser['id']] = $arUser;

	if (isset($arResult['MESSAGE']['userInGroup']))
	{
		foreach ($arResult['MESSAGE']['userInGroup'] as $arUserInGroup)
		{
			if (isset($arResult['CONTACT_LIST']['userInGroup'][$arUserInGroup['id']]['users']))
				$arResult['CONTACT_LIST']['userInGroup'][$arUserInGroup['id']]['users'] = array_merge($arResult['CONTACT_LIST']['userInGroup'][$arUserInGroup['id']]['users'], $arUserInGroup['users']);
			else
			{
				if (isset($arResult['CONTACT_LIST']['userInGroup']['other']['users']))
					$arResult['CONTACT_LIST']['userInGroup']['other']['users'] = array_merge($arResult['CONTACT_LIST']['userInGroup']['other']['users'], $arUserInGroup['users']);
				else
				{
					$arUserInGroup['id'] = 'other';
					$arResult['CONTACT_LIST']['userInGroup']['other'] = $arUserInGroup;
				}
			}
		}
	}
	if (isset($arResult['MESSAGE']['woUserInGroup']))
	{
		foreach ($arResult['MESSAGE']['woUserInGroup'] as $arWoUserInGroup)
		{
			if (isset($arResult['CONTACT_LIST']['woUserInGroup'][$arWoUserInGroup['id']]['users']))
				$arResult['CONTACT_LIST']['woUserInGroup'][$arWoUserInGroup['id']]['users'] = array_merge($arResult['CONTACT_LIST']['woUserInGroup'][$arWoUserInGroup['id']]['users'], $arWoUserInGroup['users']);
			else
			{
				if (isset($arResult['CONTACT_LIST']['woUserInGroup']['other']['users']))
					$arResult['CONTACT_LIST']['woUserInGroup']['other']['users'] = array_merge($arResult['CONTACT_LIST']['woUserInGroup']['other']['users'], $arWoUserInGroup['users']);
				else
				{
					$arWoUserInGroup['id'] = 'other';
					$arResult['CONTACT_LIST']['woUserInGroup']['other'] = $arWoUserInGroup;
				}
			}
		}
	}
}

$arResult['OPEN_TAB'] = CIMMessenger::GetOpenTabs();
$arResult['CURRENT_TAB'] = CIMMessenger::GetCurrentTab();
$arResult['STATUS'] = isset($arSettings['status'])? $arSettings['status']: 'online';
$arResult['VIEW_OFFLINE'] = isset($arSettings['viewOffline']) && $arSettings['viewOffline'] == 'N'? 'false': 'true';
$arResult['VIEW_GROUP'] = isset($arSettings['viewGroup']) && $arSettings['viewGroup'] == 'N'? 'false': 'true';
$arResult['ENABLE_SOUND'] = isset($arSettings['enableSound']) && $arSettings['enableSound'] == 'N'? 'false': 'true';
$arResult['PANEL_POSTION_HORIZONTAL'] = isset($arSettings['panelPositionHorizontal']) && in_array($arSettings['panelPositionHorizontal'], Array('left', 'center', 'right'))? $arSettings['panelPositionHorizontal']: 'right';
$arResult['PANEL_POSTION_VERTICAL'] = isset($arSettings['panelPositionVertical']) && in_array($arSettings['panelPositionVertical'], Array('top', 'bottom'))? $arSettings['panelPositionVertical']: 'bottom';

$arResult['PATH_TO_USER_PROFILE_TEMPLATE'] = COption::GetOptionString('im', 'path_to_user_profile');
$arResult['PATH_TO_USER_PROFILE'] = CComponentEngine::MakePathFromTemplate($arResult['PATH_TO_USER_PROFILE_TEMPLATE'], array('user_id' => $USER->GetId()));

CJSCore::Init(array('popup', 'ajax', 'fx', 'ls', 'date'));

$APPLICATION->SetAdditionalCSS("/bitrix/js/im/scroll/bx-scroller-style.css");
$APPLICATION->SetAdditionalCSS("/bitrix/js/im/css/messenger.css");

$APPLICATION->AddHeadScript("/bitrix/js/im/scroll/scroller.js");
$APPLICATION->AddHeadScript("/bitrix/js/im/im.js");

$arMess = IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/im/js_im.php", false, true);
$APPLICATION->AddHeadString('<script type="text/javascript">BX.message('.CUtil::PhpToJSObject($arMess, false).')</script>', true);

if (!(isset($arParams['TEMPLATE_HIDE']) && $arParams['TEMPLATE_HIDE'] == 'Y'))
	$this->IncludeComponentTemplate();

return $arResult;

?>