<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$arParams["SALE"]=intval($arParams["SALE"]);?>
<script type="text/javascript">
$(function() {
      $(".tour-list-in").each(function(){
		var height=$(this).outerHeight();
		$(this).children(".palm").css({"min-height":height});
      });
	  $(".oform.no-user").click(function(){authFormWindow.ShowLoginForm()});
});
</script>
<?if($arResult["BUY"] == true):?><font color="#BB3333">Куплен тур: <?=$_REQUEST["tourBuy"]?></font><br><?endif;?>
	<?foreach($arResult["rows"] as $row):?>
		<div class="tour-list">
            <div class="tour-list-in">
              <div class="tour-item item-right">
                <div class="t-name" style="width:305px;"><a class="tname" href="/hotels/<?=$row["HotelId"]?>.html"><?=$row["HotelName"]?></a>
                  <div class="rating">
                    <ul>
					<?if($row["StarName"]){echo str_repeat("<li class='rate'></li>", intval($row["StarName"]));}?>
                      <li class="rating-s">Рейтинг <b><?=$row["HotelRating"]?></b></li>
                    </ul>
                  </div>
                  <div class="where"><?=$row["ResortName"]?></div>
                </div>
                <div class="t-i-l">
                  <div class="img-n"><a href="/hotels/<?=$row["HotelId"]?>.html"><img src="<?=$row["HotelTitleImageUrl"]?>" alt="<?=$row["TourName"]?>" /></a></div>
                  <div class="n-name-txt">
                    <div class="n-txt">
                      <div><b>Вылет:</b> <?=$row["CheckInDate"]?> (<?=getMessage("CALENDAR_DATE_".strtoupper(date("D",MakeTimeStamp($row["CheckInDate"],"DD.MM.YYYY"))))?>.) ночей: <?=$row["Nights"]?></div>
                      <div><b>Номер:</b> <?=$row["RoomName"]?></div>
                      <div><b><?=$row["HtPlaceName"]?></b> (<?=$row["HtPlaceDescription"]?>)</div>
                      <div><b><?=$row["MealName"]?></b> (<?=$row["MealDescription"]?>)</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="palm">
               <?if($arParams["SALE"]>0):?><div class="old-price"><s><?=intval($row["Price"]/100*$arParams["SALE"])+$row["Price"]?> <?=($row["Currency"]=="RUB"?"руб.":$row["Currency"])?></s></div><?endif?>
                <div class="new-price"><?=$row["Price"]?> <?=($row["Currency"]=="RUB"?"руб.":$row["Currency"])?></div>
                <!--<div class="doplata">доплаты:</div>
                <div class="vissbor">визовый сбор - нет</div>-->
				<?if($USER->IsAuthorized()):?>
				<?if(in_array($row["OfferId"], $arResult["BASKET"])):?>
					<span class="in-basket">В корзине</span>
				<?else:?>
                <a href="javascript:void(0);" class="oform" onClick="HotelTourBuy(<?=$arParams["ID"]?>,<?=$arParams["COUNT"]?>,<?=$row["OfferId"]?>, <?=$arParams["SALE"]?>);">Оформить</a>
				<?endif?>
				<?else:?>
				<a class="oform no-user" href="javascript:void(0);">Оформить</a>
				<?endif;?>
				</div>
            </div>
          </div>
<?endforeach;?>

