<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $days = array("Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"); ?>
<?if($arResult["count"]>0):
$arParams["SALE"]=intval($arParams["SALE"]);
?>
<?if($arResult["BUY"] == true):?><font color="#BB3333">Куплен тур: <?=$_REQUEST["tourBuy"]?></font><br><?endif;?>
<div class="slide-search">
			  <div class="tour-list">
            <div class="tour-list-in">
			<?foreach($arResult["rows"] as $key=>$row):?>
				<div class="t-i-w">
                          <div class="tour-item">
                            <div class="t-name"><a class="tname" href="/hotels/<?=$row["HotelId"]?>.html"><?=$row["HotelName"]?></a></div>
                            <div class="t-i-l">
                              <div class="img-n"><a href="/hotels/<?=$row["HotelId"]?>.html"><img src="<?=$row["HotelTitleImageUrl"]?>" alt="<?=$row["TourName"]?>" /></a></div>
                              <div class="n-name-txt">
                                <div class="n-txt">
									<div><b>Вылет:</b> <?=$row["CheckInDate"]?> (<?=getMessage("CALENDAR_DATE_".strtoupper(date("D",MakeTimeStamp($row["CheckInDate"],"DD.MM.YYYY"))))?>.) ночей: <?=$row["Nights"]?></div>
									<div><b>Номер:</b> <?=$row["RoomName"]?></div>
									<?if($row["HtPlaceDescription"]):?><div><b><?=$row["HtPlaceName"]?></b> (<?=$row["HtPlaceDescription"]?>)</div><?endif?>
									<?if($row["MealDescription"]):?><div><b><?=$row["MealName"]?></b> (<?=$row["MealDescription"]?>)</div><?endif?>
                                </div>
                              </div>
                            </div>
                          </div>
                           <div class="where"><?=$row["ResortName"]?></div>
                          <div class="rating">
                            <div class="rates">
                                <?if(intVal($row["StarName"])){echo str_repeat("<div class=\"rate\"></div>", intval($row["StarName"]));}?>
                              </div>
                              <div class="rating-s">Рейтинг <b><?=$row["HotelRating"]?></b></div>
                          </div>
                          <div class="palm">
                <?if($arParams["SALE"]>0):?><div class="old-price"><s><?=intval($row["Price"]/100*$arParams["SALE"])+$row["Price"]?> <?=($row["Currency"]=="RUB"?"руб.":$row["Currency"])?></s></div><?endif?>
                <div class="new-price"><?=$row["Price"]?> <?=($row["Currency"]=="RUB"?"руб.":$row["Currency"])?></div>
                <!--<div class="doplata">доплаты:</div>
                <div class="vissbor">визовый сбор - нет</div>-->
				<?if($USER->IsAuthorized()):?>
				<?if(in_array($row["OfferId"], $arResult["BASKET"])):?>
					<span class="in-basket">В корзине</span>
				<?else:?>
                <a href="javascript:void(0);" class="oform" onClick="SearchTourDataBuy (<?=$arParams["ID"]?>, <?=$arParams["PAGE"]?>, <?=$arParams["COUNT"]?>, <?=$row["OfferId"]?>, <?=$arParams["SALE"]?>);">Оформить</a>
				<?endif?>
				<?else:?>
				<a class="oform no-user" onClick="authFormWindow.ShowLoginForm();" href="javascript:void(0);">Оформить</a>
				<?endif;?>
				</div>
               </div>
			<?
			if(($key+1)%4==0 && ($key+1)!=$arCount)echo"</div></div><div class=\"tour-list\"><div class=\"tour-list-in\">";
			endforeach?>
            </div>
          </div>
</div>


<? /* постраничная навигация */ ?>
<?/*if($arResult["pageTotal"] > 0):?>
<div class="pagination">
<ul>
	<?if($arResult["pageNumber"] > 1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]-1)?>, <?=$arResult["pageCount"]?>);">Предыдущая</a></li><?endif;?>
	<?for($i=1;$i<=($arResult["pageTotal"]+1);$i++):?>
		<?if($arResult["pageNumber"] == $i):?>
			<li><span><?=$i?></span></li>
		<?else:?>
			<li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=$i?>, <?=$arResult["pageCount"]?>);"><?=$i?></a></li>
		<?endif;?>
	<?endfor;?>
	<?if($arResult["pageNumber"] < $arResult["pageTotal"]+1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]+1)?>, <?=$arResult["pageCount"]?>);">Следующая</a></li><?endif;?>
</ul>
</div>
<?endif;*/?>
<? /* постраничная навигация */ ?>
<?if($arResult["pageTotal"] > 0):
?>
<div class="pagination">
<ul>
<?if($arResult["pageTotal"]<10):?>
	<?if($arResult["pageNumber"] > 1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]-1)?>, <?=$arResult["pageCount"]?>);">Предыдущая</a></li><?endif;?>
	<?for($i=1;$i<=($arResult["pageTotal"]+1);$i++):?>
		<?if($arResult["pageNumber"] == $i):?>
			<li><span><?=$i?></span></li>
		<?else:?>
			<li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=$i?>, <?=$arResult["pageCount"]?>);"><?=$i?></a></li>
		<?endif;?>
	<?endfor;?>
	<?if($arResult["pageNumber"] < $arResult["pageTotal"]+1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]+1)?>, <?=$arResult["pageCount"]?>);">Следующая</a></li><?endif;?>
<?else:?>
	<?$varFirst=((($arResult["pageNumber"]-2)<1)?1:$arResult["pageNumber"]-2);$varLast=(($arResult["pageNumber"]+2)>($arResult["pageTotal"]+1)?$arResult["pageTotal"]+1:$arResult["pageNumber"]+2);?>
	<?if($varFirst>1):?>
	<li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, 1, <?=$arResult["pageCount"]?>);">Начало</a></li>
	<?endif?>
	<?if($arResult["pageNumber"] > 1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]-1)?>, <?=$arResult["pageCount"]?>);"><</a></li><?endif;?>
	<?for($i=$varFirst;$i<=($varLast);$i++):?>
		<?if($arResult["pageNumber"] == $i):?>
			<li><span><?=$i?></span></li>
		<?else:?>
			<li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=$i?>, <?=$arResult["pageCount"]?>);"><?=$i?></a></li>
		<?endif;?>
	<?endfor;?>
	<?if($arResult["pageNumber"] < $arResult["pageTotal"]+1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]+1)?>, <?=$arResult["pageCount"]?>);">></a></li><?endif;?>
	<?if($varLast<$arResult["pageTotal"]+1):?>
	<li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=$arResult["pageTotal"]+1?>, <?=$arResult["pageCount"]?>);">Конец</a></li>
	<?endif;?>
<?endif?>
</ul>
</div>
<?endif;?>
<?endif?>