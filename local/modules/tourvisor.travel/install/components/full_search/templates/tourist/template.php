<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["SALE"]=intval($arParams["SALE_PERCENT"]);?>
<script type="text/javascript">var sale=<?=$arParams["SALE"]?></script>

<div class="fast-search-tour">
<h1>БЫСТРЫЙ ПОИСК ТУРА</h1>
<div class="tour-form">
 <div class="tour-form-in">
<form class="jClever" action="<?=$arParams["ACTION"]?>" name="searchTour" id="searchTour" method="GET" <?if($arParams["NEW"] == "Y"):?>target="new"<?endif;?>>
<input type="hidden" name="type" value="search">
<div class="form-col first">
<div class="select-box">
			<select name="cityFromId" id="travelCityFrom">
				<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>
				<?foreach($arResult["from"] as $city):?>
					<option value="<?=$city["Id"]?>" <?if($arParams["CITY"] == $city["Id"]):?>SELECTED<?endif;?>><?=$city["Name"]?></option>
				<?endforeach?>
			</select>
	</div>
	<div class="curort">
			<div class="cur">Курорт</div>
			<div id="resorts">
			<div id='mycustomscroll' class='flexcroll'>
			<ul>
			<li><input type="checkbox" name="resortsAny" id="resortsAny" checked=""><label>Любой</label></li>
			</ul>
				<ul id="resortsList">
					<?foreach($arResult["resorts"] as $resorts):?>
						<li><input type="checkbox" name="resortsList[]" value="<?=$resorts["Id"]?>"><label><?=$resorts["Name"]?></label></li>
					<?endforeach?>
				</ul>
			</div>
			</div>
</div>
</div>
<div class="form-col">
<div class="select-box">
			<select name="countryId" id="travelCountryTo">
				<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>
				<?foreach($arResult["to"] as $country):?>
					<option value="<?=$country["Id"]?>" <?if($arParams["COUNTRY"] == $country["Id"]):?>SELECTED<?endif;?>><?=$country["Name"]?></option>
				<?endforeach?>
			</select>
			</div>
	<div class="curort">
	<div class="cur">Отель</div>
			<div id="hotels">
			<div id='mycustomscroll' class='flexcroll'>
			<ul>
				<li><input type="checkbox" name="hotelsAny" id="hotelsAny" checked=""><label>Любой</label></li>
			</ul>
				<ul id="hotelsList">
					<?foreach($arResult["hotels"] as $hotels):?>
						<li><input type="checkbox" name="hotelsList[]" value="<?=$hotels["Id"]?>"><label><?=$hotels["Name"]?></label></li>
					<?endforeach?>
				</ul>
			</div>
			</div>
			</div>
</div>
<div class="form-col froms">
 <div class="date-pick">
     <input id="air_from" value="<?=$arParams["DAY_FROM"]?>" placeholder="Вылет с" type="text" name="departFrom" class="from" />
 </div>
				<label class="cat-label select"><b>Ночей с</b></label>
				<label class="cat-label select right"><b>Ночей до</b></label>
				<div class="select-box">
				<select class="nightfrom" name="nightsMin" name="night-from">
					<?for($i=1; $i<=30; $i++){?>
						<option value="<?=$i?>" <?if($arParams["NIGHT_FROM"] == $i):?>selected<?endif;?>><?=$i?></option>
					<?}?>
				</select>
				<select class="nightfrom" name="nightsMax" name="night-to">
					<?for($i=1; $i<=30; $i++){?>
						<option value="<?=$i?>" <?if($arParams["NIGHT_TO"] == $i):?>selected<?endif;?>><?=$i?></option>
					<?}?>
				</select>
				</div>
				<div class="fl">
				 <label class="cat-label"><b>Категория:</b></label>
				  <div class="c-row"><input type="checkbox" name="starsAny" id="starsAny" CHECKED><span>Любое</span></div>
						  <div id="starsList">
							<?foreach($arResult["stars"] as $stars):?>
								 <div class="c-row"><input type="checkbox" name="starsList[]" value="<?=$stars["Id"]?>"><span><?=$stars["Name"]?></span></div>
							<?endforeach?>
						</div>
				</div>
				<div class="fr">
				
				 <label class="cat-label"><b>Питание:</b></label>
				
                          <div class="c-row"><input type="checkbox" name="mealsAny" id="mealsAny" CHECKED><span>Любое</span></div>
						  <div id="mealsList">
							<?foreach($arResult["meals"] as $meals):?>
								 <div class="c-row"><input type="checkbox" name="mealsList[]" value="<?=$meals["Id"]?>"><span><?=$meals["Name"]?></span></div>
							<?endforeach?>
						</div>
				
				</div>
</div>
<div class="form-col last-col">
<div class="row">
      <div class="date-pick">
           <input value="<?=$arParams["DAY_TO"]?>"  placeholder="Вылет по" id="air_to" type="text" name="departTo" class="from" />
      </div>
	</div>
	 <div class="select-box">
			<label class="child-age">Кол-во взрослых и детей</label>
					<select style="width:58px" name="adults" class="sCombo">
						<option value="1">1</option>
						<option value="2" selected>2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
			
					<select style="width:58px" name="kids" class="sCombo">
						<option value="0" selected>0</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
					</select>
					
     </div>
	 <label class="child-age">Возраст детей</label>
			<div class="select-box selectChild" id="i-kidages-caption" style="padding-top:10px;padding-bottom:20px">
				<select name="kid1">
					<?for($i=0; $i<=15; $i++){?>
						<option value="<?=$i?>"><?=$i?></option>
					<?}?>
				</select>
				<select name="kid2">
					<?for($i=0; $i<=15; $i++){?>
						<option value="<?=$i?>"><?=$i?></option>
					<?}?>
				</select>
				<select name="kid3">
					<?for($i=0; $i<=15; $i++){?>
						<option value="<?=$i?>"><?=$i?></option>
					<?}?>
				</select>
	 
</div>
<div style="clear:both;"></div>
         <div class="price-pick">
              <label class="all-price">Цена от :</label>
              <input name="priceMin" type="text" />
               <label>До</label>
              <input name="priceMax" type="text" />
               
	</div>

</div>
	<div style="clear:both;"></div>
	<div class="form-bottom">
	<div class="form-bot-in">
	<label><b>Валюта:</b></label>
	<div id="i-currency">
				<input type="radio" checked="checked" value="RUB" name="currencyAlias"><label><span>RUB</span></label>
				<input type="radio" value="USD" name="currencyAlias"><label><span>USD</span></label>
				<input type="radio" value="EUR" name="currencyAlias"><label><span>EUR</span></label>
			</div>
	</div>
	<a href="#request" class="search-tour" onClick="SendFormSearchTour(1,<?=$arParams["COUNT"]?>,<?=$arParams["SALE"]?>);"></a>
  </div>
</form>
</div>
</div>
</div>
<div style="clear:both;"></div>
<div id="request" style="display: none;"></div>
 <div style="clear:both;"></div>
            <div id="searchTourStatus" class="form-result" style="display: none;">
              <div class="form-res-in">
			  <div id="angLoading" style="text-align:center"><img align="center"  src="http://ui.sletat.ru/gfx/ld2.gif"></div>
			  <div id="angContainer">
                <div class="error-txt"> Наше агентство ищет для Вас лучшие предложения.<br>Пожалуйста, ожидайте...</div>
				</div>
               </div>
            </div>
