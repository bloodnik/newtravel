if(window.jQuery==undefined) {
	alert('Необходимо подключить библиотеку jQuery');
}
$(function(){
	$("#air_from").datepicker({
		dateFormat:"dd.mm.yy",
		monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
		dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
		dayNamesShort: ['Вск', 'Пнд', 'Втр', 'Срд', 'Чтв', 'Птн', 'Сбт'], 
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'], 
		minDate: "+1d",
		maxDate: "+1y",
		changeYear: true,
		onSelect: function(){$(this).prev("label").text('');}
	    });
	$("#air_to").datepicker({
		dateFormat:"dd.mm.yy",
		monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
		dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
		dayNamesShort: ['Вск', 'Пнд', 'Втр', 'Срд', 'Чтв', 'Птн', 'Сбт'], 
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'], 
		minDate: "+3d",
		maxDate: "+1y",
		changeYear: true,
		onSelect: function(){$(this).prev("label").text('');}
	    });
	// Изменение города отправления
	$('#travelCityFrom').change(function() {
		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "country", from: $('#travelCityFrom').val()}, function(json){
			$('#travelCountryTo').empty();
			$('#travelCountryTo option').remove();
			$('#travelCountryTo').append( $('<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>') );
			$.each(json.results,function(i,country){
				$('#travelCountryTo').append( $('<option value="'+country.Id+'">'+country.Name+'</option>') );
			});
			$(".select-box select").selectBox("destroy");
			$(".select-box select").selectBox();
		});

		// Сбрасываем все остальные настройки
		$('#resortsList').empty();
		$('#resortsList option').remove();
		$("#resortsAny").attr("checked","checked");

		$('#starsList').empty();
		$('#starsList option').remove();
		$("#starsAny").attr("checked","checked");

		$('#hotelsList').empty();
		$('#hotelsList option').remove();
		$("#hotelsAny").attr("checked","checked");
	});

	// Изменение страны назначения
	$('#travelCountryTo').change(function() {
		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "resort", country: $('#travelCountryTo').val()}, function(json){
			$('#resortsList').empty();
			$('#resortsList option').remove();
			$.each(json.results,function(i,resort){
				$('#resortsList').append( $('<li><input type="checkbox" name="resortsList[]" value="'+resort.Id+'"><label>'+resort.Name+'</label></li>') );
			});

			// Устанавливаем галочки по умолчанию
			$("#resortsAny").attr("checked","checked");
			$("#hotelsAny").attr("checked","checked");

            // Изменяем доступные категории отелей
            GetHotelStars();

			// Снятие/установка галочки "Любой курорт"
			$('#resortsList input:checkbox').change(function() {
				$("#resortsAny").attr("checked","checked");
				$("#resortsList input:checkbox:checked").each(function() {
					$("#resortsAny").removeAttr("checked");
				});
				// Изменение категорий отелей при смене списка курортов
				GetHotelStars();
			});
					
		});
	});
	//
	// Зависимые изменения
	//

	// Получаем список всех доступных категорий отелей
	function GetHotelStars() {
		// Составляем список курортов
		var resorts = "";
		if($("#resortsAny").attr("checked") !== "checked") {
			$("#resortsList input:checkbox:checked").each(function(i,resort) {
				resorts = resorts+resort.value+",";
			});
		}

		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "stars", country: $('#travelCountryTo').val(), resorts: resorts}, function(json){
			$('#starsList').empty();
			$('#starsList option').remove();
			$("#starsAny").attr("checked","checked");
			$.each(json.results,function(i,star){
				$('#starsList').append( $('<div class="c-row"><input type="checkbox" name="starsList[]" value="'+star.Id+'"><span>'+star.Name+'</span></div>') );
			});

			// Снятие/установка галочки "Любая категория отеля"
			$('#starsList input:checkbox').change(function() {
				$("#starsAny").attr("checked","checked");
				$("#starsList input:checkbox:checked").each(function() {
					$("#starsAny").removeAttr("checked");
				});
				GetHotelList();
			});


			// Изменение списка отелей
			GetHotelList();
		});
	}


	// Получаем список всех доступных отелей
	function GetHotelList() {
	
		// Составляем список курортов
		var resorts = "";
		if($("#resortsAny").attr("checked") !== "checked") {
			$("#resortsList input:checkbox:checked").each(function(i,resort) {
				resorts = resorts+resort.value+",";
			});
		}

		// Составляем список категорий
		var stars = "";
		if($("#starsAny").attr("checked") !== "checked") {
			$("#starsList input:checkbox:checked").each(function(i,star) {
				stars = stars+star.value+",";
			});
		}

		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "hotels", country: $('#travelCountryTo').val(), resorts: resorts, stars: stars}, function(json){
			$('#hotelsList').empty();
			$('#hotelsList option').remove();
			$("#hotelsAny").attr("checked","checked");
			$.each(json.results,function(i,hotel){
				$('#hotelsList').append( $('<li><input type="checkbox" name="hotelsList[]" value="'+hotel.Id+'"><label>'+hotel.Name+'</label></li>') );
			});

			// Снятие/установка галочки "Любой отель"
			$('#hotelsList input:checkbox').change(function() {
				$("#hotelsAny").attr("checked","checked");
				$("#hotelsList input:checkbox:checked").each(function() {
					$("#hotelsAny").removeAttr("checked");
				});
			});
			$('.jClever').jCleverAPI("destroy");
			$('.jClever').jClever({
				applyTo:{
				checkbox: true,
				radio: true,
				button: false,
				file: false,
				input: false,
				textarea: false
					}
					}              
				);
			
		});
	}



	//
	// Обработчики галочек
	//
	// Галочка "Все курорты"
	$("#resortsAny").live("change", function(){
      GetHotelStars();
    });
	// Снятие/установка галочки "Любой курорт"
	$("#resortsList input:checkbox").live("change", function(){
      $("#resortsAny").attr("checked","checked");
		$("#resortsList input:checkbox:checked").each(function() {
			$("#resortsAny").removeAttr("checked");
		});
		// Изменение категорий отелей при смене списка курортов
		GetHotelStars();
    });
	


	
	/*$('#resortsList input:checkbox').change(function() {
		$("#resortsAny").attr("checked","checked");
		$("#resortsList input:checkbox:checked").each(function() {
			$("#resortsAny").removeAttr("checked");
		});
		// Изменение категорий отелей при смене списка курортов
		GetHotelStars();
	});*/



	// Снятие/установка галочки "Любая категория отеля"
	$("#starsList input:checkbox").live("change", function(){
      $("#starsAny").attr("checked","checked");
		$("#starsList input:checkbox:checked").each(function() {
			$("#starsAny").removeAttr("checked");
		});
		GetHotelList();
    });
	/*$('#starsList input:checkbox').change(function() {
		$("#starsAny").attr("checked","checked");
		$("#starsList input:checkbox:checked").each(function() {
			$("#starsAny").removeAttr("checked");
		});
		GetHotelList();
	});*/

	// Галочка "Все категории отелей"
	$("#starsAny").live("change", function(){
      GetHotelStars();
    });
	// Снятие/установка галочки "Любой отель"
	$("#hotelsList input:checkbox").live("change", function(){
      $("#hotelsAny").attr("checked","checked");
		$("#hotelsList input:checkbox:checked").each(function() {
			$("#hotelsAny").removeAttr("checked");
		});
		$('.jClever').jCleverAPI("destroy");
			$('.jClever').jClever({
				applyTo:{
				checkbox: true,
				radio: true,
				button: false,
				file: false,
				input: false,
				textarea: false
					}
					}              
				);
    });

	// Снятие/установка галочки "Любая категория питания"
	$("#mealsList input:checkbox").live("change", function(){
      $("#mealsAny").attr("checked","checked");
		$("#mealsList input:checkbox:checked").each(function() {
			$("#mealsAny").removeAttr("checked");
		});
		$('.jClever').jCleverAPI("destroy");
			$('.jClever').jClever({
				applyTo:{
				checkbox: true,
				radio: true,
				button: false,
				file: false,
				input: false,
				textarea: false
					}
					}              
				);
    });
});



//
// Отправка формы поиска туров
//
function SendFormSearchTour(page, count, num) {
	// Показываем что ищем туры
	$("#searchTourStatus").css('display', 'block');
	$("#request").css('display', 'none');

	$.post("/bitrix/tools/simai.travel_ajax.php", $("#searchTour").serialize(), function(RequestResult) {
		$("#request").html(RequestResult+'<br>');
							
		CheckFormSearchTourStatus(RequestResult, 5, page, count, num);
	});
}



//
// Ожидание статуса обработки
//
function CheckFormSearchTourStatus (RequestResult, time, page, count, num) {
	// Делаем запрос статуса
	$.get("/bitrix/tools/simai.travel_ajax.php", { id: RequestResult, type: "searchStatus" }, function(status) {
		//$("#request").append(status+'<br>');
		if(status != "load") {
			setTimeout(function() {
				//$('#request').append('Проверка статуса, прошло '+time+' сек<br>');
				time += 5;
				CheckFormSearchTourStatus(RequestResult, time, page, count, num);
			}, 5000);
		}
		else {
			//$('#request').append('Данные обработаны за '+time+' сек<br>');
			SearchTourData(RequestResult, page, count, num);
		}
	});
}



// Отображение результата данных
function SearchTourData (RequestResult, page, count, num) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: page, count: count, sale: num}, function(data) {
		$("#searchTourStatus").css('display', 'none');
		$("#request").css('display', 'block');
		$('#request').html(data);
	});
}

// Покупка тура
function SearchTourDataBuy (RequestResult, page, count, tour, num) {
if(num=="undefind")num=0;
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: page, count: count, tourBuy: tour, sale: num }, function(data) {
		$('#request').html(data);

		// Плюсуем кол-во элементов в корзине
		var count = ($('#sidebar_product_count').html()*1)+1;
		$('#sidebar_product_count').html(count);

		// Показываем человеку что тур куплен
		var r=confirm("Перейти в корзину для оформления тура?")
		if (r==true)
		{
			window.location = '/personal/cart/';
		}
		else
		{

		}
		$.get("/personal/cart/system.php", {ajax: "Y"}, function(data){$('#cartLine').html(data);});
	});
}



//
// Если переданы параметры то сразу ищем туры
//
function parseGetParams() {
   var $_GET = {};
   var __GET = window.location.search.substring(1).split("&");
   for(var i=0; i<__GET.length; i++) {
      var getVar = __GET[i].split("=");
      $_GET[getVar[0]] = typeof(getVar[1])=="undefined" ? "" : getVar[1];
   }
   return $_GET;
}

$(document).ready(function() {
	var curGet = parseGetParams();
	if(curGet.start == "Y") {
		SendFormSearchTour(1, curGet.count, sale);
	}
});
