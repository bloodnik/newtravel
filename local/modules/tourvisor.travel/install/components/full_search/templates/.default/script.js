if(window.jQuery==undefined) {
	alert('Необходимо подключить библиотеку jQuery');
}
$(document).ready(function() {

	// Изменение города отправления
	$('#travelCityFrom').change(function() {
		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "country", from: $('#travelCityFrom').val()}, function(json){
			$('#travelCountryTo').empty();
			$('#travelCountryTo option').remove();
			$('#travelCountryTo').append( $('<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>') );
			$.each(json.results,function(i,country){
				$('#travelCountryTo').append( $('<option value="'+country.Id+'">'+country.Name+'</option>') );
			});
		});

		// Сбрасываем все остальные настройки
		$('#resortsList').empty();
		$('#resortsList option').remove();
		$("#resortsAny").attr("checked","checked");

		$('#starsList').empty();
		$('#starsList option').remove();
		$("#starsAny").attr("checked","checked");

		$('#hotelsList').empty();
		$('#hotelsList option').remove();
		$("#hotelsAny").attr("checked","checked");
	});

	// Изменение страны назначения
	$('#travelCountryTo').change(function() {
		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "resort", country: $('#travelCountryTo').val()}, function(json){
			$('#resortsList').empty();
			$('#resortsList option').remove();
			$.each(json.results,function(i,resort){
				$('#resortsList').append( $('<div class="chk"><input type="checkbox" name="resortsList[]" value="'+resort.Id+'"><label>'+resort.Name+'</label></div>') );
			});

			// Устанавливаем галочки по умолчанию
			$("#resortsAny").attr("checked","checked");
			$("#hotelsAny").attr("checked","checked");

            // Изменяем доступные категории отелей
            GetHotelStars();

			// Снятие/установка галочки "Любой курорт"
			$('#resortsList input:checkbox').change(function() {
				$("#resortsAny").attr("checked","checked");
				$("#resortsList input:checkbox:checked").each(function() {
					$("#resortsAny").removeAttr("checked");
				});
				// Изменение категорий отелей при смене списка курортов
				GetHotelStars();
			});
		});
	});







	//
	// Зависимые изменения
	//

	// Получаем список всех доступных категорий отелей
	function GetHotelStars() {
		// Составляем список курортов
		var resorts = "";
		if($("#resortsAny").attr("checked") !== "checked") {
			$("#resortsList input:checkbox:checked").each(function(i,resort) {
				resorts = resorts+resort.value+",";
			});
		}

		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "stars", country: $('#travelCountryTo').val(), resorts: resorts}, function(json){
			$('#starsList').empty();
			$('#starsList option').remove();
			$("#starsAny").attr("checked","checked");
			$.each(json.results,function(i,star){
				$('#starsList').append( $('<div class="chk"><input type="checkbox" name="starsList[]" value="'+star.Id+'"><label>'+star.Name+'</label></div>') );
			});

			// Снятие/установка галочки "Любая категория отеля"
			$('#starsList input:checkbox').change(function() {
				$("#starsAny").attr("checked","checked");
				$("#starsList input:checkbox:checked").each(function() {
					$("#starsAny").removeAttr("checked");
				});
				GetHotelList();
			});


			// Изменение списка отелей
			GetHotelList();
		});
	}


	// Получаем список всех доступных отелей
	function GetHotelList() {
		// Составляем список курортов
		var resorts = "";
		if($("#resortsAny").attr("checked") !== "checked") {
			$("#resortsList input:checkbox:checked").each(function(i,resort) {
				resorts = resorts+resort.value+",";
			});
		}

		// Составляем список категорий
		var stars = "";
		if($("#starsAny").attr("checked") !== "checked") {
			$("#starsList input:checkbox:checked").each(function(i,star) {
				stars = stars+star.value+",";
			});
		}

		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "hotels", country: $('#travelCountryTo').val(), resorts: resorts, stars: stars}, function(json){
			$('#hotelsList').empty();
			$('#hotelsList option').remove();
			$("#hotelsAny").attr("checked","checked");
			$.each(json.results,function(i,hotel){
				$('#hotelsList').append( $('<div class="chk"><input type="checkbox" name="hotelsList[]" value="'+hotel.Id+'"><label>'+hotel.Name+'</label></div>') );
			});

			// Снятие/установка галочки "Любой отель"
			$('#hotelsList input:checkbox').change(function() {
				$("#hotelsAny").attr("checked","checked");
				$("#hotelsList input:checkbox:checked").each(function() {
					$("#hotelsAny").removeAttr("checked");
				});
			});
		});
	}



	//
	// Обработчики галочек
	//

	// Галочка "Все курорты"
	$('#resortsAny').change(function() {
		// Изменение категорий отелей при смене списка курортов
		GetHotelStars();
	});

	// Снятие/установка галочки "Любой курорт"
	$('#resortsList input:checkbox').change(function() {
		$("#resortsAny").attr("checked","checked");
		$("#resortsList input:checkbox:checked").each(function() {
			$("#resortsAny").removeAttr("checked");
		});
		// Изменение категорий отелей при смене списка курортов
		GetHotelStars();
	});



	// Снятие/установка галочки "Любая категория отеля"
	$('#starsList input:checkbox').change(function() {
		$("#starsAny").attr("checked","checked");
		$("#starsList input:checkbox:checked").each(function() {
			$("#starsAny").removeAttr("checked");
		});
		GetHotelList();
	});

	// Галочка "Все категории отелей"
	$('#starsAny').change(function() {
		// Изменение категорий отелей при смене списка курортов
		GetHotelList();
	});



	// Снятие/установка галочки "Любой отель"
	$('#hotelsList input:checkbox').change(function() {
		$("#hotelsAny").attr("checked","checked");
		$("#hotelsList input:checkbox:checked").each(function() {
			$("#hotelsAny").removeAttr("checked");
		});
	});



	// Снятие/установка галочки "Любая категория питания"
	$('#mealsList input:checkbox').change(function() {
		$("#mealsAny").attr("checked","checked");
		$("#mealsList input:checkbox:checked").each(function() {
			$("#mealsAny").removeAttr("checked");
		});
	});
});



//
// Отправка формы поиска туров
//
function SendFormSearchTour(page, count) {

	// Показываем что ищем туры
	$("#searchTourStatus").css('display', 'block');
	$("#request").css('display', 'none');

	$.post("/bitrix/tools/simai.travel_ajax.php", $("#searchTour").serialize(), function(RequestResult) {
		$("#request").html(RequestResult+'<br>');
		CheckFormSearchTourStatus(RequestResult, 5, page, count);
	});
}



//
// Ожидание статуса обработки
//
function CheckFormSearchTourStatus (RequestResult, time, page, count) {
	// Делаем запрос статуса
	$.get("/bitrix/tools/simai.travel_ajax.php", { id: RequestResult, type: "searchStatus" }, function(status) {
		//$("#request").append(status+'<br>');
		if(status != "load") {
			setTimeout(function() {
				//$('#request').append('Проверка статуса, прошло '+time+' сек<br>');
				time += 5;
				CheckFormSearchTourStatus(RequestResult, time, page, count);
			}, 5000);
		}
		else {
			//$('#request').append('Данные обработаны за '+time+' сек<br>');
			SearchTourData(RequestResult, page, count);
		}
	});
}



// Отображение результата данных
function SearchTourData (RequestResult, page, count) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: page, count: count }, function(data) {
		$("#searchTourStatus").css('display', 'none');
		$("#request").css('display','block');
		$('#request').html(data);
	});
}

// Покупка тура
function SearchTourDataBuy (RequestResult, page, count, tour) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: page, count: count, tourBuy: tour }, function(data) {
		$('#request').html(data);

		// Плюсуем кол-во элементов в корзине
		var count = ($('#sidebar_product_count').html()*1)+1;
		$('#sidebar_product_count').html(count);

		// Показываем человеку что тур куплен
		var r=confirm("Перейти в корзину для оформления тура?")
		if (r==true)
		{
			window.location = '/personal/cart/';
		}
		else
		{

		}
	});
}



//
// Если переданы параметры то сразу ищем туры
//
function parseGetParams() {
   var $_GET = {};
   var __GET = window.location.search.substring(1).split("&");
   for(var i=0; i<__GET.length; i++) {
      var getVar = __GET[i].split("=");
      $_GET[getVar[0]] = typeof(getVar[1])=="undefined" ? "" : getVar[1];
   }
   return $_GET;
}

$(document).ready(function() {
	var curGet = parseGetParams();
	if(curGet.start == "Y") {
		SendFormSearchTour(1, curGet.count);
	}
});