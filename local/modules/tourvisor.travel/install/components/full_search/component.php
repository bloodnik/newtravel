<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// Преобразовываем дату в нужный вид
$arParams["DAY_FROM"] = date("d.m.Y", time()+60*60*24*$arParams["DAY_FROM"]);
$arParams["DAY_TO"] = date("d.m.Y", time()+60*60*24*$arParams["DAY_TO"]);

// Если есть переданные данные из маленькой формы, то обрабатываем их
if(!empty($_REQUEST["start"])) {
	$arr = explode("-", $_REQUEST["night"]);
	$arParams["NIGHT_FROM"] = $arr[0];
	$arParams["NIGHT_TO"] = $arr[1];

	$arParams["COUNTRY"] = $_REQUEST["countryId"];
	$arParams["CITY"] = $_REQUEST["cityFromId"];

	$arParams["DAY_FROM"] = $arParams["DAY_TO"] = $_REQUEST["date"];
	if(!empty($_REQUEST["3day"])) {
		$date = explode(".", $_REQUEST["date"]);
		$time = mktime(0, 0, 0, $date[1], $date[0], $date[2]);

		$arParams["DAY_FROM"] = date("d.m.Y", $time-60*60*24*3);
		$arParams["DAY_TO"] = date("d.m.Y", $time+60*60*24*3);
	}
}

// Запуск поиска туров
$arParams["START"] = ($_REQUEST["start"] == "Y") ? true : false;

$arResult = array();

// Кеширование
$obCache = new CPageCache;
if($obCache->StartDataCache($arParams["CACHE_TIME"], serialize($arParams),  "/")):

	// Подключение модуля
	CModule::IncludeModule("simai.travel");

	// Откуда
	$arResult["from"] = CSimaiTravelSletat::GetDepartCities();

	// Список стран
	$arResult["to"] = CSimaiTravelSletat::GetCountries($arParams["CITY"]);

	// Список курортов (не отдает если не указана страна назначения)
	$arResult["resorts"] = CSimaiTravelSletat::GetCities($arParams["COUNTRY"]);

	// Список отелей
	$arResult["hotels"] = CSimaiTravelSletat::GetHotels($arParams["COUNTRY"]);

	// Список категорий отелей
	$arResult["stars"] = CSimaiTravelSletat::GetHotelStars($arParams["COUNTRY"]);

	// Список категорий питания
	$arResult["meals"] = CSimaiTravelSletat::GetMeals();


	$this->IncludeComponentTemplate();

    // записываем предварительно буферизированный вывод в файл кеша
    $obCache->EndDataCache();

endif;
?>