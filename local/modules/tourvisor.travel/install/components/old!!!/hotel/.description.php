<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
  "NAME" => "Отель",
  "DESCRIPTION" => "Отображает информацию о выбранном отеле",
  "ICON" => "/images/icon.gif",
  "COMPLEX" => "N",
  "PATH" => array(
    "ID" => "Студия «Симай»",
  ),
);
?>