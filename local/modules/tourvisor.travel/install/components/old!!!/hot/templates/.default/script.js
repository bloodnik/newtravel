// Ожидание статуса обработки
function HotelTourStatus (RequestResult, count) {
	// Делаем запрос статуса
	$.get("/bitrix/tools/simai.travel_ajax.php", { id: RequestResult, type: "searchStatus" }, function(status) {
		if(status != "load") {
			setTimeout(function() {
				HotelTourStatus(RequestResult, count);
			}, 2000);
		}
		else {
			HotelTourData(RequestResult, count);
		}
	});
}

// Отображение результата данных
function HotelTourData (RequestResult, count) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, template: 'hot' }, function(data) {
		$('#hotelTour').html(data);
	});
}

// Покупка тура
function HotelTourBuy (RequestResult, count, tour) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, tourBuy: tour, template: 'hot' }, function(data) {
		$('#hotelTour').html(data);
	});
}

