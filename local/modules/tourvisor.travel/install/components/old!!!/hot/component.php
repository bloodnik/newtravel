<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// Преобразовываем дату в нужный вид
$arParams["DAY_FROM"] = date("d.m.Y", time()+60*60*24*$arParams["DAY_FROM"]);
$arParams["DAY_TO"] = date("d.m.Y", time()+60*60*24*$arParams["DAY_TO"]);

$arResult = array();

// Кеширование
$obCache = new CPageCache;
if($obCache->StartDataCache($arParams["CACHE_TIME"], serialize($arParams),  "/")):

	// Подключение модуля
	CModule::IncludeModule("simai.travel");

	// Список туров в отель
	$params = array(
		'requestId'=>0,
		'pageSize'=>30,
		'pageNumber'=>1,
		"countryId"=>$arParams["COUNTRY"],
		"cityFromId"=>$arParams["CITY"],
		"cities"=>array(),
		"meals"=> array(),
		"stars"=> array($arParams["STARS"]),
		"hotels"=> array(),
		"adults"=>2,
		"kids"=>0,
		"kidsAges"=>array(),
		"nightsMin" => $arParams["NIGHT_FROM"],
		"nightsMax" => $arParams["NIGHT_TO"],
		"priceMin" => "",
		"priceMax" => "",
		"currencyAlias" => $arParams["CURRENCY"],
		"departFrom" => $arParams["DAY_FROM"],
		"departTo" => $arParams["DAY_TO"],
		'hotelIsNotInStop'=>true,
		'hasTickets'=>false,
		'ticketsIncluded'=>false,
		'updateResult'=>true,
		'useFilter'=>false,
		'f_to_id'=>array(),
		'useTree'=>false,
		'groupBy'=>false,
		'includeDescriptions'=>true
	);
	$arResult["TOUR"] = CSimaiTravelSletat::CreateRequest($params);

	$this->IncludeComponentTemplate();

    // записываем предварительно буферизированный вывод в файл кеша
    $obCache->EndDataCache();

endif;
?>