<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $days = array("Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"); ?>

<h3>Найдено: <?=$arResult["count"]?> <?=$arResult["countStr"]?></h3>
<?if($arResult["BUY"] == true):?><font color="#BB3333">Куплен тур: <?=$_REQUEST["tourBuy"]?></font><br><?endif;?>

<table style="width:100%;">
	<?foreach($arResult["rows"] as $row):?>
		<tr>
			<td>
				<b>Курорт:</b> <?=$row["ResortName"]?><br>
				<b>Отель:</b> <a href="/hotel.php?id=<?=$row["HotelId"]?>"><?=$row["HotelName"]?></a><br>
				<b>Звезд:</b> <?=$row["StarName"]?><br>
				<a href="/hotel.php?id=<?=$row["HotelId"]?>"><img src="<?=$row["HotelTitleImageUrl"]?>"></a>
			</td>
			<td>
				<b>Вылет:</b> <?=$row["CheckInDate"]?> (<?=$days[$row["CheckInDateDayOfWeek"]]?>), на <?=$row["Nights"]?> <?=$row["NightsStr"]?><br>
				<b>Номер:</b> <?=$row["RoomName"]?><br>
				<b>Номер:</b> <?=$row["HtPlaceName"]?><br>
				<b>Питание:</b> <?=$row["MealName"]?><br><br>

				<b>Рейтинг:</b> <?=$row["HotelRating"]?><br>
				<b>До вылета:</b> <?=$row["CheckInDateStringTo"]?><br>
				<b>Наличие мест:</b> не определяется системой
			</td>
			<td>
				<b>Стоимость:</b> <?=$row["Price"]?> <?=$row["Currency"]?><br>
				<?if(in_array($row["OfferId"], $arResult["BASKET"])):?>
					<font color="#33bb33">Уже в корзине!</font>
				<?else:?>
					<a href="#tourList" onClick="SearchTourDataBuy (<?=$arParams["ID"]?>, <?=$arParams["PAGE"]?>, <?=$arParams["COUNT"]?>, <?=$row["OfferId"]?>);">Заказать</a>
				<?endif;?>
				<!-- <?if($arResult["oil"] != false):?><b>Топливные сборы:</b> <?=$arResult["oil"]["Tax"]?> <?=$arResult["oil"]["CurrencyName"]?><br><?endif;?> -->
			</td>
		</tr>
		<tr><td colspan="3" style="height: 1px; background-color: #000000;"></td></tr>
	<?endforeach;?>
</table>


<? /* постраничная навигация */ ?>
<br>
<?if($arResult["pageTotal"] > 0):?>
	<?if($arResult["pageNumber"] > 1):?><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]-1)?>, <?=$arResult["pageCount"]?>);">Предыдущая</a> &nbsp;|&nbsp; <?endif;?>
	<?for($i=1;$i<=($arResult["pageTotal"]+1);$i++):?>
		<?if($i > 1):?> &nbsp;|&nbsp; <?endif;?>
		<?if($arResult["pageNumber"] == $i):?>
			<b><?=$i?></b>
		<?else:?>
			<a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=$i?>, <?=$arResult["pageCount"]?>);"><?=$i?></a>
		<?endif;?>
	<?endfor;?>
	<?if($arResult["pageNumber"] < $arResult["pageTotal"]+1):?> &nbsp;|&nbsp; <a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]+1)?>, <?=$arResult["pageCount"]?>);">Следующая</a><?endif;?>
<?endif;?>