<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// Подключение модуля
CModule::IncludeModule("simai.travel");
CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
global $DB;

$arResult["data"] = CSimaiTravelSletat::GetRequestResult($arParams["ID"]);
// Список всех выдаваемых полей
//echo "\"".implode("\", \"", array_keys($arResult["data"]["GetRequestResultResult"]["Rows"]["XmlTourRecord"][0]))."\"";
//echo "<pre>";
//var_dump($arResult["data"]["GetRequestResultResult"]["Rows"]["XmlTourRecord"][0]);
//echo "</pre>";["OfferId"]

// Кол-во найденных туров
$arResult["count"] = count($arResult["data"]["GetRequestResultResult"]["Rows"]["XmlTourRecord"]);
$arResult["countStr"] = wform(substr($arResult["count"], -2), "тур", "тура", "туров");

// Топливные сборы определяются по ["SourceId"]
$arResult["oil"] = (is_array($arResult["data"]["GetRequestResultResult"]["OilTaxes"]["XmlTourOilTax"])) ? $arResult["data"]["GetRequestResultResult"]["OilTaxes"]["XmlTourOilTax"] : false;

// Для постраничной навигации
$arResult["pageCount"] = $arParams["COUNT"];
$arResult["pageNumber"] = $arParams["PAGE"];
$arResult["pageTotal"] = floor($arResult["count"] / $arParams["COUNT"]);

// Определяем инфоблок туров
$res = CIBlock::GetList(Array(), Array('TYPE'=>'simai', 'CODE'=>'travel_tour'), true);
if($ar_res = $res->Fetch()) {
	$iblock_id = $ar_res['ID'];
}

// Составляем список туров в нужном виде
$arResult["rows"] = array();
$i = -1;
foreach($arResult["data"]["GetRequestResultResult"]["Rows"]["XmlTourRecord"] as $row) {

	// Покупка тура
	if(!empty($_REQUEST["tourBuy"]) and $_REQUEST["tourBuy"] == $row["OfferId"]) {

		global $USER;

		// Добавляем тур в базу
		$el = new CIBlockElement;
		$PROP = $row;
		$PROP["USER_ID"] = $USER->GetID();
		$arLoadProductArray = Array(
			"MODIFIED_BY"    => $USER->GetID(),
			"IBLOCK_SECTION_ID" => false,
			"IBLOCK_ID"      => $iblock_id,
			"PROPERTY_VALUES"=> $PROP,
			"NAME"           => "Куплен ".date("d.m.Y")." пользователем ".$USER->GetLogin()." (".$USER->GetFullName().")",
			"ACTIVE"         => "Y",
		);
		$PRODUCT_ID = $el->Add($arLoadProductArray);

		// Назначаем цену
		$arFields = Array(
		    "PRODUCT_ID" => $PRODUCT_ID,
		    "CATALOG_GROUP_ID" => 1,
		    "PRICE" => $row["Price"],
		    "CURRENCY" => $row["Currency"],
		    "QUANTITY_FROM" => false,
		    "QUANTITY_TO" => false
		);
		CPrice::Add($arFields);

		// Добавляем тур в корзину
		$arFields = array(
			"PRODUCT_ID" => $PRODUCT_ID,
			"PRODUCT_PRICE_ID" => 0,
			"PRICE" => $row["Price"],
			"CURRENCY" => $row["Currency"],
			"WEIGHT" => 0,
			"QUANTITY" => 1,
			"LID" => LANG,
			"DELAY" => "N",
			"CAN_BUY" => "Y",
			"NAME" => $row["TourName"],
			"CALLBACK_FUNC" => "MyBasketCallback",
			"MODULE" => "simai.travel",
			"NOTES" => "",
			"ORDER_CALLBACK_FUNC" => "MyBasketOrderCallback",
			"DETAIL_PAGE_URL" => ""
		);
		CSaleBasket::Add($arFields);

		$arResult["BUY"] = true;
	}


	// Постраничная навигация
	$i++;
	if($i < $arResult["pageCount"]*($arResult["pageNumber"]-1) or $i >= $arResult["pageCount"]*$arResult["pageNumber"]) continue;

	// Определяем день недели вылета и время до вылета
	$date = explode(".", $row["CheckInDate"]);
	$time = mktime(0, 0, 0, intval($date[1]), intval($date[0]), $date[2]);
	$row["CheckInDateDayOfWeek"] = date("w", $time);
	$row["CheckInDateStringTo"] = date_diff_str($time);

	$row["NightsStr"] = wform($row["Nights"], "ночь", "ночи", "ночей");

	$arResult["rows"][] = $row;
}


/*
echo "<pre>";
var_dump($arResult["data"]);
echo "</pre>";
*/

// Определяем какие туры куплены пользователем
$arResult["BASKET"] = array();
$dbBasketItems = CSaleBasket::GetList(
	array(
		"NAME" => "ASC",
		"ID" => "ASC"
	),
	array(
		"FUSER_ID" => CSaleBasket::GetBasketUserID(),
		"LID" => SITE_ID,
		"ORDER_ID" => "NULL"
	),
	false,
	false,
	array(
		"ID",
		"CALLBACK_FUNC",
		"MODULE",
		"PRODUCT_ID",
		"QUANTITY",
		"DELAY",
		"CAN_BUY",
		"PRICE",
		"WEIGHT"
	)
);
while ($arItems = $dbBasketItems->Fetch()) {
	$q = $DB->Query("SELECT `VALUE` FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".$arItems["PRODUCT_ID"]."' AND `IBLOCK_PROPERTY_ID`= (
		SELECT `ID` FROM `b_iblock_property` WHERE `IBLOCK_ID` = '".$iblock_id."' AND `CODE`='OfferId'
	)");
	if ($row = $q->GetNext()) {
		$arResult["BASKET"][] = $row["VALUE"];
	}
}

$this->IncludeComponentTemplate();


//
// Импортируем новые отели в систему
//
if(empty($_SESSION["HOTEL_IMPORT"][$arParams["ID"]])) {

	// Запускаем перебор отелей только 1 раз
	$_SESSION["HOTEL_IMPORT"][$arParams["ID"]] = true;

	// Определяем инфоблок отелей
	$res = CIBlock::GetList(Array(), Array('TYPE'=>'simai', 'CODE'=>'travel_hotel'), true);
	if($ar_res = $res->Fetch()) {
		$iblock_hotel = $ar_res['ID'];
	}

	// Определяем инфоблок отзывов
	$res = CIBlock::GetList(Array(), Array('TYPE'=>'simai', 'CODE'=>'travel_reviews'), true);
	if($ar_res = $res->Fetch()) {
		$iblock_reviews = $ar_res['ID'];
	}

	$hotelList = array();
	foreach($arResult["data"]["GetRequestResultResult"]["Rows"]["XmlTourRecord"] as $hotel) {

		if($hotel["HotelId"] < 1) continue;

		if(in_array($hotel["HotelId"], $hotelList)) continue;
		$hotelList[] = $hotel["HotelId"];

		// Проверяем есть ли такой отель в базе
		$q = $DB->Query("SELECT * FROM `b_iblock_element` WHERE `IBLOCK_ID` = '".$iblock_hotel."' AND `CODE`='".$hotel["HotelId"]."'");
		if ($row = $q->GetNext()) {
			continue;
		}

		// Категория первого уровня
		$arFilter = Array('IBLOCK_ID'=>$iblock_hotel, 'NAME'=>$hotel["CountryName"]);
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);
		if($ar_result = $db_list->GetNext()){
			$section_1 = $ar_result['ID'];
		}
		else {
			$bs = new CIBlockSection;
			$arFields = Array(
				"ACTIVE" => "Y",
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID" => $iblock_hotel,
				"NAME" => $hotel["CountryName"],
				"CODE" => $hotel["CountryId"],
			);
			$section_1 = $bs->Add($arFields);
		}

		// Категория второго уровня
		$arFilter = Array('IBLOCK_ID'=>$iblock_hotel, 'SECTION_ID'=>$section_1, 'NAME'=>$hotel["ResortName"]);
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);
		if($ar_result = $db_list->GetNext()){
			$section_2 = $ar_result['ID'];
		}
		else {
			$bs = new CIBlockSection;
			$arFields = Array(
				"ACTIVE" => "Y",
				"IBLOCK_SECTION_ID" => $section_1,
				"IBLOCK_ID" => $iblock_hotel,
				"NAME" => $hotel["ResortName"],
				"CODE" => $hotel["ResortId"],
			);
			$section_2 = $bs->Add($arFields);
		}

		// Добавляем отель в систему
		$el = new CIBlockElement;
		$PROP = $hotel;
		$arLoadProductArray = Array(
			"IBLOCK_SECTION_ID" => $section_2,
			"IBLOCK_ID"      => $iblock_hotel,
			"PROPERTY_VALUES"=> $PROP,
			"NAME"           => $hotel["HotelName"],
			"CODE"           => $hotel["HotelId"],
			"ACTIVE"         => "Y",
		);
		$curHotel = $el->Add($arLoadProductArray);



		//
		// ИМПОРТ ОТЗЫВОВ СО СТАРОГО САЙТА
		//

		$iblock_old_hotel = 2;
		$iblock_old_reviews = 10;

		// Категория первого уровня
		$arFilter = Array('IBLOCK_ID'=>$iblock_old_hotel, 'NAME'=>$hotel["CountryName"]);
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);
		if($ar_result = $db_list->GetNext()){
			$section_1 = $ar_result['ID'];
			// Категория второго уровня
			$arFilter = Array('IBLOCK_ID'=>$iblock_old_hotel, 'SECTION_ID'=>$section_1, 'NAME'=>$hotel["ResortName"]);
			$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);
			if($ar_result = $db_list->GetNext()){
				$section_2 = $ar_result['ID'];
				// Отель
				$arFilter = Array("IBLOCK_ID"=>$iblock_old_hotel, "%NAME"=>$hotel["HotelName"], "ACTIVE"=>"Y");
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array());
				if($old_hotel = $res->GetNext()){
					// Перебираем все доступные отзывы и дублируем их
					$arFilter = Array("IBLOCK_ID"=>$iblock_old_reviews, "PROPERTY_HOTEL"=>$old_hotel["ID"], "ACTIVE"=>"Y");
					$res = CIBlockElement::GetList(Array("active_from"=>"asc"), $arFilter, false, false, array());
					while($ob = $res->GetNextElement()){
						$arFields = $ob->GetFields();
						$arProps = $ob->GetProperties();
						// Добавляем новый отзыв в систему
						$el = new CIBlockElement;
						$PROP = array();
						foreach($arProps as $nameProp => $valueProp) {
							$PROP[$nameProp] = $valueProp["VALUE"];
						}
						$PROP["HOTEL"] = $curHotel;
						$PROP["HOTEL_CODE"] = $hotel["HotelId"];
						$arLoadProductArray = Array(
							"IBLOCK_SECTION_ID" => false,
							"IBLOCK_ID"         => $iblock_reviews,
							"PROPERTY_VALUES"   => $PROP,
							"NAME"              => $arFields["NAME"],
							"DATE_CREATE"       => $arFields["DATE_CREATE"],
							"PREVIEW_TEXT"      => $arFields["PREVIEW_TEXT"],
							"ACTIVE_FROM"       => $arFields["ACTIVE_FROM"],
							"SORT"              => $arFields["SORT"],
							"PREVIEW_TEXT_TYPE" => $arFields["PREVIEW_TEXT_TYPE"]
						);
						$el->Add($arLoadProductArray);
					}
				}
			}
		}

		//
		// ---ИМПОРТ ОТЗЫВОВ СО СТАРОГО САЙТА
		//

	} // ---Перебор списка отелей
}
//
// ---Импортируем новые отели в систему
//

function wform($count, $single, $partitive, $plural) {
	return in_array($count,
		array(1, 21, 31, 41, 51)) ? $single : (
			in_array($count, array(2, 3, 4, 22, 23, 24, 32, 33, 34, 42, 43, 44, 52, 53, 54)) ? $partitive : $plural);
}

function date_diff_str($time) {
	$diff = floor(($time - time()) / (60*60*24));
	if ($diff < 8)
		return sprintf("%d %s", $diff, wform($diff, "день", "дня", "дней"));
	else
		return sprintf("%d %s %d %s", floor($diff / 7), wform(floor($diff / 7), "неделя", "недели", "неделей"), floor($diff % 7), wform(($diff % 7), "день", "дня", "дней"));
}
?>