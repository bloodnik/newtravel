<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();

// Кеширование
$obCache = new CPageCache;
if($obCache->StartDataCache($arParams["CACHE_TIME"], serialize($arParams),  "/")):

	// Подключение модуля
	CModule::IncludeModule("simai.travel");

	// Откуда
	$arResult["from"] = CSimaiTravelSletat::GetDepartCities();

	// Определяем задан ли город по умолчанию
	if($arParams["CITY"] != "" and $arParams["CITY"] > 0) {
		// Куда
		$arResult["to"] = CSimaiTravelSletat::GetCountries($arParams["CITY"]);
	}
	else {
		// Куда
		$arResult["to"] = CSimaiTravelSletat::GetCountries();
	}

	$this->IncludeComponentTemplate();

    // записываем предварительно буферизированный вывод в файл кеша
    $obCache->EndDataCache();

endif;
?>