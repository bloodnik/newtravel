<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

Название страны: <?=$arResult["COUNTRY"]["NAME"]?><br/>
Информация о стране доступна в массиве $arResult["COUNTRY"]<br /><br />

Название курорта: <?=$arResult["CITY"]["NAME"]?><br/>
Информация о курорте доступна в массиве $arResult["CITY"]<br /><br />

<p>Дальше будет отображаться список туров если их можно найти в данный курорт</p>
<div id="cityTour"></div>

<script type="text/javascript">
if(window.jQuery==undefined) {
	alert('Необходимо подключить библиотеку jQuery');
}
$(document).ready(function() {
	CityTourStatus (<?=$arResult["TOUR"]?>, <?=$arParams["COUNT"]?>);
});
</script>
