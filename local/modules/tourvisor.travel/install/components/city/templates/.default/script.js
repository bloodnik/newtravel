// Ожидание статуса обработки
function CityTourStatus (RequestResult, count) {
	// Делаем запрос статуса
	$.get("/bitrix/tools/simai.travel_ajax.php", { id: RequestResult, type: "searchStatus" }, function(status) {
		if(status != "load") {
			setTimeout(function() {
				CityTourStatus(RequestResult, count);
			}, 2000);
		}
		else {
			CityTourData(RequestResult, count);
		}
	});
}

// Отображение результата данных
function CityTourData (RequestResult, count) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, template: 'city'}, function(data) {
		$('#cityTour').html(data);
	});
}

// Покупка тура
function CityTourBuy (RequestResult, count, tour) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, tourBuy: tour, template: 'city' }, function(data) {
		$('#cityTour').html(data);
	});
}




