<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
  "NAME" => "Курорт",
  "DESCRIPTION" => "Отображает информацию о выбранном курорте",
  "ICON" => "/images/icon.gif",
  "COMPLEX" => "N",
  "PATH" => array(
    "ID" => "Студия «Симай»",
  ),
);
?>