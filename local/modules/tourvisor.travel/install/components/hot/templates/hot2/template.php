<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["SALE"]=intval($arParams["SALE_PERCENT"]);
?>
<h2 class="idea-title">Еще немного идей для отдыха</h2>
<div id="hotelTour">Ждите, идет загрузка.</div>

<script type="text/javascript">
if(window.jQuery==undefined) {
	alert('Необходимо подключить библиотеку jQuery');
}
$(document).ready(function() {
	HotelTourStatus (<?=$arResult["TOUR"]?>, <?=$arParams["COUNT"]?>, <?=$arParams["SALE"]?>);
});
</script>