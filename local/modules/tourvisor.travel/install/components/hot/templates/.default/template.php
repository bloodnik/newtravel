<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["SALE_PERCENT"]=intval($arParams["SALE_PERCENT"]);
?>
<h2 class="hot-tours">ГОРЯЩИЕ ТУРЫ
            <div class="sort-buttons"></div>
</h2>
<div id="hotelTour">Ждите, идет загрузка.</div>

<script type="text/javascript">
if(window.jQuery==undefined) {
	alert('Необходимо подключить библиотеку jQuery');
}
$(function() {
	HotelTourStatus (<?=$arResult["TOUR"]?>, <?=$arParams["COUNT"]?>, <?=$arParams["SALE_PERCENT"]?>);
      $(".tour-item.item-right").each(function(){
		var height=$(this).outerHeight();
		$(this).next(".palm").css({"height":height});
      });
});
</script>