// Ожидание статуса обработки
function HotelTourStatus (RequestResult, count, num) {
	// Делаем запрос статуса
	$.get("/bitrix/tools/simai.travel_ajax.php", { id: RequestResult, type: "searchStatus" }, function(status) {
		if(status != "load") {
			setTimeout(function() {
				HotelTourStatus(RequestResult, count, num);
			}, 2000);
		}
		else {
			HotelTourData(RequestResult, count, num);
		}
	});
}

// Отображение результата данных
function HotelTourData (RequestResult, count, num) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, template: 'hot', sale: num}, function(data) {
		$('#hotelTour').html(data);
	});
}

// Покупка тура
function HotelTourBuy (RequestResult, count, tour, num) {
if(num=="undefind")num=0;
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, tourBuy: tour, template: 'hot',sale: num }, function(data) {
		$('#hotelTour').html(data);
		$.get("/personal/cart/system.php", {ajax: "Y"}, function(data){$('#cartLine').html(data);});
	});
}

