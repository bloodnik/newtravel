<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

Название страны: <?=$arResult["COUNTRY"]["NAME"]?><br/>
Информация о стране доступна в массиве $arResult["COUNTRY"]<br /><br />

<p>Дальше будет отображаться список туров если их можно найти в данную страну</p>
<div id="countryTour"></div>

<script type="text/javascript">
if(window.jQuery==undefined) {
	alert('Необходимо подключить библиотеку jQuery');
}
$(document).ready(function() {
	CountryTourStatus (<?=$arResult["TOUR"]?>, <?=$arParams["COUNT"]?>);
});
</script>
