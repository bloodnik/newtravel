// Ожидание статуса обработки
function CountryTourStatus (RequestResult, count) {
	// Делаем запрос статуса
	$.get("/bitrix/tools/simai.travel_ajax.php", { id: RequestResult, type: "searchStatus" }, function(status) {
		if(status != "load") {
			setTimeout(function() {
				CountryTourStatus(RequestResult, count);
			}, 2000);
		}
		else {
			CountryTourData(RequestResult, count);
		}
	});
}

// Отображение результата данных
function CountryTourData (RequestResult, count) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, template: 'country'}, function(data) {
		$('#countryTour').html(data);
	});
}

// Покупка тура
function CountryTourBuy (RequestResult, count, tour) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, tourBuy: tour, template: 'country' }, function(data) {
		$('#countryTour').html(data);
	});
}




