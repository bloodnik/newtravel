<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// Преобразовываем дату в нужный вид
$arParams["DAY_FROM"] = date("d.m.Y", time()+60*60*24*$arParams["DAY_FROM"]);
$arParams["DAY_TO"] = date("d.m.Y", time()+60*60*24*$arParams["DAY_TO"]);


$arResult = array();

// Кеширование
$obCache = new CPageCache;
if($obCache->StartDataCache($arParams["CACHE_TIME"], serialize($arParams),  "/")):

	// Подключение модуля
	CModule::IncludeModule("simai.travel");
	CModule::IncludeModule("iblock");

	// Определяем инфоблок отелей
	$res = CIBlock::GetList(Array(), Array('TYPE'=>'simai', 'CODE'=>'travel_hotel'), true);
	if($ar_res = $res->Fetch()) {
		$iblock_hotel = $ar_res['ID'];
	}

	// Информация о отеле
	$arFilter = Array("IBLOCK_ID"=>$iblock_hotel, "CODE"=>$arParams["HOTEL"], "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array());
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arProps = $ob->GetProperties();
		$arFields["PROP"] = $arProps;
		$arResult["HOTEL"] = $arFields;
	}
	else {
		echo '<font color="#bb3333">Отель не найден</font>';
		return false;
	}

	// Список туров в отель
	$params = array(
		'requestId'=>0,
		'pageSize'=>30,
		'pageNumber'=>1,
		"countryId"=>$arResult["HOTEL"]["PROP"]["CountryId"]["VALUE"],
		"cityFromId"=>$arParams["CITY"],
		"cities"=>array($arResult["HOTEL"]["PROP"]["ResortId"]["VALUE"]),
		"meals"=> array(),
		"stars"=> array(),
		"hotels"=> array($arParams["HOTEL"]),
		"adults"=>2,
		"kids"=>0,
		"kidsAges"=>array(),
		"nightsMin" => $arParams["NIGHT_FROM"],
		"nightsMax" => $arParams["NIGHT_TO"],
		"priceMin" => "",
		"priceMax" => "",
		"currencyAlias" => $arParams["CURRENCY"],
		"departFrom" => $arParams["DAY_FROM"],
		"departTo" => $arParams["DAY_TO"],
		'hotelIsNotInStop'=>true,
		'hasTickets'=>true,
		'ticketsIncluded'=>true,
		'updateResult'=>true,
		'useFilter'=>false,
		'f_to_id'=>array(),
		'useTree'=>false,
		'groupBy'=>false,
		'includeDescriptions'=>true
	);
	$arResult["TOUR"] = CSimaiTravelSletat::CreateRequest($params);

	$this->IncludeComponentTemplate();

    // записываем предварительно буферизированный вывод в файл кеша
    $obCache->EndDataCache();

endif;
?>