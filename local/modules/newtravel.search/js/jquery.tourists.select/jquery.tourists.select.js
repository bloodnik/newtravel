/**
 * Плагин для выбора количнества туристов
 */

(function ($) {

    var methods = {
        /**
         * Инициализация плагина
         * @param options
         * @returns {*}
         */
        init: function (options) {
            var settings = {
                'adults': 2,
                'child': 0,
                'childage1': 0,
                'childage2': 0,
                'childage3': 0,

                'childage1Selected': false,
                'childage2Selected': false,
                'childage3Selected': false,

                'adults_id': 'adults',
                'child_id': 'child',
                'childage1_id': 'childage1',
                'childage2_id': 'childage2',
                'childage3_id': 'childage3',
            };

            return this.each(function () {
                if (options) {
                    //Проверка. Выбраны ли возраста детей
                    options.childage1 > 0 ? settings.childage1Selected = true : false;
                    options.childage2 > 0 ? settings.childage2Selected = true : false;
                    options.childage3 > 0 ? settings.childage3Selected = true : false;

                    $.extend(settings, options);
                }

                var container = $(this),
                    body = $('body'),
                    boxId = "tourists-box-" + getRandomInt(1111, 9999),
                    box = renderBox(boxId).hide(),
                    inputs = renderInputs(),
                    saveBtn = box.find('.tourists-save'),
                    adultsList = box.find('.adultsCount > div'),
                    childList = box.find('.kidsCount > div'),
                    noKidsBtn = box.find('#noKids'),
                    childAgeList = box.find('.kidsAgeList'),
                    selectChildAge1 = box.find('#selectChildAge1'),
                    selectChildAge2 = box.find('#selectChildAge2'),
                    selectChildAge3 = box.find('#selectChildAge3'),
                    childError = box.find('.child-error');

                body.append(box);
                container.after(inputs);

                container.unbind('.touristSelect').bind('click.touristSelect', function (evt) {
                    calcPosition();
                    box.slideDown(200);
                });

                var adultsInput = $('#adults'),
                    childInput = $('#child'),
                    childAge1Input = $('#childage1'),
                    childAge2Input = $('#childage2'),
                    childAge3Input = $('#childage3');

                //Выбираем взрослых
                adultsList.unbind('.touristSelect').bind('click.touristSelect', function (evt) {
                    var target = $(evt.target);
                    if (target[0].tagName == "I") {
                        target = target.parent();
                    }
                    adultsList.removeClass("active");
                    settings.adults = target.data('count');
                    target.addClass("active");
                    adultsInput.val(settings.adults);
                    if (settings.child > 0) {
                        container.text(settings.adults + " взр. " + settings.child + "реб.");
                    } else {
                        container.text(settings.adults + " взр.");
                    }

                    changeTrigger();
                });

                //Выбираем детей
                childList.unbind('.touristSelect').bind('click.touristSelect', function (evt) {
                    var target = $(evt.target);
                    if (target[0].tagName == "I") {
                        target = target.parent();
                    }
                    settings.child = target.data('count');


                    if (settings.child > 0) {
                        container.text(settings.adults + " взр. " + settings.child + "реб.");
                    } else {
                        container.text(settings.adults + " взр.");
                    }

                    if (target.hasClass("active")) {
                        target.removeClass("active");
                        childInput.val(0);
                        settings.child = "0";
                        settings.childage1 = "0";
                        settings.childage2 = "0";
                        settings.childage3 = "0";
                        selectChildAge1.hide().val(0);
                        selectChildAge2.hide().val(0);
                        selectChildAge3.hide().val(0);
                        childAgeList.css("display", "none");
                        container.text(settings.adults + " взр.");
                        childError.hide(200);
                        changeTrigger();
                        return false;
                    }

                    childList.removeClass("active");
                    childInput.val(settings.child);

                    switch (settings.child) {
                        case 1:
                            target.addClass("active");
                            childAgeList.show();
                            selectChildAge1.css("display", "inline-block");
                            selectChildAge2.css("display", "none");
                            selectChildAge3.hide();
                            break;
                        case 2:
                            target.addClass("active");
                            childAgeList.show();
                            selectChildAge1.css("display", "inline-block");
                            selectChildAge2.css("display", "inline-block");
                            selectChildAge3.hide();
                            break;
                        case 3:
                            target.addClass("active");
                            childAgeList.show();
                            selectChildAge1.css("display", "inline-block");
                            selectChildAge2.css("display", "inline-block");
                            selectChildAge3.css("display", "inline-block");
                            break;
                        default:
                            childList.removeClass("active");
                            childAgeList.hide();
                            selectChildAge1.hide();
                            selectChildAge2.hide();
                            selectChildAge3.hide();
                    }

                    changeTrigger();
                });

                //Без детей
                noKidsBtn.unbind('.touristSelect').bind('click.touristSelect', function (evt) {
                    var trigger = $(evt.target);

                    settings.child = 0;
                    childInput.val(settings.child);
                    if (settings.child > 0) {
                        container.text(settings.adults + " взр. " + settings.child + "реб.");
                    } else {
                        container.text(settings.adults + " взр.");
                    }
                    childList.removeClass("active");
                    childAgeList.hide();
                    selectChildAge1.hide();
                    selectChildAge2.hide();
                    selectChildAge3.hide();

                    changeTrigger();
                });


                //События выбора возраста детей
                selectChildAge1.unbind('.touristSelect').bind('change.touristSelect', function (evt) {
                    var value = $(evt.target).val();
                    selectChildAge1.removeClass('has-warning');
                    childAge1Input.val(value);
                    settings.childage1 = value;
                    changeTrigger();
                });
                selectChildAge2.unbind('.touristSelect').bind('change.touristSelect', function (evt) {
                    var value = $(evt.target).val();
                    selectChildAge2.removeClass('has-warning');
                    childAge2Input.val(value);
                    settings.childage2 = value;
                    changeTrigger();
                });
                selectChildAge3.unbind('.touristSelect').bind('change.touristSelect', function (evt) {
                    var value = $(evt.target).val();
                    selectChildAge3.removeClass('has-warning');
                    childAge3Input.val(value);
                    settings.childage3 = value;
                    changeTrigger();
                });

                //Кнопка сохранить
                saveBtn.unbind('.touristSelect').bind('click.touristSelect', function (evt) {
                    if (agesCorrect()) {
                        if (box.is(':visible')) box.slideUp(200);
                        childError.hide(200);
                    } else {
                        childError.show(200);
                    }
                });

                //if user click other place of the webpage, close date range picker window
                $(document).bind('click.touristSelect', function (evt) {
                    if (agesCorrect()) {
                        if (!IsOwnBoxClicked(evt, box[0])) {
                            if (box.is(':visible')) box.slideUp(200);
                        }
                        childError.hide(200);
                    } else {
                        childError.show(200);
                    }
                });

                //Подсчет позиции блока в зависимости от позиции контейнера
                function calcPosition() {
                    var offset = container.offset(),
                        containerH = container.outerHeight(),
                        containerW = container.outerWidth(),
                        windowWidth = $(window).width();

                    if (windowWidth < 767) //left to right
                    {
                        box.css({
                            position: 'fixed',
                            top: 0,
                            left: 0,
                            width: windowWidth,
                            'z-index': 9999
                        });
                    }
                    else {
                        box.css({
                            position: 'absolute',
                            top: offset.top,
                            left: offset.left
                        });
                    }
                }

                //Проверка куда был совершен клик, чтобы закрыть блок если был клик вне его
                function IsOwnBoxClicked(evt, selfObj) {
                    return ((evt.target === container[0] || selfObj.contains(evt.target) || evt.target == selfObj || (selfObj.childNodes != undefined && $.inArray(evt.target, selfObj.childNodes) >= 0)));
                }

                function changeTrigger() {
                    //Собитие после выбора. Возвращает выбранные параметры
                    container.trigger('touristsSelect-change', {
                        'adults': settings.adults,
                        'child': settings.child,
                        'childage1': settings.childage1,
                        'childage2': settings.childage2,
                        'childage3': settings.childage3
                    });
                }

                /**
                 * Проверка на заполненность возраста детей
                 * @returns {boolean}
                 */
                function agesCorrect() {
                    var agesCorrect = true;
                    if (settings.child > 0) {
                        switch (settings.child) {
                            case 1:
                                if (settings.childage1 == 0) {
                                    selectChildAge1.addClass('has-warning');
                                    agesCorrect = false;
                                }
                                break;
                            case 2:
                                if (settings.childage1 == 0) {
                                    selectChildAge1.addClass('has-warning');
                                    agesCorrect = false;
                                }
                                if (settings.childage2 == 0) {
                                    selectChildAge2.addClass('has-warning');
                                    agesCorrect = false;
                                }
                                break;
                            case 3:
                                if (settings.childage1 == 0) {
                                    selectChildAge1.addClass('has-warning');
                                    agesCorrect = false;
                                }
                                if (settings.childage2 == 0) {
                                    selectChildAge2.addClass('has-warning');
                                    agesCorrect = false;
                                }
                                if (settings.childage3 == 0) {
                                    selectChildAge3.addClass('has-warning');
                                    agesCorrect = false;
                                }
                                break;
                        }
                    }
                    return agesCorrect;
                }
            });

            /**
             * Отрисовываем окно
             * @returns {string}
             */
            function renderBox(boxId) {
                var $html = "";

                $html += "<div id='" + boxId + "' class='touristsSelect'>";

                $html += "<div class='tourists-row'><label>Взрослых: </label>";

                $html += "<div class='adultsCount'>";
                $html += "<div data-count='1'><i class='fa fa-male fa-2x'></i></div>";
                $html += "<div data-count='2'><i class='fa fa-male fa-2x'></i><i class='fa fa-male fa-2x'></i></div>";
                $html += "<div data-count='3'><i class='fa fa-male fa-2x'></i><i class='fa fa-male fa-2x'></i><i class='fa fa-male fa-2x'></i></div>";
                $html += "<div data-count='4'><i class='fa fa-male fa-2x'></i><i class='fa fa-male fa-2x'></i><i class='fa fa-male fa-2x'></i><i class='fa fa-male fa-2x'></i></div>";
                $html += "</div>";


                $html += "<label>Количество детей:</label>";
                $html += "<a href='javascript:void(0)' id='noKids' class='pull-right small'>без детей</a>";
                $html += "<div class='kidsCount'>";
                $html += "<div data-count='1'><i class='fa fa-child fa-2x'></i></div>";
                $html += "<div data-count='2'><i class='fa fa-child fa-2x'></i><i class='fa fa-child fa-2x'></i></div>";
                $html += "<div data-count='3'><i class='fa fa-child fa-2x'></i><i class='fa fa-child fa-2x'></i><i class='fa fa-child fa-2x'></i></div>";
                $html += "</div>";

                $html += "</div>";

                $html += "<div class='tourists-row kidsAgeList'>";
                $html += "<label>Возраст детей:</label> <br/>";
                $html += "<select id='selectChildAge1' name='selectChildAge1' class='form-control'>";
                for (var i = 0; i <= 15; i++) {
                    $html += "<option value=" + i + ">" + i + "</option>"
                }
                $html += "</select>";
                $html += "<select id='selectChildAge2' name='selectChildAge2' class='form-control'>";
                for (var i = 0; i <= 15; i++) {
                    $html += "<option value=" + i + ">" + i + "</option>"
                }
                $html += "</select>";
                $html += "<select id='selectChildAge3' name='selectChildAge3' class='form-control'>";
                for (var i = 0; i <= 15; i++) {
                    $html += "<option value=" + i + ">" + i + "</option>"
                }
                $html += "</select>";

                $html += "</div>";
                $html += "<div class='text-danger small child-error' style='display: none;'>выберите возраст ребенка</div>";

                $html += "<button type='button' class='btn btn-blue tourists-save btn-block'>Сохранить</button>";


                $html += "</div>";

                return $($html);
            }

            function renderInputs() {
                $html = "";
                $html += "<input type='hidden' name='adults' id='" + settings.adults_id + "' value='" + settings.adults + "' />";
                $html += "<input type='hidden' name='child' id='" + settings.child_id + "' value='" + settings.child + "' />";
                $html += "<input type='hidden' name='childage1' id='" + settings.childage1_id + "' value='" + settings.childage1 + "' />";
                $html += "<input type='hidden' name='childage2' id='" + settings.childage2_id + "' value='" + settings.childage2 + "' />";
                $html += "<input type='hidden' name='childage3' id='" + settings.childage3_id + "' value='" + settings.childage3 + "' />";

                return $($html);
            }

            // использование Math.round() даст неравномерное распределение!
            function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }
        },

        destroy: function () {
            console.log('hide');
        }
    };

    $.fn.touristSelect = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для Jquery.touristSelect');
        }
    }
})($);


  
    
    
