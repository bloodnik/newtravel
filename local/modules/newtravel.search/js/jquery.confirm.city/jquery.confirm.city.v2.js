/**
 * Плагин вывода popover для подтверждения города вылета
 */

(function ($) {

    var methods = {
        /**
         * Инициализация плагина
         * @param options
         * @returns {*}
         */
        init: function (options) {
            var settings = {
                "departureModal" : $('#departureModal'),
                'departureName': $('#departureValue').text(),
                'departureId': $('#departure').val(),
                'popverPlacement' : 'bottom',
                'cookieName' : 'NEWTRAVEL_USER_CITY'
            };

            return this.each(function () {
                if (options) {
                    $.extend(settings, options);
                }

                var container = $(this),
                    body = $('body'),
                    popoverHtml = renderHtml(),
                    confirmBtn = popoverHtml.find('.confirmBtn'),
                    noConfirmBtn = popoverHtml.find('.noConfirmBtn');

                
                container.popover({
                    content: popoverHtml,
                    html: true,
                    placement: settings.popverPlacement,
                    title: "<i class='fa fa-map-marker'></i> &nbsp; <strong>Ваш город вылета " + settings.departureName + "?</strong>",
                    trigger: "manual",
                    delay: {"show": 2000, "hide": 200}
                });

                //Потдверждаем город
                confirmBtn.unbind('.confirmCity').bind('click.confirmCity', function (evt) {
                    container.popover('hide');
                    var curCity = settings.departureId;
                    BX.setCookie(settings.cookieName, curCity, { path: '/' });
                });
                
                //Не подтверждаем город
                noConfirmBtn.unbind('.confirmCity').bind('click.confirmCity', function (evt) {
                    container.popover('hide');
                    settings.departureModal.webuiPopover('show');
                });


                var showConfirm = BX.getCookie(settings.cookieName) == null;
                if (showConfirm) {
                    container.popover('show');
                }

            });

            /**
             * Отрисовываем popover
             * @returns {string}
             */
            function renderHtml() {
                var $html = "<div>От выбранного города зависит цена на тур, количество предложений <br> и доступность стран</div>";
                $html += "<div class='text-right'><button class='btn btn-info confirmBtn'>Да</button>&nbsp;&nbsp;<button class='btn btn-default noConfirmBtn'>Нет</button></div>";
                return $($html);
            }
        }
    };

    $.fn.confirmCity = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для Jquery.confirmCity');
        }
    }
})(jQuery);


  
    
    
