/**
 * Плагин для выбора количества ночей
 */

(function ($) {

    var methods = {
        /**
         * Инициализация плагина
         * @param options
         * @returns {*}
         */
        init: function (options) {
            var settings = {
                'nfrom_id': 'nightsfrom',
                'nto_id': 'nightsto',
                'defaultFrom': 6,
                'defaultTo': 12,
                'nfrom': 0,
                'nto': 0,
            };

            return this.each(function () {
                if (options) {
                    $.extend(settings, options);
                }

                var container = $(this),
                    body = $('body'),
                    boxId = "nights-select-" + getRandomInt(1111, 9999),
                    box = renderBox(boxId).hide(),
                    inputs = renderInputs(),
                    boxItems = box.find('.box-item');

                body.append(box);
                container.after(inputs);

                container.unbind('.nightsSelect').bind('click.nightsSelect', function (evt) {
                    calcPosition();
                    box.slideDown(200);
                });

                boxItems.unbind('.nightsSelect').bind('click.nightsSelect', function (evt) {
                    var target = evt.target,
                        nightsTo,
                        nightsFrom;

                    if (settings.nto == 0 && settings.nfrom > 0) { //Елси уже нажали на ночей От, выбираем ночей До
                        nightsTo = $(this).attr("data-value"); //Выбранное значение
                        settings.nto = parseInt(nightsTo); //Количество До
                        if (settings.nfrom <= settings.nto) { //Если  ночей от меньше чем ночей до
                            $(target).addClass("to-active");

                            $("#nightsto").val(nightsTo); //Устанавливаем значение для элемента формы
                            container.text("от " + settings.nfrom + " до " + settings.nto);

                            //Собитие после выбора. Возвращает выбранные параметры от и до
                            container.trigger('nightSelect-change', {
                                'from': settings.nfrom,
                                'to': settings.nto
                            });

                            //Обнуляем все
                            $(box).find("span").text("от:");
                            boxItems.removeClass("to-active").removeClass("from-active").removeClass("in-range");
                            settings.nfrom = 0;
                            settings.nto = 0;
                            box.slideUp(200);

                            $("#nights-error").css("display", "none");
                        } else {
                            $("#nights-error").fadeIn(200);
                            settings.nto = 0;
                        }
                    }

                    if (settings.nfrom == 0 && nightsTo == undefined) { //Если еще не выбрали ночей От
                        nightsFrom = $(target).attr("data-value"); //Выбранное значение
                        $("#nightsfrom").val(nightsFrom); //Устанавливаем значение для элемента формы
                        settings.nfrom = parseInt(nightsFrom); //Количество От
                        $(box).find("span").text("до:");
                        $(target).addClass("from-active");
                    }
                }).bind('mouseover.nightsSelect', function (evt) { //Собитие. Закрашивать выбранный участок
                    var target = evt.target;

                    if (settings.nfrom > 0) {
                        var val = $(target).attr("data-value");
                        boxItems.each(function (key, item) {
                            var dataValue = $(this).attr("data-value");
                            if (parseInt(dataValue) < parseInt(val) && parseInt(dataValue) > settings.nfrom) {
                                $(this).addClass("in-range");
                            } else {
                                $(this).removeClass("in-range");
                            }
                        })
                    }
                });


                //if user click other place of the webpage, close date range picker window
                $(document).bind('click.nightsSelect', function (evt) {
                    if (!IsOwnBoxClicked(evt, box[0])) {
                        if (box.is(':visible')) box.slideUp(200);
                    }
                });

                //Подсчет позиции блока в зависимости от позиции контейнера
                function calcPosition() {
                    var offset = container.offset(),
                        containerH = container.outerHeight(),
                        containerW = container.outerWidth(),
                        windowWidth = $(window).width();

                    if (windowWidth < 767) //left to right
                    {
                        box.css({
                            position: 'fixed',
                            top: 0,
                            left: 0,
                            width: windowWidth,
                            'z-index': 9999
                        });
                    }
                    else {
                        box.css({
                            top: offset.top,
                            left: offset.left
                        });
                    }


                }

                //Проверка куда был совершен клик, чтобы закрыть блок если был клик вне его
                function IsOwnBoxClicked(evt, selfObj) {
                    return (  evt.target === container[0] || selfObj.contains(evt.target) || evt.target == selfObj || (selfObj.childNodes != undefined && $.inArray(evt.target, selfObj.childNodes) >= 0));
                }
            });

            /**
             * Отрисовываем окно
             * @returns {string}
             */
            function renderBox(boxId) {
                var $html = "";

                $html += "<div id='" + boxId + "' class='nights-select'>Выберите ночей <span style='font-weight: bold;'>от:</span><table style='width: 100%;'>";

                $html += "<tr>";
                for (var i = 2; i <= 8; i++) {
                    $html += "<td class='box-item' data-value='" + i + "'>" + i + "</td>"
                }
                $html += "</tr>";
                $html += "<tr>";
                for (var i = 9; i <= 15; i++) {
                    $html += "<td class='box-item' data-value='" + i + "'>" + i + "</td>"
                }
                $html += "</tr>";
                $html += "<tr>";
                for (var i = 16; i <= 22; i++) {
                    $html += "<td class='box-item' data-value='" + i + "'>" + i + "</td>"
                }
                $html += "</tr>";
                $html += "<tr>";
                for (var i = 23; i <= 29; i++) {
                    $html += "<td class='box-item' data-value='" + i + "'>" + i + "</td>"
                }
                $html += "</tr>";

                $html += "</table><span class='no' id='nights-error' style='display:none; font-size: 75%;'>Ночей \"до\" не может быть меньше \"от\"</span></div>";

                return $($html);
            }

            function renderInputs() {
                $html = "";
                $html += "<input type='hidden' name='nightsfrom' id='" + settings.nfrom_id + "' value='" + settings.defaultFrom + "' />";
                $html += "<input type='hidden' name='nightsto' id='" + settings.nto_id + "' value='" + settings.defaultTo + "' />";

                return $($html);
            }

            // использование Math.round() даст неравномерное распределение!
            function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }
        },

        destroy: function () {
            console.log('hide');
        }
    };

    $.fn.nightsSelect = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для Jquery.nightsSelect');
        }
    }
})($);


  
    
    
