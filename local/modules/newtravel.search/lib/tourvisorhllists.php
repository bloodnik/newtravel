<?php

namespace Newtravel\Search;

use Bitrix\Main\Loader;
use Bitrix\Main\Entity;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Entity\Query;
use Newtravel\Search\Tourvisor;

/**
 * Class TourvisorHLLists - работа с highload блоками справочников турвизор
 *
 * @package Newtravel\Search
 */
class TourvisorHlLists {

	private function getHlId($code) {
		$config = array(
			"departures" => 2,
			"countries" => 1,
			"meals" => 3,
			"operators" => 5,
			"regions" => 4,
			"flighttime" => 6,
		);
		return $config[$code];
	}

	/**
	 * Получаем класс сущности highload блока
	 * @param $id
	 *
	 * @return Entity\Base
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	private function getEntyty ($code){
		Loader::includeModule("highloadblock");
		$hldata = HighloadBlockTable::getById(self::getHlId($code))->fetch();
		return HighloadBlockTable::compileEntity($hldata);
	}

	/**
	 * Обновляем справочники
	 */
	public static function udpateLists() {
		self::updateCountries("countries");
		self::updateDepartures("departures");
		self::updateMeal("meals");
		self::updateRegion("regions");
		self::updateOperators("operators");
		//self::updateSubregions("subregions");
	}


	/**
	 * Обновляем города вылета
	 * @throws \Exception
	 */
	private function updateDepartures($hl_id){
		$hlentity = self::getEntyty($hl_id);
		$dataClass = $hlentity->getDataClass();

		//Получаем данные от турвизора
		$tv = new Tourvisor();
		$tourvisorList = $tv->getData('departure' , array());
		$arHlList = self::getList($hl_id, array("UF_XML_ID"));
		$arXML_IDs = self::getXML_IDList($arHlList); //Уже существующие XML_ID


		foreach ($tourvisorList['lists']['departures']['departure'] as $item) {
			if(in_array($item['id'], $arXML_IDs)) continue; //Если данный XML_ID уже существует, тогда пропускаем
			$arElementFields = array(
				'UF_NAME'       => $item['name'],
				'UF_XML_ID'     => $item['id'],
				'UF_NAME_FROM'     => $item['namefrom'],
			);

			//Добавляем в HL блок
			$obresult = $dataClass::add($arElementFields);
			if ($obresult->isSuccess()) {
				PR($obresult->getId());
			}
		}
		echo "Список городов обновлен <br>";
	}

	/**
	 * Обновляем страны
	 * @throws \Exception
	 */
	private function updateCountries($hl_id){
		$hlentity = self::getEntyty($hl_id);
		$dataClass = $hlentity->getDataClass();

		//Получаем данные от турвизора
		$tv = new Tourvisor();
		$tourvisorList = $tv->getData('country');
		$arHlList = self::getList($hl_id, array("UF_XML_ID"));
		$arXML_IDs = self::getXML_IDList($arHlList); //Уже существующие XML_ID

		foreach ($tourvisorList['lists']['countries']['country'] as $item) {
			if(in_array($item['id'], $arXML_IDs)) continue; //Если данный XML_ID уже существует, тогда пропускаем
			$arElementFields = array(
				'UF_NAME'       => $item['name'],
				'UF_XML_ID'     => $item['id'],
			);

			//Добавляем в HL блок
			$obresult = $dataClass::add($arElementFields);
			if ($obresult->isSuccess()) {
				PR($obresult->getId());
			}
		}
		echo "Страны обновлены <br>";
	}

	/**
	 * Обновляем типы питания
	 * @throws \Exception
	 */
	private function updateMeal($hl_id){
		$hlentity = self::getEntyty($hl_id);
		$dataClass = $hlentity->getDataClass();

		//Получаем данные от турвизора
		$tv = new Tourvisor();
		$tourvisorList = $tv->getData('meal');
		$arHlList = self::getList($hl_id, array("UF_XML_ID"));
		$arXML_IDs = self::getXML_IDList($arHlList); //Уже существующие XML_ID


		foreach ($tourvisorList['lists']['meals']['meal'] as $item) {
			if(in_array($item['id'], $arXML_IDs)) continue; //Если данный XML_ID уже существует, тогда пропускаем
			$arElementFields = array(
				'UF_NAME'       => $item['russian'],
				'UF_CODE'       => $item['name'],
				'UF_XML_ID'     => $item['id'],
			);

			//Добавляем в HL блок
			$obresult = $dataClass::add($arElementFields);
			if ($obresult->isSuccess()) {
				PR($obresult->getId());
			}
		}
		echo "Список питания обновлен <br>";
	}

	/**
	 * Обновляем курорты страны
	 * @throws \Exception
	 */
	private function updateRegion($hl_id){
		$hlentity = self::getEntyty($hl_id);
		$dataClass = $hlentity->getDataClass();

		//Получаем данные от турвизора
		$tv = new Tourvisor();
		$tourvisorList = $tv->getData('region');
		$arHlList = self::getList($hl_id, array("UF_XML_ID"));
		$arXML_IDs = self::getXML_IDList($arHlList); //Уже существующие XML_ID

		foreach ($tourvisorList['lists']['regions']['region'] as $item) {
			if(in_array($item['id'], $arXML_IDs)) continue; //Если данный XML_ID уже существует, тогда пропускаем
			$arElementFields = array(
				'UF_NAME'       => $item['name'],
				'UF_XML_ID'     => $item['id'],
				'UF_COUNTRY_ID'       => $item['country'],
			);

			//Добавляем в HL блок
			$obresult = $dataClass::add($arElementFields);
			if ($obresult->isSuccess()) {
				PR($obresult->getId());
			}
		}
		echo "Список регионов обновлен <br>";
	}

	/**
	 * Обновляем субкурорты
	 * @throws \Exception
	 */
	private function updateSubregions($hl_id){
		$hlentity = self::getEntyty($hl_id);
		$dataClass = $hlentity->getDataClass();

		//Получаем данные от турвизора
		$tv = new Tourvisor();
		$tourvisorList = $tv->getData('subregion');
		$arHlList = self::getList($hl_id, array("UF_XML_ID"));
		$arXML_IDs = self::getXML_IDList($arHlList); //Уже существующие XML_ID

		foreach ($tourvisorList['lists']['subregions']['subregion'] as $item) {
			if(in_array($item['id'], $arXML_IDs)) continue; //Если данный XML_ID уже существует, тогда пропускаем

			$arElementFields = array(
				'UF_NAME'       => $item['name'],
				'UF_XML_ID'     => $item['id'],
				'UF_REGION_ID'     => $item['parentregion'],
			);

			//Добавляем в HL блок
			$obresult = $dataClass::add($arElementFields);
			if ($obresult->isSuccess()) {
				PR($obresult->getId());
			}
		}
	}

	/**
	 * Обновляем туроператоров
	 * @throws \Exception
	 */
	private function updateOperators($hl_id){
		$hlentity = self::getEntyty($hl_id);
		$dataClass = $hlentity->getDataClass();

		//Получаем данные от турвизора
		$tv = new Tourvisor();
		$tourvisorList = $tv->getData('operator');
		$arHlList = self::getList($hl_id, array("UF_XML_ID"));
		$arXML_IDs = self::getXML_IDList($arHlList); //Уже существующие XML_ID

		$arUsedOperators = array(11, 12, 13,15,16,17,18,23,24,25,27,28,30,31,33,36,39,40,41,43,44,47,55,58,60,64,65,79); //Используемые операторы

		foreach ($tourvisorList['lists']['operators']['operator'] as $item) {
			if(in_array($item['id'], $arXML_IDs)) continue; //Если данный XML_ID уже существует, тогда пропускаем
			if(!in_array($item['id'], $arUsedOperators)) continue; //Заполняем только используемыми операторами

			$arElementFields = array(
				'UF_NAME'       => $item['fullname'],
				'UF_XML_ID'     => $item['id'],
			);

			//Добавляем в HL блок
			$obresult = $dataClass::add($arElementFields);
			if ($obresult->isSuccess()) {
				PR($obresult->getId());
			}
		}
		echo "Список операторов обновлен <br>";
	}


	/**
	 * Получаем данные из HL блока
	 * @param $hl_id
	 * @param array $select
	 *
	 * @return array
	 */
	public static function getList($hl_code, $select = array('*'), $filter = array()){
		$hlentity = self::getEntyty($hl_code);
		$query = new Query($hlentity);
		return $query->setSelect($select)->setFilter($filter)->exec()->fetchAll();
	}
	
	/**
	 * Преобразуем двумерный массив в одномерный
	 * @param $arData - двуменый массив из UL_XML_ID hl блока
	 *
	 * @return array
	 */
	private function getXML_IDList($arData){
		$result = array();
		foreach ($arData as $arItem) {
			$result[] = $arItem['UF_XML_ID'];
		}
		return $result;
	}
}