<?php
namespace Newtravel\Search;
use CUser;
/**
 * Class Helpers - класс обертка для полезных функций
 *
 * @package Newtravel\Search
 */
class Helpers {

	/**
	 * Получаем псевдо старую цену
	 * @param $price - новая цена
	 *
	 * @return mixed - строку отформатированная строка со старой ценой
	 */
	public static function getOldPrice($price) {
		$percent = rand(10,15)/100;
		return CurrencyFormat($price + $price*$percent, 'RUB');
	}

	public static function getUserByEmail ($email = "") {
		
		$filter = Array
		(
			"EMAIL"               => $email,
		);
		$rsUsers = CUser::GetList(($by="email"), ($order="desc"), $filter); // выбираем пользователей
		if($arUser = $rsUsers->GetNext()){
			return $arUser;
		}else{
			return false;
		}
	}

	/**
	 * @param $price - оригинальная цена тура
	 * @param int $discount - скидка %
	 *
	 * Цена тура с нашей скидкой
	 *
	 * @return string
	 */
	public static function getDiscountPrice($price, $discount = 1.5){
		$percent = $discount/100;
		return $price - $price*$percent;
	}

}