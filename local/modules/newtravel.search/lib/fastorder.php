<?php
namespace Newtravel\Search;

use Bitrix\Main\Loader;
use CFormResult;
use CForm;
use CFormCRM;
use CStatistic;
use Newtravel\Search\Tourvisor;

/**
 * Class FastOrder - класс для создания результата веб формы "Заказ в 1 клик"
 *
 * @package Newtravel\Search
 */
class FastOrder {

	/**
	 * Получаем актуализацию тура. Создаем новый результат веб формы.
	 *
	 * @return mixed - json с результатом работы метода
	 */
	public static function setResult($tourid, $phone, $name, $credit) {
		$FORM_ID = 8;
		$arResult = array();

		if (empty($tourid)) {
			$arResult['STATUS'] = false;
			$arResult['ERROR']  = "Не известный номер тура";

			return json_encode($arResult);

		}
		if (empty($phone)) {
			$arResult['STATUS'] = false;
			$arResult['ERROR']  = "Не заполнен номер телефона";

			return json_encode($arResult);
		}
		if (empty($name)) {
			$arResult['STATUS'] = false;
			$arResult['ERROR']  = "Не введено имя";

			return json_encode($arResult);
		}


		//Актуализируем тур и герерируем строку с информацией о ней
		$tv   = new Tourvisor();
		$tour = $tv->getData("actualize", array('tourid' => $tourid));

		$tourString = 'Название тура: ' . $tour['data']['tour']['tourname'] . "\n";

		if($credit === "Y") {
			$tourString .=  "ТУР В РАССРОЧКУ" . "\n";
		}

		$tourString .= 'Дата вылета: ' . $tour['data']['tour']['flydate'] . "\n";
		$tourString .= 'Оператор: ' . $tour['data']['tour']['operatorname'] . "\n";
		$tourString .= 'Город вылета: ' . $tour['data']['tour']['departurename'] . "\n";
		$tourString .= 'Страна: ' . $tour['data']['tour']['countryname'] . "\n";
		$tourString .= 'Регион: ' . $tour['data']['tour']['hotelregionname'] . "\n";
		$tourString .= 'Отель: ' . $tour['data']['tour']['hotelname'] . "\n";
		$tourString .= 'Комната: ' . $tour['data']['tour']['room'] . "\n";
		$tourString .= 'Ночей: ' . $tour['data']['tour']['nights'] . "\n";
		$tourString .= 'Питание: ' . $tour['data']['tour']['meal'] . "\n";
		$tourString .= 'Размещение : ' . $tour['data']['tour']['placement'] . "\n";
		$tourString .= 'Взрослых : ' . $tour['data']['tour']['adults'] . "\n";
		$tourString .= 'Детей : ' . $tour['data']['tour']['child'] . "\n";
		if(intval($tour['data']['tour']['child']) > 0) {
			$tourString .= 'Возраст ребенка 1 : ' . $tour['data']['tour']['childage1'] . "\n";
			$tourString .= 'Возраст ребенка 2 : ' . $tour['data']['tour']['childage2'] . "\n";
			$tourString .= 'Возраст ребенка 3 : ' . $tour['data']['tour']['childage3'] . "\n";
		}
		$tourString .= 'Стоимость: ' . $tour['data']['tour']['price'] . "руб. \n";
		$tourString .= 'Ссылка на туроператора : ' . $tour['data']['tour']['operatorlink'] . "\n";
		$tourString .= 'ID тура : ' . $tourid . "\n";


		//Ответы формы
		$arValues = array(
			"form_text_83"     => htmlspecialchars($tour['data']['tour']['price']),  // "Бюджет"
			"form_text_71"     => htmlspecialchars(clearPhoneString($phone)),  // "Телефон"
			"form_text_82"     => htmlspecialchars($name),  // "Имя"
			"form_textarea_73" => $tourString,     // "Данные тура"
		);

		Loader::includeModule('form');

		//Проверяем форму на ошбики
		$arResult["ERROR"] = CForm::Check($FORM_ID, $arValues, false, "Y", "N");
		if(strlen($arResult["ERRORS"]) > 0) {
			$arResult['STATUS'] = false;
			$arResult['ERROR']  = $arResult["ERRORS"];
			return json_encode($arResult);
		}
		
		// создадим новый результат
		if (check_bitrix_sessid() && $RESULT_ID = CFormResult::Add($FORM_ID, $arValues))
		{
			$arResult['STATUS'] = true;
			$arResult['RESULT_ID'] = $RESULT_ID;

			//Выводим событие для модуля статистики
			if(Loader::includeModule("statistic"))
			{
				$event1 = "eStore";
				$event2 = "fast_order";
				$event3 = $RESULT_ID;

				$e = $event1."/".$event2."/".$event3;

				if(!is_array($_SESSION["ORDER_EVENTS"]) || (is_array($_SESSION["ORDER_EVENTS"]) && !in_array($e, $_SESSION["ORDER_EVENTS"])))
				{
					CStatistic::Set_Event($event1, $event2, $event3);
					$_SESSION["ORDER_EVENTS"][] = $e;
				}
				
				CFormCRM::onResultAdded($FORM_ID, $RESULT_ID);
				CFormResult::Mail($RESULT_ID);
			}

		}
		else
		{
			global $strError;
			$arResult['STATUS'] = false;
			$arResult['ERROR'] = $strError;
		}

		return json_encode($arResult);

	}


}