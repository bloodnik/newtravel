<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Newtravel\Search\Tourvisor;
use Newtravel\Search\FastOrder;

$request = Application::getInstance()->getContext()->getRequest();
//Массив переданных параметров
$query_list = $request->getQueryList()->toArray();

/*=========================================  Поиск туров  ===================================================*/

Loader::includeModule("newtravel.search");
$Tourvisor = new Tourvisor();


// departure - список городов вылета
//Выдает полный список городов вылета, дополнительных параметров нет.
if ($request->get('type') == "departure") {
	echo $Tourvisor->getJson('departure');
}

/* country - список стран
 * Без параметров выдается полный список стран (все страны, которые есть в системе).
 *   Параметры:
 *   cndep – город вылета. Выдается список стран, куда есть вылеты из указанного города
 */
if ($request->get('type') == "country") {
	echo $Tourvisor->getJson('country', $query_list);
}

/* region - список курортов
*  Без параметров выдается полный список курортов (с кодами стран).
*   Параметры:
*   - regcountry – страна, по которой нужно получить список курортов.
 */
if ($request->get('type') == "region") {
	echo $Tourvisor->getJson('region', $query_list);
}

/* region - список курортов
*  Без параметров выдается полный список курортов (с кодами стран).
*   Параметры:
*   - regcountry – страна, по которой нужно получить список субкурортов.
*   - parentregion – родительский регион
 */
if ($request->get('type') == "subregion") {
	echo $Tourvisor->getJson('subregion', $query_list);
}


/* meal - список типов питания
*  Без параметров
 */
if ($request->get('type') == "meal") {
	echo $Tourvisor->getJson('meal', $query_list);
}

/* stars - категорий отелей (звездность)
*  Без параметров
 */
if ($request->get('type') == "stars") {
	echo $Tourvisor->getJson('stars', $query_list);
}

/* operator - список туроператоров
*  Без параметров
 */
if ($request->get('type') == "operator") {
	echo $Tourvisor->getJson('operator', $query_list);
}

/* operator - список дат вылета
	Выдает список доступных дат вылета для выбранного города вылета и страны (используется в календарике).
	Параметры (все обязательны):
	- flydeparture – код города вылета (обязательно!).
	- flycountry – код страны (обязательно!)
 */
if ($request->get('type') == "flydate") {
	echo $Tourvisor->getJson('flydate', $query_list);
}

/* hotel - список отелей
	Выдает список отелей. Обязательно нужно указать хотя бы код страны (hotcountry)
	Параметры:
		- hotcountry – код страны, по которой нужно получить список отелей (ОБЯЗАТЕЛЬНО!)
		- hotregion – код курорта (можно несколько через запятую)
		- hotstars - звездность (и выше)
		- hotrating - рейтинг отеля (выше или равно которому будет отображаться)
		- hotactive - только отели "Активный"
		- hotrelax - только отели "Спокойный"
		- hotfamily - только отели "Семейный"
		- hothealth - только отели "Здоровье"
		- hotcity - только отели "Городской"
		- hotbeach - только отели "Пляжный"
		- hotdeluxe - только отели "Люкс (VIP)"
 */
if ($request->get('type') == "hotel") {
	echo $Tourvisor->getJson('hotel', $query_list);
}

/**
 * Получаем данные по отелю
 *
 * @param hotelcode - код отеля
 */
if ($request->get('type') == "fullhotel") {
	echo $Tourvisor->getJson('fullhotel', $query_list);
}

/*
 * Отправляем запрос на поиск тура
 * Возварщается параметр requestid - ID запроса
 * */
if ($request->get('type') == "search") {
	$query_list = array_diff($query_list, array('')); //Убираем пустые элементы массива


	if ($query_list['datefrom'] == date("d.m.Y", strtotime("+1 day"))) {
		unset($query_list['datefrom']);
	}
	if (strtotime($query_list['datefrom']) < strtotime(date("d.m.Y", strtotime("+1 day")))) { //Если дата от меньше чем завтрашняя, тогда прменяем завтрашнюю
		unset($query_list['datefrom']);
	}
	if (strtotime($query_list['dateto']) < strtotime(date("d.m.Y", strtotime("+1 day")))) { //Если дата от меньше чем завтрашняя, тогда прменяем +14 дней
		$query_list['dateto'] = date("d.m.Y", strtotime("+14 day"));
	}

	if ($query_list['adults'] == 2) {
		unset($query_list['adults']);
	}
	if ($query_list['child'] == 0) {
		unset($query_list['child']);
	}
	if ($query_list['child'] == 1 && ! isset($query_list['childage1'])) {
		$query_list['childage1'] = 2;
	}
	if ($query_list['child'] == 2 && ! isset($query_list['childage1'])) {
		$query_list['childage1'] = 2;
	} elseif ($query_list['child'] == 2 && ! isset($query_list['childage2'])) {
		$query_list['childage2'] = 2;
	}
	if ($query_list['child'] == 3 && ! isset($query_list['childage1'])) {
		$query_list['childage1'] = 2;
	} elseif ($query_list['child'] == 3 && ! isset($query_list['childage2'])) {
		$query_list['childage2'] = 2;
	} elseif ($query_list['child'] == 3 && ! isset($query_list['childage3'])) {
		$query_list['childage3'] = 2;
	}

	if ($query_list['childage1'] == 0) {
		unset($query_list['childage1']);
	}
	if ($query_list['childage2'] == 0) {
		unset($query_list['childage2']);
	}
	if ($query_list['childage3'] == 0) {
		unset($query_list['childage3']);
	}
	if ($query_list['pricefrom'] == 0) {
		unset($query_list['pricefrom']);
	}
	if ($query_list['priceto'] == 1000000) {
		unset($query_list['priceto']);
	}
	if ($query_list['nightsfrom'] == 7) {
		unset($query_list['nightsfrom']);
	}
	if ($query_list['nightsto'] == 10) {
		unset($query_list['nightsto']);
	}

	$arData = $Tourvisor->getData('search', $query_list);


	$query_list['meal']       = $query_list['meal'] ? explode(",", $query_list['meal']) : "";
	$query_list['regions']    = $query_list['regions'] ? explode(",", $query_list['regions']) : "";
	$query_list['stars']      = $query_list['stars'] ? explode(",", $query_list['stars']) : "";
	$query_list['operators']  = $query_list['operators'] ? explode(",", $query_list['operators']) : "";
	$query_list['hotels']     = $query_list['hotels'] ? explode(",", $query_list['hotels']) : "";
	$query_list['hoteltypes'] = $query_list['hoteltypes'] ? explode(",", $query_list['hoteltypes']) : "";
	$query_list['subregions'] = $query_list['subregions'] ? explode(",", $query_list['subregions']) : "";

	$query_list = array_diff($query_list, array('')); //Убираем пустые элементы массива

	//Если ищем со страницы отеля, тогда убираем ненужные параметры
	if ($request->get('hotelpage')) {
		unset($query_list['hotelcode']);
		unset($query_list['hotels']);
		unset($query_list['country']);
		unset($query_list['hotelpage']);
	}


	$arData['url'] = urldecode(http_build_query($query_list)); //Формируем url для подстановки в адресную строку

	echo json_encode($arData);
}

/*
 * Проверяем статус запроса
 *
 * */
if ($request->get('type') == "status") {
	$request_id = $request->get('requestid');
	if (isset($request_id)) {
		echo $Tourvisor->getJson('status', $query_list);
	}
}
/*
 * Получаем результат поиска
 *
 * */
if ($request->get('type') == "result") {
	$request_id = $request->get('requestid');
	if (isset($request_id)) {
		echo $Tourvisor->getJson('result', $query_list);
	}
}

/*
 * Актуализация тура
 *
 * */
if ($request->get('type') == "actualize") {
	$tour_id = $request->get('tourid');
	if (isset($tour_id)) {
		echo $Tourvisor->getJson('actualize', $query_list);
	}
}

/*
 * Детальная актуализация
 *
 * */
if ($request->get('type') == "actdetail") {
	$tour_id = $request->get('tourid');
	if (isset($tour_id)) {
		echo $Tourvisor->getJson('actdetail', $query_list);
	}
}


/*=========================================  Заказ в 1 Клик  ===================================================*/

/*
 * Заказ в 1 клик
 *
 * */
if ($request->get('type') == "fastorder") {
	$tour_id = $request->get('tourid');
	$phone   = $request->get('phone');
	$name    = $request->get('name');
	$credit  = $request->get('credit');
	if (isset($tour_id) && isset($phone)) {
		echo FastOrder::setResult($tour_id, $phone, $name, $credit);
	}
}


/*=========================================  Запрос кредита  ===================================================*/

/**
 * Отправляем письмо о запросе помощи по оформлению кредита
 */
if ($request->get('type') == "credit_request_callback") {
	$arEventFields = array(
		"ORDER_ID"       => $request->get('ORDER_ID'),
		"CONTACT_PERSON" => $request->get('CONTACT_PERSON'),
		"PHONE"          => $request->get('PHONE'),
		"EMAIL"          => $request->get('EMAIL'),

	);
	CEvent::Send("SEND_CREDIT_CALLBACK", 's1', $arEventFields);

	echo 'success';
}

/**
 * Отправляем письмо о запросе помощи по оформлению кредита для кредит европа банк
 */
if ($request->getPost('type') == "ceb_request_callback") {

	$arFiles = [];

	if ($request->getFile('FILE_1')) {
		move_uploaded_file($request->getFile('FILE_1')['tmp_name'], $_SERVER["DOCUMENT_ROOT"].'/upload/tmp/' . $request->getFile('FILE_1')['name']);
		if (file_exists($_SERVER["DOCUMENT_ROOT"].'/upload/tmp/' . $request->getFile('FILE_1')['name'])) {
			$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].'/upload/tmp/' . $request->getFile('FILE_1')['name']);
			$arFile["MODULE_ID"] = "newtravel.search";
			$fileSave = CFile::SaveFile($arFile,'ceb_credit_files');
			$arFiles[] = $fileSave;
			unlink($_SERVER["DOCUMENT_ROOT"].'/upload/tmp/' . $request->getFile('FILE_1')['name']);
		}
		else {
			echo 'copy error';
		}
	}
	if ($request->getFile('FILE_2')) {
		move_uploaded_file($request->getFile('FILE_2')['tmp_name'], $_SERVER["DOCUMENT_ROOT"].'/upload/tmp/' . $request->getFile('FILE_2')['name']);
		if (file_exists($_SERVER["DOCUMENT_ROOT"].'/upload/tmp/' . $request->getFile('FILE_2')['name'])) {
			$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].'/upload/tmp/' . $request->getFile('FILE_2')['name']);
			$arFile["MODULE_ID"] = "newtravel.search";
			$fileSave = CFile::SaveFile($arFile,'ceb_credit_files');
			$arFiles[] = $fileSave;
			unlink($_SERVER["DOCUMENT_ROOT"].'/upload/tmp/' . $request->getFile('FILE_2')['name']);
		}
	}

	$arEventFields = array(
		"ORDER_ID"      => $request->getPost('ORDER_ID'),
		"FIO"           => $request->getPost('FIO'),
		"PHONE"         => $request->getPost('PHONE'),
		"PHONE_2"       => $request->getPost('PHONE_2'),
		"PHONE_3"       => $request->getPost('PHONE_3'),
		"PHONE_4"       => $request->getPost('PHONE_4'),
		"PRICE"         => $request->getPost('PRICE'),
		"INITIAL_FEE"   => $request->getPost('INITIAL_FEE'),
		"CREDIT_PERIOD" => $request->getPost('CREDIT_PERIOD')
	);

	AddMessage2Log(\Bitrix\Main\Web\Json::encode($_FILES));

	CEvent::Send("SEND_CREDIT_CEB_CALLBACK", 's1', $arEventFields, "Y", "", $arFiles);

	echo 'success';
}


/*=========================================  Корзина туров  ===================================================*/

/**
 * Добавление в корзину туров
 */
if ($request->get('type') == "addToToursBasket") {
	$tour_id = $request->get('tourid');
	echo \Newtravel\Search\ToursBasket::addToBasket($tour_id);
}

/**
 * Удалдение из корзины туров
 */
if ($request->get('type') == "removeFromToursBasket") {
	$tour_id = $request->get('tourid');

	echo \Newtravel\Search\ToursBasket::removeFromBasket($tour_id);
}

/**
 * Возвращает количество туров
 */
if ($request->get('type') == "basketCount") {
	echo \Newtravel\Search\ToursBasket::getBasketCount();
}

/**
 * Добавляет комментарий менеджера к корзине
 */
if ($request->get('type') == "addBasketComment") {
	$bid     = $request->get('basket_id');
	$comment = $request->get('comment');
	echo \Newtravel\Search\ToursBasket::addBasketComment($bid, $comment);
}

/**
 * Добавляет корзину менедежера
 */
if ($request->get('type') == "addManagerBasket") {
	echo \Newtravel\Search\ToursBasket::addManagerBasket();
}


/*=========================================  ГОТОВЫЕ ПРЕДЛОЖЕНИЯ ===================================================*/

/*
 * Получаем md5 хэш из JSON строки
 * */
if ($request->get('type') == "getHash") {
	if (strlen($request->get('hashString')) > 0) {
		echo md5($request->get('hashString'));
	}
}

/*
 * Проверяем предложения в кэше
 * */
if ($request->get('type') == "checkCache") {
	if (strlen($request->get('hash')) > 0) {
		$cacheTime = 18000;
		$cacheId   = $request->get('hash');


		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		if ($cache->read($cacheTime, $cacheId)) {
			$result = json_encode($cache->get($cacheId));  // get our data
		} else {
			$result = "fail";
		}
		echo $result;
	}
}

/*
 * Выдаем предложения из кэше
 * */
if ($request->getPost('type') == "setCache") {
	if (strlen($request->getPost('hash')) > 0) {

		$cacheTime = 18000;
		$cacheId   = $request->get('hash');

		$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();

		$result = "N";
		if ( ! $cache->read($cacheTime, $cacheId)) {
			$data = $request->getPost('data');
			$cache->set($cacheId, $data);  // set our cache
			$result = "Y";
		}

		echo $result;
	}
}


// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>