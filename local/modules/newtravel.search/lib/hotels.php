<?php
namespace Newtravel\Search;

/**
 * Class Hotels - реализует работу с отелями от турвизора
 *
 * @package Newtravel\Search
 */
use CFile;
use CIBlockElement;
use CIBlockSection;
use Newtravel\Search\Tourvisor;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;


class Hotels {

	/**
	 * ID Инфоблока Страны
	 */
	const COUNTRIES_IBLOCK_ID = 58;

	/**
	 * Получаем отель от Турвизора и сохраняем его в базу
	 *
	 * @param int $hotelId
	 *
	 * @return bool
	 */
	public static function saveHotel($hotelId = 0) {

		if ($hotelId !== 0) {
			$tv                = new Tourvisor();
			$hotel             = $tv->getData('fullhotel', array("hotelcode" => $hotelId));
			$hotelData         = $hotel['data']['hotel'];
			$countryId         = $hotelData['countrycode'];
			$regionId          = $hotelData['regioncode'];
			$hotelData['code'] = $hotelId;

			if ($hotelData) {
				$sectionId = self::checkSectionId($regionId, $countryId);
				if ($sectionId) {
					if ( ! self::checkHotelInDB($hotelId)) {
						return  self::addNewHotel($sectionId, $hotelData);
					} else {
						return false;
					}
				}
			} else {
				echo "отель не найден";

				return false;
			}
		}

		return false;
	}

	/**
	 * Ищем ID раздела(страны) по коду региона, если такого региона нет, то по коду страны
	 *
	 * @param int $regionID
	 * @param int $countryID
	 *
	 * @return bool
	 */
	private static function checkSectionId($regionID = 0, $countryID = 0) {
		Loader::includeModule('iblock');
		$uf_region = CIBlockSection::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => self::COUNTRIES_IBLOCK_ID, "UF_ID_REGION" => $regionID), false, array('ID', 'NAME'));
		if ($arRegion = $uf_region->GetNext()) {
			return $arRegion['ID'];
		} else {
			$uf_country = CIBlockSection::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => self::COUNTRIES_IBLOCK_ID, "UF_TOURVISOR_COUNTRY" => $countryID), false, array('ID', 'NAME'));
			if ($arCountry = $uf_country->GetNext()) {
				return $arCountry['ID'];
			}
		}

		return false;
	}

	/**
	 * Добавляем новый отель в базу
	 *
	 * @param int $sectionID
	 * @param array $hotelData
	 */
	private static function addNewHotel($sectionID = 0, $hotelData = array()) {
		if ($sectionID) {
			Loader::includeModule('iblock');
			$el = new CIBlockElement;

			$territory   = explode(";", str_replace('; ', ';', $hotelData['territory']));
			$inroom      = explode(";", str_replace('; ', ';', $hotelData['inroom']));
			$roomtypes   = explode(";", str_replace('; ', ';', $hotelData['roomtypes']));
			$services    = explode(";", str_replace('; ', ';', $hotelData['services']));
			$servicefree = explode(";", str_replace('; ', ';', $hotelData['servicefree']));
			$servicepay  = explode(";", str_replace('; ', ';', $hotelData['servicepay']));
			$child       = explode(";", str_replace('; ', ';', $hotelData['child']));
			$meallist    = explode(";", str_replace('; ', ';', $hotelData['meallist']));

			$arPhoto = array();
			foreach ($hotelData['images']['image'] as $image) {
				$arPhoto[] = CFile::MakeFileArray('http:' . $image);
			}

			$PROP = array(
				"STARS"       => $hotelData['stars'],
				"REGION"      => $hotelData['region'],
				"COUNTRY"     => $hotelData['country'],
				"RATING"      => $hotelData['rating'],
				"PLACEMENT"   => $hotelData['placement'],
				"BUILD"       => $hotelData['build'],
				"PHONE"       => $hotelData['phone'],
				"SITE"        => $hotelData['site'],
				"TERRITORY"   => $territory,
				"IN_ROOM"     => $inroom,
				"ROOMTYPES"   => $roomtypes,
				"SERVICES"    => $services,
				"SERVICEFREE" => $servicefree,
				"SERVICEPAY"  => $servicepay,
				"CHILD"       => $child,
				"BEACH"       => $hotelData['beach'],
				"MEALLIST"    => $meallist,
				"LAT"         => $hotelData['coord1'],
				"LONG"        => $hotelData['coord2'],
				'MORE_PHOTO'  => $arPhoto,
				'COUNTRYCODE'  => $hotelData['countrycode'],
				'REGIONCODE'  => $hotelData['regioncode']
			);

			$arLoadProductArray = Array(
				"MODIFIED_BY"       => "1", // элемент изменен текущим пользователем
				"CODE"              => $hotelData['code'],
				"IBLOCK_SECTION_ID" => $sectionID,// элемент лежит в корне раздела
				"IBLOCK_ID"         => self::COUNTRIES_IBLOCK_ID,
				"PROPERTY_VALUES"   => $PROP,
				"NAME"              => $hotelData['name'],
				"ACTIVE"            => "Y",            // активен
				"PREVIEW_TEXT"      => $hotelData['description'],
				"DETAIL_PICTURE"    => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . "/image.gif")
			);

			if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
				echo true;
			}
		}
		return false;
	}

	/**
	 * Проверяем существование отеля в базе по его КОДУ
	 *
	 * @param int $hotelCode
	 *
	 * @return bool
	 */
	public static function checkHotelInDB($hotelCode = 0) {
		Loader::includeModule('iblock');
		if ($hotelCode) {
			$arSelect = Array("ID", "NAME");
			$arFilter = Array("IBLOCK_ID" => self::COUNTRIES_IBLOCK_ID, "CODE" => $hotelCode, "ACTIVE" => "Y");
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), $arSelect);
			if ($arFields = $res->GetNext()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Получение отеля по коду
	 *
	 * @param int $hotelCode
	 *
	 * @return array
	 */
	public static function getHotelInDB($hotelCode = 0) {
		Loader::includeModule('iblock');
		$arData = array();

		if ($hotelCode) {
			$arSelect = Array("*");
			$arFilter = Array("IBLOCK_ID" => self::COUNTRIES_IBLOCK_ID, "CODE" => $hotelCode, "ACTIVE" => "Y");
			$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), $arSelect);
			if ($ob = $res->GetNextElement()) {
				$arData = $ob->GetFields();
				$arData['PROPS'] = $ob->GetProperties();
				return $arData;
			}
		}

		return false;
	}

}