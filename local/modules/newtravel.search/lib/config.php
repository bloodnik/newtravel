<?php
$config = array(
	"HL_ID" => array(
		'Departures' => 1,
		'Countries'  => 2,
		'Meals'      => 3,
		'Regions'    => 4,
		'Operators'  => 5,
		'Subregions' => 6,
	)
);