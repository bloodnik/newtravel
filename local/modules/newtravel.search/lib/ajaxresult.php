<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();

$template = $request->get("template") ?: ".default";

// Подключение компонента вывода результата
$APPLICATION->IncludeComponent("newtravel.search:search.result", $template, array(
	"REQUEST_ID"      => $request["requestid"],
	"IS_FINISH"       => $request["isfinish"], //Завершен ли поиск
	"PAGE"            => $request["page"], //Завершен ли поиск
	"TOURS_IBLOCK_ID" => 79, //Инфоблок туров
),
	false
);

// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>