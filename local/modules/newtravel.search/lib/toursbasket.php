<?php

namespace Newtravel\Search;

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;
use Bitrix\Main\Entity\Query;
use Bitrix\Main\Loader;
use Bitrix\Main\Type\Date;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Web\Cookie;

/**
 * Class ToursBasket - реализует работу корзины туров
 *
 * @package Newtravel\Search
 */
class ToursBasket {

	/**
	 * @param string $tourId - ID тура
	 *
	 * Добавляем тур в корзину
	 *
	 * @return string - success or fail
	 *
	 */
	public static function addToBasket($tourId = "") {
		global $USER;

		$result = false;

		$isManager = in_array(8, explode(",", $USER->GetGroups()));

		//Сначала проверяем на уже существование корзины и добавлем к ней новый ID тура
		$toursId = array();
		if ($USER->IsAuthorized() && ! $isManager) {
			if ( ! empty($basket = self::getUserBasket($USER->GetID()))) {
				if ( ! empty($basket[0]['UF_TOURS'])) {
					$toursId = json_decode(base64_decode($basket[0]['UF_TOURS']), true);
				}
			}
		} else {
			$toursIdString = Application::getInstance()->getContext()->getRequest()->getCookie("TOURS_BASKET");
			if (isset($toursIdString) && strlen($toursIdString) > 0) {
				$toursId = json_decode(base64_decode($toursIdString), true);
			}
		}

		$toursId[]           = $tourId;
		$tourIdsStringBase64 = base64_encode(json_encode(array_unique($toursId)));

		if ($USER->IsAuthorized() && ! $isManager) {
			$USER_ID = $USER->GetID();
			if ( ! empty($basket = self::getUserBasket($USER_ID))) {
				$basketId = $basket[0]['ID'];

				$data = array(
					'UF_TOURS' => $tourIdsStringBase64,
					'UF_DATE'  => new DateTime()
				);

				$result = self::updateHL($basketId, $data);
			} else {
				$data   = array(
					'UF_TOURS' => $tourIdsStringBase64,
					'UF_USER'  => $USER_ID,
				);
				$result = self::addToHL($data);
			}
		} else {
			$cookie = new Cookie('TOURS_BASKET', $tourIdsStringBase64);
			Application::getInstance()->getContext()->getResponse()->addCookie($cookie);

			$result = true;
		}

		return $result ? "success" : "fail";
	}

	/**
	 * @param $tourId - ID удаляемого тура
	 *
	 * Удаляем тур из корзины
	 *
	 */
	public static function removeFromBasket($tourId) {
		global $USER;

		$isManager = in_array(8, explode(",", $USER->GetGroups()));

		$result  = false;
		$toursId = array();
		if ( ! empty($tourId)) {
			//Сначала проверяем на уже существование корзины и удаляем там ID тура
			if ($USER->IsAuthorized() && !$isManager) {
				if ( ! empty($basket = self::getUserBasket($USER->GetID()))) {
					if ( ! empty($basket[0]['UF_TOURS'])) {
						$toursId = json_decode(base64_decode($basket[0]['UF_TOURS']), true);
					}
				}
			} else {
				$toursIdString = Application::getInstance()->getContext()->getRequest()->getCookie("TOURS_BASKET");
				if (isset($toursIdString) && strlen($toursIdString) > 0) {
					$toursId = json_decode(base64_decode($toursIdString), true);
				}
			}
		}

		if ($findedTour = array_search($tourId, $toursId)) {
			unset($toursId[ $findedTour ]);
		} elseif ($toursId[0] === $tourId) {
			unset($toursId[0]);
		}

		$tourIdsStringBase64 = base64_encode(json_encode(array_unique($toursId)));

		if ($USER->IsAuthorized() && !$isManager) {
			$USER_ID = $USER->GetID();
			if ( ! empty($basket = self::getUserBasket($USER_ID))) {
				$basketId = $basket[0]['ID'];

				$data = array(
					'UF_TOURS' => $tourIdsStringBase64,
					'UF_DATE'  => new DateTime()
				);

				$result = self::updateHL($basketId, $data);
			} else {
				$data   = array(
					'UF_TOURS' => $tourIdsStringBase64,
					'UF_USER'  => $USER_ID,
				);
				$result = self::addToHL($data);
			}
		} else {
			$cookie = new Cookie('TOURS_BASKET', $tourIdsStringBase64);
			Application::getInstance()->getContext()->getResponse()->addCookie($cookie);

			$result = true;
		}

		return $result ? "success" : "fail";
	}

	/**
	 * @param $USER_ID
	 *
	 * Получание корзины по ID пользователя
	 *
	 * @return array
	 */
	public static function getUserBasket($USER_ID) {
		Loader::includeModule("highloadblock");
		$hldata   = HighloadBlockTable::getById(7)->fetch();
		$hlentity = HighloadBlockTable::compileEntity($hldata);
		$query    = new Query($hlentity);

		return $query->setSelect(array('*'))->setFilter(array('UF_USER' => $USER_ID))->exec()->fetchAll();
	}

	public static function getBasketByUserId($USER_ID) {
		Loader::includeModule("highloadblock");
		$hldata   = HighloadBlockTable::getById(7)->fetch();
		$hlentity = HighloadBlockTable::compileEntity($hldata);
		$query    = new Query($hlentity);
		$result   = $query->setSelect(array('*'))->setFilter(array('UF_USER' => $USER_ID))->exec()->fetchAll();

		return $result[0];
	}

	/**
	 * @param string $basketId
	 *
	 * Получение корзины по ID
	 *
	 * @return mixed
	 */
	public static function getBasketById($basketId = "") {
		$hldata   = HighloadBlockTable::getById(7)->fetch();
		$hlentity = HighloadBlockTable::compileEntity($hldata);
		$query    = new Query($hlentity);
		$hlresult = $query->setSelect(array('*'))->setFilter(array('ID' => $basketId))->exec()->fetchAll();

		return $hlresult[0];
	}

	/**
	 * Добавляет коризну из куки. Функционал для менеджеров. Для создания подборки
	 *
	 * @return bool|int|string
	 */
	public static function addManagerBasket() {
		global $USER;

		$result = false;

		$request    = Application::getInstance()->getContext()->getRequest();
		$tourBasket = $request->getCookie('TOURS_BASKET');
		if (isset($tourBasket) && strlen($tourBasket) > 0) {
			$data   = array(
				'UF_TOURS' => $tourBasket,
				'UF_USER'  => $USER->GetID(),
				'UF_DATE'  => new DateTime()
			);
			$result = self::addToHL($data);
		}

		return $result ? $result : 'fail';
	}


	/**
	 * @param $data - данные array()
	 *
	 * Добавлет новую записть в базу данных
	 *
	 * @return bool|int
	 */
	private function addToHL($data) {
		Loader::includeModule("highloadblock");
		$hldata    = HighloadBlockTable::getById(7)->fetch();
		$hlentity  = HighloadBlockTable::compileEntity($hldata);
		$dataClass = $hlentity->getDataClass();

		//Добавляем в HL блок
		$obresult = $dataClass::add($data);
		if ($obresult->isSuccess()) {
			return $obresult->getId();
		} else {
			return false;
		}
	}

	/**
	 * @param $basketId - id корзины
	 * @param $data - данные array()
	 *
	 * Обновляет корзниу
	 *
	 * @return bool
	 */
	private function updateHL($basketId, $data) {
		Loader::includeModule("highloadblock");
		$hldata    = HighloadBlockTable::getById(7)->fetch();
		$hlentity  = HighloadBlockTable::compileEntity($hldata);
		$dataClass = $hlentity->getDataClass();

		//Добавляем в HL блок
		$obresult = $dataClass::update($basketId, $data);
		if ($obresult->isSuccess()) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * Возвращает количество туров в корзине
	 *
	 * @return int
	 */
	public static function getBasketCount() {
		global $USER;

		$request = Application::getInstance()->getContext()->getRequest();

		$isManager = in_array(8, explode(",", $USER->GetGroups()));

		$arToursId = array();

		//Если пользователь не авторизован, то берем его корзину из cookie
		if ( ! $USER->IsAuthorized() || $isManager) {
			$tourBasket = $request->getCookie('TOURS_BASKET');
			if (isset($tourBasket) && strlen($tourBasket) > 0) {
				return count(json_decode(base64_decode($tourBasket), true));
			} else {
				return 0;
			}
		} else { //Загружаем его корзину из БД - HL инфоблока
			$arTours   = self::getBasketByUserId($USER->GetID());
			$arToursId = json_decode(base64_decode($arTours['UF_TOURS']), true);

			return count($arToursId);
		}
	}

	/**
	 * @param int $bid
	 * @param string $comment
	 *
	 * Добавляем комментарий к корзине
	 *
	 * @return string
	 */
	public static function addBasketComment($bid = 0, $comment = "") {
		$result = false;
		global $USER;

		$access = $USER->IsAdmin() || in_array(8, explode(",", $USER->GetGroups()));

		if ($access && $bid !== 0 && $comment !== "") {
			$data   = array(
				'UF_COMMENT' => htmlspecialcharsbx($comment),
			);
			$result = self::updateHL($bid, $data);
		}

		return $result ? "success" : "fail";
	}


}