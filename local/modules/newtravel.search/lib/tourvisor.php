<?php
namespace Newtravel\Search;
/**
 * Class Tourvisor - реализует работу с xml шлюзом от турвизора
 *
 * @package Newtravel\Search
 */
class Tourvisor {

	private $login = "lili";
	private $pass = "271000";

	/**
	 * Подготоваливаем url c параметрами
	 * @param string $type - тип запроса к турвизору
	 * @param array $params - массив передаваемых параметров
	 *
	 * @return string
	 */
	private function prepareUrl ($type, $params) {
		//Убираем пустые элементы из массива
		$params = array_diff($params, array(''));
		$params['type'] = $type;
		$params['authlogin'] = $this->login;
		$params['authpass'] = $this->pass;
		$param_string = http_build_query($params);

		$url = "http://tourvisor.ru/xml/list.php?format=json"; //справочники

		if($type === 'fullhotel') { //описание отеля
			$url = "http://tourvisor.ru/xml/hotel.php?format=json&removetags=1";
		}elseif($type === "search" ) { //запрос на поиск
			$url = "http://tourvisor.ru/xml/search.php?format=json";
		}elseif($type === "status" || $type === "result") { //получение результата поиска
			$url = "http://tourvisor.ru/xml/result.php?format=json";
		}elseif ($type === "actualize"){ // актуализация тура
			$url = "http://tourvisor.ru/xml/actualize.php?format=json";
		}elseif ($type === "actdetail"){ // детальная актуализация тура
			$url = "http://tourvisor.ru/xml/actdetail.php?format=json";
		}

		return $url. "&" . $param_string;
	}

	/**
	 * Делаем запрос к справочникам турвизора.
	 * @param string $type - тип запроса к турвизору
	 * @param array $params - массив передаваемых параметров
	 *
	 * @return array - возращаем массив с данными
	 */
	public function getData($type = "", $params = array()) {
		return json_decode(file_get_contents($this->prepareUrl($type, $params)), true);
	}
	
	/**
	 * Делаем запрос к справочникам турвизора.
	 * @param string $type - тип запроса к турвизору
	 * @param array $params - массив передаваемых параметров
	 *
	 * @return array - возращаем JSON строку
	 */
	public function getJson($type = "", $params = array()) {
		return file_get_contents($this->prepareUrl($type, $params));
	}

	
}