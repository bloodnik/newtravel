<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Newtravel\Search\Tourvisor;
use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Bitrix\Sale;

Loader::includeModule("iblock");
Loader::includeModule("catalog");
Loader::includeModule("sale");
Loader::includeModule("newtravel.search");

$request = Application::getInstance()->getContext()->getRequest();

$tv = new Tourvisor();

// Покупка тура
$buy    = $request->get("buy");
$tourId = $request->get("tourid");

//Перешли по ссылке со скидкой
$useDiscount = (strlen($request->get("usediscount")) > 0 && $request->get("usediscount") === "Y") ? true : false;

if ((isset($buy) && $buy == "Y") && isset($tourId)) {
	$arTour = $tv->getData("actualize", array("tourid" => $tourId, "flight" => 1));

	//Повторно запрашиваем актуализцию, но уже с валютой. TODO удалить, если турвизор добавит валюту в актуализацию с RUB
	$arTourCU = $tv->getData("actualize", array("tourid" => $tourId, "currency"=> 1));

	$arTour = $arTour["data"]["tour"];
	$arTourCU = $arTourCU["data"]["tour"];

	if (is_array($arTour) && ! empty($arTour)) {

		global $USER;

		//Очищаем корзину покупателя
		if (Loader::includeModule('sale')) {
			/* @var $basket \Bitrix\Sale\Basket */
			$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());


			CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());

			//Очищаем корзину
			if ($basket->getBasket()->count() > 0) {
				$basketItems = $basket->getBasketItems();
				/* @var $basketItem \Bitrix\Sale\BasketItem */
				foreach ($basketItems as $basketItem) {
					$basketItem->delete();
				}
				$basket->save();
			}

			// Добавляем тур в базу
			$el                   = new CIBlockElement;
			$PROP                 = $arTour;
			$PROP["USER_ID"]      = $USER->GetID();
			$PROP["tourid"]       = $tourId;
			$PROP["USE_DISCOUNT"] = $useDiscount ? "Y" : "";
			$arLoadProductArray   = Array(
				"MODIFIED_BY"       => $USER->GetID(),
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID"         => 79,
				"PROPERTY_VALUES"   => $PROP,
				"NAME"              => $arTour["departurename"] . "_" . $arTour["hotelregionname"] . "_" . $tourId,
				"tourname"          => $arTour["departurename"] . "_" . $arTour["hotelregionname"] . "_" . $tourId,
				"ACTIVE"            => "Y",
			);

			$PRODUCT_ID = $el->Add($arLoadProductArray);

			// Проставляем количество
			$arFields = array(
				"ID"             => $PRODUCT_ID,
				"QUANTITY"       => 1,
				"QUANTITY_TRACE" => "N"
			);
			CCatalogProduct::Add($arFields);

			// Назначаем цену
			$arFields      = Array(
				"PRODUCT_ID"       => $PRODUCT_ID,
				"CATALOG_GROUP_ID" => 1,
				"PRICE"            => intval($arTour["price"]),
				"CURRENCY"         => "RUB",
				"QUANTITY_FROM"    => false,
				"QUANTITY_TO"      => false
			);
			$PRODUCT_PRICE = CPrice::Add($arFields);

			// Добавляем тур в корзину
			$item = $basket->createItem('catalog', $PRODUCT_ID);

			$item->setFields(array(
				'QUANTITY' => 1,
				'CURRENCY' => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
				'LID' => Bitrix\Main\Context::getCurrent()->getSite(),
				'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
			));

			//Свойства корзины
			$basketPropertyCollection = $item->getPropertyCollection();
			$arProps = array(
				0  => array(
					"NAME"  => "Количество звезд",
					"CODE"  => "STAR_NAME",
					"VALUE" => $arTour["hotelstars"]
				),
				1  => array(
					"NAME"  => "Количество взрослых",
					"CODE"  => "ADULTS",
					"VALUE" => $arTour["adults"]
				),
				2  => array(
					"NAME"  => "Дата вылета",
					"CODE"  => "CHECK_IN_DATE",
					"VALUE" => $arTour["flydate"],
					"SORT"  => "10"
				),
				3  => array(
					"NAME"  => "Ночей",
					"CODE"  => "NIGHTS",
					"VALUE" => $arTour["nights"],
					"SORT"  => "20"
				),
				4  => array(
					"NAME"  => "Страна",
					"CODE"  => "COUNTRY_NAME",
					"VALUE" => $arTour["countryname"]
				),
				5  => array(
					"NAME"  => "Курорт",
					"CODE"  => "RESORT_NAME",
					"VALUE" => $arTour["hotelregionname"],
					"SORT"  => "30"
				),
				6  => array(
					"NAME"  => "Отель",
					"CODE"  => "HOTEL_NAME",
					"VALUE" => $arTour["hotelname"]
				),
				7  => array(
					"NAME"  => "Рейтинг отеля",
					"CODE"  => "HOTEL_RATING",
					"VALUE" => $arTour["hotelrating"]
				),
				8  => array(
					"NAME"  => "Номер",
					"CODE"  => "HT_PLACE_DESCRIPTION",
					"VALUE" => $arTour["room"]
				),
				9  => array(
					"NAME"  => "Питание",
					"CODE"  => "MEAL_DESCRIPTION",
					"VALUE" => $arTour["meal"] . " " . $arTour["mealrussian"]
				),
				10 => array(
					"NAME"  => "Город вылета",
					"CODE"  => "CITY_FROM_NAME",
					"VALUE" => $arTour["departurename"]
				),
				11 => array(
					"NAME"  => "Количество детей",
					"CODE"  => "KIDS",
					"VALUE" => $arTour["child"]
				),
				12 => array(
					"NAME"  => "Размещение",
					"CODE"  => "PLACEMENT",
					"VALUE" => $arTour["placement"]
				),
				13 => array(
					"NAME"  => "TOUR ID",
					"CODE"  => "TOUR_ID",
					"VALUE" => $tourId
				),
				14 => array(
					"NAME"  => "USE_DISCOUNT",
					"CODE"  => "USE_DISCOUNT",
					"VALUE" => $useDiscount ? "Y" : ""
				),
				15 => array(
					"NAME"  => "UE",
					"CODE"  => "UE",
					"VALUE" => $arTourCU["currency"]
				),
				16 => array(
					"NAME"  => "UE_PRICE",
					"CODE"  => "UE_PRICE",
					"VALUE" => $arTourCU["price"]
				),

			);
			if (intval($arTour["child"]) > 0) {
				for ($i = 1; $i <= $arTour["child"]; $i++) {
					$arProps[] = array(
						"NAME"  => "Возраст ребенка " . $i,
						"CODE"  => "CHILDAGE_" . $i,
						"VALUE" => $arTour[ "childage" . $i ]
					);
				}
			}

			$basketPropertyCollection->setProperty($arProps);

			$basket->save();
			$basketPropertyCollection->save();

			echo "success";
		} else {
			echo "error";
		}
	}
}


// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>