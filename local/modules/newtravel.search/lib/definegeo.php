<?php
namespace Newtravel\Search;

/**
 * Class DefineGeo - реализует работу определения города и региона по IP пользователя SxGeo
 *
 * @package Newtravel\Search
 */
class DefineGeo {

	/**
	 * DefineGeo constructor.
	 */
	public static function setRegionIso() {
		mb_internal_encoding("cp1251");
		$SxGeo    = new SxGeo($_SERVER['DOCUMENT_ROOT'] . "/local/modules/newtravel.search/SxGeoCity.dat", SXGEO_BATCH | SXGEO_MEMORY);
		$ip       = $_SERVER["REMOTE_ADDR"];
		$cityFull = $SxGeo->getCityFull($ip);
		unset($SxGeo);
		$regionIso = $cityFull['region']['iso'] && $cityFull['country']['iso'] == 'RU' ? $cityFull['region']['iso'] : "RU-MOS";

		$_SESSION["REGION_ISO"] = $regionIso;
	}

	public static function getCity() {
		$cityInfo         = TourvisorHlLists::getList('departures', array("*"), array("UF_ISO" => $_SESSION["REGION_ISO"]));
		$_SESSION["CITY"] = $cityInfo[0];

		return $cityInfo[0];
	}

	public static function setCity() {
		$cityInfo         = TourvisorHlLists::getList('departures', array("*"), array("UF_ISO" => $_SESSION["REGION_ISO"]));
		if(empty($cityInfo[0]['UF_PHONE'])){
			$cityInfo[0]['UF_PHONE'] = '8-800-505-47-61' ;
		}
		$_SESSION["CITY"] = $cityInfo[0];
	}


}