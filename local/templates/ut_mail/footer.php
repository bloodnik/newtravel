
	</td>
</tr>
</tbody>
</table>
 <!-- DIVIDER -->
<table cellpadding="0" cellspacing="0" width="660" align="center" style="table-layout: fixed;">
<tbody>
<tr>
	<td bgcolor="#ffffff" style="border-top: 1px dashed #dddddd;">
	</td>
</tr>
</tbody>
</table>
 <!-- FOOTER -->
<table cellpadding="0" cellspacing="0" width="660" align="center" style="table-layout: fixed;">
<tbody>
<tr>
	<td bgcolor="#ffffff" align="center">
		<table width="100%" cellspacing="0" cellpadding="0" align="center">
		<tbody>
		<tr>
			<td style="padding: 20px 0px;">
				<table width="100%" cellpadding="0" cellspacing="0" align="center">
				<tbody>
				<tr>
					<td style="padding: 0 0 20px 0;">
						<table cellpadding="0" cellspacing="0" align="center">
						<tbody>
						<tr>
							<td>
								<table align="left" cellpadding="0" cellspacing="0">
								<tbody>
								<tr>
									<td valign="top" style="padding: 0 10px 0 0;">
										<table width="100%" cellpadding="0" cellspacing="0">
										<tbody>
										<tr>
											<td align="left" valign="middle" style="padding: 5px 10px 5px 9px;">
												<table align="left" cellpadding="0" cellspacing="0">
												<tbody>
												<tr>
													<td align="center" valign="middle" width="24">
 <a href="https://vk.com/umnie_turisti" target="_blank"><img width="24" src="http://newtravel.su/images/email/color-vk-48.png" height="24" style="display: block; height: auto;"></a>
													</td>
													<td align="left" valign="middle" style="padding: 0 0 0 5px; line-height: 100%;">
 <a href="https://vk.com/umnie_turisti" target="_blank" style="color: #666666; font-size: 12px; text-decoration: none; word-wrap: break-word !important;">Вконтакте</a>
													</td>
												</tr>
												</tbody>
												</table>
											</td>
										</tr>
										</tbody>
										</table>
									</td>
								</tr>
								</tbody>
								</table>
								<table align="left" cellpadding="0" cellspacing="0">
								<tbody>
								<tr>
									<td valign="top" style="padding: 0 10px 0 0;">
										<table width="100%" cellpadding="0" cellspacing="0">
										<tbody>
										<tr>
											<td align="left" valign="middle" style="padding: 5px 10px 5px 9px;">
												<table align="left" cellpadding="0" cellspacing="0">
												<tbody>
												<tr>
													<td align="center" valign="middle" width="24">
 <a href="https://ok.ru/group/52199923187928/" target="_blank"><img width="24" src="http://newtravel.su/images/email/color-ok-48.png" height="24" style="display: block; height: auto;"></a>
													</td>
													<td align="left" valign="middle" style="padding: 0 0 0 5px; line-height: 100%;">
 <a href="https://ok.ru/group/52199923187928/" target="_blank" style="color: #666666; font-size: 12px; text-decoration: none; word-wrap: break-word !important;">Одноклассники</a>
													</td>
												</tr>
												</tbody>
												</table>
											</td>
										</tr>
										</tbody>
										</table>
									</td>
								</tr>
								</tbody>
								</table>
								<table align="left" cellpadding="0" cellspacing="0">
								<tbody>
								<tr>
									<td valign="top" style="padding: 0 10px 0 0;">
										<table width="100%" cellpadding="0" cellspacing="0">
										<tbody>
										<tr>
											<td align="left" valign="middle" style="padding: 5px 10px 5px 9px;">
												<table align="left" cellpadding="0" cellspacing="0">
												<tbody>
												<tr>
													<td align="center" valign="middle" width="24">
 <a href="https://www.facebook.com/pages/%D0%A3%D0%BC%D0%BD%D1%8B%D0%B5%D0%A2%D1%83%D1%80%D0%B8%D1%81%D1%82%D1%8B%D1%80%D1%84/495696677240397" target="_blank"><img width="24" src="http://newtravel.su/images/email/color-fb-48.png" height="24" style="display: block; height: auto;"></a>
													</td>
													<td align="left" valign="middle" style="padding: 0 0 0 5px; line-height: 100%;">
 <a href="https://www.facebook.com/pages/%D0%A3%D0%BC%D0%BD%D1%8B%D0%B5%D0%A2%D1%83%D1%80%D0%B8%D1%81%D1%82%D1%8B%D1%80%D1%84/495696677240397" target="_blank" style="color: #666666; font-size: 12px; text-decoration: none; word-wrap: break-word !important;">Facebook</a>
													</td>
												</tr>
												</tbody>
												</table>
											</td>
										</tr>
										</tbody>
										</table>
									</td>
								</tr>
								</tbody>
								</table>
								<table align="left" cellpadding="0" cellspacing="0">
								<tbody>
								<tr>
									<td valign="top" style="padding: 0 10px 0 0;">
										<table width="100%" cellpadding="0" cellspacing="0">
										<tbody>
										<tr>
											<td align="left" valign="middle" style="padding: 5px 10px 5px 9px;">
												<table align="left" cellpadding="0" cellspacing="0">
												<tbody>
												<tr>
													<td align="center" valign="middle" width="24">
 <a href="https://www.instagram.com/umnye_turisty/" target="_blank"><img width="24" src="http://newtravel.su/images/email/color-ig-48.png" height="24" style="display: block; height: auto;"></a>
													</td>
													<td align="left" valign="middle" style="padding: 0 0 0 5px; line-height: 100%;">
 <a href="https://www.instagram.com/umnye_turisty/" target="_blank" style="color: #666666; font-size: 12px; text-decoration: none; word-wrap: break-word !important;">Instagram</a>
													</td>
												</tr>
												</tbody>
												</table>
											</td>
										</tr>
										</tbody>
										</table>
									</td>
								</tr>
								</tbody>
								</table>
							</td>
						</tr>
						</tbody>
						</table>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>