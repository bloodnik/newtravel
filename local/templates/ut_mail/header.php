<style type="text/css">
    /* CLIENT-SPECIFIC STYLES */
    #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
    .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
    body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

    /* RESET STYLES */
    body{margin:0; padding:0;}
    img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
    table{border-collapse:collapse !important;}
    body{height:100% !important; margin:0; padding:0; width:100% !important;}

	a {color: #00aeef; text-decoration: none;}

    /* iOS BLUE LINKS */
    .appleBody a {color:#00aeef; text-decoration: none;}
    .appleFooter a {color:#666666; text-decoration: none;}
</style> <!-- HEADER -->
<table cellpadding="0" cellspacing="0" width="660" height="106" align="center" style="background: url('http://newtravel.su/images/email/header.jpg'); table-layout: fixed;">
<tbody>
<tr>
	<td>
		<div align="center" style="padding: 0 30px;">
			 <!-- LOGO/PREHEADER TEXT -->
			<table cellpadding="0" cellspacing="0" width="100%">
			<tbody>
			<tr>
				<td style="padding: 20px 0px">
					<table cellpadding="0" cellspacing="0" width="100%">
					<tbody>
					<tr>
						<td align="left">
 <a href="http://newtravel.su" target="_blank"><img width="204" alt="logo" src="http://newtravel.su/images/email/logo-w.png" height="66" style="display: block;" border="0"></a>
						</td>
						<td align="right" width="100%">
							<table cellpadding="0" cellspacing="0">
							<tbody>
							<tr>
								<td style="text-decoration: none;">
 <a href="https://vk.com/umnie_turisti" target="_blank"><img width="32" src="http://newtravel.su/images/email/color-vk-48.png" height="32" style="display: block; height: auto;"></a>
								</td>
								<td style="text-decoration: none;">
 <a href="https://ok.ru/group/52199923187928/" target="_blank"><img width="32" src="http://newtravel.su/images/email/color-ok-48.png" height="32" style="display: block; height: auto;"></a>
								</td>
								<td style="text-decoration: none;">
 <a href="https://www.facebook.com/pages/%D0%A3%D0%BC%D0%BD%D1%8B%D0%B5%D0%A2%D1%83%D1%80%D0%B8%D1%81%D1%82%D1%8B%D1%80%D1%84/495696677240397" target="_blank"><img width="32" src="http://newtravel.su/images/email/color-fb-48.png" height="32" style="display: block; height: auto;"></a>
								</td>
								<td style="text-decoration: none;">
 <a href="https://www.instagram.com/umnye__turisty/" target="_blank"><img width="32" src="http://newtravel.su/images/email/color-ig-48.png" height="32" style="display: block; height: auto;"></a>
								</td>
							</tr>
							</tbody>
							</table>
						</td>
					</tr>
					</tbody>
					</table>
				</td>
			</tr>
			</tbody>
			</table>
		</div>
	</td>
</tr>
</tbody>
</table>
 <!-- COMPACT ARTICLE SECTION -->
<table cellpadding="0" cellspacing="0" width="660" align="center" style="table-layout: fixed;">
<tbody>
<tr>
	<td align="center" style="background: #39759f url('http://newtravel.su/images/email/pic-bottom.jpg') no-repeat 0 0; padding: 30px 45px 30px;">
		<table cellpadding="0" cellspacing="0" width="100%">
		<tbody>
		<tr>
			<td style="font-size: 16px; line-height: 20px; font-weight: normal; color: #ffffff;">
				<p style="font-size: 20px;">
					 Информационное сообщение сайта Умныетуристы.рф
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
 <!-- DIVIDER -->
<table cellpadding="0" cellspacing="0" width="660" align="center" style="border-top: 1px dashed #dddddd; table-layout: fixed;">
<tbody>
<tr>
	<td bgcolor="#39749f">
	</td>
</tr>
</tbody>
</table>
 <!-- COMPACT ARTICLE SECTION -->
<table cellpadding="0" cellspacing="0" width="660" align="center" style="table-layout: fixed;">
<tbody>
<tr>
	<td bgcolor="#f8f8f8" align="center" style="padding: 40px 30px;">
		