"use strict";

var gulp = require("gulp"),
    autoprefixer = require("gulp-autoprefixer"),
    cssbeautify = require("gulp-cssbeautify"),
    removeComments = require('gulp-strip-css-comments'),
    rename = require("gulp-rename"),
    sass = require("gulp-sass"),
    cssnano = require("gulp-cssnano"),
    rigger = require("gulp-rigger"),
    uglify = require("gulp-uglify"),
    watch = require("gulp-watch"),
    plumber = require("gulp-plumber"),
    imagemin = require("gulp-imagemin"),
    run = require("run-sequence"),
    rimraf = require("rimraf");


/* Paths to source/build/watch files
=========================*/

var path = {
    build: {
        js: "assets/js/",
        css: "assets/css/",
        img: "assets/i/",
        fonts: "assets/fonts/"
    },
    src: {
        js: "src/js/*.js",
        css: "src/sass/style.scss",
        img: "src/i/**/*.*",
        fonts: "src/fonts/**/*.*"
    },
    watch: {
        js: "src/js/**/*.js",
        css: "src/sass/**/*.scss",
        img: "src/i/**/*.*",
        fonts: "src/fonts/**/*.*"
    },
    clean: "./build"
};



/* Tasks
=========================*/

gulp.task("css:build", function () {
    gulp.src(path.src.css)
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ["last 5 versions"],
            cascade: true
        }))
        .pipe(removeComments())
        .pipe(cssbeautify())
        .pipe(gulp.dest(path.build.css))
        .pipe(cssnano({
            zindex: false,
            discardComments: {
                removeAll: true
            }
        }))
        .pipe(rename("style.min.css"))
        .pipe(gulp.dest(path.build.css));
});


gulp.task("js:build", function () {
    gulp.src(path.src.js)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(gulp.dest(path.build.js))
        .pipe(uglify())
        .pipe(removeComments())
        .pipe(rename("main.min.js"))
        .pipe(gulp.dest(path.build.js));
});


gulp.task("fonts:build", function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
});


gulp.task("image:build", function () {
    gulp.src(path.src.img)
        // .pipe(imagemin({
        //     optimizationLevel: 5,
        //     progressive: true,
        //     svgoPlugins: [{removeViewBox: false}],
        //     interlaced: true
        // }))
        .pipe(gulp.dest(path.build.img));
});


gulp.task("clean", function (cb) {
    rimraf(path.clean, cb);
});


gulp.task('build', function (cb) {
    run(
        "clean",
        //"html:build",
        "css:build",
        "js:build",
        "fonts:build",
        "image:build"
    , cb);
});


gulp.task("watch", function() {
    watch([path.watch.css], function(event, cb) {
        gulp.start("css:build");
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start("js:build");
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start("image:build");
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start("fonts:build");
    });
});


gulp.task("default", function (cb) {
   run(
       "clean",
       "build",
       "watch"
   , cb);
});
