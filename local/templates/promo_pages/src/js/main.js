//= ../../bower_components/bootstrap/js/dist/util.js
//= ../../bower_components/bootstrap/js/dist/modal.js
//= ../../bower_components/bootstrap/js/dist/alert.js
//= ../../bower_components/bootstrap/js/dist/collapse.js
//= ../../bower_components/bootstrap/js/dist/button.js
//= ../../bower_components/popper.js/dist/umd/popper.js
//= ../../bower_components/bootstrap/js/dist/tooltip.js
//= ../../bower_components/bootstrap/js/dist/popover.js
//= ../../bower_components/bootstrap/js/dist/tab.js
//= ../../bower_components/bootstrap/js/dist/dropdown.js
//= ../../bower_components/bootstrap/js/dist/carousel.js

//= ../../bower_components/moment/min/moment-with-locales.js

//= ../../bower_components/simplebar/dist/simplebar.js

//= ../../bower_components/slick-carousel/slick/slick.js

//= ../../bower_components/owl.carousel/dist/owl.carousel.js

//= ../../bower_components/fancybox/dist/jquery.fancybox.js

//= ../../bower_components/jbox/Source/jBox.js

//= ../../bower_components/lodash/dist/lodash.js

//= ../../bower_components/df-visible/jquery.visible.js

//= ../../bower_components/jquery-date-range-picker/src/jquery.daterangepicker.js

//= ../../bower_components/flipclock/compiled/flipclock.min.js

//= ../../bower_components/jquery.maskedinput/dist/jquery.maskedinput.js

//= ../../node_modules/vue-the-mask/dist/vue-the-mask.js

//=  newtravel/search.js

//Отправка запрсоа на помощь по кредиту от руского стандарта
function sendCreditRequest(target, object) {
    $(target).addClass('disabled');
    console.log(object);
    $.get("/local/tools/newtravel.search.ajax.php", {type: "credit_request_callback", ORDER_ID: object.ORDER_ID, CONTACT_PERSON: object.CONTACT_PERSON, PHONE: object.PHONE, EMAIL: object.EMAIL}, function (data) {
        if (data === 'success') {
            $(target).removeClass('btn-blue').addClass('btn-success').text('Звонок заказан, ждите звонка от менеджера в ближейшее время*');
            $(target).after('<br><small>*звонок будет совершен в рабочее время офиса с 10:00 до 19:00 в будни</small>')
        }else {
            $(target).removeClass('disabled');
        }

    });
}

function sendCreditRequestCEB(e) {
    e.preventDefault();
    var form = e.target;
    var validateErrors = [];

    $(form).find('alert-danger').addClass('d-none').empty();

    var form_data = new FormData();
    form_data.append("type", "ceb_request_callback");
    form_data.append("ORDER_ID", $(form).find('input[name=ORDER_ID]').val());
    form_data.append("FIO", $(form).find('input[name=FIO]').val() || validateErrors.push('Заполните поле ФИО'));
    form_data.append("PHONE", $(form).find('input[name=PHONE]').val() || validateErrors.push('Заполните поле Мобильный телефон'));
    form_data.append("PHONE_2", $(form).find('input[name=PHONE_2]').val());
    form_data.append("PHONE_3", $(form).find('input[name=PHONE_3]').val() || validateErrors.push('Заполните поле Рабочий стационарный телефон'));
    form_data.append("PHONE_4", $(form).find('input[name=PHONE_4]').val() || validateErrors.push('Заполните поле Телефон контактного лица'));
    form_data.append("PRICE", $(form).find('input[name=PRICE]').val() || validateErrors.push('Не заполнено поле Стоимость'));
    form_data.append("INITIAL_FEE", $(form).find('input[name=INITIAL_FEE]').val() || validateErrors.push('Заполните поле Первоначальный взнос'));
    form_data.append("CREDIT_PERIOD", $(form).find('select[name=CREDIT_PERIOD]').val() || validateErrors.push('Заполните поле Срок кредитования'));
    form_data.append("FILE_1", $(form).find('input[name=FILE_1]').prop("files")[0] || validateErrors.push('Прикрепите первую страницу паспорта'));
    form_data.append("FILE_2", $(form).find('input[name=FILE_2]').prop("files")[0] || validateErrors.push('Прикрепите сраницу с регистрацией'));
    
    if(validateErrors.length === 0) {
        $.ajax({
            url: '/local/tools/newtravel.search.ajax.php',
            data: form_data,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function(data){
                if (data === 'success') {
                    $(form).find('button[name=submit]').removeClass('btn-blue').addClass('btn-success').text('Заявка на кредит отправлена');
                    $(form).find('button[name=submit]').after('<br><small>*звонок будет совершен в рабочее время офиса с 10:00 до 19:00 в будни</small>')
                }else {
                    $(form).find('button[name=submit]').removeClass('disabled');
                }
            }
        });
    } else {
        $(form).find('alert-danger').removeClass('d-none');
        validateErrors.forEach(function (key, value) {
            $(form).find('alert-danger').append('<p>'+ value +'</p>')
        })
    }
}

function showHomeCreditPopup(data) {
    KreditOtdel.Widget.attach({
        widgetType: KreditOtdel.Widget.widgetTypes.VsegdaDa,
        elementId: 'vsegda-da-buy',
        autoStart: true,
        data: {
            hostname: 'widget-test.l-kredit.ru', //hostname replaced widget-test.l-kredit.ru
            //shopId: 123,
            goods: [{name: "Туристическая путевка", price: data.PRICE, count: 1}],
            lastName: data.LAST_NAME,
            firstName: data.NAME,
            middleName: '',
            phone: data.PHONE,
            email: data.EMAIL,
            discount: 0,
            orderId: data.ORDER_ID
        }
    });
}

$(document).ready(function () {
    //Рекланый банне
    if(!BX.getCookie('BANNER_CLOSED')) {
        setTimeout(function () {
            $('#pickMeTourModal').modal('show');
        }, 5000);

    }
    /*BANNER MODAL CLOSE*/
    $('#pickMeTourModal').on('hide.bs.modal', function (event) {
        BX.setCookie('BANNER_CLOSED', true, {expires:604800, path: "/"})
    });


    $('.dropdown-toggle').dropdown();

    /*Slider*/
    $(".operators-slider").slick({
        autoplay: true,
        dots: false,
        arrows: false,
        autoplaySpeed: 3000,
        slidesToShow: 6,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2
                }
            },
            {
                dots: false,
                arrows: false,
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
        ],
    });

    /*Qoute Slider*/
    $(".quote-carousel").slick({
        autoplay: true,
        dots: true,
        arrows: false,
        autoplaySpeed: 3000,
        slidesToShow: 1,
        responsive: [
            {
                arrows: true,
                breakpoint: 600,
            }
        ],
    });

    /*Hotel Images Slider*/
    $(".hotel-photos-carousel").slick({
        autoplay: true,
        dots: false,
        arrows: true,
        autoplaySpeed: 3000,
        slidesToShow: 5,
        infinite: true,
        responsive: [
            {
                arrows: true,
                breakpoint: 1000,
                slidesToShow: 4,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                arrows: true,
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                arrows: true,
                breakpoint: 400,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1

                }
            },

        ],
    });

   /*Hotel Images Slider*/
    $(".blog-hotel-photos-carousel").slick({
        autoplay: true,
        dots: false,
        arrows: true,
        autoplaySpeed: 3000,
        slidesToShow: 4,
        infinite: true,
        responsive: [
            {
                arrows: true,
                breakpoint: 1000,
                slidesToShow: 4,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                arrows: true,
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                arrows: true,
                breakpoint: 400,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1

                }
            },

        ],
    });

    /*OWL-slider*/
    $(".sli-slider").owlCarousel({
        items: 3,
        merge: true,
        loop: true,
        margin: 10,
        video: true,
        lazyLoad: true,
        center: true,
        autoplay: true,
        autoplayTimeout: 4000,
        paginationSpeed: 400,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 2,
                nav: false
            },
            1000: {
                items: 3,
                nav: true,
            }
        }

    });


    /*ScrollTo*/
    $('.top-line nav a, a.scrollTo-link').click(function () {
        //Animate
        $('html, body').stop().animate({
            scrollTop: $($(this).attr('href')).offset().top - 160
        }, 400);
        return false;
    });

    /*GoogleMaps*/
    // The latitude and longitude of your business / place
    // var position = [54.730027, 55.930853];
    // showGoogleMaps();
    //
    // function showGoogleMaps() {
    //
    //     if ($("#googlemaps").length) {
    //         var latLng = new google.maps.LatLng(position[0], position[1]);
    //         var center = new google.maps.LatLng(54.730084, 55.925456);
    //
    //         var mapOptions = {
    //             zoom: 16, // initialize zoom level - the max value is 21
    //             streetViewControl: false, // hide the yellow Street View pegman
    //             scaleControl: true, // allow users to zoom the Google Map
    //             mapTypeId: google.maps.MapTypeId.ROADMAP,
    //             center: center
    //         };
    //
    //         map = new google.maps.Map(document.getElementById('googlemaps'),
    //             mapOptions);
    //
    //         // Show the default red marker at the location
    //         marker = new google.maps.Marker({
    //             position: latLng,
    //             map: map,
    //             draggable: false,
    //             animation: google.maps.Animation.DROP
    //         });
    //     }
    // }
    //
    // google.maps.event.addDomListener(window, 'load', showGoogleMaps);

    /*Yandex Maps*/
    ymaps.ready(init);
    function init(){
        // Создание карты.
        var myMap = new ymaps.Map("footerMap", {

            center: [54.7576,56.0056],
            zoom: 12
        });

        var myPlacemark1 = new ymaps.Placemark([54.697962, 55.989995], {
            // Хинт показывается при наведении мышкой на иконку метки.
            hintContent: 'Офис С. Перовской 50',
            // Балун откроется при клике по метке.
            balloonContent: 'График работы<br>' +
            'Пн.- Пт. с 10.00 - 20.00 <br>' +
            'Сб. с 11.00 - 17.00 <br>' +
            'Бронирование on-line круглосуточно<br>' +
            'Телефоны<br>' +
            '+7(347)216-30-61 <br>' +
            '8-800-505-47-61 (бесплатный)'
        });
        var myPlacemark2 = new ymaps.Placemark([54.7548,56.0047], {
            // Хинт показывается при наведении мышкой на иконку метки.
            hintContent: 'Офис Пр. Октября, 52',
            // Балун откроется при клике по метке.
            balloonContent: 'График работы<br>' +
            'Пн.- Пт. с 10.00 - 20.00 <br>' +
            'Сб. с 11.00 - 17.00 <br>' +
            'Бронирование on-line круглосуточно<br>' +
            'Телефоны<br>' +
            '+7(347)216-30-81 <br>' +
            '8-800-505-47-61 (бесплатный)'
        });

        myMap.geoObjects.add(myPlacemark1);
        myMap.geoObjects.add(myPlacemark2);
    }


    /*QUESTION MODAL OPEN*/
    $('#expertQuestionModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var recipient = button.data('hotel-id'); // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('.modal-body input[name=form_text_67]').val('https://умныетуристы.рф/hotel/' + recipient + "/");
    });

    /*TORG MODAL OPEN*/
    $('#torgModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var recipient = button.data('tour-info'); // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('.modal-body input[name=form_text_101]').val(recipient);
    });

});