<div class="modal fade" id="bannerModal" tabindex="-1" role="dialog" aria-labelledby="bannerModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" style="max-width: 600px">
		<div class="modal-content " style="">
			<div class="d-flex-inline align-content-end pr-2">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<h4 class="text-primary">Будьте ближе к нам c WhatsApp 😉</h4>

				<p>Мы рады Вам сообщить о запуске нового сервиса рассылок через WhatsApp. 💥</p>
				<p>Получайте самые выгодные предложения, важные новости и путешествуйте по самым выгодным предложениям.🍍🍡🌴</p>
				<p>👆Как подписаться на рассылку?</p>

				<p>Вам необходимо:</p>
				<p>👉в список своих контактов добавить номер телефона <b><a href="tel:+79659439873">+7(965) 943 98 73</a></b> и
				отправить сообщение через приложение WhatsApp с любым текстом: Например «Напишите свое имя и Ваш город».</p>

				<p>Данный номер будет использоваться ТОЛЬКО для рассылок!</p>

				<p>Подпишись и получай первым только самые лучшие предложения дня! 😉</p>

				<p><i>С уважением, Умные туристы</i></p>
			</div>


			<img title="whatsupp" alt="whatsupp" style="width: 100%;" src="<?=SITE_TEMPLATE_PATH?>/assets/i/pics/whatsupp.jpg">
		</div>
	</div>
</div>