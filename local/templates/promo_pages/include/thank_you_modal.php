<div class="modal fade" id="thankYouModal" tabindex="-1" role="dialog" aria-labelledby="thankYouModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="d-flex-inline align-content-end pr-2">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center">
				<div class="row mb-4">
					<div class="col-12">
						<div class="display-5 text-success">
							Спасибо за заявку!
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<p>В скором времени менеджеры свяжутся с Вами</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
