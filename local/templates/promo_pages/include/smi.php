<section class="search-section mt-5 mb-5" id="smi">
	<div class="container">
		<div class="row">
			<div class="col-12 mx-auto text-center">
				<h2 class="display-5 mb-4">СМИ о нас</h2>
			</div>
		</div>
		<div class="row">
			<? $APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"index_slider",
				array(
					"DISPLAY_DATE"                    => "Y",
					"DISPLAY_NAME"                    => "Y",
					"DISPLAY_PICTURE"                 => "Y",
					"DISPLAY_PREVIEW_TEXT"            => "N",
					"AJAX_MODE"                       => "N",
					"IBLOCK_TYPE"                     => "simai",
					"IBLOCK_ID"                       => "90",
					"NEWS_COUNT"                      => "5",
					"SORT_BY1"                        => "ACTIVE_FROM",
					"SORT_ORDER1"                     => "DESC",
					"SORT_BY2"                        => "NAME",
					"SORT_ORDER2"                     => "ASC",
					"FILTER_NAME"                     => "arrFilter",
					"FIELD_CODE"                      => array(
						0 => "SHOW_COUNTER",
						1 => "",
					),
					"PROPERTY_CODE"                   => array(
						0 => "",
						1 => "",
					),
					"CHECK_DATES"                     => "Y",
					"DETAIL_URL"                      => "",
					"PREVIEW_TRUNCATE_LEN"            => "",
					"ACTIVE_DATE_FORMAT"              => "d.m.Y",
					"SET_TITLE"                       => "N",
					"SET_BROWSER_TITLE"               => "Y",
					"SET_META_KEYWORDS"               => "Y",
					"SET_META_DESCRIPTION"            => "Y",
					"SET_STATUS_404"                  => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
					"ADD_SECTIONS_CHAIN"              => "N",
					"HIDE_LINK_WHEN_NO_DETAIL"        => "N",
					"PARENT_SECTION"                  => "",
					"PARENT_SECTION_CODE"             => "",
					"INCLUDE_SUBSECTIONS"             => "N",
					"CACHE_TYPE"                      => "A",
					"CACHE_TIME"                      => "36000000",
					"CACHE_FILTER"                    => "N",
					"CACHE_GROUPS"                    => "Y",
					"PAGER_TEMPLATE"                  => ".default",
					"DISPLAY_TOP_PAGER"               => "N",
					"DISPLAY_BOTTOM_PAGER"            => "N",
					"PAGER_TITLE"                     => "Новости",
					"PAGER_SHOW_ALWAYS"               => "N",
					"PAGER_DESC_NUMBERING"            => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL"                  => "N",
					"AJAX_OPTION_JUMP"                => "N",
					"AJAX_OPTION_STYLE"               => "Y",
					"AJAX_OPTION_HISTORY"             => "N",
					"COMPONENT_TEMPLATE"              => "adapt_popular_country_posts",
					"AJAX_OPTION_ADDITIONAL"          => "",
					"SET_LAST_MODIFIED"               => "N",
					"COMPOSITE_FRAME_MODE"            => "Y",
					"COMPOSITE_FRAME_TYPE"            => "AUTO",
					"PAGER_BASE_LINK_ENABLE"          => "N",
					"SHOW_404"                        => "N",
					"MESSAGE_404"                     => ""
				),
				$component
			); ?>
		</div>
	</div>
</section>