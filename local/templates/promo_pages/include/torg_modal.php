<div class="modal fade" id="torgModal" tabindex="-1" role="dialog" aria-labelledby="torgModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="d-flex-inline align-content-end pr-2">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<? $APPLICATION->IncludeComponent(
					"bitrix:form.result.new",
					"torg_from",
					Array(
						"CACHE_TIME"             => "3600",
						"CACHE_TYPE"             => "A",
						"CHAIN_ITEM_LINK"        => "",
						"CHAIN_ITEM_TEXT"        => "",
						"EDIT_URL"               => "",
						"IGNORE_CUSTOM_TEMPLATE" => "Y",
						"LIST_URL"               => "",
						"SEF_MODE"               => "N",
						"SUCCESS_URL"            => "",
						"USE_EXTENDED_ERRORS"    => "N",
						"VARIABLE_ALIASES"       => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
						"WEB_FORM_ID"            => "2"
					)
				); ?>
			</div>
		</div>
	</div>
</div>