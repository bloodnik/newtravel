<?
foreach ($arResult["ITEMS"] as $key => $arItem) {
	if (is_array($arItem["PREVIEW_PICTURE"])) {
		$arResult['ITEMS'][$key]['PREVIEW_IMG'] = CFile::ResizeImageGet(
			$arItem["PREVIEW_PICTURE"],
			array("width" => 50, "height" => 50),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true
		);
	} elseif (is_array($arResult["DETAIL_PICTURE"])) {
		$arResult['ITEMS'][$key]['PREVIEW_IMG'] = CFile::ResizeImage(
			$arItem["DETAIL_PICTURE"],
			array("width" => 50, "height" => 50),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true
		);
	}
}

