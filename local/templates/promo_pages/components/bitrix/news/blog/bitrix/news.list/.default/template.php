<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/plugins/jquery.isotope.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jquery-migrate.min.js");

use Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();
?>

<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<? if ($key === 2): ?>
		<div class="card bg-orange text-white p-3 mb-5">
			<div class="row d-flex align-items-center">
				<div class="col-12 col-md-9 col-lg-10 h4 mb-3 ">Закажи профессиональную подборку туров у наших экспертов прямо сейчас</div>
				<div class="col-12 col-md-3 col-lg-2"><a class="text-white btn btn-lg btn-warning btn-block" href="#" data-toggle="modal" data-target="#pickMeTourModal">Заказать</a></div>
			</div>
		</div>
	<? endif; ?>


	<? if ($key === 4): ?>
		<div class="card bg-warning text-white p-3 mb-5">
			<? $APPLICATION->IncludeComponent(
				"bitrix:form.result.new",
				"hot_tours_from",
				Array(
					"CACHE_TIME"             => "3600",
					"CACHE_TYPE"             => "A",
					"CHAIN_ITEM_LINK"        => "",
					"CHAIN_ITEM_TEXT"        => "",
					"EDIT_URL"               => "",
					"IGNORE_CUSTOM_TEMPLATE" => "N",
					"LIST_URL"               => "",
					"SEF_MODE"               => "N",
					"SUCCESS_URL"            => "",
					"USE_EXTENDED_ERRORS"    => "N",
					"VARIABLE_ALIASES"       => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
					"WEB_FORM_ID"            => "5",
				),
				$component
			); ?>
		</div>
	<? endif; ?>

	<article class="card mb-5" id="<?=$this->GetEditAreaId($arItem['ID']);?>" itemscope itemtype="http://schema.org/Article">
		<div style="position: relative">
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem["NAME"]?>" itemprop="url">
				<? if (is_array($arItem["PREVIEW_IMG"])): ?>
					<img class="card-img" src="<?=$arItem["PREVIEW_IMG"]["SRC"]?>" alt="<?=$arItem["NAME"]?>" title="Умныетуристы <?=$arItem["NAME"]?>">
					<meta itemprop="image" content="<?=$arItem["PREVIEW_IMG"]["SRC"]?>"/>
				<? endif; ?>
				<div class="card-img-overlay d-flex p-0">
					<h4 class="text-white mb-0 card-title align-self-end px-3 py-4 w-100 text-uppercase" style="background-color: rgba(0,0,0,0.3)" itemprop="name"><?=$arItem["NAME"]?></h4>
				</div>
			</a>
		</div>
		<div class="card-body">
			<p class="card-text">
				<? if ($arItem["PREVIEW_TEXT"]) {
					echo truncateText(strip_tags($arItem["PREVIEW_TEXT"]), 300);
				} elseif ($arItem["DETAIL_TEXT"]) {
					echo truncateText(strip_tags($arItem["DETAIL_TEXT"]), 300);
				} ?>
			</p>

			<hr>
			<div class="text-right">
				<a class="btn btn-sm btn-outline-info" href="<?=$arItem['DETAIL_PAGE_URL']?>">Читать дальше</a>
			</div>
		</div>
	</article>
<? endforeach; ?>


<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
	<div class="nav-string">
		<?=$arResult["NAV_STRING"]?>
	</div>
<? endif; ?>
