<?
if (is_array($arResult["PREVIEW_PICTURE"])) {
	$arFileTmp                                = CFile::ResizeImageGet(
		$arResult["PREVIEW_PICTURE"],
		array("width" => 816, "height" => 400),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		false
	);
	$arSize                                   = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);
	$arResult["PREVIEW_IMG"] = array(
		"SRC"    => $arFileTmp["src"],
		"WIDTH"  => IntVal($arSize[0]),
		"HEIGHT" => IntVal($arSize[1]),
	);
} elseif (is_array($arResult["DETAIL_PICTURE"])) {
	$arFileTmp                                = CFile::ResizeImageGet(
		$arResult["DETAIL_PICTURE"],
		array("width" => 816, "height" => 500),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		false
	);
	$arSize                                   = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);
	$arResult["PREVIEW_IMG"] = array(
		"SRC"    => $arFileTmp["src"],
		"WIDTH"  => IntVal($arSize[0]),
		"HEIGHT" => IntVal($arSize[1]),
	);
}

//Получаем дополнительные фотографии
if (is_array($arResult["PROPERTIES"]["MORE_PHOTO"])) {
	foreach ($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $photoId) {
		$arFileTmp                                = CFile::ResizeImageGet(
			$photoId,
			array("width" => 1000, "height" => 500),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			false
		);
		$arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"][] = $arFileTmp["src"];
	}
}


//Устанавливаем дату от и дату до
$arResult['DATEFROM'] = $arResult['DISPLAY_PROPERTIES']['DATEFROM'] ? $arResult['DISPLAY_PROPERTIES']['DATEFROM']['VALUE'] : date("d.m.Y", strtotime("+1 day"));
$arResult['DATETO'] = date("d.m.Y", strtotime($arResult['DATEFROM']. ' + 14 days' ));

//Похожие статьи
if($arResult['DISPLAY_PROPERTIES']['RELATED']){
	$arResult['RELATED'] = array();
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL");
	$arFilter = Array("IBLOCK_ID"=>IntVal(array_shift($arResult['DISPLAY_PROPERTIES']['RELATED']['LINK_ELEMENT_VALUE'])['IBLOCK_ID']), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID" =>$arResult['DISPLAY_PROPERTIES']['RELATED']['VALUE']);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>9), $arSelect);
	while($arFields = $res->GetNext()){
		$arResult['RELATED'][] = $arFields;
	}
}

//Отели
if($arResult['DISPLAY_PROPERTIES']['HOTELS']) {

	$arResult['HOTELS'] = array();
	$arSelect           = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "DETAIL_TEXT", "PROPERTY_MORE_PHOTO");
	$arFilter           = Array("IBLOCK_ID" => IntVal(58), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "ID" => $arResult['DISPLAY_PROPERTIES']['HOTELS']['VALUE']);
	$res                = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 20), $arSelect);
	while ($arFields = $res->GetNext()) {
		$arFields['PROPERTY_MORE_PHOTO_VALUE'] = array_slice($arFields['PROPERTY_MORE_PHOTO_VALUE'],0,4);
		$arResult['HOTELS'][] = $arFields;
	}
}


$cp = $this->__component;
if (is_object($cp))
	$cp->SetResultCacheKeys(array('DETAIL_PAGE_URL', 'PROPERTIES'));

?>