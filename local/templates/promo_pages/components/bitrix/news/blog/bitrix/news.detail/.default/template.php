<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
	<article class="card mb-5" itemscope itemtype="http://schema.org/Article">
		<img class="card-img-top" src="<?=$arResult["PREVIEW_IMG"]["SRC"]?>" alt="Card image cap">
		<div class="card-body">
			<div class="card-text" itemprop="articleBody">
				<div class="blog-text">
					<? if ($arResult["DETAIL_TEXT"]): ?>
						<?=$arResult["DETAIL_TEXT"]?>
					<? elseif ($arResult["PREVIEW_TEXT"]): ?>
						<?=$arResult["PREVIEW_TEXT"]?>
					<? endif ?>
				</div>

				<!--ОТЕЛИ-->
				<? if ( ! empty($arResult['HOTELS'])): ?>
					<? foreach ($arResult['HOTELS'] as $key => $arHotel): ?>

						<? if ($key === 2): ?>
							<div class="card bg-orange text-white p-3 mb-5">
								<div class="row d-flex align-items-center">
									<div class="col-12 col-md-9 col-lg-10 h4 mb-3 ">Закажи профессиональную подборку туров у наших экспертов прямо сейчас</div>
									<div class="col-12 col-md-3 col-lg-2"><a class="text-white btn btn-warning btn-block" href="#" data-toggle="modal" data-target="#pickMeTourModal">Заказать</a></div>
								</div>
							</div>
						<? endif; ?>

						<? if ($key === 4): ?>
							<div class="card bg-warning text-white p-3 mb-5">
								<? $APPLICATION->IncludeComponent(
									"bitrix:form.result.new",
									"hot_tours_from",
									Array(
										"CACHE_TIME"             => "3600",
										"CACHE_TYPE"             => "A",
										"CHAIN_ITEM_LINK"        => "",
										"CHAIN_ITEM_TEXT"        => "",
										"EDIT_URL"               => "",
										"IGNORE_CUSTOM_TEMPLATE" => "N",
										"LIST_URL"               => "",
										"SEF_MODE"               => "N",
										"SUCCESS_URL"            => "",
										"USE_EXTENDED_ERRORS"    => "N",
										"VARIABLE_ALIASES"       => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
										"WEB_FORM_ID"            => "5",
									),
									$component
								); ?>
							</div>
						<? endif; ?>

						<div class="card border-light-blue mb-5 bg-light">
							<? if (count($arHotel['PROPERTY_MORE_PHOTO_VALUE']) > 0): ?>
								<div class="blog-hotel-photos-carousel">
									<? foreach ($arHotel['PROPERTY_MORE_PHOTO_VALUE'] as $imageId): ?>
										<a class="carousel-item" data-fancybox="hotelPhotos_<?=$arHotel['CODE']?>" href="<?=CFile::GetPath($imageId)?>"><img src="<?=CFile::GetPath($imageId)?>" height="250"></a>
									<? endforeach; ?>
								</div>
							<? endif; ?>
							<div class="card-body">
								<a href="<?=$arHotel['DETAIL_PAGE_URL']?>"><h4 class="card-title"><?=$arHotel['NAME']?></h4></a>
								<p class="card-text"><?=$arHotel['DETAIL_TEXT']?></p>
								<div class="hotel-price" data-hotel-code="<?=$arHotel['CODE']?>" id="hotel_<?=$arHotel['CODE']?>"></div>
								<div class="price-descr small text-muted" style="display: none;">Показаны цены на ближайшую неделю, на 1 взрослого туриста. Для уточнения цены пройдите на страницу отеля и выполните поиск по Вашим параметрам</div>
							</div>
							<div class="card-footer">
								<noindex>
									<a class="btn btn-block btn-dark-blue" rel="nofollow" href="<?=$arHotel['DETAIL_PAGE_URL']?>?start=Y">Поиск туров в этот отель</a>
								</noindex>
							</div>
						</div>
					<? endforeach; ?>
				<? endif; ?>


				<div class="card bg-light-blue text-white p-3 mb-5">
					<div class="row d-flex align-items-center">
						<div class="col-12 col-md-9 col-lg-10 h4 mb-3 ">Не нашел подходящий тур? Пусть наши эксперты займутся этим. Закажи подборку туров!</div>
						<div class="col-12 col-md-3 col-lg-2"><a class="text-white btn btn-dark-blue btn-block" href="#" data-toggle="modal" data-target="#pickMeTourModal">Заказать</a></div>
					</div>
				</div>
			</div>
		</div>

		<div class="card-footer text-right">
			<script type="text/javascript">(function () {
                    if (window.pluso) if (typeof window.pluso.start == "function") return;
                    if (window.ifpluso == undefined) {
                        window.ifpluso = 1;
                        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                        s.type = 'text/javascript';
                        s.charset = 'UTF-8';
                        s.async = true;
                        s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                        var h = d[g]('body')[0];
                        h.appendChild(s);
                    }
                })();
			</script>
			<div class="pluso mx-3" data-background="transparent" data-options="big,square,line,horizontal,nocounter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir"></div>
		</div>
	</article>

<? if ( ! empty($arResult['RELATED'])): ?>
	<div class="row">
		<div class="col-12">
			<div class="page-heading">
				<h2 class="display-5">Также советуем почитать</h2>
			</div>
		</div>

		<div class="col-12">
			<div class="card-deck">
				<? foreach ($arResult['RELATED'] as $arItem): ?>
					<div class="card mb-2">
						<a class="text-white" href="<?=$arItem['DETAIL_PAGE_URL']?>">
							<? $img = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 250, 'height' => 200), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true) ?>
							<img class="card-img-top" src="<?=$img['src']?>" alt="<?=$arItem['NAME']?>" style="height: 200px">

							<div class="card-img-overlay" style="background-color: rgba(0,0,0,0.11)">
								<h4 class="card-title"><?=$arItem['NAME']?></h4>
							</div>
						</a>
					</div>
				<? endforeach; ?>
			</div>
		</div>
	</div>
<? endif; ?>