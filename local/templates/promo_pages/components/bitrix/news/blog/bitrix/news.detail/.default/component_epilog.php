<?
$COUNTRY_ID = ! empty($arResult['PROPERTIES']['TV_COUNTRY_ID']['VALUE']) ? $arResult['PROPERTIES']['TV_COUNTRY_ID']['VALUE'] : "";

//Получам цены на отели
global $APPLICATION;

$USER_CITY = $APPLICATION->get_cookie('USER_CITY');
$USER_CITY = isset($USER_CITY) && ! empty($USER_CITY) ? $USER_CITY : $_SESSION['CITY']['UF_XML_ID'];


\Bitrix\Main\Page\Asset::getInstance()->addString('<link href="https://' . SITE_SERVER_NAME . $arResult['DETAIL_PAGE_URL'] . '" rel="canonical" />', true);
?>

<script>
    $(document).ready(function () {
        loadPrices();
    });

    //Загрузить цены на отели
    function loadPrices() {
        $('.hotel_price').remove();
        var hotelLinks = $('.hotel-price');
        $.each(hotelLinks, function (key, value) {
            var hotelId = $(value).data('hotel-code'),
                countryId = "<?=$COUNTRY_ID?>";

            //параметры запроса
            var request_params = {
                type: "search",
                departure: <?=$USER_CITY ?: "4"?>,
                country: countryId,
                hotels: hotelId,
                adults: "1",
	            test: "15"
            };

            $.get("/local/modules/newtravel.search/lib/ajax.php", {type: "getHash", hashString: JSON.stringify(request_params)}, function (data) {
                var hash = data;

                $.get("/local/modules/newtravel.search/lib/ajax.php", {type: "checkCache", hash: hash}, function (data) {

                    if (data === 'fail') {
                        //крутилка (лоадер)
                        $(value).after('<span class="price_loader"><br><i class="fa fa-spinner fa-pulse fa-fw"></i> обновляем цены</span>');
                        startSearchPrices(request_params, hotelId, hash);

                    } else {
                        data = JSON.parse(JSON.parse(data));

                        if (parseInt(data.minprice) > 0) {
                            $('.price-descr').show();

                            var html = "<a href='/hotel/" + hotelId + "/?start=Y' rel='nofollow' class='hotel_price display-7 text-success'> цена за тур от " + parseInt(data.minprice).toLocaleString() + " руб/чел.</a>";
                            $('#hotel_' + hotelId).html(html);
                        } else {
                            $('#hotel_' + hotelId).html("<span class='hotel_price display-8 text-danger'>Нет вылетов в ближайшую неделю.</span>");
                        }
                    }
                })

            });


        });
    }

    function startSearchPrices(request_params, hotelId, hash) {
        $.getJSON("/local/modules/newtravel.search/lib/ajax.php", request_params, function (json) {
            var requestid = json.result.requestid;
            checkTourStatus(requestid, hotelId, hash);
        });
    }


    //Проверяем статус запроса
    function checkTourStatus(requestid, hotelId, hash) {

        $.getJSON("/local/modules/newtravel.search/lib/ajax.php", {type: "status", requestid: requestid}, function (json) {

            if (json.data.status.state != "finished" && json.data.status.timepassed < "45") {
                setTimeout(function () {
                    checkTourStatus(requestid, hotelId, hash);
                }, 3000);

            } else {
                //Убираем крутлилку
                $('#hotel_' + hotelId).parent().find('.price_loader').remove();

                $.post('/local/modules/newtravel.search/lib/ajax.php', {type: "setCache", hash: hash, data: JSON.stringify(json.data.status)}, function () {
                });

                if (json.data.status.minprice != "") { //стоимость определена
                    $('.price-descr').show();

                    var html = "<a href='/hotel/" + hotelId + "/?start=Y' rel='nofollow' class='hotel_price display-7 text-success'> цена за тур от " + parseInt(json.data.status.minprice).toLocaleString() + " руб/чел.</a>"

                    $('#hotel_' + hotelId).html(html);

                } else { //стоимость не найдена
                    $('#hotel_' + hotelId).html("<span class='hotel_price display-8 text-danger'>Нет вылетов в ближайшую неделю.</span>");
                }
            }
        })
    }
</script>