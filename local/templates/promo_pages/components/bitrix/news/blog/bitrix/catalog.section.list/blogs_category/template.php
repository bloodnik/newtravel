<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>

<div class="list-group">
	<? foreach ($arResult['SECTIONS'] as &$arSection) : ?>
		<a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" href="<?=$arSection["LIST_PAGE_URL"];?><?=$arSection["CODE"];?>.php"><? echo $arSection["NAME"]; ?>
			<? if ($arParams["COUNT_ELEMENTS"]) {
				?> <span class="badge badge-primary badge-pill"><? echo $arSection["ELEMENT_CNT"]; ?></span>
			<? } ?>
		</a>
	<? endforeach; ?>
</div>