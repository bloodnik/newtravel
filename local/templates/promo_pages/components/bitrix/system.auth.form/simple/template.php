<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

?>

<style>
	.bx-authform-social ul {
		padding: 5px 0 0px;
		margin: 0;
	}
</style>

<? if ($arResult["FORM_TYPE"] == "login"): ?>

	<? if ( ! empty($arParams["~AUTH_RESULT"])):
		$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
		?>
		<div class="alert alert-danger"><?=nl2br(htmlspecialcharsbx($text))?></div>
	<? endif ?>

	<? if ($arResult['ERROR_MESSAGE'] <> '' && $_GET['register'] != 'yes'):
		$text = str_replace(array("<br>", "<br />"), "\n", $arResult['ERROR_MESSAGE']['MESSAGE']);
		?>
		<script>$('#auth_modal').modal('show')</script>
		<div class="alert alert-danger"><?=nl2br(htmlspecialcharsbx($text))?></div>
	<? endif ?>


	<form class="px-4" name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

		<input type="hidden" name="AUTH_FORM" value="Y"/>
		<input type="hidden" name="TYPE" value="AUTH"/>

		<? if (strlen($arResult["BACKURL"]) > 0): ?>
			<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>"/>
		<? endif ?>
		<? foreach ($arResult["POST"] as $key => $value): ?>
			<input type="hidden" name="<?=$key?>" value="<?=$value?>"/>
		<? endforeach ?>

		<div class="form-group">
			<label>Логин</label>
			<input type="ttext" class="form-control" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>" placeholder="Введите логин">
		</div>
		<div class="form-group">
			<label>Пароль</label>
			<? if ($arResult["SECURE_AUTH"]): ?>
				<div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none">
					<div class="bx-authform-psw-protected-desc"><span></span><? echo GetMessage("AUTH_SECURE_NOTE") ?></div>
				</div>

				<script type="text/javascript">
                    document.getElementById('bx_auth_secure').style.display = '';
				</script>
			<? endif ?>
			<input type="password" class="form-control" name="USER_PASSWORD" autocomplete="off" placeholder="*****">
		</div>

		<? if ($arResult["CAPTCHA_CODE"]): ?>
			<input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>"/>

			<div class="bx-authform-formgroup-container dbg_captha">
				<div class="bx-authform-label-container">
					<? echo GetMessage("AUTH_CAPTCHA_PROMT") ?>
				</div>
				<div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA"/></div>
				<div class="bx-authform-input-container">
					<input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off"/>
				</div>
			</div>
		<? endif; ?>

		<? if ($arResult["STORE_PASSWORD"] == "Y"): ?>
			<div class="form-check">
				<label for="USER_REMEMBER" class="form-check-label">
					<input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" class="form-check-input">
					Запомнить меня
				</label>
			</div>
		<? endif ?>

		<button type="submit" name="Login" class="btn btn-primary">Войти</button>
	</form>

	<script type="text/javascript">
		<?if (strlen($arResult["LAST_LOGIN"]) > 0):?>
        try {
            document.form_auth.USER_PASSWORD.focus();
        } catch (e) {
        }
		<?else:?>
        try {
            document.form_auth.USER_LOGIN.focus();
        } catch (e) {
        }
		<?endif?>
	</script>
<? endif ?>
