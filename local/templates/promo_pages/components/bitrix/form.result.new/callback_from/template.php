<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Application;

$request    = Application::getInstance()->getContext()->getRequest();
$formresult = $request->get('formresult');
$form_id    = $request->get('WEB_FORM_ID');

$this->setFrameMode(true);
?>
<div class="row callback-form-wrap">
	<div class="col-12 col-md-3 text-center align-self-center">
		<img class="rounded-circle img-fluid" style="width: 150px;" src="<?=SITE_TEMPLATE_PATH?>/assets/i/callback_manager.jpg" alt="">
	</div>
	<div class="col align-self-center">
		<div class="display-6 mb-3 text-center text-md-left">
			Хотите, чтобы мы вам перезвонили?
		</div>
		<form name="<?=$arResult["WEB_FORM_NAME"]?>" action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data" onsubmit="ajaxSendCallBack(this)">
			<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>">
			<?=bitrix_sessid_post()?>
			<div class="small">
				<? if ($arResult["isFormErrors"] == "Y"): ?>
					<?=$arResult["FORM_ERRORS_TEXT"];?>
				<? endif; ?>
				<? if ($arResult["isFormNote"] == "Y") : ?>
					<div class="alert alert-success">
						<?=$arResult["FORM_NOTE"]?>
					</div>
				<? endif; ?>
			</div>
			<div class="form-row align-items-center">
				<div class="col-12 col-md-8 mb-2 mb-md-0">
					<label class="sr-only" for="inlineFormInput">Телефона</label>
					<input type="text" class="form-control mb-2 mb-sm-0"
					       aria-invalid="false" aria-required="true"
					       name="form_text_75" value="<?=$request->get('form_text_75')?>"
					       pattern="^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))"
					       maxlength='15'
					       placeholder="Телефон 7__________"
					       title="Номер должен содержать только цифры и начинаться с либо с 8, либо с +7"
					       required/>
				</div>
				<div class="col-12 col-md-4">
					<input type="hidden" name="web_form_apply" value="Y"/>
					<button type="submit" class="btn btn-primary btn-block ld-over-inverse" value="Y">
						Заказать
						<div class="ld ld-ring ld-spin"></div>
					</button>
				</div>
				<div class="col-12">
					<div class="form-check text-muted small">
						<label class="form-check-label">
							<input class="form-check-input" type="checkbox" name="confirm-agreement" checked id="confirm-agreement_callback">
							Я согласен с <a href="/about/agreement/" target="_blank">условиями</a> обработки персональных данных
						</label>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script>

    function ajaxSendCallBack(target) {
        event.preventDefault();

        var form = $(target),
            form_wrapper_name = '.callback-form-wrap',
            form_wrapper = $('.callback-form-wrap'),
            form_action = form.attr('action'),
            form_submit_btn = form.find('button[type=submit]'),
            form_agreement = form.find('input[name=confirm-agreement]'),
            modal = $('#callbackModal');

        //Если email пустой, то заполняем его по умолчанию

        form_submit_btn.toggleClass('running');

        if (!form_agreement.prop('checked')) {
            alert('Подтвердите согласие с условиями обработки персональных данных');
            form_submit_btn.toggleClass('running');
            return false;
        }

        $.post(form_action, form.serialize(), function (data) {
            form_wrapper.replaceWith($(data).find(form_wrapper_name));

            setTimeout(function () {
                modal.modal('hide');
            }, 1500);

            yaCounter24395911.reachGoal('CALLBACK_FORM_SEND');
            form_submit_btn.toggleClass('running');
        })
    }
</script>