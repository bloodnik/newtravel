<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

use Bitrix\Main\Application;

$request    = Application::getInstance()->getContext()->getRequest();
$formresult = $request->get('formresult');
$form_id    = $request->get('WEB_FORM_ID');

?>
<div class="question-form-wrap">
	<h3 class="display-6">Задайте вопрос эксперту</h3>

	<? if ($arResult["isFormErrors"] == "Y"): ?>
		<?=$arResult["FORM_ERRORS_TEXT"];?>
		<script>
            $(document).ready(function () {
                $('#expertQuestionModal').modal('show');
            })
		</script>
	<? endif; ?>

	<?=$arResult["FORM_NOTE"]?>

	<? if ($arResult["isFormNote"] != "Y") {
		?>

		<?=$arResult["FORM_HEADER"]?>

		<div class="row">
			<div class="col-12 col-md-6">
				<div class="form-group">
					<label>Ваше имя</label>
					<input type="text" class="form-control" name="form_text_63" value="<?=$request->get('form_text_63')?>" placeholder="Ваше имя" required/>
				</div>
				<div class="form-group">
					<label>Номер телефона *</label>
					<input type="text" class="form-control"
					       name="form_text_64"
					       aria-invalid="false" aria-required="true"
					       value="<?=$request->get('form_text_64')?>"
					       pattern="^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))"
					       maxlength='15'
					       placeholder="Телефон 7__________"
					       title="Номер должен содержать только цифры и начинаться с либо с 8, либо с +7"
					       required/>
				</div>
				<div class="form-group">
					<label>Email (необязательно)</label>
					<input type="text" class="form-control email" name="form_email_65" value="<?=$request->get('form_text_65')?>" placeholder="Email адрес"/>
				</div>
			</div>

			<div class="col-12 col-md-6">
				<div class="form-group">
					<label>Ваши пожелания или вопрос по отелю</label>
					<textarea class="form-control" name="form_textarea_66" cols="55" rows="6" placeholder=""><?=$request->get('form_textarea_66')?></textarea>
				</div>

				<div class="form-check text-muted small">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" name="confirm-agreement" checked id="confirm-agreement_question">
						Я согласен с <a href="/about/agreement/" target="_blank">условиями</a> обработки персональных данных
					</label>
				</div>

				<? if ($arResult["isUseCaptcha"] == "Y") : ?>
					<b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b>
					<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>"/>
					<p><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?></p>
					<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="120" height="34"/>
					<input type="text" name="captcha_word" class="form-control"/>
				<? endif; ?>

				<input type="hidden" class="form-control" name="form_text_67" value="http://newtravel.su/hotel/<?=$arParams['HOTEL_ID']?>/"/>
				<input type="hidden" name="web_form_apply" value="Y"/>
				<div class="">
					<input type="submit" class="btn btn-primary btn-block" value="Отправить"/>
				</div>
			</div>
		</div>
		<?=$arResult["FORM_FOOTER"]?>
	<? } ?>
</div>

<? if (isset($formresult) && (isset($form_id) && $form_id == 7)): ?>
	<script>
        $(document).ready(function () {
            yaCounter24395911.reachGoal('EXPERT_QUESTION_FORM_SEND');
            setTimeout(function () {
                $('#thankYouModal').modal('show');
            }, 1000)
        });
	</script>
<? endif; ?>

<script>
    $(document).ready(function () {
        //Если email пустой, то заполняем его по умолчанию
        $('.question-form-wrap form').on('submit', function (e) {
            if (!$('#confirm-agreement_question').prop('checked')) {
                e.preventDefault();
                alert('Подтвердите согласие с условиями обработки персональных данных');
                return false;
            }
        })
    });
</script>