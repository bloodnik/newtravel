<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Application;

$request    = Application::getInstance()->getContext()->getRequest();
$formresult = $request->get('formresult');
$form_id    = $request->get('WEB_FORM_ID');

$rand = rand(5, 100);

$this->setFrameMode(true);

?>
<style>
	.nav.nav-justified li {
		display: table-cell;
	}

	.nav.nav-justified li.active h4 {
		color: #ffffff;
	}

	.vue-form-wizard .wizard-nav-pills > li > a {
		color: rgba(0, 0, 0, 0.5);
	}

	.btn-outline-primary:hover, .btn-outline-primary:focus {
		background-color: #fff;
	}

	.scrollable-menu {
		height: auto;
		max-height: 200px;
		overflow-x: hidden;
	}

	.scrollable-menu > span {
		cursor: pointer;
	}

	.submit-btn.running {
		pointer-events: none;
	}

	[v-cloak] {
		display: none;
	}
</style>

<script>
    var country_name = '<?= defined("PROMO_COUNTRY_NAME") ? PROMO_COUNTRY_NAME : 'Таиланд'?>';
</script>

<div class="pick_form_alert">
	<? if ($arResult["isFormNote"] == "Y") : ?>
		<div class="alert alert-success">
			<?=$arResult["FORM_NOTE"]?>
		</div>
	<? endif; ?>
</div>


<? if ($arResult["isFormNote"] != "Y") : ?>
	<div id="pick_form_<?=$rand?>" style="background-color: rgba(255,255,255,0.7)" v-cloak>
		<form name="<?=$arResult["WEB_FORM_NAME"]?>" action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data"
		      :class="{'was-validated' : nameStringHasError || phoneStringHasError}">
			<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>">
			<?=bitrix_sessid_post()?>
			<form-wizard @on-complete="onComplete"
			             shape="round"
			             title="Оставь заявку на подбор тура прямо сейчас и получи 3 лучших варианта через 15 минут"
			             subtitle=""
			             color="#3498db"
			             error-color="#ff4949">

				<tab-content title="КУДА" icon="fa fa-globe">

					<div class="row">
						<div class="col-md-12">
							<? if ($arResult["isFormErrors"] == "Y"): ?>
								<?=$arResult["FORM_ERRORS_TEXT"];?>

								<script>
                                    $(document).ready(function () {
                                        $('#PickMeTourModal').modal('show');
                                    })
								</script>

							<? endif; ?>
						</div>
					</div>

					<div class="row">
						<div class="col-12">

							<div class="from-group mb-2">
								<label>Откуда</label>
								<select class="form-control" v-model="departureString">
									<option v-for="item in obDepartures">{{item.name}}</option>
								</select>
								<input type="hidden" name="form_text_88" :value="departureString">
							</div>

							<div class="from-group">
								<label>Выберите страну</label>
								<select class="form-control" v-model="countryString">
									<option v-for="item in obCountries">{{item.name}}</option>
								</select>
								<input type="hidden" name="form_text_89" :value="countryString">
							</div>
						</div>
					</div>
				</tab-content>

				<tab-content title="КОГДА" icon="fa fa-calendar-o" :before-change="validateSecondtStep">
					<div class="row">
						<div class="col-md-12 text-center dateRangeSelect">
							<label>Выберите даты</label>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group" :class="{'has-error' : dateStringHasError}">
								<input type="text" autocomplete="false" name="form_text_90" v-model="dateString" @click.once="getDateRangePicker" class="form-control daterange" readonly>
							</div>
						</div>
					</div>

					<div class="center">
						<div class="row">
							<div class="col-md-12 text-center">
								<label>Количество ночей</label>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 mb-2">
								<div class="input-group">
						            <span class="input-group-btn">
						              <button type="button" class="btn btn-primary btn-number" :disabled="nightsfrom == 1" @click="nightsfrom--">
						                  <span class="fa fa-minus"></span>
						              </button>
						            </span>
									<input type="text" name="form_text_91" class="form-control input-number" v-model="nightsfrom" min="1" max="40" readonly>
									<span class="input-group-btn">
						              <button type="button" class="btn btn-primary btn-number" @click="nightsfrom++">
						                  <span class="fa fa-plus"></span>
						              </button>
						          </span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-btn">
										<button type="button" class="btn btn-primary btn-number" :disabled="nightsto == 1" @click="nightsto--">
											<span class="fa fa-minus"></span>
										</button>
									</span>
									<input type="text" name="form_text_92" class="form-control input-number" v-model="nightsto" min="1" max="40" readonly>
									<span class="input-group-btn">
										<button type="button" class="btn btn-primary btn-number" @click="nightsto++">
											<span class="fa fa-plus"></span>
										</button>
									</span>
								</div>
							</div>

						</div>
					</div>

				</tab-content>

				<tab-content title="КТО" icon="fa fa-users">

					<div class="row">
						<div class="col-md-12 text-center dateRangeSelect">
							<label>Выберите количество туристов</label>
						</div>
					</div>

					<!--ТУРИСТЫ-->
					<div class="row">
						<div class="col-12 col-md-4 mb-3 text-center">
							<label>Взрослых</label>
							<br>
							<div class="btn-group" role="group" aria-label="First group">
								<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-white' : adults == 1}" @click="adults=1">1</button>
								<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : adults == 2}" @click="adults=2">2</button>
								<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : adults == 3}" @click="adults=3">3</button>
								<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : adults == 4}" @click="adults=4">4</button>
							</div>
						</div>

						<div class="col-12 col-md-4 mb-3 text-center">
							<label>Детей</label>
							<br>
							<div class="btn-group" role="group" aria-label="Second group">
								<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : child == 0}" @click="child=0">0</button>
								<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : child == 1}" @click="child=1; parseInt(childage1) > 2? '':childage1 = 2; childage2 = 0; childage3 = 0">1</button>
								<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : child == 2}" @click="child=2; parseInt(childage1) > 2? '':childage1 = 2; parseInt(childage2) > 2? '':childage2 = 2; childage3 =
								0">2
								</button>
								<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : child == 3}" @click="child=3; parseInt(childage1) > 2? '':childage1 = 2; parseInt(childage2) > 2? '':childage2 = 2; parseInt
								(childage3) >
						2? '':childage3 = 2">3
								</button>
							</div>
						</div>

						<!--ВОЗРАСТ ДЕТЕЙ-->
						<div class="col-12 col-md-4 mb-3 text-center">
							<label>Возраст детей</label>
							<br>
							<div class="btn-group" role="group" aria-label="Third group">
								<div class="btn-group" role="group">
									<button type="button" class="dropdown-toggle btn btn-primary" :class="{'disabled' : child == 0 }" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{childage1 | displayAgeInt}}</button>
									<div class="dropdown-menu select-dropdown" aria-labelledby="btnGroupDrop1">
										<div class="scrollable-menu">
											<span class="dropdown-item" :class="{'bg-primary text-white' : childage1 == age}" @click="childage1 = age" v-for="age in 16">{{age | displayAge}}</span>
										</div>
									</div>
								</div>
								<div class="btn-group" role="group">
									<button type="button" class="dropdown-toggle btn btn-primary" :class="{'disabled' : child < 2 }" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{childage2 | displayAgeInt}}</button>
									<div class="dropdown-menu select-dropdown" aria-labelledby="btnGroupDrop1">
										<div class="scrollable-menu">
											<span class="dropdown-item" :class="{'bg-primary text-white' : childage2 == age}" @click="childage2 = age" v-for="age in 16">{{age | displayAge}}</span>
										</div>
									</div>
								</div>
								<div class="btn-group" role="group">
									<button type="button" class="dropdown-toggle btn btn-primary" :class="{'disabled' : child < 3 }" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{childage3 | displayAgeInt}}</button>
									<div class="dropdown-menu select-dropdown" aria-labelledby="btnGroupDrop1">
										<div class="scrollable-menu">
											<span class="dropdown-item" :class="{'bg-primary text-white' : childage3 == age}" @click="childage3 = age" v-for="age in 16">{{age | displayAge}}</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<br>
					<div class="row justify-content-md-center">
						<div class="col-12 col-md-6 text-center">
							<p class="lead">
								{{touristString}}
							</p>
						</div>
					</div>

					<br>

					<input type="hidden" name="form_text_93" v-model="touristString" class="hidden">
					<input type="hidden" name="form_rand" value="<?=$rand?>" class="hidden">

				</tab-content>

				<tab-content title="ФИНИШ" icon="fa fa-check" :before-change="validateLastStep">

					<div class="row">
						<div class="col-md-12">
							<div class="from-group mb-2" :class="{'has-error' : nameStringHasError}">
								<label>Как Вас зовут?</label>
								<input type="text" required name="form_text_86" v-model="name" class="form-control" placeholder="Ваше имя">
								<small class="text-danger" :class="{'d-none' : !nameStringHasError}">Введите Ваше имя</small>
							</div>
							<div class="from-group mb-2" :class="{'has-error' : phoneStringHasError}">
								<label>Ваш телефон</label>
								<input type="text" required name="form_text_87" v-model="phone" class="form-control phone" placeholder="+79271234567">
								<small class="text-danger" :class="{'d-none' : !phoneStringHasError}">Неверный номер телефона. Введите в формате +79271234567</small>
							</div>
							<div class="from-group">
								<label>Дополнительные пожелания</label>
								<textarea v-model="message" name="form_textarea_94" class="form-control" placeholder=""></textarea>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<input type="checkbox" checked name="confirm-agreement"><label for='confirm-agreement'>Я согласен с <a href="/about/agreement/" target="_blank">условиями</a> обработки персональных данных</label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<? if ($arResult["isUseCaptcha"] == "Y") : ?>
								<b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b>
								<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>"/>
								<div class="row">
									<div class="col-md-5">
										<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="120" height="34"/>
									</div>
									<div class="col-md-7">
										<input type="text" name="captcha_word" class="form-control"/>

									</div>
								</div>
								<br>
							<? endif; ?>
						</div>
					</div>
					<input type="hidden" name="web_form_apply" value="Y"/>
					<input type="hidden" name="web_form_apply" value="Y"/>
				</tab-content>

				<a href="javascript:void(0)" class="btn btn-lg btn-orange text-white" slot="prev">НАЗАД</a>
				<a href="javascript:void(0)" class="btn btn-lg btn-orange text-white" slot="next">ДАЛЬШЕ</a>
				<a href="javascript:void(0)" class="btn btn-lg btn-success text-white ld-over-inverse submit-btn" slot="finish">
					ОТПРАВИТЬ
					<div class="ld ld-ring ld-spin"></div>
				</a>
			</form-wizard>
		</form>
	</div>
<? endif; ?>


<script>

    $(document).ready(function () {
        window.pick_form_<?=$rand?> = new Vue({
            el: '#pick_form_<?=$rand?>',
            data: {
                datefrom: moment().add(1, 'days').format('DD.MM.YYYY'),
                dateto: moment().add(15, 'days').format('DD.MM.YYYY'),
                nightsfrom: "9",
                nightsto: "12",
                countryId: '',


                adults: 2,
                child: 0,
                childage1: 0,
                childage2: 0,
                childage3: 0,

                touristString: "2 взр",
                name: "",
                phone: "",
                dateString: moment().add(1, 'days').format('DD.MM.YYYY') + " - " + moment().add(15, 'days').format('DD.MM.YYYY'),
                countryString: (typeof country_name !== "undefined") ? country_name : 'Таиланд',
                departureString: "Уфа",
                message: "",

                obDepartures: [],
                obCountries: [],

                dateStringHasError: false,
                nameStringHasError: false,
                phoneStringHasError: false,

                submitting: false,

                rand: <?=$rand?>,
            },
            created: function () {
                this.init();
            },

            methods: {
                init: function () {
                    var self = this;

                    this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "departure"}}).then(function (response) {
                            this.obDepartures = response.body.lists.departures.departure;
                            //this.obDepartures = _.sortBy(this.obDepartures, ['name']);
                        }
                    );

                    this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "country", cndep: this.departure}}).then(function (response) {
                        if (response.body.lists.countries.country !== null) {
                            this.obCountries = response.body.lists.countries.country;
                            //this.obCountries = _.sortBy(this.obCountries, ['name']);
                        }

                    });
                },

                getDateRangePicker: function () {
                    var self = this;

                    $('.daterange').dateRangePicker({
                        format: 'DD.MM.YYYY',
                        startOfWeek: 'monday',
                        singleMonth: 'auto',
                        showShortcuts: false,
                        autoClose: true,
                        selectForward: true,
                        stickyMonths: true,
                        separator: '-',
                        minDays: 1,
                        customOpenAnimation: function (cb) {
                            $(this).fadeIn(300, cb);
                        },
                        customCloseAnimation: function (cb) {
                            $(this).fadeOut(300, cb);
                        }
                    }).bind('datepicker-change', function (event, obj) {
                        self.datefrom = moment(obj.date1).format("DD.MM.YYYY");
                        self.dateto = moment(obj.date2).format("DD.MM.YYYY");
                        self.dateStringHasError = false;
                        self.dateString = self.datefrom + " - " + self.dateto
                    });

                    $('.daterange').data('dateRangePicker').open()
                },


                setTouristsString: function () {
                    var self = this;

                    self.touristString = self.adults + " взр. ";
                    if (self.child > 0) {
                        self.touristString += self.child + " реб.(";
                        for (var i = 1; i <= self.child; i++) {
                            self.touristString += self['childage' + i];
                            if (i !== self.child) {
                                self.touristString += ' и '
                            }
                        }
                        self.touristString += ")";
                    }
                },

                validateSecondtStep: function () {
                    if (this.dateString.length === 0) {
                        this.dateStringHasError = true;
                        return false;
                    }
                    return true;
                },

                validateLastStep: function () {
                    var hasError = false;
                    this.nameStringHasError = false;
                    this.phoneStringHasError = false;

                    if (this.name.length === 0) {
                        this.nameStringHasError = true;
                        hasError = true;
                    }

                    if (this.phone.search(/^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))/) === -1) {
                        this.phoneStringHasError = true;
                        hasError = true;
                    }
                    return !hasError;

                },

                onComplete: function () {
                    var self = this;


                    var form = $('#pick_form_<?=$rand?> form'),
                        form_wrapper_name = '.pick_form_alert',
                        form_wrapper = $('.pick_form_alert'),
                        form_action = form.attr('action'),
                        form_submit_btn = form.find('.submit-btn'),
                        form_agreement = form.find('input[name=confirm-agreement]'),
                        modal = $('#pickMeTourModal');


                    form.on('submit', function (e) {
                        form_submit_btn.toggleClass('running');
                        e.preventDefault();

                        if (!self.submitting) {
                            self.submitting = true; //Идет оптравка формы

                            if (!form_agreement.prop('checked')) {
                                alert('Подтвердите согласие с условиями обработки персональных данных');
                                form_submit_btn.toggleClass('running');
                                return false;
                            }

                            $.post(form_action, form.serialize(), function (data) {
                                form_wrapper.replaceWith($(data).find(form_wrapper_name)[0]);

                                $('html, body').stop().animate({
                                    scrollTop: $('body').offset().top + 160
                                }, 200);

                                $('#pick_form_<?=$rand?>').hide();

                                setTimeout(function () {
                                    modal.modal('hide');
                                }, 2000);

                                yaCounter24395911.reachGoal('PICK_ME_TOUR_FORM_SEND');
                                form_submit_btn.html('ОТПРАВЛЕНО <div class="ld ld-ring ld-spin"></div>').toggleClass('running');
                                self.submitting = false; //Отправка завершена
                            })

                        }
                    });

                    form.submit();
                }
            },


            filters: {
                displayAge: function (value) {
                    if (!value) {
                        return "";
                    } else {
                        var titles = ['год', 'года', 'лет'];
                        value = parseInt(value);
                        return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                    }
                },

                displayAgeInt: function (value) {
                    if (parseInt(value) >= 2) {
                        return value;
                    } else if (parseInt(value) <= 1) {
                        return "1";
                    }
                },

                displayNights: function (value) {
                    if (!value) {
                        return "";
                    } else {
                        var titles = ['ночь', 'ночи', 'ночей'];
                        value = parseInt(value);
                        return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                    }
                },

                displayTourists: function (value) {
                    var result = value + " взр. ",
                        child = pick_form_<?=$rand?>.child;
                    if (child > 0) {
                        result += child + " реб.(";
                        for (var i = 1; i <= child; i++) {
                            result += pick_form_<?=$rand?>['childage' + i];
                            if (i !== child) {
                                result += ', '
                            }
                        }
                        result += ")";
                    }
                    return result;
                },


                formatPrice: function (value) {
                    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
                }

            },
            watch: {
                adults: function () {
                    this.setTouristsString();
                },
                child: function () {
                    this.setTouristsString();
                },
                childage1: function () {
                    this.setTouristsString();
                },
                childage2: function () {
                    this.setTouristsString();
                },
                childage3: function () {
                    this.setTouristsString();
                },

            }
        });
    });
</script>