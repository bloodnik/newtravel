<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Application;

$request    = Application::getInstance()->getContext()->getRequest();
$formresult = $request->get('formresult');
$form_id    = $request->get('WEB_FORM_ID');

$this->setFrameMode(true);
?>
<div class="row job-form-wrap">
	<div class="col-12 align-self-center">
		<div class="display-6 mb-3 text-center text-md-left">
			Оставьте нам свое резюме и мы с Вами свяжемся!
		</div>
		<form name="<?=$arResult["WEB_FORM_NAME"]?>" action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data">
			<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>">
			<?=bitrix_sessid_post()?>
			<div class="small">
				<? if ($arResult["isFormErrors"] == "Y"): ?>
					<?=$arResult["FORM_ERRORS_TEXT"];?>
				<? endif; ?>
				<? if ($arResult["isFormNote"] == "Y") : ?>
					<div class="alert alert-success">
						<?=$arResult["FORM_NOTE"]?>
					</div>
				<? endif; ?>
			</div>

			<div class="form-group">
				<label>Ваше имя*</label>
				<input class="form-control" name="form_text_102" type="text" value="<?=$request->get('form_text_102')?>" placeholder="Введите имя" required>
			</div>
			<div class="form-group">
				<label>Телефон*</label>
				<input class="form-control"
				       type="text"
				       aria-invalid="false" aria-required="true"
				       name="form_text_103" value="<?=$request->get('form_text_103')?>"
				       pattern="^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))"
				       maxlength='15'
				       placeholder="Телефон 7__________"
				       title="Номер должен содержать только цифры и начинаться с либо с 8, либо с +7"
				       required/>
			</div>

			<div class="form-group">
				<label>Резюме</label>
				<input type="file" class="form-control" name="form_file_104">
			</div>

			<input type="hidden" name="web_form_apply" value="Y"/>
			<button type="submit" class="btn btn-primary btn-block ld-over-inverse" value="Y">
				Отправить
				<div class="ld ld-ring ld-spin"></div>
			</button>


			<div class="form-check text-muted small">
				<label class="form-check-label">
					<input class="form-check-input" type="checkbox" name="confirm-agreement" checked>
					Я согласен с <a href="/about/agreement/" target="_blank">условиями</a> обработки персональных данных
				</label>
			</div>
		</form>
	</div>
</div>

<script>

    function ajaxSend(target) {
        event.preventDefault();

        console.log(target);
        
        var form = $(target),
            form_wrapper_name = '.job-form-wrap',
            form_wrapper = $('.job-form-wrap'),
            form_action = form.attr('action'),
            form_submit_btn = form.find('button[type=submit]'),
            form_agreement = form.find('input[name=confirm-agreement]');


        form_submit_btn.toggleClass('running');

        if (!form_agreement.prop('checked')) {
            alert('Подтвердите согласие с условиями обработки персональных данных');
            form_submit_btn.toggleClass('running');
            return false;
        }

        $.post(form_action, form.serialize(), function (data) {
            console.log(data);
            form_wrapper.replaceWith($(data).find(form_wrapper_name));
            form_submit_btn.toggleClass('running');
        })
    }
</script>