<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Application;

$request    = Application::getInstance()->getContext()->getRequest();
$formresult = $request->get('formresult');
$form_id    = $request->get('WEB_FORM_ID');

$this->setFrameMode(true);

$justyfy = 'justify-content-center';
$agreement_justyfy = 'text-center';
if ($arParams['TEMPLATE_JUSTIFY']) {
	$justyfy           = "justify-content-" . $arParams['TEMPLATE_JUSTIFY'];
	$agreement_justyfy = "text-" . $arParams['TEMPLATE_JUSTIFY'];
}
?>

<div class="row <?=$justyfy?>">
	<? if ($arResult["isFormNote"] == "Y") : ?>
		<div class="col-auto">
			<div class="alert alert-success">
				<?=$arResult["FORM_NOTE"]?>
			</div>
		</div>
	<? endif; ?>
	<div class="col-12">
		<div class="callback-form-inline-wrap">
			<?=$arResult["FORM_HEADER"]?>
			<div class="form-row d-flex <?=$justyfy?>">
				<div class="col-12 small">
					<? if ($arResult["isFormErrors"] == "Y"): ?>
						<?=$arResult["FORM_ERRORS_TEXT"];?>
						<script>
                            $(document).ready(function () {
                                $('#callbackModal').modal('show');
                            })
						</script>
					<? endif; ?>
				</div>
				<div class="col-12 col-md-5 mb-2 mb-md-0">
					<label class="sr-only" for="inlineFormInput">Телефона</label>
					<input type="text" class="form-control form-control-lg mb-2 mb-sm-0"
					       aria-invalid="false" aria-required="true"
					       name="form_text_75" value="<?=$request->get('form_text_75')?>"
					       pattern="^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))"
					       maxlength='15'
					       placeholder="Телефон 7__________"
					       title="Номер должен содержать только цифры и начинаться с либо с 8, либо с +7"
					       required/>
				</div>
				<div class="col-12 col-md-3">
					<input type="hidden" name="web_form_apply" value="Y"/>
					<button type="submit" class="btn btn-orange btn-lg btn-block" value="Y">Хочу в <? if (isset($arParams["COUNTRY_NAME"])): ?><?=$arParams["COUNTRY_NAME"]?><? endif; ?></button>
				</div>
				<div class="col-12 <?=$agreement_justyfy?> text-white">
					<div class="form-check small">
						<label class="form-check-label">
							<input class="form-check-input" type="checkbox" name="confirm-agreement" checked id="confirm-agreement_callback">
							Я согласен с <a class="text-white" href="/about/agreement/" target="_blank">условиями</a> обработки персональных данных
						</label>
					</div>
				</div>
			</div>
			<?=$arResult["FORM_FOOTER"]?>
		</div>
	</div>
</div>


<? if (isset($formresult) && (isset($form_id) && $form_id == 9)): ?>
	<script>
        $(document).ready(function () {
            yaCounter24395911.reachGoal('CALLBACK_FORM_SEND');
            setTimeout(function () {
                $('#thankYouModal').modal('show');
            }, 1000)
        });
	</script>
<? endif; ?>

<script>
    $(document).ready(function () {
        //Если email пустой, то заполняем его по умолчанию
        $('.callback-form-inline-wrap form').on('submit', function (e) {
            if (!$('#confirm-agreement_callback').prop('checked')) {
                e.preventDefault();
                alert('Подтвердите согласие с условиями обработки персональных данных');
                return false;
            }
        })
    });
</script>