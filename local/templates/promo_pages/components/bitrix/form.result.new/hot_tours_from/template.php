<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Application;

$request    = Application::getInstance()->getContext()->getRequest();
$formresult = $request->get('formresult');
$form_id    = $request->get('WEB_FORM_ID');

$rand = rand(5, 50);

$this->setFrameMode(true);
?>

<!--Результаты поиска туров-->
<div class="row hottours-form-wrap<?=$rand?>">
	<div class="col align-self-center">
		<form name="<?=$arResult["WEB_FORM_NAME"]?>" action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data" onsubmit="ajaxSendHot<?=$rand?>(this)">
			<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>">
			<?=bitrix_sessid_post()?>
			<? if ($arResult["isFormErrors"] == "Y"): ?>
				<?=$arResult["FORM_ERRORS_TEXT"];?>
			<? endif; ?>

			<div class="display-8 mb-3 text-center text-md-left">
				<p>Интересует горящий тур? <br> Мы подберем самые выгодные предложения и перезвоним Вам в течение 15 минут!</p>
			</div>
			<? if ($arResult["isFormNote"] == "Y") : ?>
				<div class="alert alert-success">
					<?=$arResult["FORM_NOTE"]?>
				</div>
			<? endif; ?>
			<div class="form-row align-items-center">
				<div class="col-12 col-md-8 mb-2 mb-md-0">
					<label class="sr-only" for="inlineFormInput">Телефона</label>
					<input type="text"
					       class="form-control mb-2 mb-sm-0"
					       name="form_text_52"
					       value="<?=$request->get('form_text_52')?>"
					       pattern="^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))"
					       maxlength='15'
					       placeholder="Телефон 7__________"
					       title="Номер должен содержать только цифры и начинаться с либо с 8, либо с +7"
					       required/>
				</div>

				<div class="col-12 col-md-4">
					<input type="hidden" name="web_form_apply" value="Y"/>
					<button type="submit" class="btn btn-primary btn-block ld-over-inverse" value="Y">
						Заказать
						<div class="ld ld-ring ld-spin"></div>
					</button>
				</div>

				<div class="col-12">
					<div class="form-check text-muted small">
						<label class="form-check-label">
							<input class="form-check-input" type="checkbox" name="confirm-agreement" checked id="confirm-agreement_hottours">
							Я согласен с <a href="/about/agreement/" target="_blank">условиями</a> обработки персональных данных
						</label>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script>

    function ajaxSendHot<?=$rand?>(target) {
        event.preventDefault();

        var form = $(target),
            form_wrapper_name = '.hottours-form-wrap<?=$rand?>',
            form_wrapper = $('.hottours-form-wrap<?=$rand?>'),
            form_action = form.attr('action'),
            form_submit_btn = form.find('button[type=submit]'),
            form_agreement = form.find('input[name=confirm-agreement]');


        form_submit_btn.toggleClass('running');
        if (!form_agreement.prop('checked')) {
            alert('Подтвердите согласие с условиями обработки персональных данных');
            form_submit_btn.toggleClass('running');
            return false;
        }

        $.post(form_action, form.serialize(), function (data) {
            form_wrapper.replaceWith('<div class="h3 text-white">Спасибо! Ваша заявка принята.</div>');
            yaCounter24395911.reachGoal('HOT_TOURS_FORM_SEND');
            form_submit_btn.toggleClass('running');
        })
    }
</script>