<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Application;

$request    = Application::getInstance()->getContext()->getRequest();
$formresult = $request->get('formresult');
$form_id    = $request->get('WEB_FORM_ID');

$this->setFrameMode(true);
?>
<div class="budget-form-inline-wrap">
	<? if ($arParams['HIDE_TITLE'] !== 'Y'): ?>
		<div class="row mb-2">
			<div class="col-12 text-center">
				<h3 class="display-6 text-white">А что есть под Ваш бюджет?</h3>
			</div>
		</div>
	<? endif; ?>

	<div class="row justify-content-center">
		<? if ($arResult["isFormNote"] == "Y") : ?>
			<div class="col-auto">
				<div class="alert alert-success">
					<?=$arResult["FORM_NOTE"]?>
				</div>
			</div>
		<? endif; ?>
		<div class="col-12">
			<?=$arResult["FORM_HEADER"]?>
			<div class="form-row d-flex justify-content-center">
				<div class="form-row d-flex justify-content-center">
					<div class="col-12 small">
						<? if ($arResult["isFormErrors"] == "Y"): ?>
							<?=$arResult["FORM_ERRORS_TEXT"];?>
							<script>
                                $(document).ready(function () {
                                    $('#callbackModal').modal('show');
                                })
							</script>
						<? endif; ?>
					</div>
					<div class="col-12 col-md-3 mb-2 mb-md-0">
						<input class="form-control form-control-lg mb-2 mb-sm-0" type="text" name="form_text_95" value="<?=$request->get('form_text_95')?>" placeholder="Укажите Ваш бюджет">
					</div>
					<div class="col-12 col-md-3 mb-2 mb-md-0">

						<input type="text" class="form-control form-control-lg mb-2 mb-sm-0"
						       aria-invalid="false" aria-required="true"
						       name="form_text_96" value="<?=$request->get('form_text_96')?>"
						       pattern="^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))"
						       maxlength='15'
						       placeholder="Телефон 7__________"
						       title="Номер должен содержать только цифры и начинаться с либо с 8, либо с +7"
						       required/>
					</div>
					<div class="col-12 col-md-3">
						<input type="hidden" name="web_form_apply" value="Y"/>
						<button type="submit" class="btn btn-primary btn-lg btn-block" value="Y">Хочу узнать</button>
					</div>
					<div class="col-12 text-center text-white">
						<div class="form-check small">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" name="confirm-agreement" checked id="confirm-agreement_budget">
								Я согласен с <a class="text-white" href="/about/agreement/" target="_blank">условиями</a> обработки персональных данных
							</label>
						</div>
					</div>
				</div>
			</div>
			<?=$arResult["FORM_FOOTER"]?>
		</div>
	</div>
</div>

<? if (isset($formresult) && (isset($form_id) && $form_id == 3)): ?>
	<script>
        $(document).ready(function () {
            yaCounter24395911.reachGoal('BUDGET_FORM_SEND');
            setTimeout(function () {
                $('#thankYouModal').modal('show');
            }, 1000)
        });
	</script>
<? endif; ?>

<script>
    $(document).ready(function () {
        //Если email пустой, то заполняем его по умолчанию
        $('.budget-form-inline-wrap form').on('submit', function (e) {
            if (!$('#confirm-agreement_budget').prop('checked')) {
                e.preventDefault();
                alert('Подтвердите согласие с условиями обработки персональных данных');
                return false;
            }
        })
    });
</script>