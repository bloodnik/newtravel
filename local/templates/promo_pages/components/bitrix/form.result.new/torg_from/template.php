<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Application;

$request    = Application::getInstance()->getContext()->getRequest();
$formresult = $request->get('formresult');
$form_id    = $request->get('WEB_FORM_ID');

$this->setFrameMode(true);
?>
<div class="row torg-form-wrap">
	<div class="col-12 align-self-center">
		<div class="display-6 mb-3 text-center text-md-left">
			Может быть поторгуемся?
		</div>
		<div class="alert alert-warning">Оставьте заявку и мы снизим цену на тур, либо подберем аналогичный по цене вариант!</div>
		<form name="<?=$arResult["WEB_FORM_NAME"]?>" action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data" onsubmit="ajaxSend(this)">
			<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>">
			<?=bitrix_sessid_post()?>
			<div class="small">
				<? if ($arResult["isFormErrors"] == "Y"): ?>
					<?=$arResult["FORM_ERRORS_TEXT"];?>
				<? endif; ?>
				<? if ($arResult["isFormNote"] == "Y") : ?>
					<div class="alert alert-success">
						<?=$arResult["FORM_NOTE"]?>
					</div>
				<? endif; ?>
			</div>

			<div class="form-group">
				<label>Ваше имя*</label>
				<input class="form-control" name="form_text_97" type="text" placeholder="Введите имя" required>
			</div>
			<div class="form-group">
				<label>Телефон*</label>
				<input class="form-control"
				       type="text"
				       aria-invalid="false" aria-required="true"
				       name="form_text_98" value="<?=$request->get('form_text_75')?>"
				       pattern="^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))"
				       maxlength='15'
				       placeholder="Телефон 7__________"
				       title="Номер должен содержать только цифры и начинаться с либо с 8, либо с +7"
				       required/>
			</div>

			<div class="form-group">
				<label>Ссылка на тур у конкурента*</label>
				<input class="form-control" name="form_text_99" type="text" placeholder="" required>
				<small class="form-text text-muted">Предоставьте ссылку на сайт конкурента с данным туром</small>
			</div>

			<div class="form-group">
				<label>Ваш комментарий</label>
				<textarea class="form-control" name="form_textarea_100" rows="3"></textarea>
			</div>

			<div class="alert alert-info">
				Правила:
				<ul>
					<li>Предоставьте ссылку на сайт конкурента с данным туром</li>
					<li>Параметры тура должны быть аналогичны выбранному</li>
				</ul>
			</div>

			<input type="hidden" name="form_text_101" value="">
			<input type="hidden" name="web_form_apply" value="Y"/>
			<button type="submit" class="btn btn-primary btn-block ld-over-inverse" value="Y">
				Торгуемся
				<div class="ld ld-ring ld-spin"></div>
			</button>


			<div class="form-check text-muted small">
				<label class="form-check-label">
					<input class="form-check-input" type="checkbox" name="confirm-agreement" checked>
					Я согласен с <a href="/about/agreement/" target="_blank">условиями</a> обработки персональных данных
				</label>
			</div>
		</form>
	</div>
</div>

<script>

    function ajaxSend(target) {
        event.preventDefault();

        var form = $(target),
            form_wrapper_name = '.torg-form-wrap',
            form_wrapper = $('.torg-form-wrap'),
            form_action = form.attr('action'),
            form_submit_btn = form.find('button[type=submit]'),
            form_agreement = form.find('input[name=confirm-agreement]');


        form_submit_btn.toggleClass('running');

        if (!form_agreement.prop('checked')) {
            alert('Подтвердите согласие с условиями обработки персональных данных');
            form_submit_btn.toggleClass('running');
            return false;
        }

        $.post(form_action, form.serialize(), function (data) {
            form_wrapper.replaceWith($(data).find(form_wrapper_name));
            yaCounter24395911.reachGoal('TORG_FORM_SEND');
            form_submit_btn.toggleClass('running');

            setTimeout(function () {
                $('#torgModal').modal('hide');
            }, 1500)
        })
    }
</script>