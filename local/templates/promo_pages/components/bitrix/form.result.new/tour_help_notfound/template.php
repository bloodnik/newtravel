<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();

$this->setFrameMode(true);
?>
<!--Результаты поиска туров-->
<div class="tour-list-wrap help-form-wrap">
	<div class="tour-item">
		<div class="col-md-12">
			<? if ($arResult["isFormErrors"] == "Y"): ?>
				<?=$arResult["FORM_ERRORS_TEXT"];?>
			<? endif; ?>

			<?=$arResult["FORM_NOTE"]?>

			<? if ($arResult["isFormNote"] != "Y") : ?>
				<?=$arResult["FORM_HEADER"]?>
				<h4 class="modal-title">Упс...</h4>
				<p>
					К сожалению, по Вашему запросу подходящих вариантов не найдено. <br/>
					Попробуйте изменить критерии поиска (даты вылета, кол-во ночей)
				</p>

				<div class="col-md-5">
					<div class="row">
						<div class="from-group">
							<input type="text" class="form-control" name="form_text_57" value="<?=$request->get('form_text_57')?>" placeholder="Ваше имя" required/>
						</div>
					</div>
					<div class="row">
						<div class="from-group">
							<label></label>
							<input type="text" class="form-control phone"
							       name="form_text_58"
							       value="<?=$request->get('form_text_58')?>"
							       pattern="^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))"
							       maxlength='15'
							       placeholder="Телефон 7__________"
							       title="Номер должен содержать только цифры и начинаться с либо с 8, либо с +7"
							       required/>
						</div>
					</div>
					<div class="row">
						<div class="from-group">
							<label></label>
							<input type="email" class="form-control email" name="form_text_59" value="<?=$request->get('form_text_59')?>" placeholder="Email адрес"/>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<textarea class="form-control" name="form_textarea_60" cols="55" rows="6" style="resize: none;padding-bottom: 21px;" id="help-form-message_notfound"><?=$request->get('form_textarea_60')?></textarea>
				</div>

				<div class="col-md-12">
					<input type="checkbox" checked name="confirm-agreement" id="confirm-agreement"><label for='confirm-agreement'>Я согласен с <a href="/about/agreement/" target="_blank">условиями</a> обработки персональных данных</label>
				</div>


				<div class="col-md-12">
					<? if ($arResult["isUseCaptcha"] == "Y") : ?>
						<b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b>
						<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>"/>
						<div class="row">
							<div class="col-md-5">
								<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="120" height="34"/>
							</div>
							<div class="col-md-7">
								<input type="text" name="captcha_word" class="form-control"/>
							</div>
						</div>
						<br>
					<? endif; ?>
				</div>

				<div class="col-md-12">
					<input type="hidden" name="web_form_apply" value="Y"/>
					<input type="submit" class="btn btn-blue pull-right" onclick="yaCounter24395911.reachGoal('TOUR_HELP_NOTFOUND')" value="Отправить запрос на подбор"/>
				</div>
			<?=$arResult["FORM_FOOTER"]?>
			<? else: ?>
				<script>
                    yaCounter24395911.reachGoal('TOUR_HELP_NOTFOUND_SEND');
				</script>
			<? endif; ?>
		</div>
	</div>
</div>
<script>
    $(document).ready(function () {

        if (searchObject) {
            $("#help-form-message_notfound").val(searchObject.params.helpformmessage); //Подставляем сообщение в форму подбора
            $("#user-url_notfound").val(searchObject.params.userurl);
        }

        //Если email пустой, то заполняем его по умолчанию
        $('.help-form-wrap form').on('submit', function (e) {
            if (!$('#confirm-agreement').prop('checked')) {
                e.preventDefault();
                alert('Подтвердите согласие с условиями обработки персональных данных');
                BX.closeWait();
                return false;
            }
        })
    });
</script>