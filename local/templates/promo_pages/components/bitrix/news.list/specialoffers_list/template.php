<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$xlItemsCount = $arParams['XL_ITEMS_COUNT'] == '3' ? '4' : '3';

?>
<style>
	.specialoffers .card-img-overlay {
		background-color: rgba(0, 0, 0, 0.2);

		-webkit-transition: background .3s ease;
		-moz-transition: background .3s ease;
		-ms-transition: background .3s ease;
		-o-transition: background .3s ease;
		transition: background .3s ease;
	}

	.specialoffers .card-img-overlay:hover {
		background-color: rgba(0, 0, 0, 0.0);
	}

	.specialoffers .card-title {
		font-size: 1.5em;
	}

	.specialoffers .card-text.price {
		font-size: 1.3em;
		font-weight: bold;
	}

</style>

<div class="row specialoffers">
	<? foreach ($arResult["ITEMS"] as $arItem): ?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>


		<div class="card text-white p-0 col-12 col-md-6 col-lg-4 col-xl-<?=$xlItemsCount?> mb-3" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<a href="<?=$arItem['DISPLAY_PROPERTIES']['URL']['DISPLAY_VALUE']?>" rel="nofollow" class="text-white" style="background: url('<?=$arItem['PREVIEW_PICTURE']['SRC']?>')">
				<img class="card-img" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
				<meta itemprop="image" content="<?=$arItem['PREVIEW_PICTURE']['SRC']?>"/>
				<div class="card-img-overlay">
					<h4 class="card-title"><?=$arItem['NAME']?></h4>
					<p class="card-text price">от <span><?=CurrencyFormat($arItem['DISPLAY_PROPERTIES']['PRICE']['DISPLAY_VALUE'], "RUB")?></span> / чел.</p>
				</div>
			</a>
		</div>

	<? endforeach ?>
</div>