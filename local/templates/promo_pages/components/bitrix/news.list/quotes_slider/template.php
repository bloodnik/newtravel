<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<section class="bg-dark pt-5 pb-5">
	<div class="container">

		<div class="page-heading text-center text-white">
			<h2 class="display-5">Отзывы наших клиентов</h2>
		</div>


		<div class="text-white quote-carousel">
			<? foreach ($arResult["ITEMS"] as $arItem): ?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<div class="slide-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<blockquote class="quote-box">
						<div class="row d-flex">
							<div class="col-12 col-md-2 mb-3 align-self-center">
								<img class="mx-auto rounded-circle" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="">
							</div>
							<div class="col-12 col-md-10 align-self-center">
								<p class="quote-text" style="height: 200px; overflow: hidden">
									<?=$arItem['PREVIEW_TEXT']?>
								</p>
								<? if (strlen($arItem['PREVIEW_TEXT']) > 400): ?>
									<div class="row text-right">
										<button class="btn btn-link" onclick="$(this).parent().parent().find('.quote-text').toggleClass('open')">Подробнее</button>
									</div>
								<? endif; ?>
								<ul class="list-inline mt-2">
									<? if ($arItem['DISPLAY_PROPERTIES']['COUNTRY']):?>
										<li class="list-inline-item"><strong>Страна:</strong> <?=$arItem['DISPLAY_PROPERTIES']['COUNTRY']['VALUE']?></li>
									<? endif; ?>
									<? if ($arItem['DISPLAY_PROPERTIES']['REGION']):?>
										<li class="list-inline-item"><strong>Курорт:</strong> <?=$arItem['DISPLAY_PROPERTIES']['REGION']['VALUE']?></li>
									<? endif; ?>
									<? if ($arItem['DISPLAY_PROPERTIES']['HOTEL']):?>
										<li class="list-inline-item"><strong>Отель:</strong> <?=$arItem['DISPLAY_PROPERTIES']['HOTEL']['VALUE']?></li>
									<? endif; ?>
									<? if ($arItem['DISPLAY_PROPERTIES']['RATING']):?>
										<li class="list-inline-item"><strong>Рейтинг:</strong> <?=$arItem['DISPLAY_PROPERTIES']['RATING']['VALUE']?></li>
									<? endif; ?>
								</ul>
							</div>
						</div>
						
						<hr class="bg-secondary">
						<div class="blog-post-actions">
							<p class="blog-post-bottom pull-left">
								<em><?=$arItem['NAME']?></em>
							</p>
						</div>
					</blockquote>
				</div>
			<? endforeach; ?>
		</div>

		<div class="text-center pt-2">
			<a class="btn btn-secondary" href="/reviews/">Все отзывы</a>
		</div>

	</div>
</section>