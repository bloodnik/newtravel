<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<section class="white-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="responses">
					<? foreach ($arResult["ITEMS"] as $arItem):
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
						?>
						<div class="responses-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
							<div class="author">
								<? if ($arItem["PREVIEW_PICTURE"]): ?>
									<img class="avatar" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>">
								<? else: ?>
									<img class="avatar" src="<?=$this->GetFolder()?>/images/noava.png" alt="<?=$arItem["NAME"]?>">
								<? endif ?>
								<div class="author-detail">
										<span class="author-name">
											<? if ($arItem['DISPLAY_PROPERTIES']['DISPLAY_NAME']): ?>
												<?=$arItem['DISPLAY_PROPERTIES']['DISPLAY_NAME']['DISPLAY_VALUE']?>
											<? else: ?>
												<?=$arItem["NAME"]?>
											<? endif ?>
										</span>
								</div>
							</div>

							<blockquote>
								<p><i class="fa fa-calendar" style="color: #00aeef"></i> <?=FormatDate("d F Y", MakeTimeStamp($arItem["ACTIVE_FROM"], "DD.MM.YYYY"))?></p>
								<? if ($arItem["PREVIEW_TEXT"]): ?>
									<?=$arItem["PREVIEW_TEXT"]?>
								<? elseif ($arItem["DETAIL_TEXT"]): ?>
									<?=$arItem["DETAIL_TEXT"]?>
								<? endif ?>

								<ul class="list-inline mt-2">
									<? if ($arItem['DISPLAY_PROPERTIES']['COUNTRY']):?>
										<li class="list-inline-item"><strong>Страна:</strong> <?=$arItem['DISPLAY_PROPERTIES']['COUNTRY']['VALUE']?></li>
									<? endif; ?>
									<? if ($arItem['DISPLAY_PROPERTIES']['REGION']):?>
										<li class="list-inline-item"><strong>Курорт:</strong> <?=$arItem['DISPLAY_PROPERTIES']['REGION']['VALUE']?></li>
									<? endif; ?>
									<? if ($arItem['DISPLAY_PROPERTIES']['HOTEL']):?>
										<li class="list-inline-item"><strong>Отель:</strong> <?=$arItem['DISPLAY_PROPERTIES']['HOTEL']['VALUE']?></li>
									<? endif; ?>
									<? if ($arItem['DISPLAY_PROPERTIES']['RATING']):?>
										<li class="list-inline-item"><strong>Рейтинг:</strong> <?=$arItem['DISPLAY_PROPERTIES']['RATING']['VALUE']?></li>
									<? endif; ?>

								</ul>

							</blockquote>
						</div>
					<? endforeach ?>
				</div>
			</div>
			<div class="col-md-12">
				<?=$arResult["NAV_STRING"]?>
			</div>
		</div>
	</div>
</section>