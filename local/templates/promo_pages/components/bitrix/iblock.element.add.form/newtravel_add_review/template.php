<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<style>
	.modal-body > .row.review-add-wrap > div {
		width: 100%;
	}
	.rating-group i{
		cursor: pointer;
	}
</style>
<div class="container">
	<form class="form" name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">

		<? if (count($arResult["ERRORS"])): ?>
			<?=ShowError(implode("<br />", $arResult["ERRORS"]))?>
		<? endif ?>
		<? if (strlen($arResult["MESSAGE"]) > 0): ?>
			<br/>
			<?=ShowNote($arResult["MESSAGE"])?>
		<? endif ?>

		<?=bitrix_sessid_post()?>

		<div id="review">

			<? if ($arParams["MAX_FILE_SIZE"] > 0): ?>
				<input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>"/>
			<? endif ?>

			<div class="row">
				<div class="col">
					<div class="form-group" id="name-form">
						<label for="PROPERTY_NAME_0">Ваше имя *</label>
						<input type="text" id="PROPERTY_NAME_0" name="PROPERTY[NAME][0]" class="form-control" value="<?=$arResult['ELEMENT']['NAME']?>"/>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<label>Страна</label>
					<div class="dropdown">
						<div class="form-control country-select-btn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							{{countryName}}
						</div>
						<div class="dropdown-menu select-dropdown bg-light" aria-labelledby="dropdownMenuButton">
							<div class="col-12">
								<div class="input-group input-group-sm">
									<input type="text" v-model="countrySearchQuery" class="form-control" placeholder="поиск страны..." aria-label="поиск страны...">
									<span class="input-group-addon" v-show="countrySearchQuery.length > 0" @click="countrySearchQuery=''"><i class="fa fa-close"></i></span>
									<span class="input-group-addon"><i class="fa fa-search"></i></span>
								</div>
							</div>
							<div class="scrollable-menu">
								<h6 class="dropdown-header">Популярные</h6>
								<span class="dropdown-item" :class="{'bg-primary text-white': country == item.id}" @click="country = item.id; countryName = item.name;" v-if="IsPopularCountry(item.id)" v-for="item in filteredCountries">{{item
								.name}}</span>
								<h6 class="dropdown-header">Все страны</h6>
								<span class="dropdown-item" :class="{'bg-primary text-white': country == item.id}" @click="country = item.id; countryName = item.name;" v-if="!IsPopularCountry(item.id)" v-for="item in filteredCountries">{{item
								.name}}</span>
							</div>
						</div>
					</div>
					<input type="hidden" id="PROPERTY_960_0" name="PROPERTY[960][0]" v-model="countryName">
				</div>
			</div>

			<div class="row">
				<div class="col-12 mt-3">
					<label>Курорт</label>
					<div class="dropdown">
						<div class="form-control country-select-btn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							{{regionName}}
						</div>
						<div class="dropdown-menu select-dropdown bg-light" aria-labelledby="dropdownMenuButton">
							<div class="col-12">
								<div class="input-group input-group-sm">
									<input type="text" v-model="regionSearchQuery" class="form-control" placeholder="поиск курорта..." aria-label="поиск курорта...">
									<span class="input-group-addon" v-show="regionSearchQuery.length > 0" @click="regionSearchQuery=''"><i class="fa fa-close"></i></span>
									<span class="input-group-addon"><i class="fa fa-search"></i></span>
								</div>
							</div>
							<div class="scrollable-menu">
								<span class="dropdown-item" :class="{'bg-primary text-white': region == item.id}" @click="region = item.id; regionName = item.name;" v-for="item in filteredRegions">{{item
								.name}}</span>
							</div>
						</div>
					</div>
					<input type="hidden" id="PROPERTY_1009_0" name="PROPERTY[1009][0]" v-model="regionName">
				</div>
			</div>

			<div class="row">
				<div class="col-12 mt-3">
					<label>Отель</label>
					<div class="dropdown">
						<div class="form-control country-select-btn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							{{hotelName}}
						</div>
						<div class="dropdown-menu select-dropdown bg-light" aria-labelledby="dropdownMenuButton">
							<div class="col-12">
								<div class="input-group input-group-sm">
									<input type="text" v-model="hotelSearchQuery" class="form-control" placeholder="поиск отеля..." aria-label="поиск отеля...">
									<span class="input-group-addon" v-show="hotelSearchQuery.length > 0" @click="hotelSearchQuery=''"><i class="fa fa-close"></i></span>
									<span class="input-group-addon"><i class="fa fa-search"></i></span>
								</div>
							</div>
							<div class="scrollable-menu">
								<span class="dropdown-item" :class="{'bg-primary text-white': hotel == item.id}" @click="hotel = item.id; hotelName = item.name;" v-for="item in filteredHotels">{{item
								.name}}</span>
							</div>
						</div>
					</div>
					<input type="hidden" id="PROPERTY_1008_0" name="PROPERTY[1008][0]" v-model="hotelName">
					<input type="hidden" id="PROPERTY_959_0" name="PROPERTY[959][0]" v-model="hotel">
				</div>
			</div>

			<div class="row">
				<div class="col">
					<div class="form-group mt-3 rating-group">
						<label for="PROPERTY_NAME_0">Ваша оценка</label>
						<br>
						<i class="fa text-warning" :class="[rating > 0 ? 'fa-star': 'fa-star-o']" @click="rating=1"></i>
						<i class="fa text-warning" :class="[rating > 1 ? 'fa-star': 'fa-star-o']" @click="rating=2"></i>
						<i class="fa text-warning" :class="[rating > 2 ? 'fa-star': 'fa-star-o']" @click="rating=3"></i>
						<i class="fa text-warning" :class="[rating > 3 ? 'fa-star': 'fa-star-o']" @click="rating=4"></i>
						<i class="fa text-warning" :class="[rating > 4 ? 'fa-star': 'fa-star-o']" @click="rating=5"></i>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col">
					<div class="form-group" id="message-form">
						<label for="message">Ваш отзыв *</label>
						<textarea id="message" class="form-control" name="PROPERTY[PREVIEW_TEXT][0]"><?=$arResult['ELEMENT']['PREVIEW_TEXT']?></textarea>
					</div>
				</div>
			</div>


			<input type="hidden" id="PROPERTY_911_0" name="PROPERTY[911][0]" value="<?=$USER->GetID()?>"/>
			<input type="hidden" id="PROPERTY_1007_0" name="PROPERTY[1007][0]" v-model="rating"/>


			<? if ($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0): ?>
				<div class="formgroup">
					<label><?=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?> &nbsp;</label>
					<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>"/>
					<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA"/>
					<input type="text" class="form-control" name="captcha_word" maxlength="50" value="">
				</div>
			<? endif ?>
			<br>
			<div class="row">
				<div class="col">
					<input type="submit" name="iblock_submit" class="btn btn-blue" value="Отправить отзыв!"/>
				</div>
			</div>
			<br><br>
		</div>
	</form>
</div>

<script>
    $(document).ready(function () {
        window.review = new Vue({
            el: '#review',
            data: {
                message: 'Привет, Vue!',
                countryName: 'Таиланд',
                hotelName: '',
                regionName: '',
	            country: 2,
                region: null,
                hotel: null,
                obCountries: [],
                obRegions: [],
                obHotels: [],
                rating: 3,
                countrySearchQuery: '',
                regionSearchQuery: '',
                hotelSearchQuery: '',
                pop_countries: [9, 2, 16, 3, 4, 12, 47],
            },
            created: function () {
                this.getCountries();
            },
            methods: {
                getCountries: function () {
                    this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "country", cndep: 4}}).then(response => {
                        if (response.body.lists.countries.country !== null) {
                            this.obCountries = response.body.lists.countries.country;
	                        var cName = this.countryName;
	                        this.getRegions();
                            this.getHotels();
                        }

                    });
                },
                getRegions: function () {
                    this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "region", regcountry: this.country}}).then(response => {
                        var _this = this;
                        _this.obRegions = response.body.lists.regions.region;
                        _this.regionName = _this.obRegions[0].name;
                        _this.region = _this.obRegions[0].id;
                    });
                },

                getHotels: function () {
                    this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "hotel", hotcountry: this.country, hotregion: this.region}}).then(response => {
                        var _this = this;
                        _this.obHotels = response.body.lists.hotels.hotel;
                        _this.hotelName = _this.obHotels[0].name;
                        _this.hotel = _this.obHotels[0].id;
                    });
                },

                /**
                 * Фильтр популярных стран
                 * @return {boolean}
                 */
                IsPopularCountry: function (country) {
                    return _.includes(this.pop_countries, parseInt(country));
                },

            },
	        computed: {
                filteredCountries: function () {
                    var self = this;
                    return self.obCountries.filter(function (country) {
                        return country.name.toLowerCase().indexOf(self.countrySearchQuery.toLowerCase()) !== -1
                    })
                },
                filteredRegions: function () {
                    var self = this;
                    return self.obRegions.filter(function (region) {
                        return region.name.toLowerCase().indexOf(self.regionSearchQuery.toLowerCase()) !== -1
                    })
                },
                filteredHotels: function () {
                    var self = this,
                        filtered = [];

                    if (self.obHotels !== null) {
                        filtered = self.obHotels.filter(function (hotel) {
                            return hotel.name.toLowerCase().indexOf(self.hotelSearchQuery.toLowerCase()) !== -1
                        });
                    }

                    return filtered.slice(0, 300);
                }
	        },
	        watch: {
                country: function (newCountry) {
                    this.getRegions();
                    this.getHotels();
                },
                region: function (region) {
	                this.getHotels();
                }
	        }
        })
    })
</script>