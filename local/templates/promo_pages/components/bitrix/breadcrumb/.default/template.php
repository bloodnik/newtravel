<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string
if(empty($arResult))
	return "";


	
$strReturn = '<nav aria-label="breadcrumb" role="navigation"><ol class="breadcrumb">';

$num_items = count($arResult);
for($index = 0, $itemSize = $num_items; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	
	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
		$strReturn .= '<li class="breadcrumb-item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="'.$arResult[$index]["LINK"].'" title="'.$title.'"><span itemprop="title">'.$title.'</span></a></li>';
	else
		$strReturn .= '<li class="breadcrumb-item" aria-current="page" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">'.$title.'</span></li>';
}

$strReturn .= '</ol></nav>';

return $strReturn;
?>