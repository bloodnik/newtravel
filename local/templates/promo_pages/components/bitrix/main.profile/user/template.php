<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
CJSCore::Init(array('date'));
?>

<div class="container">

	<form method="post" name="form" action="<?=$arResult["FORM_TARGET"]?>?" enctype="multipart/form-data">
		<?=$arResult["BX_SESSION_CHECK"]?>
		<input type="hidden" name="lang" value="<?=LANG?>"/>
		<input type="hidden" name="ID" value=<?=$arResult["ID"]?>/>

		<div class="row"><h4 class="col">Редактирование личных данных</h4></div>

		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Имя</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" value="<?=$arResult["arUser"]["NAME"]?>" name="NAME" placeholder="Ваше имя"/>
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Фамилия</label>
			<div class="col-sm-10">
				<input type="text" class="form-control email" name="LAST_NAME" value="<?=$arResult["arUser"]["LAST_NAME"]?>" placeholder="Фамилия"/>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Отчество</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="SECOND_NAME" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" placeholder="Отчество"/>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Дата рождения</label>
			<div class="col-sm-10">
				<input type="text" class="form-control datepicker" onclick="BX.calendar({node: this, field: this, bTime: false})" name="PERSONAL_BIRTHDAY" value="<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>" placeholder="Дата рождения"/>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Email</label>
			<div class="col-sm-10">
				<input type="email" class="form-control email" name="EMAIL" value="<?=$arResult["arUser"]["EMAIL"]?>" placeholder="Ваш email"/>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Телефон</label>
			<div class="col-sm-10">
				<input type="text" class="form-control phone" name="PERSONAL_PHONE" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" placeholder="+7(___) ___-__-__"/>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Фотография</label>
			<div class="col-sm-10">
				<input type="file" class="form-control" name="PERSONAL_PHOTO" value="<?=$arResult["arUser"]["PERSONAL_PHOTO"]?>" placeholder="Фотография"/>
				<? if (strlen($arResult["arUser"]["PERSONAL_PHOTO"]) > 0): ?>
					<?=$arResult["arUser"]["PERSONAL_PHOTO"]?>
				<? endif; ?>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label"><?=GetMessage('LOGIN')?></label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="LOGIN" value="<?=$arResult["arUser"]["LOGIN"]?>" placeholder="<?=GetMessage('LOGIN')?>"/>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label"><?=GetMessage('NEW_PASSWORD_REQ')?></label>
			<div class="col-sm-10">
				<input type="password" class="form-control" name="NEW_PASSWORD" value="" placeholder="<?=GetMessage('NEW_PASSWORD')?>"/>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label"><?=GetMessage('NEW_PASSWORD_CONFIRM')?></label>
			<div class="col-sm-10">
				<input type="password" class="form-control" name="NEW_PASSWORD_CONFIRM" value="" placeholder="<?=GetMessage('NEW_PASSWORD_CONFIRM')?>"/>
			</div>
		</div>
		<hr>

		<div class="row">
			<div class="col">
				<?=ShowError($arResult["strProfileError"]);?>
				<?
				//echo"<pre>";print_r($arResult);echo"</pre>";
				if ($arResult['DATA_SAVED'] == 'Y') {
					echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
				}
				?>
				<div class="form-group">
					<button type="submit" class="btn btn-primary" name="save" value="Y"><?=(($arResult["ID"] > 0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?></button>
					<br>
					<small style="margin-bottom: 10px; display: block;"><? echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]; ?></small>
					<br>
				</div>
			</div>
		</div>
	</form>
</div>