<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
?>

<? if ( ! defined("SHOW_FOOTER") && SHOW_FOOTER): ?>

	<? $APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include/smi.php"),
		Array(),
		Array("MODE" => "html", "SHOW_BORDER" => false)
	);
	?>


	<section class="" id="contacts">
		<div id="footerMap" style="width: 100%; height: 500px"></div>
<!--		<div id="googlemaps"></div>-->
		<div class="contact-card">
			<div class="card">
				<h4 class="card-header">Контакты</h4>
				<div class="card-body">
					<div class="row">
						<div class="col">
							<dl class="row">
								<dt class="col-sm-12">Адрес</dt>
								<dd class="col-sm-12">г. Уфа, ул. С. Перовской 50</dd>

								<dt class="col-sm-12">График работы</dt>
								<dd class="col-sm-12">
									Пн.- Пт. с 10.00 - 20.00 <br>
									Сб. с 11.00 - 17.00 <br>
									Бронирование on-line круглосуточно
								</dd>
								<dt class="col-sm-12">Телефоны</dt>
								<dd class="col-sm-12">
									+7(347)216-30-61 <br>
									8-800-505-47-61 (бесплатный)
								</dd>
							</dl>
						</div>
						<div class="col">
							<dl class="row">
								<dt class="col-sm-12">Адрес</dt>
								<dd class="col-sm-12">г. Уфа, ул. проспект Октября, 52</dd>

								<dt class="col-sm-12">График работы</dt>
								<dd class="col-sm-12">
									Пн.- Пт. с 10.00 - 20.00 <br>
									Сб. с 11.00 - 17.00 <br>
									Бронирование on-line круглосуточно
								</dd>
								<dt class="col-sm-12">Телефоны</dt>
								<dd class="col-sm-12">
									+7(347)216-30-81 <br>
									8-800-505-47-61 (бесплатный)
								</dd>
							</dl>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<footer class="bg-primary pt-3 pb-1 text-light">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6">
					Умныетуристы.рф © 2008 - 2019 <span class="text-dark">Все права защищены</span>
				</div>
				<div class="col-12 col-md-6 text-md-right">
					<ul class="list-inline">
						<li class="list-inline-item"><a href="https://vk.com/umnie_turisti"><i class="fa fa-vk fa-2x"></i></a></li>
						<li class="list-inline-item"><a href="https://www.facebook.com/groups/umnyetouristy/"><i class="fa fa-facebook-official fa-2x"></i></a></li>
						<li class="list-inline-item"><a href="https://www.instagram.com/umnye__turisty/"><i class="fa fa-instagram fa-2x"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

<? endif; ?>

<? if (!IS_PROMO): ?>
	<? $APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include/office.php"),
		Array(),
		Array("MODE" => "html", "SHOW_BORDER" => false)
	);
	?>
<? endif; ?>

<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/callback_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>
<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/pick_me_tour_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>

<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/question_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>

<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/hot_tours_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>

<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/torg_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>

<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/review_add_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>


<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/thank_you_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>

<? if (false): //Корзина туров - ОТКЛЮЧИЛИ, пока не актуально?>
	<? $APPLICATION->IncludeComponent(
		"newtravel.search:tours.basket.small",
		"",
		Array(
			"CACHE_TIME"  => "3600",
			"CACHE_TYPE"  => "A",
			"IBLOCK_ID"   => "",
			"IBLOCK_TYPE" => "content",
			"REQUEST_ID"  => "1",
			"TOUR_ID"     => "1"
		)
	); ?>
<? endif; ?>



</body>
</html>
