<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Newtravel\Search\DefineGeo;

if ( ! isset($_SESSION['CITY']) || empty($_SESSION['CITY'])) {
	Loader::includeModule('newtravel.search');
	DefineGeo::setRegionIso();
	DefineGeo::setCity();
}

$version = '2.5';
?>
<? define("PHONE", strlen($_SESSION['CITY']['UF_PHONE']) > 0 ? $_SESSION['CITY']['UF_PHONE'] : "8-800-505-47-61") ?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<meta property="og:type" content="website">
	<meta property="og:site_name" content="Умные туристы">
	<meta property="og:title" content="Умныетуристы.рф - сервис бронирования выгодных туров онлайн, покупка туров онлайн из Москвы по всем направлениям">
	<meta property="og:description" content="Умныетуристы.рф - сервис бронирования выгодных туров онлайн, покупка туров онлайн из Москвы по всем направлениям.">
	<meta property="og:url" content="https://xn--e1agkeqhedbe9fh.xn--p1ai/">
	<meta property="og:locale" content="ru_RU">
	<meta property="og:image" content="https://xn--e1agkeqhedbe9fh.xn--p1ai/local/templates/promo_pages/assets/i/logo.png">

	<link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico"/>

	<title><? $APPLICATION->ShowTitle() ?></title>
	<? echo '<meta http-equiv="Content-Type" content="text/html; charset=' . LANG_CHARSET . '"' . (true ? ' /' : '') . '>' . "\n";

	$APPLICATION->ShowMeta("robots", false, true);
	$APPLICATION->ShowMeta("keywords", false, true);
	$APPLICATION->ShowMeta("description", false, true);
	$APPLICATION->ShowLink("canonical", null, true);
	$APPLICATION->ShowCSS(true, true);
	$APPLICATION->ShowHeadStrings();
	$APPLICATION->ShowHeadScripts();

	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/style.min.css");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/vendor/jquery.min.js");
	Asset::getInstance()->addJs("https://maps.googleapis.com/maps/api/js?key=AIzaSyAKYVzO5rPM1Gqq8crmt9Bku0uvKhr7t4g&sensor=true");
	Asset::getInstance()->addJs("https://api-maps.yandex.ru/2.1/?lang=ru_RU");

	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/main.min.js");

	/*VUE JS*/
	if (DEV_MODE):
		Asset::getInstance()->addJs("https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.js");
	else:
		Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/vendor/vue/vue.min.js");
	endif;

	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/vendor/vue/vue-resource.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/vendor/vue/vue-form-wizard.js");

	?>
	<script src="https://posonline.kreditotdel.ru/widget/kreditotdel-connect-widget.js"></script>
</head>
<body>
<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/counters.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
); ?>

<div id="snow-animation-container"></div>
<? $APPLICATION->ShowPanel() ?>

<? if ( ! IS_PROMO): ?>
	<div class="top-line">
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<div class="container">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item active">
							<a class="nav-link" href="/tours/">Поиск тура <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-orange" href="/promotions/">Акции</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/blog/">Блог</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/job/">Вакансии</a>
						</li>

						<li class="nav-item">
							<a class="nav-link" href="/faq/how_to_pay/">Как оплатить?</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/hotels/">Страны</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/reviews/">Отзывы</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/faq/">Помощь</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/about/contacts.php">Контакты</a>
						</li>
					</ul>
					<ul class="navbar-nav">
						<? if ( ! $USER->IsAuthorized()): ?>
							<li class="nav-item">
								<div class="dropdown">
									<button class="nav-link btn-link dropdown-toggle px-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Войти</button>
									<div class="dropdown-menu" style="width: 250px;">
										<? if ($_REQUEST['forgot_password'] !== 'yes'): ?>
											<? $APPLICATION->IncludeComponent(
												"bitrix:system.auth.form",
												"newtravel",
												Array(
													"COMPONENT_TEMPLATE"   => ".default",
													"COMPOSITE_FRAME_MODE" => "A",
													"COMPOSITE_FRAME_TYPE" => "AUTO",
													"FORGOT_PASSWORD_URL"  => "",
													"PATH_TO_MYPORTAL"     => "desktop.php",
													"PROFILE_URL"          => "",
													"REGISTER_URL"         => "",
													"SHOW_ERRORS"          => "Y"
												)
											); ?>
										<? else: ?>
											<div class="p-3">
												<p><a href="/auth/">Авторизация</a></p>
											</div>
										<? endif ?>
									</div>
								</div>
							</li>
						<? else: ?>
							<?
							$name = trim($USER->GetFullName());
							if ( ! $name) {
								$name = trim($USER->GetLogin());
							}
							if (strlen($name) > 15) {
								$name = substr($name, 0, 12) . '...';
							}
							?>
							<div class="dropdown">
								<button class="text-secondary btn-link dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Здравствуйте, <?=$name?>
								</button>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="/personal/order/" rel="nofollow">Мои заказы</a>
									<a class="dropdown-item" href="/personal/profile/edit.php" rel="nofollow">Изменить регистрационные данные</a>
									<a class="dropdown-item" href="?logout=yes" rel="nofollow">Выход</a>
								</div>
							</div>
						<? endif; ?>
					</ul>
				</div>
			</div>
		</nav>
	</div>
<? endif; ?>

<? if ( ! IS_HOTELS): ?>
	<? $APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
			"AREA_FILE_RECURSIVE" => "Y",
			"AREA_FILE_SHOW"      => "sect",
			"AREA_FILE_SUFFIX"    => "header",
			"EDIT_TEMPLATE"       => ""
		)
	); ?>
<? endif; ?>

<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW"      => "sect",
		"AREA_FILE_SUFFIX"    => "operators",
		"EDIT_TEMPLATE"       => ""
	)
); ?>

<? if ( ! IS_INDEX && ! IS_TOURS && ! IS_HOTELS && ! IS_PROMO): ?>
	<div class="container mt-2">
		<? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", Array(
			"START_FROM" => "0",    // Номер пункта, начиная с которого будет построена навигационная цепочка
			"PATH"       => "",    // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
			"SITE_ID"    => "-",    // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
		),
			false
		); ?>
	</div>
<? endif; ?>
