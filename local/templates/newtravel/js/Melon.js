/*jshint new: true */
/*global $, jQuery, document, window, GnMenu, setInterval, post_arrow, post_item, navigator, mina, Snap, GMaps*/
/* ==========================================================================
Document Ready Function
========================================================================== */
jQuery(document).ready(function () {
    'use strict';


    //Открываем форму заказа звонка
    $('a[href="#callback"]').on('click', function () {
        $('#callBackModal').modal('show');
    });
    //Открываем форму подбора тура
    $('a[href="#pickmetour"]').on('click', function () {
        $('#PickMeTourModal').modal('show');
    });

    //Аккордеон детальная страница страны
    $('#ac-container').accordion({
        oneOpenedItem: true,
    });

    //Зарпещаем редактирование строки с датой
    $('input[name="daterange"]').focus(function() {
        this.blur();
    });

    var Topmenu, toggleanimatedPresimg, counter_url, $teamitems, $offeritems,  $portfolioitems4, $portfolioitems3, $portfolioitems2, /*social_networks,*/ formInput, $blogitems, $blogtlitems, onMobile, imgshinevar, map, mapintab, photosintab;


    /* fullwidth carousel swipe link fix
     -------------------------------------------------------------------------- */
    var $mousePosStart = 0;
    var $mousePosEnd = 0;
    $('.carousel-wrap[data-full-width="true"] .portfolio-items .col .work-item .work-info a').mousedown(function(e){
        $mousePosStart = e.clientX;
    });

    $('.carousel-wrap[data-full-width="true"] .portfolio-items .col .work-item .work-info a').mouseup(function(e){
        $mousePosEnd = e.clientX;
    });

    $('.carousel-wrap[data-full-width="true"] .portfolio-items .col .work-item .work-info a').click(function(e){
        if(Math.abs($mousePosStart - $mousePosEnd) > 10)  return false;
    });




    /* ==========================================================================
    ToolTip
    ========================================================================== */
    $("li[data-rel=tooltip]").tooltip();
    $("a[data-rel=modal-tooltip]").tooltip();
    $("a[data-rel=tooltip]").tooltip({container: 'body'});
    $("i[data-rel=tooltip]").tooltip({container: 'body'});
    $("span[data-rel=tooltip]").tooltip({container: 'body'});


    /* ==========================================================================
    Fancy Box
    ========================================================================== */
    $(".fancybox").fancybox({
        helpers : {
            title : {
                type : 'over'
            },
            overlay : {
                speedOut : 0,
                locked: false
            }
        }
    });
    
    /* ==========================================================================
    Scroll To top
    ========================================================================== */
    jQuery('.topcontrol a').click(function () {
        jQuery('html, body').animate({scrollTop: '0px'}, 800);
        return false;
    });

    /* Footer Toggle
    ========================================================================== */
    $('.footer-toogle i').click(function () {
        var state = $(this).data('toggleState');
        if (!state) {
            $('.footer .top-footer').slideDown('slow');
            $("html, body").animate({ scrollTop: $(document).height() }, "slow");
            $('.footer-toogle i').removeClass('fa-plus');
            $('.footer-toogle i').addClass('fa-minus');
            $('.footer-toogle').css({backgroundImage: 'linear-gradient(45deg, rgba(0, 0, 0, 0) 50%, #232323 50%, #232323 100%)'});
            $('.footer').css({borderTop: '8px solid #232323'});
            $('.topcontrol a').css({backgroundColor: '#232323'});
        } else {
            $('.footer .top-footer').slideUp('slow');
            $('.footer-toogle i').addClass('fa-plus');
            $('.footer-toogle i').removeClass('fa-minus');
            $('.footer-toogle').css({backgroundImage: 'linear-gradient(45deg, rgba(0, 0, 0, 0) 50%, #333333 50%, #333333 100%)'});
            $('.footer').css({borderTop: '8px solid #333333'});
            $('.topcontrol a').css({backgroundColor: '#333333'});
        }
        $(this).data('toggleState', !state);
    });

    $('.footer .top-footer').slideUp('slow');


    /* ==========================================================================
    on mobile?
    ========================================================================== */
	onMobile = false;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) { onMobile = true; }

	if (onMobile === true) {
        $("li[data-rel=tooltip]").tooltip('destroy');
        $("a[data-rel=modal-tooltip]").tooltip('destroy');
        $("a[data-rel=tooltip]").tooltip('destroy');
        $("i[data-rel=tooltip]").tooltip('destroy');
        $("span[data-rel=tooltip]").tooltip('destroy');

        /* Blur Section
        ---------------------------------------------------------------------------*/
        $(".blur2-section").css({background: 'transparent url(images/background/2.jpg) repeat center center'});
    }

    var homeSection = $('.demo-section'),
        navbar      = $('.navbar-custom'),
        navHeight   = navbar.height(),
        width       = Math.max($(window).width(), window.innerWidth),
        mobileTest;

    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        mobileTest = true;
    }
    navbarAnimation(navbar, homeSection, navHeight);
    navbarSubmenu(width);
    hoverDropdown(width, mobileTest);

    $(window).resize(function() {
        var width = Math.max($(window).width(), window.innerWidth);
        hoverDropdown(width, mobileTest);
    });

    $(window).scroll(function() {
        navbarAnimation(navbar, homeSection, navHeight);
    });

    /* ---------------------------------------------- /*
     * Transparent navbar animation
     /* ---------------------------------------------- */

    function navbarAnimation(navbar, homeSection, navHeight) {
        var topScroll = $(window).scrollTop();
        if (navbar.length > 0 && homeSection.length > 0) {
            if(topScroll >= navHeight) {
                navbar.removeClass('navbar-transparent');
                navbar.addClass('navbar-fixed-top');
            } else {
                navbar.addClass('navbar-transparent');
                navbar.removeClass('navbar-fixed-top');
            }
        }
    }

    /* ---------------------------------------------- /*
     * Navbar submenu
     /* ---------------------------------------------- */

    function navbarSubmenu(width) {
        if (width > 767) {
            $('.navbar-custom .navbar-nav > li.dropdown').hover(function() {
                var MenuLeftOffset  = $('.dropdown-menu', $(this)).offset().left;
                var Menu1LevelWidth = $('.dropdown-menu', $(this)).width();
                if (width - MenuLeftOffset < Menu1LevelWidth * 2) {
                    $(this).children('.dropdown-menu').addClass('leftauto');
                } else {
                    $(this).children('.dropdown-menu').removeClass('leftauto');
                }
                if ($('.dropdown', $(this)).length > 0) {
                    var Menu2LevelWidth = $('.dropdown-menu', $(this)).width();
                    if (width - MenuLeftOffset - Menu1LevelWidth < Menu2LevelWidth) {
                        $(this).children('.dropdown-menu').addClass('left-side');
                    } else {
                        $(this).children('.dropdown-menu').removeClass('left-side');
                    }
                }
            });
        }
    }

    /* ---------------------------------------------- /*
     * Navbar hover dropdown on desctop
     /* ---------------------------------------------- */

    function hoverDropdown(width, mobileTest) {
        if ((width > 767) && (mobileTest !== true)) {
            $('.navbar-custom .navbar-nav > li.dropdown, .navbar-custom li.dropdown > ul > li.dropdown').removeClass('open');
            var delay = 0;
            var setTimeoutConst;
            $('.navbar-custom .navbar-nav > li.dropdown, .navbar-custom li.dropdown > ul > li.dropdown').hover(function() {
                    var $this = $(this);
                    setTimeoutConst = setTimeout(function() {
                        $this.addClass('open');
                        $this.find('.dropdown-toggle').addClass('disabled');
                    }, delay);
                },
                function() {
                    clearTimeout(setTimeoutConst);
                    $(this).removeClass('open');
                    $(this).find('.dropdown-toggle').removeClass('disabled');
                });
        } else {
            $('.navbar-custom .navbar-nav > li.dropdown, .navbar-custom li.dropdown > ul > li.dropdown').unbind('mouseenter mouseleave');
            $('.navbar-custom [data-toggle=dropdown]').not('.binded').addClass('binded').on('click', function(event) {
                event.preventDefault();
                event.stopPropagation();
                $(this).parent().siblings().removeClass('open');
                $(this).parent().siblings().find('[data-toggle=dropdown]').parent().removeClass('open');
                $(this).parent().toggleClass('open');
            });
        }
    }

    /* ---------------------------------------------- /*
     * Navbar collapse on click
     /* ---------------------------------------------- */

    $(document).on('click','.navbar-collapse.in',function(e) {
        if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
            $(this).collapse('hide');
        }
    });

}); // JavaScript Document

Number.prototype.formatMoney = function(c, d, t){
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};