/********************************************** ОБЩИЕ ФУНКЦИИ *******************************************************/
var helpformmessage = "";
var userurl = "";
$(document).ready(function () {

    /* ==========================================================================
     Выбор ночей
     ========================================================================== */
    $("#nightsValue").on("click", function () {
        $("#nights-select").css("display", "table");
    });

    $("#nights-select table tr>td").on("click", function () {
        if (nto == 0 && nfrom > 0) { //Елси уже нажали на ночей От, выбираем ночей До
            var nightsTo = $(this).attr("data-value"); //Выбранное значение
            nto = parseInt(nightsTo); //Количество До
            if (nfrom <= nto) { //Если  ночей от меньше чем ночей до
                $(this).addClass("to-active");

                $("#nightsto").val(nightsTo); //Устанавливаем значение для элемента формы
                $("#nightsValue").text("от " + nfrom + " до " + nto + " ночей");

                //Обнуляем все
                $("#nights-select>span").text("от:");
                $("#nights-select table tr>td").removeClass("to-active");
                $("#nights-select table tr>td").removeClass("from-active");
                $("#nights-select table tr>td").removeClass("in-range");
                nfrom = 0;
                nto = 0;
                $("#nights-select").hide();
                $("#nights-error").css("display", "none");
            } else {
                console.log(nto);
                console.log(nfrom);
                $("#nights-error").slideDown(200);
                nto = 0;
            }
        }

        if (nfrom == 0 && nightsTo == undefined) { //Если еще не выбрали ночей От
            var nightsFrom = $(this).attr("data-value"); //Выбранное значение
            $("#nightsfrom").val(nightsFrom); //Устанавливаем значение для элемента формы
            nfrom = parseInt(nightsFrom); //Количество От
            $("#nights-select>strong").text("до:");
            $(this).addClass("from-active");
        }
    });

    $("#nights-select table tr>td").on("mouseover", function () {
        if (nfrom > 0) {
            var val = $(this).attr("data-value");
            $("#nights-select table tr>td").each(function (key, item) {
                var dataValue = $(this).attr("data-value");
                if (parseInt(dataValue) < parseInt(val) && parseInt(dataValue) > nfrom) {
                    $(this).addClass("in-range");
                } else {
                    $(this).removeClass("in-range");
                }
            })

        }
    });

    //Закрывает выбор ночей, если кликнули вне контейнера
    $(document).mouseup(function (e) {
        var container = $("#nights-select");
        if (container.has(e.target).length === 0) {
            container.hide();
        }
    });


    /*=========================================================================
     mCustomScrollBar
     ========================================================================== */
    $(".customScroll").mCustomScrollbar();


    /*=========================================================================
     Kid's age
     ========================================================================== */
    // Show guestsblock onClick
    var touristsbox = $('.tourists');
    var touriststotal = $('.tourists-total>span');
    var kidscount = $("#child").val();
    var adulscount = $("#adults").val();


    $(".adultsCount div").on("click", function () {
        $(".adultsCount div").removeClass("active");
        adulscount = $(this).attr("data-count");
        $(this).addClass("active");
        $("#adults").val(adulscount);
        if (kidscount > 0) {
            $(touriststotal).text(adulscount + " взр. " + kidscount + "реб.");
        } else {
            $(touriststotal).text(adulscount + " взр.");
        }
    });

    $("#noKids").on("click", function () {
        kidscount = 0;
        $("#child").val(kidscount);
        if (kidscount > 0) {
            $(touriststotal).text(adulscount + " взр. " + kidscount + "реб.");
        } else {
            $(touriststotal).text(adulscount + " взр.");
        }
        $(".kidsCount div").removeClass("active");
        $(".kidsAge").css("display", "none");
        $("#age1SelectBoxItContainer").css("display", "none");
        $("#age2SelectBoxItContainer").css("display", "none");
        $("#age3SelectBoxItContainer").css("display", "none");
    });

    $(".kidsCount div").on("click", function () {
        kidscount = $(this).attr("data-count");

        if (kidscount > 0) {
            $(touriststotal).text(adulscount + " взр. " + kidscount + "реб.");
        } else {
            $(touriststotal).text(adulscount + " взр.");
        }

        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $("#child").val(0);
            $(".kidsAge").css("display", "none");
            $("#childage1SelectBoxItContainer").css("display", "none");
            $("#childage2SelectBoxItContainer").css("display", "none");
            $("#childage3SelectBoxItContainer").css("display", "none");

            $(touriststotal).text(adulscount + " взр.");

            return false;
        }

        $(".kidsCount div").removeClass("active");
        $("#child").val(kidscount);

        switch (parseInt(kidscount)) {
            case 1:
                $(this).addClass("active");
                $(".kidsAge").css("display", "block");
                $("#childage1SelectBoxItContainer").css("display", "inline-block");
                $("#childage2SelectBoxItContainer").css("display", "none");
                $("#childage3SelectBoxItContainer").css("display", "none");
                break;
            case 2:
                $(this).addClass("active");
                $(".kidsAge").css("display", "block");
                $("#childage1SelectBoxItContainer").css("display", "inline-block");
                $("#childage2SelectBoxItContainer").css("display", "inline-block");
                $("#childage3SelectBoxItContainer").css("display", "none");
                break;
            case 3:
                $(this).addClass("active");
                $(".kidsAge").css("display", "block");
                $("#childage1SelectBoxItContainer").css("display", "inline-block");
                $("#childage2SelectBoxItContainer").css("display", "inline-block");
                $("#childage3SelectBoxItContainer").css("display", "inline-block");
                break;
            default:
                $(".kidsCount div").removeClass("active");
                $(".kidsAge").css("display", "none");
                $("#childage1SelectBoxItContainer").css("display", "none");
                $("#childage2SelectBoxItContainer").css("display", "none");
                $("#childage3SelectBoxItContainer").css("display", "none");
        }
    });

    var touristselect = $('.toursit-select .form-control');
    var save = $('.toursit-select .button-save');

    touristselect.click(function () {
        touristsbox.fadeIn(120);
    });
    save.click(function () {
        touristsbox.fadeOut(120);
    });

    //Закрывает выбор туристов, если кликнули вне контейнера
    var hide = true;//Флаг закрытия окна - нужен для определения выбора из селекта вне окна
    $(document).mouseup(function (e) {
        var container = $(".tourists");
        if (container.has(e.target).length === 0) {
            if (hide) {
                container.hide();
            }
        }
    });

    //События селектов, устанавливают флаг hide в false, после выбора через 1 секудну возвращает в true
    $("select#childage1,select#childage2,select#childage3").bind({
        close: function () {
            hide = false;
            setTimeout(setHide, 1000);
        }
    });
    function setHide() {
        hide = true;
    }

});

/********************************************** ЗАПОЛНЕНИЕ ФОРМЫ *******************************************************/

//Получаем список городов вылета и заполняем модальное окно
function getDepartures(currentCity) {

    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "departure"}, function (json) {
        $('#departureList').empty();

        $('#departureList').append($('<li class="divider"></li>'));
        $('#departureList').append($('<li><a href="javascript:void(0)" onclick="selectDeparture(99, \'Без перелета\')">Без перелета</a></li>'));
        $('#departureList').append($('<li class="divider">А</li>'));

        var firstLetter = "А";
        $.each(json.lists.departures.departure, function (i, departure) {

            if (firstLetter !== departure.namefrom.substr(0, 1)) {
                $('#departureList').append($('<li class="divider">' + departure.namefrom.substr(0, 1) + '</li>'));
            }
            firstLetter = departure.namefrom.substr(0, 1);

            if (departure.id == 99) {
                return
            }

            if (departure.id == currentCity) {
                $("#departure").val(departure.id);
                $("#departureValue").text(departure.namefrom);
                $('#departureList').append($('<li class="active"><a href="javascript:void(0)" onclick="selectDeparture(' + departure.id + ', \'' + departure.namefrom + '\')">' + departure.namefrom + '</a> </li>'));
            } else {
                $('#departureList').append($('<li><a href="javascript:void(0)" onclick="selectDeparture(' + departure.id + ', \'' + departure.namefrom + '\')">' + departure.namefrom + '</a> </li>'));
            }
        });
    });

    /* ==========================================================================
     Разбивка списка на колонки
     ========================================================================== */
    setTimeout(function () {
        $('.multiColumn').autocolumnlist({columns: 4});
    }, 2000);

}

//Получаем список стран. Если выбран город вылета то задействован параметр cndep
function getCountries(cndep, currentCountry) {
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "country", cndep: cndep}, function (json) {
        $('#countryList').empty();

        $.each(json.lists.countries.country, function (i, country) {
            if (country.id == currentCountry) {
                $("#country").val(country.id);
                $("#countryValue").text(country.name);
                $('#countryList').append($('<li class="active"><a href="javascript:void(0)" onclick="selectDeparture(' + country.id + ', \'' + country.name + '\')">' + country.name + '</a> </li>'));
            } else {
                $('#countryList').append($('<li><a href="javascript:void(0)" onclick="selectCountry(' + country.id + ', \'' + country.name + '\')">' + country.name + '</a> </li>'));
            }
        });
    });
    /* ==========================================================================
     Разбивка списка на колонки
     ========================================================================== */
    setTimeout(function () {
        $('.multiColumn').autocolumnlist({columns: 4});
    }, 1000);
}


//Получаем спиоск дат вылета
function getFlydates(flydeparture, flycountry) {
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "flydate", flydeparture: flydeparture, flycountry: flycountry}, function (json) {
        /* daterangepicker
         ========================================================================== */
        //
        $('input[name="daterange"]').val();
        $('input[name="daterange"]').daterangepicker({
            autoApply: true,
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-blue',
            cancelClass: 'btn-black',
            separator: '-',
            availableDates: json.lists.flydates.flydate,
            locale: {
                format: 'DD.MM.YYYY',
                applyLabel: 'Выбрать',
                cancelLabel: 'Отмена',
                fromLabel: 'С',
                toLabel: 'По',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                firstDay: 1
            }
        });
    });
}