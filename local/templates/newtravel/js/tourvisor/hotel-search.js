/********************************************** ОБЩИЕ ФУНКЦИИ *******************************************************/
$( document ).ready(function() {

    /*=========================================================================
     mCustomScrollBar
     ========================================================================== */
    $(".customScroll").mCustomScrollbar();

    /* ==========================================================================
     Выбор ночей
     ========================================================================== */
    $("#nightsValue").on("click", function(){
        $("#nights-select").css("display", "table");
    });

    $("#nights-select table tr>td").on("click", function(){
        if(nto == 0 && nfrom > 0){ //Елси уже нажали на ночей От, выбираем ночей До
            var nightsTo = $(this).attr("data-value"); //Выбранное значение
            nto = parseInt(nightsTo); //Количество До
            if(nfrom <= nto) { //Если  ночей от меньше чем ночей до
                $(this).addClass("to-active");

                $("#nightsto").val(nightsTo); //Устанавливаем значение для элемента формы
                $("#nightsValue").text("от " + nfrom + " до "+ nto);

                //Обнуляем все
                $("#nights-select>span").text("от:");
                $("#nights-select table tr>td").removeClass("to-active");
                $("#nights-select table tr>td").removeClass("from-active");
                $("#nights-select table tr>td").removeClass("in-range");
                nfrom = 0;
                nto = 0;
                $("#nights-select").hide();
                $("#nights-error").css("display", "none");
            }else{
                $("#nights-error").slideDown(200);
                nto=0;
            }
        }

        if(nfrom == 0 && nightsTo == undefined){ //Если еще не выбрали ночей От
            var nightsFrom = $(this).attr("data-value"); //Выбранное значение
            $("#nightsfrom").val(nightsFrom); //Устанавливаем значение для элемента формы
            nfrom = parseInt(nightsFrom); //Количество От
            $("#nights-select>strong").text("до:");
            $(this).addClass("from-active");
        }
    });

    $("#nights-select table tr>td").on("mouseover", function(){
        if(nfrom>0){
            var val = $(this).attr("data-value");
            $("#nights-select table tr>td").each(function(key, item){
                var dataValue = $(this).attr("data-value");
                if(parseInt(dataValue) < parseInt(val) && parseInt(dataValue) > nfrom) {
                    $(this).addClass("in-range");
                }else{
                    $(this).removeClass("in-range");
                }
            })

        }
    });

    //Закрывает выбор ночей, если кликнули вне контейнера
    $(document).mouseup(function (e) {
        var container = $("#nights-select");
        if (container.has(e.target).length === 0){
            container.hide();
        }
    });

    /*=========================================================================
     Kid's age
     ========================================================================== */
    // Show guestsblock onClick
    var touristsbox = $('.tourists');
    var touriststotal = $('.tourists-total>span');
    var kidscount = $("#child").val();
    var adultscount = $("#adults").val();


    $(".adultsCount div").on("click", function(){
        $(".adultsCount div").removeClass("active");
        adultscount = $(this).attr("data-count");
        $(this).addClass("active");
        $("#adults").val(adultscount);
        if(kidscount >0) {
            $(touriststotal).text(adultscount + " взр. " + kidscount + "реб.");
        }else{
            $(touriststotal).text(adultscount + " взр.");
        }
    });

    $("#noKids").on("click", function(){
        $(this).css("color","#ccc");

        kidscount = 0;
        $("#child").val(kidscount);
        if(kidscount >0) {
            $(touriststotal).text(adultscount + " взр. " + kidscount + "реб.");
        }else{
            $(touriststotal).text(adultscount + " взр.");
        }
        $(".kidsCount div").removeClass("active");
        $(".kidsAge").css("display", "none");
        $("#age1SelectBoxItContainer").css("display", "none");
        $("#age2SelectBoxItContainer").css("display", "none");
        $("#age3SelectBoxItContainer").css("display", "none");
    });

    $(".kidsCount>div").on("click", function(clickEvent){

        $("#noKids").css({"color":"#E00300", "text-decoration":"underline"});

        kidscount = $(this).attr("data-count");

        if(kidscount >0) {
            $(touriststotal).text(adultscount + " взр. " + kidscount + "реб.");
        }else{
            $(touriststotal).text(adultscount + " взр.");
        }

        if($(this).hasClass("active")){
            console.log(this);
            $(this).removeClass("active");
            $("#child").val(0);
            $(".kidsAge").css("display", "none");
            $("#childage1SelectBoxItContainer").css("display", "none");
            $("#childage2SelectBoxItContainer").css("display", "none");
            $("#childage3SelectBoxItContainer").css("display", "none");

            $(touriststotal).text(adultscount + " взр.");

            return false;
        }

        $(".kidsCount div").removeClass("active");
        $("#child").val(kidscount);

        switch (parseInt(kidscount)) {
            case 1:
                $(this).addClass("active");
                $(".kidsAge").css("display", "block");
                $("#childage1SelectBoxItContainer").css("display", "inline-block");
                $("#childage2SelectBoxItContainer").css("display", "none");
                $("#childage3SelectBoxItContainer").css("display", "none");
                break;
            case 2:
                $(this).addClass("active");
                $(".kidsAge").css("display", "block");
                $("#childage1SelectBoxItContainer").css("display", "inline-block");
                $("#childage2SelectBoxItContainer").css("display", "inline-block");
                $("#childage3SelectBoxItContainer").css("display", "none");
                break;
            case 3:
                $(this).addClass("active");
                $(".kidsAge").css("display", "block");
                $("#childage1SelectBoxItContainer").css("display", "inline-block");
                $("#childage2SelectBoxItContainer").css("display", "inline-block");
                $("#childage3SelectBoxItContainer").css("display", "inline-block");
                break;
            default:
                $(".kidsCount div").removeClass("active");
                $(".kidsAge").css("display", "none");
                $("#childage1SelectBoxItContainer").css("display", "none");
                $("#childage2SelectBoxItContainer").css("display", "none");
                $("#childage3SelectBoxItContainer").css("display", "none");
        }
    });

    var touristselect = $('.toursit-select .form-control');
    var save = $('.toursit-select .button-save');


    touristselect.click( function () {touristsbox.fadeIn(120);});
    save.click( function () {touristsbox.fadeOut(120);});


    //Закрывает выбор туристов, если кликнули вне контейнера
    $(document).mouseup(function (e) {
        var container = $(".tourists");
        if (container.has(e.target).length === 0){
            container.hide();
        }
    });

});

/********************************************** ЗАПОЛНЕНИЕ ФОРМЫ *******************************************************/

//Очищаем чекбоксы
function clearCheckboxes(element, listId) {
    if ($(element).prop("checked")) {
        var clearEl = $("#" + listId).find(".list-item");
        $(clearEl).prop("checked", false);
    }

    if (listId == "hotels") {
        checkedHotels = [];
    }

};


//Получаем список городов вылета и заполняем модальное окно
function getDepartures(currentCity) {
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "departure"}, function (json) {
        $('#departureList').empty();

        $('#departureList').append($('<li class="divider"></li>'));
        $('#departureList').append($('<li><a href="javascript:void(0)" onclick="selectDeparture(99, \'Без перелета\')">Без перелета</a></li>'));
        $('#departureList').append($('<li class="divider">А</li>'));

        var firstLetter = "А";
        $.each(json.lists.departures.departure, function (i, departure) {

            if(firstLetter !== departure.namefrom.substr(0,1)) {
                $('#departureList').append($('<li class="divider">'+ departure.namefrom.substr(0,1) +'</li>'));
            }
            firstLetter = departure.namefrom.substr(0,1);

            if(departure.id == 99) {
                return
            }

            if(departure.id == currentCity) {
                $("#departure").val(departure.id);
                $("#departureValue").text(departure.namefrom);
                $('#departureList').append($('<li class="active"><a href="javascript:void(0)" onclick="selectDeparture('+departure.id+', \''+departure.namefrom+'\')">'+departure.namefrom+'</a> </li>'));
            }else{
                $('#departureList').append($('<li><a href="javascript:void(0)" onclick="selectDeparture('+departure.id+', \''+departure.namefrom+'\')">'+departure.namefrom+'</a> </li>'));
            }
        });
    });

    /* ==========================================================================
     Разбивка списка на колонки
     ========================================================================== */
    setTimeout(function() {
        $('.multiColumn').autocolumnlist({ columns: 4 });
    }, 2000);

}


//Получаем список туроператоров
function getOperators(selectedOperators) {
    var usedOperators = [11, 12, 13,15,16,17,18,23,24,25,27,28,30,31,33,36,39,40,41,43,44,47,55,58,60,64,65,79]; //Используемые туроператоры
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "operator"}, function (json) {
        $('#operators').empty();
        $.each(json.lists.operators.operator, function (i, operator) {
            var checked = "";
            if ($.inArray(operator.id, selectedOperators) >= 0) checked = "checked";
            if($.inArray(parseInt(operator.id), usedOperators) >= 0) {
                $('#operators').append('<option value="' + operator.id + '">' + operator.name + '</option>');
                //$('#operators').append('<li><input type="checkbox" class="list-item" onclick="$(\'#alloperators\').prop(\'checked\', false)" name="operators[]" ' + checked + ' value="' + operator.id + '">' + operator.name + '</li>')
            }
        });
        $('.multiply').multiselect({
            enableFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 200,
            buttonWidth: '360px',
            buttonClass: 'selectboxit  selectboxit-enabled selectboxit-btn',
            allSelectedText: 'Выбраны все туроперторы',
            selectAllText: 'Выбрать все!',
            filterPlaceholder: 'Поиск...',
            buttonText: function(options, select) {
                if (options.length === 0) {
                    return 'Выберите туроператора...';
                }
                else if (options.length > 3) {
                    return 'Выбрано ' + options.length;
                }
                else {
                    var labels = [];
                    options.each(function() {
                        if ($(this).attr('label') !== undefined) {
                            labels.push($(this).attr('label'));
                        }
                        else {
                            labels.push($(this).html());
                        }
                    });
                    return labels.join(', ') + '';
                }
            }
        });
    });
}

//Получаем список типов питания
function getMeals(selectedMeal) {
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "meal"}, function (json) {
        $.each(json.lists.meals.meal, function (i, meal) {
            var selected = "";
            if(meal.id == selectedMeal) selected="selected"
            $('#meal').append($('<option '+selected+' value="' + meal.id + '">'+meal.name+' ('+meal.russian+')</option>'))
        });
    });
    //Обновляем селекты
    setTimeout(function() {
        var selectBox = $("#meal").data("selectBox-selectBoxIt");
        selectBox.refresh();
    }, 1200);
}


//Получаем спиоск дат вылета
function getFlydates(flydeparture, flycountry) {
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "flydate", flydeparture:flydeparture, flycountry:flycountry}, function (json) {
        /* daterangepicker
         ========================================================================== */
        //
        $('input[name="daterange"]').val();
        $('input[name="daterange"]').daterangepicker({
            autoApply: true,
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-blue',
            cancelClass: 'btn-black',
            separator: '-',
            availableDates: json.lists.flydates.flydate,
            locale: {
                format: 'DD.MM.YYYY',
                applyLabel: 'Выбрать',
                cancelLabel: 'Отмена',
                fromLabel: 'С',
                toLabel: 'По',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт','Сб'],
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                firstDay: 1
            }
        });

    });
}

/********************************************** ПОИСК ТУРА *******************************************************/

function searchTours() {
    //Устанавливаем начальные значения видимых объектов
    $(".progress-section").slideDown(200);
    $(".progress-bar").css("width", "0%");
    $("#progress-percent").text("0%");
    $("#tours-found").text("0 туров найдено");
    $("#progress-status").text("идет поиск");

    $("#formok").css('display', 'none'); //Скрываем статус о успешной отправки заявки на подбор

    $("#request").css('display', 'none');
    $('body').scrollTo('#request');
    makeRequest();
}

//Делаем запрос на получение requestId
function makeRequest() {

    var departure = $("#departure").val().length > 0 ? $("#departure").val():"";
    var country = $("#country").val().length > 0 ? $("#country").val():"";

    var daterange = $("#daterange").val().length>0 ? $("#daterange").val():"";
    var arDateRange = daterange.split("-");
    var datefrom = arDateRange[0].length>0 ? $.trim(arDateRange[0]):"";
    var dateto = arDateRange[1].length>0 ? $.trim(arDateRange[1]):"";

    var nightsfrom = $("#nightsfrom").val().length>0 ? $("#nightsfrom").val():"";
    var nightsto = $("#nightsto").val().length>0 ? $("#nightsto").val():"";
    var adults = $("#adults").val().length>0 ? $("#adults").val():"";
    var child = $("#child").val().length>0 ? $("#child").val():"";
    var childage1 = $("#childage1 option:selected").val().length>0 ? $("#childage1 option:selected").val():"";
    var childage2 = $("#childage2 option:selected").val().length>0 ? $("#childage2 option:selected").val():"";
    var childage3 = $("#childage3 option:selected").val().length>0 ? $("#childage3 option:selected").val():"";
    var stars = $("#stars").val().length>0 ? $("#stars").val():"";
    var meal = $("#meal option:selected").val().length>0 ? $("#meal option:selected").val():"";
    var rating = $("#rating").val().length>0 ? $("#rating").val():"";

    var pricerange = $("#priceRange").val().length>0 ? $("#priceRange").val():"";
    var arPriceRange = pricerange.split(",");
    var pricefrom = arPriceRange[0].length>0 ? arPriceRange[0]:"";
    var priceto = arPriceRange[1].length>0 ? arPriceRange[1]:"";

    var hotels =$("#hotels").val();

    //Заполняем туроператоров
    var checkedOperators = $("#operators").val();

    var operators = "";
    if(checkedOperators != null) {
        $.each(checkedOperators, function (i, operator) {
            operators += operator + ",";
        });
        operators = operators.substring(0, operators.length - 1);
    }

    var params = {
        type:"search",
        departure:departure,
        country:country,
        datefrom:datefrom,
        dateto:dateto,
        nightsfrom:nightsfrom,
        nightsto:nightsto,
        adults:adults,
        child:child,
        childage1:childage1,
        childage2:childage2,
        childage3:childage3,
        stars:stars,
        meal:meal,
        rating:rating,
        pricefrom:pricefrom,
        priceto:priceto,
        regions:"",
        hotels:hotels,
        hoteltypes:"",
        operators: operators
    };


    //Формируем строку для формы подбора тура, если туров не будет найдено
    helpformmessage = "Из города: " + $("#departureValue").text() + "\n";
    helpformmessage += "Куда: "+ $("#countryValue").text() + "\n";
    helpformmessage += "Кто едет: "+ adults + " взрослых, " + child + " детей" + "\n";
    helpformmessage += "Дата вылета: с "+ datefrom + " по " + dateto + "\n";
    helpformmessage += "Продолжительность: "+ nightsfrom + " - " + nightsto + " ночей \n";
    helpformmessage += "Класс отеля: "+ stars+ "*" + "\n";


    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', params, function (json) {
        setTimeout(function() {

            userurl = window.location.href; //Параметр со ссылкой по которой искал пользователь(подставляется в скрытое поле в фомре помощи подбора, елси ничего не найдено)


            checkTourStatus(json.result.requestid);
        }, 1000);

    });
}

//Проверяем статус поиска туров, елси finished, тогда выводим результаты
function checkTourStatus(requestid) { 
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type:"status", requestid:requestid}, function (json) {
        if(json.data.status.state != "finished") {
            $(".progress-bar").css("width", json.data.status.progress+"%");
            setTimeout(function() {
                checkTourStatus(requestid);
            }, 3000);
            $("#progress-percent").text(json.data.status.progress+"%");

            if(json.data.status.timepassed >= 7 && json.data.status.timepassed <= 9){
                getToursData(requestid, false);

                $("#tours-found").text(json.data.status.toursfound + " туров найдено");
            }

        } else {
            $(".progress-bar").css("width", json.data.status.progress+"%");
            $('#currentPage').html(1);
            $("#progress-percent").text(json.data.status.progress+"%");
            $("#tours-found").text(json.data.status.toursfound + " туров найдено");
            getToursData(requestid, true);
        }
    });
}

//Подробная информация по туру
function actualizeTour(tourid) {
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type:"actualize", tourid:tourid}, function (json) {
        console.log(json);
    });
}


//Получаем данные по турам

function getToursData(requestid, isfinish) {

    var currentPage = parseInt($('#currentPage').html());

    $.get('/bitrix/tools/tourvisor.travel_result.php', {requestid:requestid, page:currentPage, isfinish: isfinish, template:"hotel"}, function (data) {

        if(isfinish) {
            $("#progress-percent").text("100%");
            $("#progress-status").text("поиск завершен");
            $(".progress-section").slideUp(200);

            if(currentPage == 1) {
                $("#request").css('display', 'block').html(data);
            }else{
                $("#request").append(data);
            }
            currentPage = currentPage+1;
            $('#currentPage').html(currentPage);

            $('#requestid').html(requestid);

            return false;
        }else{
            $("#request").css('display', 'block').html(data);
        }

    })
}

function buyTour(requestid,tourid) {
    $("body").css("cursor", "wait");
    $.get('/bitrix/tools/tourvisor.travel_result.php', {requestid:requestid, tourid:tourid, template:"hotel"}, function (data) {
        $('#request').html(data);
        $("body").css("cursor", "default");
    })
}
