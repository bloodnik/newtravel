/********************************************** ОБЩИЕ ФУНКЦИИ *******************************************************/
$(window).load(function () {

    /* ==========================================================================
     Выбор ночей
     ========================================================================== */
    $("#nightsValue").on("click", function () {
        $("#nights-select").css("display", "table");
    });

    $("#nights-select table tr>td").on("click", function () {
        if (nto == 0 && nfrom > 0) { //Если уже нажали на ночей От, выбираем ночей До
            var nightsTo = $(this).attr("data-value"); //Выбранное значение
            nto = parseInt(nightsTo); //Количество До
            if (nfrom <= nto) { //Если  ночей от меньше чем ночей до
                $(this).addClass("to-active");

                $("#nightsto").val(nightsTo); //Устанавливаем значение для элемента формы
                $("#nightsValue").text("от " + nfrom + " до " + nto + " ночей");

                //Обнуляем все
                $("#nights-select>span").text("от:");
                $("#nights-select table tr>td").removeClass("to-active");
                $("#nights-select table tr>td").removeClass("from-active");
                $("#nights-select table tr>td").removeClass("in-range");
                nfrom = 0;
                nto = 0;
                $("#nights-select").hide();
                $("#nights-error").css("display", "none");
            } else {
                $("#nights-error").slideDown(200);
                nto = 0;
            }
        }

        if (nfrom == 0 && nightsTo == undefined) { //Если еще не выбрали ночей От
            var nightsFrom = $(this).attr("data-value"); //Выбранное значение
            $("#nightsfrom").val(nightsFrom); //Устанавливаем значение для элемента формы
            nfrom = parseInt(nightsFrom); //Количество От
            $("#nights-select>strong").text("до:");
            $(this).addClass("from-active");
        }
    });

    $("#nights-select table tr>td").on("mouseover", function () {
        if (nfrom > 0) {
            var val = $(this).attr("data-value");
            $("#nights-select table tr>td").each(function (key, item) {
                var dataValue = $(this).attr("data-value");
                if (parseInt(dataValue) < parseInt(val) && parseInt(dataValue) > nfrom) {
                    $(this).addClass("in-range");
                } else {
                    $(this).removeClass("in-range");
                }
            })

        }
    });

    //Закрывает выбор ночей, если кликнули вне контейнера
    $(document).mouseup(function (e) {
        var container = $("#nights-select");
        if (container.has(e.target).length === 0) {
            container.hide();
        }
    });

    /*=========================================================================
     Stars rating
     ========================================================================== */
    //$("#stars").rating("update", 3);

    /*=========================================================================
     mCustomScrollBar
     ========================================================================== */
    $(".customScroll").mCustomScrollbar();

    /*=========================================================================
     Kid's age
     ========================================================================== */
    // Show guestsblock onClick
    var touristsbox = $('.tourists');
    var touriststotal = $('.tourists-total>span');
    var kidscount = $("#child").val();
    var adultscount = $("#adults").val();

    $(".adultsCount div").on("click", function () {
        $(".adultsCount div").removeClass("active");
        adultscount = $(this).attr("data-count");
        $(this).addClass("active");
        $("#adults").val(adultscount);
        if (kidscount > 0) {
            $(touriststotal).text(adultscount + " взр. " + kidscount + "реб.");
        } else {
            $(touriststotal).text(adultscount + " взр.");
        }
    });

    $("#noKids").on("click", function () {
        $(this).css("color", "#ccc");

        kidscount = 0;
        $("#child").val(kidscount);
        if (kidscount > 0) {
            $(touriststotal).text(adultscount + " взр. " + kidscount + "реб.");
        } else {
            $(touriststotal).text(adultscount + " взр.");
        }
        $(".kidsCount div").removeClass("active");
        $(".kidsAge").css("display", "none");
        $("#age1SelectBoxItContainer").css("display", "none");
        $("#age2SelectBoxItContainer").css("display", "none");
        $("#age3SelectBoxItContainer").css("display", "none");
    });

    $(".kidsCount>div").on("click", function (clickEvent) {

        $("#noKids").css({"color": "#E00300", "text-decoration": "underline"});

        kidscount = $(this).attr("data-count");

        if (kidscount > 0) {
            $(touriststotal).text(adultscount + " взр. " + kidscount + "реб.");
        } else {
            $(touriststotal).text(adultscount + " взр.");
        }

        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $("#child").val(0);
            $(".kidsAge").css("display", "none");
            $("#childage1SelectBoxItContainer").css("display", "none");
            $("#childage2SelectBoxItContainer").css("display", "none");
            $("#childage3SelectBoxItContainer").css("display", "none");

            $(touriststotal).text(adultscount + " взр.");

            return false;
        }

        $(".kidsCount div").removeClass("active");
        $("#child").val(kidscount);

        switch (parseInt(kidscount)) {
            case 1:
                $(this).addClass("active");
                $(".kidsAge").css("display", "block");
                $("#childage1SelectBoxItContainer").css("display", "inline-block");
                $("#childage2SelectBoxItContainer").css("display", "none");
                $("#childage3SelectBoxItContainer").css("display", "none");
                break;
            case 2:
                $(this).addClass("active");
                $(".kidsAge").css("display", "block");
                $("#childage1SelectBoxItContainer").css("display", "inline-block");
                $("#childage2SelectBoxItContainer").css("display", "inline-block");
                $("#childage3SelectBoxItContainer").css("display", "none");
                break;
            case 3:
                $(this).addClass("active");
                $(".kidsAge").css("display", "block");
                $("#childage1SelectBoxItContainer").css("display", "inline-block");
                $("#childage2SelectBoxItContainer").css("display", "inline-block");
                $("#childage3SelectBoxItContainer").css("display", "inline-block");
                break;
            default:
                $(".kidsCount div").removeClass("active");
                $(".kidsAge").css("display", "none");
                $("#childage1SelectBoxItContainer").css("display", "none");
                $("#childage2SelectBoxItContainer").css("display", "none");
                $("#childage3SelectBoxItContainer").css("display", "none");
        }
    });

    var touristselect = $('.toursit-select .form-control');
    var save = $('.toursit-select .button-save');


    touristselect.click(function () {
        touristsbox.fadeIn(120);
    });
    save.click(function () {
        touristsbox.fadeOut(120);
    });

    //Закрывает выбор туристов, если кликнули вне контейнера
    var hide = true;//Флаг закрытия окна - нужен для определения выбора из селекта вне окна
    $(document).mouseup(function (e) {
        var container = $(".tourists");
        if (container.has(e.target).length === 0) {
            if (hide) {
                container.hide();
            }
        }
    });

    //События селектов, устанавливают флаг hide в false, после выбора через 1 секудну возвращает в true
    $("select#childage1,select#childage2,select#childage3").bind({
        close: function () {
            hide = false;
            setTimeout(setHide, 1000);
        }
    });
    function setHide() {
        hide = true;
    }


    /*-----------------------------------------------------------------------------------------
     События для действий
     -----------------------------------------------------------------------------------------*/
    //Событие при смене курортов
    $("#regions").on("change", function (event) {

        //Если отмечаем родителя, тогда отмечаются все дочерние чекбоксы и наоборот
        var elem = event.target.parentElement;
        var checkbox = $(elem).find('input:first');
        if (checkbox.prop('checked')) {
            $.each($(elem).find('input'), function (i, el) {
                $(el).prop('checked', true);
            });
        } else {
            $.each($(elem).find('input'), function (i, el) {
                $(el).prop('checked', false);
            });
        }

        if (!checkbox.hasClass('subregion_item')) {
            crateHotelParamString();
        }

    });

    //Событие при смене типов отелей
    $("#hoteltypes input").on("change", function (event) {
        crateHotelParamString();
    });

    //Событие при смене звезд
    $("#stars").on("rating.change", function (event, value, caption) {
        crateHotelParamString();
    });

    //Событие при смене рейтинга
    $("#rating").on("change", function (event) {
        crateHotelParamString();
    });

    //Подгрузка отелей при скролле
    /* Используйте вариант $('#more').click(function() для того, чтобы дать пользователю возможность управлять процессом, кликая по кнопке "Дальше" под блоком статей (см. файл index.php) */
    $(window).scroll(function () {
        var currentPage = $("#currentPage").html();
        var requestid = $("#requestid").text();
        /* Если высота окна + высота прокрутки больше или равны высоте всего документа и ajax-запрос в настоящий момент не выполняется, то запускаем ajax-запрос */
        if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
            getToursData(requestid, currentPage);
        }
    });

});

//Очищаем чекбоксы
function clearCheckboxes(element, listId) {
    if ($(element).prop("checked")) {
        var clearEl = $("#" + listId).find(".list-item");
        $(clearEl).prop("checked", false);
    }

    if (listId == "hotels") {
        checkedHotels = [];
    }

};

/********************************************** ФИЛЬТРАЦИЯ ОТЕЛЕЙ*******************************************************/
//фильтрация по субрегионам
function subRegionFilter() {
    var subRegIds = new Array();
    $.each($('.subregion_item:checked'), function (key, elem) {
        subRegIds[key] = $(elem).val();
    });

    $("#hotels").empty();
    var hotelListClone = hotelList;
    $.each(hotelListClone, function (index, hotel) {
        var checked = "";
        if ($.inArray(hotel[3], subRegIds) >= 0) {
            $('#hotels').append($('<li><input type="checkbox" onclick="hotelSelect(this)" class="list-item" name="hotels[]" ' + checked + ' value="' + hotel[0] + '">' + hotel[1] + '</li>'));
        }
    });
    if (subRegIds.length == 0) {
        $.each(hotelListClone, function (index, hotel) {
            var checked = "";
            $('#hotels').append($('<li><input type="checkbox" onclick="hotelSelect(this)" class="list-item" name="hotels[]" ' + checked + ' value="' + hotel[0] + '">' + hotel[1] + '</li>'));
        });
    }
}

//фильтрация отелей
function filterHotel(self) {

    var filterString = $("#hotelsName").val();
    if (filterString.length >= 3) {
        $("#showSelectedHotels").prop("checked", false);
        $("#hotels").empty();
        var hotelListClone = hotelList;
        var filteredList = hotelListClone.filter(filter);
        $.each(filteredList, function (index, hotel) {
            var checked = "";
            if ($.inArray(hotel[0], checkedHotels) >= 0) checked = "checked";
            $('#hotels').append($('<li><input type="checkbox" onclick="hotelSelect(this)" class="list-item" name="hotels[]" ' + checked + ' value="' + hotel[0] + '">' + hotel[1] + '</li>'));
        });
    } else {
        $("#hotels").empty();
        $.each(hotelList, function (index, hotel) {
            var checked = "";
            if ($.inArray(hotel[0], checkedHotels) >= 0) checked = "checked";
            $('#hotels').append($('<li><input type="checkbox" onclick="hotelSelect(this)" class="list-item" name="hotels[]" ' + checked + ' value="' + hotel[0] + '">' + hotel[1] + '</li>'));

        })
    }
}
function filter(item) {
    var filterString = $("#hotelsName").val();
    var hotelName = item[1];
    hotelName = hotelName.toLowerCase();
    filterString = filterString.toLowerCase();
    pos = hotelName.indexOf(filterString);
    if (-1 < pos) {
        return true;
    } else {
        return false;
    }
}

/********************************************** ЗАПОЛНЕНИЕ ФОРМЫ *******************************************************/

//Получаем список городов вылета и заполняем модальное окно
function getDepartures(currentCity) {
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "departure"}, function (json) {
        $('#departureList').empty();

        $('#departureList').append($('<li class="divider"></li>'));
        $('#departureList').append($('<li><a href="javascript:void(0)" onclick="selectDeparture(99, \'Без перелета\')">Без перелета</a></li>'));
        $('#departureList').append($('<li class="divider">А</li>'));

        var firstLetter = "А";
        $.each(json.lists.departures.departure, function (i, departure) {

            if (firstLetter !== departure.namefrom.substr(0, 1)) {
                $('#departureList').append($('<li class="divider">' + departure.namefrom.substr(0, 1) + '</li>'));
            }
            firstLetter = departure.namefrom.substr(0, 1);

            if (departure.id == 99) {
                return
            }

            if (departure.id == currentCity) {
                $("#departure").val(departure.id);
                $("#departureValue").text(departure.namefrom);
                $('#departureList').append($('<li class="active"><a href="javascript:void(0)" onclick="selectDeparture(' + departure.id + ', \'' + departure.namefrom + '\')">' + departure.namefrom + '</a> </li>'));
            } else {
                $('#departureList').append($('<li><a href="javascript:void(0)" onclick="selectDeparture(' + departure.id + ', \'' + departure.namefrom + '\')">' + departure.namefrom + '</a> </li>'));
            }
        });
    });

    /* ==========================================================================
     Разбивка списка на колонки
     ========================================================================== */
    setTimeout(function () {
        $('.multiColumn').autocolumnlist({columns: 4});
    }, 2000);

}

//Получаем список стран. Если выбран город вылета то задействован параметр cndep
function getCountries(cndep, currentCountry) {
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "country", cndep: cndep}, function (json) {
        $('#countryList').empty();

        $.each(json.lists.countries.country, function (i, country) {
            if (country.id == currentCountry) {
                $("#country").val(country.id);
                $("#countryValue").text(country.name);
                $('#countryList').append($('<li class="active"><a href="javascript:void(0)" onclick="selectDeparture(' + country.id + ', \'' + country.name + '\')">' + country.name + '</a> </li>'));
            } else {
                $('#countryList').append($('<li><a href="javascript:void(0)" onclick="selectCountry(' + country.id + ', \'' + country.name + '\')">' + country.name + '</a> </li>'));
            }
        });
    });
    /* ==========================================================================
     Разбивка списка на колонки
     ========================================================================== */
    setTimeout(function () {
        $('.multiColumn').autocolumnlist({columns: 4});
    }, 1000);
}

//Получаем список курортов. Если выбрана страна то задействован параметр regcountry
function getRegions(regcountry, selectedRegions) {
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "region", regcountry: regcountry}, function (json) {
        $('#regions').empty();
        $.each(json.lists.regions.region, function (i, region) {
            var checked = "";
            if ($.inArray(region.id, selectedRegions) >= 0) checked = "checked";
            $('#regions').append('<li id="region_' + region.id + '"><input type="checkbox" class="list-item" onclick="$(\'#allregions\').prop(\'checked\', false)" name="regions[]" ' + checked + ' value="' + region.id + '">' + region.name + '</li>')
        });
    });
}
//Получаем список субкурортов. Если выбрана страна то задействован параметр regcountry, если выбран курорт, тогда задействуем параметр parentregion
function getSubRegions(regcountry, selectedRegions) {
    var openedIcon = '<i class="fa fa-chevron-down"></i>';
    var closedIcon = '<i class="fa fa-chevron-right"></i>';

    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "subregion", regcountry: regcountry}, function (json) {
        if(json.lists.subregions.subregion !== null) {
            $.each(json.lists.subregions.subregion, function (i, subregion) {
                var checked = "";
                if ($.inArray(subregion.id, selectedRegions) >= 0) checked = "checked";
                $('#region_' + subregion.parentregion).append('<span><input type="checkbox" class="list-item subregion_item" onclick="$(\'#allregions\').prop(\'checked\', false)" name="subregions[]" ' + checked + ' value="' + subregion.id + '" data-parent="' + subregion.parentregion + '">' + subregion.name + '</span>')
            });
            //События при смене субкурортов. Выставляем статус indeterminate для региона, если выбан хотябы один субкурорт
            $('.subregion_item').on('change', function () {
                var parentRegion = 'region_' + $(this).data('parent');
                var countCheked = $('#' + parentRegion + '>span>input:checked').length;
                subRegionFilter(); //фильтруем
                if (countCheked > 0) {
                    $('#' + parentRegion + '>input').prop("indeterminate", true);
                } else {
                    $('#' + parentRegion + '>input').prop("indeterminate", false);
                }
            });
        }
    });
}

//Получаем список туроператоров
function getOperators(selectedOperators) {
    var usedOperators = [11, 12, 13,15,16,17,18,23,24,25,27,28,30,31,33,36,39,40,41,43,44,47,55,58,60,64,65,79]; //Используемые туроператоры
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "operator"}, function (json) {
        $('#operators').empty();
        $.each(json.lists.operators.operator, function (i, operator) {
            var checked = "";
            if ($.inArray(operator.id, selectedOperators) >= 0) checked = "checked";
            if($.inArray(parseInt(operator.id), usedOperators) >= 0) {
                $('#operators').append('<li><input type="checkbox" class="list-item" onclick="$(\'#alloperators\').prop(\'checked\', false)" name="operators[]" ' + checked + ' value="' + operator.id + '">' + operator.name + '</li>')
            }
        });
    });
}

//Получаем список типов питания
function getMeals(selectedMeal) {
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "meal"}, function (json) {
        $.each(json.lists.meals.meal, function (i, meal) {
            var selected = "";
            if (meal.id == selectedMeal) selected = "selected";
            $('#meal').append($('<option ' + selected + ' value="' + meal.id + '">' + meal.name + ' (' + meal.russian + ')</option>'))
        });
    });
    //Обновляем селекты
    setTimeout(function () {
        var selectBox = $("#meal").data("selectBox-selectBoxIt");
        selectBox.refresh();
    }, 2000);
}

//Получаем список категорий отеля   НЕ ИЗПОЛЬЗУЕТСЯ!!!
function getStars() {
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "stars"}, function (json) {
        $('#starsList').empty();
        $.each(json.lists.stars.star, function (i, star) {
            $('#starsList').append('<div><input type="checkbox" name="starsList[]" value="' + star.id + '"><span>' + star.name + '</span></div>')
        });
    });
}

//Получаем спиоск дат вылета
function getFlydates(flydeparture, flycountry) {
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "flydate", flydeparture: flydeparture, flycountry: flycountry}, function (json) {
        /* daterangepicker
         ========================================================================== */
        //
        $('input[name="daterange"]').val();
        $('input[name="daterange"]').daterangepicker({
            autoApply: true,
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-blue',
            cancelClass: 'btn-black',
            separator: '-',
            availableDates: json.lists.flydates.flydate,
            locale: {
                format: 'DD.MM.YYYY',
                applyLabel: 'Выбрать',
                cancelLabel: 'Отмена',
                fromLabel: 'С',
                toLabel: 'По',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                firstDay: 1
            }
        });

    });
}

//Формируем параметры для фильтра отелей и получаем список отлелей
function crateHotelParamString(selectedHotels) {
    var hotcountry = $("#country").val();
    var hotregion = "";
    var hotstars = "1";
    var hotrating = "0";
    var hotactive = "0";
    var hotrelax = "0";
    var hotfamily = "0";
    var hothealth = "0";
    var hotcity = "0";
    var hotbeach = "0";
    var hotdeluxe = "0";

    var checkedRegions = $("#regions>li>input:checked");
    $.each(checkedRegions, function (i, region) {
        hotregion += region.value + ",";
    });

    hotregion = hotregion.substring(0, hotregion.length - 1);

    hotstars = $("#stars").val();

    var checkedRate = $("#rating option:selected");
    $.each(checkedRate, function (i, rate) {
        hotrating = rate.value;
    });

    var checkedHotactive = $("#hotactive:checked");
    $.each(checkedHotactive, function (i, el) {
        hotactive = el.value;
    });
    var checkedHotrelax = $("#hotrelax:checked");
    $.each(checkedHotrelax, function (i, el) {
        hotrelax = el.value;
    });
    var checkedHotfamily = $("#hotfamily:checked");
    $.each(checkedHotfamily, function (i, el) {
        hotfamily = el.value;
    });
    var checkedHothealth = $("#hothealth:checked");
    $.each(checkedHothealth, function (i, el) {
        hothealth = el.value;
    });
    var checkedHotcity = $("#hotcity:checked");
    $.each(checkedHotcity, function (i, el) {
        hotcity = el.value;
    });
    var checkedHotbeach = $("#hotbeach:checked");
    $.each(checkedHotbeach, function (i, el) {
        hotbeach = el.value;
    });
    var checkedHotdelux = $("#hotdeluxe:checked");
    $.each(checkedHotdelux, function (i, el) {
        hotdeluxe = el.value;
    });
    getHotels(hotcountry, hotregion, hotstars, hotrating, hotactive, hotrelax, hotfamily, hothealth, hotcity, hotbeach, hotdeluxe, selectedHotels);
}

//Получаем список отелей
function getHotels(hotcountry, hotregion, hotstars, hotrating, hotactive, hotrelax, hotfamily, hothealth, hotcity, hotbeach, hotdeluxe, selectedHotels) {

    hotelList.length = 0;
    hotelList = new Array();

    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {
        type: "hotel",
        hotcountry: hotcountry,
        hotregion: hotregion,
        hotstars: hotstars,
        hotrating: hotrating,
        hotactive: hotactive,
        hotrelax: hotrelax,
        hotfamily: hotfamily,
        hothealth: hothealth,
        hotcity: hotcity,
        hotbeach: hotbeach,
        hotdeluxe: hotdeluxe
    }, function (json) {

        $('#hotels').empty();
        $.each(json.lists.hotels.hotel, function (i, hotel) {
            //заполняем массив отелей для фильтрации
            var hotelInfo = new Array();
            hotelInfo[0] = hotel.id;
            hotelInfo[1] = hotel.name;
            hotelInfo[3] = hotel.subregion;
            hotelList[i] = hotelInfo;

            var checked = "";
            if ($.inArray(hotel.id, selectedHotels) >= 0) checked = "checked";
            if ($.inArray(hotel.id, checkedHotels) >= 0) checked = "checked";
            $('#hotels').append('<li><input type="checkbox" class="list-item" onclick="hotelSelect(this)" name="hotels[]" ' + checked + ' value="' + hotel.id + '">' + hotel.name + ' (' + hotel.stars + '*)</li>')
        });
        $("#hellopreloader_preload").css({"display": "none", "opacity": "0"});

    });
}

/********************************************** ПОИСК ТУРА *******************************************************/

function searchTours() {
    //Устанавливаем начальные значения видимых объектов
    $(".progress-section").slideDown(200);
    $(".progress-bar").css("width", "0%");
    $("#progress-percent").text("0%");
    $("#tours-found").text("0 туров найдено");
    $("#progress-status").text("идет поиск");

    $("#formok").css('display', 'none'); //Скрываем статус о успешной отправки заявки на подбор

    $("#request").css('display', 'none');
    $('body').scrollTo('#request');
    makeRequest();
}

//Делаем запрос на получение requestId
function makeRequest() {

    var departure = $("#departure").val().length > 0 ? $("#departure").val() : "";
    var country = $("#country").val().length > 0 ? $("#country").val() : "";

    var daterange = $("#daterange").val().length > 0 ? $("#daterange").val() : "";
    var arDateRange = daterange.split("-");
    var datefrom = arDateRange[0].length > 0 ? $.trim(arDateRange[0]) : "";
    var dateto = arDateRange[1].length > 0 ? $.trim(arDateRange[1]) : "";

    var nightsfrom = $("#nightsfrom").val().length > 0 ? $("#nightsfrom").val() : "";
    var nightsto = $("#nightsto").val().length > 0 ? $("#nightsto").val() : "";
    var adults = $("#adults").val().length > 0 ? $("#adults").val() : "";
    var child = $("#child").val().length > 0 ? $("#child").val() : "";
    var childage1 = $("#childage1 option:selected").val().length > 0 ? $("#childage1 option:selected").val() : "";
    var childage2 = $("#childage2 option:selected").val().length > 0 ? $("#childage2 option:selected").val() : "";
    var childage3 = $("#childage3 option:selected").val().length > 0 ? $("#childage3 option:selected").val() : "";
    var stars = $("#stars").val().length > 0 ? $("#stars").val() : "";
    var meal = $("#meal option:selected").val().length > 0 ? $("#meal option:selected").val() : "";
    var rating = $("#rating option:selected").val().length > 0 ? $("#rating option:selected").val() : "";

    var pricerange = $("#priceRange").val().length > 0 ? $("#priceRange").val() : "";
    var arPriceRange = pricerange.split(",");
    var pricefrom = arPriceRange[0].length > 0 ? arPriceRange[0] : "";
    var priceto = arPriceRange[1].length > 0 ? arPriceRange[1] : "";

    //Заполняем курорты
    var checkedRegions = $("#regions input:checked");
    var regions = "";
    $.each(checkedRegions, function (i, region) {
        regions += region.value + ",";
    });
    regions = regions.substring(0, regions.length - 1);

    //Заполняем субкурорты
    var checkedSubRegions = $("#regions li>span>input:checked");
    var subregions = "";
    $.each(checkedSubRegions, function (i, subregion) {
        subregions += subregion.value + ",";
    });
    subregions = subregions.substring(0, subregions.length - 1);

    //Заполняем туроператоров
    var checkedOperators = $("#operators input:checked");
    var operators = "";
    $.each(checkedOperators, function (i, operator) {
        operators += operator.value + ",";
    });
    operators = operators.substring(0, operators.length - 1);

    var hotels = "";
    $.each(checkedHotels, function (i, hotel) {
        hotels += hotel + ",";
    });
    hotels = hotels.substring(0, hotels.length - 1);

    var checkedHotelsTypes = $("#hoteltypes input:checked");
    var hoteltypes = "";
    $.each(checkedHotelsTypes, function (i, type) {
        hoteltypes += $(type).attr("data-value") + ",";
    });
    hoteltypes = hoteltypes.substring(0, hoteltypes.length - 1);

    var params = {
        type: "search",
        departure: departure,
        country: country,
        datefrom: datefrom,
        dateto: dateto,
        nightsfrom: nightsfrom,
        nightsto: nightsto,
        adults: adults,
        child: child,
        childage1: childage1,
        childage2: childage2,
        childage3: childage3,
        stars: stars,
        meal: meal,
        rating: rating,
        pricefrom: pricefrom,
        priceto: priceto,
        regions: regions,
        subregions: subregions,
        hotels: hotels,
        hoteltypes: hoteltypes,
        operators: operators
    };

    //Формируем строку для формы подбора тура, если туров не будет найдено
    helpformmessage = "Из города: " + $("#departureValue").text() + "\n";
    helpformmessage += "Куда: " + $("#countryValue").text() + "\n";
    helpformmessage += "Кто едет: " + adults + " взрослых, " + child + " детей" + "\n";
    helpformmessage += "Дата вылета: с " + datefrom + " по " + dateto + "\n";
    helpformmessage += "Продолжительность: " + nightsfrom + " - " + nightsto + " ночей \n";
    helpformmessage += "Класс отеля: " + stars + "*" + "\n";

    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', params, function (json) {

        //console.log(json);

        //Меняем url браузера
        urlParams = json.url;
        history.pushState(json.result.requestid, "Поиск туров", "?start=Y" + urlParams);

        userurl = window.location.href; //Параметр со ссылкой по которой искал пользователь(подставляется в скрытое поле в фомре помощи подбора, елси ничего не найдено)

        setTimeout(function () {
            checkTourStatus(json.result.requestid);
        }, 1000);

    });
}

//Проверяем статус поиска туров, елси finished, тогда выводим результаты
function checkTourStatus(requestid) {
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "status", requestid: requestid}, function (json) {
        if (json.data.status.state != "finished") {
            $(".progress-bar").css("width", json.data.status.progress + "%");
            setTimeout(function () {
                checkTourStatus(requestid);
            }, 3000);
            $("#progress-percent").text(json.data.status.progress + "%");

            if (json.data.status.timepassed >= 7 && json.data.status.timepassed <= 9) {
                getToursData(requestid, false);

                $("#tours-found").text(json.data.status.toursfound + " туров найдено");
            }

        } else {
            $(".progress-bar").css("width", json.data.status.progress + "%");
            $('#currentPage').html(1);
            $("#progress-percent").text(json.data.status.progress + "%");
            $("#tours-found").text(json.data.status.toursfound + " туров найдено");
            /*=========================================================================
             countdown
             ========================================================================== */
            var newYear = new Date();
            newYear = new Date(newYear.getFullYear() + 1, 1 - 1, 1);
            $('.defaultCountdown').countdown({
                since: '',
                compact: true
                //layout: "<span>{hn}</span>:<span>{mn}</span>:<span>{sn}</span>"
            });

            getToursData(requestid, true);
        }
    });
}

//Подробная информация по туру
function actualizeTour(tourid) {
    $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "actualize", tourid: tourid}, function (json) {
    });
}


//Получаем данные по турам
function getToursData(requestid, isfinish) {
    var currentPage = parseInt($('#currentPage').html());

    $.get('/bitrix/tools/tourvisor.travel_result.php', {requestid: requestid, page: currentPage, isfinish: isfinish}, function (data) {

        if (isfinish) {
            $("#progress-percent").text("100%");
            $("#progress-status").text("поиск завершен");
            $(".progress-section").slideUp(200);

            if (currentPage == 1) {
                $("#request").css('display', 'block').html(data);
                checkOpenedTours();
                setTimeout(function () {
                    createHotelLink();
                }, 1000);
            } else {
                $("#request").append(data);
                setTimeout(function () {
                    createHotelLink();
                }, 1000);
            }
            currentPage = currentPage + 1;
            $('#currentPage').html(currentPage);

            $('#requestid').html(requestid);

            return false;
        } else {
            $("#request").css('display', 'block').html(data);
            setTimeout(function () {
                createHotelLink();
            }, 1000);
        }
    })
}

function buyTour(requestid, tourid) {
    $("body").css("cursor", "wait");
    $.get('/bitrix/tools/tourvisor.travel_result.php', {requestid: requestid, tourid: tourid}, function (data) {
        $('#request').html(data);
        $("body").css("cursor", "default");
    })
}

function createHotelLink() {
    $(".hotel-link").each(function () {
        var href = $(this).attr("href");
        var newHref = href + urlParams;
        $(this).attr("href", newHref);
    });
}