<article>
	<div class="news-item">
		<div class="post-wrapper standard-post">
			<div class="post-thumbnail">
				<a href="/new_year_ufa.php?referer1=FromBlogNYUfa" target="_blank" title="Туры на Новый год из Уфы">
					<img src="/upload/medialibrary/843/1.jpg" alt="Туры на Новый год из Уфы"/>
				</a>
			</div>
			<h3 class="post-title">
				<a href="/new_year_ufa.php?referer1=FromBlogNYUfa" target="_blank" title="Туры на Новый год из Уфы">Туры на Новый год из Уфы</a>
			</h3>
			<div class="post-containt">
				<p>Какие планы на новогодние праздники? Отдыхаем на этот раз с 31 декабря по 8 января. Умные Туристы предлагают Вам отдохнуть по-настоящему! Отдохнуть от морозов, почти круглосуточной
					темноты и салата оливье. Море ждёт Вас в ОАЭ, Индии (Гоа), Вьетнаме, Тайланде и Турции.</p>
			</div>
			<div class="post-meta">
				<span class="post-author">
						<a href="/blog/?&arrFilter_pf[POST_AUTHOR]=6&set_filter=Фильтр&set_filter=Y"
						   title="Автор">Лилия Юнусова</a>
				</span>
				<span class="post-date">11.10.2016</span>
			</div>
		</div>
	</div>
</article>