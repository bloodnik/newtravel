<!-- Modal -->
<? if ($USER->IsAuthorized()): ?>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Ваш отзыв</h4>
				</div>
				<div class="modal-body">
					<? $APPLICATION->IncludeComponent(
						"bitrix:iblock.element.add.form",
						"newtravel_add_review",
						array(
							"IBLOCK_TYPE"                   => "content",
							"IBLOCK_ID"                     => "71",
							"STATUS_NEW"                    => "NEW",
							"LIST_URL"                      => "",
							"USE_CAPTCHA"                   => "Y",
							"USER_MESSAGE_EDIT"             => "",
							'AJAX_MODE'                     => "Y",
							"USER_MESSAGE_ADD"              => "Спасибо! Ваш отзыв успешно добавлен и в скором времени будет выведен на сайте!",
							"DEFAULT_INPUT_SIZE"            => "60",
							"RESIZE_IMAGES"                 => "Y",
							"PROPERTY_CODES"                => array(
								0 => "NAME",
								1 => "PREVIEW_TEXT",
							),
							"PROPERTY_CODES_REQUIRED"       => array(
								0 => "NAME",
								1 => "PREVIEW_TEXT",
							),
							"GROUPS"                        => array(
								0 => "3",
							),
							"STATUS"                        => "INACTIVE",
							"ELEMENT_ASSOC"                 => "CREATED_BY",
							"MAX_USER_ENTRIES"              => "100000",
							"MAX_LEVELS"                    => "100000",
							"LEVEL_LAST"                    => "Y",
							"MAX_FILE_SIZE"                 => "0",
							"PREVIEW_TEXT_USE_HTML_EDITOR"  => "N",
							"DETAIL_TEXT_USE_HTML_EDITOR"   => "N",
							"SEF_MODE"                      => "N",
							"SEF_FOLDER"                    => "/reviews/",
							"CUSTOM_TITLE_NAME"             => "ФИO",
							"CUSTOM_TITLE_TAGS"             => "",
							"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
							"CUSTOM_TITLE_DATE_ACTIVE_TO"   => "",
							"CUSTOM_TITLE_IBLOCK_SECTION"   => "",
							"CUSTOM_TITLE_PREVIEW_TEXT"     => "Отзыв",
							"CUSTOM_TITLE_PREVIEW_PICTURE"  => "",
							"CUSTOM_TITLE_DETAIL_TEXT"      => "",
							"CUSTOM_TITLE_DETAIL_PICTURE"   => "Фото"
						),
						false
					); ?>
				</div>
			</div>
		</div>
	</div>
<? endif ?>