<!--<style>-->
<!--	#feedbackTitle {-->
<!--		font-family: "Lato", sans-serif;-->
<!--		font-weight: 300;-->
<!--		line-height: 120%;-->
<!--		color: #0099da;-->
<!--		margin: 20px 0 0px 0;-->
<!--		text-transform: uppercase;-->
<!--	}-->
<!---->
<!--	#accordion .panel-heading {-->
<!--		cursor: pointer;-->
<!--	}-->
<!--</style>-->
<!---->
<!--<h3>Есть сомнения? Остались вопросы? Спроси эксперта!</h3>-->
<!--<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">-->
<!--	<div class="panel panel-default">-->
<!--		<div class="panel-heading" role="tab" id="headingOne" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"-->
<!--		     aria-controls="collapseOne">-->
<!--			<h3 class="panel-title text-uppercase">Лиля +7(965)943-99-42</h3>-->
<!--		</div>-->
<!--		<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">-->
<!--			<div class="panel-body">-->
<!--				<div class="row">-->
<!--					<div class="col-md-3"><img src="/local/templates/lending/assets/img/newtravel/manager/m1.jpg" alt="" class="img-responsive"></div>-->
<!--					<div class="col-md-9">-->
<!--						<p><strong>Я действительно люблю путешествовать! </strong>За 10 лет работы в туризме я побывала во многих странах мира. С появлением детей я стала более тщательно-->
<!--							подходить-->
<!--							к-->
<!--							отдыху за границей. Выбираю только проверенные страны и отели. Как профессионал в сфере туризма, и как мама, для отдыха с детьми я рекомендую Доминикану.-->
<!--							Здесь все тоже самое, что в Турции, но в разы лучше! Питание все включено, шикарные песчаные пляжи, анимация и аквапарки и нереальное Карибское море!-->
<!--						</p>-->
<!--					</div>-->
<!--				</div>-->
<!--				<br>-->
<!--				<div class="row">-->
<!--					<div class="col-md-4">-->
<!--						<h3>Задай вопрос эксперту</h3>-->
<!--						<p>Например:</p>-->
<!--						<ul class="small list-unstyled" style="padding-left: 10px">-->
<!--							<li>Какой курорт/пляж лучше --><?//=$config["NAME_WHERE"]?><!--?</li>-->
<!--							<li>Сколько взять с собой денег --><?//=$config["NAME_TO"]?><!--?</li>-->
<!--							<li>Сколько лететь --><?//=$config["NAME_TO"]?><!--?</li>-->
<!--							<li>Какие экскурсии --><?//=$config["NAME_WHERE"]?><!-- выбрать?</li>-->
<!--						</ul>-->
<!--						<br>-->
<!--						<a data-title="Задай вопрос эксперту!" class="btn btn-blue b24-web-form-popup-btn-14">Задать свой вопрос!</a>-->
<!--					</div>-->
<!--					<div class="col-md-4">-->
<!--						<h3>Записаться на консультацию в офисе</h3>-->
<!--						<p>Мы работаем с 10 до 19 по адресу г. Уфа, ул. Гафури 54 (<a href="/about/contacts.php">карта проезда</a>)</p>-->
<!--						<a data-title="Запишись на консультацию в офисе!" class="btn btn-blue b24-web-form-popup-btn-14">Записаться на консультацию!</a>-->
<!--					</div>-->
<!--					<div class="col-md-4">-->
<!--						<h3>Заказать обратный звонок</h3>-->
<!--						<p>Просто оставьте свой номер телефона и мы вам перезвоним немедленно!</p>-->
<!--						<a href="#callback" class="btn btn-blue">Заказать звонок</a>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--	<div class="panel panel-default">-->
<!--		<div class="panel-heading" role="tab" id="headingThree" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false"-->
<!--		     aria-controls="collapseTwo">-->
<!--			<h3 class="panel-title text-uppercase">Екатерина +7(937)350-17-31</h3>-->
<!--		</div>-->
<!--		<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">-->
<!--			<div class="panel-body">-->
<!--				<div class="row">-->
<!--					<div class="col-md-3"><img src="/local/templates/lending/assets/img/newtravel/manager/m2.jpg" alt="" class="img-responsive"></div>-->
<!--					<div class="col-md-9">-->
<!--						<p>Я обожаю активный отдых! Кататься на сноуборде в горах Андорры или Австрии или на байке по пляжам Индии! Горы есть и у нас в Красной Поляне, но за тот же-->
<!--							бюджет я выбираю Индию!)</p>-->
<!--					</div>-->
<!--				</div>-->
<!--				<br>-->
<!--				<div class="row">-->
<!--					<div class="col-md-4">-->
<!--						<h3>Задай вопрос эксперту</h3>-->
<!--						<p>Например:</p>-->
<!--						<ul class="small list-unstyled" style="padding-left: 10px">-->
<!--							<li>Какой курорт/пляж лучше --><?//=$config["NAME_WHERE"]?><!--?</li>-->
<!--							<li>Сколько взять с собой денег --><?//=$config["NAME_TO"]?><!--?</li>-->
<!--							<li>Сколько лететь --><?//=$config["NAME_TO"]?><!--?</li>-->
<!--							<li>Какие экскурсии --><?//=$config["NAME_WHERE"]?><!-- выбрать?</li>-->
<!--						</ul>-->
<!--						<br>-->
<!--						<a data-title="Задай вопрос эксперту!" class="btn btn-blue b24-web-form-popup-btn-14">Задать свой вопрос!</a>-->
<!--					</div>-->
<!--					<div class="col-md-4">-->
<!--						<h3>Записаться на консультацию в офисе</h3>-->
<!--						<p>Мы работаем с 10 до 19 по адресу г. Уфа, ул. Гафури 54 (<a href="/about/contacts.php">карта проезда</a>)</p>-->
<!--						<a data-title="Запишись на консультацию в офисе!" class="btn btn-blue b24-web-form-popup-btn-14">Записаться на консультацию!</a>-->
<!--					</div>-->
<!--					<div class="col-md-4">-->
<!--						<h3>Заказать обратный звонок</h3>-->
<!--						<p>Просто оставьте свой номер телефона и мы вам перезвоним немедленно!</p>-->
<!--						<a href="#callback" class="btn btn-blue">Заказать звонок</a>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--	<div class="panel panel-default">-->
<!--		<div class="panel-heading" role="tab" id="headingThree" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree1" aria-expanded="false"-->
<!--		     aria-controls="collapseThree1">-->
<!--			<h3 class="panel-title text-uppercase">Надежда +7(965)943-99-42</h3>-->
<!--		</div>-->
<!--		<div id="collapseThree1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">-->
<!--			<div class="panel-body">-->
<!--				<div class="row">-->
<!--					<div class="col-md-3"><img src="/local/templates/lending/assets/img/newtravel/manager/m3.jpg" alt="" class="img-responsive"></div>-->
<!--					<div class="col-md-9">-->
<!--						<p>Я люблю путешествовать! Как мама, я понимаю, что на отдыхе с ребенком важно чистое море, качественное питание, безопасность. На мой взгляд, лучший семейный отдых в-->
<!--							Юго-Восточной Азии - на Пхукете в Тайланде: идеальный песок и удобный пологий вход в море. Мы с дочкой просто обожаем Тай!-->
<!--						</p>-->
<!--					</div>-->
<!--				</div>-->
<!--				<br>-->
<!--				<div class="row">-->
<!--					<div class="col-md-4">-->
<!--						<h3>Задай вопрос эксперту</h3>-->
<!--						<p>Например:</p>-->
<!--						<ul class="small list-unstyled" style="padding-left: 10px">-->
<!--							<li>Какой курорт/пляж лучше --><?//=$config["NAME_WHERE"]?><!--?</li>-->
<!--							<li>Сколько взять с собой денег --><?//=$config["NAME_TO"]?><!--?</li>-->
<!--							<li>Сколько лететь --><?//=$config["NAME_TO"]?><!--?</li>-->
<!--							<li>Какие экскурсии --><?//=$config["NAME_WHERE"]?><!-- выбрать?</li>-->
<!--						</ul>-->
<!--						<br>-->
<!--						<a data-title="Задай вопрос эксперту!" class="btn btn-blue b24-web-form-popup-btn-14">Задать свой вопрос!</a>-->
<!--					</div>-->
<!--					<div class="col-md-4">-->
<!--						<h3>Записаться на консультацию в офисе</h3>-->
<!--						<p>Мы работаем с 10 до 19 по адресу г. Уфа, ул. Гафури 54 (<a href="/about/contacts.php">карта проезда</a>)</p>-->
<!--						<a data-title="Запишись на консультацию в офисе!" class="btn btn-blue b24-web-form-popup-btn-14">Записаться на консультацию!</a>-->
<!--					</div>-->
<!--					<div class="col-md-4">-->
<!--						<h3>Заказать обратный звонок</h3>-->
<!--						<p>Просто оставьте свой номер телефона и мы вам перезвоним немедленно!</p>-->
<!--						<a href="#callback" class="btn btn-blue">Заказать звонок</a>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--	<div class="panel panel-default">-->
<!--		<div class="panel-heading" role="tab" id="heading4" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false"-->
<!--		     aria-controls="collapseTwo">-->
<!--			<h3 class="panel-title text-uppercase">Анна +7(965)943-99-42</h3>-->
<!--		</div>-->
<!--		<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">-->
<!--			<div class="panel-body">-->
<!--				<div class="row">-->
<!--					<div class="col-md-3"><img src="/local/templates/lending/assets/img/newtravel/manager/m4.jpg" alt="" class="img-responsive"></div>-->
<!--					<div class="col-md-9">-->
<!--						<p>В силу своей профессии была во многих странах! Но моё сердце принадлежит Вьетнаму! Ведь только там можно совместить пляжный отдых (белый песок просто волшебен!),-->
<!--							приключения и отличный и бюджетный шоппинг. А ещё во Вьетнаме есть Азиатский Диснейленд и я знаю о нем практически все!-->
<!--						</p>-->
<!--					</div>-->
<!--				</div>-->
<!--				<br>-->
<!--				<div class="row">-->
<!--					<div class="col-md-4">-->
<!--						<h3>Задай вопрос эксперту</h3>-->
<!--						<p>Например:</p>-->
<!--						<ul class="small list-unstyled" style="padding-left: 10px">-->
<!--							<li>Какой курорт/пляж лучше --><?//=$config["NAME_WHERE"]?><!--?</li>-->
<!--							<li>Сколько взять с собой денег --><?//=$config["NAME_TO"]?><!--?</li>-->
<!--							<li>Сколько лететь --><?//=$config["NAME_TO"]?><!--?</li>-->
<!--							<li>Какие экскурсии --><?//=$config["NAME_WHERE"]?><!-- выбрать?</li>-->
<!--						</ul>-->
<!--						<br>-->
<!--						<a data-title="Задай вопрос эксперту!" class="btn btn-blue b24-web-form-popup-btn-14">Задать свой вопрос!</a>-->
<!--					</div>-->
<!--					<div class="col-md-4">-->
<!--						<h3>Записаться на консультацию в офисе</h3>-->
<!--						<p>Мы работаем с 10 до 19 по адресу г. Уфа, ул. Гафури 54 (<a href="/about/contacts.php">карта проезда</a>)</p>-->
<!--						<a data-title="Запишись на консультацию в офисе!" class="btn btn-blue b24-web-form-popup-btn-14">Записаться на консультацию!</a>-->
<!--					</div>-->
<!--					<div class="col-md-4">-->
<!--						<h3>Заказать обратный звонок</h3>-->
<!--						<p>Просто оставьте свой номер телефона и мы вам перезвоним немедленно!</p>-->
<!--						<a href="#callback" class="btn btn-blue">Заказать звонок</a>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--	<div class="panel panel-default">-->
<!--		<div class="panel-heading" role="tab" id="heading5" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false"-->
<!--		     aria-controls="collapseTwo">-->
<!--			<h3 class="panel-title text-uppercase">Гузель +7(965)943-99-42</h3>-->
<!--		</div>-->
<!--		<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">-->
<!--			<div class="panel-body">-->
<!--				<div class="row">-->
<!--					<div class="col-md-3"><img src="/local/templates/lending/assets/img/newtravel/manager/m5.jpg" alt="" class="img-responsive"></div>-->
<!--					<div class="col-md-9">-->
<!--						<p>Шенгенская виза, визы в Канаду, визы в США - легко! Хотя оформление виз дело серьезное, особенно в Канаду и США, но результат того стоит - незабываемый отдых и море-->
<!--							впечатлений на всю жизнь. Ниагарский водопад, Диснейленд и Лос Анжелес и многое-многое другое!-->
<!--						</p>-->
<!--					</div>-->
<!--				</div>-->
<!--				<br>-->
<!--				<div class="row">-->
<!--					<div class="col-md-4">-->
<!--						<h3>Задай вопрос эксперту</h3>-->
<!--						<p>Например:</p>-->
<!--						<ul class="small list-unstyled" style="padding-left: 10px">-->
<!--							<li>Какой курорт/пляж лучше --><?//=$config["NAME_WHERE"]?><!--?</li>-->
<!--							<li>Сколько взять с собой денег --><?//=$config["NAME_TO"]?><!--?</li>-->
<!--							<li>Сколько лететь --><?//=$config["NAME_TO"]?><!--?</li>-->
<!--							<li>Какие экскурсии --><?//=$config["NAME_WHERE"]?><!-- выбрать?</li>-->
<!--						</ul>-->
<!--						<br>-->
<!--						<a data-title="Задай вопрос эксперту!" class="btn btn-blue b24-web-form-popup-btn-14">Задать свой вопрос!</a>-->
<!--					</div>-->
<!--					<div class="col-md-4">-->
<!--						<h3>Записаться на консультацию в офисе</h3>-->
<!--						<p>Мы работаем с 10 до 19 по адресу г. Уфа, ул. Гафури 54 (<a href="/about/contacts.php">карта проезда</a>)</p>-->
<!--						<a data-title="Запишись на консультацию в офисе!" class="btn btn-blue b24-web-form-popup-btn-14">Записаться на консультацию!</a>-->
<!--					</div>-->
<!--					<div class="col-md-4">-->
<!--						<h3>Заказать обратный звонок</h3>-->
<!--						<p>Просто оставьте свой номер телефона и мы вам перезвоним немедленно!</p>-->
<!--						<a href="#callback" class="btn btn-blue">Заказать звонок</a>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--</div>-->
<!---->
<!--<script id="bx24_form_link" data-skip-moving="true">-->
<!--	(function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;-->
<!--			(w[b].forms=w[b].forms||[]).push(arguments[0])};-->
<!--		if(w[b]['forms']) return;-->
<!--		s=d.createElement('script');r=1*new Date();s.async=1;s.src=u+'?'+r;-->
<!--		h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);-->
<!--	})(window,document,'https://umnik.bitrix24.ru/bitrix/js/crm/form_loader.js','b24form');-->
<!---->
<!--	b24form({"id":"14","lang":"ru","sec":"ow5tkx","type":"link","click":""});-->
<!--</script>-->
<!---->
<!--<script>-->
<!--	$(document).ready(function () {-->
<!--		//Открываем форму заказа звонка-->
<!--		$('a[href="#callback"]').on('click', function () {-->
<!--			$('#callBackModal').modal('show');-->
<!--		});-->
<!--		-->
<!--		$('#ac-container2').accordion({-->
<!--			oneOpenedItem: true-->
<!--		});-->
<!--	});-->
<!--</script>-->