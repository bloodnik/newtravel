<article>
	<div class="news-item">
		<div class="post-wrapper standard-post">
			<div class="post-thumbnail">
				<a href="/new_year_moscow.php?referer1=FromBlogNYMos" target="_blank" title="Туры на Новый год из Москвы">
					<img src="/upload/medialibrary/02e/2.jpg" alt="Туры на Новый год из Москвы"/>
				</a>
			</div>
			<h3 class="post-title">
				<a href="/new_year_moscow.php?referer1=FromBlogNYMos" target="_blank" title="Туры на Новый год из Уфы">Туры на Новый год из Москвы</a>
			</h3>
			<div class="post-containt">
				<p>Мы подготовили для Вас отличную подборку вариантов отдыха на Новый год с прямым вылетом из Москвы.</p>
			</div>
			<div class="post-meta">
				<span class="post-author">
						<a href="/blog/?&arrFilter_pf[POST_AUTHOR]=6&set_filter=Фильтр&set_filter=Y"
						   title="Автор">Лилия Юнусова</a>
				</span>
				<span class="post-date">10.10.2016</span>
			</div>
		</div>
	</div>
</article>