<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<section class="gray-section">
    <div class="container">
        <div class="row">
            <h1 class="title">Данные тура</h1>
        </div>
    </div>
    <div class="container section-inner">
        <div class="row">
            <div class="col-xs-12 tour-data">
                <div class="tour-data-header">
                    <div class="col-md-6">
                        <h3><?=$arTour["TOUR"]["hotelname"]?> <?=$arTour["TOUR"]["hotelstars"]?>* </h3>
                        <span class="small"><?=$arTour["TOUR"]["countryname"]?>, <?=$arTour["TOUR"]["hotelregionname"]?></span>
                    </div>
                    <div class="col-md-6"></div>
                </div>
                <div class="tour-data-body">
                    <div class="col-md-4">
                        <img src="<?=$arTour["TOUR"]["hotelpicturemedium"]?>" alt="<?=$arTour["TOUR"]["hotelname"]?>">
                        <br/>
                        <span><a href="/hotel/<?= $arTour["TOUR"]["hotelcode"] ?>/" target="_blank">описание отеля</a></span>
                        <div class="circle-progress-wrapper">
                            <ul>
                                <li>Места в отеле: <span class="yes">Есть <i class="fa fa-check"></i></span></li>
                                <li>Места на рейс: <span class="yes">Есть <i class="fa fa-check"></i></span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <ul class="list">
                            <li>
                                <span class="title">Дата вылета: </span>
                                <span class="value"><?=$arTour["TOUR"]["flydate"]?>, <?=$arTour["TOUR"]["nights"]?> ночей</span>
                            </li>
                            <li>
                                <span class="title">Город вылета: </span>
                                <span class="value"> <?=$arTour["TOUR"]["departurename"]?></span>
                            </li>
                            <li>
                                <span class="title">Тип номера: </span>
                                <span class="value"><?=$arTour["TOUR"]["room"]?></span>
                            </li>
                            <li>
                                <span class="title">Размещение: </span>
                                <span class="value"><?=$arTour["TOUR"]["placement"]?></span>
                            </li>
                            <li>
                                <span class="title">Тип питания: </span>
                                <span class="value"><?=$arTour["TOUR"]["meal"]?></span>
                            </li>
                            <li>
                                <span class="title">Стоимость тура: </span>
                                <span class="value price" style="padding: 0"><?=CurrencyFormat($arTour["PRICE"], 'RUB')?></span>
                            </li>
                            <li>
                                <span class="title">в т.ч. топливный сбор: </span>
                                <span class="value" ><?=CurrencyFormat($arResult["BASKET_ITEMS"][0]["PROPS"][14]["VALUE"], 'RUB')?></span>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="tour-data-footer">
                    <div class="col-md-6">
                        <span class="sub-title">В стоимость тура включено:</span>
                        <ul class="list">
                            <li>- перелет туда-обратно</li>
                            <li>- трансфер аэропорт-отель-аэропорт*</li>
                            <li>- проживание в отеле</li>
                            <li>- медицинская страховка на время поездки</li>
                        </ul>
                        <small>* В некоторых турах по России трансфер в туре не предоставляется, уточняйте информацию у менеджеров компании.</small>
                    </div>
                    <div class="col-md-6">
                        <p>
                            Для приобретения данного тура, требуется внести данные ниже, выбрать способ оплаты, и согласиться с условиями договора публичной оферты, если у Вас возикли трудности или вопросы, закажите обратный звонок и мы Вас проконсультируем в течении минуты.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
