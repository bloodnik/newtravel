<section class="gray-section">
    <?//PR($_POST);?>
    <div class="container">
        <h1 class="title">Данные туристов</h1>
    </div>
    <div class="container section-inner">
        <div class="row">
            <div class="col-xs-12 tourist-data">
                <div class="alert alert-info text-center">Заполнение данных о туристах необязательно. Менеджер свяжется с Вами, для уточения данных</div>
                <?for ($i = 1; $i <= $arResult["TOURIST_COUNT"]; $i++):?>
                    <h3 class="text-center">Турист <?=$i?></h3>
                    <?
                        switch($i) {
                            case 1:
                                $PropId = "ORDER_PROP_".$arParams["TOURIST_1"];
                                break;
                            case 2:
                                $PropId = "ORDER_PROP_".$arParams["TOURIST_2"];
                                break;
                            case 3:
                                $PropId = "ORDER_PROP_".$arParams["TOURIST_3"];
                                break;
                            case 4:
                                $PropId = "ORDER_PROP_".$arParams["TOURIST_4"];
                                break;
                            case 5:
                                $PropId = "ORDER_PROP_".$arParams["TOURIST_5"];
                                break;
                        }

                        $GenderChecked = "";

                        if(!isset($_POST["gender".$i]) ) {
                            $GenderChecked = "checked";
                        }
                    ?>
                    <input type="hidden" id="<?=$PropId?>" name="<?=$PropId?>" value=""/>

                    <div class="tourist-item">
                        <div class="col-md-6">
                            <span>Персональные данные:</span><br/>
                            <label for="">Пол:</label>
                            <div class="form-group" style="margin-bottom: 7px;">
                                <label for="male">
                                    <input type="radio" class="gender<?=$i?>" id="tmale<?=$i?>" <?=$GenderChecked?> <?if($_POST["gender".$i] == "М"):?>checked<?endif;?>  name="gender<?=$i?>" value="М" />М
                                </label>
                                <label for="female">
                                    <input type="radio" class="gender<?=$i?>" id="tfemale<?=$i?>" <?if($_POST["gender".$i] == "Ж"):?>checked<?endif;?> name="gender<?=$i?>"  value="Ж" />Ж
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="name">Фамилия(лат)</label>
                                <input type="text" class="form-control in_english" placeholder="Familia" id="tfamilia<?=$i?>" name="tfamilia<?=$i?>" value="<?=$_POST["tfamilia".$i]?>" />
                            </div>
                            <div class="form-group">
                                <label for="fathername">Имя (лат)</label>
                                <input type="text" class="form-control" placeholder="Imiya" id="tname<?=$i?>" name="tname<?=$i?>" value="<?=$_POST["tname".$i]?>" />
                            </div>
                            <div class="form-group">
                                <label for="fathername">Дата рождения</label>
                                <input type="text" class="form-control datepicker" placeholder="дд.мм.гггг" id="tbirth<?=$i?>" name="tbirth<?=$i?>" value="<?=$_POST["tbirth".$i]?>" />
                            </div>
                            <div class="form-group">
                                <label for="fathername">Страна рождения</label>
                                <input type="text" class="form-control" placeholder="Russia" id="tcountry<?=$i?>" name="tcountry<?=$i?>" value="<?=$_POST["tcountry".$i]?>" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <span>Данные загранпаспорта:</span> <br/>

                            <div class="row">
                                <div class="col-md-3"><label for="">Серия:</label></div>
                                <div class="col-md-9"><label for="">Номер:</label></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">

                                        <input type="text" class="form-control" placeholder="" id="tseria<?=$i?>" name="tseria<?=$i?>" value="<?=$_POST["tseria".$i]?>" />
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="" id="tnomer<?=$i?>" name="tnomer<?=$i?>" value="<?=$_POST["tnomer".$i]?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name">Дата выдачи</label>
                                <input type="text" class="form-control datepicker" placeholder="дд.мм.гггг" id="tvidan<?=$i?>" name="tvidan<?=$i?>" value="<?=$_POST["tvidan".$i]?>"  />
                            </div>
                            <div class="form-group">
                                <label for="fathername">Дата окончания</label>
                                <input type="text" class="form-control datepicker" placeholder="дд.мм.гггг" id="texpdate<?=$i?>" name="texpdate<?=$i?>"  value="<?=$_POST["texpdate".$i]?>" />
                            </div>
                            <div class="form-group">
                                <label for="fathername">Кем выдан</label>
                                <input type="text" class="form-control" placeholder="" id="tkemvidan<?=$i?>" name="tkemvidan<?=$i?>" value="<?=$_POST["tkemvidan".$i]?>" />
                            </div>
                            <div class="form-group">
                                <label for="fathername">Гражданство</label>
                                <input type="text" class="form-control" placeholder="Russian Federation" id="tnation<?=$i?>" name="tnation<?=$i?>" value="<?=$_POST["tnation".$i]?>" />
                            </div>

                        </div>
                    </div><!--tourist-item-->
                <?endfor;?>

                <div class="add-infant text-center"><label for="addInfant" onclick="changeInfant()"><input type="checkbox" <?if(isset($_POST["addInfant"])):?>checked<?endif;?> onclick="changeInfant()" name="addInfant" id="addInfant" style="margin-right: 10px"/>Добавить младенца(до 2-х лет)</label></div>


                <div class="tourist-item" id="infant" <?if(!isset($_POST["addInfant"])):?>style="display: none"<?endif;?>>
                    <?
                        $GenderCheckedI = "";

                        if(!isset($_POST["genderI"]) ) {
                            $GenderCheckedI = "checked";
                        }
                    ?>

                    <h3 class="text-center">Младенец</h3>
                    <input type="hidden" id="ORDER_PROP_<?=$arParams["INFANT"]?>" name="ORDER_PROP_<?=$arParams["INFANT"]?>" value=""/>
                    <div class="col-md-6">
                        <span>Персональные данные:</span><br/>
                        <label for="">Пол:</label>
                        <div class="form-group" style="margin-bottom: 7px;">
                            <label for="male">
                                <input type="radio" class="genderI" id="tmaleI" <?=$GenderCheckedI?> <?if($_POST["genderI"] == "М"):?>checked<?endif;?>  name="genderI" value="М" />М
                            </label>
                            <label for="female">
                                <input type="radio" class="genderI" id="tfemaleI" <?if($_POST["genderI"] == "Ж"):?>checked<?endif;?> name="genderI"  value="Ж" />Ж
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="name">Фамилия(лат)</label>
                            <input type="text" class="form-control" placeholder="Familia" id="tfamiliaI" name="tfamiliaI" value="<?=$_POST["tfamiliaI"]?>" />
                        </div>
                        <div class="form-group">
                            <label for="fathername">Имя (лат)</label>
                            <input type="text" class="form-control" placeholder="Imiya" id="tnameI" name="tnameI" value="<?=$_POST["tnameI"]?>" />
                        </div>
                        <div class="form-group">
                            <label for="fathername">Дата рождения</label>
                            <input type="text" class="form-control datepicker" placeholder="дд.мм.гггг" id="tbirthI" name="tbirthI" value="<?=$_POST["tbirthI"]?>" />
                        </div>
                        <div class="form-group">
                            <label for="fathername">Страна рождения</label>
                            <input type="text" class="form-control" placeholder="Russia" id="tcountryI" name="tcountryI" value="<?=$_POST["tcountryI"]?>" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <span>Данные загранпаспорта:</span> <br/>

                        <div class="row">
                            <div class="col-md-3"><label for="">Серия:</label></div>
                            <div class="col-md-9"><label for="">Номер:</label></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">

                                    <input type="text" class="form-control" placeholder="" id="tseriaI" name="tseriaI" value="<?=$_POST["tseriaI"]?>" />
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="" id="tnomerI" name="tnomerI" value="<?=$_POST["tnomerI"]?>" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name">Дата выдачи</label>
                            <input type="text" class="form-control datepicker" placeholder="дд.мм.гггг" id="tvidanI" name="tvidanI" value="<?=$_POST["tvidanI"]?>"  />
                        </div>
                        <div class="form-group">
                            <label for="fathername">Дата окончания</label>
                            <input type="text" class="form-control datepicker" placeholder="дд.мм.гггг" id="texpdateI" name="texpdateI"  value="<?=$_POST["texpdateI"]?>" />
                        </div>
                        <div class="form-group">
                            <label for="fathername">Кем выдан</label>
                            <input type="text" class="form-control" placeholder="" id="tkemvidanI" name="tkemvidanI" value="<?=$_POST["tkemvidanI"]?>" />
                        </div>
                        <div class="form-group">
                            <label for="fathername">Гражданство</label>
                            <input type="text" class="form-control" placeholder="Russian Federation" id="tnationI" name="tnationI" value="<?=$_POST["tnationI"]?>" />
                        </div>

                    </div>
                </div> <!--tourist-item-->
            </div>
        </div>
    </div>
</section>



<script>
    function setTouristsData(tcount){
        for (var i = 1; i <= tcount; i++) {

            var ProdId, TouristData = "";
            switch(i) {
                case 1:
                    ProdId = "ORDER_PROP_<?=$arParams["TOURIST_1"]?>";
                    break;
                case 2:
                    ProdId = "ORDER_PROP_<?=$arParams["TOURIST_2"]?>";
                    break;
                case 3:
                    ProdId = "ORDER_PROP_<?=$arParams["TOURIST_3"]?>";
                    break;
                case 4:
                    ProdId = "ORDER_PROP_<?=$arParams["TOURIST_4"]?>";
                    break;
                case 5:
                    ProdId = "ORDER_PROP_<?=$arParams["TOURIST_5"]?>";
                    break;
            }

            TouristData = "Пол: " + $(".gender"+i+":checked").val() + "\r\n";
            TouristData += "Фамилия: " + $("#tfamilia"+i).val() + "\r\n";
            TouristData += "Имя: " + $("#tname"+i).val() + "\r\n";
            TouristData += "Дата рождения: " + $("#tbirth"+i).val() + "\r\n";
            TouristData += "Страна рождения: " + $("#tcountry"+i).val() + "\r\n";
            TouristData += "Серия: " + $("#tseria"+i).val() + " Номер: " + $("#tnomer"+i).val() + "\r\n";
            TouristData += "Дата выдачи: " + $("#tvidan"+i).val() + "\r\n";
            TouristData += "Дата окончания: " + $("#texpdate"+i).val() + "\r\n";
            TouristData += "Кем выдан: " + $("#tkemvidan"+i).val() + "\r\n";
            TouristData += "Гражданство: " + $("#tnation"+i).val() + "\r\n";

            $("#"+ProdId).val(TouristData);
        }

        if($("#addInfant").prop("checked")){
            TouristData = "Пол: " + $(".genderI"+":checked").val() + "\r\n";
            TouristData += "Фамилия: " + $("#tfamiliaI").val() + "\r\n";
            TouristData += "Имя: " + $("#tnameI").val() + "\r\n";
            TouristData += "Дата рождения: " + $("#tbirthI").val() + "\r\n";
            TouristData += "Страна рождения: " + $("#tcountryI").val() + "\r\n";
            TouristData += "Серия: " + $("#tseriaI").val() + " Номер: " + $("#tnomerI").val() + "\r\n";
            TouristData += "Дата выдачи: " + $("#tvidanI").val() + "\r\n";
            TouristData += "Дата окончания: " + $("#texpdateI").val() + "\r\n";
            TouristData += "Кем выдан: " + $("#tkemvidanI").val() + "\r\n";
            TouristData += "Гражданство: " + $("#tnationI").val() + "\r\n";

            console.log(TouristData);

            $("#ORDER_PROP_<?$arParams["INFANT"]?>").val(TouristData);
        }


    }

    
    /*========================================================================
     Выбор младенца
     ==========================================================================*/
    function changeInfant() {
        if ($("#addInfant").prop("checked")) {
            $("#infant").slideDown(500);
        } else {
            $("#infant").slideUp(500);
        };
    }
</script>