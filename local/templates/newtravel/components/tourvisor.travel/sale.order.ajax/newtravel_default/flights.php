<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(!$USER->IsAuthorized()):?>
    <div class="col-xs-12 text-center">
        <div class="row">
            <div class="alert alert-warning">
                Если у Вас уже есть учетная запись на сайте, выполните
                <a href="/auth/" class="btn btn-white"
                   onclick="$('body').scrollTo(0); return authFormWindow.ShowLoginForm()">вход</a>
            </div>
        </div>
    </div>
<?endif;?>
<?if(!empty($arTour["FLIGHTS"])):?>
    <section class="gray-section">
        <div class="container">
            <h1 class="title">Выбор рейса и туроператора</h1>
        </div>
        <div class="container section-inner">

            <div class="row">
                <div class="col-xs-12 turoperator-data">
                    <div class="col-md-4">
                        Туроператор
                    </div>
                    <div class="col-md-4">
                        Авиакомпания
                    </div>
                    <div class="col-md-4">
                        Цена за номер
                    </div>
                    <?$prevAirCompany = "";?>
                    <?foreach($arTour["FLIGHTS"]["flight"] as $key=>$arFlight):?>
                        <?
                            $checked = "";
                        ?>
                        <?if(isset($_POST["turoperator"]) && $_POST["turoperator"] == $key):?>
                            <?$checked = "checked";?>
                            <?
                            $selectedFlight = "Авиакомпания: " . $arFlight["forward"]["aircompanyrus"] . "\r\n";
                            $selectedFlight .= "Рейс туда: " . $arFlight["forward"]["flydate"] . "\r\n";
                            $selectedFlight .= $arFlight["forward"]["timefrom"] . " " . $arFlight["forward"]["terminalfrom"] . "-" . $arFlight["forward"]["timeto"] . " " . $arFlight["forward"]["terminalto"] . "\r\n";
                            $selectedFlight .= $arFlight["forward"]["plane"] . "(" . $arFlight["forward"]["flightnum"] . ") \r\n";
                            $selectedFlight .= "Рейс обратно: " . $arFlight["backward"]["flydate"] . "\r\n";
                            $selectedFlight .= $arFlight["backward"]["timefrom"] . " " . $arFlight["backward"]["terminalfrom"] . "-" . $arFlight["backward"]["timeto"] . " " . $arFlight["backward"]["terminalto"] . "\r\n";
                            $selectedFlight .= $arFlight["backward"]["plane"] . "(" . $arFlight["backward"]["flightnum"] . ")";
                            ?>
                        <?elseif(empty($_POST) && $key == 0):?>
                            <?$checked = "checked";?>
                            <?
                            $selectedFlight = "Авиакомпания: " . $arFlight["forward"]["aircompanyrus"] . "\r\n";
                            $selectedFlight .= "Рейс туда: " . $arFlight["forward"]["flydate"] . "\r\n";
                            $selectedFlight .= $arFlight["forward"]["timefrom"] . " " . $arFlight["forward"]["terminalfrom"] . "-" . $arFlight["forward"]["timeto"] . " " . $arFlight["forward"]["terminalto"] . "\r\n";
                            $selectedFlight .= $arFlight["forward"]["plane"] . "(" . $arFlight["forward"]["flightnum"] . ") \r\n";
                            $selectedFlight .= "Рейс обратно: " . $arFlight["backward"]["flydate"] . "\r\n";
                            $selectedFlight .= $arFlight["backward"]["timefrom"] . " " . $arFlight["backward"]["terminalfrom"] . "-" . $arFlight["backward"]["timeto"] . " " . $arFlight["backward"]["terminalto"] . "\r\n";
                            $selectedFlight .= $arFlight["backward"]["plane"] . "(" . $arFlight["backward"]["flightnum"] . ")";
                            ?>
                        <?endif;?>

                        <?if($arFlight["forward"]["aircompanyrus"] !== $prevAirCompany):?>
                            <div class="item">
                                <label for="turoperator<?=$key?>">
                                    <div class="col-md-4">
                                        <input type="radio" id="turoperator<?=$key?>" name="turoperator" onclick="BX('turoperator<?=$key?>').checked=true;changeFlight();" value="<?=$key?>" <?=$checked?> data-sub-item="sub-item<?=$key?>"/>
                                        <input type="hidden" name="turoperatorPrice<?=$key?>" value="<?=$arFlight["pricedata"]["price"]?>"/>
                                        <input type="hidden" name="turoperatorOilTax<?=$key?>" value="<?=$arFlight["pricedata"]["fuelcharge"]?>"/>
                                        <img src="https://tourvisor.ru/pics/operators/searchlogo/<?=$arTour["TOUR"]["operatorcode"]?>.gif" alt="<?=$arTour["TOURS"]["operatorname"]?>">
                                    </div>
                                    <div class="col-md-4">
                                        <?=$arFlight["forward"]["aircompanyrus"]?>
                                    </div>
                                    <div class="col-md-4 price">
                                        <?=CurrencyFormat($arFlight["pricedata"]["price"], "RUB")?>
                                    </div>
                                </label>
                            </div>
                            <div class="sub-item" id="sub-item<?=$key?>" <?if($checked == ""):?>style="display: none" <?endif?>>
                                <div class="col-md-6 small">
                                    <label for="forward<?=$key?>">
                                        <input type="radio" id="forward<?=$key?>" <?=$checked?> name="forward<?=$key?>" value="forward<?=$key?>"/>
                                        <strong style="color:#00AEEF">Рейс туда</strong> - <?=$arFlight["forward"]["flydate"]?>
                                    </label>
                                    <br/>
                                    <strong><?=$arFlight["forward"]["timefrom"]?></strong> <?=$arFlight["forward"]["terminalfrom"]?> - <strong><?=$arFlight["forward"]["timeto"]?></strong> <?=$arFlight["forward"]["terminalto"]?><br/>
                                    <span><?=$arFlight["forward"]["plane"]?>(<?=$arFlight["forward"]["flightnum"]?>)</span>
                                    <br/>
                                </div>

                                <div class="col-md-6 small">
                                    <label for="backward<?=$key?>">
                                        <input type="radio" id="backward<?=$key?>" <?=$checked?> name="backward<?=$key?>" value="backward<?=$key?>"/>
                                        <strong style="color:#00AEEF">Рейс обратно</strong> - <?=$arFlight["backward"]["flydate"]?>
                                    </label>
                                    <br/>
                                    <strong><?=$arFlight["backward"]["timefrom"]?></strong> <?=$arFlight["backward"]["terminalfrom"]?> - <strong><?=$arFlight["backward"]["timeto"]?></strong> <?=$arFlight["backward"]["terminalto"]?><br/>
                                    <span><?=$arFlight["backward"]["plane"]?>(<?=$arFlight["backward"]["flightnum"]?>)</span>
                                </div>
                            </div>
                        <?endif;?>
                        <?$prevAirCompany = $arFlight["forward"]["aircompanyrus"];?>
                    <?endforeach;?>
                    <input type="hidden" name="ORDER_PROP_<?=$arParams["FLIGHT_DATA"]?>" id="ORDER_PROP_<?=$arParams["FLIGHT_DATA"]?>" value="<?=$selectedFlight?>"/>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        /*=======================================================================
         Выбор туроператора и рейса
         ========================================================================= */
        function changeFlight() {
            $(".sub-item").slideUp(500);
            $(".sub-item input").prop("checked", false);
            var subItem = $(".turoperator-data .item input:checked").attr("data-sub-item");
            $("#" + subItem).slideDown(500);

            $("#" + subItem).find("input").prop("checked", true);

            submitForm();
        }
    </script>
<?endif;?>