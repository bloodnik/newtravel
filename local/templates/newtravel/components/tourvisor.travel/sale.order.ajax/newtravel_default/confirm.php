<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
    if ($arDeliv = CSaleDelivery::GetByID($arResult["ORDER"]["DELIVERY_ID"]))
    {
        //PR($arDeliv);
    }

    if ($arPaySys = CSalePaySystem::GetByID($arResult["ORDER"]["DELIVERY_ID"], $arResult["ORDER"]["PERSON_TYPE_ID"]))
    {
        //PR($arPaySys);
    }
?>

<section class="gray-section">
    <div class="container">
        <h1 class="title"><?=GetMessage("SOA_TEMPL_ORDER_COMPLETE")?></h1>
    </div>
    <div class="container section-inner">
        <div class="col-xs-12">
            <?
            if (!empty($arResult["ORDER"]))
            {
                ?>
                <table class="sale_order_full_table">
                    <tr>
                        <td>
                            <?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"]))?>
                            <br /><br />
                            <?= GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?>
                        </td>
                    </tr>
                </table>

                <?if(isset($arDeliv)):?>
                    <br/>
                    <div class="pay_name"><strong>Выбранный офис обслуживания:</strong></div>
                    <div class="paysystem_name"><?=$arDeliv["NAME"]?></div>

                    <small><?=$arDeliv["DESCRIPTION"]?></small>
                <?endif;?>

                <?
                if (!empty($arResult["PAY_SYSTEM"]))
                {
                    ?>
                    <br /><br />

                    <table class="sale_order_full_table">
                        <tr>
                            <td class="ps_logo">
                                <div class="pay_name"><?=GetMessage("SOA_TEMPL_PAY")?></div>
                                <?=CFile::ShowImage($arResult["PAY_SYSTEM"]["LOGOTIP"], 100, 100, "border=0", "", false);?>
                                <div class="paysystem_name"><?= $arResult["PAY_SYSTEM"]["NAME"] ?></div><br>
                            </td>
                        </tr>
                        <?
                        if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
                        {
                            ?>
                            <tr>
                                <td>
                                    <?
                                    if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
                                    {
                                        ?>
                                        <script language="JavaScript">
                                            window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
                                        </script>
                                        <?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))))?>
                                        <?
                                        if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE']))
                                        {
                                            ?><br />
                                            <?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&pdf=1&DOWNLOAD=Y")) ?>
                                            <?
                                        }
                                    }
                                    else
                                    {
                                        if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
                                        {
                                            include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?
                        }
                        ?>
                    </table>
                    <?
                }
            }
            else
            {
                ?>
                <b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

                <table class="sale_order_full_table">
                    <tr>
                        <td>
                            <?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
                            <?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
                        </td>
                    </tr>
                </table>
                <?
            }
            ?>
        </div>
    </div>
</section>
