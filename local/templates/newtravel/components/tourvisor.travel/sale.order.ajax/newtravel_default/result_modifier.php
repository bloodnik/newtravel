<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$TOUR_ID = $arResult["BASKET_ITEMS"][0]["PROPS"][16]["VALUE"];

if (!empty($TOUR_ID)) {
    $login = "lili";
    $pass = "271000";

    $param .= "&tourid=" . $TOUR_ID;
    $param .= "&flights=1";
    $param .= "&request=0";

    $url = "http://tourvisor.ru/xml/actualize.php?format=json" . $param . "&authlogin=" . $login . "&authpass=" . $pass;
    $arTour = makeQuery($url);

    $arFlights = $arTour["data"]["flights"];
    $arTour = $arTour["data"]["tour"];
    $arResult["BASKET_ITEMS"][0]["FLIGHTS"] = $arFlights;
    $arResult["BASKET_ITEMS"][0]["TOUR"] = $arTour;

    //Не закончено
    $tempForward = array();
    $tempBackward = array();
    foreach($arFlights["flight"] as $key=>$arFlight) {

        if(!in_array($arFlight["forward"]["flightnum"], $tempForward)){
            $tempForward[] = $arFlight["forward"]["flightnum"];

            $dataFly[$arFlight["forward"]["aircompany"]]["forward"][] = $arFlight["forward"];

        }
        if(!in_array($arFlight["backward"]["flightnum"], $tempBackward)){
            $tempBackward[] = $arFlight["backward"]["flightnum"];

            $dataFly[$arFlight["backward"]["aircompany"]]["backward"][] = $arFlight["backward"];
        }

    }

    //PR($dataFly);

}

$AdultsCount = $arResult["BASKET_ITEMS"][0]["PROPS"][4]["VALUE"]; //Количество взрослых
$ChildsCount = $arResult["BASKET_ITEMS"][0]["PROPS"][13]["VALUE"]; //Количество детей

$arResult["TOURIST_COUNT"] = $AdultsCount + $ChildsCount; //Общее количество туристов

//Функция выполнения get-запроса через CURl
function makeQuery($url)
{
    if ($curl = curl_init())
        curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $json = curl_exec($curl);
    $arRes = objectToArr(json_decode($json));
    curl_close($curl);
    return $arRes;
}