<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<section class="gray-section">
	<div class="container section-inner">
		<div class="row">
			<div class="col-xs-12 pay-data">
				<div class="summary">
					<div class="col-md-9">
						<span class="itogo">Итого к оплате:</span>
						<span
							class="price"><?=$arResult["ORDER_TOTAL_PRICE_FORMATED"]?></span> <? if (isset($arResult["BANK_MARGIN"])): ?>(в том числе <?=CurrencyFormat($arResult["BANK_MARGIN"], "RUB")?> комиссия банка)<? endif; ?>
						<br/>

						<p class="small">
							Внимание! Данный тура требует дополнительной проверки менеджером. <br/>
							Окончательная стоимость тура может быть скорректирована.
						</p>
					</div>
					<div class="col-md-3">
						<input type="button" onclick="submitForm('Y'); return false;" class="btn btn-blue btn-lg pull-right" value="Оформить заказ"/>
						<a href="" style="display: none" id="startBtn" onclick="startScripts()">Запустить</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>