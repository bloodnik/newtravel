<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
	<section class="breadcrumb-wrapper">
		<div class="container">
			<div class="row">
				<div class="page-description">
					<h3><? echo $arResult["USER"]["LAST_NAME"] ? $arResult["USER"]["LAST_NAME"] . " " : "";
						echo $arResult["USER"]["NAME"] ? $arResult["USER"]["NAME"] . " " : "";
						echo $arResult["USER"]["SECOND_NAME"] ? $arResult["USER"]["SECOND_NAME"] . " " : ""; ?>, собственной персоной! Это вы!</h3>
				</div>
			</div>
		</div>
	</section>

	<section class="white-section personal-page">
		<div class="container">
			<div class="row">
				<div class="about-me-wrapper">
					<div class="col-md-6">
						<div class="about-me">
							<div class="img-cover">
								<div class="thumbnail">
									<img src="<?=SITE_TEMPLATE_PATH?>/images/user-bg.jpg" alt="member cover"/>
								</div>
							</div>
							<div class="member-img">
								<div class="thumbnail">
									<? $w = 140;
									$h    = 140;
									$src = SimaiResizeImage($arResult["USER"]["PERSONAL_PHOTO"], $w, $h) ?>
									<img src="<?=($src ? $src : SITE_TEMPLATE_PATH . "/images/no-avatar.gif")?>" width="<?=$w?>" height="<?=$h?>"/>
								</div>
							</div>
							<div class="member-info">
								<h2 class="name"><? echo $arResult["USER"]["LAST_NAME"] ? $arResult["USER"]["LAST_NAME"] . " " : "";
									echo $arResult["USER"]["NAME"] ? $arResult["USER"]["NAME"] . " " : "";
									echo $arResult["USER"]["SECOND_NAME"] ? $arResult["USER"]["SECOND_NAME"] . " " : ""; ?></h2>
								<h3 class="title"><?=$arResult["USER"]["PERSONAL_PROFESSION"]?></h3>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="text-widget">
							<div class="text-widget-title">
								<h3>Персоноальные данные</h3>
							</div>
							<ul class="personal-details">
								<li>
									<span class="title">Логин  на сайте</span>
									<span class="value"><?=$arResult["USER"]["LOGIN"]?></span>
								</li>
								<li>
									<span class="title">ФИО</span>
									<? if ($arResult["USER"]["NAME"]): ?>
										<span class="value"><? echo $arResult["USER"]["LAST_NAME"] ? $arResult["USER"]["LAST_NAME"] . " " : "";
											echo $arResult["USER"]["NAME"] ? $arResult["USER"]["NAME"] . " " : "";
											echo $arResult["USER"]["SECOND_NAME"] ? $arResult["USER"]["SECOND_NAME"] . " " : ""; ?></span>
									<? endif; ?>
								</li>
								<li>
									<span class="title">Дата рождения</span>
									<span class="value">16.03.2014</span>
								</li>
								<li>
									<span class="title">Контакный номер</span>
									<span class="value"><?=$arResult["USER"]["PERSONAL_PHONE"] ?: "Не указан"?></span>
								</li>
								<li>
									<span class="title">Контактный @-адрес</span>
									<span class="value"><a href="mailto:<?=$arResult["USER"]["EMAIL"]?>"><?=$arResult["USER"]["EMAIL"]?></a></span>
								</li>
								<li>
									<span class="title">Страна, город</span>
									<span class="value">Россия, Уфа</span>
								</li>
							</ul>
							<a href="/personal/profile/edit.php">Редактировать свои данные</a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="text-widget ">
							<div class="text-widget-title">
								<h3>Личный счет на сайте</h3>
							</div>
							<h5>На вашем счету:</h5>
							<div class="price">
								<? $APPLICATION->IncludeComponent("bitrix:sale.personal.account", "profile", Array(
										"SET_TITLE" => "N"
									)
								); ?>
							</div>
						</div>
						<div class="text-widget categories">
							<ul>
								<li><a href="#myOrders">Мои заказы</a></li>
								<li><a href="#myDocs">Мои документы в поездку</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="gray-section" id='myOrders'>
		<div class="container">
			<div class="row">
				<div class="text-widget">
					<div class="text-widget-title">
						<h3>Мои заказы:</h3>
					</div>

					<? if ($arResult["SALE_ORDER"]):
						$arStatus = array(
							"N" => "Принят",
							"P" => "Частично оплачен",
							"F" => "Вцыполнен",
							"B" => "Бронирование"
						); ?>

						<ul class="portfolio-grid-3 order-list" id="da-thumbs">
							<? foreach ($arResult["SALE_ORDER"] as $key => $arItem): ?>
								<li class="">
									<ul class="personal-details">
										<li>
											<span class="title">Номер тура:</span>
											<span class="value"><?=$arItem["ID"]?></span>
										</li>
										<li>
											<span class="title">Стоимость:</span>
											<span class="value"><?=$arItem["PRICE"]?></span>
										</li>
										<li>
											<span class="title">Статус:</span>
											<span class="value"><?=$arStatus[ $arItem["STATUS_ID"] ]?></span>
										</li>
										<li>
											<span class="title">Оплачен:</span>
											<span class="value"><? if ($arItem["PAYED"] == "Y"): ?>оплачено (№ <?=$arItem["EMP_PAYED_ID"]?>)<? else: ?> неоплачено<? endif; ?></span>
										</li>
										<li>
											<span class="title">Отменен:</span>
											<span class="value"><? if ($arItem["CANCELED"] == Y): ?>Да<? else: ?>Нет<? endif; ?></span>
										</li>
										<a href="/personal/order/detail/<?=$arItem["ID"]?>/">Подробнее</a>
									</ul>
								</li>
							<? endforeach; ?>
						</ul>
					<? else: ?>
						<p>Тут будут отражены Ваши заказы, их статусы после их оформления в магазине</p>
					<? endif ?>
				</div>
			</div>
		</div>
	</section>


	<!-- Документы в поездку -->
	<section class='white-section' id='myDocs'>
		<div class="container">
			<div class="row">
				<div class="text-widget">
					<div class="text-widget-title">
						<h3>Мои документы в поездку</h3>
						<? if (is_array($arResult["USER"]["UF_FILES"]) && ! empty($arResult["USER"]["UF_FILES"])): ?>
							<ul class="docss">
								<? foreach ($arResult["USER"]["UF_FILES"] as $v):$arFile = CFile::GetFileArray($v); ?>
									<li><a href="<?=$arFile["SRC"]?>"><?=$arFile["ORIGINAL_NAME"]?></a></li>
								<? endforeach ?>
							</ul>
							<div class="tss"> * — Тут билеты: сохрани и распечатай с собой в поездку!</div>
						<? else: ?>
							Нет документов
						<? endif ?>
					</div>
				</div>
			</div>
		</div>
	</section><!-- /Документы в поездку -->