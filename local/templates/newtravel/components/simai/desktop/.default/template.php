<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!defined("BX_GADGET_DEFAULT"))
{
	define("BX_GADGET_DEFAULT", true);
	?>
	<script type="text/javascript">
	var updateURL = '<?=htmlspecialcharsback($arResult['UPD_URL'])?>';
	var langGDError1 = '<?=GetMessage("CMDESKTOP_TDEF_ERR1")?>';
	var langGDError2 = '<?=GetMessage("CMDESKTOP_TDEF_ERR2")?>';
	var langGDConfirm1 = '<?=GetMessage("CMDESKTOP_TDEF_CONF")?>';
	var langGDConfirmUser = '<?=GetMessage("CMDESKTOP_TDEF_CONF_USER")?>';
	var langGDConfirmGroup = '<?=GetMessage("CMDESKTOP_TDEF_CONF_GROUP")?>';
	var langGDCancel = "<?echo GetMessage("CMDESKTOP_TDEF_CANCEL")?>";
	</script>
	<?if($arResult["PERMISSION"]>"R"):?>
	<script type="text/javascript" src="/bitrix/components/bitrix/desktop/script.js?v=<?=filemtime($_SERVER['DOCUMENT_ROOT'].'/bitrix/components/bitrix/desktop/script.js');?>"></script>
	<?endif?>
	<div id="antiselect" style="height:100%; width:100%; left: 0; top: 0; position: absolute; -moz-user-select: none !important; display: none; background-color:#FFFFFF; -moz-opacity: 0.01;"></div>
	<?
}


$gadgetButtons = "";
?>




<?if($arResult["PERMISSION"]>"R"): // Это меню справа для админа?>
<?
$allGD = Array();
foreach($arResult['ALL_GADGETS'] as $gd)
{
	$allGD[] = Array(
		'ID' => $gd["ID"],
		'TEXT' =>
			'<div style="text-align: left;">'.($gd['ICON1']?'<img src="'.($gd['ICON']).'" align="left">':'').
			'<b>'.(htmlspecialchars($gd['NAME'])).'</b><br>'.(htmlspecialchars($gd['DESCRIPTION'])).'</div>',
		);
}
?>
<script type="text/javascript">
arGDGroups = <?=CUtil::PhpToJSObject($arResult["GROUPS"])?>;
new BXGadget('<?=$arResult["ID"]?>', <?=CUtil::PhpToJSObject($allGD)?>);
</script>



<?
$gadgetButtons = '
<div class="sidebar-buttons">
	<a href="" class="sidebar-button" onclick="getGadgetHolder(\''.AddSlashes($arResult["ID"]).'\').ShowAddGDMenu(this);return false;">
		<span class="sidebar-button-top"><span class="corner left"></span><span class="corner right"></span></span>
		<span class="sidebar-button-content"><span class="sidebar-button-content-inner"><i class="sidebar-button-create"></i><b>'.GetMessage("CMDESKTOP_TDEF_ADD").'</b></span></span>
		<span class="sidebar-button-bottom"><span class="corner left"></span><span class="corner right"></span></span>
	</a>';


if($arResult["PERMISSION"]>"W")
{
	if ($arParams["MODE"] == "SU")
		$mode = "'SU'";
	elseif ($arParams["MODE"] == "SG")
		$mode = "'SG'";
	else
		$mode = "";

	$gadgetButtons .= '<a href="" class="sidebar-button" onclick="getGadgetHolder(\''.AddSlashes($arResult["ID"]).'\').SetForAll('.$mode.'); return false;">
		<span class="sidebar-button-top"><span class="corner left"></span><span class="corner right"></span></span>
		<span class="sidebar-button-content"><span class="sidebar-button-content-inner"><i class="sidebar-button-accept"></i><b>'.GetMessage("CMDESKTOP_TDEF_SET").'</b></span></span>
		<span class="sidebar-button-bottom"><span class="corner left"></span><span class="corner right"></span></span>
	</a>';
}
	$gadgetButtons .= '<a href="" class="sidebar-button" onclick="getGadgetHolder(\''.AddSlashes($arResult["ID"]).'\').ClearUserSettings(); return false;">
		<span class="sidebar-button-top"><span class="corner left"></span><span class="corner right"></span></span>
		<span class="sidebar-button-content"><span class="sidebar-button-content-inner"><i class="sidebar-button-delete"></i><b>'.GetMessage("CMDESKTOP_TDEF_CLEAR").'</b></span></span>
		<span class="sidebar-button-bottom"><span class="corner left"></span><span class="corner right"></span></span>
	</a>';

$gadgetButtons .= '</div>';
?>

<?endif;?>



<form action="<?=POST_FORM_ACTION_URI?>" method="POST" id="GDHolderForm_<?=$arResult["ID"]?>">
<input type="hidden" name="holderid" value="<?=$arResult["ID"]?>">
<input type="hidden" name="gid" value="0">
<input type="hidden" name="action" value="">
</form>




<table class="gadgetholder gadgetholder-<?=$arResult["ID"]?>" cellspacing="0" cellpadding="0" width="100%" id="GDHolder_<?=$arResult["ID"]?>">
  <tbody>
    <tr>
    <?for($i=0; $i<$arResult["COLS"]; $i++):?>
    	<?if($i==0):?>
    	<td class="gd-page-column<?=$i?>" valign="top" width="<?=$arResult["COLUMN_WIDTH"][$i]?>" id="s0">
	    <?if ($arResult["COLS"] == 1):?><?=$gadgetButtons?><?endif?>
    	<?elseif($i==$arResult["COLS"]-1):?>
	 	  <td width="10">
	        <div style="WIDTH: 10px"></div>
	        <br />
	      </td>
	      <td class="gd-page-column<?=$i?>" valign="top" width="<?=$arResult["COLUMN_WIDTH"][$i]?>" id="s2">
		    <?=$gadgetButtons?>
    	<?else:?>
	 	  <td width="10">
	        <div style="WIDTH: 10px"></div>
	        <br />
	      </td>
	      <td class="gd-page-column<?=$i?>" valign="top"  width="<?=$arResult["COLUMN_WIDTH"][$i]?>" id="s1">
 		<?endif?>
		<?foreach($arResult["GADGETS"][$i] as $arGadget):
			$bChangable = true;
			if (
				!$GLOBALS["USER"]->IsAdmin() 
				&& array_key_exists("GADGETS_FIXED", $arParams) 
				&& is_array($arParams["GADGETS_FIXED"]) 
				&& in_array($arGadget["GADGET_ID"], $arParams["GADGETS_FIXED"])
				&& array_key_exists("CAN_BE_FIXED", $arGadget)
				&& $arGadget["CAN_BE_FIXED"]
			)
				$bChangable = false;
			?>
			<table id="t<?=$arGadget["ID"]?>" class="data-table-gadget<?=($arGadget["HIDED"]?' gdhided':'')?>"><tr><td><div class="gdparent">
					<div class="gdheader" style="cursor:move;" onmousedown="return getGadgetHolder('<?=AddSlashes($arResult["ID"])?>').DragStart('<?=$arGadget["ID"]?>', event)" onmouseover="this.className=this.className.replace(/\s*gdheader-hover\s*/,'') + ' gdheader-hover';" onmouseout="this.className=this.className.replace(/\s*gdheader-hover\s*/,'')">

						<div class="gdheader-actions">
						<?
						if ($bChangable)
						{
							?><a class="gdremove" href="javascript:void(0)" onclick="return getGadgetHolder('<?=AddSlashes($arResult["ID"])?>').Delete('<?=$arGadget["ID"]?>');" title="<?=GetMessage("CMDESKTOP_TDEF_DELETE")?>"></a><?
						}
						?>
						<a class="gdhide" href="javascript:void(0)" onclick="return getGadgetHolder('<?=AddSlashes($arResult["ID"])?>').Hide('<?=$arGadget["ID"]?>', this);" title="<?=GetMessage("CMDESKTOP_TDEF_HIDE")?>"></a>
						<?
						if ($bChangable)
						{
							?><a class="gdsettings<?=($arGadget["NOPARAMS"]?' gdnoparams':'')?>" href="javascript:void(0)" onclick="return getGadgetHolder('<?=AddSlashes($arResult["ID"])?>').ShowSettings('<?=$arGadget["ID"]?>');" title="<?=GetMessage("CMDESKTOP_TDEF_SETTINGS")?>"></a><?
						}
						?>
						</div>
						<div class="gdheader-title"><?=$arGadget["TITLE"]?></div>
					</div>
				<div class="ghheader-underline"></div>
				<div class="gdoptions" style="display:none" id="dset<?=$arGadget["ID"]?>"></div>
				<div class="gdcontent" id="dgd<?=$arGadget["ID"]?>">
					<?=$arGadget["CONTENT"]?>
				</div>
			</div></td></tr></table>
			<div style="display:none; border:1px #404040 dashed; margin-bottom:8px;" id="d<?=$arGadget["ID"]?>"></div>
 		<?endforeach;?>
 	  </td>
    <?endfor;?>
    </tr>
  </tbody>
</table>
