<?
$MESS["CMDESKTOP_TDEF_ERR1"] = "Beim Speichern der Gadgetposition auf dem Server ist ein Fehler aufgetreten";
$MESS["CMDESKTOP_TDEF_ERR2"] = "Es ist ein Fehler beim hinzufьgen des Gadgets auf den Server aufgetreten";
$MESS["CMDESKTOP_TDEF_CONF"] = "Ihre Dashboard-Einstellungen werden fьr alle nicht autorisierte oder neue Nutzer auf Standard gesetzt. Wollen Sie fortfahren?";
$MESS["CMDESKTOP_TDEF_CONF_USER"] = "Ihre Arbeitsplatzeinstellungen werden an alle neue Nutzerprofile als Standard angewendet. Wollen Sie fortfahren?";
$MESS["CMDESKTOP_TDEF_CONF_GROUP"] = "Ihre Arbeitsplatzeinstellungen werden an alle neue Nutzerprofile als Standard angewendet. Wollen Sie fortfahren?";
$MESS["CMDESKTOP_TDEF_ADD"] = "Hinzufьgen";
$MESS["CMDESKTOP_TDEF_SET"] = "Als Standardeinstellungen speichern";
$MESS["CMDESKTOP_TDEF_CLEAR"] = "Aktuelle Einstellungen zurьcksetzen";
$MESS["CMDESKTOP_TDEF_CANCEL"] = "Abbrechen";
$MESS["CMDESKTOP_DESC_NAME"] = "Dashboard";
$MESS ['CMDESKTOP_DEMO_DATA_BLOCK_TITLE'] = "Demodaten lцschen";
$MESS ['CMDESKTOP_DEMO_DATA_BLOCK_DESC'] = 'Um die Demodaten von Ihrem Portal zu lцschen, verwenden Sie bitte den <b>Bereinigungsassistent</b>. Um den Assistent zu starten klicken Sie auf "Bereinigungsassistent" im Administrativen Panel oder auf <a href="#LINK_TO_WIZARD#">diesen Link</a>.';
?>