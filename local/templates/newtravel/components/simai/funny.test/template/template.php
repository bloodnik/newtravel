<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["TEST_FALSE_COUNT"] == 0):?>
<h2>Результаты теста:</h2>
<?
foreach($arResult["SECTION_TEST"][0]["UF_FUNNY_TEST_RESULT"] as $arItem)
{
	$matches = array();
	preg_match('/\s*(\d+)[\s-_]+(\d+)/', $arItem, $matches);
	if($matches[1] <= $arResult["TEST_RESULT"] && $arResult["TEST_RESULT"] <= $matches[2])
	{
?>
<h3><?=$arItem?></h3>
<?
	}
}
?>
<br>
<a href="/funny_test.php?reload_funny_test=1">Пройти тест заново :)</a></p>
<?else:?>
<div class="funny-test-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="funny-test-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<h2><?=$arItem["NAME"]?></h2>
		<h3><?=$arItem["ANSWERS"]["NAME"]?></h3>
		<ul>
		<?foreach($arItem["ANSWERS"]["VALUE"] as $k=>$v):?>
			<li>
				<input<?=($arResult["TEST_VALUES"][$arItem["ID"]]===strVal($k)?' checked="checked"':"")?>
					type="radio"
					name="ANSWERS"
					value="<?=$k?>"
					onchange="funny_test_cookie('<?=$arItem["ID"]?>', '<?=$k?>', '<?=preg_match_1('/<b>(\d+)<\/b>/', $arResult["NAV_STRING"])?>', '<?=$arResult["COOKIE"]["PREFIX"]."_".$arResult["COOKIE"]["NAME"]?>', '<?=date("r", $arResult["COOKIE"]["EXPIRES"])?>', '<?=CAjax::GetSession()?CAjax::GetSession():CAjax::GetComponentID('simai:funny.test','template','');?>')"
				 />&nbsp;<?=$v?>
			</li>
		<?endforeach;?>
		</ul>
		<?if($arResult["TEST_FALSE_COUNT"] == 1):?>
			<h3>Выберите последний ответ и <a href="/funny_test.php?clear_cache=Y">просмотрите результат теста</a></h3>
		<?else:?>
			<h3>Выберите ответ:</h3>
		<?endif;?>
		<?
		$s = "";
		foreach($arResult["TEST_VALUES"] as $k=>$v)
			if($arItem["ID"] != $k)
				$s .= $k.":".$v.",";
		?>
		<?/*foreach($arItem["ANSWERS"]["VALUE"] as $k=>$v):?><?=($k+1)?>)&nbsp;&nbsp;<input<?=($arResult["TEST_VALUES"][$arItem["ID"]]===strVal($k)?' checked="checked"':"")?> type="radio" name="ANSWERS" value="<?=$k?>" onchange="funny_test_cookie('<?=$arItem["ID"]?>', '<?=$k?>')">&nbsp;&nbsp;&nbsp;<?endforeach;*/?>
	</div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br/><?=$arResult["NAV_STRING"]?><?endif;?>
</div>
<?endif;?>