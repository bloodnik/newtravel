<?
$MESS ["AUTH_LOGIN_LINK"] = "Login";
$MESS ['AUTH_LOGIN_BUTTON'] = "Autorization";
$MESS ["AUTH_CLOSE_WINDOW"] = "Close";
$MESS ["OUR_SUBSCRIBE"] = "Subscribe";
$MESS ['AUTH_LOGOUT_BUTTON'] = "Logout";
$MESS ['AUTH_REMEMBER_ME'] = "Remember me";
$MESS ['AUTH_FORGOT_PASSWORD_2'] = "Forgot your password?";
$MESS ['AUTH_REGISTER'] = "Register";
$MESS ["AUTH_TITLE"] = "Authorization";
$MESS ["AUTH_LOGIN"] = "Login";
$MESS ["AUTH_PASSWORD"] = "Password";
$MESS ["AUTH_PROFILE"] = "My profile";
$MESS ["AUTH_MP"] = "My portal";
$MESS ["AUTH_WELCOME_TEXT"] = "Hello";
$MESS ["AUTH_REGISTRATION"] = "Registration";
$MESS ["AUTH_NAME"] = "Name";
$MESS ["AUTH_SECOND_NAME"] = "Second name";
$MESS ["AUTH_E_MAIL"] = "E-mail";
$MESS ["AUTH_LOGIN"] = "Login";
$MESS ["PASSWORD"] = "Passsword";
$MESS ["AUTH_CONFIRM_PASSWORD"] = "Confirm password";
$MESS ["AUTH_ENTER_CODE"] = "Enter the code";
$MESS ["AUTH_REGISTER_CLEAR"] = "Clear";
$MESS ["CAPTCHA_REG_CODE"] = "Enter the code";
$MESS ["REGISTER_FIELD_LOGIN_INF"] = "Login at least three characters";
$MESS ["REGISTER_FIELD_PASSWORD"] = "Password must be at least 6 characters long";
?>