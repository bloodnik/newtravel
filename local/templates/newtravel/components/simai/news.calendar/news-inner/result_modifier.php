<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams["FILTER_NAME"] = trim($arParams["FILTER_NAME"]);
$arParams["LIST_URL"] = trim($arParams["LIST_URL"]);
if(strlen($arParams["FILTER_NAME"]) > 0 && strlen($arParams["LIST_URL"]) > 0)
{
	foreach($arResult["MONTH"] as $week => $arWeek)
	{
		foreach($arWeek as $day => $arDay)
		{
			if(count($arDay["events"])>0)
			{
				$timeFROM = mktime(0, 0, 0, $arResult["currentMonth"], $arDay["day"], $arResult["currentYear"]);
				$timeTO   = mktime(0, 0, 0, $arResult["currentMonth"], $arDay["day"]+1, $arResult["currentYear"]);

				$strFROM = date($GLOBALS["DB"]->DateFormatToPHP(CLang::GetDateFormat("SHORT")), $timeFROM);
				$strTO   = date($GLOBALS["DB"]->DateFormatToPHP(CLang::GetDateFormat("SHORT")), $timeTO);

				$LIST_URL = $arParams["LIST_URL"];
				if(strpos($LIST_URL, "?") === false)
					$LIST_URL .= "?";
				if(strpos($LIST_URL, "&") !== false)
					$LIST_URL .= "&";


				$LIST_URL .= URLEncode($arParams["FILTER_NAME"]."[>=".$arParams["DATE_FIELD"]."]")."=".URLEncode($strFROM);
				$LIST_URL .= "&".URLEncode($arParams["FILTER_NAME"]."[<".$arParams["DATE_FIELD"]."]")."=".URLEncode($strTO);

				$arResult["MONTH"][$week][$day]["events"][0]["url"] = htmlspecialchars($LIST_URL);
				$arResult["MONTH"][$week][$day]["events"][0]["title"] = "";
			}
		}
	}
}
if (substr($APPLICATION->GetCurUri(),0,5) != "/eng/") {
$arResult["WEEK_DAYS"][0]["SHORT_ENG"] = "пн";
$arResult["WEEK_DAYS"][1]["SHORT_ENG"] = "вт";
$arResult["WEEK_DAYS"][2]["SHORT_ENG"] = "ср";
$arResult["WEEK_DAYS"][3]["SHORT_ENG"] = "чт";
$arResult["WEEK_DAYS"][4]["SHORT_ENG"] = "пт";
$arResult["WEEK_DAYS"][5]["SHORT_ENG"] = "сб";
$arResult["WEEK_DAYS"][6]["SHORT_ENG"] = "вс";
}
else
{
$arResult["WEEK_DAYS"][0]["SHORT_ENG"] = "mon";
$arResult["WEEK_DAYS"][1]["SHORT_ENG"] = "tue";
$arResult["WEEK_DAYS"][2]["SHORT_ENG"] = "wed";
$arResult["WEEK_DAYS"][3]["SHORT_ENG"] = "thu";
$arResult["WEEK_DAYS"][4]["SHORT_ENG"] = "fri";
$arResult["WEEK_DAYS"][5]["SHORT_ENG"] = "sat";
$arResult["WEEK_DAYS"][6]["SHORT_ENG"] = "sun";
}
$arResult["C_MONTH"] = IntVal($arResult["currentMonth"]);
$arResult["C_YEAR"] = IntVal($arResult["currentYear"]);
$arResult["P_MONTH"] = ($arResult["C_MONTH"] < 2 ? 12 : $arResult["C_MONTH"] - 1);
$arResult["P_YEAR"] = ($arResult["C_MONTH"] < 2 ? $arResult["C_YEAR"]-1 : $arResult["C_YEAR"]);
$arResult["N_MONTH"] = ($arResult["C_MONTH"] > 11 ? 1 : $arResult["C_MONTH"] + 1);
$arResult["N_YEAR"] = ($arResult["C_MONTH"] > 11 ? $arResult["C_YEAR"] + 1 : $arResult["C_YEAR"]);
?>