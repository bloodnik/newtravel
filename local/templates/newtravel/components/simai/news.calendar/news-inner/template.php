<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?//echo'<pre>';print_r($arResult);echo'</pre>';?>
<div class="news-calendar-compact-footer">
<table width="100%"><tr>
<td class="nccf-left"><a href="?year=<?=$arResult["P_YEAR"]?>&month=<?=$arResult["P_MONTH"]?>"><img src="<?=$templateFolder?>/images/null.gif" height="11" width="6" alt="left"></a></td>
<td><?=$arResult["TITLE"]?></td>
<td  class="nccf-right"><a href="?year=<?=$arResult["N_YEAR"]?>&month=<?=$arResult["N_MONTH"]?>"><img src="<?=$templateFolder?>/images/null.gif" height="11" width="6" alt="right"></a></td>
</tr>
</table>
</div>
<div class="news-calendar-compact">
	<table width='100%' class='NewsCalTable'>
	<tr>
	<?foreach($arResult["WEEK_DAYS"] as $WDay):?>
		<td align="center" class='NewsCalHeader'><?=$WDay["SHORT_ENG"]?></td>
	<?endforeach?>
	</tr>
	<?foreach($arResult["MONTH"] as $arWeek):?>
	<tr>
		<?foreach($arWeek as $arDay):?>
		<td class='<?=$arDay["td_class"]?>'>
			<?if(count($arDay["events"])>0):?>
				<a title="<?=$arDay["events"][0]["title"]?>" href="<?=$APPLICATION->GetCurPage()?>?year=<?=$arResult["C_YEAR"]?>&month=<?=$arResult["C_MONTH"]?>&day=<?=$arDay["day"]?>" class="<?=$arDay["day_class"]?>"><?=$arDay["day"]?></a>
			<?else:?>
				<span class="<?=$arDay["day_class"]?>"><?=$arDay["day"]?></span>
			<?endif;?>
		</td>
		<?endforeach?>
	</tr >
	<?endforeach?>
	</table>
</div>