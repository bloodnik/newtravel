<?
$MESS ['SALE_RECORDS_LIST'] = "В список заказов";
$MESS ['SALE_CANCEL_ORDER1'] = "Вы уверены, что хотите аннулировать заказ";
$MESS ['SALE_CANCEL_ORDER2'] = "заказ";
$MESS ['SALE_CANCEL_ORDER3'] = "Внимание: отмена заказа необратима!";
$MESS ['SALE_CANCEL_ORDER4'] = "Укажите, пожалуйста, причину отмены заказа";
$MESS ['SALE_CANCEL_ORDER_BTN'] = "Отменить заказ";
?>