<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();

$this->setFrameMode(true);
?>
<!--Результаты поиска туров-->
<div class="tour-list-wrap help-form-wrap">
	<div class="tour-item">
		<div class="col-md-12">
			<? if ($arResult["isFormErrors"] == "Y"): ?>
				<?=$arResult["FORM_ERRORS_TEXT"];?>
			<? endif; ?>
			
			<?=$arResult["FORM_NOTE"]?>

			<? if ($arResult["isFormNote"] != "Y") : ?>
				<?=$arResult["FORM_HEADER"]?>
				<div class="col-md-12">
					<div class="row">

						<div class="from-group">
							<input type="text" class="form-control" name="form_text_57" value="<?=$request->get('form_text_57')?>" placeholder="Ваше имя" required/>
						</div>

						<div class="from-group">
							<label></label>
							<input type="text" class="form-control"
							       name="form_text_58"
							       pattern="^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))"
							       maxlength='15'
							       placeholder="Телефон 7__________"
							       title="Номер должен содержать только цифры и начинаться с либо с 8, либо с +7"
							       value="<?=$request->get('form_text_58')?>"
							       required />
						</div>

						<div class="from-group">
							<label></label>
							<input type="email" class="form-control email" name="form_text_59" placeholder="Email адрес" value="<?=$request->get('form_text_59')?>" />
						</div>

						<div class="form-group">
							<label></label>
							<p>Бюджет позволит подобрать Вам туры наиболее эффективно</p>
						</div>

						<input type="number" min="0" class="form-control" name="form_text_61" placeholder="Ваш бюджет" value="<?=$request->get('form_text_61')?:'0.00'?>"/>
						<div class="form-group">
							<label></label>
                                <textarea class="form-control" name="form_textarea_60" cols="55" rows="6" style="resize: none;padding-bottom: 21px;" id="help-form-message">
	                                <?=$request->get('form_textarea_60')?>
                                </textarea>
						</div>

						<div class="form-group">
							<input type="checkbox" checked name="confirm-agreement" id="confirm-agreement"><label for='confirm-agreement'>Я согласен с <a href="/about/agreement/" target="_blank">условиями</a> обработки персональных данных</label>
						</div>
					</div>
				</div>


				<div>
					<? if ($arResult["isUseCaptcha"] == "Y") : ?>
						<b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b>
						<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>"/>
						<div class="row">
							<div class="col-md-5">
								<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="120" height="34" />
							</div>
							<div class="col-md-7">
								<input type="text" name="captcha_word" class="form-control"/>
							</div>
						</div>
						<br>
					<? endif; ?>
				</div>

				<div class="col-md-12">
					<div class="row">
						<input type="hidden" name="web_form_apply" value="Y"/>
						<input type="submit" class="btn btn-blue pull-right" value="Отправить запрос"/>
					</div>
				</div>
				<?=$arResult["FORM_FOOTER"]?>
			<? endif; ?>
		</div>
	</div>
</div>
<script>
	$(document).ready(function () {
		//Если email пустой, то заполняем его по умолчанию
		$('.help-form-wrap form').on('submit', function (e) {
            if (!$('#confirm-agreement').prop('checked')) {
                e.preventDefault();
                alert('Подтвердите согласие с условиями обработки персональных данных');
                BX.closeWait();
                return false;
            }
		})
	});
</script>
