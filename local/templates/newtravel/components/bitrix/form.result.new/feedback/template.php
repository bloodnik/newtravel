<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();

$this->setFrameMode(true);

?>
<!--Результаты поиска туров-->
<div class="tour-list-wrap help-form-wrap feedback-form">
	<div class="tour-item">
		<div class="col-md-6 col-sm-12">
			<? if ($arResult["isFormErrors"] == "Y"): ?>
				<?=$arResult["FORM_ERRORS_TEXT"];?>
			<? endif; ?>
			<?=$arResult["FORM_NOTE"]?>

			<? if ($arResult["isFormNote"] != "Y") : ?>
				<?=$arResult["FORM_HEADER"]?>

				<h4 class="modal-title">Ваши отзывы и предложения</h4>

				<div class="col-md-12">
					<div class="row">
						<div class="from-group">
							<input type="text" class="form-control" name="form_text_1" value="<?=$request->get('form_text_1')?>" placeholder="Ваше имя *" required/>
						</div>
					</div>
					<div class="row">
						<div class="from-group">
							<label></label>
							<input type="text" required class="form-control" name="form_text_84" value="<?=$request->get('form_text_84')?>" placeholder="Ваш номер телефона *"/>
						</div>
					</div>
					<div class="row">
						<div class="from-group">
							<label></label>
							<input type="email" class="form-control email" name="form_text_49" value="<?=$request->get('form_text_49')?>" placeholder="Email адрес"/>
						</div>
					</div>
					<div class="row">
						<div class="from-group">
							<label></label>
							<textarea class="form-control" name="form_textarea_2" cols="55" rows="6" style="resize: none;padding-bottom: 21px;" id="help-form-message"><?=$request->get('form_textarea_2')?></textarea>
						</div>
					</div>

					<div class="row">
						<div class="from-group">
							<input type="checkbox" checked name="confirm-agreement" id="confirm-agreement"><label for='confirm-agreement'>Я согласен с <a href="/about/agreement/" target="_blank">условиями</a> обработки персональных данных</label>
						</div>
					</div>

				</div>

				<div class="col-md-7 col-sm-12">
					<? if ($arResult["isUseCaptcha"] == "Y") : ?>
						<b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b>
						<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>"/>
						<div class="row">
							<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="120" height="34"/>
							<input type="text" name="captcha_word" class="form-control"/>
						</div>
						<br>
					<? endif; ?>
				</div>
				<div class="col-md-5 col-sm-12" style="margin-top: 15px;">
					<div class="row">
						<input type="hidden" name="form_text_79" value="<?=isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null?>"/>
						<input type="hidden" name="web_form_apply" value="Y"/>
						<input type="submit" class="btn btn-blue pull-right" value="Отправить"/>
					</div>
				</div>
				<?=$arResult["FORM_FOOTER"]?>
			<? endif; ?>
		</div>
	</div>
</div>
<div class="clearfix"></div>


<script>
    $(document).ready(function () {
        $('.feedback-form form').on('submit', function (e) {
            if(!$('#confirm-agreement').prop('checked')){
                e.preventDefault();
                alert('Подтвердите согласие с условиями обработки персональных данных');
                return false;
            }
        })
    });
</script>