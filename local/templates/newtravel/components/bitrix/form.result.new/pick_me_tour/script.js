'use strict';

window.onload = function () {
    window.pick_form = new Vue({
        el: '#pick_form',
        data: {
            datefrom: moment().add(1, 'days').format('DD.MM.YYYY'),
            dateto: moment().add(15, 'days').format('DD.MM.YYYY'),
            nightsfrom: "9",
            nightsto: "12",

            adults: 2,
            child: 0,
            childage1: 0,
            childage2: 0,
            childage3: 0,

            touristString: "",
            name: "",
            phone: "",
            dateString: moment().add(1, 'days').format('DD.MM.YYYY') + " - " + moment().add(15, 'days').format('DD.MM.YYYY'),
            countryString: "Турция",
            departureString: "Уфы",
            message: "",

            obDepartures: [],
            obCountries: [],

            dateStringHasError: false,
            nameStringHasError: false,
            phoneStringHasError: false,
        },
        created: function () {
            this.init();
        },

        methods: {
            init: function () {
                let self = this;

                setTimeout(function () {
                    $('.daterange').dateRangePicker({
                        format: 'DD.MM.YYYY',
                        startOfWeek: 'monday',
                        singleMonth: 'auto',
                        showShortcuts: false,
                        autoClose: true,
                        selectForward: true,
                        stickyMonths: true,
                        separator: '-',
                        minDays: 1,
                    }).bind('datepicker-change', function (event, obj) {
                        self.datefrom = moment(obj.date1).format("DD.MM.YYYY");
                        self.dateto = moment(obj.date2).format("DD.MM.YYYY");
                        self.dateStringHasError = false;
                        self.dateString = self.datefrom + " - " + self.dateto
                    });

                    //===============================Выбор туристов===============================================//
                    $('.tourists-select').touristSelect({
                        adults: self.adults,
                        child: self.child,
                        childage1: self.childage1,
                        childage2: self.childage2,
                        childage3: self.childage3
                    }).bind('touristsSelect-change', function (event, obj) {
                        self.adults = obj.adults;
                        self.child = obj.child;
                        self.childage1 = obj.childage1;
                        self.childage2 = obj.childage2;
                        self.childage3 = obj.childage3;

                        self.touristString = self.adults + " взр. ";
                        if (self.child > 0) {
                            self.touristString += self.child + " реб.(";
                            for (let i = 1; i <= self.child; i++) {
                                self.touristString += self['childage' + i] + ", ";
                            }
                            self.touristString += ")";
                        }
                    });
                }, 1000);


                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "departure"}}).then(response => {
                        this.obDepartures = response.body.lists.departures.departure;
                        //this.obDepartures = _.sortBy(this.obDepartures, ['name']);
                    }
                );

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "country", cndep: this.departure}}).then(response => {
                    if (response.body.lists.countries.country !== null) {
                        this.obCountries = response.body.lists.countries.country;
                        //this.obCountries = _.sortBy(this.obCountries, ['name']);
                    }

                });
            },

            validateSecondtStep: function () {
                if (this.dateString.length === 0) {
                    this.dateStringHasError = true;
                    return false;
                }
                return true;
            },

            validateLastStep: function () {
                let hasError = false;
                this.nameStringHasError = false;
                this.phoneStringHasError = false;

                if(this.name.length === 0){
                    this.nameStringHasError = true;
                    hasError = true;
                }

                if(this.phone.search(/^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))/) === -1){
                    this.phoneStringHasError = true;
                    hasError = true;
                }
                return !hasError;

            },

            onComplete: function () {
                let form = $('form[NAME=SIMPLE_FORM_4]');

                if (!form.find('#confirm-agreement').prop('checked')) {
                    alert('Подтвердите согласие с условиями обработки персональных данных');
                    return false;
                }

                form.submit();
            }
        },


        filters: {
            displayAge: function (value) {
                if (!value) {
                    return "";
                } else {
                    let titles = ['год', 'года', 'лет'];
                    value = parseInt(value);
                    return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                }
            },

            displayNights: function (value) {
                if (!value) {
                    return "";
                } else {
                    let titles = ['ночь', 'ночи', 'ночей'];
                    value = parseInt(value);
                    return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                }
            },

            displayTourists: function (value) {
                let result = value + " взр. ";
                if (pick_form.child > 0) {
                    result += pick_form.child + " реб.(";
                    for (let i = 1; i <= pick_form.child; i++) {
                        result += pick_form['childage' + i] + ", ";
                    }
                    result += ")";
                }
                return result;
            },


            formatPrice: function (value) {
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
            }

        }
    });
};