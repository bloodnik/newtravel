<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
foreach($arResult["SECTIONS"] as $key=>$arSection) {
    $arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "DETAIL_TEXT", "DETAIL_PICTURE");
    $arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_ID"=> $key);
    $res = CIBlockElement::GetList(Array('SORT' => 'ASC','ID' => 'DESC'), $arFilter, false, Array("nPageSize"=>50), $arSelect);
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $arResult["SECTIONS"][$key]["ITEMS"][] = $arFields;
        $arResult["ITEMS"][] = $arFields;
    }
}

?>