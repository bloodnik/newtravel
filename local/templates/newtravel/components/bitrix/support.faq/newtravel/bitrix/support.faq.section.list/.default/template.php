<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<? //display sections?>
<? //PR($arResult)?>


<div class="faq-page">
	<nav class="col-sm-3" id="myScrollspy">
		<ul class="nav nav-pills nav-stacked" data-spy="affix" data-offset-top="80">
			<? foreach ($arResult['SECTIONS'] as $key => $val): ?>
				<? if ($val["ELEMENT_CNT"] > 0): ?>
					<li class="dropdown"
					    onclick="$('.sub-dropdown').removeClass('active'); $(this).find('.sub-dropdown').addClass('active');">
						<a class="dropdown-toggle title" data-toggle=""
						   href="#<?=$val["ITEMS"][0]["ID"]?>"><?=$val["NAME"]?> <span class="caret"></span></a>
						<ul class="sub-dropdown">
							<? foreach ($val["ITEMS"] as $arItem): ?>
								<li><a href="#<?=$arItem["ID"]?>"
								       class="dropdown-toggle"><?=$arItem["NAME"]?></a></li>
							<? endforeach; ?>
						</ul>
					</li>
				<? endif ?>
			<? endforeach; ?>
		</ul>
	</nav>
	<div class="col-sm-9">
		<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
			<div class="section" id="<?=$arItem["ID"]?>">
				<h2 data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$key?>"><?=$arItem["NAME"]?></h2>
				<div class="content panel-collapse collapse <? if($key==0){echo 'in';}?>" id="collapse<?=$key?>" class="panel-collapse collapse in">
					<? if ( ! empty($arItem["DETAIL_TEXT"])): ?>
						<?=$arItem["DETAIL_TEXT"]?>
					<? else: ?>
						<?=$arItem["PREVIEW_TEXT"]?>
					<? endif ?>
				</div>
			</div>
			<hr>
		<? endforeach ?>
	</div>
</div>

<script>

    $("#myScrollspy ul li a[href^='#']").on('click', function (e) {

        // prevent default anchor click behavior
        e.preventDefault();

        // store hash
        var hash = this.hash;

        // animate
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 300, function () {

            // when done, add hash to url
            // (default click behaviour)
            window.location.hash = hash;
        });

    });
</script>

