<?
$MESS["REVIEWS_TOURIUST"] = "Отзывы туристов";
$MESS["READ_ALL"] = "Читать полностью";
$MESS["TITLE_PLACEMENT"] = "Размещение";
$MESS["TITLE_SERVICE"] = "Сервис";
$MESS["NONE_AUTHOR"] = "Неизвестный";
$MESS["TITLE_KITCHEN"] = "Кухня";
$MESS["TITLE_AUTHOR"] = "Автор";
$MESS["CATALOG_QUANTITY_FROM"] = "От #FROM#";
$MESS["CATALOG_QUANTITY_TO"] = "До #TO#";
$MESS["CT_BCS_QUANTITY"] = "Количество";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
?>