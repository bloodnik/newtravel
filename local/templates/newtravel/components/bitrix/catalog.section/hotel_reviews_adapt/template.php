<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

//Подчет количества
$arFilter = array(
	'IBLOCK_ID'           => $arResult["IBLOCK_ID"],
	"PROPERTY_HOTEL_CODE" => $_REQUEST["id"]// ID инфоблока
	// любые другие параметры, например 'ACTIVE' => 'Y'
);
$res      = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
if ($el = $res->Fetch()) {
	$countItems = $el['CNT'];
}

?>
<div class="row reviews" id='reviews'>
	<div class="col-md-10">
		<h3>Отзывы</h3>
	</div>

	<div class="col-md-2">
		<? if ( ! $USER->IsAuthorized()): ?>
			Авторизуйтесь чтобы добавить отзыв!
		<? else: ?>
			<a href="#add_review" data-toggle="modal" class="btn btn-primary">Добавить отзыв!</a>
		<? endif ?>
	</div>


	<? if ($countItems > 0): ?>
		<? foreach ($arResult["ITEMS"] as $i => $arItem): ?>
			<? if ( ! empty($arItem["PREVIEW_TEXT"])): ?>
				<!-- Отзыв умного туриста-->
				<div class="col-md-12 review smart">
					<div class="col-md-2 user-preview">
						<?
						$rsUser        = CUser::GetByID($arItem["DISPLAY_PROPERTIES"]["USER"]["VALUE"]);
						$arUser        = $rsUser->Fetch();
						$personalPhoto = CFile::GetPath($arUser["PERSONAL_PHOTO"]);
						?>
						<? if (isset($personalPhoto)): ?>
							<img class="user-pic" src="<?=$personalPhoto?>">
						<? else: ?>
							<img class="user-pic" src="<?=SITE_TEMPLATE_PATH?>/images/no-review_avatar.gif">
						<? endif; ?>

						<a href="#" class="username"><?=(is_array($arItem["DISPLAY_PROPERTIES"]["AUTHOR"]) ? $arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["VALUE"] : getMessage("NONE_AUTHOR"))?></a>
						<!-- 		                <p class="user-position">Старший менеджер по работке с клиентами</p>
												<img class="utlogo" src="images/newtravel/utlogo_min.gif"> -->
					</div>
					<div class="col-md-10 user-review">
						<div class="col-md-8">
							<!-- <h3>Отзыв умного туриста</h3> -->
						</div>
						<div class="col-md-4">
							<span><? if (is_array($arItem["DISPLAY_PROPERTIES"]["RATING_PLACE"])): ?><?=getMessage("TITLE_PLACEMENT")?>: <?=$arItem["DISPLAY_PROPERTIES"]["RATING_PLACE"]["VALUE"]?><? endif ?></span>
							<span><? if (is_array($arItem["DISPLAY_PROPERTIES"]["RATING_SERVICE"])): ?><?=getMessage("TITLE_SERVICE")?>: <?=$arItem["DISPLAY_PROPERTIES"]["RATING_SERVICE"]["VALUE"]?><? endif ?></span>
							<span><? if (is_array($arItem["DISPLAY_PROPERTIES"]["RATING_KITCHEN"])): ?><?=getMessage("TITLE_KITCHEN")?>: <?=$arItem["DISPLAY_PROPERTIES"]["RATING_KITCHEN"]["VALUE"]?><? endif ?></span>
						</div>
						<div class="col-md-12 text">
							<? //PR($arItem);?>
							<?=$arItem["PREVIEW_TEXT"]?>
						</div>
					</div>
				</div><!-- /Отзыв умного туриста-->
			<? endif; ?>
		<? endforeach ?>
	<? else: ?>
		Отзывов нет
	<? endif; ?>
</div>

<?=$arResult["NAV_STRING"]?>


<script>
	<?if($countItems > 0):?>
	$(".review-count").html("<?=$countItems?>");
	<?endif;?>
</script>


<? if ($USER->IsAuthorized()): ?>
	<div class="modal fade" id="add_review" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="container">
			<div class="row">
				<div class="modal-dialog modal-add-tourist">
					<div class="modal-content">
						<? $APPLICATION->IncludeComponent(
							"bitrix:iblock.element.add.form",
							"adapt_add_review",
							Array(
								"AJAX_MODE"                     => "N",
								"SEF_MODE"                      => "N",
								"IBLOCK_TYPE"                   => "simai",
								"IBLOCK_ID"                     => "57",
								"PROPERTY_CODES"                => array("752", "754", "755", "756", "757", "758", "759", "NAME", "PREVIEW_TEXT"),
								"PROPERTY_CODES_REQUIRED"       => array("NAME"),
								"GROUPS"                        => array("3"),
								"STATUS_NEW"                    => "N",
								"STATUS"                        => "ANY",
								"LIST_URL"                      => "",
								"ELEMENT_ASSOC"                 => "CREATED_BY",
								"MAX_USER_ENTRIES"              => "100000",
								"MAX_LEVELS"                    => "100000",
								"LEVEL_LAST"                    => "Y",
								"USE_CAPTCHA"                   => "N",
								"USER_MESSAGE_EDIT"             => "Отзыв сохранен успешно",
								"USER_MESSAGE_ADD"              => "Отзыв добавлен успешно",
								"DEFAULT_INPUT_SIZE"            => "30",
								"RESIZE_IMAGES"                 => "N",
								"MAX_FILE_SIZE"                 => "0",
								"PREVIEW_TEXT_USE_HTML_EDITOR"  => "N",
								"DETAIL_TEXT_USE_HTML_EDITOR"   => "N",
								"CUSTOM_TITLE_NAME"             => "",
								"CUSTOM_TITLE_TAGS"             => "",
								"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
								"CUSTOM_TITLE_DATE_ACTIVE_TO"   => "",
								"CUSTOM_TITLE_IBLOCK_SECTION"   => "",
								"CUSTOM_TITLE_PREVIEW_TEXT"     => "Описание",
								"CUSTOM_TITLE_PREVIEW_PICTURE"  => "",
								"CUSTOM_TITLE_DETAIL_TEXT"      => "",
								"CUSTOM_TITLE_DETAIL_PICTURE"   => ""
							)
						); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<? endif ?>
