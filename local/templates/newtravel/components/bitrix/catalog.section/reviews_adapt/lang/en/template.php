<?
$MESS["REVIEWS_TOURIUST"] = "Tourist reviews";
$MESS["READ_ALL"] = "Read all";
$MESS["TITLE_PLACEMENT"] = "Placement";
$MESS["TITLE_SERVICE"] = "Service";
$MESS["NONE_AUTHOR"] = "Unknown";
$MESS["TITLE_KITCHEN"] = "Kitchen";
$MESS["TITLE_AUTHOR"] = "Author";
$MESS["CATALOG_QUANTITY_FROM"] = "#FROM# and more";
$MESS["CATALOG_QUANTITY_TO"] = "up to #TO#";
$MESS["CT_BCS_QUANTITY"] = "Quantity";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
?>