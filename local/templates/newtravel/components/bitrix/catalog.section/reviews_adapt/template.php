<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$countItems=count($arResult["ITEMS"])-1;
if($countItems<0)return;
?>
<section class="white-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-widget">
                    <div class="text-widget-title">
                        <h3>Отзывы от Умных Туристов</h3>
                    </div>
                    <div id="testimonial-flexslider" class="flexslider testimonial-flexslider">
                        <ul class="slides">
                        <?foreach($arResult["ITEMS"] as $i=>$arItem):?>
                            <?
                                //Получаем свойства пользователя по ID
                                CModule::IncludeModule("user");
                                $rsUser = CUser::GetByID($arItem["PROPERTIES"]["SITE_REVIEW_USER"]["VALUE"]);
                                $arUser = $rsUser->Fetch();
                            ?>
                            <li>
                                <div class="testimonial">
                                    <div class="author">
                                        <img src="<?=CFile::GetPath($arUser["PERSONAL_PHOTO"])?>" alt="<?=$arItem["NAME"]?>">
                                    </div>

                                    <blockquote>
                                        <q><?=truncateText(strip_tags($arItem["PREVIEW_TEXT"]),400);?></q>
                                    </blockquote>

                                    <div class="author-detail"><?=$arItem["NAME"]?>, <span><?=$arUser["PERSONAL_PROFESSION"]?></span></div>
                                </div>
                            </li>
                        <?endforeach;?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>