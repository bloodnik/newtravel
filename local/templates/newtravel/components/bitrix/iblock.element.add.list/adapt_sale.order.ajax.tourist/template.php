<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>


<script>
//Функция выбора туристов
// self - Элемент
// ID - ID туриста
// add - флаг добавления/удаления (tru/false)
function addTouristList(self, touristNum, ID, add) {
	//добавлеяем ID выбранных туристов

    //Выбираем ID свойства заказа
    var orderPropId;

    switch (touristNum) {
        case 1:
            orderPropId = 45;
            break;
        case 2:
            orderPropId = 46;
            break;
        case 3:
            orderPropId = 47;
            break;
        case 4:
            orderPropId = 48;
            break;
        case 5:
            orderPropId = 49;
            break;
        default:
            orderPropId = 0;
            break;
    }


	var prop=$('#ORDER_PROP_33,#ORDER_PROP_39');
	var str = prop.text ();
	if(add==true) {
		$("#"+ID+" .cancel" ).css("display", "block");
		$(self).css("background", "#c4c4c4").html("Взят!").attr("checked", "true");

		prop.text(str+','+ID);
	} else {
		$("#"+ID+" .cancel" ).css("display", "none");
		$("#"+ID+" .add" ).css("background", "#428BCA").html("Взять с собой!").removeAttr("checked");
		prop.text(str.replace(','+ID, ''));
	}


    var adult = '';

    if($('#PASSPORT_ADULT_' + ID).html() !== undefined) {
        adult += $('#PASSPORT_ADULT_' + ID).html();
    }
    if(add==true) {
        $('#ORDER_PROP_' + orderPropId).text(adult);
    }else{
        $('#ORDER_PROP_' + orderPropId).text("");
    }


//    $('.add[checked]').each(function(index) {
//        var chekedId = this.id;
//        if($('#PASSPORT_ADULT_' + chekedId).html() !== undefined)
//            adult += $('#PASSPORT_ADULT_' + chekedId).html();
//    });
//    $('#ORDER_PROP_40').text(adult);

}
</script>


<section class='tourist-section order-header-section'>

	<div class="container">
		<div class="row tab-section profile-select">
			
			<!-- Уведомления -->
			<?if (strlen($arResult["MESSAGE"]) > 0):?>
				<?=ShowNote($arResult["MESSAGE"])?>
			<?endif?><!-- /Уведомления -->

			<?
			foreach($arResult["ADULT"] as $id => $text){
				echo '<div style="display:none;" id="PASSPORT_ADULT_'.$id.'">'.$text.'</div>';
			}
			?>


		    <div class="tab-buttons">
		        <ul class="nav">
		            <li class="active first"><a href="#current-tourists">Использование туристов введенных ранее</a></li>
		            <li class='last'><a data-toggle="modal" id="add_new" href="#addForm">Добавить новых туристов</a></li>
		        </ul>
		    </div>

			<div id="" class="tab-content">

	            <div class="tab-pane active" id="current-tourists">

	            	<div class="col-md-5"><strong>Ранее использованные данные туристов:</strong></div>
	            	<div class="col-md-7"><p>Вы можете просто выбрать ранее использованные при заказах данные туристов, нажав "Взять с собой".</p></div>

	            	<div class="col-md-12">
	            		На всякий случай проверьте правильность введенных данных!
	            	</div>
					<?if($arResult["NO_USER"] == "N"):?>
						<?if (count($arResult["ELEMENTS"]) > 0):?>	
							<?$count = 1?>
							<?foreach ($arResult["ELEMENTS"] as $arElement):?>

								<?if($arElement["ACTIVE"] == "Y"):?>
									<div class="col-md-4 tourists_list" id="<?=$arElement["ID"]?>">			
										
										<?
									    $res = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"]);
										while ($ob = $res->GetNext()){												
										        $VALUES[$ob['CODE']] = $ob['VALUE'];
										}?>																						
                                        <?if (strlen($VALUES["INFANT"]) > 0){
                                            $blockHeader = "Ребенок";
                                        }else{
                                            $blockHeader = "Турист";
                                        }
                                        ?>

										<div class="block-header">
											<p>Выбрать - <?=$blockHeader?> №<?=$count?></p>
										</div>										
										<div class="block-body">
											<div class="col-md-12">
												<label for="tourist-sname">Фамилия</label>
												<input type="text" class='' id='tourist-sname' value="<?=$VALUES['ZG_SURNAME']?>" disabled>
											</div>
											<div class="col-md-12">
												<label for="tourist-name">Имя</label>
												<input type="text" class='' id='tourist-name' value="<?=$VALUES['ZG_NAME']?>" disabled>
											</div>
											<div class="col-md-12">
												<label for="tourist-bday">Дата рождения</label>
												<input type="text" class='' id='tourist-bday' value="<?=$VALUES['BIRTH_DATE']?>" disabled>
											</div>
											<div class="col-md-12">
												<label for="tourist-ser">Серия</label>
												<input type="text" class='' id='tourist-ser' value="<?=$VALUES['ZG_SERIES']?>" disabled>
											</div>
											<div class="col-md-12">
												<label for="tourist-nom">Номер</label>
												<input type="text" class='' id='tourist-nom' value="<?=$VALUES['ZG_NUMBER']?>" disabled>
											</div>
											<div class="col-md-12">
												<label for="tourist-expdate">Действителен до</label>
												<input type="text" class='' id='tourist-expdate' value="<?=$VALUES['ZG_VALID_TO']?>" disabled>
											</div>
											<div class="col-md-12">	
												<label for="tourist-vidan">Кем выдан</label>
												<input type="text" class='' id='tourist-vidan' value="<?=$VALUES['ZG_ISSUED']?>" disabled>	
											</div>			

											<div class="col-md-6">
												<div class="btn btn-warning cancel" id="<?=$arElement["ID"]?>" onclick="addTouristList(this, <?=$count?>, <?=$arElement["ID"]?>, false)" style="display:none">
												Убрать</div>
											</div>	

											<div class="col-md-6">
												<div class="btn btn-primary add" id="<?=$arElement["ID"]?>" onclick="addTouristList(this, <?=$count?>, <?=$arElement["ID"]?>, true)">
													Взять с собой
												</div>			
											</div>
										</div>											                   
					                </div>
		                		<?endif?>
		                		<?$count++?>
		                	<?endforeach?>
		                <?endif;?>	 	   
		            <?endif;?>             		                              
				</div>
			</div>
		</div>		    
	</div>
    <a id="list-refresh" style="display:none;" rel="nofollow" href="<?=$APPLICATION->GetCurPage()?>">Обновить</a>
</section>

<?if (strlen($arResult["NAV_STRING"]) > 0):?><?=$arResult["NAV_STRING"]?><?endif?>	

