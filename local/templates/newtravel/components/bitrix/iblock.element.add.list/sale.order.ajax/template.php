<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>"; print_r($arResult); echo "</pre>";
$colspan = 2;
if ($arResult["CAN_EDIT"] == "Y") $colspan++;
if ($arResult["CAN_DELETE"] == "Y") $colspan++;
?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<script>
function addTouristList(self) {
	var prop=$('#ORDER_PROP_33,#ORDER_PROP_39');
	var str = prop.val();
	if($(self).is(':checked')) {
		prop.val(str+','+$(self).val());
	} else {
		prop.val(str.replace(','+$(self).val(), ''));
	}

			var adult = '';
			var child = '';

			$('input:checked.tourist_list_id').each(function(index) {
			    //alert(index + ': ' + $(this).val());
			    if($('#PASSPORT_CHILD_' + $(this).val()).html() !== undefined)
				    child += $('#PASSPORT_CHILD_' + $(this).val()).html();
				if($('#PASSPORT_ADULT_' + $(this).val()).html() !== undefined)
				    adult += $('#PASSPORT_ADULT_' + $(this).val()).html();
			});


			$('#ORDER_PROP_40').val(adult);
			$('#ORDER_PROP_41').val(child);

}
</script>
<?
foreach($arResult["CHILD"] as $id => $text) {
	echo '<div style="display:none;" id="PASSPORT_CHILD_'.$id.'">'.$text.'</div>';
}
foreach($arResult["ADULT"] as $id => $text){
	echo '<div style="display:none;" id="PASSPORT_ADULT_'.$id.'">'.$text.'</div>';
}
?>



<div class="left-col">
              <div class="tour-list" style="margin-top: 32px;">
			   <div class="tour-list-in">
			   <div class="detail-name"><a style="padding:5px 0px 0 15px;" class="tname"><?=getMessage("IBLOCK_LIST_CANT_PASPORT");?></a></div>
<?if($arResult["NO_USER"] == "N"):?>
 <div class="row">
	<div class="row-title"><?=GetMessage("IBLOCK_ADD_LIST_TITLE")?>:</div>

	<?if (count($arResult["ELEMENTS"]) > 0):?>
		<?foreach ($arResult["ELEMENTS"] as $arElement):?>
			<?if($arElement["ACTIVE"] == "Y"):?>
				<div class="r-list-tourist">
				<input class="tourist_list_id" id="row_id_<?=$arElement["ID"]?>" type="checkbox" name="TOURIST_ID" value="<?=$arElement["ID"]?>" onchange="addTouristList(this);"<?=(in_array($arElement["ID"], explode(',', trim($GLOBALS["ORDER_PROP_33"], ',')))?' checked="checked"':'')?> />
				<label for="row_id_<?=$arElement["ID"]?>"><?=$arElement["NAME"]?></label>
				<?if ($arResult["CAN_EDIT"] == "Y"):?>
				<?if ($arElement["CAN_EDIT"] == "Y"):?><a target="_blank" href="<?=$arParams["EDIT_URL"]?>?edit=Y&amp;CODE=<?=$arElement["ID"]?>"><?=GetMessage("IBLOCK_ADD_LIST_EDIT")?><?else:?>&nbsp;<?endif?></a>
				<?endif?>
				<?if ($arResult["CAN_DELETE"] == "Y"):?>
				<?if ($arElement["CAN_DELETE"] == "Y"):?>
				<a href="?delete=Y&amp;CODE=<?=$arElement["ID"]?>&amp;<?=bitrix_sessid_get()?>" onClick="if(confirm('<?echo CUtil::JSEscape(str_replace("#ELEMENT_NAME#", $arElement["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM")))?>')){$.get('?delete=Y&amp;CODE=<?=$arElement["ID"]?>&amp;<?=bitrix_sessid_get()?>');$(this).parent().parent().hide();return false;} else return false;"><?=GetMessage("IBLOCK_ADD_LIST_DELETE")?></a><?else:?>&nbsp;<?endif?>
				<?endif?>
				</div>
			<?endif?>
		<?endforeach?>
	<?endif?>
	<input type="button" onclick="$('#tourist_add').slideToggle();" value="Добавить туриста" />
</div>
<?endif?>
</div>
</div>
</div>
<?if (strlen($arResult["NAV_STRING"]) > 0):?><?=$arResult["NAV_STRING"]?><?endif?>