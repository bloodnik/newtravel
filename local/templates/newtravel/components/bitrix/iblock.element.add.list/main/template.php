<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<div class="tl-h1"><h1><?=GetMessage("IBLOCK_ADD_LIST_TITLE")?></h1></div>
<div class="tourist-list">
<div class="tourist-list-wrap">
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<?if($arResult["NO_USER"] == "N"):?>
	<?if (count($arResult["ELEMENTS"]) > 0):?>
		<?foreach ($arResult["ELEMENTS"] as $arElement):?>
			<?if($arElement["ACTIVE"] == "Y"):?>
			<div class="tlw-element">
				<span class="tlw-name"><?=$arElement["NAME"]?></span>
				<?if ($arResult["CAN_EDIT"] == "Y"):?>
				<?if ($arElement["CAN_EDIT"] == "Y"):?><a href="<?=$arParams["EDIT_URL"]?>?edit=Y&amp;CODE=<?=$arElement["ID"]?>"><?=GetMessage("IBLOCK_ADD_LIST_EDIT")?></a><?else:?>&nbsp;<?endif?>
				<?endif?>
				<?if ($arResult["CAN_DELETE"] == "Y"):?>
				<?if ($arElement["CAN_DELETE"] == "Y"):?><a href="?delete=Y&amp;CODE=<?=$arElement["ID"]?>&amp;<?=bitrix_sessid_get()?>" onClick="if(confirm('<?echo CUtil::JSEscape(str_replace("#ELEMENT_NAME#", $arElement["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM")))?>')){$.get('?delete=Y&amp;CODE=<?=$arElement["ID"]?>&amp;<?=bitrix_sessid_get()?>');$(this).parent().parent().hide();return false;} else return false;"><?=GetMessage("IBLOCK_ADD_LIST_DELETE")?></a><?else:?>&nbsp;<?endif?>
				<?endif?>
			</div>
			<?endif?>
		<?endforeach?>
	<?else:?>
			<span id="tourist_list_empty"><?=GetMessage("IBLOCK_ADD_LIST_EMPTY")?></span>
	<?endif?>
		<div class="tlw-add"><a href="javascript:void(0);" onclick="$('#tourist_add').slideToggle();">Добавить</a></div>
<?endif?>
</div>
</div>
<?if(strlen($arResult["NAV_STRING"]) > 0):?><?=$arResult["NAV_STRING"]?><?endif?>