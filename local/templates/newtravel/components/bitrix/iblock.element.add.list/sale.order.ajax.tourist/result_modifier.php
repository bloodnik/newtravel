<?
$arResult["ADULT"] = array();
foreach ($arResult["ELEMENTS"] as $cell=>$arElement) {

	$prop = array();
	$db_props = CIBlockElement::GetProperty(1, $arElement["ID"], array("sort" => "asc"), Array());
	while($ar_props = $db_props->Fetch()) {
		$prop[$ar_props["CODE"]] = $ar_props["VALUE"];
	}

	$text = "";
	$text .= "Имя: ".$prop["ZG_NAME"]."\r\n";
	$text .= "Фамилия: ".$prop["ZG_SURNAME"]."\r\n";
	$text .= "Дата рождения: ".$prop["BIRTH_DATE"]."\r\n";
	$text .= "Загранпаспорт серия: ".$prop["ZG_SERIES"]."\r\n";
	$text .= "Загранпаспорт номер: ".$prop["ZG_NUMBER"]."\r\n";
	$text .= "Загранпаспорт дата выдачи: ".$prop["ZG_ISSUE_DATE"]."\r\n";
	$text .= "Загранпаспорт кем выдан: ".$prop["ZG_ISSUED"]."\r\n";
	$text .= "Загранпаспорт срок действия: ".$prop["ZG_VALID_TO"]."\r\n\r\n";

	// Различаем взрослых и детей
	if($prop["PREFIX"] == 59) {
		unset($arResult["ELEMENTS"][$cell]);
	}
	else {
		$arResult["ADULT"][$arElement["ID"]] = $text;
	}
}
?>