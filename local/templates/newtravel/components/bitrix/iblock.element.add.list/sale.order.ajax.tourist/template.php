<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<script>
function addTouristList(self) {
	var prop=$('#ORDER_PROP_33,#ORDER_PROP_39');
	var str = prop.val();
	if($(self).is(':checked')) {
		prop.val(str+','+$(self).val());
	} else {
		prop.val(str.replace(','+$(self).val(), ''));
	}

			var adult = '';
			var child = '';

			$('input:checked.tourist_list_id').each(function(index) {
				if($('#PASSPORT_ADULT_' + $(this).val()).html() !== undefined)
				    adult += $('#PASSPORT_ADULT_' + $(this).val()).html();
			});
			$('#ORDER_PROP_40').val(adult);
}
</script>
<?
foreach($arResult["ADULT"] as $id => $text){
	echo '<div style="display:none;" id="PASSPORT_ADULT_'.$id.'">'.$text.'</div>';
}
if($arResult["NO_USER"] == "N"):?>
<div class="jClever">
 <div class="row">
	<div class="row-title"><?=GetMessage("IBLOCK_ADD_LIST_TITLE")?>:</div>

	<?if (count($arResult["ELEMENTS"]) > 0):?>
		<?foreach ($arResult["ELEMENTS"] as $arElement):?>
			<?if($arElement["ACTIVE"] == "Y"):?>
				<div class="r-list-tourist">
				<input class="tourist_list_id" id="row_id_<?=$arElement["ID"]?>" type="checkbox" name="TOURIST_ID" value="<?=$arElement["ID"]?>" onchange="addTouristList(this);" />
				<label for="row_id_<?=$arElement["ID"]?>"><?=$arElement["NAME"]?></label>
				<?if ($arResult["CAN_EDIT"] == "Y"):?>
				<?if ($arElement["CAN_EDIT"] == "Y"):?><a target="_blank" href="<?=$arParams["EDIT_URL"]?>?edit=Y&amp;CODE=<?=$arElement["ID"]?>"><?=GetMessage("IBLOCK_ADD_LIST_EDIT")?><?else:?>&nbsp;<?endif?></a>
				<?endif?>
				<?if ($arResult["CAN_DELETE"] == "Y"):?>
				<?if ($arElement["CAN_DELETE"] == "Y"):?>
				<a href="?delete=Y&amp;CODE=<?=$arElement["ID"]?>&amp;<?=bitrix_sessid_get()?>" onClick="if(confirm('<?echo CUtil::JSEscape(str_replace("#ELEMENT_NAME#", $arElement["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM")))?>')){$.get('?delete=Y&amp;CODE=<?=$arElement["ID"]?>&amp;<?=bitrix_sessid_get()?>',function(){$(this).parent().remove();});return false;} else return false; $('#tourist-refresh').click()"><?=GetMessage("IBLOCK_ADD_LIST_DELETE")?></a><?else:?>&nbsp;<?endif?>
				<?endif?>
				</div>
			<?endif?>
		<?endforeach?>
	<?endif?>
</div>
</div>
<?endif?>

<?if (strlen($arResult["NAV_STRING"]) > 0):?><?=$arResult["NAV_STRING"]?><?endif?>	

