<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<script>
function addChildList(self) {

	//Стоимость от количества детей 02.07.2014
	var count = 0;
	var checkedCount = $(".infant:checked"); //Выборка checked элементов 
	count = checkedCount.length; // Количество выборки
	//alert(count);
	$.cookie('COUNT_OF_CHILD', count); //Устанавливаем куки с количеством детей
		
	$(".doplata_child").html(" и " + count + " инфанта"); //Выводим информацию
	
	
	var old_doplata_price = $("#old_doplata_price").text(); //Текущая стоимсть доплаты RUB
	var old_doplata_price_usd = $("#old_doplata_price_usd").text(); //Текущая стоимсть доплаты USD 
	var doplata_price = $("#tax_price_hidden").text(); // Стоимость топливного сбора RUB
	var doplata_price_usd = $("#tax_price_usd_hidden").text(); // Стоимость топливного сбора USD
	var order_price = $("#price_without_margin").text(); //Стоимость без наценки банка
	
	if (count==0) {
		var new_doplata_price = parseInt(old_doplata_price); //Если установлено детей 0 отнимаем стоимость топливного сбора
		var new_doplata_price_usd = parseInt(old_doplata_price_usd); //Если установлено детей 0 отнимаем стоимость топливного сбора USD
		var new_order_price = parseInt(order_price) - (parseInt(doplata_price) * count) ; // Если установлено детей 0 отнимаем стоимость топливного сбора
		if ($("#ID_PAY_SYSTEM_ID_11").prop("checked")) {
			new_order_price = new_order_price + (new_order_price/100*2);
		}
	}
	else {
		var new_doplata_price = parseInt(old_doplata_price) + (parseInt(doplata_price) * count); //Увеличиваем доплату на количество детей
		var new_doplata_price_usd = parseInt(old_doplata_price_usd) + (parseInt(doplata_price_usd) * count); //Увеличиваем доплату на количество детей
		var new_order_price = parseInt(order_price) + (parseInt(doplata_price) * count) ; //Увеличиваем стоимость заказа на количество детей
		if ($("#ID_PAY_SYSTEM_ID_11").prop("checked")) {
			new_order_price = new_order_price + (new_order_price/100*2);
		}		
	}
	
	$(".doplata_price").empty();
	$.cookie('ORDER_PRICE', parseInt(new_order_price)); //Устанавливаем куки с ценой
	$(".doplata_price").html(new_doplata_price); //Выводим новую стоимость доплаты RUB
	$(".doplata_price_usd").html(new_doplata_price_usd); //Выводим новую стоимость доплаты USD
	$(".order_price").empty();
	$(".order_price").html(Math.round(new_order_price))
	//
	//
	
	var prop=$('#ORDER_PROP_33,#ORDER_PROP_39');
	var str = prop.val();
	if($(self).is(':checked')) {
		prop.val(str+','+$(self).val());
	} else {
		prop.val(str.replace(','+$(self).val(), ''));
	}

			var child = '';

			$('input:checked.tourist_list_id').each(function(index) {
			    //alert(index + ': ' + $(this).val());
			    if($('#PASSPORT_CHILD_' + $(this).val()).html() !== undefined)
				    child += $('#PASSPORT_CHILD_' + $(this).val()).html();
			});
			$('#ORDER_PROP_41').val(child);
}
</script>
<?
foreach($arResult["CHILD"] as $id => $text) {
	echo '<div style="display:none;" id="PASSPORT_CHILD_'.$id.'">'.$text.'</div>';
}
if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<?if($arResult["NO_USER"] == "N"):?>
<div class="jClever">
 <div class="row">
	<div class="row-title"><?=GetMessage("IBLOCK_ADD_LIST_TITLE")?>:</div>

	<?if (count($arResult["ELEMENTS"]) > 0):?>
		<?foreach ($arResult["ELEMENTS"] as $arElement):?>
			<?if($arElement["ACTIVE"] == "Y"):?>
				<div class="r-list-tourist">
				<input class="tourist_list_id infant" id="row_id_<?=$arElement["ID"]?>" type="checkbox" name="TOURIST_ID" value="<?=$arElement["ID"]?>" onchange="addChildList(this);"/>
				<label for="row_id_<?=$arElement["ID"]?>"><?=$arElement["NAME"]?></label>
				<?if ($arResult["CAN_EDIT"] == "Y"):?>
				<?if ($arElement["CAN_EDIT"] == "Y"):?><a target="_blank" href="<?=$arParams["EDIT_URL"]?>?edit=Y&amp;CODE=<?=$arElement["ID"]?>"><?=GetMessage("IBLOCK_ADD_LIST_EDIT")?><?else:?>&nbsp;<?endif?></a>
				<?endif?>
				<?if ($arResult["CAN_DELETE"] == "Y"):?>
				<?if ($arElement["CAN_DELETE"] == "Y"):?>
				<a href="?delete=Y&amp;CODE=<?=$arElement["ID"]?>&amp;<?=bitrix_sessid_get()?>" onClick="if(confirm('<?echo CUtil::JSEscape(str_replace("#ELEMENT_NAME#", $arElement["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM")))?>')){$.get('?delete=Y&amp;CODE=<?=$arElement["ID"]?>&amp;<?=bitrix_sessid_get()?>',function(){$(this).parent().remove();});return false;} else return false;"><?=GetMessage("IBLOCK_ADD_LIST_DELETE")?></a><?else:?>&nbsp;<?endif?>
				<?endif?>
				</div>
			<?endif?>
		<?endforeach?>
	<?endif?>
</div>
</div>
<?endif?>
<?if (strlen($arResult["NAV_STRING"]) > 0):?><?=$arResult["NAV_STRING"]?><?endif?>