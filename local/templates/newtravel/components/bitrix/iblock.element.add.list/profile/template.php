<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if (count($arResult["ELEMENTS"]) > 0):?>
	<ul class="myinfo">
		<?foreach ($arResult["ELEMENTS"] as $arElement):?>
				<li><a href="/personal/tourists/"><?=$arElement["NAME"]?></a></li>
		<?endforeach?>
    </ul>
                 <div class="tss">* — <?=getMessage("IBLOCK_LIST_WARNING");?></div>
<?else:?>
<?=getMessage("IBLOCK_ADD_LIST_EMPTY")?>
<?endif?>