<?
$MESS ['IBLOCK_ADD_LIST_TITLE'] = "Список туристов (отметить тех, кто летит в тур)";
$MESS ['IBLOCK_ADD_LIST_EMPTY'] = "Список туристов пуст";
$MESS ['IBLOCK_ADD_LIST_EDIT'] = "редактировать";
$MESS ['IBLOCK_ADD_LIST_DELETE'] = "удалить";
$MESS ['IBLOCK_ADD_LIST_DELETE_CONFIRM'] = "Вы действительно хотите удалить профиль: #ELEMENT_NAME#?";
$MESS ['IBLOCK_ADD_LINK_TITLE'] = "Добавить";
$MESS ['IBLOCK_LIST_WARNING'] = "Нажмите для редактирования списка туристов";
?>