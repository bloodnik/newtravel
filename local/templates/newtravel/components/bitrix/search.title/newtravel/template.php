<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    <!-- gn-search-item -->
    <div class="dropdown-search">
        <form role="form" class="searchform" method="get" action="<?echo $arResult["FORM_ACTION"]?>">
            <input type="search" class="form-control"  name="q" placeholder="Поиск..." autocomplete="off">
            <button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
