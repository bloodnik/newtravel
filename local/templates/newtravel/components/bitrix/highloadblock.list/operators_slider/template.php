<?

if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

if ( ! empty($arResult['ERROR'])) {
	echo $arResult['ERROR'];

	return false;
}
$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/js/highloadblock/css/highloadblock.css');
$this->addExternalCss($this->GetFolder() . "/js/assets/owl.carousel.min.css");
$this->addExternalCss($this->GetFolder() . "/js/assets/owl.theme.default.min.css");
$this->addExternalJs($this->GetFolder() . "/js/owl.carousel.min.js");
?>

<style>
	.operators-slider__wrap {
		background: #fff;
		padding: 30px 0;
		border-bottom: 1px solid #e6e6f7;
	}

	.owl-carousel .owl-video-wrapper {
		position: relative;
		height: 100%;
		background: #000;
	}

	.owl-carousel .owl-item img {
		background: transparent;
	}


</style>

<div class="operators-slider__wrap">
	<div class="owl-carousel owl-theme operators-slider">
		<? foreach ($arResult["rows"] as $arItem): ?>
			<?=$arItem['UF_LOGO']?>
		<? endforeach; ?>
	</div>
</div>


<script>
    $(document).ready(function () {
        setTimeout(function () {
            $(".operators-slider").owlCarousel({
                autoplay: true,
                autoplayTimeout: 5000,
                slideSpeed: 1000,
                autoWidth: true,
                paginationSpeed: 400,
                items: 6,
                dots: false,
                loop: true,
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 3,
                    nav: false
                },
                1000: {
                    items: 5,
                    nav: true,
                    loop: false
                },
                1600: {
                    items: 6,
                    nav: true,
                    loop: false
                },

            });
        }, 1000)

    });
</script>