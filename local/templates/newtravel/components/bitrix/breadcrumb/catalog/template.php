<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string
if(empty($arResult))
	return "";
	
$curPage = $GLOBALS['APPLICATION']->GetCurPage($get_index_page=false);

if ($curPage != SITE_DIR)
{
	if (empty($arResult) || $curPage != $arResult[count($arResult)-1]['LINK'])
		$arResult[] = array('TITLE' =>  htmlspecialcharsback($GLOBALS['APPLICATION']->GetTitle(false, true)), 'LINK' => $curPage);
}
$strReturn = '<div class="breadcrumbs"><ul>';
	
		
for($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++)
{
if($index > 0)
		$strReturn .= '<li class="separator"></li>';
if($index==0){$class="home";}else{$class="b-link";}
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	if($arResult[$index]["LINK"] <> "" && $index<(count($arResult)-1))
	 	$strReturn .= '<li class="'.$class.'"  itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="url"><span itemprop="title">'.$title.'</span></a></li>';
	else
		$strReturn .= '<li class="b-link link-active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="javascript:void(0);" itemprop="url"><span itemprop="title">'.$title.'</span></a></li>';
}

$strReturn .= '</ul></div>';
return $strReturn;
?>
