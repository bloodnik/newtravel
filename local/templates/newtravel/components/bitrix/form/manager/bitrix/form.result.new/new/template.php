<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y")
{
?>
<?=$arResult["FORM_HEADER"]?>

<?
/***********************************************************************************
						form questions
***********************************************************************************/
?>

<?$page = $APPLICATION->GetCurPage(); //Текущая страница?>

<?if ($page == "/faq/application.php"):?>
	<div><h1>Заказать тур менеджеру</h1></div>
	</br>
	
	<?$arQuestionId = array("SIMPLE_QUESTION_696","SIMPLE_QUESTION_851","SIMPLE_QUESTION_851_hJ9EU","SIMPLE_QUESTION_760","SIMPLE_QUESTION_717","SIMPLE_QUESTION_426","SIMPLE_QUESTION_382","SIMPLE_QUESTION_783",
			"SIMPLE_QUESTION_574","SIMPLE_QUESTION_466","SIMPLE_QUESTION_422","SIMPLE_QUESTION_310","SIMPLE_QUESTION_395","SIMPLE_QUESTION_739","SIMPLE_QUESTION_533","SIMPLE_QUESTION_521","SIMPLE_QUESTION_305",
			"SIMPLE_QUESTION_526");?>
			
	<pre><?//print_r($arQuestionId)?></pre>		
	
	<?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion):?>
		<?if (in_array($FIELD_SID, $arQuestionId)):?>	
		
			<?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
				<span class="error-fld" title="<?=$arResult["FORM_ERRORS"][$FIELD_SID]?>"></span>
			<?endif;?>		
			
			<div class="form-question">
				<div class="q-caption">
					<?=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?>
					<?=$arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />".$arQuestion["IMAGE"]["HTML_CODE"] : ""?>
				</div>
				<div class="q-answer"><?=$arQuestion["HTML_CODE"]?></div>
				</br>
			</div> 
		<?endif;?>
	<?endforeach;?>
<?endif;?>


<?if ($page == "/club_burning.php"):?>
	<div><h1>Заявка на вступление в клуб «Горящие чемоданы»</h1></div>
	</br>
	
	<?$arQuestionId = array("SIMPLE_QUESTION_847", "SIMPLE_QUESTION_415","SIMPLE_QUESTION_696","SIMPLE_QUESTION_851","SIMPLE_QUESTION_717","SIMPLE_QUESTION_426","SIMPLE_QUESTION_382","SIMPLE_QUESTION_783",
			"SIMPLE_QUESTION_574","SIMPLE_QUESTION_466","SIMPLE_QUESTION_422","SIMPLE_QUESTION_310","SIMPLE_QUESTION_395","SIMPLE_QUESTION_739","SIMPLE_QUESTION_533","SIMPLE_QUESTION_521","SIMPLE_QUESTION_526");?>	
	
	<?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion):?>
		<pre><?//print_r($FIELD_SID)?></pre>		
		<?if (in_array($FIELD_SID, $arQuestionId)):?>	
		
			<?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
				<span class="error-fld" title="<?=$arResult["FORM_ERRORS"][$FIELD_SID]?>"></span>
			<?endif;?>		
			
			<div class="form-question">
				<div class="q-caption">
					<?=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?>
					<?=$arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />".$arQuestion["IMAGE"]["HTML_CODE"] : ""?>
				</div>
				<div class="q-answer"><?=$arQuestion["HTML_CODE"]?></div>
				</br>
			</div> 
		<?endif;?>
	<?endforeach;?>
<?endif;?>

<?if($arResult["isUseCaptcha"] == "Y"){?>
<?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?>
<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" />
<?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?>
<input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" />
<?} // isUseCaptcha?>


<input <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />
<?if ($arResult["F_RIGHT"] >= 15):?>
&nbsp;<input type="hidden" name="web_form_apply" value="Y" /><input type="submit" name="web_form_apply" value="<?=GetMessage("FORM_APPLY")?>" />
<?endif;?>
&nbsp;<input type="reset" value="<?=GetMessage("FORM_RESET");?>" />

<p>
<?=$arResult["REQUIRED_SIGN"];?> - <?=GetMessage("FORM_REQUIRED_FIELDS")?>
</p>
<?=$arResult["FORM_FOOTER"]?>
<?
} //endif (isFormNote)
?>
