<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//PR($arResult);
$page = $APPLICATION->GetCurPage();
?>
<?if(count($arResult["ITEMS"]["AnDelCanBuy"]) > 0):?>

	<?
	$i=0;
	foreach($arResult["ITEMS"]["AnDelCanBuy"] as $arBasketItems):?>
		<?	if(!$arBasketItems['PRODUCT_ID']==502545 && !count($arBasketItems["PROPS"])>0)continue?>
		<!-- =========================================
		Gray Section
		========================================== -->
		<section class="gray-section detail-tour">
		<!-- container -->
		<div class="container">
		    <!--Шапка страницы тура-->
		    <div class="row">
		        <div class="breadcrumbs">
		            <ul>
		                <li class="first active">1. Подробнее о туре</li>
		                <li class="">2. Заполнение данных</li>
		                <li class="last">3. Оплата</li>
		            </ul>
		        </div>
		    </div>

		    <div class="row page-header">
                <div class="detail-item-resort">Тур в отель</div> <div class="hotel-name">
                    <?if(is_array($arBasketItems["PROPS"]["HOTEL_ID"])):?>
                        <a href="/hotels/<?=$arBasketItems["PROPS"]["HOTEL_ID"]["VALUE"]?>.html" target="_blank">
                            <?=htmlspecialchars_decode(htmlspecialchars_decode($arBasketItems["PROPS"]["HOTEL_NAME"]["VALUE"]))?>
                        </a>
                    <?else:?>
                        <?=$arBasketItems["NAME"]?>
                    <?endif;
                    unset($arBasketItems["PROPS"]["HOTEL_ID"]);?>
                </div>
                <div class="detail-hotel-stars">
                    <?switch ($arBasketItems["PROPS"]["STAR_NAME"]["VALUE"]) {
                        case "1":
                            echo '<i class="fa fa-star"></i>';
                            break;
                        case "2":
                            echo '<i class="fa fa-star"></i><i class="fa fa-star"></i>';
                            break;
                        case "3":
                            echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
                            break;
                        case "4":
                            echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
                            break;
                        case "5":
                            echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
                            break;
                        default:
                            echo "";
                            break;
                    }?>
                </div>
                <div class="detail-item-resort"><?=$arBasketItems["PROPS"]["COUNTRY_NAME"]["VALUE"]?>, <?=$arBasketItems["PROPS"]["RESORT_NAME"]["VALUE"]?></div>
		    </div><!--/Шапка страницы тура-->

		    <div class="row" style="border-bottom: 1px solid #c4c4c4; padding-bottom: 15px">
		        <div class="col-md-6 tour-info">
		            <div class="flights">
		                <div class="">Туда: <?=$arBasketItems["PROPS"]["CHECK_IN_DATE"]["VALUE"]?> <?=$arBasketItems["PROPS"]["CITY_FROM_NAME"]["VALUE"]?>-<?=$arBasketItems["PROPS"]["RESORT_NAME"]["VALUE"]?></div>
		                <!-- div class="">Обратно: 28.08.2014 Сиде-Уфа</div -->
		                <div class="">Ночей: <?=$arBasketItems["PROPS"]["NIGHTS"]["VALUE"]?></div>
		            </div>
		            <div class="hotel">
		                <div class="">Отель: <?=$arBasketItems["PROPS"]["HOTEL_NAME"]["VALUE"]?></div>
		                <div class="">Количество звезд: <?=$arBasketItems["PROPS"]["STAR_NAME"]["VALUE"]?></div>
		                <div class="">Номер: <?=$arBasketItems["PROPS"]["HT_PLACE_DESCRIPTION"]["VALUE"]?></div>
		                <div class="">Питание: <?=$arBasketItems["PROPS"]["MEAL_DESCRIPTION"]["VALUE"]?></div>
		            </div>
		            <div class="includes">
		                <div class="name">В стоимость тура включено:</div>
                        <ul class="information-list">
                            <li><i class="fa fa-plane fa-3x"></i><span>Авиаперелет, туда-обратно</span></li>
                            <li><i class="fa fa-arrow-circle-right fa-3x"></i><span>Трансфер, Аэропорт-Отель-Аэропорт</span></li>
                            <li><i class="fa fa-home fa-3x"></i><span>Проживание в отеле</span></li>
                            <li><i class="fa fa-list fa-3x"></i><span>Питание в отеле (на выбор)</span></li>
                            <li><i class="fa fa-plus fa-3x"></i><span>Медицинская страховка</span></li>
                        </ul>
		            </div>
		        </div>
		        <div class="col-md-6">
                    <div class="jcarousel-wrapper">

                        <!-- Carousel -->
                        <div class="jcarousel" data-jcarousel="true">
                            <ul style="left: 0px; top: 0px;">
                                <?foreach ($arResult["HOTEL"]["ImageUrls"]["string"] as $value):?>
                                    <li><a href="<?=$value?>" class="fancybox" data-fancybox-group="gallery3"><img src="<?=$value?>" width="600" height="400" alt=""></a></li>
                                <?endforeach?>
                            </ul>
                        </div>

                        <!-- Prev/next controls -->
                        <a href="#" class="jcarousel-control-prev inactive" data-jcarouselcontrol="true"><i class="fa fa-arrow-circle-left fa-2x"></i></a>
                        <a href="#" class="jcarousel-control-next" data-jcarouselcontrol="true"><i class="fa fa-arrow-circle-right fa-2x"></i></a>

                    </div>
		        </div>
		    </div>

		    <div class="row extras">
		        <h4>Дополнительные услуги:</h4>
                <?//Получаем цены для доп услуг
                    $priceInsurance = GetCatalogProductPrice(505156, 2);
                    $priceMedInsurance = GetCatalogProductPrice(505157, 2);
                ?>
		        <ul>

					<?foreach ($arResult["EXTRAS"] as $key => $extra):?>								
			            <li>
			                <div class="col-md-4 name"><?=$extra["NAME"]?></div>
			                <div class="col-md-6 descr">Оформить <?=$extra["NAME"]?> за <?=$extra["PRICE"]["PRICE"] * $arBasketItems["PROPS"]["ADULTS"]["VALUE"] ?> рублей на всех</div>
			                <?if (in_array($extra["ID"], $arResult["BASKET_ITEMS_ID"])):?>
			                	<?$basketItemId = $arResult["BASKET_ITEMS_ID"]["ID"][$extra["ID"]]?>
				                <div class="col-md-2 btn btn-warning" id="add<?=$extra['ID']?>" onclick="addExtras(<?=$extra['ID']?>, <?=$arBasketItems["PROPS"]["ADULTS"]["VALUE"]?>)" style="display: none">Добавить!</div>
		                        <div class="col-md-2 btn btn-danger" id="del<?=$extra['ID']?>" basketItemId="<?=$basketItemId?>" onclick="deleteExtras(<?=$extra['ID']?>)" style="display: block">Удалить!</div>
	                        <?else:?>
				                <div class="col-md-2 btn btn-warning" id="add<?=$extra['ID']?>" onclick="addExtras(<?=$extra['ID']?>, <?=$arBasketItems["PROPS"]["ADULTS"]["VALUE"]?>)" style="display: block">Добавить!</div>
		                        <div class="col-md-2 btn btn-danger" id="del<?=$extra['ID']?>" basketItemId="" onclick="deleteExtras(<?=$extra['ID']?>)" style="display: none">Удалить!</div>	                        
	                        <?endif?>
			            </li>						
					<?endforeach?>		        
		        </ul>

		    </div>

		    <div class="row summary">
		        <div class="col-md-4 col-xs-12">
		            <h4>Цена тура:</h4>		            
		            <div class="price"><?if($arResult["allSum"]>0){echo $arResult["allSum_FORMATED"];}?></div>
		        </div>
		        <div class="col-md-4 descr">
                    <div class="taxes">
                        <?if ($arBasketItems["PROPS"]["FINAL_OIL_TAX"]["VALUE"] > 0):?>
                            Стоимость тура без доплат -  <?=$arBasketItems["PROPS"]["CLEAR_PRICE"]["VALUE"]?> Р<br/>
                            Стоимость топливных сборов на <?=$arBasketItems["PROPS"]["ADULTS"]["VALUE"]+$arBasketItems["PROPS"]["KIDS"]["VALUE"]?> человек - <?=$arBasketItems["PROPS"]["FINAL_OIL_TAX"]["VALUE"]?> Р <br/>
                        <?endif?>
                        <?foreach($arResult["ITEMS"]["AnDelCanBuy"] as $arExtras):?>
                            <?if(count($arExtras["PROPS"])>0)continue?>
                            <?=$arExtras["NAME"]?> - <?=$arExtras["PRICE_FORMATED"]?></br>
                        <?endforeach?>
                    </div>
		        </div>
		        <div class="col-md-4 col-xs-12  buy-btn">
		            <input type="submit" class="oforma btn btn-warning col-xs-12" value="Оформить тур!" name="BasketOrder"  id="basketOrderButton2">
		        </div>
		    </div>

		</div>
		</section><!-- /section -->
	<?endforeach;?>
<?else:
		echo ShowNote(GetMessage("SALE_NO_ACTIVE_PRD"));
endif;?>

<script type="text/javascript">


    $(window).load(function(){

        $('.jcarousel').jcarousel({
            // Configuration goes here
        });

    });

    function addExtras(ID, quantity) {
        $.get("/bitrix/tools/simai.travel_ajax.php", { id: ID, type: "addExtras", quantity: quantity }, function(result) {
            $("#del"+ID).attr("basketItemId", result);
            $(".price").addClass("load");

        })
            .success(function() {
                $.get("<?=$page?>", {}, function(data) {
                    var price = $(data).find(".price").text();
                    $(".price").text(price).removeClass("load");
                });

                $("#add"+ID).css("display", "none");
                $("#del"+ID).css("display", "block");

            });
    }
    function deleteExtras(ID) {
        var basketItemId = $("#del"+ID).attr("basketItemId");
        $.get("/bitrix/tools/simai.travel_ajax.php", { id: basketItemId, type: "delExtras"}, function(data) {
        	$(".price").addClass("load");
        })
            .success(function(data) {
                $.get("<?=$page?>", {}, function(data) {
                    var price = $(data).find(".price").text();
                    $(".price").html(price).removeClass("load");
                });


                $("#add"+ID).css("display", "block");
                $("#del"+ID).css("display", "none");

            });
    }
</script>