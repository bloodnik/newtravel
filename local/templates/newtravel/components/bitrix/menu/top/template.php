<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (empty($arResult))return;?>
<div class="t-menu">
<ul>
<?foreach($arResult as $arItem):?>
            <li class="tml<?if($arItem["SELECTED"]):?> active<?endif?>"><a href="<?=$arItem["LINK"]?>"><span class="lb"></span><span class="cb"><?=$arItem["TEXT"]?></span><span class="rb"></span></a></li>
<?endforeach?>
 </ul>
</div>