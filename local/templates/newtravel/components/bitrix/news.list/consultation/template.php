<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="news-list">

<?
//Получаем код текущего инфоблока

foreach($arResult["ITEMS"] as $arItem):?>
		<?if($arItem["PREVIEW_TEXT"]):?>

			<?  $text = TruncateText($arItem["PREVIEW_TEXT"], 300);
				$IBlockName = "IBLOCK_".$arItem["IBLOCK_ID"]."_SECTION";
			   	if (UserAccess($IBlockName, $arItem["IBLOCK_SECTION_ID"])):?>
					<div class="access">
					<?if($arItem["DETAIL_TEXT"]):?>
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="black_line_off"><?=$text?></a>
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="/images/change.gif" border="0"></a>
					<?else:?>
						<?echo $text;?>
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="/images/reply.gif" border="0"></a>
					<?endif;?>
					</div>
			   	<?else:?>
					<div class="no_access">
					<?if($arItem["DETAIL_TEXT"]):?>
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="black_line_off"><?=$text?></a>
					<?else:?>
						<?echo $text;?>&nbsp;<span class="wait_answer">Ответ ожидается</span>
					<?endif;?>
					</div>
			   	<?endif;?>

		<?endif;?>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
