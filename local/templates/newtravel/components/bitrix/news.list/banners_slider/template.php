<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container" style="position: relative">
	<div id="banners">
		<? foreach ($arResult["ITEMS"] as $arItem): ?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<a href="<?=$arItem["PREVIEW_TEXT"]?>" target="_blank">
				<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="Умныетуристы.рф. Баннер <?=$arItem["NAME"]?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>" style="width: 1140px;"/>
			</a>

		<? endforeach; ?>
	</div>
<!--	<a class="caroufredsel-prev" href="#" style="display: block;"><span> <i class="fa fa-chevron-left"></i> </span></a>-->
<!--	<div class="caroufredsel-pag" style="display: block;"></div>-->
<!--	<a class="caroufredsel-next" href="#" style="display: block;"><span> <i class="fa fa-chevron-right"></i> </span></a>-->
</div>

<script>
	BX.ready(function () {
		// Слайдер банноеров на главной
		$('#banners').carouFredSel({
			items: 1,
			width: 1170,
			height: 400,
			direction: "right",
			responsive: true,
			pagination: ".caroufredsel-pag",
			prev: ".caroufredsel-prev",
			next: ".caroufredsel-next",
			swipe: {
				onTouch: true,
				onMouse: true,
				options: {
					excludedElements: "button, input, select, textarea, .noSwipe"
				}
			},
			scroll: {items: 1, timeoutDuration: 10000},
			mousewheel: {items: 1},
			auto: {play: true}
		});
	});
</script>