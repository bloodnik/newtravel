<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>

<? foreach ($arResult["ITEMS"] as $arItem): ?>
	<?
	$db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), Array("CODE" => "REAL_PICTURE"));
	if ($ar_props = $db_props->Fetch()) {
		$REAL_PICTURE = CFile::GetPath(IntVal($ar_props["VALUE"]));
	}
	?>
	<!-- latest-works -->
	<ul class="resort-gallery my-work">
		<!-- item 1 -->
		<li>
			<!-- img-figure-overlayer -->
			<div class="img-figure-overlayer">
				<a class="fancybox" data-fancybox-group="gallery3" href="<?=$REAL_PICTURE?>" title="<?=$arItem['NAME']?>">
					<i class="fa fa-search"></i>
				</a>
			</div><!-- /img-figure-overlayer -->

			<!-- img-figure -->
			<div class="img-figure">
				<!-- image -->
				<img src="<?=$REAL_PICTURE?>" alt="">
			</div><!-- /img-figure -->

		</li><!-- /item 1 -->

	</ul><!-- /latest-works -->
<? endforeach; ?>


<script>
	/* ==========================================================================
	 Latest Works
	 ========================================================================== */
	$(".latest-works li").mouseenter(function () {
		$(this).find('.img-figure-overlayer a:first-child').addClass('animated slideInLeft');
	});
	$(".latest-works li").mouseleave(function () {
		$(this).find('.img-figure-overlayer a:first-child').removeClass('animated slideInLeft');
	});

	$(".latest-works li").mouseenter(function () {
		$(this).find('.img-figure-overlayer a:last-child').addClass('animated slideInRight');
	});
	$(".latest-works li").mouseleave(function () {
		$(this).find('.img-figure-overlayer a:last-child').removeClass('animated slideInRight');
	});
</script>
