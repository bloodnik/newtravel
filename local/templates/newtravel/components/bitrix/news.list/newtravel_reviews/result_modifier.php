<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

/*Для rel=prev rel=next*/
$arResult['NAV_RESULT_NAV_NUM'] = $arResult['NAV_RESULT']->NavNum;

$arResult['NAV_RESULT_NAV_PAGE_NOMER'] = $arResult['NAV_RESULT']->NavPageNomer;

$arResult['NAV_RESULT_NAV_PAGE_COUNT'] = $arResult['NAV_RESULT']->NavPageCount;

if(!empty($arResult['SECTION'])){
	$arResult['SECTION_SECTION_PAGE_URL'] = !empty($arResult['SECTION']['PATH'][0]['SECTION_PAGE_URL']) ? $arResult['SECTION']['PATH'][0]['SECTION_PAGE_URL'] : null;
}else{
	$arResult['SECTION_SECTION_PAGE_URL'] = $arResult['LIST_PAGE_URL'];
}

$this->__component->SetResultCacheKeys([
	'NAV_RESULT_NAV_NUM',
	'NAV_RESULT_NAV_PAGE_NOMER',
	'NAV_RESULT_NAV_PAGE_COUNT',
	'SECTION_SECTION_PAGE_URL',
]);