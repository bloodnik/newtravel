<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>

<?

foreach ($arResult['ITEMS'] as $key => $arItem) {
	if ($arItem['DISPLAY_PROPERTIES']['EXPERT']) {
		global $USER;
		$obUser = $USER->GetByID($arItem['DISPLAY_PROPERTIES']['EXPERT']['DISPLAY_VALUE']);

		$arUser = $obUser->GetNext();
		$arUserInfo = array();
		$arUserInfo['NAME']          = $arUser['NAME'] . " " . $arUser['LAST_NAME'];
		$arUserInfo['WORK_POSITION'] = $arUser['WORK_POSITION'];
		if(!empty($arUser['PERSONAL_PHOTO'])){
			$arUserInfo['PHOTO']         = CFile::GetPath($arUser['PERSONAL_PHOTO']);
		}

		$arResult["ITEMS"][ $key ]["USER_INFO"] = $arUserInfo;

	}

}

$this->__component->arResultCacheKeys = array_merge($this->__component->arResultCacheKeys, array('ITEMS'));

