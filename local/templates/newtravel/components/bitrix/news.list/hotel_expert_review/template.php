<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>
<? if ( ! empty($arResult['ITEMS'])): ?>
	<section class="white-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 nopadding">
					<div class="responses">
						<h3 class="responses-title">Отзывы об отеле</h3>

						<? foreach ($arResult["ITEMS"] as $arItem):
							$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
							$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
							?>
							<div class="responses-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
								<? if ($arItem['DISPLAY_PROPERTIES']['EXPERT']): ?>
									<div class="author">
										<? if ($arItem["USER_INFO"]['PHOTO']): ?>
											<img class="avatar" src="<?=$arItem["USER_INFO"]['PHOTO']?>" alt="<?=$arItem["USER_INFO"]["NAME"]?>">
										<? else: ?>
											<img class="avatar" src="<?=SITE_TEMPLATE_PATH?>/images/clients/noava.png" alt="<?=$arItem["USER_INFO"]["NAME"]?>">
										<? endif ?>
										<div class="author-detail">
											<span class="author-name"><?=$arItem["USER_INFO"]['NAME']?></span>
											<small class="author-post"><?=$arItem["USER_INFO"]['WORK_POSITION']?></small>
										</div>

										<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" target="_blank" class="btn btn-blue">
											Задать вопрос <br> эксперту
										</a>

									</div>
								<? endif; ?>

								<blockquote>
									<h3><?=$arItem['NAME']?></h3>
									<? if ($arItem["PREVIEW_TEXT"]): ?>
										<?=$arItem["PREVIEW_TEXT"]?>
									<? elseif ($arItem["DETAIL_TEXT"]): ?>
										<?=$arItem["DETAIL_TEXT"]?>
									<? endif ?>
									<div></div>

									<? if ($arItem['DISPLAY_PROPERTIES']['MORE_PHOTO']): ?>
										<br>
										<? foreach ($arItem['DISPLAY_PROPERTIES']['MORE_PHOTO']['VALUE'] as $arFileId): ?>
											<a class="hotel_pics" href="<?=CFile::GetPath($arFileId)?>" rel="gall"><img src="<?=CFile::GetPath($arFileId)?>"></a>
										<? endforeach; ?>
										<div class="clearfix"></div>
									<? endif; ?>
								</blockquote>
							</div>
						<? endforeach ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<script>
		BX.ready(function () {
			$("a.hotel_pics").fancybox({
				'transitionIn': 'elastic',
				'transitionOut': 'elastic',
				'speedIn': 600,
				'speedOut': 200,
				'overlayShow': false
			});
		})
	</script>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Задать вопрос эксперту</h4>
				</div>
				<div class="modal-body">
					<? $APPLICATION->IncludeComponent(
						"bitrix:form.result.new",
						"expert_question",
						Array(
							"HOTEL_ID"               => $arParams['HOTEL_ID'],
							"SEF_MODE"               => "Y",
							"WEB_FORM_ID"            => "7",
							"LIST_URL"               => "",
							"EDIT_URL"               => "",
							"SUCCESS_URL"            => "",
							"CHAIN_ITEM_TEXT"        => "",
							"CHAIN_ITEM_LINK"        => "",
							"IGNORE_CUSTOM_TEMPLATE" => "N",
							"USE_EXTENDED_ERRORS"    => "N",
							"CACHE_TYPE"             => "A",
							"CACHE_TIME"             => "3600",
							"SEF_FOLDER"             => "/test/",
							"EXPERT_NAME"            => $arItem["USER_INFO"]['NAME'],
							"VARIABLE_ALIASES"       => Array()
						),
						false
					); ?>
				</div>
			</div>
		</div>
	</div>
<? endif; ?>