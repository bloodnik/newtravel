<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>

<?
global $APPLICATION;

if(!empty($arResult['ITEMS']) && !empty($arResult['ITEMS'][0]['PREVIEW_TEXT'])){
	$APPLICATION->SetPageProperty('description', mb_strimwidth(str_replace("\n", " ",strip_tags($arResult['ITEMS'][0]['PREVIEW_TEXT'])), 0, 250, "..."));
}
?>
