<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ITEMS"])<1){echo"<div class=\"details\" style=\"margin-top:-1px;\"><div class=\"detail-in\">".GetMessage("NO_EXCURS")."</div></div>";return;}?>

<section class="gray-section">
    <div class="container">

        <div class="row">

            <div class="col-md-12">
                <div class="section-title">
                    <!-- main-title -->
                    <div class="main-title">
                        <h2>Горящие путёвки</h2>
                    </div><!-- /main-title -->
                </div>
            </div>

        </div>

        <div class="row">
            <div class="hot-tours">
                <?$i=1?>
                <?foreach($arResult["ITEMS"] as $arItem):?>
                    <?//PR($arItem)?>
                    <?if($i==1):?>
                        <div class="col-xs-12 col-sm-6 height-2 ht-wrap">
                            <a href="/hot-tours/?countryId=<?=$arItem["CODE"]?>&clear_cache=Y">
                                <div class="ht-tour" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');">
                                    <div class="ht-info">
                                        <div class="title">
                                            <span><?=$arItem["NAME"]?></span>
                                        </div>
                                        <div class="ht-price">
                                            <h2><span>от <?=strlen($arItem["PROPERTIES"]["MIN_PRICE"]["VALUE"]>0)?$arItem["PROPERTIES"]["MIN_PRICE"]["VALUE"]:"0"?><i class="fa fa-rub"></i><small> за <i class="fa fa-user"></i></small></span></h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?elseif($i==2):?>
                        <div class="col-xs-12 col-sm-6 height-1 ht-wrap">
                            <a href="/hot-tours/?countryId=<?=$arItem["CODE"]?>&clear_cache=Y">
                                <div class="ht-tour" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');">
                                    <div class="ht-info">
                                        <div class="title">
                                            <span><?=$arItem["NAME"]?></span>
                                        </div>
                                        <div class="ht-price">
                                            <h2><span>от <?=strlen($arItem["PROPERTIES"]["MIN_PRICE"]["VALUE"]>0)?$arItem["PROPERTIES"]["MIN_PRICE"]["VALUE"]:"0"?><i class="fa fa-rub"></i><small> за <i class="fa fa-user"></i></small></span></h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?elseif($i>=3):?>
                        <div class="col-xs-12 col-sm-3 height-1 ht-wrap">
                            <a href="/hot-tours/?countryId=<?=$arItem["CODE"]?>&clear_cache=Y">
                                <div class="ht-tour" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');">
                                    <div class="ht-info">
                                        <div class="title">
                                            <span><?=$arItem["NAME"]?></span>
                                        </div>
                                        <div class="ht-price">
                                            <h2><span>от <?=strlen($arItem["PROPERTIES"]["MIN_PRICE"]["VALUE"]>0)?$arItem["PROPERTIES"]["MIN_PRICE"]["VALUE"]:"0"?><i class="fa fa-rub"></i><small> за <i class="fa fa-user"></i></small></span></h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?endif;?>
                    <?$i++?>
                <?endforeach?>

            </div><!-- /hot-tours -->
        </div><!-- /row -->

    </div><!-- /container -->
</section>
