<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="specialoffer-list row">
	<div class="section-title">
		<div class="main-title">
			<h2><?=$arParams['PAGER_TITLE']?></h2>
		</div>
		<div class="description-title">
			<h3><?=$arParams['TITLE_DESCRIPTION']?></h3>
		</div>
	</div>
	<? foreach ($arResult["ITEMS"] as $arItem): ?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="col-md-4 col-xs-6 col-sm-6 item-wrap" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="item" itemscope itemtype="http://schema.org/Product" >
				<!--noindex-->
				<a href="<?=$arItem['DISPLAY_PROPERTIES']['URL']['DISPLAY_VALUE']?>" rel="nofollow" class="img-link" target="_blank" style="background: url('<?=$arItem['PREVIEW_PICTURE']['SRC']?>')">
					<meta itemprop="image" content="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" />
				</a>

				<div class="item_name" itemprop="name"><?=$arItem['NAME']?></div>
				<a href="<?=$arItem['DISPLAY_PROPERTIES']['URL']['DISPLAY_VALUE']?>" rel="nofollow" itemprop="url" target="_blank">
					<div class="item_price">
						<div class="left" itemprop="description">
							<? if ($arItem['DISPLAY_PROPERTIES']['DATE']): ?>
								<small><?=$arItem['DISPLAY_PROPERTIES']['DATE']['DISPLAY_VALUE']?></small>
								<br>
							<? endif; ?>
							<? if ($arItem['DISPLAY_PROPERTIES']['NIGHTS']): ?>
								<small><?=$arItem['DISPLAY_PROPERTIES']['NIGHTS']['DISPLAY_VALUE']?> ночей</small>
							<? endif; ?>
						</div>
						<div class="right" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
							<meta itemprop="price" content="<?=$arItem['DISPLAY_PROPERTIES']['PRICE']['DISPLAY_VALUE']?>" />
							<meta itemprop="priceCurrency" content="RUB" />
							от <span><?=CurrencyFormat($arItem['DISPLAY_PROPERTIES']['PRICE']['DISPLAY_VALUE'], "RUB")?></span> / чел.
						</div>
					</div>
				</a>
				<!--/noindex-->
			</div>
		</div>
	<? endforeach ?>
</div>
