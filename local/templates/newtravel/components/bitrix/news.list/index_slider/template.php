<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$this->addExternalCss($this->GetFolder() . "/js/assets/owl.carousel.min.css");
$this->addExternalCss($this->GetFolder() . "/js/assets/owl.theme.default.min.css");
$this->addExternalJs($this->GetFolder() . "/js/owl.carousel.min.js");
?>

<style>
	.owl-carousel .item-video {
		height: 300px;
	}

	.owl-carousel .owl-video-wrapper {
		position: relative;
		height: 100%;
		background: #000;
	}

</style>
<div class="container">

	<div class="section-title">
		<!-- main-title -->
		<div class="main-title">
			<h2>СМИ о нас</h2>
		</div>
		<!-- /main-title --> <!-- description-title --> <br>
		<!-- /description-title -->
	</div>

	<div class="owl-carousel owl-theme sli-slider">
		<? foreach ($arResult["ITEMS"] as $arItem): ?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<? if (strlen($arItem['~PREVIEW_TEXT']) > 0): ?>
				<div class="item-video" data-merge="1" id="<?=$this->GetEditAreaId($arItem['ID']);?>"><a class="owl-video" href="<?=$arItem['~PREVIEW_TEXT']?>"></a></div>
			<? else: ?>
				<div class="item-video" data-merge="1" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<a class="fancybox" rel="nofollow" href="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
						<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="">
					</a>
				</div>
			<? endif; ?>
		<? endforeach; ?>
	</div>
</div>


<script>
    $(document).ready(function () {
        $(".sli-slider").owlCarousel({
            items: 3,
            merge: true,
            loop: true,
            margin: 10,
            video: true,
            lazyLoad: true,
            center: true,
            autoplay : true,
            autoplayTimeout : 4000,
            paginationSpeed: 400,

        });
    });
</script>