<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="actions-section">
	<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
		<div class="section" id="<?=$key?>">
			<h2 data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$key?>" class="collapsed"><?=$arItem['NAME']?></h2>
			<div class="content panel-collapse collapse " id="collapse<?=$key?>" style="">
				<hr>
				<? if ($arItem['PREVIEW_PICTURE']): ?>
					<div class="thumbnail"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt=""></div>
				<? endif; ?>
				<? if (strlen($arItem['PREVIEW_TEXT']) > 0): ?>
					<?=$arItem['PREVIEW_TEXT']?>
				<? elseif (strlen($arItem['DETAIL_TEXT']) > 0): ?>
					<?=$arItem['DETAIL_TEXT']?>
				<? endif; ?>
			</div>
		</div>
		<div class="clearfix"></div>
	<? endforeach ?>
</div>
