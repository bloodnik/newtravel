<?
foreach($arResult["ITEMS"] as $key=>$arItem){
	if(is_array($arItem["PREVIEW_PICTURE"])){
		$arFileTmp = CFile::ResizeImageGet(
			$arItem["PREVIEW_PICTURE"],
			array("width" => 130, "height" => 95),
			BX_RESIZE_IMAGE_EXACT,
			false
		);
		$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"].$arFileTmp["src"]);
		$arResult["ITEMS"][$key]["PREVIEW_IMG"]= array(
			"SRC" => $arFileTmp["src"],
			"WIDTH" => IntVal($arSize[0]),
			"HEIGHT" => IntVal($arSize[1]),
		);
		}
		elseif(is_array($arItem["DETAIL_PICTURE"])){
			$arFileTmp = CFile::ResizeImageGet(
			$arItem["DETAIL_PICTURE"],
			array("width" => 130, "height" => 95),
			BX_RESIZE_IMAGE_EXACT,
			false
		);
		$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"].$arFileTmp["src"]);
		$arResult["ITEMS"][$key]["PREVIEW_IMG"]= array(
			"SRC" => $arFileTmp["src"],
			"WIDTH" => IntVal($arSize[0]),
			"HEIGHT" => IntVal($arSize[1]),
		);
		}
}


/*Для rel=prev rel=next*/
$arResult['NAV_RESULT_NAV_NUM'] = $arResult['NAV_RESULT']->NavNum;

$arResult['NAV_RESULT_NAV_PAGE_NOMER'] = $arResult['NAV_RESULT']->NavPageNomer;

$arResult['NAV_RESULT_NAV_PAGE_COUNT'] = $arResult['NAV_RESULT']->NavPageCount;

if(!empty($arResult['SECTION'])){
	$arResult['SECTION_SECTION_PAGE_URL'] = !empty($arResult['SECTION']['PATH'][0]['SECTION_PAGE_URL']) ? $arResult['SECTION']['PATH'][0]['SECTION_PAGE_URL'] : null;
}else{
	$arResult['SECTION_SECTION_PAGE_URL'] = $arResult['LIST_PAGE_URL'];
}

$this->__component->SetResultCacheKeys([
	'NAV_RESULT_NAV_NUM',
	'NAV_RESULT_NAV_PAGE_NOMER',
	'NAV_RESULT_NAV_PAGE_COUNT',
	'SECTION_SECTION_PAGE_URL',
]);

?>