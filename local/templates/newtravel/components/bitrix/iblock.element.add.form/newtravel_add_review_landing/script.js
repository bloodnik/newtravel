$('document').ready(function(){
    $('.landing_review_wrap input[type="text"], .landing_review_wrap input[type="email"], .landing_review_wrap textarea').focus(function(){
        var background = $(this).attr('id');
        $('#' + background + '-form').addClass('formgroup-active');
        $('#' + background + '-form').removeClass('formgroup-error');
    });
    $('.landing_review_wrap input[type="text"], .landing_review_wrap input[type="email"], .landing_review_wrap textarea').blur(function(){
        var background = $(this).attr('id');
        $('#' + background + '-form').removeClass('formgroup-active');
    });

    function errorfield(field){
        $(field).addClass('formgroup-error');
        console.log(field);
    }

    $(".landing_review_wrap #waterform").submit(function() {
        var stopsubmit = false;

        if($('.landing_review_wrap #name').val() == "") {
            errorfield('#name-form');
            stopsubmit=true;
        }
        if($('.landing_review_wrap #email').val() == "") {
            errorfield('#email-form');
            stopsubmit=true;
        }
        if(stopsubmit) return false;
    });

});