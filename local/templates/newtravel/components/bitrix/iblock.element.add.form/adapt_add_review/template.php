<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>


<?if (count($arResult["ERRORS"])):?>
	<?=ShowError(implode("<br />", $arResult["ERRORS"]))?>
<?endif?>



<?if (strlen($arResult["MESSAGE"]) > 0):?>
    <script type="text/javascript">
        $(document).ready(function() {
            //$('#add_review').modal('hide');
        }
    </script
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>


<div class="add-new-tourist-form">

	<!-- modal-header -->
	<div class="modal-header form-header">
	    <!-- X close -->
		<div class="col-md-10">
			<p>Добавить отзыв</p>
		</div>	
		<div class="col-md-2">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		</div>		
	</div><!-- /modal-header -->

	<!-- modal-body -->
	
			<form name="iblock_add" id="add_review" class='add-new-tourist-form form-horizontal' action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data" onsubmit="">
			<div class="modal-body form-body">

				<?=bitrix_sessid_post();
				//echo"<pre>";print_r($arResult["PROPERTY_LIST_FULL"]);echo"</pre>";
				?>
				<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
					<?if (is_array($arResult["PROPERTY_LIST"]) && count($arResult["PROPERTY_LIST"] > 0)):?>
						<input type="hidden" value="Отзыв" id="PROPERTY_NAME_0" name="PROPERTY[NAME][0]" />
							
						<?foreach ($arResult["PROPERTY_LIST"] as $propertyID):?>
							<div class="col-md-12">
							<?if(!in_array($propertyID, array("NAME"))):?>
									<?
									if (intval($propertyID) > 0)
									{
										if (
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
											&&
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
										)
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
										elseif (
											(
												$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
												||
												$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
											)
											&&
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
										)
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
									}
									elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

									if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
									{
										$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
										$inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
									}
									else
									{
										$inputNum = 1;
									}

									if($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
										$INPUT_TYPE = "USER_TYPE";
									else
										$INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

									switch ($INPUT_TYPE):
										case "USER_TYPE":
											for ($i = 0; $i<$inputNum; $i++)
											{
												if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
												{
													$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
													$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
												}
												elseif ($i == 0)
												{
													$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
													$description = "";
												}
												else
												{
													$value = "";
													$description = "";
												}
												if($propertyID=="45"){
												echo '<label for="tourist-sname">Дата рождения</label>';
												echo '<input type="text" class="familia " placeholder="Дата рождения" value="'.$value.'"  name="PROPERTY['.$propertyID.']['.$i.'][VALUE]"/>';
												}elseif($propertyID=="55"){
												echo '<label for="tourist-sname">Действителен до</label>';													
												echo '<input type="text" class="familia " value="'.$value.'" name="PROPERTY['.$propertyID.']['.$i.'][VALUE]"/>';
												}
												else{
												echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
													array(
														$arResult["PROPERTY_LIST_FULL"][$propertyID],
														array(
															"VALUE" => $value,
															"DESCRIPTION" => $description,
														),
														array(
															"VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
															"DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
															"FORM_NAME"=>"iblock_add",
														),
													));
												}
											}
										break;
										case "TAGS":
											$APPLICATION->IncludeComponent(
												"bitrix:search.tags.input",
												"",
												array(
													"VALUE" => $arResult["ELEMENT"][$propertyID],
													"NAME" => "PROPERTY[".$propertyID."][0]",
													"TEXT" => 'size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"].'"',
												), null, array("HIDE_ICONS"=>"Y")
											);
											break;
										case "HTML":
											$LHE = new CLightHTMLEditor;
											$LHE->Show(array(
												'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]"),
												'width' => '100%',
												'height' => '200px',
												'inputName' => "PROPERTY[".$propertyID."][0]",
												'content' => $arResult["ELEMENT"][$propertyID],
												'bUseFileDialogs' => false,
												'bFloatingToolbar' => false,
												'bArisingToolbar' => false,
												'toolbarConfig' => array(
													'Bold', 'Italic', 'Underline', 'RemoveFormat',
													'CreateLink', 'DeleteLink', 'Image', 'Video',
													'BackColor', 'ForeColor',
													'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
													'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
													'StyleList', 'HeaderList',
													'FontList', 'FontSizeList',
												),
											));
											break;
										case "T":
                                            switch ($propertyID) {

                                                case "PREVIEW_TEXT":
                                                    echo '<label for="tourist-sname" style="text-align: left">Описание</label>';
                                                    $placeholder="";
                                                    $class="pass-firstname form-control input-md";
                                                    break;

                                            }
											for ($i = 0; $i<$inputNum; $i++)
											{

												if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
													$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
												elseif ($i == 0)
													$value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
												else
													$value = "";
											?>

											<textarea  class="form-control" cols="40" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]?>" style="resize: none" name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
											<?
											}
										break;

										case "S":
										case "N":
											for ($i = 0; $i<$inputNum; $i++)
											{
												if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
													$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
												elseif ($i == 0)
													$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
												else
													$value = "";
											?>
											<?
											if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"):?><br /><?
												$APPLICATION->IncludeComponent(
													'bitrix:main.calendar',
													'',
													array(
														'FORM_NAME' => 'iblock_add',
														'INPUT_NAME' => "PROPERTY[".$propertyID."][".$i."]",
														'INPUT_VALUE' => $value,
													),
													null,
													array('HIDE_ICONS' => 'Y')
												);
											endif;

                                            $hidden = "";
											switch ($propertyID) {
											case 755:
												echo '<label for="tourist-sname">Дата посещения</label>';
												$placeholder="Введите дату";
												$class="";
												break;
											case 756:
												echo '<label for="tourist-sname">Рейтинг размещения</label>';
												$placeholder="от 1 до 5";
												$class="pass-name form-control input-md";
												break;
											case 757:
												echo '<label for="tourist-sname">Рейтинг кухни</label>';
												$placeholder="от 1 до 5";
												$class="pass-series form-control input-md";
												break;
											case 758:
												echo '<label for="tourist-sname">Рейтинг сервиса</label>';
												$placeholder="от 1 до 5";
												$class="pass-number form-control input-md";
												break;
											case 759:
                                                $value = $_REQUEST['id'];
	                                            $hidden= "hidden";
                                                $class="";
                                                $placeholder="ID отеля";
												break;
                                            case 754:
                                                $value = $USER->GetFullName();
                                                $hidden= "hidden";
                                                $class="";
                                                $placeholder="Имя пользователя";
                                                break;
                                            case 752:
                                                $value = $USER->GetID();
                                                $hidden= "hidden";
                                                $class="";
                                                $placeholder="ID пользователя";
                                                break;
											default:
												$placeholder=$arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"];
												$class="pass-other";
											}?>
                                            <?if($propertyID=="755"):?>
                                                <div class="input-group date" id="">
                                                    <input type="text" placeholder="<?=$placeholder?>" class="familia form-control <?=$class?>" <?=$hidden?> id="PROPERTY_<?=$propertyID?>_<?=$i?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" value="<?=$value?>" /><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            <?else:?>
                                                <input type="text" placeholder="<?=$placeholder?>" class="familia <?=$class?>" <?=$hidden?> id="PROPERTY_<?=$propertyID?>_<?=$i?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" value="<?=$value?>" />
                                            <?endif;?>
											<?
											}
										break;
										case "F":
											for ($i = 0; $i<$inputNum; $i++)
											{
												$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
												?>
												<input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
												<input type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>" /><br />
												<?

												if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
												{
													?>
													<input type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyID?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label><br />
													<?

													if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"])
													{
														?>
														<img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="<?=$arResult["ELEMENT_FILES"][$value]["HEIGHT"]?>" width="<?=$arResult["ELEMENT_FILES"][$value]["WIDTH"]?>" border="0" /><br />
														<?
													}
													else
													{
														?>
														<?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
														<?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
														[<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
														<?
													}
												}
											}
										break;
										case "L":

											if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
												$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
											else
												$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

											switch ($type):
												case "checkbox":
												case "radio":

													//echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"][$propertyID]); echo "</pre>";

													foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
													{
														$checked = false;
														if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
														{
															if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))
															{
																foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum)
																{
																	if ($arElEnum["VALUE"] == $key) {$checked = true; break;}
																}
															}
														}
														else
														{
															if ($arEnum["DEF"] == "Y") $checked = true;
														}

														?>
														<input type="<?=$type?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label><br />
														<?
													}
												break;

												case "dropdown":
												case "multiselect":
												if($propertyID!="129"):?>
												<select name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
												<?
													if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
													else $sKey = "ELEMENT";

													foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
													{
														$checked = false;
														if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
														{
															foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
															{
																if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
															}
														}
														else
														{
															if ($arEnum["DEF"] == "Y") $checked = true;
														}
														?>
														<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
														<?
													}
												?>
												</select>
												<?
												else:?>
												<input type="hidden" name="PROPERTY[<?=$propertyID?>]" value="58"/>
												<?endif;
												break;

											endswitch;
										break;
									endswitch;?>
							<?endif;?>
							</div>
						<?endforeach;?>
						<?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
									<?=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?>
									<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
									<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
								<?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span>:
								<input type="text" name="captcha_word" maxlength="50" value="">
						<?endif?>
					<?endif?>	
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-primary" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
				</div>					
				<?
				if(strlen($arParams["LIST_URL"]) > 0 && $arParams["ID"] > 0):?><input type="button" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" /><?endif?>
			</form>
</div>


<script type="text/javascript">

</script>