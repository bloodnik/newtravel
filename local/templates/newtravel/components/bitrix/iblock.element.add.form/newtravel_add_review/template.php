<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>

<form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
	<? if (count($arResult["ERRORS"])): ?>
		<?=ShowError(implode("<br />", $arResult["ERRORS"]))?>
	<? endif ?>
	<? if (strlen($arResult["MESSAGE"]) > 0): ?>
		<br/>
		<?=ShowNote($arResult["MESSAGE"])?>
	<? endif ?>

	<?=bitrix_sessid_post()?>
	<? if ($arParams["MAX_FILE_SIZE"] > 0): ?>
		<input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>"/>
	<? endif ?>

	<div class="form-group" id="name-form">
		<label for="PROPERTY_NAME_0">Ваше имя *</label>
		<input type="text" id="PROPERTY_NAME_0" name="PROPERTY[NAME][0]" class="form-control" value="<?=$arResult['ELEMENT']['NAME']?>"/>
	</div>

	<div class="form-group" id="message-form">
		<label for="message">Ваш отзыв *</label>
		<textarea id="message" class="form-control" name="PROPERTY[PREVIEW_TEXT][0]"><?=$arResult['ELEMENT']['PREVIEW_TEXT']?></textarea>
	</div>

	<input type="hidden" id="PROPERTY_911_0" name="PROPERTY[911][0]" value="<?=$USER->GetID()?>"/>

	<? if ($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0): ?>
		<div class="formgroup">
			<label><?=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?> &nbsp;</label>
			<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>"/>
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA"/>
			<input type="text" class="form-control" name="captcha_word" maxlength="50" value="">
		</div>
	<? endif ?>
	<br>
	<input type="submit" name="iblock_submit" class="btn btn-blue" value="Отправить отзыв!"/>
	<br><br>
</form>