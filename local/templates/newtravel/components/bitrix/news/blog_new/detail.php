<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jquery-migrate.min.js");
?>

<section class="blog-section">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<? $ElementID = $APPLICATION->IncludeComponent(
					"bitrix:news.detail",
					"",
					Array(
						"DISPLAY_DATE"              => $arParams["DISPLAY_DATE"],
						"DISPLAY_NAME"              => $arParams["DISPLAY_NAME"],
						"DISPLAY_PICTURE"           => $arParams["DISPLAY_PICTURE"],
						"DISPLAY_PREVIEW_TEXT"      => $arParams["DISPLAY_PREVIEW_TEXT"],
						"IBLOCK_TYPE"               => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID"                 => $arParams["IBLOCK_ID"],
						"FIELD_CODE"                => $arParams["DETAIL_FIELD_CODE"],
						"PROPERTY_CODE"             => $arParams["DETAIL_PROPERTY_CODE"],
						"DETAIL_URL"                => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
						"SECTION_URL"               => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
						"META_KEYWORDS"             => $arParams["META_KEYWORDS"],
						"META_DESCRIPTION"          => $arParams["META_DESCRIPTION"],
						"BROWSER_TITLE"             => $arParams["BROWSER_TITLE"],
						"SET_CANONICAL_URL"         => $arParams["DETAIL_SET_CANONICAL_URL"],
						"DISPLAY_PANEL"             => $arParams["DISPLAY_PANEL"],
						"SET_LAST_MODIFIED"         => $arParams["SET_LAST_MODIFIED"],
						"SET_TITLE"                 => $arParams["SET_TITLE"],
						"MESSAGE_404"               => $arParams["MESSAGE_404"],
						"SET_STATUS_404"            => $arParams["SET_STATUS_404"],
						"SHOW_404"                  => $arParams["SHOW_404"],
						"FILE_404"                  => $arParams["FILE_404"],
						"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
						"ADD_SECTIONS_CHAIN"        => $arParams["ADD_SECTIONS_CHAIN"],
						"ACTIVE_DATE_FORMAT"        => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
						"CACHE_TYPE"                => $arParams["CACHE_TYPE"],
						"CACHE_TIME"                => $arParams["CACHE_TIME"],
						"CACHE_GROUPS"              => $arParams["CACHE_GROUPS"],
						"USE_PERMISSIONS"           => $arParams["USE_PERMISSIONS"],
						"GROUP_PERMISSIONS"         => $arParams["GROUP_PERMISSIONS"],
						"DISPLAY_TOP_PAGER"         => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
						"DISPLAY_BOTTOM_PAGER"      => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
						"PAGER_TITLE"               => $arParams["DETAIL_PAGER_TITLE"],
						"PAGER_SHOW_ALWAYS"         => "N",
						"PAGER_TEMPLATE"            => $arParams["DETAIL_PAGER_TEMPLATE"],
						"PAGER_SHOW_ALL"            => $arParams["DETAIL_PAGER_SHOW_ALL"],
						"CHECK_DATES"               => $arParams["CHECK_DATES"],
						"ELEMENT_ID"                => $arResult["VARIABLES"]["ELEMENT_ID"],
						"ELEMENT_CODE"              => $arResult["VARIABLES"]["ELEMENT_CODE"],
						"IBLOCK_URL"                => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
						"USE_SHARE"                 => $arParams["USE_SHARE"],
						"SHARE_HIDE"                => $arParams["SHARE_HIDE"],
						"SHARE_TEMPLATE"            => $arParams["SHARE_TEMPLATE"],
						"SHARE_HANDLERS"            => $arParams["SHARE_HANDLERS"],
						"SHARE_SHORTEN_URL_LOGIN"   => $arParams["SHARE_SHORTEN_URL_LOGIN"],
						"SHARE_SHORTEN_URL_KEY"     => $arParams["SHARE_SHORTEN_URL_KEY"],
						"ADD_ELEMENT_CHAIN"         => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : '')
					),
					$component
				); ?>

				<? $APPLICATION->IncludeFile(
					$APPLICATION->GetTemplatePath("include/managers.php"),
					Array(),
					Array("MODE" => "html", "SHOW_BORDER" => true)
				);
				?>
				<div class="post-comments">
					<? $APPLICATION->IncludeComponent(
						"cackle.comments",
						"",
						Array()
					); ?>
				</div>

			</div>

			<div class="col-md-3">
				<aside class="sidebar">
					<? $APPLICATION->ShowViewContent("blog_author"); ?>
					<div class="widget categories">
						<div class="widget-title">
							<h3>Категории</h3>
						</div>
						<div class="widget-containt">
							<ul>
								<? $APPLICATION->IncludeComponent( //Подключение списка категорий
									"bitrix:catalog.section.list",
									"blogs_category",
									Array(
										"VIEW_MODE"           => "LINE",
										"SHOW_PARENT_NAME"    => "Y",
										"IBLOCK_TYPE"         => $arParams['IBLOCK_TYPE'],
										"IBLOCK_ID"           => $arParams['IBLOCK_ID'],
										"SECTION_ID"          => $_REQUEST["SECTION_ID"],
										"SECTION_CODE"        => $_REQUEST["SECTION_CODE"],
										"SECTION_URL"         => "",
										"COUNT_ELEMENTS"      => "Y",
										"TOP_DEPTH"           => "1",
										"SECTION_FIELDS"      => array(),
										"SECTION_USER_FIELDS" => array(),
										"ADD_SECTIONS_CHAIN"  => "Y",
										"CACHE_TYPE"          => "A",
										"CACHE_TIME"          => "36000000",
										"CACHE_GROUPS"        => "Y"
									),
									$component
								);
								?>
							</ul>
						</div>
					</div>

					<div class="text-widget categories" style="overflow: hiddden">
						<div class="widget-title">
							<h3 id="hot">Поиск туров</h3>
						</div>
						<div class="col-xs-12 col-sm-12" style="margin-bottom: 20px">
							<div class="row">
								<? $APPLICATION->IncludeComponent(
									"newtravel.search:simple.search",
									"",
									Array(
										"ADULTS"               => "2",
										"CACHE_TIME"           => "3600",
										"CACHE_TYPE"           => "A",
										"CITY"                 => "4",
										"COMPOSITE_FRAME_MODE" => "A",
										"COMPOSITE_FRAME_TYPE" => "AUTO",
										"COUNTRY"              => '4',
										"DATE_TO"              => "14",
										"NIGHTSFROM"           => "6",
										"NIGHTSTO"             => "14",
										"STARS"                => "1"
									)
								); ?>
							</div>
						</div>
					</div>


					<div class="widget tags">
						<div class="widget-title">
							<h3>Тэги</h3>
						</div>
						<div class="widget-containt">
							<ul>
								<? $GLOBALS["arrFILTER"] = array("PARAM2" => 75) ?>
								<? $APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "", Array( //Подключение облака тегов
									"FONT_MAX"        => "50",
									"FONT_MIN"        => "10",
									"COLOR_NEW"       => "3E74E6",
									"COLOR_OLD"       => "C0C0C0",
									"PERIOD_NEW_TAGS" => "",
									"SHOW_CHAIN"      => "Y",
									"COLOR_TYPE"      => "Y",
									"WIDTH"           => "100%",
									"SORT"            => "CNT",
									"PAGE_ELEMENTS"   => "15",
									"PERIOD"          => "",
									"URL_SEARCH"      => "/search/index.php",
									"TAGS_INHERIT"    => "Y",
									"CHECK_DATES"     => "Y",
									"FILTER_NAME"     => "arrFILTER",
									"arrFILTER"       => "iblock",
									"CACHE_TYPE"      => "A",
									"CACHE_TIME"      => "360000"
								),
									$component
								); ?>
							</ul>
						</div>
					</div>
					<div class="widget archive">
						<div class="widget-title">
							<h3>Заявка на подбор тура</h3>
						</div>
						<div class="widget-containt">
							<div class="text-center">
								<button class="btn btn-blue" id="helpForm" style="margin-top: 20px" data-toggle="modal" data-target="#helpFormModal">Помощь в подборе тура</button>
							</div>
						</div>
					</div>
					<div class="widget recent-posts">
						<div class="widget-title">
							<h3>Популярные записи</h3>
						</div>
						<div class="widget-containt">
							<? $APPLICATION->IncludeComponent( //Подключение популярных постов
								"bitrix:news.list",
								"popular_posts",
								Array(
									"DISPLAY_DATE"                    => "Y",
									"DISPLAY_NAME"                    => "Y",
									"DISPLAY_PICTURE"                 => "Y",
									"DISPLAY_PREVIEW_TEXT"            => "N",
									"AJAX_MODE"                       => "N",
									"IBLOCK_TYPE"                     => $arParams['IBLOCK_TYPE'],
									"IBLOCK_ID"                       => $arParams['IBLOCK_ID'],
									"NEWS_COUNT"                      => "5",
									"SORT_BY1"                        => "SHOW_COUNTER",
									"SORT_ORDER1"                     => "DESC",
									"SORT_BY2"                        => "DATE_ACTIVE",
									"SORT_ORDER2"                     => "ASC",
									"FILTER_NAME"                     => "",
									"FIELD_CODE"                      => array(
										0 => "SHOW_COUNTER",
										2 => " SHOW_COUNTER_START"
									),
									"PROPERTY_CODE"                   => array(),
									"CHECK_DATES"                     => "Y",
									"DETAIL_URL"                      => "",
									"PREVIEW_TRUNCATE_LEN"            => "",
									"ACTIVE_DATE_FORMAT"              => "d.m.Y",
									"SET_STATUS_404"                  => "N",
									"SET_TITLE"                       => "N",
									"INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
									"ADD_SECTIONS_CHAIN"              => "N",
									"HIDE_LINK_WHEN_NO_DETAIL"        => "N",
									"PARENT_SECTION"                  => "",
									"PARENT_SECTION_CODE"             => "",
									"INCLUDE_SUBSECTIONS"             => "N",
									"CACHE_TYPE"                      => "A",
									"CACHE_TIME"                      => "36000000",
									"CACHE_FILTER"                    => "N",
									"CACHE_GROUPS"                    => "Y",
									"PAGER_TEMPLATE"                  => ".default",
									"DISPLAY_TOP_PAGER"               => "N",
									"DISPLAY_BOTTOM_PAGER"            => "N",
									"PAGER_TITLE"                     => "Новости",
									"PAGER_SHOW_ALWAYS"               => "N",
									"PAGER_DESC_NUMBERING"            => "N",
									"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
									"PAGER_SHOW_ALL"                  => "N",
									"AJAX_OPTION_JUMP"                => "N",
									"AJAX_OPTION_STYLE"               => "Y",
									"AJAX_OPTION_HISTORY"             => "N"
								),
								$component
							); ?>
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>

<!-----------------------------------------------------------------------------------------------------
	Модальное окно формы подбора тура
 ------------------------------------------------------------------------------------------------------>
<div class="modal fade" id="helpFormModal" tabindex="-1" role="dialog" aria-labelledby="helpFormModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Помощь в подборе тура</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<? $APPLICATION->IncludeComponent(
						"bitrix:form.result.new",
						"tour_help_sidebar",
						Array(
							"AJAX_MODE"              => "Y",
							"SEF_MODE"               => "N",
							"WEB_FORM_ID"            => "6",
							"LIST_URL"               => "",
							"EDIT_URL"               => "",
							"SUCCESS_URL"            => "",
							"CHAIN_ITEM_TEXT"        => "",
							"CHAIN_ITEM_LINK"        => "",
							"IGNORE_CUSTOM_TEMPLATE" => "N",
							"USE_EXTENDED_ERRORS"    => "N",
							"CACHE_TYPE"             => "A",
							"CACHE_TIME"             => "36000",
							"VARIABLE_ALIASES"       => Array(
								"WEB_FORM_ID" => "WEB_FORM_ID",
								"RESULT_ID"   => "RESULT_ID"
							),
						),
						false
					); ?>
				</div>
			</div>

		</div>
	</div>
</div>

<div class="fixed-sidebar1" style="display: none;">
	<div class="card-wrap">
		<div class="card">
			<a href="javascript:void(0)"><img src="//static.tourvisor.ru/hotel_pics/big/2/fisherman-harbour-luxury-hotel-spa-0.jpg" class="card-img-top" style="height: 170px;"></a>
			<div class="card-body" style="height: 300px;"><h5 class="card-title"><a href="javascript:void(0)">FISHERMAN HARBOUR LUXURY HOTEL &amp; SPA 5*</a></h5>
				<small class="text-muted">Пхукет, Таиланд</small>
				<p class="card-text">
					07.11.2017,
					<span>2 взр. <!----></span>,
					13 ночей, DBL, Завтрак, Anex
				</p>
				<div class="card-text"><p>без скидки: <span class="text-muted display-8"><s>106 107 Р<!----></s></span></p>
					Цена:
					<a href="#" class="h4 text-success">
						95 592 Р<!----></a></div>
				<p class="card-text">
					<small class="text-muted">Обновлено 5 часов назад</small>
				</p>
			</div>
			<div class="card-footer"><a href="#" class="btn btn-primary btn-block ld-over detail-btn_139007684042">
					Купить
					<div class="ld ld-ring ld-spin"></div>
				</a></div>
		</div>

		<br>

		<div class="card"><a href="javascript:void(0)"><img src="//static.tourvisor.ru/hotel_pics/big/2/fisherman-harbour-luxury-hotel-spa-0.jpg" class="card-img-top" style="height: 170px;"></a>
			<div class="card-body" style="height: 300px;"><h5 class="card-title"><a href="javascript:void(0)">FISHERMAN HARBOUR LUXURY HOTEL &amp; SPA 5*</a></h5>
				<small class="text-muted">Пхукет, Таиланд</small>
				<p class="card-text">
					07.11.2017,
					<span>2 взр. <!----></span>,
					13 ночей, DBL, Завтрак, Anex
				</p>
				<div class="card-text"><p>без скидки: <span class="text-muted display-8"><s>106 107 Р<!----></s></span></p>
					Цена:
					<a href="#" class="h4 text-success">
						95 592 Р<!----></a></div>
				<p class="card-text">
					<small class="text-muted">Обновлено 5 часов назад</small>
				</p>
			</div>
			<div class="card-footer"><a href="#" class="btn btn-primary btn-block ld-over detail-btn_139007684042">
					Купить
					<div class="ld ld-ring ld-spin"></div>
				</a></div>
		</div>

	</div>
	<button class="sidebar-open-btn" onclick="$('.fixed-sidebar').toggleClass('open')"><i class="fa fa-fire"></i></button>
</div>

