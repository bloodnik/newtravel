<?
$COUNTRY_ID = ! empty($arResult['PROPERTIES']['TV_COUNTRY_ID']['VALUE']) ? $arResult['PROPERTIES']['TV_COUNTRY_ID']['VALUE'] : "";

//Получам цены на отели
global $APPLICATION;

$USER_CITY = $APPLICATION->get_cookie('USER_CITY');
$USER_CITY = isset($USER_CITY) && ! empty($USER_CITY) ? $USER_CITY : $_SESSION['CITY']['UF_XML_ID'];


\Bitrix\Main\Page\Asset::getInstance()->addString('<link href="https://' . SITE_SERVER_NAME . $arResult['DETAIL_PAGE_URL'] . '" rel="canonical" />', true);
?>

<script>
    //Загрузить цены на отели
    function loadPrices() {
        yaCounter24395911.reachGoal('LOAD_PRICE_BTN');

        $('.loadPriceBtn').attr('disabled', 'disabled');
        $('.hotel_price').remove();
        var hotelLinks = $('.news-detail-text a[href^="/hotel/"]');
        $.each(hotelLinks, function (key, value) {
            var arHref = $(value).attr('href').split('/'),
                countryId = "<?=$COUNTRY_ID?>";

            if (arHref[1] == 'hotel') {
                var hotelId = arHref[2];

                //крутилка (лоадер)
                $(value).after('<span class="price_loader"><br><i class="fa fa-spinner fa-pulse fa-fw"></i> обновляем цены</span>');

                //параметры запроса
                var request_params = {
                    type: "search",
                    departure: <?=$USER_CITY ?: "4"?>,
                    country: countryId,
                    hotels: hotelId,
                    nightsfrom: $('#nightsfrom_blog').val(),
                    nightsto: $('#nightsto_blog').val(),
                    datefrom: $('#datefrom_blog').val(),
                    dateto: $('#dateto_blog').val(),
                    adults: $('#adults_blog').val(),
                    child: $('#child_blog').val(),
                    childage1: $('#childage1_blog').val(),
                    childage2: $('#childage2_blog').val(),
                    childage3: $('#childage3_blog').val(),
                };

                //Добавляем параметры дат вылета в url
                var hotelLink = new Url($(value).attr('href'));
                hotelLink.query.datefrom = request_params.datefrom;
                hotelLink.query.dateto = request_params.dateto;
                hotelLink.query.nightsfrom = request_params.nightsfrom;
                hotelLink.query.nightsto = request_params.nightsto;
                hotelLink.query.adults = request_params.adults;
                hotelLink.query.child = request_params.child;
                hotelLink.query.childage1 = request_params.childage1;
                hotelLink.query.childage2 = request_params.childage2;
                hotelLink.query.childage3 = request_params.childage3;
                $(value).attr('href', hotelLink.path.toString() + "?start=Y&" + hotelLink.query.toString());

                $.getJSON("/local/tools/newtravel.search.ajax.php", request_params, function (json) {
                    var requestid = json.result.requestid;
                    checkTourStatus(requestid, $(value).attr('href'));
                });


            }
        });

        //Если есть ссылка на сравнение, тогда подменяем там даты вылета
        var compareLink = $('.news-detail-text a[href*="/tours/?start=Y"]'),
            compareUrl = new Url($(compareLink[0]).attr('href'));

        compareUrl.query.datefrom = $('#datefrom_blog').val();
        compareUrl.query.dateto = $('#dateto_blog').val();
        compareUrl.query.nightsfrom = $('#nightsfrom_blog').val();
        compareUrl.query.nightsto = $('#nightsto_blog').val();
        compareUrl.query.adults = $('#adults_blog').val();
        compareUrl.query.child = $('#child_blog').val();
        compareUrl.query.childage1 = $('#childage1_blog').val();
        compareUrl.query.childage2 = $('#childage2_blog').val();
        compareUrl.query.childage3 = $('#childage3_blog').val();
        delete compareUrl.query["departure"];
        $(compareLink[0]).attr('href', compareUrl);


    }
    //Проверяем статус запроса
    function checkTourStatus(requestid, href) {
        $.getJSON("/local/tools/newtravel.search.ajax.php", {type: "status", requestid: requestid}, function (json) {
            if (json.data.status.state != "finished" && json.data.status.timepassed < "45") {
                setTimeout(function () {
                    checkTourStatus(requestid, href);
                }, 3000);

            } else {
                //Количество туристов
                var tourists = $('#adults_blog').val() + ' взр.';
                if ($('#child_blog').val() > 0) {
                    tourists += " + " + $('#child_blog').val() + " реб."
                }
                tourists += " на " + $('#nightsfrom_blog').val() + "-" + $('#nightsto_blog').val() + " нч."; //Строка количество туристов, ночей

                //Убираем крутлилку
                $('a[href="' + href + '"]').parent().find('.price_loader').remove();
                $('.loadPriceBtn').attr('disabled', false);

                if (json.data.status.minprice != "") { //стоимость определена
                    $('a[href="' + href + '"]').after("<div><a href='" + href + "' target='_blank' rel='nofollow' class='hotel_price'><br> цена за тур от " + parseInt(json.data.status.minprice).toLocaleString() + " руб.</a><a href='" + href + "' " +
	                    "target='_blank' rel='nofollow' class='hotel_price btn btn-blue btn-sm'>Заказать</a></div>");
                } else { //стоимость не найдена
                    $('a[href="' + href + '"]').after("<span class='hotel_price'><br> нет вылетов в заданный период</span>");
                }

            }
        })
    }
</script>