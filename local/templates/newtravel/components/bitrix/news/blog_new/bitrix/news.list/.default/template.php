<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/plugins/jquery.isotope.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jquery-migrate.min.js");

use Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();
?>

<div id="blog-normal" class="blog-normal">
	<? $count = 1 ?>
	<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<article id="<?=$this->GetEditAreaId($arItem['ID']);?>" itemscope itemtype="http://schema.org/Article">
			<div class="news-item">
				<div class="post-wrapper standard-post">
					<? if (is_array($arItem["PREVIEW_IMG"])): ?>
						<div class="post-thumbnail" itemtype="http://schema.org/ImageObject">
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem["NAME"]?>" itemprop="url">
								<img src="<?=$arItem["PREVIEW_IMG"]["SRC"]?>" alt="<?=$arItem["NAME"]?>" title="Умныетуристы <?=$arItem["NAME"]?>"/>
								<meta itemprop="image" content="<?=$arItem["PREVIEW_IMG"]["SRC"]?>" />
							</a>
						</div>
					<? else: ?>
						<div class="post-thumbnail">
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem["NAME"]?>">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/no_blog_image.jpg" alt="<?=$arItem["NAME"]?>" title="Умныетуристы <?=$arItem["NAME"]?>"/>
							</a>
						</div>
					<? endif; ?>
					<h3 class="post-title">
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem["NAME"]?>"><span itemprop="name"><?=$arItem["NAME"]?></span></a>
					</h3>
					<div class="post-containt" itemprop="articleBody">
						<p> <? if ($arItem["PREVIEW_TEXT"]) {
								echo truncateText(strip_tags($arItem["PREVIEW_TEXT"]), 100);
							} elseif ($arItem["DETAIL_TEXT"]) {
								echo truncateText(strip_tags($arItem["DETAIL_TEXT"]), 100);
							} ?></p>
					</div>
					<div class="post-meta">
						<? if ($arItem["DISPLAY_PROPERTIES"]["POST_AUTHOR"]): ?>
							<span class="post-author" itemprop="author" itemscope itemtype="http://schema.org/Person">
							<a href="/blog/?&arrFilter_pf[POST_AUTHOR]=<?=$arItem["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["ID"]?>&set_filter=Фильтр&set_filter=Y"
							   rel="nofollow"
							   title="Автор">
								<span itemprop="name"><?=$arItem["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["NAME"]?> <?=$arItem["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["LAST_NAME"]?></span>
							</a>
						</span>
						<? endif; ?>
						<? if ($arItem['DISPLAY_ACTIVE_FROM']): ?>
							<span itemprop="datePublished" content="<?=FormatDate("Y-m-d", MakeTimeStamp($arItem["DISPLAY_ACTIVE_FROM"]))?>" class="post-date hidden"><?=$arItem['DISPLAY_ACTIVE_FROM']?></span>
						<? endif; ?>
					</div>
				</div>
			</div>
		</article>
	<? endforeach; ?>
</div>

<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
	<div class="nav-string">
		<?=$arResult["NAV_STRING"]?>
	</div>
<? endif; ?>

<script type="text/javascript">
	jQuery(window).load(function () {
		'use strict';
		var $blogitems, $blogtlitems;

		$('.loading').animate({
			opacity: 0
		}, 1000);
		$('.loading').hide(1500);

		$('.blog-normal').animate({
			opacity: 1
		}, 2500);
		function blog_post_item() {
			$blogitems = $('#blog-normal');
			$blogitems.isotope({
				transformsEnabled: false,
				itemSelector: 'article',
				animationEngine: 'jquery',
				animationOptions: {
					duration: 0,
					easing: 'linear',
					queue: false
				}
			});
		}

		function blog_post_direction() {
			$('#blog-normal article').each(function () {
				var posLeft = $(this).css('left');
				if (posLeft === '0px') {
					$(this).removeClass('right-post');
					$(this).addClass('left-post');
				} else {
					$(this).removeClass('left-post');
					$(this).addClass('right-post');
				}
			});
		}

		//blog_post_direction();
		//blog_post_item();
	});

	/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
	var disqus_shortname = 'newtravel'; // required: replace example with your forum shortname

	/* * * DON'T EDIT BELOW THIS LINE * * */
	(function () {
		var s = document.createElement('script');
		s.async = true;
		s.type = 'text/javascript';
		s.src = '//' + disqus_shortname + '.disqus.com/count.js';
		(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
	}());
</script>
