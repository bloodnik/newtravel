<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<ul>
	<? foreach ($arResult["ITEMS"] as $arItem): ?>
		<li class="col-md-6 col-xs-12">
			<div class="post-thumbnail">
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<? echo $arItem["NAME"] ?>">
					<? if (is_array($arItem["PREVIEW_PICTURE"])): ?>
						<img src="<?=$arItem['PREVIEW_IMG']['src']?>" alt="<? echo $arItem["NAME"] ?>"/>
					<? else: ?>
						<img src="<?=SITE_TEMPLATE_PATH?>/images/no-avatar.gif" alt="<? echo $arItem["NAME"] ?>"/>
					<? endif ?>
				</a>
			</div>
			<div class="post-containt">
				<div class="post-title">
					<h5>
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<? echo $arItem["NAME"] ?>">
							<? echo $arItem["NAME"] ?>
						</a>
					</h5>
				</div>
				<div class="post-date">
					<h6>
						<? echo $arItem["DISPLAY_ACTIVE_FROM"] ?>
					</h6>
				</div>
			</div>
		</li>
	<? endforeach ?>
</ul>
