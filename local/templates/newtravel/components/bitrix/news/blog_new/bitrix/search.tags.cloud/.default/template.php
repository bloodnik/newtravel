<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if ($arParams["SHOW_CHAIN"] != "N" && ! empty($arResult["TAGS_CHAIN"])):
	?>
	<!--noindex -->
		<div class="search-tags-chain" <?=$arParams["WIDTH"]?>><?
			foreach ($arResult["TAGS_CHAIN"] as $tags):?>
				<li><a href="<?=$tags["TAG_PATH"]?>" rel="nofollow"><?=$tags["TAG_NAME"]?></a>
					[<a href="<?=$tags["TAG_WITHOUT"]?>" class="search-tags-link" rel="nofollow">x</a>]
				</li>
			<?endforeach; ?>
		</div>
	<!--/noindex-->
	<?
endif;

if (is_array($arResult["SEARCH"]) && ! empty($arResult["SEARCH"])):
	?>
	<!--noindex-->
		<?
		foreach ($arResult["SEARCH"] as $key => $res) {
			?>
			<li><a href="<?=$res["URL"]?>" rel="nofollow" title="Тэг"><?=$res["NAME"]?></a></li><?
		}
		?>
	<!--/noindex-->
	<?
endif;
?>