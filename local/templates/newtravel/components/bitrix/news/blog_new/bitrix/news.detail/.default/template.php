<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


if ($arResult['DISPLAY_PROPERTIES']['LOAD_PRICE_BTN'] && $arResult['DISPLAY_PROPERTIES']['TV_COUNTRY_ID']):
	$this->addExternalCss(SITE_TEMPLATE_PATH . "/js/daterangepicker2/daterangepicker.min.css");

	$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/daterangepicker2/jquery.daterangepicker.min.js");
	$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/url.min.js");

	//Выбор ночей
	$this->addExternalCss("/local/modules/newtravel.search/js/jquery.nights.select/jquery.nights.select.min.css");
	$this->addExternalJs("/local/modules/newtravel.search/js/jquery.nights.select/jquery.nights.select.min.js");

	//Выбор туристов
	$this->addExternalCss("/local/modules/newtravel.search/js/jquery.tourists.select/jquery.tourists.select.min.css");
	$this->addExternalJs("/local/modules/newtravel.search/js/jquery.tourists.select/jquery.tourists.select.min.js");
endif;

//$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jquery-migrate.min.js");
?>
	<!--suppress ALL -->
	<div class="single-post">
		<article itemscope itemtype="http://schema.org/Article">
			<div class="post-wrapper standard-post">
				<div class="post-thumbnail" itemtype="http://schema.org/ImageObject">
					<? if (is_array($arResult["PREVIEW_IMG"])): ?>
						<meta itemprop="image" content="<?=$arResult["PREVIEW_IMG"]["SRC"]?>"/>
						<img src="<?=$arResult["PREVIEW_IMG"]["SRC"]?>" alt="<?=$arResult["NAME"]?>"
						     title="<?=$arResult["NAME"]?>"/>
					<? else: ?>
						<img src="<?=SITE_TEMPLATE_PATH?>/images/no_blog_image.jpg" width="<?=$arResult["PREVIEW_IMG"]["WIDTH"]?>" height="<?=$arResult["PREVIEW_IMG"]["HEIGHT"]?>"
						     alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>"/>
					<? endif ?>
				</div>
				<div class="post-title col-xs-12">
					<h3 itemprop="name"><?=$arResult["NAME"]?></h3>
				</div>
				<div class="post-containt" itemprop="articleBody">

					<? if ($arResult['DISPLAY_PROPERTIES']['LOAD_PRICE_BTN'] && $arResult['DISPLAY_PROPERTIES']['TV_COUNTRY_ID']): ?>
						<div class="load-price-wrap row" id="load-price">
							<div class="col-md-3">
								<label>Выберите дату поездки</label>
								<div class="from-group">
									<input type="text" class="form-control" id="daterange_blog" name="daterange_blog" value="<?=$arResult['DATEFROM'] . '-' . $arResult['DATETO']?>"/>
									<input type="hidden" id="datefrom_blog" name="datefrom" value="<?=$arResult['DATEFROM']?>"/>
									<input type="hidden" id="dateto_blog" name="dateto" value="<?=$arResult['DATETO']?>"/>
								</div>
							</div>
							<div class="col-md-3">
								<label>Ночей: </label>
								<div class="from-group">
									<div class="form-control pseudo-input" id="nightsValueBlog">от 6 до 14</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="toursit-select from-group">
									<label>Туристы: </label>
									<div class="form-control tourists-total__blog">
										2 взр.
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<label>&nbsp;</label>
								<div class="from-group">
									<button class="loadPriceBtn btn btn-blue" onclick="loadPrices()">Загрузить цены</button>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					<? endif; ?>

					<? if ($arResult["DETAIL_TEXT"]): ?>
						<div class="news-detail-text"><?=$arResult["DETAIL_TEXT"]?>    </div>
					<? elseif ($arResult["PREVIEW_TEXT"]): ?>
						<div class="news-detail-text"><?=$arResult["PREVIEW_TEXT"]?>    </div>
					<? endif ?>

					<? foreach ($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"] as $photo): //Галерея?>

						<ul class="resort-gallery my-work">
							<li>
								<div class="img-figure-overlayer">
									<a class="fancybox" rel="nofollow" data-fancybox-group="gallery3" href="<?=$photo?>" title="<?=$arResult['NAME']?>">
										<i class="fa fa-search"></i>
									</a>
								</div>
								<div class="img-figure">
									<img src="<?=$photo?>" alt="">
								</div>
							</li>
						</ul>
					<? endforeach; ?>
				</div>

				<div class="row">
					<div class="col-md-12">
						<a onclick='$("html, body").animate({scrollTop: $("#load-price").offset().top - 300}, 2000);' class="btn btn-blue">Поменять параметры поиска</a>
						<a class="btn btn-green" href="#callback">Заказать звонок</a>
						<a class="btn btn-green" href="#pickmetour">Заказать подборку</a>
					</div>
				</div>

				<div class="post-meta">
					<? if ($arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]): ?>
						<span class="post-author">
							<a href="/blog/?&arrFilter_pf[POST_AUTHOR]=<?=$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["ID"]?>&set_filter=Фильтр&set_filter=Y"
							   title="Автор"><?=$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["NAME"]?> <?=$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["LAST_NAME"]?></a>
						</span>
					<? endif; ?>
					<span class="post-date" itemprop="datePublished" content="<?=FormatDate("Y-m-d", MakeTimeStamp($arResult["DISPLAY_ACTIVE_FROM"]))?>"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
				</div>
			</div>
			<? if ( ! empty($arResult["TAGS_ARRAY"]) && $arResult["TAGS_ARRAY"][0] !== ""): ?>
				<div class="post-tags">
					<noindex>
						<ul>
							<? foreach ($arResult["TAGS_ARRAY"] as $tag): ?>
								<li><a href="/search/index.php?tags=<?=$tag?>" rel="nofollow" target="_blank" title="tag"><?=$tag?></a></li>
							<? endforeach; ?>
						</ul>
					</noindex>
				</div>
			<? endif ?>
		</article>

		<script type="text/javascript">(function () {
                if (window.pluso)if (typeof window.pluso.start == "function") return;
                if (window.ifpluso == undefined) {
                    window.ifpluso = 1;
                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript';
                    s.charset = 'UTF-8';
                    s.async = true;
                    s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                    var h = d[g]('body')[0];
                    h.appendChild(s);
                }
            })();</script>
		<div class="pluso" style="padding: 12px;" data-background="transparent" data-options="medium,square,line,horizontal,counter,theme=05"
		     data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print"></div>


		<? if (isset($arResult["DISPLAY_PROPERTIES"]['POST_AUTHOR'])): //Отложенный ?>
			<? $this->SetViewTarget("blog_author"); ?>
			<div class="widget " itemprop="author" itemscope itemtype="http://schema.org/Person">
				<div class="widget-title">
					<h3>Автор поста</h3>
				</div>
				<div class="author-box-img">
					<? if (isset($arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]['PERSONAL_PHOTO'])): ?>
						<div style="height: 67px; overflow: hidden">
							<img src="<?=CFile::GetPath($arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["PERSONAL_PHOTO"])?>" alt="Автор"/>
						</div>
					<? else: ?>
						<img src="<?=SITE_TEMPLATE_PATH?>/images/no-avatar.gif" alt="Автор"/>
					<? endif; ?>
				</div>
				<div class="author-box-title">
					<h3 itemprop="name">
						<a href="/blog/?&arrFilter_pf[POST_AUTHOR]=<?=$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["ID"]?>&set_filter=Фильтр&set_filter=Y"
						   title="Автор"><?=$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["NAME"]?> <?=$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["LAST_NAME"]?></a>
					</h3>
				</div>
				<div class="author-box-content">
					<? if (strlen($arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["PERSONAL_PROFESSION"]) > 0): ?>
						<p><?=$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["PERSONAL_PROFESSION"]?></p>
					<? else: ?>
						<p>------------</p>
					<? endif; ?>

				</div>
				<div class="clearfix"></div>
			</div>
			<? $this->EndViewTarget(); ?>
		<? endif; ?>
	</div>

<? if ($arResult['DISPLAY_PROPERTIES']['LOAD_PRICE_BTN'] && $arResult['DISPLAY_PROPERTIES']['TV_COUNTRY_ID']): ?>
	<script>
        $(document).ready(function () {
            /**
             * Выбор диапазона дат
             */
            $('input[name=daterange_blog]').dateRangePicker({
                format: 'DD.MM.YYYY',
                startOfWeek: 'monday',
                singleMonth: 'auto',
                showShortcuts: false,
                autoClose: true,
                selectForward: true,
                stickyMonths: true,
                separator: '-'
            }).bind('datepicker-change', function (event, obj) {
                $("#datefrom_blog").val(moment(obj.date1).format("DD.MM.YYYY"));
                $("#dateto_blog").val(moment(obj.date2).format("DD.MM.YYYY"));
                $("#daterange_blog").val($("#datefrom_blog").val() + "-" + $("#dateto_blog").val());
            });


            $('#nightsValueBlog').nightsSelect({
                nfrom_id: "nightsfrom_blog",
                nto_id: "nightsto_blog",
                defaultFrom: 6,
                defaultTo: 14
            }).bind('nightSelect-change', function (event, obj) {
                $('#nightsfrom_blog').val(obj.from);
                $('#nightsto_blog').val(obj.to);
            });

            //===============================Выбор туристов===============================================//
            $('.tourists-total__blog').touristSelect({
                adults: 2,
                child: 0,
                childage1: "",
                childage2: "",
                childage3: "",
                adults_id: "adults_blog",
                child_id: "child_blog",
                childage1_id: "childage1_blog",
                childage2_id: "childage2_blog",
                childage3_id: "childage3_blog",
            }).bind('touristsSelect-change', function (event, obj) {
                $('#adults_blog').val(obj.adults);
                $('#child_blog').val(obj.child);
                $('#childage1_blog').val(obj.childage1);
                $('#childage2_blog').val(obj.childage2);
                $('#childage3_blog').val(obj.childage3);
            });

        });

	</script>
<? endif ?>