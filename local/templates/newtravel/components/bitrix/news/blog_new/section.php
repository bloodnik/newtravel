<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?
if(isset($_REQUEST["post_author"])) {
	$GLOBALS["arrFilter"] = array("PROPERTY_POST_AUTHOR" => $_REQUEST["post_author"]);
}
?>
<?if($arParams["USE_FILTER"]=="Y"):?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.filter",
		"",
		Array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"FILTER_NAME" => $arParams["FILTER_NAME"],
			"FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
			"PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		),
		$component
	);
	?>
	<br />
<?endif?>

<section class="blog-section">
	<div class="container">
		<div class="row">
			<div class="col-md-9" style="overflow: hidden;">
				<?$APPLICATION->IncludeComponent(
					"bitrix:news.list",
					"",
					Array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"NEWS_COUNT" => $arParams["NEWS_COUNT"],
						"SORT_BY1" => $arParams["SORT_BY1"],
						"SORT_ORDER1" => $arParams["SORT_ORDER1"],
						"SORT_BY2" => $arParams["SORT_BY2"],
						"SORT_ORDER2" => $arParams["SORT_ORDER2"],
						"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
						"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
						"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
						"SET_TITLE" => $arParams["SET_TITLE"],
						"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
						"MESSAGE_404" => $arParams["MESSAGE_404"],
						"SET_STATUS_404" => $arParams["SET_STATUS_404"],
						"SHOW_404" => $arParams["SHOW_404"],
						"FILE_404" => $arParams["FILE_404"],
						"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
						"ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_FILTER" => $arParams["CACHE_FILTER"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
						"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
						"PAGER_TITLE" => $arParams["PAGER_TITLE"],
						"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
						"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
						"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
						"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
						"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
						"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
						"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
						"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
						"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
						"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
						"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
						"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
						"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
						"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
						"FILTER_NAME" => $arParams["FILTER_NAME"],
						"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
						"CHECK_DATES" => $arParams["CHECK_DATES"],

						"PARENT_SECTION" => $arResult["VARIABLES"]["SECTION_ID"],
						"PARENT_SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
						"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
						"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
						"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
					),
					$component
				);?>
			</div>

			<div class="col-md-3">
				<aside class="sidebar">
					<div class="widget categories">
						<div class="widget-title">
							<h3>Категории</h3>
						</div>
						<div class="widget-containt">
							<ul>
								<? $APPLICATION->IncludeComponent( //Подключение списка категорий
									"bitrix:catalog.section.list",
									"blogs_category",
									Array(
										"VIEW_MODE"           => "LINE",
										"SHOW_PARENT_NAME"    => "Y",
										"IBLOCK_TYPE"         => $arParams['IBLOCK_TYPE'],
										"IBLOCK_ID"           => $arParams['IBLOCK_ID'],
										"SECTION_ID"          => $_REQUEST["SECTION_ID"],
										"SECTION_CODE"        => $_REQUEST["SECTION_CODE"],
										"SECTION_URL"         => "",
										"COUNT_ELEMENTS"      => "Y",
										"TOP_DEPTH"           => "1",
										"SECTION_FIELDS"      => array(),
										"SECTION_USER_FIELDS" => array(),
										"ADD_SECTIONS_CHAIN"  => "Y",
										"CACHE_TYPE"          => "A",
										"CACHE_TIME"          => "36000000",
										"CACHE_GROUPS"        => "Y"
									),
									$component
								);
								?>
							</ul>
						</div>
					</div>
					<div class="widget recent-posts">
						<div class="widget-title">
							<h3>Популярные записи</h3>
						</div>
						<div class="widget-containt">
							<? $APPLICATION->IncludeComponent( //Подключение популярных постов
								"bitrix:news.list",
								"popular_posts",
								Array(
									"DISPLAY_DATE"                    => "Y",
									"DISPLAY_NAME"                    => "Y",
									"DISPLAY_PICTURE"                 => "Y",
									"DISPLAY_PREVIEW_TEXT"            => "N",
									"AJAX_MODE"                       => "N",
									"IBLOCK_TYPE"                     => $arParams['IBLOCK_TYPE'],
									"IBLOCK_ID"                       => $arParams['IBLOCK_ID'],
									"NEWS_COUNT"                      => "7",
									"SORT_BY1"                        => "PROPERTY_POPULAR",
									"SORT_ORDER1"                     => "DESC",
									"SORT_BY2"                        => "SORT",
									"SORT_ORDER2"                     => "ASC",
									"FILTER_NAME"                     => "",
									"FIELD_CODE"                      => array(0 => "SORT"),
									"PROPERTY_CODE"                   => array(),
									"CHECK_DATES"                     => "Y",
									"DETAIL_URL"                      => "",
									"PREVIEW_TRUNCATE_LEN"            => "",
									"ACTIVE_DATE_FORMAT"              => "d.m.Y",
									"SET_STATUS_404"                  => "N",
									"SET_TITLE"                       => "N",
									"INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
									"ADD_SECTIONS_CHAIN"              => "N",
									"HIDE_LINK_WHEN_NO_DETAIL"        => "N",
									"PARENT_SECTION"                  => "",
									"PARENT_SECTION_CODE"             => "",
									"INCLUDE_SUBSECTIONS"             => "N",
									"CACHE_TYPE"                      => "A",
									"CACHE_TIME"                      => "36000000",
									"CACHE_FILTER"                    => "N",
									"CACHE_GROUPS"                    => "Y",
									"PAGER_TEMPLATE"                  => ".default",
									"DISPLAY_TOP_PAGER"               => "N",
									"DISPLAY_BOTTOM_PAGER"            => "N",
									"PAGER_TITLE"                     => "Новости",
									"PAGER_SHOW_ALWAYS"               => "N",
									"PAGER_DESC_NUMBERING"            => "N",
									"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
									"PAGER_SHOW_ALL"                  => "N",
									"AJAX_OPTION_JUMP"                => "N",
									"AJAX_OPTION_STYLE"               => "Y",
									"AJAX_OPTION_HISTORY"             => "N"
								),
								$component
							); ?>
						</div>
					</div>
					<div class="widget archive">
						<div class="widget-title">
							<h3>Заявка на подбор тура</h3>
						</div>
						<div class="widget-containt">
							<div class="text-center">
								<button class="btn btn-blue" id="helpForm" style="margin-top: 20px" data-toggle="modal" data-target="#helpFormModal">Помощь в подборе тура</button>
							</div>
						</div>
					</div>
					<div class="widget tags">
						<div class="widget-title">
							<h3>Тэги</h3>
						</div>
						<div class="widget-containt">
							<ul>
								<? $GLOBALS["arrFILTER"] = array("PARAM2" => 75) ?>
								<? $APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "", Array( //Подключение облака тегов
									"FONT_MAX"        => "50",
									"FONT_MIN"        => "10",
									"COLOR_NEW"       => "3E74E6",
									"COLOR_OLD"       => "C0C0C0",
									"PERIOD_NEW_TAGS" => "",
									"SHOW_CHAIN"      => "Y",
									"COLOR_TYPE"      => "Y",
									"WIDTH"           => "100%",
									"SORT"            => "CNT",
									"PAGE_ELEMENTS"   => "10",
									"PERIOD"          => "",
									"URL_SEARCH"      => "/search/index.php",
									"TAGS_INHERIT"    => "Y",
									"CHECK_DATES"     => "Y",
									"FILTER_NAME"     => "arrFILTER",
									"arrFILTER"       => "iblock",
									"CACHE_TYPE"      => "A",
									"CACHE_TIME"      => "360000"
								),
									$component
								); ?>
							</ul>
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>

<!-----------------------------------------------------------------------------------------------------
    Модальное окно формы подбора тура
 ------------------------------------------------------------------------------------------------------>
<div class="modal fade" id="helpFormModal" tabindex="-1" role="dialog" aria-labelledby="helpFormModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Помощь в подборе тура</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<? $APPLICATION->IncludeComponent(
						"bitrix:form.result.new",
						"tour_help_sidebar",
						Array(
							"AJAX_MODE"              => "Y",
							"SEF_MODE"               => "N",
							"WEB_FORM_ID"            => "6",
							"LIST_URL"               => "",
							"EDIT_URL"               => "",
							"SUCCESS_URL"            => "",
							"CHAIN_ITEM_TEXT"        => "",
							"CHAIN_ITEM_LINK"        => "",
							"IGNORE_CUSTOM_TEMPLATE" => "N",
							"USE_EXTENDED_ERRORS"    => "N",
							"CACHE_TYPE"             => "A",
							"CACHE_TIME"             => "3600",
							"VARIABLE_ALIASES"       => Array(
								"WEB_FORM_ID" => "WEB_FORM_ID",
								"RESULT_ID"   => "RESULT_ID"
							),
						),
						false
					); ?>
				</div>
			</div>

		</div>
	</div>
</div>

