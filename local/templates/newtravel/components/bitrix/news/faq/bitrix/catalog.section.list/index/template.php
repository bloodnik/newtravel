<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<?
$showAll   = $arParams['SHOW_ALL_SECTIONS_AND_Q'] ? $arParams['SHOW_ALL_SECTIONS_AND_Q'] : 'N';
$elementsQ = $arParams['SHOWS_ELEMENTS_Q'] = 400;
?>

<div class="faq panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<? foreach ($arResult["SECTIONS"] as $cell => $arSection): ?>
		<?
		$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>

		<? if ($arParams['SHOWS_ELEMENTS_Q'] == 0): ?>
			<a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="bxr-faq-section-name"><?=$arSection["NAME"]?></a>
		<? else: ?>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$cell?>" aria-expanded="false" aria-controls="collapse<?=$cell?>">
							<?=$arSection["NAME"]?>
						</a>
					</h4>
				</div>
				<div id="collapse<?=$cell?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?=$cell?>">
					<div class="panel-body">
						<?
						$arSelectQ = Array("ID", "NAME", "DETAIL_TEXT", "PREVIEW_TEXT");
						$arFilterQ = Array("IBLOCK_ID" => $arSection['IBLOCK_ID'], "SECTION_ID" => $arSection["ID"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
						$arNavQ    = false;
						if ($elementsQ && $showAll == "N") {
							$arNavQ = array("nPageSize" => $elementsQ);
						}
						$resQ = CIBlockElement::GetList(Array(), $arFilterQ, false, $arNavQ, $arSelectQ);
						while ($obQ = $resQ->GetNext()) {
							?>
							<h3 class="bxr-font-color"><?=$obQ['NAME']?></h3>
							<div class="descr">
								<?if($obQ['PREVIEW_TEXT']):?>
									<?=$obQ['PREVIEW_TEXT']?>
								<?endif;?>
								
								<p><?=$obQ['DETAIL_TEXT']?></p>
							</div>
						<? } ?>
					</div>
				</div>
				<? if ($elementsQ && $showAll == "N" && $elementsQ < $arSection['ELEMENT_CNT']): ?>
					<div class="bxr-faq-show-all-q">
						<a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=GetMessage("SHOW_ALL_ELEMENTS")?></a>
					</div>
				<? endif; ?>
			</div>

		<? endif; ?>

	<? endforeach; ?>
</div>