<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? $APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"",
	Array(
		"IBLOCK_TYPE"         => $arParams['IBLOCK_TYPE'],
		"IBLOCK_ID"           => $arParams['IBLOCK_ID'],
		"SECTION_ID"          => "",
		"SECTION_CODE"        => "",
		"COUNT_ELEMENTS"      => "N",
		"TOP_DEPTH"           => "1",
		"SECTION_FIELDS"      => array(0 => "", 1 => "",),
		"SECTION_USER_FIELDS" => array(0 => "", 1 => "",),
		"SECTION_URL"         => "",
		"CACHE_TYPE"          => "A",
		"CACHE_TIME"          => "36000000",
		"CACHE_GROUPS"        => "Y",
		"ADD_SECTIONS_CHAIN"  => "Y",
		"EL_IN_COL"           => "4"
	),
	$component
) ?>
