<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? $el = $APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"",
	array(
		"IBLOCK_TYPE"                     => $arParams['IBLOCK_TYPE'],
		"IBLOCK_ID"                       => $arParams['IBLOCK_ID'],
		"SECTION_ID"                      => $arResult['VARIABLES']['SECTION_ID'],
		"SECTION_CODE"                    => "",
		"SECTION_USER_FIELDS"             => array(
			0 => "UF_TITLE",
			1 => "UF_KEYWORDS",
			2 => "UF_GALLERY",
			3 => "UF_DESCRIPTION",
			4 => "",
		),
		"ELEMENT_SORT_FIELD"              => "sort",
		"ELEMENT_SORT_ORDER"              => "asc",
		"ELEMENT_SORT_FIELD2"             => "id",
		"ELEMENT_SORT_ORDER2"             => "desc",
		"FILTER_NAME"                     => "arrFilter",
		"INCLUDE_SUBSECTIONS"             => "Y",
		"SHOW_ALL_WO_SECTION"             => "N",
		"SECTION_URL"                     => "",
		"DETAIL_URL"                      => "",
		"SECTION_ID_VARIABLE"             => "SECTION_ID",
		"SET_META_KEYWORDS"               => "Y",
		"META_KEYWORDS"                   => "UF_KEYWORDS",
		"SET_META_DESCRIPTION"            => "Y",
		"META_DESCRIPTION"                => "UF_DESCRIPTION",
		"BROWSER_TITLE"                   => "UF_TITLE",
		"ADD_SECTIONS_CHAIN"              => "Y",
		"DISPLAY_COMPARE"                 => "N",
		"SET_TITLE"                       => "Y",
		"SET_STATUS_404"                  => "N",
		"PAGE_ELEMENT_COUNT"              => "50",
		"LINE_ELEMENT_COUNT"              => "1",
		"PROPERTY_CODE"                   => array(
			0 => "",
			1 => "",
		),
		"OFFERS_LIMIT"                    => "1",
		"PRICE_CODE"                      => array(),
		"USE_PRICE_COUNT"                 => "N",
		"SHOW_PRICE_COUNT"                => "1",
		"PRICE_VAT_INCLUDE"               => "Y",
		"BASKET_URL"                      => "/personal/basket.php",
		"ACTION_VARIABLE"                 => "action",
		"PRODUCT_ID_VARIABLE"             => "id",
		"USE_PRODUCT_QUANTITY"            => "N",
		"PRODUCT_QUANTITY_VARIABLE"       => "quantity",
		"ADD_PROPERTIES_TO_BASKET"        => "Y",
		"PRODUCT_PROPS_VARIABLE"          => "prop",
		"PARTIAL_PRODUCT_PROPERTIES"      => "N",
		"PRODUCT_PROPERTIES"              => array(),
		"CACHE_TYPE"                      => "A",
		"CACHE_TIME"                      => "36000000",
		"CACHE_NOTES"                     => "",
		"CACHE_FILTER"                    => "N",
		"CACHE_GROUPS"                    => "Y",
		"PAGER_TEMPLATE"                  => "newtravel",
		"DISPLAY_TOP_PAGER"               => "N",
		"DISPLAY_BOTTOM_PAGER"            => "Y",
		"PAGER_TITLE"                     => "Товары",
		"PAGER_SHOW_ALWAYS"               => "Y",
		"PAGER_DESC_NUMBERING"            => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "360000",
		"PAGER_SHOW_ALL"                  => "N",
		"HIDE_NOT_AVAILABLE"              => "N",
		"CONVERT_CURRENCY"                => "N",
		"AJAX_OPTION_JUMP"                => "N",
		"AJAX_OPTION_STYLE"               => "Y",
		"AJAX_OPTION_HISTORY"             => "N",
		"AJAX_OPTION_ADDITIONAL"          => "",
		"SET_BROWSER_TITLE"               => "Y"
	),
	$component
); ?>

<section class="gray-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<? $APPLICATION->ShowViewContent("country_desc"); //выводим из буфера ?>

				<?$APPLICATION->IncludeFile(
					$APPLICATION->GetTemplatePath("include/managers.php"),
					Array(),
					Array("MODE" => "html", "SHOW_BORDER" => true)
				);
				?>

				<div class="post-comments">
					<? $APPLICATION->IncludeComponent(
						"cackle.comments",
						"",
						Array()
					); ?>
				</div>
			</div>

			<div class="col-md-4">
				<!-- Курорты страны -->
				<? $APPLICATION->IncludeComponent(
					"bitrix:catalog.section.list",
					"adapt_curorts",
					Array(
						"IBLOCK_TYPE"         => $arParams['IBLOCK_TYPE'],
						"IBLOCK_ID"           => $arParams['IBLOCK_ID'],
						"SECTION_ID"          => $arResult['VARIABLES']['SECTION_ID'],
						"SECTION_CODE"        => "",
						"COUNT_ELEMENTS"      => "N",
						"TOP_DEPTH"           => "1",
						"SECTION_FIELDS"      => array(),
						"SECTION_USER_FIELDS" => array(),
						"SECTION_URL"         => "",
						"CACHE_TYPE"          => "A",
						"CACHE_TIME"          => "36000000",
						"CACHE_GROUPS"        => "Y",
						"ADD_SECTIONS_CHAIN"  => "N",
						"EL_IN_COL"           => "2",
						"COUNTRY_NAME"        => $GLOBALS['COUNTRY_INFO']["NAME"]
					),
					$component
				); ?>
				<div class="text-widget categories" style="overflow: visible">
					<div class="widget-title">
						<h3 id="hot">Горящие туры в <?=$GLOBALS['COUNTRY_INFO']["NAME"]?></h3>
					</div>
					<div class="col-xs-12 col-sm-12" style="margin-bottom: 20px">
						<div class="row">
							<?$APPLICATION->IncludeComponent(
								"newtravel.search:simple.search",
								"",
								Array(
									"ADULTS" => "2",
									"CACHE_TIME" => "3600",
									"CACHE_TYPE" => "A",
									"CITY" => "4",
									"COMPOSITE_FRAME_MODE" => "A",
									"COMPOSITE_FRAME_TYPE" => "AUTO",
									"COUNTRY" => $GLOBALS["TOURISOR_COUNTRY"],
									"DATE_TO" => "14",
									"NIGHTSFROM" => "6",
									"NIGHTSTO" => "14",
									"STARS" => "1"
								)
							);?>
						</div>
					</div>
				</div>

				<div class="widget recent-posts" style="overflow: hidden;">
					<div class="widget-title">
						<h3>Популярные записи блога</h3>
					</div>

					<div class="widget-containt">
						<? $GLOBALS["arrFilter"] = array("PROPERTY_POST_COUNTRY" => $arResult["VARIABLES"]["SECTION_ID"]) ?>
						<? $APPLICATION->IncludeComponent( //Подключение популярных постов
							"bitrix:news.list",
							"popular_posts",
							Array(
								"DISPLAY_DATE"                    => "Y",
								"DISPLAY_NAME"                    => "Y",
								"DISPLAY_PICTURE"                 => "Y",
								"DISPLAY_PREVIEW_TEXT"            => "N",
								"AJAX_MODE"                       => "N",
								"IBLOCK_TYPE"                     => "simai",
								"IBLOCK_ID"                       => "75",
								"NEWS_COUNT"                      => "5",
								"SORT_BY1"                        => "SHOW_COUNTER",
								"SORT_ORDER1"                     => "DESC",
								"SORT_BY2"                        => "NAME",
								"SORT_ORDER2"                     => "ASC",
								"FILTER_NAME"                     => "arrFilter",
								"FIELD_CODE"                      => array(0 => "SHOW_COUNTER"),
								"PROPERTY_CODE"                   => array(),
								"CHECK_DATES"                     => "Y",
								"DETAIL_URL"                      => "",
								"PREVIEW_TRUNCATE_LEN"            => "",
								"ACTIVE_DATE_FORMAT"              => "d.m.Y",
								"SET_STATUS_404"                  => "N",
								"SET_TITLE"                       => "N",
								"INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
								"ADD_SECTIONS_CHAIN"              => "N",
								"HIDE_LINK_WHEN_NO_DETAIL"        => "N",
								"PARENT_SECTION"                  => "",
								"PARENT_SECTION_CODE"             => "",
								"INCLUDE_SUBSECTIONS"             => "N",
								"CACHE_TYPE"                      => "A",
								"CACHE_TIME"                      => "36000000",
								"CACHE_FILTER"                    => "Y",
								"CACHE_GROUPS"                    => "Y",
								"PAGER_TEMPLATE"                  => ".default",
								"DISPLAY_TOP_PAGER"               => "N",
								"DISPLAY_BOTTOM_PAGER"            => "N",
								"PAGER_TITLE"                     => "Новости",
								"PAGER_SHOW_ALWAYS"               => "N",
								"PAGER_DESC_NUMBERING"            => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL"                  => "N",
								"AJAX_OPTION_JUMP"                => "N",
								"AJAX_OPTION_STYLE"               => "Y",
								"AJAX_OPTION_HISTORY"             => "N"
							),
							$component
						); ?>
					</div>
				</div>
				
				<?
				$GLOBALS["arrExFilter"] = Array("PROPERTY_EXCURSION_PLACE" => $arResult["VARIABLES"]["SECTION_ID"]);
				$APPLICATION->IncludeComponent(
					"bitrix:news.list",
					"excurs",
					array(
						"IBLOCK_TYPE"                     => "permits",
						"IBLOCK_ID"                       => "11",
						"NEWS_COUNT"                      => "5",
						"SORT_BY1"                        => "ACTIVE_FROM",
						"SORT_ORDER1"                     => "DESC",
						"SORT_BY2"                        => "SORT",
						"SORT_ORDER2"                     => "ASC",
						"FILTER_NAME"                     => "arrExFilter",
						"FIELD_CODE"                      => array(
							0 => "NAME",
							1 => "PREVIEW_PICTURE",
							2 => "DETAIL_PICTURE",
							3 => "DATE_ACTIVE_FROM",
							4 => "ACTIVE_FROM",
							5 => "DATE_CREATE",
							6 => "",
						),
						"PROPERTY_CODE"                   => array(
							0 => "",
							1 => "",
						),
						"CHECK_DATES"                     => "Y",
						"DETAIL_URL"                      => "",
						"AJAX_MODE"                       => "Y",
						"AJAX_OPTION_JUMP"                => "N",
						"AJAX_OPTION_STYLE"               => "Y",
						"AJAX_OPTION_HISTORY"             => "N",
						"CACHE_TYPE"                      => "A",
						"CACHE_TIME"                      => "36000000",
						"CACHE_FILTER"                    => "Y",
						"CACHE_GROUPS"                    => "Y",
						"PREVIEW_TRUNCATE_LEN"            => "",
						"ACTIVE_DATE_FORMAT"              => "d.m.Y",
						"SET_TITLE"                       => "N",
						"SET_STATUS_404"                  => "N",
						"INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
						"ADD_SECTIONS_CHAIN"              => "N",
						"HIDE_LINK_WHEN_NO_DETAIL"        => "N",
						"PARENT_SECTION"                  => "",
						"PARENT_SECTION_CODE"             => "",
						"INCLUDE_SUBSECTIONS"             => "Y",
						"PAGER_TEMPLATE"                  => "country-tabs",
						"DISPLAY_TOP_PAGER"               => "N",
						"DISPLAY_BOTTOM_PAGER"            => "Y",
						"PAGER_TITLE"                     => "Экскурсии",
						"PAGER_SHOW_ALWAYS"               => "N",
						"PAGER_DESC_NUMBERING"            => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL"                  => "Y",
						"AJAX_OPTION_ADDITIONAL"          => "",
						"COUNTRY_NAME"                    => $GLOBALS['COUNTRY_INFO']["NAME"]
					),
					$component
				); ?>

				<div class="widget archive">
					<div class="widget-title">
						<h3>Заявка на подбор тура</h3>
					</div>
					<div class="widget-containt">

						<? $APPLICATION->IncludeComponent(
							"bitrix:form.result.new",
							"tour_help_sidebar",
							Array(
								"AJAX_MODE"              => "Y",
								"SEF_MODE"               => "N",
								"WEB_FORM_ID"            => "6",
								"LIST_URL"               => "",
								"EDIT_URL"               => "",
								"SUCCESS_URL"            => "",
								"CHAIN_ITEM_TEXT"        => "",
								"CHAIN_ITEM_LINK"        => "",
								"IGNORE_CUSTOM_TEMPLATE" => "N",
								"USE_EXTENDED_ERRORS"    => "N",
								"CACHE_TYPE"             => "A",
								"CACHE_TIME"             => "36000",
								"VARIABLE_ALIASES"       => Array(
									"WEB_FORM_ID" => "WEB_FORM_ID",
									"RESULT_ID"   => "RESULT_ID"
								),
							),
							false
						); ?>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
<?
unset($GLOBALS["TOURISOR_COUNTRY"]);
unset($GLOBALS["COUNTRY_INFO"]);
?>
<script>
	$(document).ready(function () {
		var helpformmessage = "";
		var userurl = "";
	})

</script>

