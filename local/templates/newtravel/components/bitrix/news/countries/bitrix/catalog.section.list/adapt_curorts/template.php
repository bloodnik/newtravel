<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>
<? $arPopularCurorts = array() ?>
<? if (count($arResult["SECTIONS"]) > 0): ?>
	<div class="text-widget categories_resort">
		<div class="widget-title">
			<h3>Курорты <?=$arParams["COUNTRY_NAME"]?></h3>
		</div>
		<div class="nav nav-tabs" role="tablist">
			<a href="#popular" class="popular-resorts" role="tab" data-toggle="tab">Популярные курорты</a>
			<div class="all-resorts"><a href="#all_resorts" role="tab" data-toggle="tab">Все курорты</a></div>
		</div>
		<div class="tab-content">
			<div class="tab-pane fade active in" id="popular">
				<ul>
					<? foreach ($arResult["SECTIONS"] as $i => $arSection): ?>
						<? if ($arSection['POPULAR']): ?>
							<li><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a></li>
						<? endif ?>
					<? endforeach; ?>
				</ul>
			</div>
			<div class="tab-pane fade" id="all_resorts">
				<ul>
					<? foreach ($arResult["SECTIONS"] as $i => $arSection): ?>
						<li><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a></li>
					<? endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
<? endif; ?>

