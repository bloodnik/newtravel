<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
foreach ($arResult["SECTIONS"] as $i => $arSection):
	$db_list = CIBlockSection::GetList(Array(SORT => "ASC"), $arFilter = Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ID" => $arSection["ID"]), true, $arSelect = Array("UF_*"));
	$popular = false;
	while ($arPr = $db_list->GetNext()) {
		if (strlen($arPr["UF_POPULAR"]) > 0) {
			$arResult["SECTIONS"][$i]['POPULAR'] = true;
		}
	}
endforeach;
