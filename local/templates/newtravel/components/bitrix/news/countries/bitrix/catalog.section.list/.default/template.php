<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
$countSection = count($arResult["SECTIONS"]);

if ( ! empty($arResult["TOPTEN"])):?>
	<section class="white-section specialoffer-list">
		<div class="container">
			<div class="row">
				<? foreach ($arResult["TOPTEN"] as $i => $arSection): ?>
					<div class="col-md-3 col-xs-6 col-sm-6 item-wrap">
						<div class="item">
							<a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="img-link" style="background: url('<?=$arSection['PREVIEW_IMG']['SRC']?>')"></a>
							<a href="<?=$arSection["SECTION_PAGE_URL"]?>">
								<div class="item_price">
									<div class="right">
										<?=$arSection['NAME']?>
									</div>
								</div>
							</a>
						</div>
					</div>
				<? endforeach; ?>
			</div>
		</div>
	</section>
<? endif ?>


<? if ( ! empty($arResult["SECTIONS"])): ?>
	<section class="white-section">
		<div class="container">
			<div class="row">
				<ul class="country_list">
					<? foreach ($arResult["SECTIONS"] as $i => $arSection): ?>
						<li>
							<? if (is_array($arSection["PREVIEW_IMG"])): ?>
								<a href="<?=$arSection["SECTION_PAGE_URL"]?>"><img alt="<?=$arSection["NAME"]?>" title="<?=$arSection["NAME"]?>" src="<?=$arSection["PREVIEW_IMG"]["SRC"]?>"/></a>
							<? else: ?>
								<a href="<?=$arSection["SECTION_PAGE_URL"]?>"><img alt="<?=$arSection["NAME"]?>" title="<?=$arSection["NAME"]?>"
								                                                   src="<?=SITE_TEMPLATE_PATH?>/images/map_marker.png"/></a>
							<? endif ?>
							<a href="<?=$arSection["SECTION_PAGE_URL"]?>">> <?=$arSection["NAME"]?></a>
							<? if (($i + 1) % $arParams["EL_IN_COL"] == 0 && $countSection != $i + 1): ?><? endif ?>
						</li>
					<? endforeach ?>
				</ul>
			</div>
		</div>
	</section>
<? endif ?>
