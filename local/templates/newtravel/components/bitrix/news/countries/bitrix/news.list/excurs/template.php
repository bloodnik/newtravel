<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>
<div class="widget recent-posts">
	<div class="widget-title">
		<h3>Экскурсии <?=$arParams["COUNTRY_NAME"]?></h3>
	</div>
	<div class="widget-containt">
		<? if (count($arResult["ITEMS"]) < 1) {
			echo "<div class=\"details\" style=\"margin-top:-1px;margin-bottom: 15px;\"><div class=\"detail-in\">" . GetMessage("NO_EXCURS") . "</div></div>";

			return;
		} ?>
		<ul>
			<? foreach ($arResult["ITEMS"] as $arItem): ?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<li>
					<div class="post-thumbnail">
						<? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_IMG"])): ?>
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem["NAME"]?>">
								<img src="<?=$arItem["PREVIEW_IMG"]["SRC"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>"/>
							</a>
						<? endif ?>
					</div>
					<div class="post-containt">
						<!-- post-title -->
						<div class="post-title">
							<h5>
								<? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]): ?>
									<a href="<?=$arItem["DETAIL_PAGE_URL"]?>    " title="">
										> <?=$arItem["NAME"]?>
									</a>
								<? endif; ?>
							</h5>
						</div>
					</div>
				</li>
			<? endforeach; ?>
		</ul>
	</div>
</div>
