<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

$topTenCounties = array();

foreach ($arResult["SECTIONS"] as $i => $arSection) {

	$arUserFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_" . $arSection["IBLOCK_ID"] . "_SECTION", $arSection['ID']);
	//Разделяем страны из ТОП10 в отдельный массив
	$arResult["SECTIONS"][ $i ]['TOPTEN'] = $arUserFields['UF_TOPTEN']['VALUE']; //Страна из ТОП 10
	if ($arUserFields['UF_TOPTEN']['VALUE']) {

		$topTenCounties[$i] = $arResult["SECTIONS"][ $i ];

		if (is_array($arSection["PICTURE"])) {
			$arFileTmp                                 = CFile::ResizeImageGet(
				$arSection["PICTURE"],
				array("width" => 300, "height" => 185),
				BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
				false
			);
			$arSize                                    = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);
			$topTenCounties[ $i ]["PREVIEW_IMG"] = array(
				"SRC"    => $arFileTmp["src"],
				"WIDTH"  => IntVal($arSize[0]),
				"HEIGHT" => IntVal($arSize[1]),
			);
		}

		unset($arResult["SECTIONS"][ $i ]);
		continue;
	}


	if (is_array($arSection["PICTURE"])) {
		$arFileTmp                                 = CFile::ResizeImageGet(
			$arSection["PICTURE"],
			array("width" => 40, "height" => 40),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			false
		);
		$arSize                                    = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);
		$arResult["SECTIONS"][ $i ]["PREVIEW_IMG"] = array(
			"SRC"    => $arFileTmp["src"],
			"WIDTH"  => IntVal($arSize[0]),
			"HEIGHT" => IntVal($arSize[1]),
		);
	}
}

$arResult['TOPTEN'] = $topTenCounties;

?>