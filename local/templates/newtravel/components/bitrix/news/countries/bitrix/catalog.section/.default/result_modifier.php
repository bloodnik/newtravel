<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
//Получаем пользовательские поля
if ($arResult["DEPTH_LEVEL"] == 1) {
	$arUserFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_" . $arResult["IBLOCK_ID"] . "_SECTION", $arResult["ID"]);
} else {
	$arUserFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_" . $arResult["IBLOCK_ID"] . "_SECTION", $arResult["IBLOCK_SECTION_ID"]);
}
$arResult["COUNTRY_INFO"] = $arUserFields;


unset($countryInfo, $arUserFields);


if ($arResult["DEPTH_LEVEL"] == 1) {
	$CountryFlag = CFile::ResizeImageGet(
		$arResult["PICTURE"],
		array("width" => 140, "height" => 100),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		true
	);
	$CountryImg  = CFile::ResizeImageGet(
		$arResult["DETAIL_PICTURE"],
		array("width" => 545, "height" => 400),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		true
	);

	$arResult['COUNTRY_FLAG'] = $CountryFlag;
	$arResult['COUNTRY_IMG']  = $CountryImg;
	$arResult['CURORT_CODE']  = "";
} else {

	$res = CIBlockSection::GetByID($arResult["PATH"][0]['ID']);
	if($ar_res = $res->GetNext())

	$CountryFlag = CFile::ResizeImageGet(
		$ar_res["PICTURE"],
		array("width" => 140, "height" => 100),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		true
	);
	$CountryImg  = CFile::ResizeImageGet(
		$arResult["DETAIL_PICTURE"],
		array("width" => 545, "height" => 400),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		true
	);

	$arResult['COUNTRY_FLAG'] = $CountryFlag;
	$arResult['COUNTRY_IMG']  = $CountryImg;
	$arResult['CURORT_CODE']  = $arResult["CODE"];
}

$cp = $this->__component;
$cp->SetResultCacheKeys(array(
	"COUNTRY_INFO",
	"SECTION_PAGE_URL",
	"DEPARTURE_INFO"
));