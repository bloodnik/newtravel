<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>

<div class="single-post">
	<article>
		<div class="post-wrapper standard-post">
			<div class="post-thumbnail">
				<? if (is_array($arResult["PREVIEW_IMG"])): ?>
					<img src="<?=$arResult["PREVIEW_IMG"]["SRC"]?>" alt="<?=$arResult["NAME"]?>"
					     title="<?=$arResult["NAME"]?>"/>
				<? else: ?>
					<img src="<?=SITE_TEMPLATE_PATH?>/images/no_blog_image.jpg" width="<?=$arResult["PREVIEW_IMG"]["WIDTH"]?>" height="<?=$arResult["PREVIEW_IMG"]["HEIGHT"]?>"
					     alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>"/>
				<? endif ?>
			</div>
			<div class="post-title">
				<h3><?=$arResult["NAME"]?></h3>
			</div>
			<div class="post-containt">
				<? if ($arResult["DETAIL_TEXT"]): ?>
					<div class="news-detail-text"><?=$arResult["DETAIL_TEXT"]?>    </div>
				<? elseif ($arResult["PREVIEW_TEXT"]): ?>
					<div class="news-detail-text"><?=$arResult["PREVIEW_TEXT"]?>    </div>
				<? endif ?>

				<? foreach ($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"] as $photo): //Галерея?>
					
					<ul class="resort-gallery my-work">
						<li>
							<div class="img-figure-overlayer">
								<a class="fancybox" data-fancybox-group="gallery3" href="<?=$photo?>" title="<?=$arResult['NAME']?>">
									<i class="fa fa-search"></i>
								</a>
							</div>
							<div class="img-figure">
								<img src="<?=$photo?>" alt="">
							</div>
						</li>
					</ul>
				<? endforeach; ?>
			</div>
			<div class="post-meta">
				<? if ($arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]): ?>
					<span class="post-author">
							<a href="/blog/?&arrFilter_pf[POST_AUTHOR]=<?=$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["ID"]?>&set_filter=Фильтр&set_filter=Y"
							   title="Автор"><?=$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["NAME"]?> <?=$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["LAST_NAME"]?></a>
						</span>
				<? endif; ?>
				<span class="post-date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
				<span class="post-comment"><a href="<?=$arResult["DETAIL_PAGE_URL"]?>#disqus_thread"></a></span>
			</div>
		</div>
		<div class="post-tags">
			<noindex>
				<ul>
					<? foreach ($arResult["TAGS_ARRAY"] as $tag): ?>
						<li><a href="/search/index.php?tags=<?=$tag?>" rel="nofollow" target="_blank" title="tag"><?=$tag?></a></li>
					<? endforeach; ?>
				</ul>
			</noindex>
		</div>
	</article>

	<script type="text/javascript">(function () {
			if (window.pluso)if (typeof window.pluso.start == "function") return;
			if (window.ifpluso == undefined) {
				window.ifpluso = 1;
				var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
				s.type = 'text/javascript';
				s.charset = 'UTF-8';
				s.async = true;
				s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
				var h = d[g]('body')[0];
				h.appendChild(s);
			}
		})();</script>
	<div class="pluso" style="padding: 12px;" data-background="transparent" data-options="medium,square,line,horizontal,counter,theme=05"
	     data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print"></div>


	<? if (isset($arResult["DISPLAY_PROPERTIES"]['POST_AUTHOR'])): //Отложенны?>
		<? $this->SetViewTarget("blog_author"); ?>
		<div class="widget ">
			<div class="widget-title">
				<h3>Автор поста</h3>
			</div>
			<div class="author-box-img">
				<? if (isset($arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]['PERSONAL_PHOTO'])): ?>
					<div style="height: 67px; overflow: hidden">
						<img src="<?=CFile::GetPath($arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["PERSONAL_PHOTO"])?>" alt="Автор"/>
					</div>
				<? else: ?>
					<img src="<?=SITE_TEMPLATE_PATH?>/images/no-avatar.gif" alt="Автор"/>
				<? endif; ?>
			</div>
			<div class="author-box-title">
				<h3>
					<a href="/blog/?&arrFilter_pf[POST_AUTHOR]=<?=$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["ID"]?>&set_filter=Фильтр&set_filter=Y"
					   title="Автор"><?=$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["NAME"]?> <?=$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["LAST_NAME"]?></a>
				</h3>
			</div>
			<div class="author-box-content">
				<? if (strlen($arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["PERSONAL_PROFESSION"]) > 0): ?>
					<p><?=$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"]["PERSONAL_PROFESSION"]?></p>
				<? else: ?>
					<p>------------</p>
				<? endif; ?>

			</div>
		</div>
		<? $this->EndViewTarget(); ?>
	<? endif; ?>
</div>