<?
$GLOBALS["COUNTRY_INFO"]=$arResult['PATH'][0];
$GLOBALS["TOURISOR_COUNTRY"]=$arResult['COUNTRY_INFO']["UF_TOURVISOR_COUNTRY"]["VALUE"];

use Newtravel\Search\TourvisorHlLists;
\Bitrix\Main\Loader::includeModule('newtravel.search');
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();


//Получаем информацию о текущем городе из куки
$cityInfo = TourvisorHlLists::getList('departures', array("*"), array("UF_XML_ID" => $request->getCookie('USER_CITY')));
//Получаем информацию о стране
$countryInfo = TourvisorHlLists::getList('countries', array("*"), array("UF_XML_ID" => $arResult["COUNTRY_INFO"]['UF_TOURVISOR_COUNTRY']['VALUE']));

//Получаем информацию по времени вылета из города в страну
$flighttime = TourvisorHlLists::getList('flighttime', array("*"), array("UF_DEPARTURE" => $cityInfo[0]['ID'], "UF_COUNTRY" => $countryInfo[0]['ID']));

$arResult['DEPARTURE_INFO'] = $cityInfo[0];
$arResult['DEPARTURE_INFO']['FLIGHTTIME'] = $flighttime[0]['UF_TIME'];

unset($cityInfo, $countryInfo, $flighttime);

\Bitrix\Main\Page\Asset::getInstance()->addString('<link href="https://'.SITE_SERVER_NAME.$arResult['SECTION_PAGE_URL'].'" rel="canonical" />',true, 'BEFORE_CSS');

?>

<script>
	var time = '<?=$arResult['DEPARTURE_INFO']['FLIGHTTIME']?>';
	if(time.length > 0 ){
		$('.flightinfo').removeClass('hidden');
	}
	$('.flightinfo .title').text('Перелет из <?=$arResult["DEPARTURE_INFO"]['UF_NAME_FROM']?>');
	$('.flightinfo .value').text('<?=$arResult['DEPARTURE_INFO']['FLIGHTTIME']?>');
</script>
