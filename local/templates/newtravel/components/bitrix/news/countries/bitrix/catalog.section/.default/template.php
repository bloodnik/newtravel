<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>
<section class="white-section">
	<div class="container">
		<div class="row">
			<div class="about-me-wrapper">
				<div class="col-md-6">
					<div class="about-me">
						<div class="img-cover">
							<div class="thumbnail">
								<? if (is_array($arResult['COUNTRY_IMG']) > 0): ?>
									<div style="background: url('<?=$arResult['COUNTRY_IMG']['src']?>');height: 325px;background-size: cover;"></div>
								<? else: ?>
									<img src="<?=SITE_TEMPLATE_PATH?>/images/no-image-available.jpg" title="Умныетуристы. <?=$arResult['NAME']?>" alt="<?=$arResult['NAME']?>"/>
								<? endif; ?>
							</div>
						</div>
						<div class="member-img">
							<div class="thumbnail">
								<? if (is_array($arResult['COUNTRY_FLAG']) > 0): ?>
									<img src="<?=$arResult['COUNTRY_FLAG']['src']?>" title="Умныетуристы <?=$arResult['NAME']?>" alt="<?=$arResult['NAME']?>"/>
								<? else: ?>
									<img src="<?=SITE_TEMPLATE_PATH?>/images/no-avatar.gif" alt="member cover"/>
								<? endif; ?>
							</div>
						</div>
						<div class="member-info">
							<h2 class="name">
								<? if ($arResult["DEPTH_LEVEL"] == 1) : ?>
									<?=$arResult["NAME"]?>
								<? else: ?>
									<?=$arResult["PATH"][0]["NAME"]?>
								<? endif ?>
							</h2>
							<h3 class="title">
								<? if ($arResult["DEPTH_LEVEL"] > 1) : ?>
									<?=$arResult["NAME"]?>
								<? endif; ?>
							</h3>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="text-widget">
						<div class="text-widget-title">
							<h3><?=$arResult["NAME"]?> кратко</h3>
						</div>
						<ul class="personal-details">
							<? if ($arResult["COUNTRY_INFO"]["UF_FLIGHT_HOURS"]): ?>
								<li class="flightinfo hidden">
									<span class="title"></span>
									<span class="value"></span>
								</li>
							<? endif ?>
							<? if (isset($arResult["COUNTRY_INFO"]["UF_SEASON"])): ?>
								<li>
									<span class="title">Сезон</span>
									<span class="value">
		                                    <?=$arResult["COUNTRY_INFO"]["UF_SEASON"]['VALUE']?>
	                                </span>
								</li>
							<? endif ?>
							<? if (isset($arResult["COUNTRY_INFO"]["UF_GTM"])): ?>
								<li>
									<span class="title">Часовой пояс</span>
									<span class="value">
		                                    <?=$arResult["COUNTRY_INFO"]["UF_GTM"]['VALUE']?>
	                                </span>
								</li>
							<? endif ?>
							<? if (isset($arResult["COUNTRY_INFO"]["UF_CURRENCY"])): ?>
								<li>
									<span class="title">Валюта</span>
									<span class="value">
		                                    <?=$arResult["COUNTRY_INFO"]["UF_CURRENCY"]['VALUE']?>
	                                </span>
								</li>
							<? endif ?>
							<? if (isset($arResult["COUNTRY_INFO"]["UF_LANG"])): ?>
								<li>
									<span class="title">Язык</span>
									<span class="value">
		                                    <?=$arResult["COUNTRY_INFO"]["UF_LANG"]['VALUE']?>
	                                </span>
								</li>
							<? endif ?>
						</ul>
						<div class="member-social-icons">
							<h5>Добавить закладку в социалку!</h5>
							<script type="text/javascript">(function () {
                                    if (window.pluso) if (typeof window.pluso.start == "function") return;
                                    if (window.ifpluso == undefined) {
                                        window.ifpluso = 1;
                                        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                                        s.type = 'text/javascript';
                                        s.charset = 'UTF-8';
                                        s.async = true;
                                        s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                                        var h = d[g]('body')[0];
                                        h.appendChild(s);
                                    }
                                })();</script>
							<div class="pluso" data-background="#ffffff"
							     data-options="medium,round,multiline,horizontal,counter,theme=02"
							     data-services="vkontakte,facebook,twitter,odnoklassniki,google,moimir,livejournal,email,print"></div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="text-widget categories">
						<div class="text-widget-title">
							<h3>Навигатор по <?=$arResult["NAME"]?></h3>
						</div>
						<ul>
							<li><a href="#description">Описание <?=$arResult["NAME"]?></a></li>
							<li><a href="#excurs">Экскурсии в <?=$arResult["NAME"]?></a></li>
							<li><a href="#description">Статьи и советы о <?=$arResult["NAME"]?></a></li>
							<li><a href="#gallery">Фоторафии <?=$arResult["NAME"]?></a></li>
							<li><a href="#gallery">Видео <?=$arResult["NAME"]?></a></li>
							<li><a href="#hot">Горящие туры в <?=$arResult["NAME"]?></a></li>
						</ul>
						<noindex>
							<a href="/tours/?start=Y&country=<?=$arResult["COUNTRY_INFO"]["UF_TOURVISOR_COUNTRY"]['VALUE']?>&nightsfrom=6&nightsto=14&adults=2&child=0&childage1=0&childage2=0&childage3=0&stars=1&meal=1&rating=0"
							   target="_blank" class="btn btn-main" style="margin-top:20px" rel="nofollow">
								Смотреть цены в <?=$arResult["NAME"]?>
							</a>
						</noindex>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<? $this->SetViewTarget("country_desc"); //сохраняем в буфер. выведем в section.php?>
<div class="text-widget">
	<div class="text-widget-title">
		<? if ($arResult["DEPTH_LEVEL"] == 1) : ?>
			<h1 id="description">Описание страны <?=$arResult["NAME"]?></h1>
		<? else: ?>
			<h1 id="description">Описание курорта <?=$arResult["NAME"]?></h1>
		<? endif; ?>
	</div>
	<? if ($arResult["DESCRIPTION"]): ?>
		<div class="description prof"><?=$arResult["DESCRIPTION"]?></div>
	<? else: ?>

		<?=getMessage("NO_DESCRIPTION")?>
	<? endif ?>

	<? if (count($arResult['ITEMS']) > 0): ?>
		<h2 style="margin-top: 15px;">Отели <?=$arResult["NAME"]?></h2>

		<div class="description prof">
			<?foreach ($arResult['ITEMS'] as $arItem):?>
				<span class="label label-default"><a style="color: #fff;" href="/hotel/<?=$arItem['CODE']?>/" target="_blank"><?=$arItem['NAME']?></a></span>
			<?endforeach;?>
		</div>

		<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
			<div class="nav-string">
				<?=$arResult["NAV_STRING"]?>
			</div>
		<? endif; ?>
	<? endif; ?>

</div>

<div class="col-md-12">
	<div class="short-section-title">
		<h3 id="gallery"><?=$arResult["NAME"]?> - Фотогалерея и Видео</h3>
	</div>
</div>
<? $this->EndViewTarget(); ?>

