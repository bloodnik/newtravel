<?
$this->addExternalCss(SITE_TEMPLATE_PATH . "/js/fancybox/jquery.fancybox-1.3.4.css");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/fancybox/jquery.fancybox-1.3.4.pack.js");

if (is_array($arResult["PREVIEW_PICTURE"])) {
	$arFileTmp                                = CFile::ResizeImageGet(
		$arResult["PREVIEW_PICTURE"],
		array("width" => 816, "height" => 400),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		false
	);
	$arSize                                   = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);
	$arResult["PREVIEW_IMG"] = array(
		"SRC"    => $arFileTmp["src"],
		"WIDTH"  => IntVal($arSize[0]),
		"HEIGHT" => IntVal($arSize[1]),
	);
} elseif (is_array($arResult["DETAIL_PICTURE"])) {
	$arFileTmp                                = CFile::ResizeImageGet(
		$arResult["DETAIL_PICTURE"],
		array("width" => 816, "height" => 500),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		false
	);
	$arSize                                   = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);
	$arResult["PREVIEW_IMG"] = array(
		"SRC"    => $arFileTmp["src"],
		"WIDTH"  => IntVal($arSize[0]),
		"HEIGHT" => IntVal($arSize[1]),
	);
}

//Получаем дополнительные фотографии
if (is_array($arResult["PROPERTIES"]["MORE_PHOTO"])) {
	foreach ($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $photoId) {
		$arFileTmp                                = CFile::ResizeImageGet(
			$photoId,
			array("width" => 1000, "height" => 500),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			false
		);
		$arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"][] = $arFileTmp["src"];
	}
}

//Заполняем массив тегов
$tags                   = explode(", ", $arResult["TAGS"]);
$arResult["TAGS_ARRAY"] = $tags;

//Получаем свойства пользователя по ID
CModule::IncludeModule("user");
$rsUser                                        = CUser::GetByID($arResult["PROPERTIES"]["POST_AUTHOR"]["VALUE"]);
$arUser                                        = $rsUser->Fetch();
$arResult["DISPLAY_PROPERTIES"]["POST_AUTHOR"] = $arUser;
?>