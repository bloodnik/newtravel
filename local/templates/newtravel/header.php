<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
//use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Loader;
use Newtravel\Search\DefineGeo;

if ( ! isset($_SESSION['CITY']) || empty($_SESSION['CITY'])) {
	Loader::includeModule('newtravel.search');
	DefineGeo::setRegionIso();
	DefineGeo::setCity();
}
?>
<? define("PHONE", strlen($_SESSION['CITY']['UF_PHONE']) > 0 ? $_SESSION['CITY']['UF_PHONE'] : "8-800-505-47-61") //определяем константу с номером телефона strlen($_SESSION['CITY']['UF_PHONE']) > 0 ? $_SESSION['CITY']['UF_PHONE'] : "8-800-505-47-61"?>
<!DOCTYPE html>
<head>
	<title><? $APPLICATION->ShowTitle() ?></title>

	<?php
	$APPLICATION->AddBufferContent([$APPLICATION, 'GetLink'], 'prev');
	$APPLICATION->AddBufferContent([$APPLICATION, 'GetLink'], 'next');
	?>

	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name='yandex-verification' content='69434ba0b952afef'/>
	<meta name='yandex-verification' content='742486978b12aed5'/>
	<!--booking verify-->
	<meta name='B-verify' content='011fa8923c6c8033d424ec58de7391294d2b442e'/>

	<meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black"/>
	<meta name="GOOGLEBOT" content="index follow"/>
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico"/>

	<meta name="theme-color" content="#00AEEF"/>
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="#00AEEF">

	<meta property="og:type" content="website">
	<meta property="og:site_name" content="Умные туристы">
	<meta property="og:title" content="Умныетуристы.рф - сервис бронирования выгодных туров онлайн, покупка туров онлайн из Москвы по всем направлениям">
	<meta property="og:description" content="Умныетуристы.рф - сервис бронирования выгодных туров онлайн, покупка туров онлайн из Москвы по всем направлениям.">
	<meta property="og:url" content="https://xn--e1agkeqhedbe9fh.xn--p1ai/">
	<meta property="og:locale" content="ru_RU">
	<meta property="og:image" content="https://xn--e1agkeqhedbe9fh.xn--p1ai/local/templates/promo_pages/assets/i/logo.png">


	<!-- link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption|PT+Sans:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet" -->
	<?
	$APPLICATION->ShowHead();
	//CSS
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/bootstrap.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/font-awesome/css/font-awesome.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/fontello.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/animate.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/flexslider.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/fancybox/jquery.fancybox.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/menu.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/mCustomScrollbar/jquery.mCustomScrollbar.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.min.css");

	//JS

	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.js");
	//Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/vendor/modernizr.custom.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-ui.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/vendor/bootstrap.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/plugins/jquery.flexslider-min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/plugins/jquery.carouFredSel-6.2.1-packed.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/fancybox/jquery.fancybox.pack.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/plugins/jquery.accordion.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/plugins/jquery.cookie.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/plugins/jquery.scrollTo-min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/daterangepicker/moment.min.js");
	//Asset::getInstance()->addJs("https://maps.googleapis.com/maps/api/js?key=AIzaSyAKYVzO5rPM1Gqq8crmt9Bku0uvKhr7t4g&sensor=true");

	//Маска ввода
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.inputmask/js/inputmask.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.inputmask/js/jquery.inputmask.min.js");

	/*VUE JS*/
	Asset::getInstance()->addJs("https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.min.js");
	Asset::getInstance()->addJs("https://cdn.jsdelivr.net/npm/vue-resource@1.3.3");

	//Основные скрипты
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/Melon.js");
	?>
	<script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-55392832-1', 'auto');
        ga('send', 'pageview');
	</script>

</head>
<body data-spy="scroll" data-target="#myScrollspy" data-offset="300">
<? $APPLICATION->ShowPanel() ?>

<? global $USER; ?>
<? if ($USER->IsAdmin() || in_array(7, $USER->GetUserGroup($USER->GetID())) || in_array(5, $USER->GetUserGroup($USER->GetID()))): ?>
	<style>
		.navbar-transparent {
			top: auto;
		}

		.navbar-nav > div {
			display: inline;
		}
	</style>
<? endif; ?>
<div>
	<div class="top-line">
		<div class="container">
			<div class="col-md-6 col-sm-6 hidden-xs">
<!--				--><? if ($_SESSION['CITY']['UF_ISO'][0] === "RU-BA"): ?>
					<p><i class="fa fa-home"></i> г. Уфа, пр. Октября, 52</p>
<!--				--><? endif; ?>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-md-right text-sm-right text-lg-right">
				<div>
					<p>
						<i class="fa fa-phone"></i> <a href="tel:<?=PHONE?>"><?=PHONE?></a>
						&nbsp;
						<a href="#callback" class="btn btn-callback">Заказать звонок</a>

					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- Menu-->
	<nav class="navbar navbar-custom  navbar-transparent demo-section">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">
					<img src="<?=SITE_TEMPLATE_PATH?>/images/newtravel/logo-vek.svg" alt="Умныетуристы.рф" title="Умныетуристы.рф - сервис онлайн бронирования туров"/>
				</a>
			</div>
			<div class="collapse navbar-collapse" id="custom-collapse">
				<ul class="nav navbar-nav navbar-right">
					<!-- /Menu-->
					<? $APPLICATION->IncludeComponent(
						"bitrix:menu",
						"newtravel_multilevel",
						array(
							"ROOT_MENU_TYPE"        => "main",
							"MAX_LEVEL"             => "2",
							"CHILD_MENU_TYPE"       => "left",
							"USE_EXT"               => "N",
							"DELAY"                 => "N",
							"ALLOW_MULTI_SELECT"    => "N",
							"MENU_CACHE_TYPE"       => "A",
							"MENU_CACHE_TIME"       => "36000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS"   => array(),
							"COMPONENT_TEMPLATE"    => "newtravel_multilevel",
							"COMPOSITE_FRAME_MODE"  => "A",
							"COMPOSITE_FRAME_TYPE"  => "AUTO"
						),
						false
					); ?>
					<? if ($USER->IsAuthorized()):
						$name = trim($USER->GetFullName());
						if ( ! $name) {
							$name = trim($USER->GetLogin());
						}
						if (strlen($name) > 15) {
							$name = substr($name, 0, 12) . '...';
						}
						?>
						<!--noindex-->
						<li class="dropdown user_menu">
							<a class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown"><?=htmlspecialcharsbx($name)?></a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
								<li role="presentation"><a role="menuitem" tabindex="-1" href="/personal/order/">Моя заказы</a></li>
								<li role="presentation"><a role="menuitem" tabindex="-1" href="/personal/profile/edit.php">Изменить данные</a></li>
								<li role="presentation" class="divider"></li>
								<li role="presentation"><a role="menuitem" tabindex="-1" href="?logout=yes" style="padding-bottom: 10px;">Выйти</a></li>
							</ul>
						</li>
						<!--/noindex-->
					<? else: ?>
						<!--noindex-->
						<li class="top-bar-link">
							<a href="javascript:void(0)" rel="nofollow" data-toggle="modal" data-target="#auth_modal">Личный кабинет</a>
						</li>
						<!--/noindex-->
					<? endif ?>
				</ul>
			</div>
		</div>
	</nav>

	<? $APPLICATION->IncludeComponent(
		"newtravel:tours.notifications",
		".default",
		array(
			"IBLOCK_TYPE" => "simai",
			"IBLOCK_ID"   => "77", //77
			"FILTER_LINK" => "",
			"CACHE_TYPE"  => "A",
			"CACHE_TIME"  => "36000",
			"ALERT_TYPE"  => "warning"
		),
		false
	); ?>
	<? if ( ! IS_INDEX && ! IS_TOURS): ?>
		<? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "country_breadcrumb", Array(
			"START_FROM" => "0",    // Номер пункта, начиная с которого будет построена навигационная цепочка
			"PATH"       => "",    // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
			"SITE_ID"    => "-",    // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
		),
			false
		); ?>
		<div class="row">
			<div class="col-md-12 text-center text-uppercase">
				<h1 class="main-title"><? $APPLICATION->ShowTitle(false) ?></h1>
			</div>
		</div>
	<? endif; ?>

	<? if ( ! IS_INDEX && ! IS_HOTELS && SHOW_CONTAINER): ?>
	<div class="container">
		<? endif; ?>
			