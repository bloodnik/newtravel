<? if (SHOW_CONTAINER): ?>
	</div>
<? endif; ?>
<!-- footer -->
<div class="footer">
	<div class="container">
		<div class="row">
			<div class="topcontrol">
				<a href="#" title="To Top"><i class="fa fa-angle-up"></i></a>
			</div>

			<div class="footer-toogle">
				<i class="fa fa-plus"></i>
			</div>

			<div class="top-footer">
				<div class="col-md-9">
					<div class="col-md-4">
						<div class="footer-widget categories">
							<div class="footer-widget-title"><h3>Навигация по сайту</h3></div>

							<ul>
								<li><a href="/">Домой</a></li>
								<li>Туры</li>
								<li>> <a href="/tours/">Поиск тура</a></li>
								<li>> <a href="#">Умные рецепты</a></li>
								<li>> <a href="/hot-tours/">Горящие туры</a></li>
								<li><a href="/hotels/">Страны</a></li>
								<li><a href="/communication/blogs/">Блоги</a></li>
								<li><a href="/about/">О проекте</a></li>
								<li><a href="/faq/">Помощь</a></li>
								<li><a href="/about/contacts.php/">Контакты</a></li>
							</ul>
						</div>
					</div>

					<div class="col-md-4">
						<div class="footer-widget">
							<div class="footer-widget-title"><h3>Блоги</h3></div>
							<div class="footer-widget-recent-posts">
								<? $APPLICATION->IncludeComponent(
									"bitrix:news.list",
									"popular_posts",
									array(
										"DISPLAY_DATE"                    => "Y",
										"DISPLAY_NAME"                    => "Y",
										"DISPLAY_PICTURE"                 => "Y",
										"DISPLAY_PREVIEW_TEXT"            => "N",
										"AJAX_MODE"                       => "N",
										"IBLOCK_TYPE"                     => "simai",
										"IBLOCK_ID"                       => "75",
										"NEWS_COUNT"                      => "5",
										"SORT_BY1"                        => "ACTIVE_FROM",
										"SORT_ORDER1"                     => "DESC",
										"SORT_BY2"                        => "NAME",
										"SORT_ORDER2"                     => "ASC",
										"FILTER_NAME"                     => "arrFilter",
										"FIELD_CODE"                      => array(
											0 => "SHOW_COUNTER",
											1 => "",
										),
										"PROPERTY_CODE"                   => array(
											0 => "",
											1 => "",
										),
										"CHECK_DATES"                     => "Y",
										"DETAIL_URL"                      => "",
										"PREVIEW_TRUNCATE_LEN"            => "",
										"ACTIVE_DATE_FORMAT"              => "d.m.Y",
										"SET_TITLE"                       => "N",
										"SET_BROWSER_TITLE"               => "Y",
										"SET_META_KEYWORDS"               => "Y",
										"SET_META_DESCRIPTION"            => "Y",
										"SET_STATUS_404"                  => "N",
										"INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
										"ADD_SECTIONS_CHAIN"              => "N",
										"HIDE_LINK_WHEN_NO_DETAIL"        => "N",
										"PARENT_SECTION"                  => "",
										"PARENT_SECTION_CODE"             => "",
										"INCLUDE_SUBSECTIONS"             => "N",
										"CACHE_TYPE"                      => "A",
										"CACHE_TIME"                      => "36000000",
										"CACHE_FILTER"                    => "N",
										"CACHE_GROUPS"                    => "Y",
										"PAGER_TEMPLATE"                  => ".default",
										"DISPLAY_TOP_PAGER"               => "N",
										"DISPLAY_BOTTOM_PAGER"            => "N",
										"PAGER_TITLE"                     => "Новости",
										"PAGER_SHOW_ALWAYS"               => "N",
										"PAGER_DESC_NUMBERING"            => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL"                  => "N",
										"AJAX_OPTION_JUMP"                => "N",
										"AJAX_OPTION_STYLE"               => "Y",
										"AJAX_OPTION_HISTORY"             => "N",
										"COMPONENT_TEMPLATE"              => "adapt_popular_country_posts",
										"AJAX_OPTION_ADDITIONAL"          => "",
										"SET_LAST_MODIFIED"               => "N",
										"COMPOSITE_FRAME_MODE"            => "Y",
										"COMPOSITE_FRAME_TYPE"            => "AUTO",
										"PAGER_BASE_LINK_ENABLE"          => "N",
										"SHOW_404"                        => "N",
										"MESSAGE_404"                     => ""
									),
									$component
								); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<? $APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW"      => "sect",
							"AREA_FILE_SUFFIX"    => "inc_copyright",
							"AREA_FILE_RECURSIVE" => "Y",
							"EDIT_TEMPLATE"       => ""
						)
					); ?>
				</div>
				<div class="col-md-3">
					<? $APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW"      => "sect",
							"AREA_FILE_SUFFIX"    => "inc_social_icons",
							"AREA_FILE_RECURSIVE" => "Y",
							"EDIT_TEMPLATE"       => ""
						)
					); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!--noindex-->
<? if ( ! $USER->IsAuthorized()): ?>
	<div class="modal fade" id="auth_modal" tabindex="-1" role="dialog" aria-labelledby="Авторизация">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Вход</h4>
				</div>
				<div class="modal-body">
					<? $APPLICATION->IncludeComponent(
						"bitrix:system.auth.form",
						"newtravel",
						Array(
							"COMPONENT_TEMPLATE"   => ".default",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
							"FORGOT_PASSWORD_URL"  => "",
							"PATH_TO_MYPORTAL"     => "desktop.php",
							"PROFILE_URL"          => "",
							"REGISTER_URL"         => "",
							"SHOW_ERRORS"          => "Y"
						)
					); ?>
				</div>
			</div>
		</div>
	</div>
<? endif; ?>
<!--/noindex-->

<script type="text/javascript">
    BX.ready(function () {
		/* ==========================================================================
		 Read_more_info
		 ========================================================================== */
        $('#read_more_seo').click(
            function () {
                $(".fst-wrap").css("height", "auto");
                $(this).css("display", "none");
            }
        );
    });
</script>

<!--noindex-->
<!-- Thank you modal-->
<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/thank_you_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>

<!-- Callback modal-->
<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/callback_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>
<!-- Callback modal-->
<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/pick_me_tour_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>

<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/review_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>

<?$APPLICATION->IncludeComponent(
	"newtravel.search:tours.basket.small",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "",
		"IBLOCK_TYPE" => "content",
		"REQUEST_ID" => "1",
		"TOUR_ID" => "1"
	)
);?>


<!--/noindex-->

<?$useWebvisor = $APPLICATION->GetCurDir() === '/tours/' ? 'false': true; //Исползьзовать ли вебвизор на странице?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter24395911 = new Ya.Metrika({
                    id: 24395911,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: <?=$useWebvisor?>,
                    trackHash: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
	<div><img src="https://mc.yandex.ru/watch/24395911" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->


<!--LiveInternet counter-->
<script
		type="text/javascript">new Image().src = "//counter.yadro.ru/hit?r" + escape(document.referrer) + ((typeof(screen) == "undefined") ? "" : ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ? screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) + ";" + Math.random();
</script>
<!--/LiveInternet-->

<!--Подлючение Callhunter-->
<script type="text/javascript" src="//cdn.callbackhunter.com/cbh.js?hunter_code=1d2d367c83b2b587788245248e556902" charset="UTF-8"></script>
<!--/Подлючение Callhunter-->

<script charset="UTF-8" src="//cdn.sendpulse.com/js/push/00672d5c1961d1b761edb3cf12a24215_0.js" async></script>


<script type="text/javascript" src="//vk.com/js/api/openapi.js?150"></script>

<!-- VK Widget -->
<div id="vk_community_messages"></div>
<script type="text/javascript">
    VK.Widgets.CommunityMessages("vk_community_messages", 7500134, {widgetPosition: "left",disableExpandChatSound: "1",tooltipButtonText: "Есть вопрос?"});
</script>

<!--Bitrix 24 OpenLines-->
<!--<script data-skip-moving="true">-->
<!--    (function(w,d,u,b){-->
<!--        s=d.createElement('script');r=(Date.now()/1000|0);s.async=1;s.src=u+'?'+r;-->
<!--        h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);-->
<!--    })(window,document,'https://cdn.bitrix24.ru/b427459/crm/site_button/loader_2_gmgk2i.js');-->
<!--</script>-->
<!--/Bitrix 24 OpenLines-->


</body >

</html >
