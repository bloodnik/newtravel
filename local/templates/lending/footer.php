<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
?>

<? if ( false): ?>
	<div class="container specialists">
		<div class="row">
			<div class="col-md-12">
				<h2 class="right-line text-uppercase">Наши специалисты</h2>
				<ul class="timeline-2">
					<li class="">
						<time class="timeline-time">
							<?=PHONE?>
							<span>Лиля</span>
						</time>
						<i class="timeline-2-point"></i>
						<div class="panel panel-default">
							<div class="panel-heading">Я действительно люблю путешествовать!</div>
							<div class="panel-body">
								<div class="col-md-6">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/newtravel/manager/m1.jpg" alt="" class="img-responsive">
									<p> За 10 лет работы в туризме я побывала во многих странах мира. С появлением детей я стала более тщательно подходить к
										отдыху за границей. Выбираю только проверенные страны и отели. Как профессионал в сфере туризма, и как мама, для отдыха с детьми я рекомендую Доминикану.
										Здесь все тоже самое, что в Турции, но в разы лучше! Питание все включено, шикарные песчаные пляжи, анимация и аквапарки и нереальное Карибское море!
									</p>
								</div>
								<div class="col-md-6">

								</div>
							</div>
						</div>
					</li>
					<li class="">
						<time class="timeline-time">
							<?=PHONE?>
							<span>Анна</span>
						</time>
						<i class="timeline-2-point"></i>
						<div class="panel panel-default">
							<div class="panel-heading">В силу своей профессии была во многих странах.</div>
							<div class="panel-body">
								<div class="col-md-6">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/newtravel/manager/m4.jpg" alt="" class="img-responsive">
									<p>
										Но моё сердце принадлежит Вьетнаму! Ведь только там можно совместить пляжный отдых (белый песок просто волшебен!),
										приключения и отличный и бюджетный шоппинг. А ещё во Вьетнаме есть Азиатский Диснейленд и я знаю о нем практически все!
									<p>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
					</li>
					<li class="">
						<time class="timeline-time">
							<?=PHONE?>
							<span>Гузель</span>
						</time>
						<i class="timeline-2-point"></i>
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="col-md-6">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/newtravel/manager/m5.jpg" alt="" class="img-responsive">
									<p>
										Шенгенская виза, визы в Канаду, визы в США - легко! Хотя оформление виз дело серьезное, особенно в Канаду и США, но результат того стоит - незабываемый отдых и
										море впечатлений на всю жизнь. Ниагарский водопад, Диснейленд и Лос Анжелес и многое-многое другое!
									<p>
								</div>
								<div class="col-md-6">

								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
<? endif; ?>

</div>
</div>

<footer id="footer">
	<p>&copy; 2016 <a href="/">Умныетуристы.рф</a>, Все права защищены.</p>
</footer>

<div style="display:none">
	<div id="feedbackForm">

		<div class="col-md-12 text-center">
			<h3 id="feedbackTitle">Задай вопрос эксперту!</h3>
		</div>
		<script id="bx24_form_inline" data-skip-moving="true">
            (function (w, d, u, b) {
                w['Bitrix24FormObject'] = b;
                w[b] = w[b] || function () {
                        arguments[0].ref = u;
                        (w[b].forms = w[b].forms || []).push(arguments[0])
                    };
                if (w[b]['forms']) return;
                s = d.createElement('script');
                r = 1 * new Date();
                s.async = 1;
                s.src = u + '?' + r;
                h = d.getElementsByTagName('script')[0];
                h.parentNode.insertBefore(s, h);
            })(window, document, 'https://umnik.bitrix24.ru/bitrix/js/crm/form_loader.js', 'b24form');

            b24form({"id": "14", "lang": "ru", "sec": "ow5tkx", "type": "inline"});
		</script>
	</div>
</div>

<?$APPLICATION->IncludeComponent(
	"newtravel.search:tours.basket.small",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "",
		"IBLOCK_TYPE" => "content",
		"REQUEST_ID" => "1",
		"TOUR_ID" => "1"
	)
);?>


<!-- Thank you modal-->
<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/thank_you_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>

<!-- Callback modal-->
<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/callback_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>
<!-- Callback modal-->
<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/pick_me_tour_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>

<? $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include/hot_tours_modal.php"),
	Array(),
	Array("MODE" => "html", "SHOW_BORDER" => false)
);
?>


<script>
    $(document).ready(function () {

        //Открываем форму заказа звонка
        $('a[href="#callback"]').on('click', function () {
            $('#callBackModal').modal('show');
        });

        //Открываем форму подбора
        $('a[href="#pickmetour"]').on('click', function () {
            $('#PickMeTourModal').modal('show');
        });


        //Открываем форму заказа звонка
        $('a[href="#hot_tours_modal"]').on('click', function () {
            $('#hotToursModal').modal('show');
        });


        $("#callbackModal, #feedbackModal, #feedbackModal2").fancybox({
            'hideOnContentClick': true,
            padding: 0,
            showCloseButton: false,
            autoScale: true,
            autoDimensions: true,
            width: 100,
            afterLoad: function () {
                var title = $(this.element).data('title');
                $("#feedbackTitle").text(title);
            }
        });
    });
</script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter24395911 = new Ya.Metrika({
                    id: 24395911,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
	<div><img src="https://mc.yandex.ru/watch/24395911" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

<!--LiveInternet counter-->
<script
		type="text/javascript">new Image().src = "//counter.yadro.ru/hit?r" + escape(document.referrer) + ((typeof(screen) == "undefined") ? "" : ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ? screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) + ";" + Math.random();
</script>
<!--/LiveInternet-->

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-55392832-1', 'auto');
    ga('send', 'pageview');
</script>

<!--Подлючение Callhunter-->
<script type="text/javascript" src="//cdn.callbackhunter.com/cbh.js?hunter_code=1d2d367c83b2b587788245248e556902" charset="UTF-8"></script>
<!--/Подлючение Callhunter-->
</body>
</html>
