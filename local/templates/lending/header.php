<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Newtravel\Search\DefineGeo;

if ( ! isset($_SESSION['CITY']) || empty($_SESSION['CITY'])) {
	Loader::includeModule('newtravel.search');
	DefineGeo::setRegionIso();
	DefineGeo::setCity();
}
?>
<? define("PHONE", strlen($_SESSION['CITY']['UF_PHONE']) > 0 ? $_SESSION['CITY']['UF_PHONE'] : "8-800-505-47-61")

?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<meta property="og:type" content="website">
	<meta property="og:site_name" content="Умные туристы">
	<meta property="og:title" content="Умныетуристы.рф - сервис бронирования выгодных туров онлайн, покупка туров онлайн из Москвы по всем направлениям">
	<meta property="og:description" content="Умныетуристы.рф - сервис бронирования выгодных туров онлайн, покупка туров онлайн из Москвы по всем направлениям.">
	<meta property="og:url" content="https://xn--e1agkeqhedbe9fh.xn--p1ai/">
	<meta property="og:locale" content="ru_RU">
	<meta property="og:image" content="https://xn--e1agkeqhedbe9fh.xn--p1ai/local/templates/promo_pages/assets/i/logo.png">


	<link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico"/>

	<title><? $APPLICATION->ShowTitle() ?></title>
	<?
	$APPLICATION->ShowHead();


	//CSS
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/preload.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/vendors.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/syntaxhighlighter/shCore.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/style-blue.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/width-full.css");

	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/star-rating/css/star-rating.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/jquery.countdown/jquery.countdown.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/fancybox/jquery.fancybox.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/mCustomScrollbar/jquery.mCustomScrollbar.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/slider/css/slider.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/selectboxit.min.css");


	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/custom.css");
	//JS
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/vendors.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/syntaxhighlighter/shCore.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/syntaxhighlighter/shBrushXml.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/syntaxhighlighter/shBrushJScript.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/app.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/home_profile.js");


	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-ui.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/bootstrap.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.cookie.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/fancybox/jquery.fancybox.pack.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/star-rating/js/star-rating.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.countdown/jquery.plugin.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.countdown/jquery.countdown.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.countdown/jquery.countdown-ru.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/slider/js/bootstrap-slider.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/daterangepicker/moment.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jQuery.AutoColumnList/jquery.autocolumnlist.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.scrollTo-min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/selectboxit.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.inputmask/js/inputmask.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.inputmask/js/jquery.inputmask.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/tourvisor/full-search.js");

	/*VUE JS*/
	Asset::getInstance()->addJs("https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.min.js");
	Asset::getInstance()->addJs("https://cdn.jsdelivr.net/npm/vue-resource@1.3.3");
	?>

	<!--[if lt IE 9]>
	<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/html5shiv.min.js");?>
	<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/respond.min.js");?>
	<![endif]-->
</head>
<body>
<? $APPLICATION->ShowPanel() ?>
<div id="preloader">
	<div id="status">&nbsp;</div>
</div>

<div id="sb-site">
	<div class="boxed">

		<header id="header-full-top" class="header-full">
			<div class="container">
				<a href="/">
					<div class="header-full-title">
						<h1 class="animated fadeInRight"></h1>
						<p class="animated fadeInRight"></p>
					</div>
				</a>
				<div class="header-block">
					<div class="phone">+7347-216-30-51</div>
					<div class="address hidden-xs">г. Уфа, ул. Гафури, д 54</div>
					<div class="pull-right hidden-xs">
						<a class="btn btn-ar btn-primary btn-lg animated fadeInLeft animation-delay-2" href="#callback">Заказать звонок</a>
					</div>
				</div>
			</div> <!-- container -->
		</header> <!-- header-full -->