<!--Модальное окно формы подбора тура-->
<div class="modal fade" id="callBackModal" tabindex="-1" role="dialog" aria-labelledby="callBackModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="callBackModalLabel">Заказать звонок</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<? $APPLICATION->IncludeComponent(
						"bitrix:form.result.new",
						"callback_from",
						Array(
							"AJAX_MODE"              => "N",
							"SEF_MODE"               => "N",
							"WEB_FORM_ID"            => "9",
							"LIST_URL"               => "",
							"EDIT_URL"               => "",
							"SUCCESS_URL"            => "",
							"CHAIN_ITEM_TEXT"        => "",
							"CHAIN_ITEM_LINK"        => "",
							"IGNORE_CUSTOM_TEMPLATE" => "N",
							"USE_EXTENDED_ERRORS"    => "N",
							"CACHE_TYPE"             => "A",
							"CACHE_TIME"             => "3600",
							"VARIABLE_ALIASES"       => Array(
								"WEB_FORM_ID" => "WEB_FORM_ID",
								"RESULT_ID"   => "RESULT_ID"
							),
						),
						false
					); ?>
				</div>
			</div>
		</div>
	</div>
</div>