<!--Модальное окно формы подбора тура-->
<div class="modal fade" id="PickMeTourModal" tabindex="-1" role="dialog" aria-labelledby="PickMeTourLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="PickMeTourLabel">Подберите мне тур</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<? $APPLICATION->IncludeComponent(
						"bitrix:form.result.new",
						"pick_me_tour",
						Array(
							"CACHE_TIME"             => "3600",
							"CACHE_TYPE"             => "A",
							"CHAIN_ITEM_LINK"        => "",
							"CHAIN_ITEM_TEXT"        => "",
							"EDIT_URL"               => "",
							"IGNORE_CUSTOM_TEMPLATE" => "N",
							"LIST_URL"               => "",
							"SEF_MODE"               => "N",
							"SUCCESS_URL"            => "",
							"USE_EXTENDED_ERRORS"    => "N",
							"VARIABLE_ALIASES"       => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
							"WEB_FORM_ID"            => "4"
						)
					); ?>
				</div>
			</div>
		</div>
	</div>
</div>