<div class="modal fade" id="thankYouModal" tabindex="-1" role="dialog" aria-labelledby="TULabel" aria-hidden="true">
	<div class="modal-dialog moadal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 text-center text-uppercase">
						<h2>Спасибо за заявку!</h2><br>
						<i class="fa fa-check-circle-o fa-5x" style="color: #88bd23;" aria-hidden="true"></i>
					</div>
				</div>
				<br>
				<div class="text-center">
					<h3>Также можете подписаться на наши соц.сети</h3>
				</div>
				<div class="row text-center">
					<div class="col-md-3">
						<a href="https://vk.com/umnie_turisti" target="_blank"><i class="fa fa-vk fa-3x" style="color: #507299;"></i></a>
					</div>
					<div class="col-md-3">
						<a href="http://ok.ru/group/52199923187928" target="_blank"><i class="fa fa-odnoklassniki fa-3x" style="color: #EE8208;"></i></a>
					</div>
					<div class="col-md-3">
						<a href="https://www.facebook.com/groups/umnyetouristy/" target="_blank"><i class="fa fa-facebook-official fa-3x" style="color: #3B5998;"></i></a>
					</div>
					<div class="col-md-3">
						<a href="https://www.instagram.com/umnye__turisty/" target="_blank"><i class="fa fa-instagram fa-3x" style="color: #3B5998;"></i></a>
					</div>
				</div>
				<br>
			</div>
		</div>
	</div>
</div>