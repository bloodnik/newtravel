<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/plugins/gmaps.min.js");
?>

<?
$arBannersRange = array(
	2 => 0,
	6 => 1,
	9 => 2
);
?>

<? if ( ! empty($arResult["TOURS"]["hotel"])): ?>
	<div class="col-xs-12 text-center" style="margin-bottom: 5px">
		<a href="#" class="small" onclick="$('.tour-item-footer').slideToggle()">развернуть все туры</a>
		<span class="small" style="color: #cd0a0a; margin-left: 10px">цены указаны с учетом топливных сборов</span>
	</div>

	<div class="col-xs-12 tours-section">
		<!--Результаты поиска туров-->
		<div class="tour-list-wrap">
			<ul class="tour-list">
				<? foreach ($arResult["TOURS"]["hotel"] as $key => $arHotel): ?>
					<li class="tour-item" data-hotel-code="<?=$arHotel['hotelcode']?>">
						<!--Увеличенные изображения отеля-->
						<div id="fullImg_<?=$arHotel["hotelcode"]?>" style="z-index: 2; position: absolute; display: none;right: 1%;background-color: rgba(255, 255, 255, 0.8);width: 75%;height: 12%;">
							<img src="#" style="width: 400px;    margin-left: 10%;margin-top: 10%;"/>
						</div>

						<!--Шапка элемента тура-->
						<div class="col-xs-12 col-md-2 tour-img">
							<noindex><a href="/hotel/<?=$arHotel["hotelcode"]?>/?start=Y" target="_blank" rel="nofollow" class="hotel-link"><img
										src="<?=$arHotel["picturelink"]?>"
										alt="<?=$arHotel["hotelname"]?>"/></a></noindex>
						</div>
						<div class="col-xs-12 col-md-7">
							<div class="item-hotel-stars">
								<?
								switch ($arHotel['hotelstars']) {
									case "1":
										echo '<i class="fa fa-star"></i>';
										break;
									case "2":
										echo '<i class="fa fa-star"></i><i class="fa fa-star"></i>';
										break;
									case "3":
										echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
										break;
									case "4":
										echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
										break;
									case "5":
										echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
										break;
									default:
										echo "";
										break;
								}
								?>
							</div>
							<div class="item-hotel-name">
								<noindex><a target="_blank" href="/hotel/<?=$arHotel["hotelcode"]?>/?start=Y" rel="nofollow" class="hotel-link"><?=$arHotel["hotelname"]?></a></noindex>
							</div>

							<span class="tour-item-resort"><?=$arHotel["countryname"]?>, <?=$arHotel["regionname"]?></span>

							<div class="tour-item-info small">
								<p>
									<?=$arHotel["hoteldescription"]?>
								</p>
								<div class="tour-buttons">
									<noindex><a href="javascript:void(0)" onclick="openHotelDesc('<?=$arHotel["hotelcode"]?>')" rel="nofollow" class="btn btn-sm btn-white" target="_blank">Об отеле</a>
									</noindex>
									<a href="javascript:void(0)" onclick="$('.hotel_<?=$arHotel["hotelcode"]?>').slideUp();$('.tours_<?=$arHotel["hotelcode"]?>').slideToggle()"
									   class="btn btn-sm btn-white hidden-xs">Цены</a>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-2 right-group">
							<div class="row">
								<div class="col-md-12 col-xs-6">
									<div class="row">
										<div class="item-rating text-left">
											<div class="small">
												Рейтинг
											</div>
											<span><?=$arHotel["hotelrating"]?></span>
										</div>
									</div>
								</div>
								<div class="col-md-12 col-xs-6">
									<div class="row">
										<div class="tour-item-min-price text-left small">
											Цена от:
                                <span
	                                onclick="$('.hotel_<?=$arHotel["hotelcode"]?>').slideUp();$('.tours_<?=$arHotel["hotelcode"]?>').slideToggle(); setOpenedTours('.tours_<?=$arHotel["hotelcode"]?>')"><?=CurrencyFormat($arHotel["price"], 'RUB')?>
	                                <i class="fa fa-chevron-down pull-right"></i></span>
											за тур

										</div>
									</div>
								</div>
							</div>
						</div>

						<!--Подвал элемента тура-->
						<div class="col-md-12 tour-item-footer tours_<?=$arHotel["hotelcode"]?> col-xs-12" style="display: none">
							<table class="table table-striped tours-table"
							       style="text-align: center !important;">
								<thead>
								<tr>
									<th></th>
									<th>тур</th>
									<th>вылет/ночей</th>
									<th>туристы</th>
									<th>номер/питание</th>
									<th>цена</th>
									<th></th>
								</tr>
								</thead>
								<tbody class="small">
								<? foreach ($arHotel["tours"]["tour"] as $arTour): ?>
									<tr>
										<td><img src="https://tourvisor.ru/pics/operators/searchlogo/<?=$arTour['operatorcode']?>.gif" alt="<?=$arTour['operatorname']?>"/>
										</td>
										<td><?=$arTour["tourname"]?></td>
										<td>
											<?=$arTour["flydate"]?><br/>
											<?=$arTour["nights"]?> ночи(-ей)
										</td>
										<td>
											<?=$arTour["adults"]?> взрослых
											<? if ($arTour["child"] > 0): ?>
												<br/>
												<?=$arTour["child"]?> детей
											<? endif ?>
										</td>
										<td>
											<?=$arTour["room"]?><br/>
											<?=$arTour["meal"]?> <?=$arTour["mealrussian"]?>
										</td>
										<td class="price"><?=CurrencyFormat($arTour["price"], 'RUB')?>
										</td>
										<td>
											<a href="/personal/order/make" target="_blank" class="btn btn-blue" onclick="buyTour('<?=$arParams["REQUEST_ID"]?>', '<?=$arTour["tourid"]?>')">КУПИТЬ</a>
										</td>
									</tr>
								<? endforeach; ?>
								</tbody>
							</table>
						</div>
						<!--/Подвал элемента тура-->

						<div class="col-md-12 tour-item-footer hotel_<?=$arHotel["hotelcode"]?> col-xs-12" style="display: none; padding-bottom: 15px">
							<hr/>
							<div class="col-md-3 images">

							</div>

							<div class="col-md-9 small">
								<p class="description"></p>
								<a target="_blank" href="/hotel/<?=$arHotel["hotelcode"]?>/?start=Y" rel="nofollow" class="hotel-link pull-right btn bnt-sm btn-blue" style="margin-bottom: 10px">Подробнее</a><br/>
							</div>
							<div class="col-md-12">
								<div id="mapintab_<?=$arHotel["hotelcode"]?>" style="height: 200px;"></div>
							</div>
						</div>
					</li>
					<? if (isset($arBannersRange[ $key ])): ?>
						<a href="<?=$arResult["BANNERS"][ $arBannersRange[ $key ] ]["PREVIEW_TEXT"]?>" target="_blank">
							<li class="tour-item banner" style="
								background: url('<?=CFile::GetPath($arResult["BANNERS"][ $arBannersRange[ $key ] ]["PREVIEW_PICTURE"])?>');
								background-size: cover;
								background-position: 51%;">
							</li>
						</a>
					<? endif; ?>
				<? endforeach; ?>
				<!--Форма подбора тура-->
				<li>
					<div class="text-center" id="tour-help-form-link" style="background: #00aeef; padding: 20px; color: #fff">
						Нужна помощь в подборе тура? <a href="javascript:void(0)" onclick="showTourHelpForm()" class="btn btn-white">Оставить заявку</a>
					</div>
					<div class="col-xs-12 tours-section" id="tour-help-form-conatainer" style="display: none">
						<? $APPLICATION->IncludeComponent(
							"bitrix:form.result.new",
							"tour_help",
							Array(
								"AJAX_MODE"              => "Y",
								"SEF_MODE"               => "N",
								"WEB_FORM_ID"            => "6",
								"LIST_URL"               => "/tours",
								"EDIT_URL"               => "/tours",
								"SUCCESS_URL"            => "",
								"CHAIN_ITEM_TEXT"        => "",
								"CHAIN_ITEM_LINK"        => "",
								"IGNORE_CUSTOM_TEMPLATE" => "N",
								"USE_EXTENDED_ERRORS"    => "N",
								"CACHE_TYPE"             => "A",
								"CACHE_TIME"             => "3600",
								"VARIABLE_ALIASES"       => Array(
									"WEB_FORM_ID" => "WEB_FORM_ID",
									"RESULT_ID"   => "RESULT_ID"
								),
							),
							false
						); ?>

					</div>
				</li>
				<!--/Форма подбора тура-->
			</ul>
		</div>
	</div>
<? elseif (empty($arResult["TOURS"]["hotel"]) && $arParams["IS_FINISH"] == "true"): ?>
	<div class="col-xs-12 tours-section">
		<? $APPLICATION->IncludeComponent(
			"bitrix:form.result.new",
			"tour_help_notfound",
			Array(
				"AJAX_MODE"              => "Y",
				"SEF_MODE"               => "N",
				"WEB_FORM_ID"            => "6",
				"LIST_URL"               => "/tours",
				"EDIT_URL"               => "/tours",
				"SUCCESS_URL"            => "",
				"CHAIN_ITEM_TEXT"        => "",
				"CHAIN_ITEM_LINK"        => "",
				"IGNORE_CUSTOM_TEMPLATE" => "N",
				"USE_EXTENDED_ERRORS"    => "N",
				"CACHE_TYPE"             => "A",
				"CACHE_TIME"             => "3600",
				"VARIABLE_ALIASES"       => Array(
					"WEB_FORM_ID" => "WEB_FORM_ID",
					"RESULT_ID"   => "RESULT_ID"
				),
			),
			false
		); ?>

	</div>
<? endif; ?>

<script>
	function openHotelDesc(hotelcode) {
		$(".tours_" + hotelcode).slideUp();
		$(".map_" + hotelcode).slideUp();

		$.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "fullhotel", hotelcode: hotelcode}, function (json) {

			$(".hotel_" + hotelcode).find(".description").html(json.data.hotel.description);

			$(".hotel_" + hotelcode).find(".images").empty();

			$(json.data.hotel.images.image).each(function (i, item) {
				$(".hotel_" + hotelcode).find(".images").append("<a href='" + item + "' class='fancybox'><img onmouseover='viewFullImg(this," + hotelcode + ")' src='" + item + "' style='height:40px; width:50px; margin-bottom:5px;margin-right:5px'></a>");
			});

			setTimeout(function () {
				mapintab = new GMaps({
					el: '#mapintab_' + hotelcode,
					height: 322,
					width: 100,
					scrollwheel: false,
					lat: json.data.hotel.coord1,
					lng: json.data.hotel.coord2
				});

				mapintab.addMarker({
					lat: json.data.hotel.coord1,
					lng: json.data.hotel.coord2,
					clickable: false,
					icon: "<?=SITE_TEMPLATE_PATH?>/images/mappin.png"
				});
			}, 500);
			$(".hotel_" + hotelcode).slideToggle();
		});
	}

	function viewFullImg(img, hotelcode) {
		$("#fullImg_" + hotelcode).css("display", "block")
		$("#fullImg_" + hotelcode + ">img").attr("src", $(img).attr("src"));
	}

	$(".images").on("mouseout", function () {
		$("div[id^='fullImg']").css("display", "none");
	});


	function showTourHelpForm() {
		$("#tour-help-form-link").slideUp(200);
		$("#tour-help-form-conatainer").slideDown(200);
	}

</script>