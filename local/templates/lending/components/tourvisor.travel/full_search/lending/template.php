<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>
<?
$userDeparture = isset($arResult["DEPARTURE"]) ? $arResult["DEPARTURE"] : $arParams["CITY"];

$departure = isset($_REQUEST["departure"]) && ($_REQUEST["departure"] > 0) ? $_REQUEST["departure"] : $userDeparture;
$country   = isset($_REQUEST["country"]) && ($_REQUEST["country"] > 0) ? $_REQUEST["country"] : $arParams["COUNTRY"];

$dateFrom  = isset($_REQUEST["datefrom"]) ? $_REQUEST["datefrom"] : date("d.m.Y", strtotime("+1 DAY"));
$dateTo    = isset($_REQUEST["dateto"]) ? $_REQUEST["dateto"] : date("d.m.Y", strtotime("+" . $arParams["DATE_TO"] . " DAY"));
$dateRange = isset($_REQUEST["daterange"]) ? $_REQUEST["daterange"] : $dateFrom . " - " . $dateTo;

$nightsFrom = isset($_REQUEST["nightsfrom"]) ? $_REQUEST["nightsfrom"] : $arParams["NIGHTSFROM"];
$nightsTo   = isset($_REQUEST["nightsto"]) ? $_REQUEST["nightsto"] : $arParams["NIGHTSTO"];
$stars      = isset($_REQUEST["stars"]) ? $_REQUEST["stars"] : $arParams["STARS"];
$rating     = isset($_REQUEST["rating"]) ? $_REQUEST["rating"] : "";
$meal       = isset($_REQUEST["meal"]) ? $_REQUEST["meal"] : "";
$adults     = isset($_REQUEST["adults"]) ? $_REQUEST["adults"] : $arParams["ADULTS"];
$child      = isset($_REQUEST["child"]) ? $_REQUEST["child"] : "0";
$childage1  = isset($_REQUEST["childage1"]) ? $_REQUEST["childage1"] : "0";
$childage2  = isset($_REQUEST["childage2"]) ? $_REQUEST["childage2"] : "0";
$childage3  = isset($_REQUEST["childage3"]) ? $_REQUEST["childage3"] : "0";

$priceFrom  = isset($_REQUEST["pricefrom"]) ? $_REQUEST["pricefrom"] : "0";
$priceTo    = isset($_REQUEST["priceto"]) ? $_REQUEST["priceto"] : "1000000";
$priceRange = $priceFrom . "," . $priceTo;

$hotactive = isset($_REQUEST["hotactive"]) ? $_REQUEST["hotactive"] : "";
$hotrelax  = isset($_REQUEST["hotrelax"]) ? $_REQUEST["hotrelax"] : "";
$hotfamily = isset($_REQUEST["hotfamily"]) ? $_REQUEST["hotfamily"] : "";
$hothealth = isset($_REQUEST["hothealth"]) ? $_REQUEST["hothealth"] : "";
$hotcity   = isset($_REQUEST["hotcity"]) ? $_REQUEST["hotcity"] : "";
$hotbeach  = isset($_REQUEST["hotbeach"]) ? $_REQUEST["hotbeach"] : "";
$hotdeluxe = isset($_REQUEST["hotdeluxe"]) ? $_REQUEST["hotdeluxe"] : "";

$regions    = explode(",", $_REQUEST["regions"]);
$subregions = explode(",", $_REQUEST["subregions"]);
$operators  = explode(",", $_REQUEST["operators"]);
$hotels     = explode(",", $_REQUEST["hotels"]);

$templateFolder = &$this->GetFolder();
?>

<!-- =========================================
Tour Section ( Slider )
========================================== -->
<!-- tour-section -->
<section class="gray-section" style="padding-top: 15px">
	<div class="container">
		<div class="tour-form-top row">
			<div class="row">
				<form class="form-inline" role="form" action="/tours" name="searchTour" id="searchTour"
				      method="GET" <? if ($arParams["NEW"] == "Y"): ?>target="new"<? endif; ?>>

					<input type="hidden" name="type" value="search">

					<div class="col-md-2 col-sm-12">
						<div class="row">
							<div class="input-group">
								<input type="hidden" name="departure" id="departure" value=""/>
								<label>Город вылета: </label>

								<div class="form-control pseudo-input" id="departureValue" href="#departureModal"></div>
							</div>
						</div>
					</div>
					<div class="col-md-2 col-sm-12">
						<div class="row">
							<div class="input-group">
								<input type="hidden" name="country" id="country" value=""/>
								<label>Страна: </label>

								<div class="form-control pseudo-input" id="countryValue" href="#countryModal"></div>
							</div>
						</div>
					</div>
					<div class="col-md-2 col-sm-12">
						<div class="row">
							<div class="input-group">
								<label>Дата вылета:</label>
								<input type="text" class="form-control" name="daterange" id="daterange" value="<?=$dateRange?>" style="height: 36px;"/>
							</div>
						</div>
					</div>
					<div class="col-md-2 col-xs-12">
						<div class="row">
							<div class="input-group">
								<label>Ночей: </label>
								<input type="hidden" name="nightsfrom" id="nightsfrom" value="<?=$nightsFrom?>"/>
								<input type="hidden" name="nightsto" id="nightsto" value="<?=$nightsTo?>"/>
								<div class="form-control pseudo-input" id="nightsValue">от <?=$nightsFrom?> до <?=$nightsTo?></div>
								<div id="nights-select">
									Выберите <span style='font-weight: bold;'>от:</span>
									<table>
										<tr>
											<td data-value="2">2</td>
											<td data-value="3">3</td>
											<td data-value="4">4</td>
											<td data-value="5">5</td>
											<td data-value="6">6</td>
											<td data-value="7">7</td>
											<td data-value="8">8</td>
										</tr>
										<tr>
											<td data-value="9">9</td>
											<td data-value="10">10</td>
											<td data-value="11">11</td>
											<td data-value="12">12</td>
											<td data-value="13">13</td>
											<td data-value="14">14</td>
											<td data-value="15">15</td>
										</tr>
										<tr>
											<td data-value="16">16</td>
											<td data-value="17">17</td>
											<td data-value="18">18</td>
											<td data-value="19">19</td>
											<td data-value="20">20</td>
											<td data-value="21">21</td>
											<td data-value="22">22</td>
										</tr>
										<tr>
											<td data-value="23">23</td>
											<td data-value="24">24</td>
											<td data-value="25">25</td>
											<td data-value="26">26</td>
											<td data-value="27">27</td>
											<td data-value="28">28</td>
											<td data-value="29">29</td>
										</tr>
									</table>
									<span class="no" id="nights-error" style="display:none; font-size: 75%;">Ночей "до" не может быть меньше "от"</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 col-sm-12">
						<div class="row">
							<div class="toursit-select input-group">
								<div class="tourists" style="display: none;">
									<div class="hotel-tour-form-line ">
										<label>Взрослых: </label>

										<div class="adultsCount">
											<input type="hidden" value="<?=$adults?>" name="adults" id="adults"/>

											<div class="adultsCount1 <? if ($adults == 1): ?>active<? endif; ?>"
											     data-count="1">
												<i class="fa fa-male fa-2x"></i>
											</div>
											<div class="adultsCount2 <? if ($adults == 2): ?>active<? endif; ?>"
											     data-count="2">
												<i class="fa fa-male fa-2x"></i>
												<i class="fa fa-male fa-2x"></i>
											</div>
											<div class="adultsCount3 <? if ($adults == 3): ?>active<? endif; ?>"
											     data-count="3">
												<i class="fa fa-male fa-2x"></i>
												<i class="fa fa-male fa-2x"></i>
												<i class="fa fa-male fa-2x"></i>
											</div>
											<div class="adultsCount4 <? if ($adults == 4): ?>active<? endif; ?>"
											     data-count="4">
												<i class="fa fa-male fa-2x"></i>
												<i class="fa fa-male fa-2x"></i>
												<i class="fa fa-male fa-2x"></i>
												<i class="fa fa-male fa-2x"></i>
											</div>
										</div>

										<label>Количество детей:</label>
										<a href="javascript:void(0)" id="noKids" class="pull-right small">убрать детей</a>

										<div class="kidsCount">
											<input type="hidden" value="<?=$child?>" name="child" id="child"/>

											<div class="kidsCount1 <? if ($child == 1): ?>active<? endif; ?>"
											     data-count="1">
												<i class="fa fa-child fa-2x"></i>
											</div>
											<div class="kidsCount2 <? if ($child == 2): ?>active<? endif; ?>"
											     data-count="2">
												<i class="fa fa-child fa-2x"></i>
												<i class="fa fa-child fa-2x"></i>
											</div>
											<div class="kidsCount3 <? if ($child == 3): ?>active<? endif; ?>"
											     data-count="3">
												<i class="fa fa-child fa-2x"></i>
												<i class="fa fa-child fa-2x"></i>
												<i class="fa fa-child fa-2x"></i>
											</div>
										</div>

									</div>
									<div class="hotel-tour-form-line kidsAge">
										<label>Возраст детей:</label> <br/>
										<select id="childage1" name="childage1" class="form-control">
											<? for ($i = 0; $i <= 15; $i++) { ?>
												<option value="<?=$i?>"<? if ($childage1 == $i): ?> selected<? endif; ?>><?=$i?></option>
											<? } ?>
										</select>
										<select id="childage2" name="childage2" class="form-control">
											<? for ($i = 0; $i <= 15; $i++) { ?>
												<option value="<?=$i?>"<? if ($childage2 == $i): ?> selected<? endif; ?>><?=$i?></option>
											<? } ?>
										</select>
										<select id="childage3" name="childage3" class="form-control">
											<? for ($i = 0; $i <= 15; $i++) { ?>
												<option value="<?=$i?>"<? if ($childage3 == $i): ?> selected<? endif; ?>><?=$i?></option>
											<? } ?>
										</select>
									</div>

									<button type="button" class="btn btn-ar btn-info button-save btn-block">Сохранить</button>
								</div>
								<label>Туристы: </label>

								<div class="form-control tourists-total"><span><?=$adults?> взр. <? if ($child > 0): ?><?=$child?> реб.<? endif; ?></span></div>
							</div>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="form-group" style="  margin-top: 30px; width: 100%">
							<noindex>
								<button type="button" onclick="searchTours(this)" class="btn btn-ar btn-primary pull-right animated pulse infinite" style="width: 100%">Искать <i class="fa
								fa-search"></i></button>
							</noindex>
						</div>
					</div>
				</form>
			</div>
			<div class="row">
				<a href="javascript:void(0)" class="show-filter-link" onclick="showFilter();">скрыть/показать дополнительные параметры</a>
			</div>
		</div>

		<div class="row tour-body">
			<div class="tour-form-sidebar col-xs-12 col-md-3" style="display: none;">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<div class="col-xs-12">
							<label>Категория отеля: </label>
							<input id="stars" type="number" name="stars" class="rating" data-min="0" data-max="5"
							       data-step="1" data-size="xs" data-show-caption="false" data-show-clear="false">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<label>Категория питания: </label>
						</div>
						<div class="col-xs-12">
							<div class="row">
								<select class="form-control" name="meal" id="meal">
									<option value="1">Любое</option>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<label>Рейтинг отеля: </label>
						</div>
						<div class="col-xs-12">
							<div class="row">
								<select class="form-control" name="rating" id="rating">
									<option value="0">Любой</option>
									<option <? if ($rating == 2): ?>selected<? endif; ?> value="3">>3</option>
									<option <? if ($rating == 3): ?>selected<? endif; ?> value="3.5">>3.5</option>
									<option <? if ($rating == 4): ?>selected<? endif; ?> value="4">>4</option>
									<option <? if ($rating == 5): ?>selected<? endif; ?> value="4.5">>4.5</option>
								</select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-12">
							<label>Тип отеля: </label>
						</div>
						<div class="whiteBg" id="hoteltypes">
							<div class="col-xs-12">
								<label class="checkbox-inline" for="hotall"><input type="checkbox" value="" name="all" id="hotall">Любой</label>
								<label class="checkbox-inline" for="hotactive"><input type="checkbox" value="1" name="hotactive" <? if ($hotactive == 1): ?>checked<? endif; ?> id="hotactive"
								                                                      data-value="active">Активный</label>
							</div>
							<div class="col-xs-12">
								<label class="checkbox-inline" for="hotdeluxe"><input type="checkbox" value="1" name="hotdeluxe" <? if ($hotdeluxe == 1): ?>checked<? endif; ?>id="hotdeluxe"
								                                                      data-value="deluxe">Deluxe</label>
								<label class="checkbox-inline" for="hotrelax"><input type="checkbox" value="1" name="hotrelax" <? if ($hotrelax == 1): ?>checked<? endif; ?> id="hotrelax"
								                                                     data-value="relax">Спокойный</label>
							</div>
							<div class="col-xs-12">
								<label class="checkbox-inline" for="hotfamily"><input type="checkbox" value="1" name="hotfamily" <? if ($hotfamily == 1): ?>checked<? endif; ?> id="hotfamily"
								                                                      data-value="family">Семейный</label>
								<label class="checkbox-inline" for="hotcity"><input type="checkbox" value="1" name="hotcity" <? if ($hotcity == 1): ?>checked<? endif; ?> id="hotcity"
								                                                    data-value="city">Городской</label>
							</div>
							<div class="col-xs-12">
								<label class="checkbox-inline" for="hothealth"><input type="checkbox" value="1" name="hothealth" <? if ($hothealth == 1): ?>checked<? endif; ?> id="hothealth"
								                                                      data-value="health">Здоровье</label>
								<label class="checkbox-inline" for="hotbeach"><input type="checkbox" value="1" name="hotbeach" <? if ($hotbeach == 1): ?>checked<? endif; ?> id="hotbeach"
								                                                     data-value="beach">Пляжный</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-12 price-group">
							<label>Стоимость: </label>
						</div>
						<div class="col-xs-12">
							<input type="text" class="slider" value="" data-slider-min="0" data-slider-max="1000000" data-slider-step="1000" data-slider-value="[<?=$priceFrom?>,<?=$priceTo?>]"
							       data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide">
							<input type="hidden" class="form-control" id="priceRange" placeholder="от" aria-describedby="sizing-addon2" value="<?=$priceRange?>">
							<div class="row">
								<div class="col-xs-4 price-box" id="price-min"><?=CurrencyFormat($priceFrom, 'RUB')?></div>
								<div class="col-xs-4 col-xs-offset-4 price-box text-right" id="price-max"><?=CurrencyFormat($priceTo, 'RUB')?></div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-12">
							<label>Курорты: </label>
						</div>
						<div class="whiteBg customScroll">
							<ul id="regions">
							</ul>
						</div>
						<div class="col-xs-12 col-md-5">
							<label for="allhotel"><input type="checkbox" id="allregions" name="regions[]" onclick="clearCheckboxes(this, 'regions')" value="0" style="margin-right: 10px">Любой</label>
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-12">
							<label>Отели: </label>
						</div>
						<input type="text" style="background: #fff" class="form-control" placeholder="Поиск...Например 'ROSE HOTEL' " aria-describedby="sizing-addon2" name="hotelsName" value=""
						       id="hotelsName" onkeyup="filterHotel(this)">

						<div class="whiteBg customScroll">
							<ul id="hotels">

							</ul>
						</div>
						<div class="col-xs-12 col-md-5">
							<label for="allhotel"><input type="checkbox" id="allhotel" name="hotels[]" onclick="clearCheckboxes(this, 'hotels')" value="0" style="margin-right: 10px">Любой</label>
						</div>
						<div class="col-xs-12 col-md-7">
							<label for="showSelectedHotels"><input type="checkbox" id="showSelectedHotels" name="hotels[]" onclick="showOnlySelectedHotels(this)" value="0" style="margin-right: 10px">Выбраные
								отели</label>
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-12">
							<label>Туроператор: </label>
						</div>
						<div class="whiteBg customScroll">
							<ul id="operators">
							</ul>
						</div>
						<div class="col-xs-12 col-md-5">
							<label for="allhotel"><input type="checkbox" id="alloperators" name="regions[]" onclick="clearCheckboxes(this, 'operators')" value="0"
							                             style="margin-right: 10px">Любой</label>
						</div>
					</div>


					<div class="form-group">
						<noindex>
							<button type="button" class="btn btn-ar btn-info pull-right" onclick="searchTours()" style="width: 100%">Поехали</button>
						</noindex>
					</div>
				</form>

				<div class="row hidden-xs">

					<div class="col-xs-12"><label for="">Актуальность туров: </label></div>
					<div class="timer">
						<div class="defaultCountdown">

						</div>
					</div>

					<div class="col-xs-12"><label for="">В стоимость каждого тура входит: </label></div>

					<div class="whiteBg">
						<ul class="additional-list">
							<li><i class="fa fa-plane fa-2x"></i> <span>Авиаперелет туда-обратно</span></li>
							<li><i class="fa fa-arrow-circle-o-right fa-2x"></i> <span>Трансфер аэропорт-отель-аэропорт</span></li>
							<li><i class="fa fa-home fa-2x"></i> <span>Проживание в отеле</span></li>
							<li><i class="fa fa-list fa-2x"></i> <span>Питание в отеле на выбор</span></li>
							<li><i class="fa fa-plus fa-2x"></i> <span>Медицинская страховка</span></li>
						</ul>
					</div>
				</div>
			</div>

			<!-- =========================================
			Результаты
			========================================== -->
			<div class="col-xs-12 col-md-12 tour-form-content">

				<div class="col-xs-12 progress-section" style="display: none">
					<!--Результаты поиска туров-->
					<div class="col-md-4 col-sm-9">
						<div class="progress progress-striped active" style="display: block">
							<div class="progress-bar" style="width: 0%; color:#000"></div>
						</div>
					</div>
					<div class="col-md-2 col-sm-2" id="progress-percent">0%</div>
					<div class="col-md-3 col-xs-6" id="progress-status">идет поиск</div>
					<div class="col-md-3 col-xs-6" id="tours-found">0 туров найдено</div>
				</div>

				<div id="currentPage" style="display: none">1</div>
				<div id="requestid" style="display: none">0</div>

				<div id="request" style="display: none;"></div>

				<div class="more-tours-btn text-center" style="display: none;">
					<a href="javascript:void(0)" class="btn btn-ar btn-info" id="more-tours">Показать еще...</a>
					<br>
					<br>
				</div>

				<? if (isset($_REQUEST["formresult"]) && $_REQUEST["formresult"] == "addok"): ?>
					<!--Результаты поиска туров-->
					<div class="tour-list-wrap" id="formok">
						<div class="tour-item">
							<div class="col-md-12 yes">
								Заявка на подбор успешно отправлена
							</div>
						</div>
					</div>
				<? endif; ?>

				<div style="clear:both;"></div>
			</div>

		</div>
	</div>
</section>

<!-----------------------------------------------------------------------------------------------------
     Модальное окно выбора города отправления
 ------------------------------------------------------------------------------------------------------>
<div style="display:none">
	<div id="departureModal">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Выберите город вылета</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-2 popularDepartures hidden-xs hidden-sm">
						<ul class="nt-list">
							<li><a href="javascript:void(0)" onclick="selectDeparture(1, 'Москва')">Москва</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(3, 'Екатеринбург')">Екатеринбург</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(6, 'Челябинск')">Челябинск</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(28, 'Оренбург')">Оренбург</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(7, 'Самара')">Самара</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(4, 'Уфа')">Уфа</a></li>
						</ul>
					</div>
					<div class="col-md-10 allDepartures">
						<div class="row">
							<div class="list customScroll">
								<ul class="nt-list multiColumn" id="departureList">
									<li class="divider"></li>
									<li><a href="#">Без перелета</a></li>
									<li class="divider">А</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<!-----------------------------------------------------------------------------------------------------
    Модальное окно выбора страны
 ------------------------------------------------------------------------------------------------------>
<div style="display:none">
	<div id="countryModal">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Выберите страну</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-3 popularCountry hidden-xs">
						<h5>Популярные страны</h5>
						<ul class="nt-list">
							<? foreach ($arResult["POPULAR_COUNTRIES"] as $key => $popCountry): ?>
								<li>
									<img src="<?=CFile::GetPath($popCountry["PICTURE"])?>" alt="<?=$popCountry["NAME"]?>"/>
									<a href="javascript:void(0)" onclick="selectCountry(<?=$key?>, '<?=$popCountry["NAME"]?>')"><?=$popCountry["NAME"]?></a>
								</li>
							<? endforeach; ?>
						</ul>
					</div>
					<div class="col-sm-9 allCountry">
						<h5>Выберите страну:</h5>
						<div class="row">
							<div class="list customScroll">
								<ul class="nt-list multiColumn" id="countryList">
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	//Глобальные переменные
	var nfrom = 0; //Ночей от
	var nto = 0; //Ночей до
	var openedtours = []; //уже открытые туры
	var hotelList = new Array(); //Масив загруженных отелей
	var checkedHotels = <?=CUtil::PhpToJSObject($hotels)?>; //Масив выбранных отелей
	var helpformmessage, userurl, urlParams; //Сообщение для подбора тура и ссылка пользователя на поиск
	var filterHide = true;

	//Событие при смене города отправления
	function selectDeparture(id, name) {
		$("#departure").val(id);
		$("#departureValue").text(name);
		$.fancybox.close();
		getCountries(id);

		getFlydates(id, $("#country").val());
	}

	//Событие при смене страны
	function selectCountry(id, name) {
		$("#country").val(id);
		$("#countryValue").text(name);
		$.fancybox.close();
		getRegions(id, "");
		getSubRegions(id, "");
		crateHotelParamString();

		getFlydates($("#departure").val(), id);

		checkedHotels = [];
	}

	//Запоминаем открытые туры в отеле
	function setOpenedTours(className) {
		setTimeout(function () {
			var display = $(className).css("display");
			if (display == "block") {
				openedtours.push(className);
			} else {
				openedtours.splice(className, 1);
			}
		}, 500);
	}
	//Востанавливием открытые туры в отеле
	function checkOpenedTours() {
		$(openedtours).each(function (key, item) {
			$(item).css("display", "block")
		})
	}

	//Запоминаем выбранные отели
	function hotelSelect(hotel) {
		var checked = $(hotel).prop("checked");
		var id = $(hotel).val();

		if (checked) {
			checkedHotels.push(id);
		} else {
			checkedHotels.splice(checkedHotels.indexOf(id), 1);
		}

		$("#allhotel").prop("checked", false);
	}

	//Показываем только выборанные отели
	function showOnlySelectedHotels(elem) {
		if ($(elem).prop("checked")) {
			$("#hotels").find(".list-item").each(function (index, item) {
				if (!$(item).prop("checked")) {
					$(this).parent().css("display", "none");
				}
			});
		} else {
			$("#hotels").find(".list-item").each(function (index, item) {
				$(this).parent().css("display", "block");
			});
		}
	}


	function showFilter() {
		if (filterHide) {
			filterHide = false;
			$('.tour-form-sidebar').slideDown(200);
			$('.tour-form-content').removeClass('col-md-12');
			$('.tour-form-content').addClass('col-md-9');
		} else {
			filterHide = true;
			$('.tour-form-sidebar').slideUp(200);
			$('.tour-form-content').removeClass('col-md-9');
			$('.tour-form-content').addClass('col-md-12');

		}
	}

	$(document).ready(function () {
		$("#countryValue, #departureValue").fancybox({
			'hideOnContentClick': true,
			padding: 0,
			showCloseButton: false,
			autoScale: true,
			autoDimensions: true,
			width: 100
		});

		/*=========================================================================
		 range slider
		 ========================================================================== */
		$('.slider').slider({});
		$(".slider").on("slide", function (slideEvt) {
			$("#price-min").text((slideEvt.value[0]).formatMoney(0, ' ', ' ') + " ₽");
			$("#price-max").text((slideEvt.value[1]).formatMoney(0, ' ', ' ') + " ₽");
			$("#priceRange").val(slideEvt.value);
		});

		//Звездный рейтинг
		$("#stars").rating("update", <?=$stars?>);

		var startSearch = "";
		startSearch = "<?=$_REQUEST["start"]?>";

		getDepartures(<?=$departure?>);
		getCountries(<?=$departure?>, <?=$country?>);
		getRegions(<?=$country?>, <?=CUtil::PhpToJSObject($regions)?>);
		getMeals(<?=$meal?>);
		getFlydates(<?=$departure?>, <?=$country?>);
		getOperators(<?=CUtil::PhpToJSObject($operators)?>);

		setTimeout(function () {
			getSubRegions(<?=$country?>, <?=CUtil::PhpToJSObject($subregions)?>);
			subRegionFilter();
			crateHotelParamString(<?=CUtil::PhpToJSObject($hotels)?>);
		}, 2000);

		setTimeout(function () {
			if (startSearch == 'Y') {
				searchTours();
			}
		}, 2500);

	});
</script>