<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Application;

$request    = Application::getInstance()->getContext()->getRequest();
$formresult = $request->get('formresult');
$form_id    = $request->get('WEB_FORM_ID');

$this->setFrameMode(true);


$this->addExternalCss('https://unpkg.com/vue-form-wizard/dist/vue-form-wizard.min.css');
$this->addExternalJs('https://unpkg.com/vue-form-wizard/dist/vue-form-wizard.js');

$this->addExternalCss(SITE_TEMPLATE_PATH . "/js/daterangepicker2/daterangepicker.min.css");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/daterangepicker2/jquery.daterangepicker.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/daterangepicker/moment-with-locales.js");

//Выбор ночей
$this->addExternalCss("/local/modules/newtravel.search/js/jquery.nights.select/jquery.nights.select.min.css");
$this->addExternalJs("/local/modules/newtravel.search/js/jquery.nights.select/jquery.nights.select.min.js");
//Выбор туристов
$this->addExternalCss("/local/modules/newtravel.search/js/jquery.tourists.select/jquery.tourists.select.min.css");
$this->addExternalJs("/local/modules/newtravel.search/js/jquery.tourists.select/jquery.tourists.select.min.js");
?>
	<style>
		.nav.nav-justified li {
			display: table-cell;
		}

		.nav.nav-justified li.active h4 {
			color: #ffffff;
		}

		[v-cloak] {
			display: none;
		}
	</style>

	<div class="col-md-12">
		<?=$arResult["FORM_NOTE"]?>
	</div>

<? if ($arResult["isFormNote"] != "Y") : ?>
	<div id="pick_form">

		<?=$arResult["FORM_HEADER"]?>
		<form-wizard @on-complete="onComplete"
		             shape="square"
		             title="Оставь заявку на подбор тура прямо сейчас и получи 3 лучших варианта через 15 минут"
		             subtitle=""
		             color="#3498db"
		             error-color="#ff4949">
			<tab-content title="КУДА" icon="fa fa-globe">

				<div class="row">
					<div class="col-md-12">
						<? if ($arResult["isFormErrors"] == "Y"): ?>
							<?=$arResult["FORM_ERRORS_TEXT"];?>

							<script>
                                $(document).ready(function () {
                                    $('#PickMeTourModal').modal('show');
                                })
							</script>

						<? endif; ?>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">

						<div class="from-group">
							<label>Откуда</label>
							<select class="form-control" v-model="departureString">
								<option v-for="item in obDepartures">{{item.namefrom}}</option>
							</select>
							<input type="hidden" name="form_text_88" :value="departureString">
						</div>

						<div class="from-group">
							<label>Выберите страну</label>
							<select class="form-control" v-model="countryString">
								<option v-for="item in obCountries">{{item.name}}</option>
							</select>
							<input type="hidden" name="form_text_89" :value="countryString">
						</div>
					</div>
				</div>
			</tab-content>
			<tab-content title="КОГДА" icon="fa fa-calendar-o" :before-change="validateSecondtStep">
				<div class="row">
					<div class="col-md-12 text-center dateRangeSelect">
						<label>Выберите даты</label>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group" :class="{'has-error' : dateStringHasError}">
							<input type="text" autocomplete="false" name="form_text_90" v-model="dateString" class="form-control daterange" placeholder="">
						</div>
					</div>
				</div>

				<div class="center">
					<div class="row">
						<div class="col-md-12 text-center">
							<label>Количество ночей</label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
						            <span class="input-group-btn">
						              <button type="button" class="btn btn-default btn-number" :disabled="nightsfrom == 1" @click="nightsfrom--">
						                  <span class="glyphicon glyphicon-minus"></span>
						              </button>
						            </span>
								<input type="text" name="form_text_91" class="form-control input-number" v-model="nightsfrom" min="1" max="10">
								<span class="input-group-btn">
						              <button type="button" class="btn btn-default btn-number" @click="nightsfrom++">
						                  <span class="glyphicon glyphicon-plus"></span>
						              </button>
						          </span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
									<span class="input-group-btn">
										<button type="button" class="btn btn-default btn-number" :disabled="nightsto == 1" @click="nightsto--">
											<span class="glyphicon glyphicon-minus"></span>
										</button>
									</span>
								<input type="text" name="form_text_92" class="form-control input-number" v-model="nightsto" min="1" max="10">
								<span class="input-group-btn">
										<button type="button" class="btn btn-default btn-number" @click="nightsto++">
											<span class="glyphicon glyphicon-plus"></span>
										</button>
									</span>
							</div>
						</div>

					</div>
				</div>

			</tab-content>

			<tab-content title="КТО" icon="fa fa-users">

				<div class="row">
					<div class="col-md-12 text-center dateRangeSelect">
						<label>Выберите количество туристов</label>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="form_text_93" v-model="touristString" class="form-control tourists-select" placeholder="">
						</div>
					</div>
				</div>

			</tab-content>

			<tab-content title="ФИНИШ" icon="fa fa-check" :before-change="validateLastStep">

				<div class="row">
					<div class="col-md-12">
						<div class="from-group" :class="{'has-error' : nameStringHasError}">
							<label>Как Вас зовут?</label>
							<input type="text" required name="form_text_86" v-model="name" class="form-control" placeholder="Ваше имя">
							<small class="text-danger" :class="{hidden : !nameStringHasError}">Введите Ваше имя</small>
						</div>
						<div class="from-group" :class="{'has-error' : phoneStringHasError}">
							<label>Ваш телефон</label>
							<input type="text" required name="form_text_87" v-model="phone" class="form-control phone" placeholder="+79271234567">
							<small class="text-danger" :class="{hidden : !phoneStringHasError}">Неверный номер телефона. Введите в формате +79271234567</small>
						</div>
						<div class="from-group">
							<label>Дополнительные пожелания</label>
							<textarea v-model="message" name="form_textarea_94" class="form-control" placeholder=""></textarea>
						</div>

					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<input type="checkbox" checked name="confirm-agreement" id="confirm-agreement"><label for='confirm-agreement'>Я согласен с <a href="/about/agreement/" target="_blank">условиями</a> обработки персональных данных</label>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<? if ($arResult["isUseCaptcha"] == "Y") : ?>
							<b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b>
							<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>"/>
							<div class="row">
								<div class="col-md-5">
									<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="120" height="34"/>
								</div>
								<div class="col-md-7">
									<input type="text" name="captcha_word" class="form-control"/>

								</div>
							</div>
							<br>
						<? endif; ?>
					</div>
				</div>
				<input type="hidden" name="web_form_apply" value="Y"/>
				<input type="hidden" name="web_form_apply" value="Y"/>
			</tab-content>

			<a class="btn btn-lg btn-blue" slot="prev">НАЗАД</a>
			<a class="btn btn-lg btn-blue" slot="next">ДАЛЬШЕ</a>
			<a class="btn btn-lg btn-success" slot="finish">ОТПРАВИТЬ</a>
		</form-wizard>
		<?=$arResult["FORM_FOOTER"]?>
	</div>
<? endif; ?>

<? if (isset($formresult) && (isset($form_id) && $form_id == 4)): ?>
	<script>
        $(document).ready(function () {
            setTimeout(function () {
                $('#thankYouModal').modal('show');
            }, 1000)
        })
	</script>
<? endif; ?>