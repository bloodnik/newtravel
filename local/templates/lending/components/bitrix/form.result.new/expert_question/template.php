<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>

<div class="col-md-12 form-wrap">
	<? if ($arResult["isFormErrors"] == "Y"): ?><?=$arResult["FORM_ERRORS_TEXT"];?><? endif; ?>

	<?=$arResult["FORM_NOTE"]?>

	<? if ($arResult["isFormNote"] != "Y") {
		?>
		<?=$arResult["FORM_HEADER"]?>

		<div class="col-md-12">
			<div class="row">
				<div class="from-group">
					<input type="text" class="form-control" name="form_text_63" placeholder="Ваше имя"/>
				</div>
			</div>
			<div class="row">
				<div class="from-group">
					<label></label>
					<input type="text" class="form-control phone" name="form_text_64" placeholder="Номер телефона"/>
				</div>
			</div>
			<div class="row">
				<div class="from-group">
					<label></label>
					<input type="text" class="form-control email" name="form_email_65" placeholder="Email адрес"/>
				</div>
			</div>
			<div class="row">
				<div class="form-group">
					<label></label>
					<textarea class="form-control" name="form_textarea_66" cols="55" rows="6" placeholder="Ваш вопрос"></textarea>
				</div>
			</div>
			
			<? if ($arResult["isUseCaptcha"] == "Y") : ?>
				<div class="row">
					<b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b>
					<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>"/>
					<p><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?></p>
					<div class="row">
						<div class="col-md-5">
							<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="120" height="34"/>
						</div>
						<div class="col-md-7">
							<input type="text" name="captcha_word" value="" class="form-control"/>
						</div>
					</div>
					<br>
				</div>
			<? endif; ?>

			<div class="form-group">
				<input type="hidden" class="form-control" name="form_text_67" value="http://newtravel.su/hotel/<?=$arParams['HOTEL_ID']?>/"/>
				<input type="hidden" name="form_text_69" class="NAME_FOR_CRM" value="Вопрос эксперту по отелям"/>
				<input type="hidden" name="web_form_apply" value="Y"/>
				<input type="submit" class="btn btn-blue pull-right" value="Отправить"/>
			</div>
		</div>
		<?=$arResult["FORM_FOOTER"]?>
	<? } ?>
</div>
<script>
	$(document).ready(function () {
		$('.phone').inputmask("+7(999) 999-99-99");
		//Если email пустой, то заполняем его по умолчанию
		$('.form-wrap form').on('submit', function () {
			var email = $(this).find('.email');
			if (email.val() == "") {
				email.val("user@noemail.net");
			}
			//Заполняем заголовок для CRM
			var phone = $(this).find('.phone');
			if (phone.val() != "") {
				$('.NAME_FOR_CRM').val("Вопрос эксперту по отелям: " + phone.val());
			}
		})
	});
</script>
