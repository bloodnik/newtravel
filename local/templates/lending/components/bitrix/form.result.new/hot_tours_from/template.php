<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Application;

$request    = Application::getInstance()->getContext()->getRequest();
$formresult = $request->get('formresult');
$form_id    = $request->get('WEB_FORM_ID');

$this->setFrameMode(true);
?>

<!--Результаты поиска туров-->
<div class="hottours-form-wrap">
	<div class="col-md-12">
		<? if ($arResult["isFormErrors"] == "Y"): ?>
			<?=$arResult["FORM_ERRORS_TEXT"];?>
			<script>
                $(document).ready(function () {
                    $('#hotToursModal').modal('show');
                })
			</script>

		<? endif; ?>
		<?=$arResult["FORM_NOTE"]?>

		<? if ($arResult["isFormNote"] != "Y") {
			?>
			<?=$arResult["FORM_HEADER"]?>

			<div class="col-md-12">
				<div class="row text-info">
					<div class="alert alert-info">
						<p>Интересует горящий тур? <br> Мы подберем самые выгодные предложения и перезвоним Вам в течение 15 минут!</p>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<div class="row">
					<div class="from-group">
						<label>Ваш номер телефона</label>
						<input type="text"
						       class="form-control"
						       name="form_text_52"
						       value="<?=$request->get('form_text_52')?>"
						       pattern="^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))"
						       maxlength='15'
						       placeholder="Телефон 7__________"
						       title="Номер должен содержать только цифры и начинаться с либо с 8, либо с +7"
						       required/>
					</div>
				</div>
			</div>
			<br>
			<div class="col-md-12">
				<div class="row">
					<? if ($arResult["isUseCaptcha"] == "Y") : ?>
						<b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b>
						<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>"/>
						<div class="row">
							<div class="col-md-5">
								<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="120" height="34"/>
							</div>
							<div class="col-md-7">
								<input type="text" name="captcha_word" class="form-control"/>
							</div>
						</div>
						<br>
					<? endif; ?>
				</div>
			</div>

			<div class="col-md-12">
				<div>
					<input type="checkbox" checked name="confirm-agreement" id="confirm-agreement_hottours"><label for='confirm-agreement_callback'>Я согласен с <a href="/about/agreement/" target="_blank">условиями</a> обработки персональных
						данных</label>
				</div>
			</div>

			<div class="col-md-12">
				<input type="hidden" name="web_form_apply" value="Y"/>
				<input type="submit" class="btn btn-blue pull-right" value="Уточнить"/>
			</div>
			<?=$arResult["FORM_FOOTER"]?>
		<? } ?>
	</div>
</div>

<? if (isset($formresult) && (isset($form_id) && $form_id == 5)): ?>
	<script>
        $(document).ready(function () {
            setTimeout(function () {
                $('#thankYouModal').modal('show');
            }, 1000)
        });
	</script>
<? endif; ?>


<script>
    $(document).ready(function () {
        //Если email пустой, то заполняем его по умолчанию
        $('.hottours-form-wrap form').on('submit', function (e) {
            if (!$('#confirm-agreement_hottours').prop('checked')) {
                e.preventDefault();
                alert('Подтвердите согласие с условиями обработки персональных данных');
                return false;
            }
        })
    });
</script>