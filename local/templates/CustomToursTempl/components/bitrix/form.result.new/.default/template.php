<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>

<? if ($arResult["isFormErrors"] == "Y"): ?><?=$arResult["FORM_ERRORS_TEXT"];?><? endif; ?>

<?=$arResult["FORM_NOTE"]?>

<? if ($arResult["isFormNote"] != "Y"): ?>
	<?=$arResult["FORM_HEADER"]?>
	<? if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y") : ?>

		<? if ($arResult["isFormTitle"]): ?>
			<h3><?=$arResult["FORM_TITLE"]?></h3>
		<? endif ?>

		<? if ($arResult["isFormImage"] == "Y"): ?>
			<a href="<?=$arResult["FORM_IMAGE"]["URL"]?>" target="_blank" alt="<?=GetMessage("FORM_ENLARGE")?>">
				<img src="<?=$arResult["FORM_IMAGE"]["URL"]?>"
				     <? if ($arResult["FORM_IMAGE"]["WIDTH"] > 300): ?>width="300"
				     <? elseif ($arResult["FORM_IMAGE"]["HEIGHT"] > 200): ?>height="200"<? else: ?><?=$arResult["FORM_IMAGE"]["ATTR"]?><? endif; ?> hspace="3" vscape="3" border="0"/></a>
		<? endif; ?>
	<? endif; ?>

	<? foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion): ?>
		<fieldset>
			<? if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])): ?>
				<span class="error-fld" title="<?=$arResult["FORM_ERRORS"][ $FIELD_SID ]?>"></span>
			<? endif; ?>
			<label for="name"><?=$arQuestion["CAPTION"]?>
				<? if ($arQuestion["REQUIRED"] == "Y"): ?>
					<span class="form-required starrequired">*</span>
				<? endif; ?>
			</label>

			<?=$arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />" . $arQuestion["IMAGE"]["HTML_CODE"] : ""?>

			<?=$arQuestion["HTML_CODE"]?>

		</fieldset>
	<? endforeach; ?>

	<br>

	<? if ($arResult["isUseCaptcha"] == "Y"): ?>
		<b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b>
		<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>"/>
		<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40"/>
		<?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?>
		<input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext"/>
	<? endif; ?>


	<button <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> class="btn" type="submit" name="web_form_submit" value="Y">
		<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>
	</button>

	<button class="btn btn_1" type="reset" value="Y">Сбросить</button>

	<div class="form-table__submissions"><span class="form-required starrequired">*</span> - Поля, обязательны для заполнения</div>

	<?=$arResult["FORM_FOOTER"]?>
<? endif; ?>