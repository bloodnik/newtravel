<?
/** @global CMain $APPLICATION */
define('STOP_STATISTICS', true);

use Bitrix\Main,
	Bitrix\Main\Loader,
	Bitrix\Sale,
	Bitrix\Main\Application;

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$request = Application::getInstance()->getContext()->getRequest();

$buy = $request->getPost('buy');

if (isset($buy) && $buy == "Y") {

	Loader::includeModule('sale');
	
	CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());

	//количество
	$quantity = $request->getPost('quantity');

	//ID товара (торговый каталог)
	$productId = $request->getPost('productId');

	// Получение корзины для текущего пользователя
	$basket = \Bitrix\Sale\Basket::loadItemsForFUser(
		\Bitrix\Sale\Fuser::getId(),
		\Bitrix\Main\Context::getCurrent()->getSite()
	);

	if ($item = $basket->getExistsItem('catalog', $productId)) {

		//Обновление товара в корзине
		$item->setField('QUANTITY', $item->getQuantity() + $quantity);
	} else {

		//Добавление товара
		$item = $basket->createItem('catalog', $productId);
		$item->setFields([
			'QUANTITY'               => $quantity,
			'CURRENCY'               => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
			'LID'                    => \Bitrix\Main\Context::getCurrent()->getSite(),
			'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
		]);
	}
	//Сохранение изменений
	$basket->save();

	echo 'success';

}


