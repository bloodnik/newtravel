<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>

<script>
	var ShopData = [];
	ShopData.Offers = <?=CUtil::PhpToJSObject($arResult['OFFERS_NEW'])?>;
	ShopData.redirectUrl = '<?=$arParams['BASKET_URL']?>';
</script>

<section class="head-section detail">
	<div class="cover" style="background: #000 url('<?=$arResult['DETAIL_PICTURE']['SRC']?>') center center / cover;"></div>
	<div class="container">
		<div class="promo-wrap text-center ">
			<? if ($arResult['DISPLAY_PROPERTIES']['TOUR_DATE']): ?>
				<div class="tour-date">
					Даты проведения <br>
					<?=$arResult['DISPLAY_PROPERTIES']['TOUR_DATE']['VALUE']?>
				</div>
			<? endif; ?>
			<h1><?=$arResult['NAME']?></h1>
		</div>

		<div class="col-md-9">
			<? if ($arResult['PREVIEW_TEXT']): ?>
				<div class="list">
					<?=$arResult['PREVIEW_TEXT']?>
				</div>
			<? endif; ?>
			<div class="clearfix"></div>
		</div>
		<div class="col-md-3 text-right">
			<h3 class="price">от <?=$arResult['CUR_OFFER']['PRICE']?> руб.</h3>
			<a href="#buy-section" class="btn btn-success btn-lg btn-goto">Купить</a>
		</div>
	</div>
</section>

<section class="description-section">
	<div class="container">
		<h2 class="section-header text-center text-uppercase">Описание тура</h2>
		<div class="text-center">
			<div class="section-subheader"><h3><span>Тебя ждёт незабываемый отдых</span></h3></div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<?=$arResult['DETAIL_TEXT']?>
			</div>
			<div class="col-md-6">
				<img src="<?=SITE_TEMPLATE_PATH?>/img/map.png" style="width: 100%;" alt="">
			</div>
		</div>
	</div>
</section>

<section class="line-section suhum">
	<div class="container">
		<div class="col-md-12 text-center">
			<h2>
				Хотите зарядиться энергией, поправить здоровье и начать заниматься и познавать практики сразу с одним из опытнейших практиков Башкирии <strong>Виталием Лялиным</strong>?
			</h2>
		</div>
	</div>
</section>

<section class="description-section">
	<div class="container">
		<h2 class="section-header text-center text-uppercase">Виталий Лялин</h2>
		<div class="row">
			<div class="col-md-12">
				<p><strong>Лялин Виталий Александрович</strong> — руководитель уфимской школы крийя-йоги.</p>
				<p>Тридцать пять лет своей жизни посвятил изучению восточных искусств. Начинал с боевых искусств — ушу, стиль змеи, тайцзицюань 24 формы ян. Двадцать пять лет практикует йогу, обучался у просветленных мастеров из Индии, двадцать лет
					преподает, десять лет ведет группу для инструкторов йоги.</p>
				<p>Редкий человек в эзотерических кругах Уфы не знает Виталия. В нем столько энергии и желания делиться с людьми опытом, знаниями и светом своей души, что круг его деятельности не ограничивается только ведением занятий по йоге и ушу.
					Виталий каждый год организует в Уфе Фестиваль восточных искусств, устраивает семинары по вегетарианской кухне, проводит сатсанги в чайном клубе, издает великолепные йога-календари. В Нуримановском районе Башкирии, в тихом
					уединенном
					месте Виталий построил ашрам – центр для занятий йогой и медитацией. В центре регулярно проводятся выездные семинары, туда приезжают учителя и мастера со всего мира, а в свободное время двери ашрама открыты для всех, кто
					занимается
					духовной практикой. «В мире нет ничего, кроме любви», — часто повторяет Виталий своим ученикам. Находясь рядом с ним, очень легко в это поверить, потому что именно такой мир он создает вокруг себя, — наполненный любовью и
					благостью</p>

			</div>
		</div>
	</div>
</section>

<section class="description-section">
	<div class="container">
		<h2 class="section-header text-center text-uppercase">Детали тура</h2>
		<div class="row">
			<div class="col-md-6">
				<h3>Место проживания:</h3>
				<p>
					Проживание в мини-гостинице, на первой береговой линии, в трехместном номере.
					Цена на размещение на основном месте (обычная кровать) 15900р, размещение на допол. местах –на современная раскладушка с матрасом 12 750р., в спальнике на верхней террасе 10 200р.
				</p>
				<p><strong>Вне зависимости от типа номера, у вас будет включено</strong></p>
				<ul class="list-unstyled list">
					<li>- 3-х разовое ПОЛЕЗНОЕ и ВКУСНОЕ питание</li>
					<li>- Также в стоимость входит</li>
					<li>- Медицинская страховка на время поездки</li>
					<li>- Трансфер от аэропорта (города Адлер) до гостиницы</li>
					<li>- 12 занятий по йоге</li>
					<li>- 7 интерактивных лекций по йоге, ЗОЖ, аюрведе и пр.</li>
					<li>- 5 занятий по медитации</li>
					<li>- 5 мастер-классов по работе с телом (цигун, массаж, оздоровительные процедуры)</li>
					<li>- 3 занятия по мантропению</li>
					<li>- 2 мастер-класса по здоровому питанию</li>
				</ul>
				<br>
				<p>Таким образом, все включено, Вам остается лишь выбрать удобный для Вас способ, как добраться до Сухуми, а там мы Вас встретим!</p>
			</div>
			<div class="col-md-6 thumbnail">
				<? if ($arResult['DISPLAY_PROPERTIES']['MORE_PHOTO']): ?>
					<div class="slider">
						<? foreach ($arResult['DISPLAY_PROPERTIES']['MORE_PHOTO']['VALUE'] as $arPhoto): ?>
							<div class="thumbnail"><img src="<?=CFile::GetPath($arPhoto)?>" alt=""></div>
						<? endforeach; ?>
					</div>
				<? endif; ?>
			</div>
		</div>
	</div>
</section>


<section class="buy-section" id="buy-section">
	<div class="container">
		<div class=" text-left">
			<div class="panel panel-info">
				<div class="panel-heading"><h3 class="panel-title">Внимание! Как добраться до Абхазии?</h3></div>
				<div class="panel-body">
					<p>
						Чтобы Вам было легче сориентироваться по ценам перевозчиков, мы узнали цены и выходит, что:
					<p>Проезд на автобусе Уфа-Сухум-Уфа (<strong>6000руб</strong>., выезд 02го приезд 04го вечером, обратно 10го выезд, 12го в Уфе.)</p>
					<p>Если Вы выберете Авиабилеты со стыковкой в Москве туда и обратно, то выйдет <strong>13 900р</strong>.</p>
					<p>Прямые рейсы 5го туда 12го обратно <strong>17 700р</strong>.</p>

					</p>
					<p>Билет на самолет приобретается самостоятельно <a href="https://search.aviasales.ru/UFA0506AER15061" target="_blank">Подобрать билет на самолет</a></p>
				</div>
			</div>
		</div>
		<form action="<?=$this->GetFolder()?>/ajax.php" id="buy-form">
			<input type="hidden" name="buy" value="Y">
			<input type="hidden" name="lux_offer" id="lux_offer" value="N">
			<input type="hidden" name="productId" id="productId" value="<?=$arResult['CUR_OFFER']['ID']?>">
			<div class="row price-row">
				<div class="col-md-6 col-xs-6">
					Стоимость тура
				</div>
				<div class="col-md-6 col-xs-6 text-right">
					<span id="current-price" data-original-price='<?=$arResult['CUR_OFFER']['PRICE']?>'><?=$arResult['CUR_OFFER']['PRICE']?></span> РУБ
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="order-props">
						Состав заказа:
						<ul>
							<li><?=$arResult['NAME']?></li>
							<li id="place-info"><?=$arResult['CUR_OFFER']['NAME']?></li>
						</ul>
					</div>
				</div>
				<div class="col-md-6 text-right">
					<div class="pull-right" style="width: 230px;">
						<label>Выберите тип размещения:</label>
						<br>
						<? foreach ($arResult['OFFERS_NEW'] as $arOffer): ?>
							<? $btnClass = $arOffer == $arResult['CUR_OFFER'] ? "btn-primary" : "btn-info" ?>
							<a href="javascript:void(0)" id="offer_<?=$arOffer['ID']?>" class="btn <?=$btnClass?> btn-sm setOfferBtn" data-offer-id="<?=$arOffer['ID']?>"><?=$arOffer['NAME']?> -
								<?=CCurrencyLang::CurrencyFormat($arOffer['PRICE'], 'RUB')?></a>
						<? endforeach; ?>
						<label>Количество туристов</label>
						<div class="input-group">
							<span class="input-group-addon" onclick="changeQuantity(false)"><i class="glyphicon glyphicon-minus"></i></span>
							<input type="number" class="form-control" name="quantity" id="quantity" value="1">
							<span class="input-group-addon" onclick="changeQuantity(true)"><i class="glyphicon glyphicon-plus"></i></span>
						</div>
					</div>
					<div class="clearfix"></div>
					<br>
				</div>
			</div>
			<div class="clearfix"></div>
			<br>
			<button type="submit" class="pull-right btn btn-success btn-lg text-uppercase">Оформить</button>
		</form>
	</div>
</section>


<script>
	$(document).ready(function () {
		$('.slider').owlCarousel({
			loop: true,
			margin: 10,
			nav: false,
			items: 1,
		});


		var setOfferBtn = $('.setOfferBtn'),
			productId = $('#productId'),
			current_price = $('#current-price'),
			placeInfo = $('#place-info'),
			form = $('#buy-form');

		setOfferBtn.on('click', function () {
			var curQuantity = $('#quantity').val(),
				offerId = $(this).data('offer-id');
			var offer = ShopData.Offers[offerId];

			productId.val(offer.ID);
			current_price.text(parseInt(offer.PRICE) * curQuantity).data('original-price', offer.PRICE);
			placeInfo.text(offer.NAME);

			setOfferBtn.removeClass('btn-primary').addClass('btn-info');
			$(this).removeClass('btn-info').addClass('btn-primary');
		});


		form.on('submit', function (e) {
			e.preventDefault();
			var href = form.attr('action'),
				data = form.serialize();

			$.post(
				href,
				data,
				function (response) {
					console.log(response);
					if (response == 'success') {
						window.location = ShopData.redirectUrl;
					}
				}
			);

		})

	});

	//Меняем отображение кнопки
	function changeAddLuxBtn(status) {
		var addLuxBtn = $('#addLux');
		if (status) {
			addLuxBtn.removeClass('btn-info').addClass('btn-danger').html('<i class="glyphicon glyphicon-minus"></i> Обычное место');
		} else {
			addLuxBtn.removeClass('btn-danger').addClass('btn-info').html('<i class="glyphicon glyphicon-plus"></i> Улучшенное место');
		}
	}

	function changeQuantity(up) {
		var curQuantity = $('#quantity').val(),
			curPrice = $('#current-price'),
			originalPrice = $('#current-price').data('original-price');
		if (up) {
			$('#quantity').val(parseInt(curQuantity) + 1);
		} else {
			if (curQuantity > 1) {
				$('#quantity').val(parseInt(curQuantity) - 1);
			}
		}

		curPrice.text(parseInt(originalPrice) * parseInt($('#quantity').val()))
	}

</script>