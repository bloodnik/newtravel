<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

$arNewOffer     = [];


if (is_array($arResult["OFFERS"]) && ! empty($arResult["OFFERS"])) {
	foreach ($arResult["OFFERS"] as $key => $arOffer) {
		$arNewOffer[$arOffer['ID']]['ID']    = $arOffer['ID'];
		$arNewOffer[$arOffer['ID']]['PRICE'] = $arOffer['PRICES']['BASE']['VALUE'];
		$arNewOffer[$arOffer['ID']]['NAME']  = $arOffer['PROPERTIES']['PLACE']['VALUE'];
	}
}
$arResult['OFFERS_NEW'] = $arNewOffer;
$arResult['CUR_OFFER'] = reset($arResult['OFFERS_NEW']);





?>
