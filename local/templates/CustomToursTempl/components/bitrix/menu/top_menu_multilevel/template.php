<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

if (empty($arResult)) {
	return;
}
?>
<ul class="menu">

	<?
	$previousLevel = 0;
	$firstRoot     = false;
	foreach ($arResult as $itemIdex => $arItem): ?>

	<? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif ?>

	<? if ($arItem["IS_PARENT"]):
	$countSub = 0; ?>
	<? if ($arItem["DEPTH_LEVEL"] == 1): ?>
	<li>
		<a href="<?=$arItem["LINK"]?>" class="menu__item <? if ($arItem["SELECTED"]):?>menu__item_active<?endif ?>"><span><?=$arItem["TEXT"]?></span></a>
		<ul class="menu__sub">
			<? else: ?>
			<li><a href="<?=$arItem["LINK"]?>" class="menu__item <? if ($arItem["SELECTED"]):?>menu__item_active<?endif ?>"><span><?=$arItem["TEXT"]?></span></a>
				<ul>
					<?endif ?>
					<? else:?>
						<? if ($arItem["PERMISSION"] > "D"):?>
							<? if ($arItem["DEPTH_LEVEL"] == 1):?>
								<li>
									<a href="<?=$arItem["LINK"]?>" class="menu__item <? if ($arItem["SELECTED"]):?>menu__item_active<?endif ?>">
										<span><?=$arItem["TEXT"]?></span>
									</a>
								</li>
							<?
							else:?>
								<li>
									<? if ($arItem["DEPTH_LEVEL"] == 3 && $countSub == 0):?>
										<? $countSub++;
									endif ?>
									<a href="<?=$arItem["LINK"]?>" class="menu__item <? if ($arItem["SELECTED"]):?>menu__item_active<?endif ?>">
										<span><?=$arItem["TEXT"]?></span>
									</a>
								</li>
							<?endif ?>
						<?
						else:?>
							<? if ($arItem["DEPTH_LEVEL"] == 1):?>
								<li>
									<a href="" class="menu__item <? if ($arItem["SELECTED"]):?>menu__item_active<?endif ?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>">
										<span><?=$arItem["TEXT"]?></span>
									</a>
								</li>
							<?
							else:?>
								<li><a href="" class="menu__item" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>">
										<span><?=$arItem["TEXT"]?></span>
									</a>
								</li>
							<?endif ?>
						<?endif; ?>
					<?
					endif;

					$previousLevel = $arItem["DEPTH_LEVEL"];
					if ($arItem["DEPTH_LEVEL"] == 1) {
						$firstRoot = true;
					}
					?>
					<?endforeach;
					if ($previousLevel > 1):
						echo str_repeat("</ul></li>", ($previousLevel - 1));
					endif; ?>
				</ul>
