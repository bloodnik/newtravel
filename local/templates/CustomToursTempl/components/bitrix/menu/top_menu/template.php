<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>

<? if ( ! empty($arResult)): ?>
	<nav class="menu">
		<ul>
			<? foreach ($arResult as $arItem): ?>
				<? if ($arItem["PERMISSION"] > "D"): ?>
					<li>
						<a href="<?=$arItem["LINK"]?>"
						   class="menu__item <? if ($arItem["SELECTED"]): ?>active<? endif; ?>">
							<?=$arItem["TEXT"]?>
						</a>
					</li>
				<? endif ?>
			<? endforeach ?>
		</ul>
	</nav>
<? endif ?>

