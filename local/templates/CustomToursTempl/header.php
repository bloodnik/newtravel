<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex, nofollow">
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">

	<meta property="og:type" content="website">
	<meta property="og:site_name" content="Умные туристы">
	<meta property="og:title" content="Умныетуристы.рф - сервис бронирования выгодных туров онлайн, покупка туров онлайн из Москвы по всем направлениям">
	<meta property="og:description" content="Умныетуристы.рф - сервис бронирования выгодных туров онлайн, покупка туров онлайн из Москвы по всем направлениям.">
	<meta property="og:url" content="https://xn--e1agkeqhedbe9fh.xn--p1ai/">
	<meta property="og:locale" content="ru_RU">
	<meta property="og:image" content="https://xn--e1agkeqhedbe9fh.xn--p1ai/local/templates/promo_pages/assets/i/logo.png">

	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico"/>
	
	<title><? $APPLICATION->ShowTitle() ?></title>
	<?
	$APPLICATION->ShowHead();

	//CSS
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/bootstrap/css/bootstrap.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/styles.css");
	Asset::getInstance()->addCss('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/owl-carousel/owl.carousel.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/owl-carousel/owl.theme.green.min.css");

	//JS
	Asset::getInstance()->addJs('https://code.jquery.com/jquery-2.2.4.min.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/bootstrap/js/bootstrap.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/owl-carousel/owl.carousel.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/scripts.js");

	?>
</head>
<body data-spy="scroll" data-target=".head-section">
<? $APPLICATION->ShowPanel() ?>
<section class="top-line">
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="container midd">
				<div class="row">
					<div class="col-md-4 col-xs-5"><a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/img/logo-vek.svg" class="logo" alt=""></a></div>
					<div class="col-md-4 col-xs-7 col-md-push-4 text-right">
						<div class="text-muted">По всем вопросам звоните:</div>
						<div class="text-primary"><h3>+7(347)216-30-71</h3></div>
					</div>
					<div class="col-md-4 col-md-pull-4 text-center text-uppercase description">Туры в Абхазию</div>
				</div>
			</div>
		</div>
	</div>
</section>