<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

?>
<footer>
	<p>© <?=date('Y')?><a style="color:#00aaef; text-decoration:none;" href="#"> Умныетуристы.рф</a>, Все права защищены.</p>
</footer>

<!--<iframe-->
<!--	src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d746368.9892006746!2d40.7903656817123!3d43.05186927485176!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x405f2f4f13a442ff%3A0x934852e625989a6f!2z0JDQsdGF0LDQt9C40Y8!5e0!3m2!1sru!2sru!4v1495007480823"-->
<!--	width="100%" height="400" frameborder="0" style="border:0; margin-top: 50px;" allowfullscreen></iframe>-->

<!--noindex-->
<? if ( ! $USER->IsAuthorized()): ?>
	<div class="modal fade" id="auth_modal" tabindex="-1" role="dialog" aria-labelledby="Авторизация">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Вход</h4>
				</div>
				<div class="modal-body">
					<? $APPLICATION->IncludeComponent(
						"bitrix:system.auth.form",
						"newtravel",
						Array(
							"COMPONENT_TEMPLATE"   => ".default",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
							"FORGOT_PASSWORD_URL"  => "",
							"PATH_TO_MYPORTAL"     => "desktop.php",
							"PROFILE_URL"          => "",
							"REGISTER_URL"         => "",
							"SHOW_ERRORS"          => "Y"
						)
					); ?>
				</div>
			</div>
		</div>
	</div>
<? endif; ?>
<!--/noindex-->

</body>
</html>