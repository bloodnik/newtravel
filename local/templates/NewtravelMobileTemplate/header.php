<? define("NOT_CHECK_FILE_PERMISSIONS", true); ?>
<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Bitrix\Main\Page\Asset;

//mobile init
if ( ! CModule::IncludeModule("mobileapp")) {
	die();
}
CMobile::Init();
?>
<!DOCTYPE html >
<html class="<?=CMobile::$platform;?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">


	<meta property="og:type" content="website">
	<meta property="og:site_name" content="Умные туристы">
	<meta property="og:title" content="Умныетуристы.рф - сервис бронирования выгодных туров онлайн, покупка туров онлайн из Москвы по всем направлениям">
	<meta property="og:description" content="Умныетуристы.рф - сервис бронирования выгодных туров онлайн, покупка туров онлайн из Москвы по всем направлениям.">
	<meta property="og:url" content="https://xn--e1agkeqhedbe9fh.xn--p1ai/">
	<meta property="og:locale" content="ru_RU">
	<meta property="og:image" content="https://xn--e1agkeqhedbe9fh.xn--p1ai/local/templates/promo_pages/assets/i/logo.png">

	<? if (true): ?>
		<? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/Framework7/dist/css/framework7.material.min.css"); ?>
		<? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/Framework7/dist/css/framework7.material.colors.min.css"); ?>
	<? else: ?>
		<? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/Framework7/dist/css/framework7.ios.min.css"); ?>
		<? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/Framework7/dist/css/framework7.ios.colors.min.css"); ?>
	<? endif; ?>
	<? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/Framework7/dist/css/framework7-icons.css"); ?>
	<? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/Framework7/dist/css/my-app.css"); ?>

	<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-2.2.4.min.js"); ?>
	<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/moment.js"); ?>
	<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/js-cookie/js.cookie.js"); ?>
	<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/script.js"); ?>

	<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/Framework7/dist/js/framework7.min.js"); ?>
	<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/Framework7/dist/js/my-app.js"); ?>

	<? $APPLICATION->ShowHead(); ?>

	<meta http-equiv="Content-Type" content="text/html;charset=<?=SITE_CHARSET?>"/>
	<meta name="format-detection" content="telephone=no">



</head>
<body><? //$APPLICATION->ShowPanel();?>
<? $APPLICATION->ShowPanel() ?>
<div class="views">
	<div class="view view-main">
		<script type="text/javascript">
			app.pullDown({
				enable: true,
				callback: function () {
					document.location.reload();
				},
				downtext: "<?=GetMessage("MB_PULLDOWN_DOWN")?>",
				pulltext: "<?=GetMessage("MB_PULLDOWN_PULL")?>",
				loadtext: "<?=GetMessage("MB_PULLDOWN_LOADING")?>"
			});
		</script>
