<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
  "NAME" => "Форма поиска туров для страницы отеля",
  "DESCRIPTION" => "Отображает форму поиска туров на странице отеля",
  "ICON" => "/images/icon.gif",
  "COMPLEX" => "N",
  "PATH" => array(
    "ID" => "Турвизор",
  ),
);
?>