<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

$this->addExternalCss(SITE_TEMPLATE_PATH."/js/bootstrap-multiselect/css/bootstrap-multiselect.css");
$this->addExternalCss(SITE_TEMPLATE_PATH."/js/footable/footable.core.min.css");

$this->addExternalJs(SITE_TEMPLATE_PATH."/js/bootstrap-multiselect/js/bootstrap-multiselect.js");
$this->addExternalJs(SITE_TEMPLATE_PATH."/js/daterangepicker/daterangepicker.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH."/js/jQuery.AutoColumnList/jquery.autocolumnlist.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH."/js/footable/footable.min.js");
?>
<?
$departure = isset($_REQUEST["departure"]) && ($_REQUEST["departure"] > 0) ? $_REQUEST["departure"] : $arParams["CITY"];
$country   = isset($_REQUEST["country"]) && ($_REQUEST["country"] > 0) ? $_REQUEST["country"] : $arParams["COUNTRY_CODE"];

$dateFrom  = isset($_REQUEST["datefrom"]) ? $_REQUEST["datefrom"] : date("d.m.Y", strtotime("+1 DAY"));
$dateTo    = isset($_REQUEST["dateto"]) ? $_REQUEST["dateto"] : date("d.m.Y", strtotime("+60 DAY"));
$dateRange = isset($_REQUEST["daterange"]) ? $_REQUEST["daterange"] : $dateFrom . " - " . $dateTo;

$nightsFrom = isset($_REQUEST["nightsfrom"]) ? $_REQUEST["nightsfrom"] : $arParams["NIGHTSFROM"];
$nightsTo   = isset($_REQUEST["nightsto"]) ? $_REQUEST["nightsto"] : $arParams["NIGHTSTO"];
$stars      = isset($_REQUEST["stars"]) ? $_REQUEST["stars"] : $arParams["STARS"];
$rating     = isset($_REQUEST["rating"]) ? $_REQUEST["rating"] : "";
$meal       = isset($_REQUEST["meal"]) ? $_REQUEST["meal"] : "";
$adults     = isset($_REQUEST["adults"]) ? $_REQUEST["adults"] : $arParams["ADULTS"];
$child      = isset($_REQUEST["child"]) ? $_REQUEST["child"] : "0";
$childage1  = isset($_REQUEST["childage1"]) ? $_REQUEST["childage1"] : "0";
$childage2  = isset($_REQUEST["childage2"]) ? $_REQUEST["childage2"] : "0";
$childage3  = isset($_REQUEST["childage3"]) ? $_REQUEST["childage3"] : "0";

$priceFrom  = isset($_REQUEST["pricefrom"]) ? $_REQUEST["pricefrom"] : "0";
$priceTo    = isset($_REQUEST["priceto"]) ? $_REQUEST["priceto"] : "1000000";
$priceRange = $priceFrom . "," . $priceTo;

$hotactive = isset($_REQUEST["hotactive"]) ? $_REQUEST["hotactive"] : "";
$hotrelax  = isset($_REQUEST["hotrelax"]) ? $_REQUEST["hotrelax"] : "";
$hotfamily = isset($_REQUEST["hotfamily"]) ? $_REQUEST["hotfamily"] : "";
$hothealth = isset($_REQUEST["hothealth"]) ? $_REQUEST["hothealth"] : "";
$hotcity   = isset($_REQUEST["hotcity"]) ? $_REQUEST["hotcity"] : "";
$hotbeach  = isset($_REQUEST["hotbeach"]) ? $_REQUEST["hotbeach"] : "";
$hotdeluxe = isset($_REQUEST["hotdeluxe"]) ? $_REQUEST["hotdeluxe"] : "";

$regions   = explode(",", $_REQUEST["regions"]);
$hotels    = explode(",", $_REQUEST["hotels"]);
$operators = explode(",", $_REQUEST["operators"]);

$templateFolder = &$this->GetFolder();
?>

<!-- =========================================
Gray Section ( hotel tour form )
========================================== -->
<section class="gray-section" style="background: #e1e1e1">
	<link href="<?=$templateFolder?>/style.css" rel="stylesheet" type="text/css"/>
	<div class="container">

		<div class="hotel-tour-form">
			<div class="row">

				<div class="col-md-12">
					<h3 class="hotel-tour-form-title">Туры в отель</h3>
				</div>

			</div><!-- /row -->

			<div class="row">
				<div class="col-md-12">
					<input type="hidden" name="hotels" id="hotels" value="<?=$_REQUEST["hotelcode"]?>"/>
					<input type="hidden" name="country" id="country" value="<?=$arParams["COUNTRY_CODE"]?>"/>
					<input type="hidden" name="pricerange" id="priceRange" value="<?=$priceRange?>"/>
					<input type="hidden" name="rating" id="rating" value="<?=$rating?>"/>
					<input type="hidden" name="stars" id="stars" value="<?=$stars?>"/>


					<div class="hotel-tour-form-line">
						<span><strong>C вылетом из</strong></span>
						<input type="hidden" name="departure" id="departure" value="<?=$departure?>"/>
						<div class="form-control pseudo-input" id="departureValue" style="display: inline-block;width: 180px;" data-toggle="modal" data-target="#departureModal"></div>
					</div>
					<div class="hotel-tour-form-line">
						<span><strong>даты вылета</strong></span>
						<input type="text" class="form-control" name="daterange" id="daterange" value="<?=$dateRange?>" style="display: inline-block;width: 160px;height: 36px;"/>
					</div>
					<div class="hotel-tour-form-line">
						<span><strong>ночей:</strong></span>
						<input type="hidden" name="nightsfrom" id="nightsfrom" value="<?=$nightsFrom?>"/>
						<input type="hidden" name="nightsto" id="nightsto" value="<?=$nightsTo?>"/>
						<div class="form-control pseudo-input" style="display: inline-block;width: 100px;" id="nightsValue">от <?=$nightsFrom?> до <?=$nightsTo?></div>
						<div id="nights-select">
							Выберите <span style="font-weight: bold;">от:</span>
							<table>
								<tr>
									<td data-value="2">2</td>
									<td data-value="3">3</td>
									<td data-value="4">4</td>
									<td data-value="5">5</td>
									<td data-value="6">6</td>
									<td data-value="7">7</td>
									<td data-value="8">8</td>
								</tr>
								<tr>
									<td data-value="9">9</td>
									<td data-value="10">10</td>
									<td data-value="11">11</td>
									<td data-value="12">12</td>
									<td data-value="13">13</td>
									<td data-value="14">14</td>
									<td data-value="15">15</td>
								</tr>
								<tr>
									<td data-value="16">16</td>
									<td data-value="17">17</td>
									<td data-value="18">18</td>
									<td data-value="19">19</td>
									<td data-value="20">20</td>
									<td data-value="21">21</td>
									<td data-value="22">22</td>
								</tr>
								<tr>
									<td data-value="23">23</td>
									<td data-value="24">24</td>
									<td data-value="25">25</td>
									<td data-value="26">26</td>
									<td data-value="27">27</td>
									<td data-value="28">28</td>
									<td data-value="29">29</td>
								</tr>
							</table>
							<span class="no" id="nights-error" style="display:none; font-size: 75%;">Ночей "до" не может быть меньше "от"</span>
						</div>
					</div>
					<div class="hotel-tour-form-line ">
						<div class="toursit-select">
							<div class="tourists" style="display: none;">
								<div class="hotel-tour-form-line ">
									<label>Взрослых: </label>
									<div class="adultsCount">
										<input type="hidden" value="<?=$adults?>" name="adults" id="adults"/>

										<div class="adultsCount1 <? if ($adults == 1): ?>active<? endif; ?>"
										     data-count="1">
											<i class="fa fa-male fa-2x"></i>
										</div>
										<div class="adultsCount2 <? if ($adults == 2): ?>active<? endif; ?>"
										     data-count="2">
											<i class="fa fa-male fa-2x"></i>
											<i class="fa fa-male fa-2x"></i>
										</div>
										<div class="adultsCount3 <? if ($adults == 3): ?>active<? endif; ?>"
										     data-count="3">
											<i class="fa fa-male fa-2x"></i>
											<i class="fa fa-male fa-2x"></i>
											<i class="fa fa-male fa-2x"></i>
										</div>
										<div class="adultsCount4 <? if ($adults == 4): ?>active<? endif; ?>"
										     data-count="4">
											<i class="fa fa-male fa-2x"></i>
											<i class="fa fa-male fa-2x"></i>
											<i class="fa fa-male fa-2x"></i>
											<i class="fa fa-male fa-2x"></i>
										</div>
									</div>

									<label>Количество детей:</label>
									<a href="javascript:void(0)" id="noKids" class="pull-right small">убрать детей</a>

									<div class="kidsCount">
										<input type="hidden" value="<?=$child?>" name="child" id="child"/>

										<div class="kidsCount1 <? if ($child == 1): ?>active<? endif; ?>"
										     data-count="1">
											<i class="fa fa-child fa-2x"></i>
										</div>
										<div class="kidsCount2 <? if ($child == 2): ?>active<? endif; ?>"
										     data-count="2">
											<i class="fa fa-child fa-2x"></i>
											<i class="fa fa-child fa-2x"></i>
										</div>
										<div class="kidsCount3 <? if ($child == 3): ?>active<? endif; ?>"
										     data-count="3">
											<i class="fa fa-child fa-2x"></i>
											<i class="fa fa-child fa-2x"></i>
											<i class="fa fa-child fa-2x"></i>
										</div>
									</div>
								</div>
								<div class="hotel-tour-form-line kidsAge">
									<label>Возраст детей:</label> <br/>
									<select id="childage1" name="childage1" class="form-control">
										<? for ($i = 0; $i <= 15; $i++) { ?>
											<option
												value="<?=$i?>"<? if ($childage1 == $i): ?> selected<? endif; ?>><?=$i?></option>
										<? } ?>
									</select>
									<select id="childage2" name="childage2" class="form-control">
										<? for ($i = 0; $i <= 15; $i++) { ?>
											<option
												value="<?=$i?>"<? if ($childage2 == $i): ?> selected<? endif; ?>><?=$i?></option>
										<? } ?>
									</select>
									<select id="childage3" name="childage3" class="form-control">
										<? for ($i = 0; $i <= 15; $i++) { ?>
											<option
												value="<?=$i?>"<? if ($childage3 == $i): ?> selected<? endif; ?>><?=$i?></option>
										<? } ?>
									</select>
								</div>

								<button type="button" class="btn btn-blue button-save btn-block">Сохранить</button>
							</div>

							<span><strong>туристы</strong></span>
							<div class="form-control pseudo-input tourists-total" style="display: inline-block;width: 121px"><span><?=$adults?>
									взр. <? if ($child > 0): ?><?=$child?> реб.<? endif; ?></span></div>
						</div>
					</div>

					<div class="hotel-tour-form-line">
						<span><strong>питание</strong></span>
						<select name="meal" id="meal">
							<option value="1">Любое</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5 hotel-tour-form-line">
					<span class="operator-label"><strong>Туроператор</strong></span>
					<select id="operators" class="multiply" multiple="multiple"></select>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-7">
					<noindex>
						<button id="search" name="search" style="width: 105px" onclick="searchTours()" class="btn btn-blue">Найти</button>
						<noindex>
				</div>
				<div class="col-md-12">
					<hr>

					<div class="hotel-tour-results">
						<!-- =========================================
						Результаты
						========================================== -->
						<div class="col-xs-12">

							<div class="col-xs-12 progress-section" style="display: none">
								<!--Результаты поиска туров-->
								<div class="col-md-4 col-sm-9">
									<div class="progress progress-striped active" style="display: block">
										<div class="progress-bar" style="width: 0%; color:#000"></div>
									</div>
								</div>
								<div class="col-md-2 col-sm-2" id="progress-percent">0%</div>
								<div class="col-md-3 col-xs-6" id="progress-status">идет поиск</div>
								<div class="col-md-3 col-xs-6" id="tours-found">0 туров найдено</div>
							</div>

							<div id="currentPage" style="display: none">1</div>
							<div id="requestid" style="display: none">0</div>

							<div id="request" style="display: none;"></div>

							<? if (isset($_REQUEST["formresult"]) && $_REQUEST["formresult"] == "addok"): ?>
								<!--Результаты поиска туров-->
								<div class="tour-list-wrap" id="formok">
									<div class="tour-item">
										<div class="col-md-12 yes">
											Заявка на подбор успешно отправлена
										</div>
									</div>
								</div>
							<? endif; ?>
						</div>
					</div><!-- /hotel-tour-results -->
				</div>
				<div class="col-md-12">
					<a href="javascript:void(0)" class="show-all" onclick="$('.hidden-tour').show(200);">Показать все предложения</a>
				</div>
			</div>
		</div><!-- /hotel-tour-form -->

	</div><!-- /container -->
</section>


<!-----------------------------------------------------------------------------------------------------
    Модальное окно выбора города отправления
 ------------------------------------------------------------------------------------------------------>
<div class="modal fade" id="departureModal" tabindex="-1" role="dialog" aria-labelledby="departureModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Выберите город вылета</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-2 popularDepartures hidden-xs hidden-sm">
						<ul class="nt-list">
							<li><a href="javascript:void(0)" onclick="selectDeparture(1, 'Москва')">Москва</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(3, 'Екатеренбург')">Екатеренбург</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(6, 'Челябинск')">Челябинск</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(28, 'Оренбург')">Оренбург</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(7, 'Самара')">Самара</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(4, 'Уфа')">Уфа</a></li>
						</ul>
					</div>
					<div class="col-md-10 allDepartures">
						<div class="row">
							<div class="list customScroll">
								<ul class="nt-list multiColumn" id="departureList">
									<li class="divider"></li>
									<li><a href="#">Без перелета</a></li>
									<li class="divider">А</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<!-----------------------------------------------------------------------------------------------------
    Модальное окно выбора страны
 ------------------------------------------------------------------------------------------------------>
<div class="modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="countryModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Выберите страну</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-2 popularCountry hidden-xs">
						<h5>Популярные страны</h5>
						<ul class="nt-list">
							<? foreach ($arResult["POPULAR_COUNTRIES"] as $key => $popCountry): ?>
								<li>
									<img src="<?=CFile::GetPath($popCountry["PICTURE"])?>" alt="<?=$popCountry["NAME"]?>"/>
									<a href="javascript:void(0)" onclick="selectCountry(<?=$key?>, '<?=$popCountry["NAME"]?>')"><?=$popCountry["NAME"]?></a>
								</li>
							<? endforeach; ?>
						</ul>
					</div>
					<div class="col-sm-10 allCountry">
						<h5>Выберите страну:</h5>
						<div class="row">
							<div class="list customScroll">
								<ul class="nt-list multiColumn" id="countryList">
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<script>
	//Глобальные переменные
	var nfrom = 0; //Ночей от
	var nto = 0; //Ночей до

	//Событие при смене города отправления
	function selectDeparture(id, name) {
		$("#departure").val(id);
		$("#departureValue").text(name);
		$("#departureModal").modal('hide');
		getCountries(id);

		getFlydates(id, $("#country").val());
	}


	$(document).ready(function () {
		var startSearch = "";
		startSearch = "<?=$_REQUEST["start"]?>";

		getDepartures(<?=$departure?>);

		getMeals(<?=$meal?>);
		getFlydates(<?=$departure?>, <?=$country?>);
		getOperators(<?=CUtil::PhpToJSObject($operators)?>);

		setTimeout(function () {
			if (startSearch == 'Y') {
				searchTours();
			}
		}, 3000);


	});
</script>