<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>
<?
$dateFrom       = date("d.m.Y", strtotime("1 DAY"));
$dateTo         = date("d.m.Y", strtotime("+" . $arParams["DATE_TO"] . " DAY"));
$dateRange      = $dateFrom . " - " . $dateTo;
$departure      = isset($arResult["DEPARTURE"]) ? $arResult["DEPARTURE"] : $arParams["CITY"];
$country      = isset($arParams["COUNTRY"]) ? $arParams["COUNTRY"] : "4";
$templateFolder = &$this->GetFolder();
?>

<link rel="stylesheet" href="<?=$templateFolder?>/style.css">

<div class="simple-search-form">
	<form action="/tours">
		<div class="form-group">
			<input type="hidden" name="departure" id="departure" value=""/>
			<label for="">Смотреть туры из города:</label> <span id="departureValue" data-toggle="modal" data-target="#departureModal"></span>
		</div>
		<div class="form-group">
			<div class="row">
				<input type="hidden" name="country" id="country" value=""/>
				<div class="col-md-5">
					<label for="">Куда поедем?: </label>
				</div>
				<div class="col-md-7">
					<div class="form-control pseudo-input" id="countryValue" data-toggle="modal" data-target="#countryModal"></div>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-md-5">
					<label for="">Даты вылета: </label>
				</div>
				<div class="col-md-7">
					<input type="text" class="form-control" id="daterange" name="daterange" value="<?=$dateRange?>"/>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-md-5">
					<label for="">Ночей: </label>
				</div>
				<div class="col-md-7">
					<input type="hidden" name="nightsfrom" id="nightsfrom" value="<?=$arParams["NIGHTSFROM"]?>"/>
					<input type="hidden" name="nightsto" id="nightsto" value="<?=$arParams["NIGHTSTO"]?>"/>
					<div class="form-control pseudo-input" id="nightsValue">от <?=$arParams["NIGHTSFROM"]?> до <?=$arParams["NIGHTSTO"]?></div>
					<div id="nights-select">
						Выберите <span style="font-weight: bold;">от:</span>
						<table style="width: 100%;">
							<tr>
								<td data-value="2">2</td>
								<td data-value="3">3</td>
								<td data-value="4">4</td>
								<td data-value="5">5</td>
								<td data-value="6">6</td>
								<td data-value="7">7</td>
								<td data-value="8">8</td>
							</tr>
							<tr>
								<td data-value="9">9</td>
								<td data-value="10">10</td>
								<td data-value="11">11</td>
								<td data-value="12">12</td>
								<td data-value="13">13</td>
								<td data-value="14">14</td>
								<td data-value="15">15</td>
							</tr>
							<tr>
								<td data-value="16">16</td>
								<td data-value="17">17</td>
								<td data-value="18">18</td>
								<td data-value="19">19</td>
								<td data-value="20">20</td>
								<td data-value="21">21</td>
								<td data-value="22">22</td>
							</tr>
							<tr>
								<td data-value="23">23</td>
								<td data-value="24">24</td>
								<td data-value="25">25</td>
								<td data-value="26">26</td>
								<td data-value="27">27</td>
								<td data-value="28">28</td>
								<td data-value="29">29</td>
							</tr>
						</table>
						<span class="no" id="nights-error" style="display:none; font-size: 75%;">Ночей "до" не может быть меньше "от"</span>
					</div>
				</div>
			</div>
		</div>

		<div class="toursit-select form-group">
			<div class="tourists" style="display: none;">
				<div class="hotel-tour-form-line ">
					<label>Взрослых: </label>

					<div class="adultsCount">
						<input type="hidden" value="2" name="adults" id="adults"/>
						<div class="adultsCount1 <? if ($adults == 1): ?>active<? endif; ?>" data-count="1">
							<i class="fa fa-male fa-2x"></i>
						</div>
						<div class="adultsCount2 active" data-count="2">
							<i class="fa fa-male fa-2x"></i>
							<i class="fa fa-male fa-2x"></i>
						</div>
						<div class="adultsCount3>" data-count="3">
							<i class="fa fa-male fa-2x"></i>
							<i class="fa fa-male fa-2x"></i>
							<i class="fa fa-male fa-2x"></i>
						</div>
						<div class="adultsCount4" data-count="4">
							<i class="fa fa-male fa-2x"></i>
							<i class="fa fa-male fa-2x"></i>
							<i class="fa fa-male fa-2x"></i>
							<i class="fa fa-male fa-2x"></i>
						</div>
					</div>

					<label>Количество детей:</label>
					<a href="javascript:void(0)" id="noKids" class="pull-right small">без детей</a>
					<div class="kidsCount">
						<input type="hidden" value="0" name="child" id="child"/>
						<div class="kidsCount1" data-count="1">
							<i class="fa fa-child fa-2x"></i>
						</div>
						<div class="kidsCount2" data-count="2">
							<i class="fa fa-child fa-2x"></i>
							<i class="fa fa-child fa-2x"></i>
						</div>
						<div class="kidsCount3" data-count="3">
							<i class="fa fa-child fa-2x"></i>
							<i class="fa fa-child fa-2x"></i>
							<i class="fa fa-child fa-2x"></i>
						</div>
					</div>

				</div>
				<div class="hotel-tour-form-line kidsAge">
					<label>Возраст детей:</label> <br/>
					<select id="childage1" name="childage1" class="form-control">
						<? for ($i = 0; $i <= 15; $i++) { ?>
							<option value="<?=$i?>"><?=$i?></option>
						<? } ?>
					</select>
					<select id="childage2" name="childage2" class="form-control">
						<? for ($i = 0; $i <= 15; $i++) { ?>
							<option value="<?=$i?>"><?=$i?></option>
						<? } ?>
					</select>
					<select id="childage3" name="childage3" class="form-control">
						<? for ($i = 0; $i <= 15; $i++) { ?>
							<option value="<?=$i?>"><?=$i?></option>
						<? } ?>
					</select>
				</div>

				<button type="button" class="btn btn-blue button-save btn-block">Сохранить</button>
			</div>
			<div class="row">
				<div class="col-md-5">
					<label>Туристы: </label>
				</div>
				<div class="col-md-7">
					<div class="form-control tourists-total"><span><?=$arParams["ADULTS"]?> взр.</span></div>
				</div>
			</div>
		</div>

		<div class="form-group">
			<input type="hidden" name="start" id="start" value="Y"/>
			<button type="submit" class="btn btn-blue" style="width: 100%">Поехали!</button>
		</div>
	</form>
	<div class="row">
		<div class="col-md-12 text-center"><a href="/tours" class="small">подробная форма поиска</a></div>
	</div>
</div>


<!-----------------------------------------------------------------------------------------------------
    Модальное окно выбора города отправления
 ------------------------------------------------------------------------------------------------------>
<div class="modal fade" id="departureModal" tabindex="-1" role="dialog" aria-labelledby="departureModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Выберите город вылета</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3 popularDepartures">
						<ul class="nt-list">
							<li><a href="javascript:void(0)" onclick="selectDeparture(1, 'Москва')">Москва</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(3, 'Екатеринбург')">Екатеринбург</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(6, 'Челябинск')">Челябинск</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(28, 'Оренбург')">Оренбург</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(7, 'Самара')">Самара</a></li>
							<li><a href="javascript:void(0)" onclick="selectDeparture(4, 'Уфа')">Уфа</a></li>
						</ul>
					</div>
					<div class="col-md-9 allDepartures">
						<div class="list customScroll">
							<ul class="nt-list multiColumn" id="departureList">
								<li class="divider"></li>
								<li><a href="#">Без перелета</a></li>
								<li class="divider">А</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<!-----------------------------------------------------------------------------------------------------
    Модальное окно выбора страны
 ------------------------------------------------------------------------------------------------------>
<div class="modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="countryModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Выберите страну</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3 popularCountry">
						<h5>Популярные страны</h5>
						<ul class="nt-list">
							<? foreach ($arResult["POPULAR_COUNTRIES"] as $key => $popCountry): ?>
								<li>
									<img src="<?=CFile::GetPath($popCountry["PICTURE"])?>" alt="<?=$popCountry["NAME"]?>"/>
									<a href="javascript:void(0)" onclick="selectCountry(<?=$key?>, '<?=$popCountry["NAME"]?>')"><?=$popCountry["NAME"]?></a>
								</li>
							<? endforeach; ?>
						</ul>
					</div>
					<div class="col-md-9 allCountry">
						<h5>Выберите страну:</h5>
						<div class="list customScroll">
							<ul class="nt-list multiColumn" id="countryList">
							</ul>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<script>
	//Глобальные переменные
	var nfrom = 0; //Ночей от
	var nto = 0; //Ночей до


	//Событие при смене города отправления
	function selectDeparture(id, name) {
		$("#departure").val(id);
		$("#departureValue").text(name);
		$("#departureModal").modal('hide');
		getCountries(id);

		getFlydates(id, $("#country").val());
	}

	//Событие при смене страны
	function selectCountry(id, name) {
		$("#country").val(id);
		$("#countryValue").text(name);
		$("#countryModal").modal('hide');

		getFlydates($("#departure").val(), id);
	}


	$(document).ready(function () {
		getDepartures(<?=$departure?>);
		getCountries(<?=$departure?>, <?=$country?>);
		getFlydates(<?=$departure?>, <?=$country?>);
	});

</script>