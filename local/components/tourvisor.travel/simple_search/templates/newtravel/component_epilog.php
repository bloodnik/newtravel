<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>

<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/jQuery.AutoColumnList/jquery.autocolumnlist.min.js"></script>
<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/tourvisor/simple-search.js"></script>
