<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
  "NAME" => "Простой поиск туров",
  "DESCRIPTION" => "Отображает упрощенную форму поиска туров",
  "ICON" => "/images/icon.gif",
  "COMPLEX" => "N",
  "PATH" => array(
    "ID" => "Турвизор",
  ),
);
?>