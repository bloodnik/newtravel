<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

//PR($_REQUEST);

// Подключение модуля
CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
global $DB;

//Функция выполнения get-запроса через CURl
function makeQuery($url) {
	if ($curl = curl_init()) {
		curl_setopt($curl, CURLOPT_URL, $url);
	}
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$json  = curl_exec($curl);
	$arRes = objectToArr(json_decode($json));
	curl_close($curl);

	return $arRes;
}

$cacheTime = 3600;
$cacheId   = 'arCacheResult';
$cachePath = '/newtravel/search/';
$obCache   = new CPHPCache();

//Получаем кеш
if ($obCache->InitCache($cacheTime, $cacheId, $cachePath)) {
	$res = $obCache->GetVars();
	if (is_array($res["arResult"]) && (count($res["arResult"]) > 0)) {
		$arCacheResult = $res["arResult"];
	}
	$arResult = $arCacheResult;
}
//Если кеша нет
if ( ! is_array($arCacheResult)) {

	//Получаем баннеры
	$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "PREVIEW_TEXT");
	$arFilter = array("IBLOCK_CODE" => "banner", "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");

	$resBanner = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => 3), $arSelect);

	while ($ob = $resBanner->GetNextElement()) {
		$arFields              = $ob->GetFields();
		$arResult["BANNERS"][] = $arFields;
	}

	// Определяем инфоблок туров
	$res = CIBlock::GetList(Array(), Array('TYPE' => 'simai', 'CODE' => 'travel_tour'), true);
	if ($ar_res = $res->Fetch()) {
		$arResult["TOURS_IBLOCK_ID"] = $ar_res['ID'];
	}

	//////////// end cache /////////
	$obCache->StartDataCache($cacheTime, $cacheId, $cachePath);
	$obCache->EndDataCache(array("arResult" => $arResult));
}


if (isset($arParams["REQUEST_ID"])) {
	$login = "lili";
	$pass  = "271000";

	$onPage = $_REQUEST["onpage"]?:"25";

	$param = "&type=result";
	$param .= "&requestid=" . $_REQUEST["requestid"];
	$param .= "&page=" . $_REQUEST["page"];
	$param .= "&onpage=" . $onPage;
	$param .= "&nodescription=0";

	$url = "http://tourvisor.ru/xml/result.php?format=json" . $param . "&authlogin=" . $login . "&authpass=" . $pass;

	//PR($url);

	$arData = makeQuery($url);
}

$arResult["REQUEST_URL"]    = $url;
$arResult["REQUEST_ID"]    = $arParams["REQUEST_ID"];
$arResult["TOURS"]         = $arData["data"]["result"];
$arResult["SEARCH_STATUS"] = $arData["data"]["status"];


// Покупка тура
if ( ! empty($_REQUEST["tourid"])) {

	$param .= "&tourid=" . $_REQUEST["tourid"];
	$param .= "&flights=1";
	$param .= "&request=0";

	$url    = "http://tourvisor.ru/xml/actualize.php?format=json" . $param . "&authlogin=" . $login . "&authpass=" . $pass;
	$arTour = makeQuery($url);

	global $USER;

	if (CModule::IncludeModule("sale")) {
		CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
	}

	$arTour = $arTour["data"]["tour"];


	// Добавляем тур в базу
	$el                 = new CIBlockElement;
	$PROP               = $arTour;
	$PROP["USER_ID"]    = $USER->GetID();
	$PROP["requestId"]  = $arParams["REQUEST_ID"];
	$PROP["tourid"]     = $_REQUEST["tourid"];
	$arLoadProductArray = Array(
		"MODIFIED_BY"       => $USER->GetID(),
		"IBLOCK_SECTION_ID" => false,
		"IBLOCK_ID"         => $arResult["TOURS_IBLOCK_ID"],
		"PROPERTY_VALUES"   => $PROP,
		"NAME"              => $arTour["departurename"] . "_" . $arTour["hotelregionname"] . "_" . $_REQUEST["tourid"],
		"tourname"          => $arTour["departurename"] . "_" . $arTour["hotelregionname"] . "_" . $_REQUEST["tourid"],
		"ACTIVE"            => "Y",
	);
	$PRODUCT_ID         = $el->Add($arLoadProductArray);

	// Проставляем количество
	$arFields = array(
		"ID"             => $PRODUCT_ID,
		"QUANTITY"       => 1,
		"QUANTITY_TRACE" => "N"
	);
	CCatalogProduct::Add($arFields);

	// Назначаем цену
	$arFields      = Array(
		"PRODUCT_ID"       => $PRODUCT_ID,
		"CATALOG_GROUP_ID" => 1,
		"PRICE"            => $arTour["price"],
		"CURRENCY"         => "RUB",
		"QUANTITY_FROM"    => false,
		"QUANTITY_TO"      => false
	);
	$PRODUCT_PRICE = CPrice::Add($arFields);

	// Добавляем тур в корзину
	$arFields          = array(
		"PRODUCT_ID"          => $PRODUCT_ID,
		"PRODUCT_PRICE_ID"    => $PRODUCT_PRICE,
		"PRICE"               => $arTour["price"],
		"CURRENCY"            => "RUB",
		"WEIGHT"              => 0,
		"QUANTITY"            => 1,
		"LID"                 => LANG,
		"DELAY"               => "N",
		"CAN_BUY"             => "Y",
		"NAME"                => $arTour["departurename"] . "_" . $arTour["hotelregionname"] . "_" . $_REQUEST["tourid"],
		"CALLBACK_FUNC"       => "CatalogBasketCallback",
		"MODULE"              => "catalog",
		"NOTES"               => "",
		"ORDER_CALLBACK_FUNC" => "MyBasketOrderCallback",
		"DETAIL_PAGE_URL"     => ""
	);
	$arFields["PROPS"] = array(
		0  => array(
			"NAME"  => "Количество звезд",
			"CODE"  => "STAR_NAME",
			"VALUE" => $arTour["hotelstars"]
		),
		1  => array(
			"NAME"  => "Количество взрослых",
			"CODE"  => "ADULTS",
			"VALUE" => $arTour["adults"]
		),
		2  => array(
			"NAME"  => "Дата вылета",
			"CODE"  => "CHECK_IN_DATE",
			"VALUE" => $arTour["flydate"],
			"SORT"  => "10"
		),
		3  => array(
			"NAME"  => "Ночей",
			"CODE"  => "NIGHTS",
			"VALUE" => $arTour["nights"],
			"SORT"  => "20"
		),
		4  => array(
			"NAME"  => "Страна",
			"CODE"  => "COUNTRY_NAME",
			"VALUE" => $arTour["countryname"]
		),
		5  => array(
			"NAME"  => "Курорт",
			"CODE"  => "RESORT_NAME",
			"VALUE" => $arTour["hotelregionname"],
			"SORT"  => "30"
		),
		6  => array(
			"NAME"  => "Id отеля",
			"CODE"  => "HOTEL_ID",
			"VALUE" => $arTour["hotelcode"]
		),
		7  => array(
			"NAME"  => "Отель",
			"CODE"  => "HOTEL_NAME",
			"VALUE" => $arTour["hotelname"]
		),
		8  => array(
			"NAME"  => "Рейтинг отеля",
			"CODE"  => "HOTEL_RATING",
			"VALUE" => $arTour["hotelrating"]
		),
		9  => array(
			"NAME"  => "Номер",
			"CODE"  => "HT_PLACE_DESCRIPTION",
			"VALUE" => $arTour["room"]
		),
		10 => array(
			"NAME"  => "Питание",
			"CODE"  => "MEAL_DESCRIPTION",
			"VALUE" => $arTour["meal"] . " " . $arTour["mealrussian"]
		),
		11 => array(
			"NAME"  => "Стоимость",
			"CODE"  => "PRICE",
			"VALUE" => "Оригинальная стоимость: " . $arTour["price"] . "  \r\nТопливный сбор: " . $arTour["fuelcharge"] . "RUB"
		),
		12 => array(
			"NAME"  => "Город вылета",
			"CODE"  => "CITY_FROM_NAME",
			"VALUE" => $arTour["departurename"]
		),
		13 => array(
			"NAME"  => "Количество детей",
			"CODE"  => "KIDS",
			"VALUE" => $arTour["child"]
		),
		14 => array(
			"NAME"  => "Общая сумма топливных доплат",
			"CODE"  => "FINAL_OIL_TAX",
			"VALUE" => $arTour["fuelcharge"]
		),
		15 => array(
			"NAME"  => "Цена без доплат",
			"CODE"  => "CLEAR_PRICE",
			"VALUE" => $arTour["price"] - $arTour["fuelcharge"] - $arTour["visa"]
		),
		16 => array(
			"NAME"  => "TOUR ID",
			"CODE"  => "TOUR_ID",
			"VALUE" => $_REQUEST["tourid"]
		),
		17 => array(
			"NAME"  => "Размещение",
			"CODE"  => "PLACEMENT",
			"VALUE" => $arTour["placement"]
		),
		18 => array(
			"NAME"  => "Фотография",
			"CODE"  => "HOTELIMG",
			"VALUE" => $arTour["hotelpicturemedium"]
		),
		19 => array(
			"NAME"  => "Описание",
			"CODE"  => "HOTEL_DESCRIPTION",
			"VALUE" => $arTour["hoteldescription"]
		),
		21 => array(
			"NAME"  => "Ссылка на оператора",
			"CODE"  => "OPERATORLINK",
			"VALUE" => $arTour["operatorlink"]
		),

	);
	CSaleBasket::Add($arFields);


	//Add2BasketByProductID($PRODUCT_ID, 1, $arFields, $arFields["PROPS"]);
	//Add2Basket($PRODUCT_PRICE, 1, array(), $arFields["PROPS"]);
	$arResult["BUY"] = true;

}

$this->IncludeComponentTemplate();

?>