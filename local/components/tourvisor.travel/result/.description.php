<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
  "NAME" => "Выводит найденные туры",
  "DESCRIPTION" => "Не подключается, вызывается только самим модулем",
  "ICON" => "/images/icon.gif",
  "COMPLEX" => "N",
  "PATH" => array(
    "ID" => "Турвизор",
  ),
);
?>