<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>

<? if (!empty($arResult["TOURS"]["hotel"])): ?>


    <table class="footable">
        <thead>
        <tr>
            <th></th>
            <th>Даты тура</th>
            <th data-hide="phone">Ночи</th>
            <th data-hide="phone,tablet">Питание</th>
            <th data-hide="phone,tablet">Тип номера</th>
            <th data-hide="phone">Цена на сайте</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($arResult["TOURS"]["hotel"] as $arHotel): ?>
            <?foreach($arHotel["tours"]["tour"] as $key=>$arTour):?>
                <tr <?if($key >= 3):?>class="hidden-tour" style="display: none" <?endif;?>>
                    <td class="favourite"><img src="https://tourvisor.ru/pics/operators/searchlogo/<?=$arTour['operatorcode']?>.gif" alt="<?=$arTour['operatorname']?>"/></td>
                    <td><?=$arTour["flydate"]?></td>
                    <td><?=$arTour["nights"]?></td>
                    <td><?=$arTour["meal"]?> <?=$arTour["mealrussian"]?></td>
                    <td><?=$arTour["room"]?></td>
                    <td><?=CurrencyFormat($arTour["price"], 'RUB')?></td>
                    <td><a  href="/personal/order/make" target="_blank" class="btn btn-blue pull-right" onclick="buyTour('<?=$arParams["REQUEST_ID"]?>', '<?=$arTour["tourid"]?>')" >подробнее</a></td>
                </tr>
            <?endforeach;?>
        <?endforeach;?>
        </tbody>
    </table>

<?elseif(empty($arResult["TOURS"]["hotel"]) && $arParams["IS_FINISH"] == "true"):?>
    <div class="col-xs-12 tours-section">
        <?$APPLICATION->IncludeComponent(
            "bitrix:form.result.new",
            "tour_help",
            Array(
                "AJAX_MODE" => "Y",
                "SEF_MODE" => "N",
                "WEB_FORM_ID" => "6",
                "LIST_URL" => "/tours",
                "EDIT_URL" => "/tours",
                "SUCCESS_URL" => "",
                "CHAIN_ITEM_TEXT" => "",
                "CHAIN_ITEM_LINK" => "",
                "IGNORE_CUSTOM_TEMPLATE" => "N",
                "USE_EXTENDED_ERRORS" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "VARIABLE_ALIASES" => Array(
                    "WEB_FORM_ID" => "WEB_FORM_ID",
                    "RESULT_ID" => "RESULT_ID"
                ),
            ),
            false
        );?>

    </div>
<? endif; ?>
