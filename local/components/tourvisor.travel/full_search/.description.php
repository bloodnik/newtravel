<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
  "NAME" => "Полный поиск туров",
  "DESCRIPTION" => "Отображает полную форму поиска туров",
  "ICON" => "/images/icon.gif",
  "COMPLEX" => "N",
  "PATH" => array(
    "ID" => "Турвизор",
  ),
);
?>