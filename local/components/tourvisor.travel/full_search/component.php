<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

$geoCity   = $GLOBALS["cityFull"]["city"]["name_ru"];
$geoRegion = $GLOBALS["cityFull"]["region"]["name_ru"];

$arResult = array();

if ( ! empty($geoCity)) {
	$arSelect = Array("ID", "NAME", "CODE");
	$arFilter = Array(
		"IBLOCK_CODE" => "tourvisor.departure",
		"NAME"        => $geoCity,
		"ACTIVE_DATE" => "Y",
		"ACTIVE"      => "Y"
	);

	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 50), $arSelect);

	while ($ob = $res->GetNextElement()) {
		$arFields  = $ob->GetFields();
		$departure = $arFields["CODE"];
	}
	$arResult["DEPARTURE"] = $departure;

}

$arResult["DEPARTURE"] = $arParams['CITY'];

if (empty($arResult["DEPARTURE"])) {
	$arFilter = Array("IBLOCK_CODE" => "tourvisor.departure", "NAME" => $geoRegion);
	$db_list  = CIBlockSection::GetList(Array($by => $order), $arFilter, true, array("ID"));
	while ($ar_result = $db_list->GetNext()) {
		$SECTION_ID = $ar_result["ID"];
	}

	$arSelect = Array("ID", "NAME", "CODE");
	$arFilter = Array(
		"IBLOCK_CODE" => "tourvisor.departure",
		"SECTION_ID"  => $SECTION_ID,
		"ACTIVE_DATE" => "Y",
		"ACTIVE"      => "Y"
	);

	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 50), $arSelect);

	while ($ob = $res->GetNextElement()) {
		$arFields  = $ob->GetFields();
		$departure = $arFields["CODE"];
	}
	$arResult["DEPARTURE"] = $departure;

}

//Область получения данных из БД. Кеширование
$cacheTime = 0;
$cacheId   = 'arCacheSearch';
$cachePath = '/newtravel/search/';
$obCache   = new CPHPCache();

//Получаем кеш
if ($obCache->InitCache($cacheTime, $cacheId, $cachePath)) {
	$res = $obCache->GetVars();
	if (is_array($res["arResult"]) && (count($res["arResult"]) > 0)) {
		$arCacheResult = $res["arResult"];
	}
	$arResult = $arCacheResult;
}
//Если кеша нет
if ( ! is_array($arCacheResult)) {

//=======Получаем популярные страны========/
	$arSelect = Array("ID", "NAME", "CODE", "PREVIEW_PICTURE");
	$arFilter = Array("IBLOCK_CODE" => "popular_countries", "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
	$res      = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array(), $arSelect);
	while ($ob = $res->GetNextElement()) {
		$arFields                                           = $ob->GetFields();
		$arResult["POPULAR_COUNTRIES"][ $arFields["CODE"] ] = array(
			"NAME"    => $arFields["NAME"],
			"PICTURE" => $arFields["PREVIEW_PICTURE"]
		);
	}
	//////////// end cache /////////
	$obCache->StartDataCache($cacheTime, $cacheId, $cachePath);
	$obCache->EndDataCache(array("arResult" => $arResult));
}

if (CModule::IncludeModule("currency")) {
}

$this->IncludeComponentTemplate();

