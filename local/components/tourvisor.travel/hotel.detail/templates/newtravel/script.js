/********************************************** ОБЩИЕ ФУНКЦИИ *******************************************************/
$( document ).ready(function() {

    var imgshinevar, map, mapintab, photosintab, $activetab;

    /* ==========================================================================
     Bootstrap Tabs (Map + Photos)
     ========================================================================== */
    function createHotelsCarousel(id) {
        var element = $(id);
        var result;

        element.imagesLoaded(function(instance){

            result = element.carouFredSel({
                circular    : true,
                prev: ".caroufredsel-prev",
                next: ".caroufredsel-next",
                height: 320,
                width: "100%",
                items       : {
                    visible: 'odd+2'
                },
                swipe       : {
                    onTouch     : true,
                    onMouse     : true,
                    options     : {
                        excludedElements: "button, input, select, textarea, .noSwipe"
                    },
                    onBefore    : function(){
                        //hover effect fix
                        element.trigger('mouseleave');
                        element.find('.carousel-item').trigger('mouseup');
                    }
                },
                scroll      : {
                    items: 1
                    //timeoutDuration: 1250
                },
                mousewheel  :{
                    items: 1
                },
                auto        : {
                    play: true
                }
            });

            element.find('.carousel-item').fancybox({
                cyclic          : true,
                opacity         : true,
                overlayOpacity  : 0.9,
                zoomSpeedIn     : 1000,
                scrolling       : 'no',
                onStart         : function() {
                    element.trigger("pause");
                },
                onClosed        : function() {
                    element.trigger("play");
                },
                onComplete      : function(){
                    $('#fancybox-wrap').css('margin-top', '-50px');
                    var fb = $("#fancybox-content");
                    $(fb).css({'opacity': '1', 'height': '520px'});
                }
            });

        }).animate({'opacity': 1}, 1000);//images loaded

        return result;
    }

    createHotelsCarousel('.hotel-photos .carousel');

    $(document).on('shown.bs.tab', 'a[href="#tab-photo"]', function (e) {

        if (photosintab == undefined) {

            createHotelsCarousel('.hotel-photos .carousel');

        } else {
            $('.hotel-photos .carousel').trigger('updateSizes');
        }

    });

    $(document).on('shown.bs.tab', 'a[href="#tab-photo"]', function (e) {
        $('.carousel-photo').trigger('updateSizes');
    });


    /* fullwidth carousel swipe link fix
     -------------------------------------------------------------------------- */
    var $mousePosStart = 0;
    var $mousePosEnd = 0;
    $('.carousel-wrap[data-full-width="true"] .portfolio-items .col .work-item .work-info a').mousedown(function(e){
        $mousePosStart = e.clientX;
    });

    $('.carousel-wrap[data-full-width="true"] .portfolio-items .col .work-item .work-info a').mouseup(function(e){
        $mousePosEnd = e.clientX;
    });

    $('.carousel-wrap[data-full-width="true"] .portfolio-items .col .work-item .work-info a').click(function(e){
        if(Math.abs($mousePosStart - $mousePosEnd) > 10)  return false;
    });



    /* ==========================================================================
     ToolTip
     ========================================================================== */
    $("li[data-rel=tooltip]").tooltip();
    $("a[data-rel=modal-tooltip]").tooltip();
    $("a[data-rel=tooltip]").tooltip({container: 'body'});
    $("i[data-rel=tooltip]").tooltip({container: 'body'});
    $("span[data-rel=tooltip]").tooltip({container: 'body'});

});
