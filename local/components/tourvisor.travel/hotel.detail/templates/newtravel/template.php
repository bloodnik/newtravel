<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/plugins/jquery.isotope.js");
$this->addExternalJs(SITE_TEMPLATE_PATH."/js/plugins/gmaps.min.js");
?>
<section class="white-section">
	<div class="tabs-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-7 col-sm-12">
					<div class="hotel-title">
						<h3><?=$arResult["hotel"]["name"]?></h3>
                                <span class="hotel-stars">
                                <?
                                switch ($arResult["hotel"]['stars']) {
	                                case "1":
		                                echo '<i class="icon icon-star-filled"></i>';
		                                break;
	                                case "2":
		                                echo '<i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i>';
		                                break;
	                                case "3":
		                                echo '<i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i>';
		                                break;
	                                case "4":
		                                echo '<i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i>';
		                                break;
	                                case "5":
		                                echo '<i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i>';
		                                break;
	                                default:
		                                echo "";
		                                break;
                                }
                                ?>
                                </span>
                                <span class="links">
                                    <a href="#"><?=$arResult["hotel"]["country"]?></a>, <a href="#"><?=$arResult["hotel"]["region"]?></a>
                                </span>
					</div>
				</div>

				<div class="col-lg-4 col-md-7 col-sm-12">
					<div class="hotel-info">
						<span class="rating">Рейтинг: <?=$arResult["hotel"]["rating"]?> / 5</span>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-photo" data-toggle="tab"><i class="icon icon-picture"></i>Фотографии</a></li>
						<li><a href="#tab-map" id="gmap" data-toggle="tab"><i class="icon icon-location"></i>Карта</a></li>
						<li><a href="#tour-search-anchor"><i class="icon icon-flight"></i>Туры в отель</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div id="map-photos" class="tab-content">
			<div class="tab-pane fade in active" id="tab-photo">
				<div class="hotel-photos" style="position: relative">
					<ul class="carousel">
						<? foreach ($arResult["hotel"]["images"]["image"] as $image): ?>
							<li>
								<a class="carousel-item" href="<?=$image?>"><img src="<?=$image?>" height="220" alt="<?=$arResult["hotel"]["name"]?>"></a>
							</li>
						<? endforeach; ?>
					</ul>
					<a class="caroufredsel-prev" href="#" style="display: block;"><span> <i class="fa fa-chevron-left"></i> </span></a>
					<a class="caroufredsel-next" href="#" style="display: block;"><span> <i class="fa fa-chevron-right"></i> </span></a>
				</div>
			</div>

			<div class="tab-pane" id="tab-map">
				<div id="mapintab" style="height: 350px;"></div>
				<script>
					$(document).on('shown.bs.tab', 'a[href="#tab-map"]', function (e) {

						mapintab = new GMaps({
							el: '#mapintab',
							//height: 322,
							scrollwheel: false,
							lat: <?=isset($arResult["hotel"]["coord1"]) ? $arResult["hotel"]["coord1"] : "0"?>,
							lng: <?=isset($arResult["hotel"]["coord2"]) ? $arResult["hotel"]["coord2"] : "0"?>
						});

						mapintab.addMarker({
							lat: <?=isset($arResult["hotel"]["coord1"]) ? $arResult["hotel"]["coord1"] : "0"?>,
							lng: <?=isset($arResult["hotel"]["coord2"]) ? $arResult["hotel"]["coord2"] : "0"?>,
							clickable: false,
							icon: "<?=SITE_TEMPLATE_PATH?>/images/mappin.png"
						});
					});
				</script>
			</div>
		</div>
	</div>
	<div class="container" id="tour-search-anchor">
		<div class="hotel-details">
			<div class="row">
				<div class="col-md-4">
					<div class="hotel-spec-item i-contacts">
						<h3>Контакты</h3>
						<p><span>Телефон:</span> <?=$arResult["hotel"]["phone"]?></p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="hotel-spec-item i-info">
						<h3>Кратко об отеле</h3>
						<? if (isset($arResult["hotel"]["build"])): ?>
							<p><span>Год постройки:</span> <?=$arResult["hotel"]["build"]?></p>
						<? endif; ?>
						<? if (isset($arResult["hotel"]["repair"])): ?>
							<p><span>Последний ремонт:</span> <?=$arResult["hotel"]["repair"]?></p>
						<? endif; ?>
						<? if (isset($arResult["hotel"]["placement"])): ?>
							<p><span>Расположение:</span><?=$arResult["hotel"]["placement"]?></p>
						<? endif; ?>
					</div>
				</div>
				<div class="col-md-4 beach">
					<div class="hotel-spec-item i-beach">
						<h3>Пляж</h3>
						<?=$arResult["hotel"]["beach"]?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!--Поиск тура-->
<? $APPLICATION->IncludeComponent(
	"tourvisor.travel:hotel.search", 
	"newtravel",
	array(
		"CITY" => "4",
		"COUNTRY" => "4",
		"NIGHTSFROM" => "6",
		"NIGHTSTO" => "14",
		"ADULTS" => "2",
		"STARS" => "3",
		"ACTION" => "",
		"NEW" => "N",
		"COUNT" => "10",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"COUNTRY_CODE" => $arResult["hotel"]["countrycode"]
	),
	false
); ?>

<!-- =========================================
White Section ( hotel detailed + responses )
========================================== -->
<section class="white-section">
	<div class="tabs-box">

		<div class="container">
			<div class="row">

				<div class="col-md-12">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-services" data-toggle="tab"><i class="icon icon-cogs"></i>Услуги отеля</a></li>
						<li><a href="#tab-info" data-toggle="tab"><i class="icon icon-info"></i>Подробное описание</a></li>
					</ul>
				</div><!-- /col-md-12 -->

			</div><!-- /row -->
		</div><!-- /container -->

		<div class="tab-content">
			<div class="tab-pane fade in active" id="tab-services">
				<div class="container">
					<? if (isset($arResult["territory"]) && ! empty($arResult["territory"])): ?>
						<div class="col-sm-12 hotel-spec-item">
							<h3>Инфраструктура отеля:</h3>
							<i class="fa fa-hand-o-up"></i>
							<? foreach ($arResult["territory"] as $item): ?>
								<div><?=$item?></div>
							<? endforeach; ?>
						</div>
					<? endif ?>
					<? if (isset($arResult["services"]) && ! empty($arResult["services"])): ?>
						<div class="col-sm-12 hotel-spec-item">
							<h3>Услуги отлея:</h3>
							<i class="fa fa-lightbulb-o"></i>
							<? foreach ($arResult["services"] as $item): ?>
								<div><?=$item?></div>
							<? endforeach; ?>
						</div>
					<? endif ?>
					<? if (isset($arResult["animation"]) && ! empty($arResult["animation"])): ?>
						<div class="col-sm-12 hotel-spec-item">
							<h3>Развлечения(анимация):</h3>
							<i class="fa fa-smile-o"></i>
							<? foreach ($arResult["animation"] as $item): ?>
								<div><?=$item?></div>
							<? endforeach; ?>
						</div>
					<? endif; ?>
					<? if (isset($arResult["child"]) && ! empty($arResult["child"])): ?>
						<div class="col-sm-12 hotel-spec-item">
							<h3>Услуги для детей:</h3>
							<i class="fa fa-child"></i>
							<? foreach ($arResult["child"] as $item): ?>
								<div><?=$item?></div>
							<? endforeach; ?>
						</div>
					<? endif; ?>
					<? if (isset($arResult["servicefree"]) && ! empty($arResult["servicefree"])): ?>
						<div class="col-sm-12 hotel-spec-item">
							<h3>Бесплатные услуги:</h3>
							<i class="fa fa-check-circle-o"></i>
							<? foreach ($arResult["servicefree"] as $item): ?>
								<div><?=$item?></div>
							<? endforeach; ?>
						</div>
					<? endif; ?>
					<? if (isset($arResult["servicepay"]) && ! empty($arResult["servicepay"])): ?>
						<div class="col-sm-12 hotel-spec-item">
							<h3>Платные услуги:</h3>
							<i class="fa fa-money"></i>
							<? foreach ($arResult["servicepay"] as $item): ?>
								<div><?=$item?></div>
							<? endforeach; ?>
						</div>
					<? endif; ?>
					<? if (isset($arResult["meallist"]) && ! empty($arResult["meallist"])): ?>
						<div class="col-sm-12 hotel-spec-item">
							<h3>Типы питания:</h3>
							<i class="fa fa-cutlery"></i>
							<? foreach ($arResult["meallist"] as $item): ?>
								<div><?=$item?></div>
							<? endforeach; ?>
						</div>
					<? endif; ?>

				</div>
			</div><!-- /tab-pane -->

			<div class="tab-pane fade in" id="tab-info">
				<div class="container">
					<? if (isset($arResult["hotel"]["description"]) && ! empty($arResult["hotel"]["description"])): ?>
						<p><?=$arResult["hotel"]["description"]?></p>
					<? else: ?>
						<p>Описание отеля недоступно</p>
					<? endif ?>
				</div>
			</div><!-- /tab-pane -->

		</div><!-- /tab-content -->

	</div><!-- /tabs-box -->