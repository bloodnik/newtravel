<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

$arResult = array();

//Функция выполнения get-запроса через CURl
function makeQuery($url) {
	if ($curl = curl_init()) {
		curl_setopt($curl, CURLOPT_URL, $url);
	}
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$json  = curl_exec($curl);
	$arRes = objectToArr(json_decode($json));
	curl_close($curl);

	return $arRes;
}

$login = "lili";
$pass  = "271000";


if (isset($_REQUEST["hotelcode"])) {
	$param = "&hotelcode=" . $_REQUEST["hotelcode"];
}

$url = "http://tourvisor.ru/xml/hotel.php?format=json&removetags=1" . $param . "&authlogin=" . $login . "&authpass=" . $pass;

$arData = makeQuery($url);

$arResult = $arData["data"];

$arResult["territory"]   = explode(";", $arData["data"]["hotel"]["territory"]);
$arResult["services"]    = explode(";", $arData["data"]["hotel"]["services"]);
$arResult["animation"]   = explode(";", $arData["data"]["hotel"]["animation"]);
$arResult["child"]       = explode(";", $arData["data"]["hotel"]["child"]);
$arResult["servicefree"] = explode(";", $arData["data"]["hotel"]["servicefree"]);
$arResult["servicepay"]  = explode(";", $arData["data"]["hotel"]["servicepay"]);
$arResult["meallist"]    = explode(";", $arData["data"]["hotel"]["meallist"]);

$APPLICATION->SetTitle($arResult["hotel"]["name"]);

$this->IncludeComponentTemplate();

