<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>
<!-- =========================================
Gray Section ( hotel tour form )
========================================== -->
<section class="gray-section" style="background: #eaeaea">
	<div class="container">
		<div class="hotel-tour-form">
			<div class="row">
				<div class="col-md-12">
					<h3 class="hotel-tour-form-title">Туры в отель</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="hotel-tour-form-line departure_wrap" data-container="body">
						<span><strong>C вылетом из</strong></span>
						<input type="hidden" name="departure" id="departure" value="<?=$arResult['CURRENT_DEPARTURE']['UF_XML_ID']?>"/>
						<div class="form-control pseudo-input" id="departureValue" style="display: inline-block;width: 180px;" data-toggle="modal" data-target="#departureModal">
							<?=$arResult['CURRENT_DEPARTURE']['UF_NAME_FROM']?>
						</div>
					</div>

					<div class="hotel-tour-form-line daterange_input" data-toggle="tooltip" data-placement="top" data-trigger="manual" title="Выбирайте  даты вылета">
						<span><strong>даты вылета</strong></span>
						<input type="text" class="form-control" name="daterange" id="daterange" value="<?=$arResult['REQUEST_PARAMS']['DATERANGE']?>"
						       style="display: inline-block;width: 160px;height: 36px;"/>
					</div>
					<div class="hotel-tour-form-line">
						<span><strong>ночей:</strong></span>
						<div class="form-control pseudo-input" style="display: inline-block;width: 100px;" id="nightsValue">
							от <?=$arResult['REQUEST_PARAMS']['NIGHTSFROM']?> до <?=$arResult['REQUEST_PARAMS']['NIGHTSTO']?>
						</div>
					</div>
					<div class="hotel-tour-form-line adults_input" data-toggle="tooltip" data-placement="top" data-trigger="manual" title="Выберите взрослых и детей">
						<div class="toursit-select">
							<span><strong>туристы</strong></span>
							<div class="form-control pseudo-input tourists-total" style="display: inline-block;width: 121px">
								<?=$arResult['REQUEST_PARAMS']['ADULTS']?>взр. <? if ($arResult['REQUEST_PARAMS']['CHILD'] > 0): ?><?=$arResult['REQUEST_PARAMS']['CHILD']?> реб.<? endif; ?>
							</div>
						</div>
					</div>

					<div class="hotel-tour-form-line">
						<span><strong>питание</strong></span>
						<select name="meal" id="meal" class="pseudo-input">
							<option value="1">Любое</option>
							<? foreach ($arResult['MEAL_LIST'] as $arMeal): ?>
								<option <? if ($arResult['REQUEST_PARAMS']['MEAL'] == $arMeal['UF_XML_ID']): ?>selected<? endif; ?> value="<?=$arMeal['UF_XML_ID']?>">
									<?=$arMeal['UF_NAME'] . "(" . $arMeal['UF_CODE'] . ")"?>
								</option>
							<? endforeach; ?>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5 hotel-tour-form-line">
					<span class="operator-label"><strong>Туроператор</strong></span>
					<select id="operators" class="multiply" multiple="multiple">
						<? foreach ($arResult['OPERATOR_LIST'] as $arOperator): ?>
							<option value="<?=$arOperator['UF_XML_ID']?>" <? if ($arOperator['SELECTED']): ?>selected<? endif; ?>><?=$arOperator['UF_NAME']?></option>
						<? endforeach; ?>
					</select>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-7">
					<!--noindex-->
						<button id="search" name="search" style="width: 105px" onclick="searchObject.searchTours()" class="btn btn-blue"><i class="fa fa-search"></i> Найти</button>
					<!--/noindex-->
				</div>
				<div class="col-md-12">
					<hr>

					<div class="hotel-tour-results">
						<!-- =========================================
						Результаты
						========================================== -->
						<div class="col-xs-12">

							<div class="col-xs-12 progress-section" style="display: none">
								<!--Результаты поиска туров-->
								<div class="col-md-4 col-sm-9">
									<div class="progress progress-striped active" style="display: block">
										<div class="progress-bar" style="width: 0%; color:#000"></div>
									</div>
								</div>
								<div class="col-md-2 col-sm-2" id="progress-percent">0%</div>
								<div class="col-md-3 col-xs-6" id="progress-status">идет поиск</div>
								<div class="col-md-3 col-xs-6" id="tours-found">0 туров найдено</div>
							</div>

							<div id="currentPage" style="display: none">1</div>
							<div id="requestid" style="display: none">0</div>

							<div id="request" style="display: none;"></div>

							<? if (isset($_REQUEST["formresult"]) && $_REQUEST["formresult"] == "addok"): ?>
								<!--Результаты поиска туров-->
								<div class="tour-list-wrap" id="formok">
									<div class="tour-item">
										<div class="col-md-12 yes">
											Заявка на подбор успешно отправлена
										</div>
									</div>
								</div>
							<? endif; ?>
						</div>
					</div><!-- /hotel-tour-results -->
				</div>
				<div class="col-md-12">
					<a href="javascript:void(0)" rel="nofollow" class="show-all hidden" onclick="$('.hidden-tour').slideToggle(200);">показать/скрыть все предложения</a>
				</div>
			</div>
		</div><!-- /hotel-tour-form -->

	</div><!-- /container -->
</section>


<!-----------------------------------------------------------------------------------------------------
    Модальное окно выбора города отправления
 ------------------------------------------------------------------------------------------------------>
<div class="modal fade" id="departureModal" tabindex="-1" role="dialog" aria-labelledby="departureModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="departureModalLabel">Выберите город вылета</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-2 popularDepartures hidden-xs hidden-sm">
						<ul class="nt-list">
							<li><a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(1, 'Москва')">Москва</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(3, 'Екатеринбург')">Екатеринбург</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(6, 'Челябинск')">Челябинск</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(28, 'Оренбург')">Оренбург</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(7, 'Самара')">Самара</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(4, 'Уфа')">Уфа</a></li>
						</ul>
					</div>
					<div class="col-md-10 allDepartures">
						<div class="row">
							<div class="list customScroll">
								<ul class="nt-list multiColumn" id="departureList">
									<li class="divider"></li>
									<li><a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(99, 'Без перелета')">Без перелета</a></li>
									<li class="divider">А</li>
									<? $first_letter = 'А'; ?>
									<? foreach ($arResult['DEPARTURE_LIST'] as $arDeparture): ?>
										<? if ($first_letter !== substr($arDeparture['UF_NAME'], 0, 2)): //Если новая буква, тогда выводим разделитель?>
											<li class="divider"><?=substr($arDeparture['UF_NAME'], 0, 2)?></li>
										<? endif; ?>
										<? $first_letter = substr($arDeparture['UF_NAME'], 0, 2); ?>
										<? if ($arDeparture['UF_XML_ID'] == 99) {
											continue;
										} //Убираем из списка, т.к. уже выведен до цикла?>
										<li>
											<a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(<?=$arDeparture['UF_XML_ID']?>, '<?=$arDeparture['UF_NAME']?>')">
												<?=$arDeparture['UF_NAME']?>
											</a>
										</li>
									<? endforeach; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Detail Tour Modal -->
<div class="modal fade" id="tourActulizeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

		</div>
	</div>
</div>

<script>
	var searchObject = new JSNewtravelSearch(<? echo CUtil::PhpToJSObject($arResult['REQUEST_PARAMS'], false, true); ?>);
	var helpformmessage, userurl; //Сообщение для подбора тура

</script>