<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CTourOrder $component */
global $USER;

$this->addExternalJs("https://posonline.kreditotdel.ru/widget/kreditotdel-connect-widget.js");

?>

<div class="preload" style="display: none;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="loader">
					<div class="loader-inner">
						<div class="box-1"></div>
						<div class="box-2"></div>
						<div class="box-3"></div>
						<div class="box-4"></div>
					</div>
					<span class="text">ждем...</span>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
    var orderData = <?=CUtil::PhpToJSObject($arResult)?>;
</script>

<div id="order-page" v-cloak>
	<div v-if="!data.DONE">
		<div v-if="data.TOUR_AVAILABLE">
			<div v-if="!data.DONE">
				<form class="form" action="<?=POST_FORM_ACTION_URI?>" name="ORDER_FORM" method="post">
					<? include "tourdata.php" ?>
					<? include "fligths.php" ?>
					<? include "user.php" ?>
					<? include "tourists.php" ?>
					<? include "delivery.php" ?>
					<? include "payments.php" ?>
					<? include "summary.php" ?>
				</form>
			</div>
		</div>
		<div v-else>
			<div class="container">
				<div class="alert alert-danger" v-for="error in data.ERRORS">
					{{error}}
				</div>
				<div class="display-7">Для поиска нового тура пройдите на страницу <a href="/tours/">поиска туров</a></div>
			</div>
		</div>
	</div>
	<div v-else>
		<? include "done.php" ?>
	</div>
</div>
