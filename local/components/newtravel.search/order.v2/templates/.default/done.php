<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CTourOrder $component */

?>
<section class="py-5 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-heading text-center">
					<h2 class="display-5 mb-4 ">Спасибо! Ваш заказ сформирован</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">

			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<p class="display-6">Номер вашего заказа: {{data.ORDER.ID}}</p>
				<p>В скором времени наши менеджеры свяжутся с Вами</p>

				<hr>

				<p class="h3 mb-3" style="font-weight: 600;">Для завершения бронирования необходимо провести оплату</p>

				<p class="display-7">Общая стоимость: <span class="h3 text-success" style="font-weight: 500;">{{data.ORDER.PRICE | formatPrice}}</span></p>

				<div class="display-7 mb-3">Варианты оплаты:</div>
				<div class="alert alert-warning" v-if="data.ORDER.PAYMENTS.length > 1">
					Вы выбрали разделение платежа. Можете оплатить любой из них прямо сейчас. Второй платеж необходимо оплатить за 2 недели до начала тура. <br>
					<span style="font-weight: bold;">Просим учесть,</span> что сумма второго платежа будет пересчитана по курсу валюты тура, на день оплаты!
				</div>
			</div>

			<div class="col-12" v-if="data.ORDER.CALCULATED">
				<div v-if="! data.ORDER.PAYED">
					<div class="row">
						<div class="col-12 col-md-6" v-for="(payment, index) in data.ORDER.PAYMENTS">
							<div v-if="payment.ACTION_FILE.length > 0 && payment.NEW_WINDOW && !payment.IS_CASH && !payment.IS_PAID">
								<div class="card p-3 mb-3">
									<div class="row d-flex align-items-center">
										<div class="col-6 display-7">
											{{payment.NAME}}
										</div>
										<div class="col-6 text-right">
											<div class="display-7" style="font-weight: 400;"><a class="btn btn-success" :href="payment.PAY_URL">Оплатить {{payment.SUM | formatPrice}}</a></div>
										</div>
									</div>
								</div>
							</div>
							<div v-else>
								<div class="card p-3 mb-3">
									<div class="row d-flex align-items-center">
										<div class="col-6 display-7">
											{{payment.NAME}}
											<span class="text-success" v-if="payment.IS_PAID">оплачено</span>
										</div>
										<div class="col-6 text-right">
											<div class="display-7" style="font-weight: 400;">{{payment.SUM | formatPrice}}</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="alert alert-info" v-html="data.ORDER.PAYMENTS[0].DESCRIPTION"></div>
							<div v-if="data.ORDER.PAYMENTS[0].ACTION_FILE.length > 0 && !data.ORDER.PAYMENTS[0].NEW_WINDOW" v-html="data.ORDER.PAYMENTS[0].BUFFERED_OUTPUT">

							</div>
						</div>
					</div>
				</div>
				<div v-else>
					<span class="display-6 text-success">Заказ оплачен</span>
				</div>
			</div>
			<div class="col-12" v-else>
				<button class="btn btn-warning" @click="recalculateOrder">Пересчитать стоимость заказа</button>
			</div>

			<div class="col-12 mt-4">
				Подробную информацию о заказе можно посмотреть в <a :href="'/personal/order/?ORDER_ID=' + data.ORDER.ID">персональном разеле</a>
			</div>
		</div>

	</div>
</section>
