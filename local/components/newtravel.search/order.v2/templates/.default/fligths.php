<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CTourOrder $component */

?>

<section>
	<div class="container py-5 bg-light">
		<div class="row">
			<div class="col-12">
				<div class="page-heading text-center">
					<h2 class="display-5">Информация о рейсах</h2>
				</div>
			</div>
		</div>
		<div class="row" v-if="data.TOURDATA.DETAIL !== undefined">
			<div class="col-12 text-center display-7">
				<div v-if="data.TOURDATA.DETAIL.flights.length > 0">
					<div class="row">
						<div class="col-12 col-md-6 mb-3" v-for="flight in data.TOURDATA.DETAIL.flights" v-if="flight.forward.length > 0">
							<div class="h3">Туда</div>
							{{flight.dateforward}} - {{flight.forward[0].departure.port.name}}({{flight.forward[0].departure.port.id}}), {{flight.forward[0].departure.time}}
							<i class="fa fa-arrow-circle-right text-success"></i>
							{{flight.forward[0].arrival.port.name}}({{flight.forward[0].arrival.port.id}}), {{flight.forward[0].arrival.time}}
						</div>
						<div class="col-12 col-md-6 mb-3" v-for="flight in data.TOURDATA.DETAIL.flights" v-if="flight.backward.length > 0">
							<div class="h3">Обратно</div>
							{{flight.datebackward}} - {{flight.backward[0].departure.port.name}}({{flight.backward[0].departure.port.id}}), {{flight.backward[0].departure.time}}
							<i class="fa fa-arrow-circle-right text-success"></i>
							{{flight.backward[0].arrival.port.name}}({{flight.backward[0].arrival.port.id}}), {{flight.backward[0].arrival.time}}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div v-else>
			<div class="alert alert-warning">
				<strong>Внимание! Не удалось получить данные о перелете</strong> <br>
				Рейс не определился, цена может быть не актуальной. <br>
				Просим связаться с менеджером и уточнить стоимость, перед заказом данного тура
			</div>
		</div>
	</div>
</section>

