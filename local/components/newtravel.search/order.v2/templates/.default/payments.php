<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CTourOrder $component */

?>

<section>
	<div class="container py-5 bg-light">
		<div class="row">
			<div class="col-12">
				<div class="page-heading text-center">
					<h2 class="display-5 mb-4">Способы оплаты</h2>
				</div>
			</div>
		</div>

		<div class="pay-systems">
			<div class="other-pay-systems" v-if="!showCreditPaySystems">
				<div class="row">
					<div class="col-12 col-md-4" v-for="(paysystem, index) in data.ORDER.PAY_SYSTEMS.OTHER">
						<div class="card p-3 mb-3" :class="{'border-primary' : paysystem.ID === data.SELECTED_PAYSYSTEM}"
						     @click="changePaySystem(paysystem.ID)"
						     @mouseover="paySystemDescription = data.ORDER.PAY_SYSTEMS.OTHER[paysystem.ID].DESCRIPTION">
							<div class="row d-flex align-items-center">
								<div class="col-3">
									<img class="img-thumbnail img-responsive" :src="paysystem.LOGO_PATH" alt="">
								</div>
								<div class="col-9">
									<div class="display-7" style="font-weight: 400;">{{paysystem.NAME}}</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col d-flex justify-content-between">
						<button type="button" class="btn btn-primary btn-lg" @click="doSplitPayment">
							<span v-if="data.SPLIT_PAYMENT_IS_ACTIVE === 'N'" data-container="body" data-toggle="popover" data-placement="bottom" data-trigger="hover">
								Хочу разделить платеж по 50%
							</span>
							<span v-if="data.SPLIT_PAYMENT_IS_ACTIVE === 'Y'">Не хочу делить платеж</span>
						</button>
						<button type="button" class="btn btn-primary btn-lg" @click="showCreditPaySystems = true; changePaySystem(17)">Хочу оформить тур в кредит или рассрочку</button>
					</div>
				</div>

				<div class="alert alert-warning mt-3" v-if="data.SPLIT_PAYMENT_IS_ACTIVE === 'Y'">
					Вы разделили платеж. Изменить способ оплаты или оплатить второй платеж Вы сможете в личном кабиенете, после оформления заказа.
					<span style="font-weight: bold;">Просим учесть,</span> что сумма второго платежа будет пересчитана по курсу валюты тура, на день оплаты!
				</div>

			</div>

			<div class="credit-pay-systems" v-if="showCreditPaySystems">
				<div class="row">
					<div class="col-12 col-md-6" v-for="(paysystem, index) in data.ORDER.PAY_SYSTEMS.CREDIT">
						<div class="card p-3 mb-3" :class="{'border-primary' : paysystem.ID === data.SELECTED_PAYSYSTEM}"
						     @click="changePaySystem(paysystem.ID)"
						     @mouseover="paySystemDescription = data.ORDER.PAY_SYSTEMS.CREDIT[paysystem.ID].DESCRIPTION">
							<div class="row d-flex align-items-center">
								<div class="col-3">
									<img class="img-thumbnail img-responsive" :src="paysystem.LOGO_PATH" style="height: 50px;" alt="">
								</div>
								<div class="col-9">
									<div class="display-7" style="font-weight: 400;">{{paysystem.NAME}}</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row text-left text-md-right">
					<div class="col">
						<button type="button" class="btn btn-primary btn-lg" @click="showCreditPaySystems = false; changePaySystem(1)">Выбрать другие способы оплаты</button>
					</div>
				</div>
			</div>
			<div class="alert alert-info mt-3" v-html="paySystemDescription" v-if="paySystemDescription"></div>
		</div>

		<input type="hidden" name="payment_id" v-model="data.SELECTED_PAYSYSTEM">
		<input type="hidden" name="split_payment" v-model="data.SPLIT_PAYMENT_IS_ACTIVE">

</section>
