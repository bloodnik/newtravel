<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CTourOrder $component */
?>

<section class="py-5" id="tourists-props-section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-heading text-center">
					<h2 class="display-5">Паспортные данные туристов</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="alert alert-warning d-flex align-items-center">
					<span><i class="fa fa-warning fa-2x"></i></span>
					<span class="ml-3">
						Уважаемые туристы, просим внимательно заполнять Ваши паспотные данные.
						По этим данным будет оформлен договор.
						<hr>
						Поля отмеченные "<span class="text-danger h4">*</span>" обязательны к заполнению, это позволит нам оперативнее
						забронировать для Вас тур. Оставшиеся данные наш менеджер уточнит у Вас по телефону
					</span>
				</div>
			</div>
		</div>

		<div class="row" v-if="data.ERRORS.TOURIST_PROPS_INVALID">
			<div class="col-12">
				<div class="alert alert-danger d-flex align-items-center">
					<span><i class="fa fa-warning fa-2x"></i></span>
					<span class="ml-3">
						Упс, Вы заполнили не все обязательные поля необходимые для бронирования тура
					</span>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="card mb-3" v-for="adult in parseInt(data.TOURDATA.adults)">
					<h4 class="card-header bg-primary text-white">
						{{adult | numberToText}} турист
						<div class="btn-group" role="group" aria-label="Basic example">
							<button type="button" class="btn btn-light text-primary" @click="setTouristSex(adult, 'М')" :class="{active: tourists['TOURIST_SEX_' + adult] == 'М'}">М</button>
							<button type="button" class="btn btn-light text-primary" @click="setTouristSex(adult, 'Ж')" :class="{active: tourists['TOURIST_SEX_' + adult] == 'Ж'}">Ж</button>
						</div>
					</h4>
					<div class="card-body">
						<div class="form-row mb-3">
							<div class="col-12 col-md-4">
								<label>Имя <span class="text-danger">*</span></label>
								<input type="text" class="form-control form-control-lg" :name="'TOURIST_NAME_' + adult" v-model="tourists['TOURIST_NAME_' + adult]" placeholder="PETR" required>
							</div>
							<div class="col-12 col-md-4">
								<label>Фамилия <span class="text-danger">*</span></label>
								<input type="text" class="form-control form-control-lg" :name="'TOURIST_LASTNAME_' + adult" v-model="tourists['TOURIST_LASTNAME_' + adult]" placeholder="PETROV" required>
							</div>
							<div class="col-12 col-md-4">
								<label>Дата рождения <span class="text-danger">*</span></label>
								<input type="text" class="form-control form-control-lg" v-mask="'##.##.####'" :name="'TOURIST_BIRTH_' + adult" v-model="tourists['TOURIST_BIRTH_' + adult]" placeholder="01.01.1988" required>
							</div>
						</div>
						<div class="form-row mb-3">
							<div class="col-12 col-md-4">
								<label>Серия и номер паспорта</label>
								<input type="text" class="form-control form-control-lg passport" v-mask="'## #######'" :name="'TOURIST_PASSPORT_' + adult" v-model="tourists['TOURIST_PASSPORT_' + adult]" placeholder="00 0000000">
							</div>
							<div class="col-12 col-md-4">
								<label>Срок действия</label>
								<input type="text" class="form-control form-control-lg" v-mask="'##.##.####'" :name="'TOURIST_VALID_' + adult" v-model="tourists['TOURIST_VALID_' + adult]" placeholder="31.12.2025">
							</div>
							<div class="col-12 col-md-4">
								<label>Гражданство</label>
								<input type="text" class="form-control form-control-lg" :name="'TOURIST_NATION_' + adult" v-model="tourists['TOURIST_NATION_' + adult]" placeholder="Russian Federation">
							</div>
						</div>
					</div>
				</div>


				<div class="row" v-if="data.ERRORS.CHILD_PROPS_INVALID" id="child-props-section">
					<div class="col-12">
						<div class="alert alert-danger d-flex align-items-center">
							<span><i class="fa fa-warning fa-2x"></i></span>
							<span class="ml-3"> Упс, Вы заполнили не все обязательные поля необходимые для бронирования тура </span>
						</div>
					</div>
				</div>

				<div class="card mb-3" v-if="parseInt(data.TOURDATA.child) > 0" v-for="child in parseInt(data.TOURDATA.child)">
					<h4 class="card-header bg-warning text-white">{{child | numberToText}} ребенок</h4>
					<div class="card-body">
						<div class="form-row mb-3">
							<div class="col-12 col-md-4">
								<label>Имя <span class="text-danger">*</span></label>
								<input type="text" class="form-control form-control-lg" :name="'CHILD_NAME_' + child" v-model="tourists['CHILD_NAME_' + child]" placeholder="PETR" required>
							</div>
							<div class="col-12 col-md-4">
								<label>Фамилия <span class="text-danger">*</span></label>
								<input type="text" class="form-control form-control-lg" :name="'CHILD_LASTNAME_' + child" v-model="tourists['CHILD_LASTNAME_' + child]" placeholder="PETROV" required>
							</div>
							<div class="col-12 col-md-4">
								<label>Дата рождения <span class="text-danger">*</span></label>
								<input type="text" class="form-control form-control-lg" v-mask="'##.##.####'" :name="'CHILD_BIRTH_' + child" v-model="tourists['CHILD_BIRTH_' + child]" placeholder="01.01.1988" required>
							</div>
						</div>
						<div class="form-row mb-3">
							<div class="col-12 col-md-4">
								<label>Серия и номер паспорта</label>
								<input type="text" class="form-control form-control-lg passport" v-mask="'## #######'" :name="'CHILD_PASSPORT_' + child" v-model="tourists['CHILD_PASSPORT_' + child]" placeholder="00 0000000">
							</div>
							<div class="col-12 col-md-4">
								<label>Срок действия</label>
								<input type="text" class="form-control form-control-lg" v-mask="'##.##.####'" :name="'CHILD_VALID_' + child" v-model="tourists['CHILD_VALID_' + child]" placeholder="31.12.2025">
							</div>
							<div class="col-12 col-md-4">
								<label>Гражданство</label>
								<input type="text" class="form-control form-control-lg" :name="'CHILD_NATION_' + child" v-model="tourists['CHILD_NATION_' + child]" placeholder="Russian Federation">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container">
	<hr>
</div>