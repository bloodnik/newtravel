'use strict';

$(document).ready(function () {

    window.orderApp = new Vue({
        el: '#order-page',
        data: {
            data: orderData,
            paySystemDescription: "", //orderData.ORDER.PAY_SYSTEMS.OTHER[orderData.SELECTED_PAYSYSTEM].DESCRIPTION,//orderData.ORDER.PAY_SYSTEMS.OTHER[orderData.SELECTED_PAYSYSTEM].DESCRIPTION,
            agreement: true,
            showCreditPaySystems: false,
            tourists: {},
            dogovor: ""
        },
        created: function () {
            var self = this;

            if (!self.data.DONE) {
                if (self.data.TOURDATA !== undefined) {
                    for (var i = 1; i <= parseInt(this.data.TOURDATA.adults); i++) {
                        self.$set(self.tourists, "TOURIST_NAME_" + i, "");
                        self.$set(self.tourists, "TOURIST_LASTNAME_" + i, "");
                        self.$set(self.tourists, "TOURIST_BIRTH_" + i, "");
                        self.$set(self.tourists, "TOURIST_PASSPORT_" + i, "");
                        self.$set(self.tourists, "TOURIST_VALID_" + i, "");
                        self.$set(self.tourists, "TOURIST_NATION_" + i, "");
                        self.$set(self.tourists, "TOURIST_SEX_" + i, "М");
                    }

                    for (var i = 1; i <= parseInt(this.data.TOURDATA.child); i++) {
                        self.$set(self.tourists, "CHILD_NAME_" + i, "");
                        self.$set(self.tourists, "CHILD_LASTNAME_" + i, "");
                        self.$set(self.tourists, "CHILD_BIRTH_" + i, "");
                        self.$set(self.tourists, "CHILD_PASSPORT_" + i, "");
                        self.$set(self.tourists, "CHILD_VALID_" + i, "");
                        self.$set(self.tourists, "CHILD_NATION_" + i, "");
                        self.$set(self.tourists, "CHILD_SEX_" + i, "");
                    }
                }
            }
        },

        mounted: function () {
            var self = this;

            $('[data-toggle="tooltip"]').tooltip();

            $('span[data-toggle=popover]').popover({
                content: "Включить разделение платежа по 50%. Просим учесть, что сумма второго платежа будет пересчитана по курсу валюты тура, на день оплаты!"
            });

            //Скрипт подключения онлайн кредита от Хоум Кредит
            if(self.data.SELECTED_PAYSYSTEM === "24") {
                KreditOtdel.Widget.attach({
                    widgetType: KreditOtdel.Widget.widgetTypes.VsegdaDa,
                    elementId: 'vsegda-da-buy',
                    autoStart: true,
                    data: {
                        hostname: 'умныетуристы.рф', //hostname replaced widget-test.l-kredit.ru
                        shopId: 123,
                        goods: [{name: "Туристическая путевка", price: self.data.ORDER.PRICE, count: 1}],
                        lastName: '',
                        firstName: self.data.ORDER.PROPS.SHOW.CONTACT_PERSON.VALUE,
                        middleName: '',
                        phone: self.data.ORDER.PROPS.SHOW.PHONE.VALUE,
                        email: self.data.ORDER.PROPS.SHOW.EMAIL.VALUE,
                        discount: 0,
                        orderId: self.data.ORDER.ID
                    }
                });
            }


            if (!self.data.DONE) {
                if (self.data.TOURDATA !== undefined) {
                    if (self.data.TOURDATA.operatorlink !== undefined) {
                        self.data.ORDER.PROPS.HIDDEN.OPERATOR_LINK.VALUE = self.data.TOURDATA.operatorlink;
                    }

                    if (self.data.TOURDATA.DETAIL !== undefined) {
                        var flightData,
                            flights = this.data.TOURDATA.DETAIL.flights[0];

                        flightData = "Туда: " + flights.dateforward + " - " + flights.forward[0].departure.port.name + "(" + flights.forward[0].departure.port.id + ") ," + flights.forward[0].departure.time;
                        flightData += " -> " + flights.forward[0].arrival.port.name + "(" + flights.forward[0].arrival.port.id + ") ," + flights.forward[0].arrival.time + " | Авиа - " + flights.forward[0].company.name + "(" + flights.forward[0].number + ")";
                        flightData += " Обратно: " + flights.datebackward + " - " + flights.backward[0].departure.port.name + "(" + flights.backward[0].departure.port.id + ") ," + flights.backward[0].departure.time;
                        flightData += " -> " + flights.backward[0].arrival.port.name + "(" + flights.backward[0].arrival.port.id + ") ," + flights.backward[0].arrival.time + " | Авиа - " + flights.backward[0].company.name + "(" + flights.backward[0].number + ")";

                        self.data.ORDER.PROPS.HIDDEN.FLIGHT_DATA.VALUE = flightData;
                    }
                }
            }
        },
        methods: {
            createTouristsPropString: function () {
                var self = this;

                if (!self.data.DONE) {
                    if (self.data.TOURDATA !== undefined) {
                        for (var i = 1; i <= parseInt(self.data.TOURDATA.adults); i++) {
                            self.data.ORDER.PROPS.HIDDEN['TOURIST_' + i].VALUE = "Пол: " + self.tourists['TOURIST_SEX_' + i]
                                + ", Имя: " + self.tourists['TOURIST_NAME_' + i]
                                + ", Фамилия: " + self.tourists['TOURIST_LASTNAME_' + i]
                                + ", Дата рождения: " + self.tourists['TOURIST_BIRTH_' + i]
                                + ", Паспорт: " + self.tourists['TOURIST_PASSPORT_' + i]
                                + ", Срок паспорта: " + self.tourists['TOURIST_VALID_' + i]
                                + ", Граждансто: " + self.tourists['TOURIST_NATION_' + i]
                        }

                        for (var i = 1; i <= parseInt(self.data.TOURDATA.child); i++) {
                            self.data.ORDER.PROPS.HIDDEN['CHILD_' + i].VALUE = "Пол: " + self.tourists['CHILD_SEX_' + i]
                                + ", Имя: " + self.tourists['CHILD_NAME_' + i]
                                + ", Фамилия: " + self.tourists['CHILD_LASTNAME_' + i]
                                + ", Дата рождения: " + self.tourists['CHILD_BIRTH_' + i]
                                + ", Паспорт: " + self.tourists['CHILD_PASSPORT_' + i]
                                + ", Срок паспорта: " + self.tourists['CHILD_VALID_' + i]
                                + ", Граждансто: " + self.tourists['CHILD_NATION_' + i]
                        }
                    }
                }
            },

            setTouristSex : function (adult, sex) {
                this.$set(this.tourists, 'TOURIST_SEX_' + adult, sex);
            },

            //Применяем купон
            addCoupon : function () {
                this.sendForm({'coupon': this.data.COUPON.USED_COUPON, 'addCoupon' : 'Y'});
            },

            //Очищаем купон
            clearCoupon : function () {
                this.data.COUPON.USED_COUPON = "";
                this.sendForm({'clearCoupon': "Y"});
            },


            changePaySystem: function (paysystem_id) {
                this.data.SELECTED_PAYSYSTEM = paysystem_id;
                this.sendForm();
            },

            changeDelivery: function (delivery_id) {
                this.data.SELECTED_DELIVERY = delivery_id;
                this.sendForm();
            },

            //Разделяем оплату
            doSplitPayment: function () {
                this.sendForm({'split_payment': this.data.SPLIT_PAYMENT_IS_ACTIVE === 'Y' ? "N" : "Y"});
            },

            //Пересчитываем стоимость заказа
            recalculateOrder: function () {
                this.sendForm({'recalculate': "Y", order_id: this.data.ORDER.ID, 'is_ajax' : 'Y'});
            },


            sendForm: function (newData = {}) {
                var self = this;
                setTimeout(function () {
                    var formData = self.getFormData();

                    _.forEach(newData, function (value, key) {
                        formData[key] = value
                    });

                    $('.preload').show();
                    $.post(self.POST_FORM_URI, formData, function (response) {
                        self.data = response;
                        $('.preload').hide();
                    });
                }, 100);
            },


            submitOrder: function () {
                var self = this;
                self.createTouristsPropString();

                setTimeout(function () {
                    var formData = self.getFormData();

                    formData.save = 'Y';
                    $('.preload').show();
                    $.post(self.POST_FORM_URI, formData, function (response) {
                        $('.preload').hide();

                        self.data = response;
                        if (self.data.ERRORS['USER_PROPS_INVALID']) {
                            $('html, body').stop().animate({
                                scrollTop: $('#user-props-section').offset().top - 160
                            }, 400);
                        } else if (self.data.ERRORS['TOURIST_PROPS_INVALID']) {
                            $('html, body').stop().animate({
                                scrollTop: $('#tourists-props-section').offset().top - 160
                            }, 400);
                        } else if (self.data.ERRORS['CHILD_PROPS_INVALID']) {
                            $('html, body').stop().animate({
                                scrollTop: $('#child-props-section').offset().top - 160
                            }, 400);
                        }

                        if(self.data.DONE && parseInt(self.data.ORDER.ID) > 0){
                            document.location.href= self.data.POST_FORM_URI + '?order_id='+ self.data.ORDER.ID;
                        }
                    });
                }, 100);
            },

            getFormData: function () {
                var from = $('form[name=ORDER_FORM]');
                var unindexed_array = from.serializeArray();
                var indexed_array = {};

                $.map(unindexed_array, function (n, i) {
                    indexed_array[n['name']] = n['value'];
                });
                return indexed_array;
            },

            openDogovorModal: function () {
                var self = this;

                $('#dogovorModal .modal-body').load(location.protocol + '//' + location.hostname + '/about/dogovor/',function(){
                    var html = $(this).html();

                    html = html.replace(/#date_create#/g, moment().format('DD.MM.YYYY') + " г.");
                    html = html.replace(/#FIO#/g, self.data.ORDER.PROPS.SHOW.CONTACT_PERSON.VALUE);
                    html = html.replace(/#PHONE#/g, self.data.ORDER.PROPS.SHOW.PHONE.VALUE);
                    html = html.replace(/#EMAIL#/g, self.data.ORDER.PROPS.SHOW.EMAIL.VALUE);
                    html = html.replace(/#COUNTRY#/g, self.data.TOURDATA.countryname);
                    html = html.replace(/#DATE_FROM#/g, self.data.TOURDATA.flydate);
                    html = html.replace(/#DATE_TO#/g, moment(self.data.TOURDATA.flydate, 'DD.MM.YYYY').add(self.data.TOURDATA.nights, 'days').format('DD.MM.YYYY'));
                    html = html.replace(/#HOTEL#/g, self.data.TOURDATA.hotelname + ' ' + self.data.TOURDATA.hotelstars + '*');
                    html = html.replace(/#ROOM#/g, self.data.TOURDATA.room);
                    html = html.replace(/#PLACEMENT#/g, self.data.TOURDATA.placement);
                    html = html.replace(/#MEAL#/g, self.data.TOURDATA.meal);
                    html = html.replace(/#PRICE_RUB#/g, self.data.ORDER.PRICE);
                    html = html.replace(/#UE_PRICE#/g, self.data.ORDER.BASKET.UE_PRICE);

                    var touristsTable = "<table class='table table-responsive table-bordered' border=\"1\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\"><tr><td>Ф.И.О.</td><td>Статус (м, ж, реб)</td><td>Дата рождения</td><td>Паспортные данные</td></tr>";

                    for (var i = 1; i <= parseInt(self.data.TOURDATA.adults); i++) {

                        touristsTable += "<tr>";
                        touristsTable += "<td>"+ self.tourists['TOURIST_NAME_' + i] + " " + self.tourists['TOURIST_LASTNAME_' + i] + "</td>";
                        touristsTable += "<td>"+ self.tourists['TOURIST_SEX_' + i] + "</td>";
                        touristsTable += "<td>"+ self.tourists['TOURIST_BIRTH_' + i] + "</td>";
                        touristsTable += "<td>"+ self.tourists['TOURIST_PASSPORT_' + i] + ", " + self.tourists['TOURIST_VALID_' + i] + ", " + self.tourists['TOURIST_NATION_' + i] + "</td>";
                        touristsTable += "</tr>";
                    }

                    for (var i = 1; i <= parseInt(self.data.TOURDATA.child); i++) {

                        touristsTable += "<tr>";
                        touristsTable += "<td>"+ self.tourists['CHILD_NAME_' + i] + " " + self.tourists['CHILD_LASTNAME_' + i] + "</td>";
                        touristsTable += "<td>"+ self.tourists['CHILD_SEX_' + i] + "</td>";
                        touristsTable += "<td>"+ self.tourists['CHILD_BIRTH_' + i] + "</td>";
                        touristsTable += "<td>"+ self.tourists['CHILD_PASSPORT_' + i] + ", " + self.tourists['CHILD_VALID_' + i] + ", " + self.tourists['CHILD_NATION_' + i] + "</td>";
                        touristsTable += "</tr>";
                    }
                    touristsTable += "</table>";
                    html = html.replace(/#TOURISTS_1#/g, touristsTable);

                    var touristsTable2 = "<table class='table table-responsive table-bordered' border=\"1\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\"><tr><td>Ф.И.О.</td><td>Статус (м, ж, реб)</td><td>Дата рождения</td><td>Паспортные данные</td><td>Подпись</td></tr>";

                    for (var i = 1; i <= parseInt(self.data.TOURDATA.adults); i++) {

                        touristsTable2 += "<tr>";
                        touristsTable2 += "<td>"+ self.tourists['TOURIST_NAME_' + i] + " " + self.tourists['TOURIST_LASTNAME_' + i] + "</td>";
                        touristsTable2 += "<td>"+ self.tourists['TOURIST_SEX_' + i] + "</td>";
                        touristsTable2 += "<td>"+ self.tourists['TOURIST_BIRTH_' + i] + "</td>";
                        touristsTable2 += "<td>"+ self.tourists['TOURIST_PASSPORT_' + i] + ", " + self.tourists['TOURIST_VALID_' + i] + ", " + self.tourists['TOURIST_NATION_' + i] + "</td>";
                        touristsTable2 += "<td></td>";
                        touristsTable2 += "</tr>";
                    }

                    for (var i = 1; i <= parseInt(self.data.TOURDATA.child); i++) {

                        touristsTable2 += "<tr>";
                        touristsTable2 += "<td>"+ self.tourists['CHILD_NAME_' + i] + " " + self.tourists['CHILD_LASTNAME_' + i] + "</td>";
                        touristsTable2 += "<td>"+ self.tourists['CHILD_SEX_' + i] + "</td>";
                        touristsTable2 += "<td>"+ self.tourists['CHILD_BIRTH_' + i] + "</td>";
                        touristsTable2 += "<td>"+ self.tourists['CHILD_PASSPORT_' + i] + ", " + self.tourists['CHILD_VALID_' + i] + ", " + self.tourists['CHILD_NATION_' + i] + "</td>";
                        touristsTable2 += "<td></td>";
                        touristsTable2 += "</tr>";
                    }
                    touristsTable2 += "</table>";
                    html = html.replace(/#TOURISTS_2#/g, touristsTable2);

                    self.dogovor = html;

                    $(this).html(html);

                    $('#dogovorModal').modal({show:true});
                });
            },

            printDogovor: function() {
                var mywindow = window.open('', 'PRINT', 'height=400,width=600');

                mywindow.document.write('<html><head><title>Договор оферта</title>');
                mywindow.document.write('</head><body >');
                mywindow.document.write(this.dogovor);
                mywindow.document.write('</body></html>');

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10*/

                mywindow.print();
                mywindow.close();

                return true;
            }

        },

        filters:
            {
                formatPrice: function (value) {
                    return parseInt(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
                }
                ,

                getOldPrice: function (value) {
                    value = parseInt(value) + parseInt(value * 0.11);
                    return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
                }
                ,

                getCreditPrice: function (value) {
                    value = (parseInt(value) + parseInt(value * 0.0717)) / 6;
                    return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
                }
                ,

                displayNights: function (value) {
                    if (!value) {
                        return "";
                    } else {
                        var titles = ['ночь', 'ночи', 'ночей'];
                        value = parseInt(value);
                        return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                    }
                }
                ,


                numberToText: function (value) {
                    switch (value) {
                        case 1:
                            return 'Первый';
                            break;
                        case 2:
                            return 'Второй';
                            break;
                        case 3:
                            return 'Третий';
                            break;
                        case 4:
                            return 'Четвертый';
                            break;
                        case 5:
                            return 'Пятый';
                            break;
                        case 6:
                            return 'Шестой';
                            break;
                        case 7:
                            return 'Седьмой';
                            break;
                    }

                }
                ,

                pluralText: function (value) {
                    var titles = ['тур', 'тура', 'туров'];
                    value = parseInt(value);
                    return value + " " + titles[(value % 10 === 1 && value % 100 !== 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                }
            }
    })
})
;