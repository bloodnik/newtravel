.<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CTourOrder $component */

?>

<section class="">
	<div class="container py-5 bg-light">
		<div class="row">
			<div class="col-12 col-md-8">
				<div class="display-6 text-success mb-3">Итого</div>

				<div class="row d-flex align-items-center">
					<dt class="col-5 h4 text-primary">Сумма к оплате</dt>
					<dd class="col-7 display-6 text-primary" style="font-weight: 400;">{{data.ORDER.PRICE | formatPrice}}</dd>

					<div class="col-12" v-if="data.SPLIT_PAYMENT_IS_ACTIVE === 'Y'">
						<div class="row d-flex align-items-center">
							<div class="col-12">
								<small>Вы разделили платеж:</small>
							</div>
							<div class="col-12">
								<div class="row d-flex align-items-center" v-for="(summ, index) in data.PAYMENTS">
									<dt class="col-5 h5">{{index+1 | numberToText}} платеж</dt>
									<dd class="col-7 display-7" >{{summ | formatPrice}}</dd>
								</div>
							</div>

						</div>
					</div>

					<div class="col-12" v-if="data.ORDER.DISCOUNT_PRICE != 0">
						<div class="row d-flex align-items-center">
							<div class="col-12">
								<small>В том числе:</small>
							</div>
							<dt class="col-5 h5"><span v-if="data.ORDER.DISCOUNT_PRICE < 0">Скидка</span><span v-if="data.ORDER.DISCOUNT_PRICE > 0">Наценка за оплату картой</span></dt>
							<dd class="col-7 display-7" >{{data.ORDER.DISCOUNT_PRICE | formatPrice}}</dd>
						</div>
					</div>
				</div>
			</div>

			<div class="col-12 col-md-4">
				<div class="mb-3">
					<div class="display-6 text-success mb-3">У меня есть промокод</div>
					<div class="input-group">
						<input type="text" class="form-control" v-model="data.COUPON.USED_COUPON" name="coupon" placeholder="PROMO">
						<span class="input-group-addon" data-toggle="tooltip" data-placement="top" title="Отменить купон" v-if="data.COUPON.IS_ENTERED" @click="clearCoupon"><i class="fa fa-close"></i></span>
						<span class="input-group-btn"><button class="btn btn-primary" type="button" @click="addCoupon"><i class="fa fa-chevron-right"></i></button></span>
					</div>
					<small class="text-success" v-if="data.COUPON.IS_ENTERED">Купон {{data.COUPON.USED_COUPON}} успешно прменен</small>
					<small class="text-success" v-if="data.COUPON.CLEARED">Купон успешно отменен</small>
					<small class="text-danger" v-if="data.ERRORS.COUPON">{{data.ERRORS.COUPON}}</small>
				</div>
				<div class="">
					<div class="display-6 text-success mb-3">Пожелания к заказу</div>
					<div class="input-group">
						<textarea class="form-control" name="USER_DESCRIPTION"></textarea>
					</div>
				</div>

			</div>
		</div>
		<hr>
		<div class="row mt-4">
			<div class="col-12">
				<div class="alert alert-danger" v-if="data.ERRORS.REGISTER" v-html="data.ERRORS.REGISTER"></div>
			</div>
			<div class="col-12 col-md-6">
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" value="" v-model="agreement">
						Я ознакомлен с условиями <a class="dogovorOpen" @click="openDogovorModal" target="_blank" href="javascript:void(0)">договора оферты</a> на туристическое обслуживаение и согласен с условиями обработки персональных данных
					</label>
				</div>
			</div>
			<div class="col-12 col-md-6 text-center text-md-right">
				<button class="btn btn-lg btn-success ld-over-inverse" :disabled="!agreement" name="SUBMIT_ORDER" value="Y" type="submit" @click.prevent="submitOrder">
					ОФОРМИТЬ
					<div class="ld ld-ring ld-spin"></div>
				</button>
			</div>
			<input type="hidden" name="is_ajax" value="Y">
		</div>
</section>

<!-- Modal -->
<div class="modal fade" id="dogovorModal" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" @click="printDogovor">Печать</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
</div>