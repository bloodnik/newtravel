<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CTourOrder $component */

?>

<section>
	<div class="container py-5 bg-light">
		<div class="row">
			<div class="col-12">
				<div class="page-heading text-center">
					<h2 class="display-5 mb-4">Способ получения документов</h2>
				</div>
			</div>
		</div>

		<div class="pay-systems">
			<div>
				<div class="row">
					<div class="col-12 col-md-4" v-for="(delivery, id) in data.ORDER.DELIVERY">
						<div class="card p-3 mb-3" :class="{'border-primary' : id === data.SELECTED_DELIVERY}"
						     @click="changeDelivery(id)">
							<div class="row d-flex align-items-center">
								<div class="col">
									<div class="display-7" style="font-weight: 400;">{{delivery.NAME}}</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<input type="hidden" name="delivery_id" v-model="data.SELECTED_DELIVERY">

</section>
