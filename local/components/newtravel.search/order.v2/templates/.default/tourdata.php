<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CTourOrder $component */

?>
<section class="py-5" v-if="data.TOURDATA !== undefined">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-3 mb-3 d-flex align-items-center">
				<img :src="data.TOURDATA.hotelpicturemedium" :alt="data.TOURDATA.hotelname">
			</div>

			<div class="col-12 col-md-9">
				<div class="text-muted">{{data.TOURDATA.tourname}}</div>
				<h1 class="display-6 text-primary mb-3">{{data.TOURDATA.hotelname}} {{data.TOURDATA.hotelstars}} * <span class="text-muted small">({{data.TOURDATA.hotelregionname}}, {{data.TOURDATA.countryname}})</span></h1>

				<div class="row">
					<div class="col-12 display-7 text-info pb-3">Детали тура:</div>
					<div class="col-12 col-md-6">
						<div class="row">
							<dt class="col-5">Город вылета</dt>
							<dd class="col-7">{{data.TOURDATA.departurename}}</dd>

							<dt class="col-5">Туроператор</dt>
							<dd class="col-7">{{data.TOURDATA.operatorname}}</dd>

							<dt class="col-5">Вылет</dt>
							<dd class="col-7">{{data.TOURDATA.flydate}}</dd>

							<dt class="col-5">Туристы</dt>
							<dd class="col-7">{{data.TOURDATA.placement}}</dd>
						</div>
					</div>

					<div class="col-12 col-md-6">
						<div class="row">
							<dt class="col-5">Ночей</dt>
							<dd class="col-7">{{data.TOURDATA.nights | displayNights}}</dd>

							<dt class="col-5">Номер</dt>
							<dd class="col-7">{{data.TOURDATA.room}}</dd>

							<dt class="col-5">Размещение</dt>
							<dd class="col-7">{{data.TOURDATA.placement}}</dd>

							<dt class="col-5">Питание</dt>
							<dd class="col-7">{{data.TOURDATA.meal}}</dd>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
<section class="py-3 bg-light-blue">
	<div class="container">

		<div class="row">
			<div class="col-12 text-center text-white">
				<div class="display-6">Стоимость тура</div><div class="display-5 text-dark-blue" style="font-weight: 500;">{{data.ORDER.PRICE | formatPrice}}</div>
			</div>
		</div>
	</div>
</section>