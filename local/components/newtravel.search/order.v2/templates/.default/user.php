<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CTourOrder $component */

?>

<section class="py-5" id="user-props-section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-heading text-center">
					<h2 class="display-5 mb-4 ">Данные заказчика</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="form-row">
					<div class="col-12 col-md-4" v-for="prop in data.ORDER.PROPS.SHOW">
						<label>{{prop.NAME}}</label>
						<input type="text" class="form-control form-control-lg" v-model="data.ORDER.PROPS.SHOW[prop.CODE].VALUE" :name="prop.CODE" :class="{'border-danger' : prop.HAS_ERROR}">
						<small class="form-text text-danger" v-if="prop.HAS_ERROR">{{data.ORDER.PROPS_MESSAGE[prop.CODE].ERROR}}</small>
						<small class="form-text text-muted">{{data.ORDER.PROPS_MESSAGE[prop.CODE].DESCRIPTION}}</small>
					</div>
				</div>
			</div>
		</div>

		<input type="hidden" v-for="prop in data.ORDER.PROPS.HIDDEN" v-model="data.ORDER.PROPS.HIDDEN[prop.CODE].VALUE" :name="prop.CODE">
</section>
