<?

use Bitrix\Main\Loader;
use Bitrix\Main\Type\Date;
use Bitrix\Sale\DiscountCouponsManager;
use Bitrix\Sale\Internals\BasketTable;
use Bitrix\Main\Localization\Loc;

if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

class CTourOrder extends CBitrixComponent {

	/**
	 * Наценка за онлайн оплату
	 */
	const ONLINE_PAY_COMISSION = 0.018;

	/**
	 * Скидка за самостоятельность
	 */
	const SELF_DISCOUNT = 0.02;

	/**
	 * @var \Bitrix\Sale\Order
	 */
	public $order;

	/**
	 * @var array
	 */
	protected $errors = [];

	/**
	 * @var array
	 */
	protected $arResponse = [];

	/**
	 * Идентификатор тура(товара) в корзине
	 * @var int
	 */
	protected $basketTourProductId;

	/**
	 * ID тура от турвизора, хранится в свойстве товара TOUR_ID
	 * @var string
	 */
	protected $tourid;

	/**
	 * Применяется ли скидка за самостоятельность, хранится в свойстве товара USE_DISCOUNT
	 * @var bool
	 */
	protected $usediscount;

	/**
	 * Цена в USD
	 * @var bool
	 */
	protected $usd_price;

	/**
	 * Формирование массива свойств заказа
	 * Выделяем в массив свойства для отображения и свойства, которые дложны быть скрыты (будут заполняться автоматический через vuejs)
	 * @return array
	 */
	protected function getAvailableOrderProps() {
		$props       = array("SHOW" => array(), "HIDDEN" => array());
		$hiddenProps = array('TOURIST_1', "TOURIST_2", "TOURIST_3", "TOURIST_4", "TOURIST_5", "CHILD_1", "CHILD_2", "CHILD_3", "FLIGHT_DATA", "OPERATOR_LINK", "RECALCULATE_DATE");
		$shownProps  = array('CONTACT_PERSON', "EMAIL", "PHONE");

		/** @var \Bitrix\Sale\PropertyValue $prop */
		foreach ($this->order->getPropertyCollection() as $prop):
			if (in_array($prop->getField('CODE'), $hiddenProps)) {
				$props['HIDDEN'][ $prop->getField('CODE') ] = array(
					"ID"       => $prop->getPropertyId(),
					"CODE"     => $prop->getField('CODE'),
					"NAME"     => $prop->getName(),
					"VALUE"    => $prop->getValue(),
					"REQUIRED" => $prop->isRequired() ? true : false
				);
			}
			if (in_array($prop->getField('CODE'), $shownProps)) {
				$props['SHOW'][ $prop->getField('CODE') ] = array(
					"ID"        => $prop->getPropertyId(),
					"CODE"      => $prop->getField('CODE'),
					"NAME"      => $prop->getName(),
					"VALUE"     => $prop->getValue(),
					"HAS_ERROR" => false,
					"REQUIRED"  => $prop->isRequired() ? true : false
				);
			}
		endforeach;

		return $props;
	}

	/**
	 * Актуализация тура из турвизора
	 */
	protected function actualizeTourFromTourvisor() {
		if (Loader::includeModule('newtravel.search')) {
			$tv       = new \Newtravel\Search\Tourvisor();
			$tourdata = $tv->getData("actualize", array("tourid" => $this->tourid));

			if (isset($tourdata['error']) && ! isset($tourdata['data'])) {
				$this->errors[]                   = "Изивините, данный тур больше недоступен";
				$this->arResult['TOUR_AVAILABLE'] = false;
			} else {
				$this->arResult['TOURDATA']       = $tourdata['data']['tour'];
				$this->arResult['TOUR_AVAILABLE'] = true;

				//Обновляем цену из полученной актуализации из турвизора
				$this->order->getBasket()->getItemById($this->basketTourProductId)->setPrice(intval($this->arResult['TOURDATA']['price']));

				if ($this->arResult['TOURDATA']['detailavailable'] == "1") {
					$detailtourdata = $tv->getData("actdetail", array("tourid" => $this->tourid));
					if (isset($detailtourdata['iserror']) && ! empty($detailtourdata['iserror'])) {
						$this->errors[] = $detailtourdata['iserror'];
					} else {
						$this->arResult['TOURDATA']['DETAIL'] = $detailtourdata;
					}
				}
			}

		} else {
			$this->errors[] = "Невозможно подключить модуль newtravel.search";
		}
	}

	function __construct($component = null) {
		parent::__construct($component);

		if ( ! Loader::includeModule('sale')) {
			$this->errors[] = 'No sale module';
		};

		if ( ! Loader::includeModule('catalog')) {
			$this->errors[] = 'No catalog module';
		};
	}

	function onPrepareComponentParams($arParams) {
		if (isset($arParams['PERSON_TYPE_ID']) && intval($arParams['PERSON_TYPE_ID']) > 0) {
			$arParams['PERSON_TYPE_ID'] = intval($arParams['PERSON_TYPE_ID']);
		} else {
			if (intval($this->request['payer']['person_type_id']) > 0) {
				$arParams['PERSON_TYPE_ID'] = intval($this->request['payer']['person_type_id']);
			} else {
				$arParams['PERSON_TYPE_ID'] = 1;
			}
		}

		if ( ! isset($arParams['PATH_TO_CATALOG']) && strlen($arParams['PATH_TO_CATALOG']) === 0) {
			$arParams['PATH_TO_CATALOG'] = '/tours/'; //Путь к каталогу, редирект идет туда, если корзина пустая
		}

		if ( ! isset($arParams['DEFAULT_DELIVERY_ID']) && strlen($arParams['DEFAULT_DELIVERY_ID']) === 0) {
			$arParams['DEFAULT_DELIVERY_ID'] = '7'; //Служба доставки по умолчанию
		}

		if ( ! isset($arParams['DEFAULT_PAYMENT_ID']) && strlen($arParams['DEFAULT_PAYMENT_ID']) === 0) {
			$arParams['DEFAULT_PAYMENT_ID'] = '1'; //Плат. система по умолчанию
		}

		if (isset($arParams['IS_AJAX']) && ($arParams['IS_AJAX'] == 'Y' || $arParams['IS_AJAX'] == 'N')) {
			$arParams['IS_AJAX'] = $arParams['IS_AJAX'] == 'Y';
		} else {
			if (isset($this->request['is_ajax']) && ($this->request['is_ajax'] == 'Y' || $this->request['is_ajax'] == 'N')) {
				$arParams['IS_AJAX'] = $this->request['is_ajax'] == 'Y';
			} else {
				$arParams['IS_AJAX'] = false;
			}
		}

		return $arParams;
	}

	/**
	 * Создаем виртуальный заказ
	 */
	protected function createVirtualOrder() {
		global $USER;

		try {
			$siteId = \Bitrix\Main\Context::getCurrent()->getSite();

			/**
			 * Текущая корзина пльзователя
			 * Sale\Basket $basket */
			$basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

			//Если корзина пустая, тогда редирект
			if (count($basket) == 0) {
				LocalRedirect($this->arParams['PATH_TO_CATALOG']);
			}

			//Создаем новый заказ
			$this->order = \Bitrix\Sale\Order::create($siteId, $USER->GetID());
			$this->order->setPersonTypeId($this->arParams['PERSON_TYPE_ID']);
			$this->order->setBasket($basket);

			$this->setOrderProps();

			//================ОТГРУЗКА=====================//
			/* @var $shipmentCollection \Bitrix\Sale\ShipmentCollection */
			$shipmentCollection = $this->order->getShipmentCollection();

			if (intval($this->request['delivery_id']) > 0) {
				$shipment = $shipmentCollection->createItem(
					Bitrix\Sale\Delivery\Services\Manager::getObjectById(
						intval($this->request['delivery_id'])
					)
				);
			} elseif (intval($this->arParams['DEFAULT_DELIVERY_ID']) > 0) {
				$shipment = $shipmentCollection->createItem(
					Bitrix\Sale\Delivery\Services\Manager::getObjectById(
						intval($this->arParams['DEFAULT_DELIVERY_ID'])
					)
				);
			} else {
				$shipment = $shipmentCollection->createItem();
			}

			/** @var $shipmentItemCollection \Bitrix\Sale\ShipmentItemCollection */
			$shipmentItemCollection = $shipment->getShipmentItemCollection();
			$shipment->setField('CURRENCY', $this->order->getCurrency());

			foreach ($this->order->getBasket()->getOrderableItems() as $item) {
				/**
				 * @var $item \Bitrix\Sale\BasketItem
				 * @var $shipmentItem \Bitrix\Sale\ShipmentItem
				 * @var $item \Bitrix\Sale\BasketItem
				 */
				$shipmentItem = $shipmentItemCollection->createItem($item);
				$shipmentItem->setQuantity($item->getQuantity());

				//Перебираем свойства товара(тура), для получения значений свойств товара
				$basketPropRes = \Bitrix\Sale\Internals\BasketPropertyTable::getList(array(
					'filter' => array(
						"BASKET_ID" => $item->getId(),
					),
				));
				while ($property = $basketPropRes->fetch()) {
					if ($property['CODE'] === 'TOUR_ID') {
						$this->tourid              = $property['VALUE'];
						$this->basketTourProductId = $item->getId();
					}
					if ($property['CODE'] === 'USE_DISCOUNT') {
						$this->usediscount = $property['VALUE'] === 'Y';
					}
					if ($property['CODE'] === 'UE_PRICE') {
						$this->usd_price = $property['VALUE'];
					}
				}
			}


			$arDeliveryServiceAll = Bitrix\Sale\Delivery\Services\Manager::getRestrictedObjectsList($shipment);
			/**
			 * @var $deliveryItem \Bitrix\Sale\DeliveryService
			 */
			foreach ($arDeliveryServiceAll as $key => $deliveryItem) {
				$this->arResult["ORDER"]['DELIVERY'][$deliveryItem->getId()]['NAME'] = $deliveryItem->getName();
			}

		} catch (\Exception $e) {
			$this->errors[] = $e->getMessage();
		}
	}

	/**
	 * Получение существующего заказа
	 *
	 * @param $order_id
	 */
	protected function getCreatedOrder($order_id) {
		$this->order = $this->order = \Bitrix\Sale\Order::load($this->request['order_id']);

		foreach ($this->order->getBasket()->getOrderableItems() as $item) {
			/**
			 * @var $item \Bitrix\Sale\BasketItem
			 * @var $shipmentItem \Bitrix\Sale\ShipmentItem
			 * @var $item \Bitrix\Sale\BasketItem
			 */

			//Перебираем свойства товара(тура)
			$basketPropRes = \Bitrix\Sale\Internals\BasketPropertyTable::getList(array(
				'filter' => array(
					"BASKET_ID" => $item->getId(),
				),
			));

			while ($property = $basketPropRes->fetch()) {
				if ($property['CODE'] === 'TOUR_ID') {
					$this->tourid              = $property['VALUE'];
					$this->basketTourProductId = $item->getId();
				}
				if ($property['CODE'] === 'USE_DISCOUNT') {
					$this->usediscount = $property['VALUE'] === 'Y';
				}
			}
		}

		$paymentCollection     = $this->order->getPaymentCollection();
		$arPaySystemServiceAll = [];
		//Список созданных оплат
		/* @var \Bitrix\Sale\Payment $payment */
		foreach ($paymentCollection as $key => $payment) {
			$arPaySystemServices = \Bitrix\Sale\PaySystem\Manager::getListWithRestrictions($payment);


			$arPaySystemServiceAll += $arPaySystemServices;
			$this->arResult['ORDER']['PAYMENTS'][ $key ]['ID']          = $payment->getPaymentSystemId();
			$this->arResult['ORDER']['PAYMENTS'][ $key ]['NAME']        = $payment->getField('PAY_SYSTEM_NAME');
			$this->arResult['ORDER']['PAYMENTS'][ $key ]['SUM']         = $payment->getSum();
			$this->arResult['ORDER']['PAYMENTS'][ $key ]['DESCRIPTION'] = $arPaySystemServices[ $payment->getPaymentSystemId() ]['DESCRIPTION'];
			$this->arResult['ORDER']['PAYMENTS'][ $key ]['NEW_WINDOW']  = $arPaySystemServices[ $payment->getPaymentSystemId() ]['NEW_WINDOW'] === 'Y' ? true : false;
			$this->arResult['ORDER']['PAYMENTS'][ $key ]['ACTION_FILE'] = $arPaySystemServices[ $payment->getPaymentSystemId() ]['ACTION_FILE'];
			$this->arResult['ORDER']['PAYMENTS'][ $key ]['IS_CASH']     = $arPaySystemServices[ $payment->getPaymentSystemId() ]['IS_CASH'] === 'Y' ? true : false;
			$this->arResult['ORDER']['PAYMENTS'][ $key ]['IS_PAID']     = $payment->isPaid();
			$this->arResult['ORDER']['PAYMENTS'][ $key ]['PAY_URL']     = htmlspecialcharsbx($this->arParams["PATH_TO_PAYMENT"]) . '?ORDER_ID=' . urlencode(urlencode($this->order->getField('ACCOUNT_NUMBER'))) . '&PAYMENT_ID=' . $payment->getField('ACCOUNT_NUMBER');

			$paySystemService = \Bitrix\Sale\PaySystem\Manager::getObjectById($payment->getPaymentSystemId());
			if ($arPaySystemServices[ $payment->getPaymentSystemId() ]['NEW_WINDOW'] === 'N' || $arPaySystemServices[ $payment->getPaymentSystemId() ]['ID'] == \Bitrix\Sale\PaySystem\Manager::getInnerPaySystemId()) {
				/** @var \Bitrix\Sale\PaySystem\ServiceResult $initResult */
				$initResult = $paySystemService->initiatePay($payment, null, \Bitrix\Sale\PaySystem\BaseServiceHandler::STRING);
				if ($initResult->isSuccess()) {
					$this->arResult['ORDER']['PAYMENTS'][ $key ]['BUFFERED_OUTPUT'] = $initResult->getTemplate();
				} else {
					$this->errors[] = $initResult->getErrorMessages();
				}
			}
		}


		/**
		 * Получение массива списка доступных платежных систем, идет деление на 2 типа, кредит(рассрочка) и остальные(нал-безнал)
		 * arResult['ORDER']['PAY_SYSTEMS'] = array('CREDIT', 'OTHER')
		 */
		$creditPaySystemsId = array(17, 23, 24, 27, 4);
		$otherPaySystemsId  = array(1, 6, 18, 25, 26, 28);
		$paySystemsList     = array();

		foreach ($arPaySystemServiceAll as $key => $paySystem) {

			$paySystem['DESCRIPTION'] = !isset($paySystem['DESCRIPTION']) ? $paySystem['DESCRIPTION'] : "";

			if (in_array($key, $creditPaySystemsId)) {
				$paySystemsList['CREDIT'][ $key ] = $paySystem;

				if (intval($this->order->getId()) > 0) {
					$paySystemsList['CREDIT'][ $key ]['PAY_URL'] = htmlspecialcharsbx($this->arParams["PATH_TO_PAYMENT"]) . '?ORDER_ID=' . urlencode(urlencode($this->order->getField('ACCOUNT_NUMBER'))) . '&PAYMENT_ID=' . $paySystem['ID'];
				}

				if (intval($paySystem['LOGOTIP']) > 0) {
					$paySystemsList['CREDIT'][ $key ]['LOGO_PATH'] = CFile::GetPath($paySystem['LOGOTIP']);
				}
			}

			if (in_array($key, $otherPaySystemsId)) {
				$paySystemsList['OTHER'][ $key ] = $paySystem;

				if (intval($this->order->getId()) > 0) {
					$paySystemsList['OTHER'][ $key ]['PAY_URL'] = htmlspecialcharsbx($this->arParams["PATH_TO_PAYMENT"]) . '?ORDER_ID=' . urlencode(urlencode($this->order->getField('ACCOUNT_NUMBER'))) . '&PAYMENT_ID=' . $paySystem['ID'];
				}

				if (intval($paySystem['LOGOTIP']) > 0) {
					$paySystemsList['OTHER'][ $key ]['LOGO_PATH'] = CFile::GetPath($paySystem['LOGOTIP']);
				}
			}
		}

		$this->arResult['ORDER']['PAY_SYSTEMS'] = $paySystemsList;
	}

	/**
	 * Работа с платежными системами
	 */
	protected function setOrderPayments() {
		//======================ПЛАТЕЖНАЯ СИСТЕМА=========================//
		$paymentId = "";
		if (intval($this->request['payment_id']) > 0) {
			$paymentId = intval($this->request['payment_id']);
		} elseif (intval($this->arParams['DEFAULT_PAYMENT_ID']) > 0) {
			$paymentId = intval($this->arParams['DEFAULT_PAYMENT_ID']);
		}

		/*Payment*/
		$arPaySystemServiceAll = [];
		$paymentCollection     = $this->order->getPaymentCollection();

		//Разделение платежа на 50%
		if (isset($this->request['split_payment']) && $this->request['split_payment'] === 'Y') {
			$splitPayment = true;
			$paymentPrice = intval($this->order->getPrice()) / 2;
		} else {
			$splitPayment = false;
			$paymentPrice = intval($this->order->getPrice());
		}

		$remainingSum = intval($this->order->getPrice());

		//Пока остаточтная сумма не равна 0 в цикле добавляем оплаты
		/** @var \Bitrix\Sale\Payment $extPayment */
		while ($remainingSum) {
			$remainingSum = $remainingSum - $paymentPrice;

			$extPayment          = $paymentCollection->createItem();
			$arPaySystemServices = \Bitrix\Sale\PaySystem\Manager::getListWithRestrictions($extPayment);

			$arPaySystemServiceAll += $arPaySystemServices;

			if (array_key_exists($paymentId, $arPaySystemServiceAll)) {
				$arPaySystem = $arPaySystemServiceAll[ $paymentId ];
			} else {
				reset($arPaySystemServiceAll);

				$arPaySystem = current($arPaySystemServiceAll);
			}

			if ( ! empty($arPaySystem)) {
				$extPayment->setFields(array(
					'PAY_SYSTEM_ID'   => $arPaySystem["ID"],
					'PAY_SYSTEM_NAME' => $arPaySystem["NAME"]
				));

				$extPayment->setField('SUM', $splitPayment ? intval($this->order->getPrice() / 2) : $this->order->getPrice());

			} else {
				$extPayment->delete();
			}
		}

		/**
		 * Получение массива списка доступных платежных систем, идет деление на 2 типа, кредит(рассрочка) и остальные(нал-безнал)
		 * arResult['ORDER']['PAY_SYSTEMS'] = array('CREDIT', 'OTHER')
		 */
		$creditPaySystemsId = array(17, 23, 24, 27, 4);
		$otherPaySystemsId  = array(1, 6, 18, 25, 26, 28);
		$paySystemsList     = array();
		foreach ($arPaySystemServiceAll as $key => $paySystem) {

			$paySystem['DESCRIPTION'] = isset($paySystem['DESCRIPTION']) ? $paySystem['DESCRIPTION'] : "";


			if (in_array($key, $creditPaySystemsId)) {
				$paySystemsList['CREDIT'][ $key ] = $paySystem;

				if (intval($this->order->getId()) > 0) {
					$paySystemsList['CREDIT'][ $key ]['PAY_URL'] = htmlspecialcharsbx($this->arParams["PATH_TO_PAYMENT"]) . '?ORDER_ID=' . urlencode(urlencode($this->order->getField('ACCOUNT_NUMBER'))) . '&PAYMENT_ID=' . $paySystem['ID'];
				}

				if (intval($paySystem['LOGOTIP']) > 0) {
					$paySystemsList['CREDIT'][ $key ]['LOGO_PATH'] = CFile::GetPath($paySystem['LOGOTIP']);
				}
			}

			if (in_array($key, $otherPaySystemsId)) {
				$paySystemsList['OTHER'][ $key ] = $paySystem;

				if (intval($this->order->getId()) > 0) {
					$paySystemsList['OTHER'][ $key ]['PAY_URL'] = htmlspecialcharsbx($this->arParams["PATH_TO_PAYMENT"]) . '?ORDER_ID=' . urlencode(urlencode($this->order->getField('ACCOUNT_NUMBER'))) . '&PAYMENT_ID=' . $paySystem['ID'];
				}

				if (intval($paySystem['LOGOTIP']) > 0) {
					$paySystemsList['OTHER'][ $key ]['LOGO_PATH'] = CFile::GetPath($paySystem['LOGOTIP']);
				}
			}
		}

		$this->arResult['ORDER']['PAY_SYSTEMS'] = $paySystemsList;

		/**
		 * Отдельный массив с суммой оплат
		 * Нужен чисто для вывода сумм оплат в итоговом разделе summary.php
		 * @var \Bitrix\Sale\Payment $payment
		 */
		$this->arResult['PAYMENTS'] = array();
		foreach ($paymentCollection as $payment) {
			$this->arResult['PAYMENTS'][] = $payment->getSum();
		}
	}

	/**
	 * Проверка и применение купона на скидку
	 */
	protected function checkCoupon() {
		$this->arResult['COUPON'] = array(
			'USED_COUPON' => "", //Испльзуемый купон, строка
			'IS_ENTERED'  => false, //Купон применен
			'CLEARED'     => false, //Купон отменен
		);

		\Bitrix\Sale\DiscountCouponsManager::init();

		if (isset($this->request['coupon']) && strlen($this->request['coupon']) > 0) {
			$couponData = \Bitrix\Sale\DiscountCouponsManager::getData($this->request['coupon']);

			//Проверяем на существование купона
			if ( ! \Bitrix\Sale\DiscountCouponsManager::isExist($this->request['coupon']) || $couponData['CHECK_CODE'] === \Bitrix\Sale\DiscountCouponsManager::COUPON_CHECK_NO_ACTIVE) {
				\Bitrix\Sale\DiscountCouponsManager::clear(true);
				$this->errors['COUPON'] = "К сожалению, этот купон не действителен";
			} else {
				$this->arResult['COUPON']['USED_COUPON'] = $this->request['coupon'];

				$this->arResult['COUPON']['IS_ENTERED'] = \Bitrix\Sale\DiscountCouponsManager::add($this->arResult['COUPON']['USED_COUPON']);
				$discount                               = $this->order->getDiscount();
				$discount->calculate();

				//$discount->getApplyResult();
			}
			$this->order->doFinalAction(true);
		} elseif (isset($this->request['addCoupon']) && isset($this->request['coupon']) && strlen($this->request['coupon']) === 0 && ! isset($this->request['clearCoupon'])) {
			$this->errors['COUPON'] = "Пожалуйста, введите код купона";
		}

		//Очищаем(отменяем) купон
		if (isset($this->request['clearCoupon']) && $this->request['clearCoupon'] === "Y") {
			\Bitrix\Sale\DiscountCouponsManager::clear(true);
			$this->arResult['COUPON']['CLEARED'] = true;
		}

	}

	/**
	 * Действия со свойствами заказа
	 * Заполняем свойства по коду из полученого request
	 */
	protected function setOrderProps() {
		foreach ($this->order->getPropertyCollection() as $prop) {
			/** @var \Bitrix\Sale\PropertyValue $prop */
			$value = '';

			if (empty($value)) {
				foreach ($this->request as $key => $val) {
					if (strtolower($key) == strtolower($prop->getField('CODE'))) {
						$value = $val;
					}
				}
			}

			if (empty($value)) {
				$value = $prop->getProperty()['DEFAULT_VALUE'];
			}

			if ( ! empty($value)) {
				$prop->setValue($value);
			}
		}
		$this->order->setField('USER_DESCRIPTION', $this->request['USER_DESCRIPTION']);
	}

	/**
	 * Заполняем массив arResult необходимыми значениями
	 */
	protected function fillArResult() {
		//Флаг, что зазаз офорлен
		if (isset($this->request['order_id']) && intval($this->request['order_id']) > 0) {
			$this->arResult['DONE'] = true; //Заказ уже существует
		} else {
			$this->arResult['DONE'] = false; //Заказ еще не сохранен
		}


		$this->arResult['ORDER']['ID']             = $this->order->getId(); //ID заказа
		$this->arResult['ORDER']['PERSON_TYPE_ID'] = $this->order->getPersonTypeId(); //Тип плательщика (всегда ФизЛицо)
		$this->arResult['ORDER']['USER_ID']        = $this->order->getUserId(); //ИД пользователя
		$this->arResult['ORDER']['PRICE']          = $this->order->getPrice(); //Стоимость заказа
		$this->arResult['ORDER']['PAYED']          = $this->order->isPaid(); //Флаг оплаченного заказа
		$this->arResult['SELECTED_PAYSYSTEM']      = $this->order->getPaymentSystemId()[0]; //Выпбранная платежная система(по умолчанию Наличка)
		$this->arResult['SELECTED_DELIVERY']      = $this->order->getDeliverySystemId()[1]; //Выпбранная служба доставки
		$this->arResult['ORDER']['PROPS']          = $this->getAvailableOrderProps(); //Массив свойств заказа для вывода в шаблоне

		// Вычисляем стоимость скидки, либо стоимость наценки
		// Цена заказа - базовая цена товара
		// !!!ВАЖНО!!! актуально только тогда, кодгда в корзине 1 товар(тур), переделать если в корзине будут несколько товаров.
		if ($this->order->getBasket()->getItemById($this->basketTourProductId)) {
			$this->arResult['ORDER']['DISCOUNT_PRICE'] = $this->arResult['ORDER']['PRICE'] - $this->order->getBasket()->getItemById($this->basketTourProductId)->getBasePrice();
		}

		//Элементы корзины в массив
		$this->arResult['ORDER']['BASKET'] = array();
		if ($item = $this->order->getBasket()->getItemById($this->basketTourProductId)) {
			$this->arResult['ORDER']['BASKET']['ID']          = $item->getId();
			$this->arResult['ORDER']['BASKET']['PRODUCT_ID']  = $item->getProductId();
			$this->arResult['ORDER']['BASKET']['PRICE']       = $item->getPrice();
			$this->arResult['ORDER']['BASKET']['FINAL_PRICE'] = $item->getFinalPrice();
			$this->arResult['ORDER']['BASKET']['BASE_PRICE']  = $item->getBasePrice();
			$this->arResult['ORDER']['BASKET']['QUANTITY']    = $item->getQuantity();
			$this->arResult['ORDER']['BASKET']['CAN_BUY']     = $item->canBuy();
			$this->arResult['ORDER']['BASKET']['UE_PRICE']     = $this->usd_price;
		}


		$this->arResult['USE_DISCOUNT']  = $this->usediscount; //Флаг использования скидки у товара
		$this->arResult['POST_FORM_URI'] = POST_FORM_ACTION_URI; //Ссылка POST_FORM_ACTION_URI

		//Флаг разделения оплаты
		$this->arResult['SPLIT_PAYMENT_IS_ACTIVE'] = isset($this->request['split_payment']) && $this->request['split_payment'] === 'Y' ? "Y" : "N";
	}

	/**
	 * Валидация формы
	 * В массив $this->errors[] добаяются флаги
	 * USER_PROPS_INVALID - ошибка в заполнении обязательных свойств заказа, флаг нужен для скрола к ошибке в шаблоне
	 * TOURIST_PROPS_INVALID - ошибка в заполнении обязательных полей туристов, нужно для скрола к месту ошибки в шаблоне
	 * CHILD_PROPS_INVALID - ошибка в заполнении обязательных полей детей, нужно для скрола к месту ошибки в шаблоне
	 *
	 * @return bool
	 */
	protected function validateForm() {
		$valid = true;

		//Валидация полей заказчика
		$this->errors['USER_PROPS_INVALID'] = false;
		foreach ($this->arResult['ORDER']['PROPS']['SHOW'] as $key => $prop):
			if (isset($this->request[ $prop['CODE'] ]) && empty($this->request[ $prop['CODE'] ]) && $prop['REQUIRED']) {
				$this->arResult['ORDER']['PROPS']['SHOW'][ $key ]['HAS_ERROR'] = true;
				$valid                                                         = false;

				$this->errors['USER_PROPS_INVALID'] = true;
			} elseif ($key == 'EMAIL' && ! check_email($this->request[ $prop['CODE'] ])) {
				$this->arResult['ORDER']['PROPS']['SHOW'][ $key ]['HAS_ERROR'] = true;
				$valid                                                         = false;
				$this->errors['USER_PROPS_INVALID']                            = true;
			}
		endforeach;

		//Валидация полей туристов
//		$this->errors['TOURIST_PROPS_INVALID'] = false;
//		$requiredTourstsFields                 = array('TOURIST_NAME', 'TOURIST_LASTNAME', 'TOURIST_BIRTH');
//
//		for ($i = 1; $i <= intval($this->arResult['TOURDATA']['adults']); $i++) {
//			foreach ($requiredTourstsFields as $field) {
//				if (strlen($this->request[ $field . "_" . $i ]) === 0) {
//					$this->errors['TOURIST_PROPS_INVALID'] = true;
//					$valid                                 = false;
//				}
//			}
//		}
//
//		//Валидация полей детей
//		$requiredChildFields                 = array('CHILD_NAME', 'CHILD_LASTNAME', 'CHILD_BIRTH');
//		$this->errors['CHILD_PROPS_INVALID'] = false;
//		for ($i = 1; $i <= intval($this->arResult['TOURDATA']['child']); $i++) {
//			foreach ($requiredChildFields as $field) {
//				if (strlen($this->request[ $field . "_" . $i ]) === 0) {
//					$this->errors['CHILD_PROPS_INVALID'] = true;
//					$valid                               = false;
//				}
//			}
//		}

		return $valid;
	}

	//Запускаем компонент
	public function executeComponent() {
		global $APPLICATION, $USER;

		try{
			if ($this->arParams['IS_AJAX']) {
				$APPLICATION->RestartBuffer();
			}

			//Заказ оформлен, т.е известно его ИД
			if (isset($this->request['order_id']) && intval($this->request['order_id']) > 0) {
				$this->arResult['DONE'] = true;// Флаг что заказ офорлен, нужен для отображения нужно шаблона

				$this->getCreatedOrder($this->request['order_id']); //Получение объекта заказа по его ИД

				$dayInterval = '';
				$orderDate   = new DateTime($this->order->getDateInsert()->format('d.m.Y')); //Дата формирования заказа
				$obInterval  = $orderDate->diff(new DateTime());
				$dayInterval = $obInterval->days;

				//Если не заполнено свойство Дата пересчета по курсу
				if (!empty($this->order->getPropertyCollection()->getItemByOrderPropertyId(59)->getValue())) {
					$recalculateDate = new DateTime($this->order->getPropertyCollection()->getItemByOrderPropertyId(59)->getValue());
					$obInterval      = $recalculateDate->diff(new DateTime());
					$dayInterval     = $obInterval->days;
				}

				//Флаг того, что заказ пересчитан на теукщий день, если дата отличается от даты создания заказа

				if ($dayInterval > 0) {
					$this->arResult['ORDER']['CALCULATED'] = false;
				} else {
					$this->arResult['ORDER']['CALCULATED'] = true;
				}

				//Запуск пересчета заказа по параметру "recalculate"
				if (isset($this->request['recalculate']) && $this->request['recalculate'] === "Y") {
					$this->recalculateOrder();
					$this->arResult['ORDER']['CALCULATED'] = true;
				}

			} else { //Зазаза нет, формируем "виртуальный" заказ
				$this->createVirtualOrder();
				$this->checkCoupon();
				$this->actualizeTourFromTourvisor(); //Акуталзация тура у турвизора
				$this->setOrderPayments();
			}

			/*Заполняем массив arResult*/
			$this->fillArResult();

			$this->arResult['REQUEST'] = $this->request->toArray();

			//Событие сохранения зазаза
			if ($this->request->isPost() && isset($this->request['save']) && $this->request['save'] == 'Y' && $this->validateForm()) {
				if ($this->registerUser()) {
					$this->order->save();
					$this->arResult['ORDER']['ID'] = $this->order->getId();
					$this->arResult['DONE']        = true; //Заказ сохранен
				}
			}

			$this->arResult['ERRORS'] = $this->errors;
		}catch (Exception $e){
			$this->arResult['ERRORS']["Exception"] = $e->getMessage();
		}

		/*AJAX OR NOT AJAX*/
		if ($this->arParams['IS_AJAX']) {
			ob_start();
			$this->includeComponentTemplate();
			$this->arResponse = $this->arResult;
			ob_end_clean();

			header('Content-Type: application/json');
			echo json_encode($this->arResponse);
			$APPLICATION->FinalActions();
			die();
		} else {
			$this->includeComponentTemplate();
		}
	}


	/**
	 * ***********************Пересчет заказа в зависимости от курса**************************
	 */
	protected function recalculateOrder() {
		$current_rate  = 0;
		$ue_currency   = '';
		$ue_price      = 0;
		$newOrderPrice = 0;
		$use_discount  = false;

		/** @var \Bitrix\Sale\Basket $basket */
		if ( ! ($basket = \Bitrix\Sale\Basket::loadItemsForOrder($this->order))) {
			throw new \Bitrix\Main\ObjectNotFoundException('Entity "Basket" not found');
		}

		if ($item = $basket->getItemById($this->basketTourProductId)) {

			//Получаем цену в валюте и название валюты из свойств товара в корзине заказа
			$basketPropRes = \Bitrix\Sale\Internals\BasketPropertyTable::getList(array(
				'filter' => array(
					"BASKET_ID" => $item->getId(),
				),
			));
			while ($property = $basketPropRes->fetch()) {

				//Цена в УЕ
				if ($property['CODE'] === 'UE_PRICE') {
					$orderPaymentSystem = $this->order->getPaymentSystemId();

					//Если онлайн оплата, тогда прибавляем комиссию
					if($orderPaymentSystem[0] == 25) {
						$ue_price = floatval($property['VALUE']) + (intval($property['VALUE']) * self::ONLINE_PAY_COMISSION);
					}else {
						$ue_price = intval($property['VALUE']);
					}
				}

				//Скидка за самостоятельность
				if ($property['CODE'] === 'USE_DISCOUNT') {
					$use_discount = $property['VALUE'] === 'Y';
				}

				//Валюта УЕ
				if ($property['CODE'] === 'UE') {
					$ue_currency = $property['VALUE'];
				}
			}

			$ue_price = $use_discount ? $ue_price - floatval($ue_price * self::SELF_DISCOUNT) : $ue_price;


			//Получаем курс валюты на текущий день
			$currencyRes = \Bitrix\Currency\CurrencyRateTable::getList(array(
				'filter' => array(
					"CURRENCY"  => $ue_currency,
					'DATE_RATE' => new \Bitrix\Main\Type\DateTime()
				),
			));
			/** @var \Bitrix\Currency\CurrencyRateTable $currency */
			$current_rate = 0;
			while ($currency = $currencyRes->fetch()) {
				$current_rate = floatval($currency['RATE']); //Курс
			}

			//Считаем новую стоимсоть заказа в зависимотсти от курса валюты + 2,5%
			//И устанавливаем новую полную цену для товара в корзине
			$newOrderPrice = $ue_price * (floatval($current_rate) + (floatval($current_rate) * 0.025));
			if ($newOrderPrice > 0) {
				$item->setPrice($newOrderPrice, true);
				$item->save();
			}
		}

		if ($newOrderPrice > 0) {

			//Счтиаем скидки
			$discount = $this->order->getDiscount();
			\Bitrix\Sale\DiscountCouponsManager::clearApply(true);
			\Bitrix\Sale\DiscountCouponsManager::useSavedCouponsForApply(true);
			$discount->setOrderRefresh(true);
			$discount->setApplyResult(array());

			$res = $basket->refreshData(array('PRICE', 'COUPONS'));

			if ( ! $res->isSuccess()) {
				$this->errors[] = $res->getErrors();
			}

			$res = $discount->calculate();
			if ( ! $res->isSuccess()) {
				$this->errors[] = $res->getErrors();
			}


			//Меняем сумму оплаты для выбранного типа оплат
			if ( ! $this->order->isCanceled() && ! $this->order->isPaid()) {
				/** @var \Bitrix\Sale\PaymentCollection $paymentCollection */
				$paymentCollection = $this->order->getPaymentCollection();
				/** @var \Bitrix\Sale\Payment $payment */
				foreach ($paymentCollection as $key => $payment) {
					//Если платежа 2, тогда делим новую сумму пополам
					if ($paymentCollection->count() == 2) {
						$newPaymentSumm = $newOrderPrice/2;
					}else {
						$newPaymentSumm = $newOrderPrice;
					}
					if (!$payment->isPaid()) {
						$payment->setFieldNoDemand('SUM', $newPaymentSumm);
					}
				}
			}

			//Сохраняем заказ
			if (empty($this->errors)) {
				//Обновляем занчение свойство заказа RECALCULATE_DATE, текущей датой
				foreach ($this->order->getPropertyCollection() as $prop) {
					/** @var \Bitrix\Sale\PropertyValue $prop */
					if ($prop->getField('CODE') === 'RECALCULATE_DATE') {
						$prop->setValue(new Date());
					}
				}

				$res = $this->order->save();
				if ( ! $res->isSuccess()) {
					$this->errors[] = $res->getErrors();
				}
			}

			//Снова получаем заказ и сохраняем его
			//Не знаю зачем так, но без этого сумма заказа не обновляется
			$this->order = Bitrix\Sale\Order::load($this->order->getId());
			$this->order->refreshData();
			$this->order->save();

			//Снова получаем список оплат с новыми суммами
			$paymentCollection = $this->order->getPaymentCollection();
			//Список созданных оплат
			/* @var \Bitrix\Sale\Payment $payment */
			foreach ($paymentCollection as $key => $payment) {
				$arPaySystemServices = \Bitrix\Sale\PaySystem\Manager::getListWithRestrictions($payment);

				$this->arResult['ORDER']['PAYMENTS'][ $key ]['ID']          = $payment->getPaymentSystemId();
				$this->arResult['ORDER']['PAYMENTS'][ $key ]['NAME']        = $payment->getField('PAY_SYSTEM_NAME');
				$this->arResult['ORDER']['PAYMENTS'][ $key ]['SUM']         = $payment->getSum();
				$this->arResult['ORDER']['PAYMENTS'][ $key ]['DESCRIPTION'] = $arPaySystemServices[ $payment->getPaymentSystemId() ]['DESCRIPTION'];
				$this->arResult['ORDER']['PAYMENTS'][ $key ]['NEW_WINDOW']  = $arPaySystemServices[ $payment->getPaymentSystemId() ]['NEW_WINDOW'] === 'Y' ? true : false;
				$this->arResult['ORDER']['PAYMENTS'][ $key ]['ACTION_FILE'] = $arPaySystemServices[ $payment->getPaymentSystemId() ]['ACTION_FILE'];
				$this->arResult['ORDER']['PAYMENTS'][ $key ]['IS_CASH']     = $arPaySystemServices[ $payment->getPaymentSystemId() ]['IS_CASH'] === 'Y' ? true : false;
				$this->arResult['ORDER']['PAYMENTS'][ $key ]['IS_PAID']     = $payment->isPaid();
				$this->arResult['ORDER']['PAYMENTS'][ $key ]['PAY_URL']     = htmlspecialcharsbx($this->arParams["PATH_TO_PAYMENT"]) . '?ORDER_ID=' . urlencode(urlencode($this->order->getField('ACCOUNT_NUMBER'))) . '&PAYMENT_ID=' . $payment->getField('ACCOUNT_NUMBER');

				$paySystemService = \Bitrix\Sale\PaySystem\Manager::getObjectById($payment->getPaymentSystemId());
				if ($arPaySystemServices[ $payment->getPaymentSystemId() ]['NEW_WINDOW'] === 'N' || $arPaySystemServices[ $payment->getPaymentSystemId() ]['ID'] == \Bitrix\Sale\PaySystem\Manager::getInnerPaySystemId()) {
					/** @var \Bitrix\Sale\PaySystem\ServiceResult $initResult */
					$initResult = $paySystemService->initiatePay($payment, null, \Bitrix\Sale\PaySystem\BaseServiceHandler::STRING);
					if ($initResult->isSuccess()) {
						$this->arResult['ORDER']['PAYMENTS'][ $key ]['BUFFERED_OUTPUT'] = $initResult->getTemplate();
					} else {
						$this->errors[] = $initResult->getErrorMessages();
					}
				}
			}
		}


	}

	/**
	 * ***************** Регистрация нового пользователя **********************
	 */
	protected function registerUser() {
		global $USER;
		$email = $this->request['EMAIL'];

		//Если пользователь уже авторизирован, тогда возвращаем истину
		if ($USER->IsAuthorized()) {
			return true;
		}

		//Пытаемся авторизвать пользователя по email... !!!ДЫРА В БЕЗОПАСНОСТИ!!!
		$success_auth = $this->doAuthByEmail($email);

		//Если пользоваетля таким email не существует, тогда регистрируем нового пользователя
		if ( ! $success_auth) {
			$arEmail = explode("@", $email);
			$login   = $email;
			$pass    = $this->generatePassword();

			$user     = new CUser;
			$arFields = Array(
				"NAME"             => $this->request['NAME'],
				"EMAIL"            => $email,
				"LOGIN"            => $login,
				"LID"              => "ru",
				"ACTIVE"           => "Y",
				"GROUP_ID"         => array(3),
				"PASSWORD"         => $pass,
				"CONFIRM_PASSWORD" => $pass,
			);

			$USER_ID = $user->Add($arFields);

			if ($USER_ID > 0) {
				$USER->Authorize($USER_ID);
				if ($USER->IsAuthorized()) {
					CUser::SendUserInfo($USER->GetID(), SITE_ID, Loc::getMessage("INFO_REQ"), true);
					$success_auth = true;
				}
			} else {
				$this->errors['REGISTER'] = "Ошибка регистрации нового пользователя" . (strlen($user->LAST_ERROR) > 0 ? ": " . $user->LAST_ERROR : "");

				return false;
			}
		} else {
			return true;
		}
	}

	/**
	 * @param $email
	 * Авторизация по EMAIL
	 *
	 * @return bool
	 */
	protected function doAuthByEmail($email) {
		global $USER;
		$rsUsers = CUser::GetList($by = "", $order = "", array('=EMAIL' => $email), array('EXTERNAL_AUTH_ID'));

		if ($arUser = $rsUsers->GetNext()) {

			if (isset($arUser['EXTERNAL_AUTH_ID']) && ! empty($arUser['EXTERNAL_AUTH_ID'])) {
				return false;
			} else {
				//Не авторизовываем, если пользователь из группы администраторов
				if ( ! in_array('1', CUser::GetUserGroup($arUser['ID']))) {
					/* TODO ДЫРА В БЕЗОПАСНОСТИ !!!!!!!!!!!!!!!!!!!!!!!!!!!! */
					$USER->Authorize($arUser['ID']);

					return true;
				} else {
					$this->errors['REGISTER'] = "Ошибка, к сожалению не получается создать заказ для Вас. Пожалуйста войдите под своим логином и паролем";

					return false;
				}
			}
		}

		return false;
	}

	/**
	 * @param int $length
	 * Генерируем пароль для новго пользователя
	 *
	 * @return string
	 */
	private function generatePassword($length = 6) {
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$count = mb_strlen($chars);

		for ($i = 0, $result = ''; $i < $length; $i++) {
			$index  = rand(0, $count - 1);
			$result .= mb_substr($chars, $index, 1);
		}

		return $result;
	}
}

?>