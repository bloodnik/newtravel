<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

$this->addExternalCss($this->GetFolder() . "/loading/loading.css");
$this->addExternalCss($this->GetFolder() . "/loading/loading-btn.css");

$this->addExternalJs($this->GetFolder() . "/dist/script.js");

$random = rand(200, 1000);

?>
<div class="pt-3 pb-3 ld-over" :class="{'running' : searchInAction}" id="tours-offer_<?=$random?>" style="min-height: 530px;" v-cloak>
	<div class="row">
		<? if ($arParams['ITEM_STYLE'] === 'media'): ?>
			<ul class="list-unstyled w-100 d-flex flex-column flex-md-row h-100 flex-wrap offers-carousel">
				<li class="media mb-3" v-for="hotel in obSearchResults">
					<div class="d-flex bg-light p-3 align-self-stretch">
						<img class="d-flex align-self-center mr-3 pl-3" :src="hotel.picturelink">
						<div class="media-body d-flex flex-column align-content-start">
							<h5 class="mt-0 mb-1"><a :href="'/hotel/'+hotel.hotelcode + '/'" target="_blank">{{hotel.hotelname}} {{hotel.hotelstars}}*</a></h5>
							<small class="text-muted">{{hotel.regionname}}, {{hotel.countryname}}</small>
							<p>
								{{hotel.tours.tour[0].flydate }},
								<span>{{hotel.tours.tour[0].adults}} взр. <span v-if="parseInt(hotel.tours.tour[0].child) > 0">+ {{hotel.tours.tour[0].child}} реб.</span></span>,
								{{hotel.tours.tour[0].nights | displayNights}}, {{hotel.tours.tour[0].placement}}, {{hotel.tours.tour[0].mealrussian}}, {{hotel.tours.tour[0].operatorname}}
							</p>
							<div>
								Цена:
								<a class="h4 text-success" @click.prevent="getDetail(hotel.tours.tour[0].tourid)" href="#">
									{{hotel.tours.tour[0].price | formatPrice}}
								</a>
								<p>без скидки: <span class="text-muted display-8"><s>{{hotel.tours.tour[0].price | getOldPrice}}</s></span></p>
							</div>
							<small class="text-muted">Обновлено {{hotel.lastUpdate | dateFromNow}}</small>
							<div class="mt-auto text-right">
								<a class="btn btn-info ld-ext-left" :class="'detail-btn_'+ hotel.tours.tour[0].tourid" href="#" @click.prevent="getDetail(hotel.tours.tour[0].tourid)">
									Купить
									<div class="ld ld-ring ld-spin"></div>
								</a>
							</div>
						</div>
					</div>
				</li>
			</ul>
		<? elseif ($arParams['ITEM_STYLE'] === 'card'): ?>
			<div class="offers-carousel owl-carousel owl-theme">
				<div class="mb-3" v-for="hotel in obSearchResults">
					<div class="card">
						<img class="card-img-top" :src="hotel.picturelink" style="height: 170px;">
						<div class="card-body" style="height: 300px;">
							<h5 class="card-title"><a :href="'/hotel/'+hotel.hotelcode + '/'" target="_blank">{{hotel.hotelname}} {{hotel.hotelstars}}*</a></h5>
							<small class="text-muted">{{hotel.regionname}}, {{hotel.countryname}}</small>
							<p class="card-text">
								{{hotel.tours.tour[0].flydate }},
								<span>{{hotel.tours.tour[0].adults}} взр. <span v-if="parseInt(hotel.tours.tour[0].child) > 0">+ {{hotel.tours.tour[0].child}} реб.</span></span>,
								{{hotel.tours.tour[0].nights | displayNights}}, {{hotel.tours.tour[0].placement}}, {{hotel.tours.tour[0].mealrussian}}, {{hotel.tours.tour[0].operatorname}}
							</p>
							<div class="card-text">
								Цена:
								<a class="h4 text-success" @click.prevent="getDetail(hotel.tours.tour[0].tourid)" href="#">
									{{hotel.tours.tour[0].price | formatPrice}}<span v-if="params.PRICE_PER_ONE === 'Y'">/чел.</span>
								</a>
								<p>без скидки: <span class="text-muted display-8"><s>{{hotel.tours.tour[0].price | getOldPrice}}<span v-if="params.PRICE_PER_ONE === 'Y'">/чел.</span></s></span></p>
							</div>
							<p class="card-text">
								<small class="text-muted">Обновлено {{hotel.lastUpdate | dateFromNow}}</small>
							</p>
						</div>
						<div class="card-footer">
							<a class="btn btn-info btn-block ld-over" :class="'detail-btn_'+ hotel.tours.tour[0].tourid" href="#" @click.prevent="getDetail(hotel.tours.tour[0].tourid)">
								Купить
								<div class="ld ld-ring ld-spin"></div>
							</a>
						</div>
					</div>
				</div>
			</div>
		<? endif; ?>
	</div>
	<div class="ld ld-ring ld-spin"></div>
</div>

<script>
    var params_<?=$random?> = <?=CUtil::PhpToJSObject($arParams)?>;

    $(document).ready(function () {
        setTimeout(function () {
            window.tourOffers_<?=$random?> = new Vue({
                el: '#tours-offer_<?=$random?>',
                data: {
                    params: params_<?=$random?>,
                    toursNotFound: false,
                    searchInAction: false,
                    obSearchResults: [],
                    hash: "",
                    debug: true,
                },
                created: function () {
                    var self = this;

                    searchInAction = true;
                    //Получаем хэш строку по JSON строку текущих параметров.
                    self.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "getHash", hashString: JSON.stringify(self.params)}}).then(function (response) {
                        self.hash = response.body;

                        //Проверяем актуальность кэша. Если кэша нет, тогда делаем запросы в турвзизор, иначе берем из кэша
                        self.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "checkCache", hash: self.hash}}).then(function (response) {
                            if (response.body === 'fail') {
                                self.startSearch();
                            } else {
                                self.obSearchResults = response.body;
                                searchInAction = false;
                                self.loadSlider();
                            }
                        });
                    });

                },
                methods: {
                    loadSlider: function () {
                        setTimeout(function () {
                            $('.offers-carousel').owlCarousel({
                                loop: true,
                                margin: 10,
                                nav: false,
                                autoplay: true,
                                autoplayTimeout: 3000,
                                autoplayHoverPause: true,
                                center:true,
                                responsive: {
                                    0: {
                                        items: 1
                                    },
                                    600: {
                                        items: 3
                                    },
                                    1000: {
                                        items: 4
                                    }
                                }
                            })
                        }, 300);

                    },

                    startSearch: function () {
                        var self = this;

                        self.searchInAction = true;
                        self.toursNotFound = false;
                        self.obSearchResults = [];
                        self.requestid = '';

                        var params = {
                            type: "search",
                            departure: this.params.DEPARTURE_ID,
                            country: this.params.COUNTRY_ID,
                            hotels: this.params.HOTELS_ID,
                            adults: this.params.ADULTS,
                            child: this.params.CHILD,
                            childage1: this.params.CHILDAGE1,
                            childage2: this.params.CHILDAGE2,
                            childage3: this.params.CHILDAGE3,
                            pricefrom: this.params.PRICE_FROM,
                            priceto: this.params.PRICE_TO,
                            datefrom: this.params.DATE_FROM,
                            dateto: this.params.DATE_TO,
                            operators: "",
                            pricetype : this.params.PRICE_PER_ONE === "Y" ? "1" : "0",
                            meal : this.params.MEAL !== undefined ? this.params.MEAL : "",
                            stars : this.params.STARS !== undefined ? this.params.STARS : "",
                            rating : this.params.RATING !== undefined ? this.params.RATING : "",
                            regions : this.params.REGIONS !== undefined ? this.params.REGIONS : "",
                            subregions : this.params.SUBREGIONS !== undefined ? this.params.SUBREGIONS : "",
                        };

                        self.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: params}).then(function (response) {
                            if (response.body.result.requestid !== undefined) {
                                self.requestid = response.body.result.requestid;
                                self.searchUrl = response.body.url;

                                setTimeout(function () {
                                    self.getStatus();
                                }, 3000);
                            } else {
                                if (this.debug) {
                                    console.log("Ошибка startSearch");
                                }
                            }
                        });
                    },

                    //Запрос состояние поиска
                    getStatus: function () {
                        var self = this;

                        if (self.debug) {
                            console.log('=========== Проверяем статус ==========');
                        }

                        this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "status", requestid: self.requestid, nodescription: 0, operatorstatus: 0}}).then(function (response) {
                            if (response.body.data.status !== undefined) { //Запрос прошел без ошибок
                                if (response.body.data.status.progress !== "100") { //Повторяем проверку статуса
                                    setTimeout(function () {
                                        if (self.debug) {
                                            console.log("Не все операторы отработали, повторяем запрос статусов");
                                        }
                                        self.getStatus();
                                    }, 2000);
                                } else {
                                    if (self.debug) {
                                        console.log("==Все обработно! Урра!!!==");
                                        console.log("==Выводим все результаты на экран==");
                                    }
                                    self.searchInAction = false;
                                    self.getResult();
                                }
                            } else {
                                if (self.debug) {
                                    console.log("Ошибка getStatus");
                                }
                            }
                        });
                    },

                    //Получение результатов поиска
                    getResult: function () {
                        var self = this;

                        self.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "result", requestid: self.requestid, page: self.pageNumber}}).then(function (response) {
                            if (response.body.data.status.state === 'finished' && parseInt(response.body.data.status.toursfound) === 0) {
                                self.toursNotFound = true;
                            }

                            if (response.body.data.result !== undefined) {
                                if (self.debug) {
                                    console.log('======= Получите результаты ========');
                                }

                                self.obSearchResults = response.body.data.result.hotel;

                                //Срзеаем массив если задано количество
                                if (self.params.ONPAGE !== undefined && parseInt(self.params.ONPAGE) > 0) {
                                    self.obSearchResults = _.slice(self.obSearchResults, 0, parseInt(self.params.ONPAGE));
                                }

                                _.forEach(self.obSearchResults, function (hotel, key) {
                                    self.obSearchResults[key].lastUpdate = moment().format('DD.MM.YYYY, HH:mm:ss');
                                });

                                self.searchInAction = false;

                                self.loadSlider();

                                $.post('/local/modules/newtravel.search/lib/ajax.php', {type: "setCache", hash: self.hash, data: self.obSearchResults}, function (data) {

                                });

                            } else {
                                if (self.debug) {
                                    console.log("Ошибка getResult");
                                }
                            }
                        });
                    },

                    //Детальная актуализация, взывает модальное окно
                    getDetail: function (tourid) {
                        var detailParams = {
                            tourid: tourid,
                        };

                        $('.detail-btn_' + tourid).addClass('running');

                        var myModal = new jBox('Modal', {
                            onClose: function () {
                                $('.jBox-Modal').remove();
                                $('.jBox-overlay').remove();
                                $('.detail-btn_' + tourid).removeClass('running');
                            },
                        });

                        myModal.open({
                            responsiveWidth: true,
                            responsiveHeight: true,
                            width: "950",
                            height: "900",
                            closeButton: true,
                            blockScroll: true,
                            ajax: {
                                url: '/local/components/newtravel.search/full.search.v2/templates/.default/detail.php',
                                data: detailParams,
                                reload: 'strict',
                                spinner: true,
                            },
                        });
                    },

                },
                filters: {
                    formatPrice: function (value) {
                        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
                    },
                    displayNights: function (value) {
                        if (!value) {
                            return "";
                        } else {
                            var titles = ['ночь', 'ночи', 'ночей'];
                            value = parseInt(value);
                            return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                        }
                    },

                    //Старая цена
                    getOldPrice: function (value) {
                        value = parseInt(value) + parseInt(value * 0.11);
                        return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
                    },

                    //Сколько прошло врмени
                    dateFromNow: function (value) {
                        moment.locale('ru');
                        return moment(value, "DD.MM.YYYY, HH:mm:ss").fromNow()
                    },


                }
            })
        }, 500 + parseInt(<?=$random?>));
    })


</script>
