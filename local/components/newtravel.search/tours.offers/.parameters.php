<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


if(!CModule::IncludeModule("iblock"))
	return;

$arTypes = CIBlockParameters::GetIBlockTypes();

$arIBlocks=array();
$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => "Тип Инфоблока",
			"TYPE" => "LIST",
			"VALUES" => $arTypes,
			"DEFAULT" => "news",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => "ID каталога туров",
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => '',
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
		"HOTELS_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "ID отелей(через запятую)",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"DATE_FROM" => Array(
			"PARENT" => "BASE",
			"NAME" => "Дата от",
			"TYPE" => "STRING",
			"DEFAULT" => date("d.m.Y", strtotime("+1 day")),
		),
		"DATE_TO" => Array(
			"PARENT" => "BASE",
			"NAME" => "Дата от",
			"TYPE" => "STRING",
			"DEFAULT" => date("d.m.Y", strtotime("+14 day")),
		),
		"DEPARTURE_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "Город вылета",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"COUNTRY_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "ID страны",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"PRICE_FROM" => Array(
			"PARENT" => "BASE",
			"NAME" => "Цена от",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"PRICE_TO" => Array(
			"PARENT" => "BASE",
			"NAME" => "Цена до",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"ADULTS" => Array(
			"PARENT" => "BASE",
			"NAME" => "Взрослых",
			"TYPE" => "STRING",
			"DEFAULT" => "2",
		),
		"CHILD" => Array(
			"PARENT" => "BASE",
			"NAME" => "Детей",
			"TYPE" => "STRING",
			"DEFAULT" => "0",
		),
		"CHILDAGE1" => Array(
			"PARENT" => "BASE",
			"NAME" => "Возраст 1",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"CHILDAGE2" => Array(
			"PARENT" => "BASE",
			"NAME" => "Возраст 2",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"CHILDAGE3" => Array(
			"PARENT" => "BASE",
			"NAME" => "Возраст 3",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"ITEM_STYLE" => array(
			"PARENT" => "BASE",
			"NAME" => "Отображение",
			"TYPE" => "LIST",
			"VALUES" => array('media' => 'Медиа список', 'card' => 'Карточки'),
			"DEFAULT" => "media",
		),
		"SINGLE_ITEM" => array(
			"PARENT" => "BASE",
			"NAME" => "Один элемент",
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),

		"PRICE_PER_ONE" => array(
			"PARENT" => "BASE",
			"NAME" => "Цена за одного",
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),


		"CACHE_TIME"  =>  array("DEFAULT"=>3600)
	),
);
?>