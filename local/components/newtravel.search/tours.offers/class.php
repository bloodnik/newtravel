<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Entity\Query;
use Newtravel\Search\ToursBasket;
use Newtravel\Search\Tourvisor;
use Bitrix\Main\Loader;
use Bitrix\Main\Application;

class CToursOffers extends CBitrixComponent {


	//Запускаем компонент
	public function executeComponent() {
		global $USER;

		$this->includeComponentTemplate();
	}
}

?>