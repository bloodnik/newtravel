<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<section class="pt-3 pb-3" id="reviews">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-heading">
					<h2 class="display-5">Похожие отели</h2>
				</div>
			</div>

			<div class="col-12">
				<div class="card-deck">
						<? foreach ($arResult['RELATED_HOTELS'] as $arHotel): ?>
						<div class="card mb-2">
							<a class="text-white" href="/hotel/<?=$arHotel['CODE']?>/">
								<? $img = CFile::ResizeImageGet($arHotel['PROPERTY_MORE_PHOTO_VALUE'][2], array('width' => 250, 'height' => 200), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true) ?>
								<img class="card-img-top" src="<?=$img['src']?>" alt="<?=$arHotel['NAME']?>" style="height: 200px">

								<div class="card-img-overlay" style="background-color: rgba(0,0,0,0.11)">
									<h4 class="card-title"><?=$arHotel['NAME']?> <?=$arHotel['PROPERTY_STARS_VALUE']?>*</h4>
									<div class="h5">Рейтинг: <span class="display-6"><?=$arHotel['PROPERTY_RATING_VALUE']?> / 5</span></div>
								</div>
							</a>
						</div>
					<? endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>