<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>

<section class="pt-3 bg-light">
	<div class="container">
		<?$APPLICATION->IncludeComponent(
			"newtravel.search:hotel.search.v2",
			"",
			Array(
				"ADULTS" => "2",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"CITY" => "4",
				"COUNTRY" => $arResult["hotel"]['PROPS']['COUNTRYCODE']['VALUE'],
				"HOTELCODE" => $arResult["hotel"]['CODE'],
			),
			$component
		);?>
	</div>
</section>

<section class="bg-info pt-3 pb-3">
	<div class="container">
		<div class="col-12 text-center">
			<div class="display-6 text-white">Не нашли подходящий тур? Закажи подборку у наших экспертов!</div>
		</div>
		<div class="col-12 text-center mt-2"><a class="btn btn-orange" href="#" data-toggle="modal" data-target="#pickMeTourModal">Оставить заявку</a></div>
	</div>
</section>