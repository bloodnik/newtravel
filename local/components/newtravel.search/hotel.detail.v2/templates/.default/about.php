<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<section class="pt-3 pb-3" id="about">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-heading">
					<h2 class="display-5">Об отеле <?=$arResult['hotel']['NAME']?></h2>
				</div>
			</div>

			<div class="col-12 pb-5">

				<? if ($arProps["RATING"]['VALUE']): ?>
					<p><span class="h5">Рейтинг: </span> <span class="display-7"><?=$arProps["RATING"]['VALUE']?>/<span class="text-success">5</span></span></p>
				<? endif; ?>

				<? if ($arProps["PHOTE"]['VALUE']): ?>
					<p><span class="h5">Телефон: </span> <?=$arProps["PHOTE"]['VALUE']?></p>
				<? endif; ?>

				<? if ($arProps["BUILD"]['VALUE'] || $arProps["REPAIR"]['VALUE'] || $arProps["PLACEMENT"]['VALUE'] || $arProps["BEACH"]['VALUE']): ?>
					<? if (isset($arProps["BUILD"]['VALUE'])): ?>
						<p><span class="h5">Год постройки: </span> <?=$arProps["BUILD"]['~VALUE']?></p>
					<? endif; ?>
					<? if (isset($arProps["REPAIR"]['VALUE'])): ?>
						<p><span class="h5">Последний ремонт: </span> <?=$arProps["REPAIR"]['~VALUE']?></p>
					<? endif; ?>
					<? if (isset($arProps["PLACEMENT"]['VALUE'])): ?>
						<p><span class="h5">Расположение: </span><?=$arProps["PLACEMENT"]['~VALUE']?></p>
					<? endif; ?>
					<? if (isset($arProps["BEACH"]['VALUE'])): ?>
						<p><span class="h5">Пляж: </span> <?=$arProps["BEACH"]['~VALUE']?></p>
					<? endif; ?>
				<? endif; ?>

				<? if ( ! empty($arResult['hotel']["PREVIEW_TEXT"])): ?>
					<p class="h5">Описание</p>
					<p><?=$arResult['hotel']["~PREVIEW_TEXT"]?></p>
				<? else: ?>
					<p>Описание отеля недоступно</p>
				<? endif ?>
			</div>

			<? if ( ! empty($arResult["SERVICES"])): ?>
				<div class="col-12">
					<div class="page-heading">
						<h2 class="display-5">Инфраструктура</h2>
					</div>

					<? foreach ($arResult["SERVICES"] as $arRow): ?>
						<div class="row thumbnail">
							<? foreach ($arRow as $arItem): ?>
								<div class="col-12 col-md-4 mb-3">
									<p class="h5 text-info"><?=$arItem['NAME']?></p>
									<? foreach ($arItem['ITEMS'] as $arService): ?>
										<div><?=$arService?></div>
									<? endforeach; ?>
								</div>
							<? endforeach; ?>
						</div>
					<? endforeach; ?>

				</div>
			<? endif; ?>



		</div>
	</div>
</section>