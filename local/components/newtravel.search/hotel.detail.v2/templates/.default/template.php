<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var $this CBitrixComponentTemplate */
$this->setFrameMode(true);

$this->addExternalJs("/local/templates/newtravel/js/plugins/gmaps.min.js");

$arProps = $arResult["hotel"]['PROPS'];

?>
<?include("header.php")?>

<?include("photo.php")?>

<?include("search.php")?>

<?include("about.php")?>

<? if (!empty($arResult['RELATED_HOTELS'])): ?>
	<?include("related.php")?>
<? endif; ?>

<? if (!empty($arResult['REVIEWS'])): ?>
	<?include("reviews.php")?>
<? endif; ?>
