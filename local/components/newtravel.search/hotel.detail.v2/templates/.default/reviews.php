<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<section class="pt-3 pb-3" id="reviews">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-heading">
					<h2 class="display-5">Отзывы</h2>
				</div>
			</div>

			<div class="col-12">
				<? foreach ($arResult['REVIEWS'] as $arReview): ?>
					<div class="card mb-2">
						<div class="card-header">
							<?=$arReview['name']?>
						</div>
						<div class="card-body">
							<? if ($arReview['rate']): ?>
								<div class="card-title">Оценка: <?=$arReview['rate']?></div>
							<? endif; ?>
							<p class="card-text"><?=$arReview['content']?></p>
							<p class="text-muted">Время отдыха: <?=$arReview['traveltime']?></p>
						</div>
					</div>
				<? endforeach; ?>
			</div>
		</div>
	</div>
</section>