<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
global $APPLICATION;

$arProps = $arResult['hotel']['PROPS'];

$APPLICATION->SetPageProperty("title", $arProps["COUNTRY"]['VALUE'] . ", " . $arProps["REGION"]['VALUE'] . ", " . $arResult['hotel']["NAME"] . " из Уфы - Умныетуристы.рф");
$APPLICATION->SetTitle($arProps["COUNTRY"]['VALUE'] . ", " . $arProps["REGION"]['VALUE'] . ", " . $arResult['hotel']["NAME"]);
$APPLICATION->AddChainItem($arProps["COUNTRY"]['VALUE']);
$APPLICATION->AddChainItem($arResult['hotel']["NAME"]);
$APPLICATION->SetPageProperty("description", $arResult['hotel']["NAME"] . ". Информация об отеле. Закажи по выгодной цене!");
$APPLICATION->SetDirProperty("keywords", $arResult['hotel']["NAME"] . " Уфа цена");
\Bitrix\Main\Page\Asset::getInstance()->addString('<link href="https://' . SITE_SERVER_NAME . '/hotel/' . $arResult['hotel']['CODE'] . '/" rel="canonical" />', true);