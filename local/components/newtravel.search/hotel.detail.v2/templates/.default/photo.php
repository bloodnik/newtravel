<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<section id="photo">
	<div class="row no-gutters">
		<? if (count($arProps['MORE_PHOTO']['VALUE']) > 0): ?>
			<?$col_9 = "";?>
			<? if ( $arProps['LAT']['VALUE'] || $arProps['LONG']['VALUE']){ $col_9 = "col-md-9"; }?>
				<div class="col-12 <?=$col_9?>">
				<? if (count($arProps['MORE_PHOTO']['VALUE']) > 0): ?>
					<div class="hotel-photos-carousel">
						<? foreach ($arProps['MORE_PHOTO']['SRC'] as $image): ?>
							<a class="carousel-item" data-fancybox="hotelPhotos" href="<?=$image?>"><img src="<?=$image?>" height="250" alt="<?=$arResult["hotel"]["NAME"]?>"></a>
						<? endforeach; ?>
					</div>
				<? endif; ?>
			</div>
		<? endif; ?>
		<? if ($arProps['LAT']['VALUE'] && $arProps['LONG']['VALUE']): ?>
			<div class="col-12 col-md-3" style="width: auto; height: 250px;">
				<div id="hotelmap" style="height: 100%"></div>
				<script>
                    $(document).ready(function () {
                        ymaps.ready(init);
                        function init() {
                            // Создание карты.
                            var myMap = new ymaps.Map("hotelmap", {
                                center: [<?=isset($arProps['LAT']['VALUE']) ? $arProps['LAT']['VALUE'] : "0"?>, <?=isset($arProps['LONG']['VALUE']) ? $arProps['LONG']['VALUE'] : "0"?>],
                                zoom: 16
                            });
                            var myPlacemark = new ymaps.Placemark([<?=isset($arProps['LAT']['VALUE']) ? $arProps['LAT']['VALUE'] : "0"?>, <?=isset($arProps['LONG']['VALUE']) ? $arProps['LONG']['VALUE'] : "0"?>], {
                                hintContent: '',
                                balloonContent: ''
                            }, {
                                iconImageHref: '/local/templates/newtravel/images/mappin.png',
                            });
                            myMap.geoObjects.add(myPlacemark);
                        }
                    });


//                        var hotelmap = new GMaps({
//                            el: 'hotelmap',
//                            height: 100,
//                            width: 100,
//                            scrollwheel: false,
//                            lat: <?//=isset($arProps['LAT']['VALUE']) ? $arProps['LAT']['VALUE'] : "0"?>//,
//                            lng: <?//=isset($arProps['LONG']['VALUE']) ? $arProps['LONG']['VALUE'] : "0"?>
//                        });
//
//                        hotelmap.addMarker({
//                            lat: <?//=isset($arProps['LAT']['VALUE']) ? $arProps['LAT']['VALUE'] : "0"?>//,
//                            lng: <?//=isset($arProps['LONG']['VALUE']) ? $arProps['LONG']['VALUE'] : "0"?>//,
//                            clickable: false,
//                            icon: "/local/templates/newtravel/images/mappin.png"
//                        });

                   // google.maps.event.addDomListener(window, 'load', showHotelMap);
				</script>
			</div>
		<? endif; ?>
	</div>
</section>