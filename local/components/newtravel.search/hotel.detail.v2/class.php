<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

use Newtravel\Search\Tourvisor;
use Newtravel\Search\Hotels;
use Bitrix\Main\Loader;

class CHotelDetail extends CBitrixComponent {

	//Запускаем компонент
	/**
	 *
	 */
	public function executeComponent() {
		global $APPLICATION;
		Loader::includeModule("newtravel.search");

		$arProps  = array();
		$arResult = array();

		$hotelCode = intval($this->request->get('hotelcode')) ?: "";

		if (empty($hotelCode)) {
			LocalRedirect("/tours");
		}

		//Сохраняем отель в базу
		Hotels::saveHotel($hotelCode);

		$cache_id = $this->request->get('hotelcode') ?: "";
		$cache_id .= "_" . $this->request->get('departure') ?: "";
		$cache_id .= "_" . $this->request->get('datefrom') ?: "";
		$cache_id .= "_" . $this->request->get('dateto') ?: "";

		if ($this->startResultCache($this->arParams['CACHE_TIME'], $cache_id)) {
			//Если отель есть в БД
			if (Hotels::checkHotelInDB($hotelCode)) {
				$arResult['hotel'] = Hotels::getHotelInDB($hotelCode);

				$arProps = $arResult['hotel']['PROPS'];

				$arHotelLists = array();


				foreach ($arProps['MORE_PHOTO']['VALUE'] as $key => $photoId) {
					$arResult['hotel']['PROPS']['MORE_PHOTO']['SRC'][] = CFile::GetPath($photoId);
				}

				$arService["territory"]   = $arProps['TERRITORY']['VALUE'];
				$arService["services"]    = $arProps['SERVICES']['VALUE'];
				$arService["child"]       = $arProps['CHILD']['VALUE'];
				$arService["servicefree"] = $arProps['SERVICEFREE']['VALUE'];
				$arService["servicepay"]  = $arProps['SERVICEPAY']['VALUE'];
				$arService["meallist"]    = $arProps['MEALLIST']['VALUE'];

				if ($arService["territory"] && is_array($arService["territory"])) {
					$arHotelLists[] = array(
						"NAME"  => "Инфраструктура отеля",
						"ICON"  => "hand-o-up",
						"ITEMS" => $arService["territory"]
					);
				}
				if ($arService["services"] && is_array($arService["services"])) {
					$arHotelLists[] = array(
						"NAME"  => "Услуги отеля",
						"ICON"  => "lightbulb-o",
						"ITEMS" => $arService["services"]
					);
				}
				if ($arService["animation"] && is_array($arService["animation"])) {
					$arHotelLists[] = array(
						"NAME"  => "Развлечения (Анимация)",
						"ICON"  => "smile-o",
						"ITEMS" => $arService["animation"]
					);
				}
				if ($arService["child"] && is_array($arService["child"])) {
					$arHotelLists[] = array(
						"NAME"  => "Услуги для детей",
						"ICON"  => "child",
						"ITEMS" => $arService["child"]
					);
				}
				if ($arService["servicefree"] && is_array($arService["servicefree"])) {
					$arHotelLists[] = array(
						"NAME"  => "Бесплатные услуги",
						"ICON"  => "check-circle-o",
						"ITEMS" => $arService["servicefree"]
					);
				}
				if ($arService["servicepay"] && is_array($arService["servicepay"])) {
					$arHotelLists[] = array(
						"NAME"  => "Платные услуги",
						"ICON"  => "money",
						"ITEMS" => $arService["servicepay"]
					);
				}
				if ($arService["meallist"] && is_array($arService["meallist"])) {
					$arHotelLists[] = array(
						"NAME"  => "Типы питания",
						"ICON"  => "cutlery",
						"ITEMS" => $arService["meallist"]
					);
				}
				$arHotelLists = array_chunk($arHotelLists, 3);

				$arResult["SERVICES"]  = $arHotelLists;
				$arResult["HOTELCODE"] = $hotelCode;

				$tv             = new Tourvisor();
				$arHotelReviews = $tv->getData('fullhotel', array("hotelcode" => $hotelCode, "reviews" => 1));

				//ОТЗЫВЫ
				$arResult['REVIEWS'] = isset($arHotelReviews['data']['hotel']['reviews']) ? $arHotelReviews['data']['hotel']['reviews']['review'] : array();

				//ПОХОЖИЕ ОТЕЛИ
				$arResult['RELATED_HOTELS'] = array();
				$arSelect                   = Array("ID", "NAME", "CODE", "PROPERTY_MORE_PHOTO", "PROPERTY_STARS", "PROPERTY_RATING");
				$arFilter                   = Array(
					"IBLOCK_ID"         => IntVal(58),
					"ACTIVE_DATE"       => "Y",
					"ACTIVE"            => "Y",
					"!CODE"            => $hotelCode,
					">=PROPERTY_STARS"    => $arProps["STARS"]['VALUE'],
					"SECTION_ID"        => $arResult['hotel']['IBLOCK_SECTION_ID'],
					"PROPERTY_COUNTRY"  => $arProps["COUNTRY"]['VALUE'],
					">=PROPERTY_RATING" => $arProps["RATING"]['VALUE'] - 0.5,
					"!PROPERTY_RATING" => 0,
				);
				$res                        = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 3), $arSelect);
				while ($arFields = $res->GetNext()) {
					$arResult['RELATED_HOTELS'][] = $arFields;
				}


				$this->arResult = $arResult;

				unset($arResult);
				unset($arHotelLists);
				unset($arHotelReviews);
				unset($arData);
				unset($arService);
			}

			$this->includeComponentTemplate();
		}


	}
}

;
?>