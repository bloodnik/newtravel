<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

class CFullSearchV2 extends CBitrixComponent {


	//Запускаем компонент
	public function executeComponent() {
		\Bitrix\Main\Loader::includeModule('newtravel.search');

		$this->includeComponentTemplate();
	}

}

?>