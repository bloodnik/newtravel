'use strict';

import HotelDatePicker from 'vue-hotel-datepicker';

$(document).ready(function () {
    window.simpleSearchApp = new Vue({
        el: '#simple-search',
        components: {
            HotelDatePicker
        },
        data: {
            /*Текущие свойства*/
            departure: "4", //Текущий город
            country: "4", //Текущая страна
            adults: 2, //Количество взрослых
            child: 0, //Количество детей
            childage1: "0", //Возраст ребенка 1
            childage2: "0", //Возраст ребенка 2
            childage3: "0", //Возраст ребенка 3
            nightsfrom: 6, //Количество ночей от
            nightsto: 14, //Количество ночей до
            datefrom: arParams.DATE_FROM ? arParams.DATE_FROM : moment().add(1, 'days').format('DD.MM.YYYY'), //Дата вылета от
            dateto: arParams.DATE_TO ? arParams.DATE_TO: moment().add(14, 'days').format('DD.MM.YYYY'), //Дата вылета до

            /*Объекты*/
            obDepartures: [],
            obCountries: [],
            obFlydates: [],

            /*Наименования*/
            countryName: "Турция",
            departureName: "Уфы",

            /*служебные*/
            pop_countries: [4,1,15,5,6,20,14,47],

            countrySearchQuery: "",

            ruRU: {
                night: 'ночь',
                nights: 'ночей',
                'day-names': ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
                'check-in': 'С',
                'check-out': 'По',
                'month-names': ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            },

            /*поиск*/
            touristString: "2 взр.",

            debug: false,
            searchUrl: "",
            mobile: false,
            showAdditionalParams: false,
        },

        created: function () {
            let _this = this;

            this.init();

            // dunno why v-on="resize: func" not working
            if (document.body.clientWidth <= 700) {
                _this.$set(_this, 'mobile', true);
            } else {
                _this.$set(_this, 'mobile', false);
            }
            global.window.addEventListener('resize', function () {
                if (document.body.clientWidth <= 700) {
                    _this.$set(_this, 'mobile', true);
                } else {
                    _this.$set(_this, 'mobile', false);
                }
            });
        },

        methods: {
            init: function () {
                let _this = this;
                _this.getDepartures();
                _this.getCountries();
                _this.getFlydates();
            },

            getDepartures: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "departure"}}).then(response => {
                        this.obDepartures = response.body.lists.departures.departure;
                        this.obDepartures = _.sortBy(this.obDepartures, ['name']);

                        //Подтвреждение города
                        // $('.departure_wrap').confirmCity({
                        //     departureModal: $('.cityFrom'),
                        //     departureId: this.departure
                        // });
                    }
                );
            },

            getCountries: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "country", cndep: this.departure}}).then(response => {
                    if (response.body.lists.countries.country !== null) {
                        this.obCountries = response.body.lists.countries.country;
                        this.obCountries = _.sortBy(this.obCountries, ['name']);
                    }

                });
            },


            getFlydates: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "flydate", flydeparture: this.departure, flycountry: this.country}}).then(response => {
                    if (response.body.lists.flydates !== null) {
                        this.obFlydates = response.body.lists.flydates.flydate;
                        //this.datepickerAttrs[0].dates = this.obFlydates;

                    } else {
                        //this.datepickerAttrs[0].dates = [];
                    }
                });
            },

            checkInChange: function (date) {
                this.datefrom = moment(date).format('DD.MM.YYYY');
                this.dateto = moment(date).format('DD.MM.YYYY');
            },
            checkOutChange: function (date) {
                this.dateto = moment(date).format('DD.MM.YYYY')
            },

            /**
             * Фильтр популярных стран
             * @return {boolean}
             */
            IsPopularCountry: function (country) {
                return _.includes(this.pop_countries, parseInt(country));
            },


            setTouristsString: function () {
                var self = this;

                self.touristString = self.adults + " взр. ";
                if (self.child > 0) {
                    self.touristString += self.child + " реб.(";
                    for (var i = 1; i <= self.child; i++) {
                        self.touristString += self['childage' + i];
                        if (i !== self.child) {
                            self.touristString += ' и '
                        }
                    }
                    self.touristString += ")";
                }
            },
        },

        watch: {
            departure: function () {
                this.getCountries();
                this.getFlydates();

                BX.setCookie('NEWTRAVEL_USER_CITY', this.departure, {path: '/'});
            },
            country: function (newCountry) {
                this.getFlydates();
            },
            adults: function () {
                this.setTouristsString();
            },
            child: function () {
                this.setTouristsString();
            },
            childage1: function () {
                this.setTouristsString();
            },
            childage2: function () {
                this.setTouristsString();
            },
            childage3: function () {
                this.setTouristsString();
            }
        },

        computed: {
            groupedDepartures: function () {
                return _.groupBy(this.obDepartures, function (s) {
                    return s.name.charAt();
                });
            },

            groupedCountries: function () {
                return _.groupBy(this.obCountries, function (s) {
                    return s.name.charAt();
                });
            },

            filteredCountries: function () {
                let self = this;
                return self.obCountries.filter(function (country) {
                    return country.name.toLowerCase().indexOf(self.countrySearchQuery.toLowerCase()) !== -1
                })
            },
            startDate: function () {
               return moment(this.datefrom, 'DD.MM.YYYY')._d;
            },
            endDate: function () {
                return moment(this.dateto, 'DD.MM.YYYY')._d;
            }

        },

        filters: {
            displayAge: function (value) {
                if (!value) {
                    return "";
                }else if(parseInt(value) === 1){
                    return "< 2 лет";
                }
                else {
                    let titles = ['год', 'года', 'лет'];
                    value = parseInt(value);
                    return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                }
            },

            displayAgeInt: function (value) {
                if (parseInt(value) >= 2 ) {
                    return value;
                }else if(parseInt(value) <= 1){
                    return "<2";
                }
            },

            displayNights: function (value) {
                if (!value) {
                    return "";
                } else {
                    let titles = ['ночь', 'ночи', 'ночей'];
                    value = parseInt(value);
                    return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                }
            },
        },
    });


    $('.select-dropdown.no-close').click(function (event) {
        event.stopPropagation();
    });

});