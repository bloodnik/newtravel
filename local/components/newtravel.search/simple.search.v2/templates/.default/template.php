<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

$templateFolder = $this->GetFolder();

$version = "2.9";

//Подтверждение города
//$this->addExternalJs("/local/modules/newtravel.search/js/jquery.confirm.city/jquery.confirm.city.v2.js?ver=" . $version);

$this->addExternalCss($this->GetFolder() . "/css/style.css?ver=" . $version);

$this->addExternalJs($this->GetFolder() . "/dist/script.js?ver=" . $version);

$vertical = $arParams['THEME'] == 'vertical' ? true : false;

?>

<script>
    var arParams = <?=CUtil::PhpToJSObject($arParams)?>;
</script>

<div id="simple-search-module">
	<div id="simple-search">

		<div class="btn btn-block load-spinner ld-over-inverse running" style="height: 50px;" v-cloak>
			<div class="ld ld-ring ld-spin"></div>
		</div>

		<div class="pb-3" v-cloak>
			<form name="simple-search" action="/tours/?start=Y" method="get">

				<!--ГОРОД ВЫЛЕТА-->
				<div class="row">
					<div class="col-12">
						<div class="h5 text-<?if($vertical):?>dark<?else:?>white<?endif;?>">Поиск туров из
							<div class="dropdown d-inline-block">
								<a href="javascript:void(0)" class="text-uppercase display-7 departure_wrap text-light-blue" style="font-weight: 400;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{departureName}}</a>
								<div class="dropdown-menu select-dropdown bg-light" style="width: 210px">
									<div class="scrollable-menu">
										<span class="dropdown-item" :class="{'bg-primary text-white' : departure == 99 }" @click="departure = 99; departureName = 'Без перелета'">Без перелета</span>
										<span v-for="(item, letter) in groupedDepartures">
											<h6 class="dropdown-header">{{ letter}}</h6>
											<span class="dropdown-item"
											      :class="{'bg-primary text-white' : departureItem.id == departure }"
											      v-for="departureItem in item"
											      @click="departure = departureItem.id; departureName = departureItem.namefrom"
											      v-if="departureItem.id != 99"
											>
												{{departureItem.namefrom}}
											</span>
										</span>
									</div>
								</div>
							</div>
							<input type="hidden" v-model="departure" name="departure">
						</div>
					</div>
				</div>

				<div class="row no-gutters">

					<!--СТРАНЫ-->
					<div class="mb-2 <?if($vertical):?>col-12<?else:?>col-sm-6 col-md-2<?endif;?>">
						<div class="dropdown">
							<button class="form-control form-control-lg country-select-btn text-left dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								{{countryName}}
							</button>
							<div class="dropdown-menu select-dropdown bg-light" aria-labelledby="dropdownMenuButton">
								<div class="col-12">
									<div class="input-group input-group-sm">
										<input type="text" v-model="countrySearchQuery" class="form-control" placeholder="поиск страны..." aria-label="поиск страны...">
										<span class="input-group-addon" v-show="countrySearchQuery.length > 0" @click="countrySearchQuery=''"><i class="fa fa-close"></i></span>
										<span class="input-group-addon"><i class="fa fa-search"></i></span>
									</div>
								</div>
								<div class="scrollable-menu">
									<h6 class="dropdown-header">Популярные</h6>
									<span class="dropdown-item" :class="{'bg-primary text-white': country == item.id}" @click="country = item.id; countryName = item.name;" v-if="IsPopularCountry(item.id)" v-for="item in filteredCountries">{{item
								.name}}</span>
									<h6 class="dropdown-header">Все страны</h6>
									<span class="dropdown-item" :class="{'bg-primary text-white': country == item.id}" @click="country = item.id; countryName = item.name;" v-if="!IsPopularCountry(item.id)" v-for="item in filteredCountries">{{item
								.name}}</span>
								</div>
							</div>
						</div>

						<input type="hidden" v-model="country" name="country">
					</div>

					<!--ДАТЫ ВЫЛЕТА-->
					<div class="mb-2 <?if($vertical):?>col-12<?else:?>col-sm-6 col-md-3<?endif;?>">
						<hotel-date-picker @check-in-changed="checkInChange"
						                   @check-out-changed="checkOutChange"
						                   :first-day-of-week="1"
						                   :min-nights="0"
						                   format="DD.MM.YYYY" :max-nights="14" :i18n="ruRU" :starting-date-value="startDate"
						                   :ending-date-value="endDate"></hotel-date-picker>
						<input type="hidden" v-model="datefrom" name="datefrom">
						<input type="hidden" v-model="dateto" name="dateto">
					</div>

					<!--НОЧЕЙ-->
					<div class="mb-2 <?if($vertical):?>col-12<?else:?>col-sm-6 col-md-3<?endif;?>">
						<div class="dropdown">
							<button class="form-control form-control-lg dropdown-toggle text-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">от {{nightsfrom}} до {{nightsto}} ночей</button>
							<div class="dropdown-menu select-dropdown no-close bg-light">
								<div class="col-12 text-center mb-2">
									<label>Ночей от:</label>
									<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" :disabled="nightsfrom == 1" @click="nightsfrom--">
												<span class="fa fa-minus"></span>
											</button>
										</span>
										<input type="text" v-model="nightsfrom" name="nightsfrom" class="form-control input-number" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" @click="nightsfrom++">
												<span class="fa fa-plus"></span>
											</button>
										</span>
									</div>
								</div>
								<div class="col-12 text-center">
									<label>Ночей до:</label>
									<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" :disabled="nightsto == 1" @click="nightsto--">
												<span class="fa fa-minus"></span>
											</button>
										</span>
										<input type="text" v-model="nightsto" name="nightsto" class="form-control input-number" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" @click="nightsto++">
												<span class="fa fa-plus"></span>
											</button>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!--ТУРИСТЫ-->
					<div class="mb-2 <?if($vertical):?>col-12<?else:?>col-sm-6 col-md-3<?endif;?>">
						<div class="dropdown">
							<button class="form-control form-control-lg dropdown-toggle text-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{touristString}}</button>
							<div class="dropdown-menu select-dropdown no-close bg-light">
								<div class="col-12 text-center">
									<label>Взрослых</label><br>
								</div>

								<div class="col-12 text-center">
									<div class="btn-group" role="group" aria-label="First group">
										<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : adults == 1}" @click="adults=1">1</button>
										<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : adults == 2}" @click="adults=2">2</button>
										<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : adults == 3}" @click="adults=3">3</button>
										<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : adults == 4}" @click="adults=4">4</button>
									</div>
								</div>

								<div class="col-12 text-center">
									<label class="mt-2">Детей</label>
								</div>
								<div class="col-12 text-center">
									<div class="btn-group" role="group" aria-label="Second group">
										<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : child == 0}" @click="child=0">0</button>
										<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : child == 1}" @click="child=1; parseInt(childage1) > 2? '':childage1 = 2; childage2 = 0; childage3 = 0">1</button>
										<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : child == 2}"
										        @click="child=2; parseInt(childage1) > 2? '':childage1 = 2; parseInt(childage2) > 2? '':childage2 = 2; childage3 = 0">2
										</button>
										<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : child == 3}" @click="child=3; parseInt(childage1) > 2? '':childage1 = 2; parseInt(childage2) > 2? '':childage2 = 2; parseInt(childage3) >
						2? '':childage3 = 2">3
										</button>
									</div>
								</div>

								<div class="col-12 text-center">
									<label class="mt-2">Возраст детей</label>
								</div>

								<div class="col-12 text-center">
									<div class="input-group input-group-sm mt-3" v-if="child >= 1">
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" :disabled="childage1 == 1" @click="childage1--">
												<span class="fa fa-minus"></span>
											</button>
										</span>
										<input type="text" v-model="childage1" name="childage1" class="form-control input-number" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" @click="childage1++">
												<span class="fa fa-plus"></span>
											</button>
										</span>
									</div>
									<div class="input-group input-group-sm mt-3" v-if="child >= 2">
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" :disabled="childage2 == 1" @click="childage2--">
												<span class="fa fa-minus"></span>
											</button>
										</span>
										<input type="text" v-model="childage2" name="childage2" class="form-control input-number" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" @click="childage2++">
												<span class="fa fa-plus"></span>
											</button>
										</span>
									</div>
									<div class="input-group input-group-sm mt-3" v-if="child == 3">
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" :disabled="childage3 == 1" @click="childage3--">
												<span class="fa fa-minus"></span>
											</button>
										</span>
										<input type="text" v-model="childage3" name="childage3" class="form-control input-number" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" @click="childage3++">
												<span class="fa fa-plus"></span>
											</button>
										</span>
									</div>
								</div>
							</div>
						</div>


						<input type="hidden" v-model="adults" name="adults">
						<input type="hidden" v-model="child" name="child">
						<input type="hidden" name="start" value="Y">
					</div>

					<div class="mb-2 <?if($vertical):?>col-12<?else:?>col-sm-12 col-md-1<?endif;?>">
						<button class="btn btn-primary btn-lg btn-block">Жми!</button>
					</div>
				</div>
			</form>
		</div>


	</div>
</div>