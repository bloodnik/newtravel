'use strict';

const webpack = require('webpack');

module.exports = {
    entry: {
        search: './src/search-form.js',
    },
    output: {
        path: __dirname + "/dist",
        filename: "[name].js",
        library: '[name]'

    },

    watch: true,

    watchOptions: {
        aggregateTimeout: 100
    },

    devtool: "cheap-inline-module-source-map",

    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('development')
            }
        })
    ],

    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' для webpack 1
        }
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader?optional[]=runtime',
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.vue$/,
                exclude: /node_modules/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        ts: 'ts-loader'
                    },
                    esModule: true
                }
            },
        ]
    }
};
//
//
// module.exports.plugins.push(
//     new webpack.optimize.UglifyJsPlugin({
//         compress:{
//             warnings: false,
//             drop_console: true,
//             unsafe : true
//         }
//     })
// )