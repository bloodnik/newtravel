<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

$templateFolder = $this->GetFolder();

$version = "2.9";

//Подтверждение города
//$this->addExternalJs("/local/modules/newtravel.search/js/jquery.confirm.city/jquery.confirm.city.v2.js?ver=" . $version);

$this->addExternalCss($this->GetFolder() . "/css/loading/loading.css");
$this->addExternalCss($this->GetFolder() . "/css/loading/loading-btn.css");
$this->addExternalCss($this->GetFolder() . "/css/style.css?ver=" . $version);

$this->addExternalJs($this->GetFolder() . "/dist/search.js?ver=" . $version);

?>
<div id="search-module">
	<script>
        var arHotelParams = <?=CUtil::PhpToJSObject($arParams)?>;
	</script>

	<div class="mt-3" id="search">

		<div class="btn btn-block load-spinner ld-over-inverse running mb-3" style="height: 50px;" v-cloak>
			<div class="ld ld-ring ld-spin"></div>
		</div>

		<div v-cloak>

			<!--ГОРОД ВЫЛЕТА-->
			<com-departure :departures-data="obDepartures" :departure="departure"></com-departure>

			<div class="row bg-primary align-items-center pb-3 pt-3 text-light">


				<!--ДАТЫ ВЫЛЕТА-->
				<div class="col-12 col-md-4 col-lg-4">
					<label>Даты вылета</label>
					<div class="dropdown">
						<div class="form-control" id="dropdownMenuButtonDates" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span>{{datefrom}} - {{dateto}}</span>
						</div>

						<div class="dropdown-menu select-dropdown bg-light" aria-labelledby="dropdownMenuButtonDates">
							<div id="dateRangeSelectContainer"></div>
						</div>
					</div>
				</div>

				<!--НОЧЕЙ-->
				<div class="col-12 col-md-4 col-lg-4">
					<label>Ночей</label>

					<div class="dropdown">
						<button class="form-control dropdown-toggle text-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">от {{nightsfrom}} до {{nightsto}} ночей</button>
						<div class="dropdown-menu select-dropdown no-close bg-light">
							<div class="col-12 text-center mb-2">
								<label>Ночей от:</label>
								<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" :disabled="nightsfrom == 1" @click="nightsfrom--">
												<span class="fa fa-minus"></span>
											</button>
										</span>
									<input type="text" v-model="nightsfrom" name="nightsfrom" class="form-control input-number" readonly>
									<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" @click="nightsfrom++">
												<span class="fa fa-plus"></span>
											</button>
										</span>
								</div>
							</div>
							<div class="col-12 text-center">
								<label>Ночей до:</label>
								<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" :disabled="nightsto == 1" @click="nightsto--">
												<span class="fa fa-minus"></span>
											</button>
										</span>
									<input type="text" v-model="nightsto" name="nightsto" class="form-control input-number" readonly>
									<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" @click="nightsto++">
												<span class="fa fa-plus"></span>
											</button>
										</span>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!--ТУРИСТЫ-->
				<div class="col-12 col-md-4 col-lg-4">
					<label class="mr-5">Туристы</label>
					<div class="dropdown">
						<div class="form-control" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{touristString}}</div>
						<div class="dropdown-menu select-dropdown no-close bg-light">

							<!--ВЗРОСЛЫЕ-->
							<div class="col-12 text-center">
								<label>Взрослых</label><br>
							</div>

							<div class="col-12 text-center">
								<div class="btn-group" role="group" aria-label="First group">
									<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : adults == 1}" @click="adults=1">1</button>
									<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : adults == 2}" @click="adults=2">2</button>
									<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : adults == 3}" @click="adults=3">3</button>
									<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : adults == 4}" @click="adults=4">4</button>
								</div>
							</div>

							<!--ДЕТИ-->
							<div class="col-12 text-center">
								<label class="mt-2">Детей</label>
							</div>
							<div class="col-12 text-center">
								<div class="btn-group" role="group" aria-label="Second group">
									<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : child == 0}" @click="child=0">0</button>
									<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : child == 1}" @click="child=1; parseInt(childage1) > 2? '':childage1 = 2; childage2 = 0; childage3 = 0">1</button>
									<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : child == 2}"
									        @click="child=2; parseInt(childage1) > 2? '':childage1 = 2; parseInt(childage2) > 2? '':childage2 = 2; childage3 = 0">2
									</button>
									<button type="button" class="btn btn-primary" :class="{'btn-outline-primary text-dark' : child == 3}" @click="child=3; parseInt(childage1) > 2? '':childage1 = 2; parseInt(childage2) > 2? '':childage2 = 2; parseInt(childage3) >
						2? '':childage3 = 2">3
									</button>
								</div>
							</div>

							<!--ВОЗРАСТ ДЕТЕЙ-->
							<div class="col-12 text-center" v-if="child >= 1">
								<label class="mt-2">Возраст детей</label>
							</div>

							<div class="col-12 text-center px-4">
								<div class="input-group" v-if="child >= 1">
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" :disabled="childage1 == 1" @click="childage1--">
												<span class="fa fa-minus"></span>
											</button>
										</span>
									<input type="text" v-model="childage1" name="childage1" class="form-control input-number" readonly>
									<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" @click="childage1++">
												<span class="fa fa-plus"></span>
											</button>
										</span>
								</div>
								<div class="input-group mt-3" v-if="child >= 2">
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" :disabled="childage2 == 1" @click="childage2--">
												<span class="fa fa-minus"></span>
											</button>
										</span>
									<input type="text" v-model="childage2" name="childage2" class="form-control input-number" readonly>
									<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" @click="childage2++">
												<span class="fa fa-plus"></span>
											</button>
										</span>
								</div>
								<div class="input-group mt-3" v-if="child == 3">
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" :disabled="childage3 == 1" @click="childage3--">
												<span class="fa fa-minus"></span>
											</button>
										</span>
									<input type="text" v-model="childage3" name="childage3" class="form-control input-number" readonly>
									<span class="input-group-btn">
											<button type="button" class="btn btn-primary btn-number" @click="childage3++">
												<span class="fa fa-plus"></span>
											</button>
										</span>
								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="col-12 mt-3 text-right" :class="{'d-none' : showAdditionalParams, 'd-block d-md-none' : !showAdditionalParams}">
					Показать дополнительные параметры
					<button class="btn btn-warning btn-sm text-white" @click="showAdditionalParams = !showAdditionalParams"><i class="fa fa-plus"></i></button>
				</div>
			</div>

			<div class="row bg-light justify-content-end align-items-center pb-2 pt-3" :class="{'d-block' : showAdditionalParams, 'd-none d-md-flex' : !showAdditionalParams}">

				<!--ПИТАНИЕ-->
				<div class="col-12 col-md-6 col-lg-3">
					<label>Питание</label>

					<div class="dropdown">
						<div class="form-control" id="dropdownMenuButtonMeal" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span>{{!meal ? 'Любое' : obMealsName.join(', ')}}</span>
						</div>

						<div class="dropdown-menu select-dropdown no-close bg-light" aria-labelledby="dropdownMenuButtonMeal">
							<div class="scrollable-menu">
								<span class="dropdown-item" :class="{'bg-primary text-white' : meal == ''}" @click="meal = ''; obMealsSelected = []; obMealsName=[]">Любое</span>
								<span class="dropdown-item" :class="{'bg-primary text-white' : _.includes(obMealsSelected, item.id)}" @click="mealsSelect(item)" v-for="item in obMeals">{{item.name}} - {{item.russian}}</span>
							</div>
						</div>
					</div>
				</div>

				<!--ТУРОПЕРАТОРЫ-->
				<div class="col-12 col-md-6 col-lg-3">
					<label>Туроператоры</label>

					<div class="dropdown">
						<div class="form-control" id="dropdownMenuButtonTO" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span>{{!operators ? 'Любой' : obOperatorsName.join(', ')}}</span>
						</div>

						<div class="dropdown-menu select-dropdown no-close bg-light" aria-labelledby="dropdownMenuButtonTO">
							<div class="scrollable-menu">
								<span class="dropdown-item" @click="operators = ''; obOperatorsSelected = []; obOperatorsName=[];" :class="{'bg-primary text-white' : operators == ''}">Любой</span>
								<span class="dropdown-item" @click="operatorSelect(item)" :class="{'bg-primary text-white' : _.includes(obOperatorsSelected, item.id)}" v-for="item in obOperators">{{item.name}}</span>
							</div>
						</div>
					</div>
				</div>

				<!--ЦЕНА-->
				<div class="col-12 col-md-6 col-lg-3">
					<label>Цена</label>

					<div class="input-group">
						<input type="text" class="form-control" placeholder="от" v-model="pricefrom">
						<span class="input-group-addon" id="basic-addon1">-</span>
						<input type="text" class="form-control" placeholder="до" v-model="priceto">
					</div>
				</div>

				<!--ИСКАТЬ-->
				<div class="col-12 col-md-3 align-self-end d-none d-md-block">
					<button class="btn btn-lg btn-block btn-orange ld-over-inverse" @click="startSearch" :class="searchInAction?'running':''" :disabled="searchInAction">
						ИСКАТЬ !
						<div class="ld ld-ring ld-spin"></div>
					</button>
				</div>

			</div>

			<div class="row" id="results">
				<div class="col-12 d-block d-md-none mt-3">
					<a class="btn btn-info btn-block mb-4" href="#" data-toggle="modal" data-target="#pickMeTourModal">Оставить заявку на подбор тура</a>
					<button class="btn btn-lg btn-block btn-orange ld-over-inverse" @click="startSearch" :class="searchInAction?'running':''" :disabled="searchInAction">
						ИСКАТЬ !
						<div class="ld ld-ring ld-spin"></div>
					</button>
				</div>
				<div class="col-12 mt-3 text-right" :class="{'d-none' : !showAdditionalParams, 'd-block d-md-none' : showAdditionalParams}">
					Скрыть дополнительные параметры
					<button class="btn btn-danger btn-sm text-white" @click="showAdditionalParams = !showAdditionalParams"><i class="fa fa-minus"></i></button>
				</div>

				<div class="col-12">
					<com-progress-bar :search-in-action="searchInAction" :search-progress="searchProgress" :all-rows-count="allRowsCount"></com-progress-bar>
					<com-results :results-data="obSearchResults" :search-url="searchUrl" :tours-not-found="toursNotFound"></com-results>
				</div>
			</div>

			<div class="modal fade fastOrderModalWrap" id="fastOrderModal" tabindex="-1" role="dialog" aria-labelledby="fastOrderModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-sm" role="document">
					<div class="modal-content">
						<div class="d-flex-inline align-content-end pr-2">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<h3 class="text-primary display-8">Все очень просто! Закажите тур<span id="credit-info" style="display: none;"> в рассрочку</span>, оставив только имя и номер телефона!</h3>
								</div>
							</div>

							<div class="row" :class="{'d-none' : !fastFormSended}">
								<div class="col-12">
									<div class="alert alert-success" role="alert">Ваш заказ успешно отправлен. В скором времени специалисты свяжутся с Вами.</div>
								</div>
							</div>

							<div class="row">
								<div class="col-12 mb-3">
									<div class="from-group">
										<input type="text" class="form-control name_input" :class="{'is-invalid' : fastFormNameHasError}" v-model="fastFormName" name="form_text_82" placeholder="Ваше имя" required/>
									</div>
								</div>
								<div class="col-12 mb-3">
									<div class="from-group">
										<input type="text" class="form-control phone phone_input" :class="{'is-invalid' : fastFormPhoneHasError}" v-model="fastFormPhone" name="form_text_71" placeholder="Номер телефона" required/>
									</div>
								</div>
								<div class="col-12">
									<div class="from-group">
										<?=bitrix_sessid_post()?>
										<input name="fastTourId" type="hidden">
										<input name="credit" type="hidden" value="N">
										<button class="btn btn-success btn-block" @click="fastOrder">Заказать</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
    $(document).ready(function () {
        /*FAST ORDER MODAL OPEN*/
        $('#fastOrderModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var recipient = button.data('fast-tour-id'); // Extract info from data-* attributes

            var modal = $(this);

            var info = button.data('info');

            if (info === 'credit') {
                modal.find('.modal-body #credit-info').show();
                modal.find('.modal-body input[name=credit]').val("Y");
            } else {
                modal.find('.modal-body #credit-info').hide();
                modal.find('.modal-body input[name=credit]').val("N");
            }

            modal.find('.modal-body input[name=fastTourId]').val(recipient);
        })
    });
</script>