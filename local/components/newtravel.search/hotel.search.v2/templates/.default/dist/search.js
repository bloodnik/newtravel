var search =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
    name: 'com-departure',
    props: ['departuresData', 'departure'],
    data() {
        return {
            cityFromName: "Уфы"
        };
    },
    methods: {
        //Устанавливаем ID города
        setDeparture: function (departureItem) {
            this.cityFromName = departureItem.namefrom;
            this.$root.$data.departure = departureItem.id;
        }
    },
    computed: {
        //Группируем города по буквам
        groupedDepartures: function () {
            return _.groupBy(this.departuresData, function (s) {
                return s.name.charAt();
            });
        }
    },
    watch: {
        departure: function () {
            this.cityFromName = _.find(this.departuresData, ['id', this.departure.toString()]) ? _.find(this.departuresData, ['id', this.departure.toString()]).namefrom : this.cityFromName;
        }
    }
});

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
    name: 'com-results',
    props: ['resultsData', 'searchUrl', 'toursNotFound'],
    data() {
        return {
            showHiddenTours: false,
            obHotelsDescr: {},
            obBanners: {
                //                    0: {
                //                        name: "Баннер 1",
                //                        img: "",
                //                    },
                //                    1: {
                //                        name: "Баннер 2",
                //                        img: "",
                //                    },
            }
        };
    },
    methods: {

        //Детальная актуализация, взывает модальное окно
        getDetail: function (tourid) {
            var detailParams = {
                tourid: tourid
            };

            $('.detail-btn_' + tourid).addClass('running');

            var myModal = new jBox('Modal', {
                onClose: function () {
                    $('.jBox-Modal').remove();
                    //$('.jBox-overlay').remove();
                    $('.detail-btn_' + tourid).removeClass('running');
                }
            });

            myModal.open({
                responsiveWidth: true,
                responsiveHeight: true,
                width: "950",
                height: "900",
                closeButton: true,
                blockScroll: true,
                ajax: {
                    url: '/local/components/newtravel.search/full.search.v2/templates/.default/detail.php',
                    data: detailParams,
                    reload: 'strict',
                    spinner: true
                }
            });
        },

        toggleHiddenTours: function () {
            this.showHiddenTours = !this.showHiddenTours;
            if (!this.showHiddenTours) $('html, body').animate({ scrollTop: $('#results').offset().top }, 100);
        },

        //Добавление тура в корзину
        addTour2Basket: function (tourid, event) {
            this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "addToToursBasket", tourid: tourid } }).then(function (response) {
                if (response.body === 'success') {
                    $(event.target).text('Добавлено');
                    if (window.tbs !== undefined) {
                        window.tbs.getCount();
                    }
                }
            });
        }

    },
    filters: {
        formatPrice: function (value) {
            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
        },

        getOldPrice: function (value) {
            value = parseInt(value) + parseInt(value * 0.11);
            return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
        },

        getCreditPrice: function (value) {
            value = (parseInt(value) + parseInt(value * 0.0717)) / 6;
            return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
        },

        shortText: function (value) {
            let sliced = value.slice(0, 100);
            if (sliced.length < value.length) {
                sliced += '...';
            }

            return sliced;
        },

        displayNights: function (value) {
            if (!value) {
                return "";
            } else {
                var titles = ['ночь', 'ночи', 'ночей'];
                value = parseInt(value);
                return value + " " + titles[value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2];
            }
        }

    }

});

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
    name: 'com-progress-bar',
    props: ['searchInAction', 'searchProgress', 'allRowsCount']
});

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

var _ComDeparture = __webpack_require__(6);

var _ComDeparture2 = _interopRequireDefault(_ComDeparture);

var _ComResults = __webpack_require__(8);

var _ComResults2 = _interopRequireDefault(_ComResults);

var _ComProgressBar = __webpack_require__(10);

var _ComProgressBar2 = _interopRequireDefault(_ComProgressBar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

$(document).ready(function () {

    window.searchApp = new Vue({
        el: '#search',
        data: {
            /*Текущие свойства*/
            departure: "4", //Текущий город
            country: parseInt(arHotelParams.COUNTRY) > 0 ? arHotelParams.COUNTRY : "2", //Текущая страна
            hotels: parseInt(arHotelParams.HOTELCODE) > 0 ? arHotelParams.HOTELCODE : "0", //Отель
            meal: "", //Выбранные типы питания
            adults: 2, //Количество взрослых
            child: 0, //Количество детей
            childage1: "0", //Возраст ребенка 1
            childage2: "0", //Возраст ребенка 2
            childage3: "0", //Возраст ребенка 3
            nightsfrom: 6, //Количество ночей от
            nightsto: 14, //Количество ночей до
            pricefrom: "", //Цена от
            priceto: "", //Цена до
            datefrom: moment().add(1, 'days').format('DD.MM.YYYY'), //Дата вылета от
            dateto: moment().add(14, 'days').format('DD.MM.YYYY'), //Дата вылета до
            operators: "", //Список операторов
            requestid: "", //ID запроса
            pageNumber: 1, //Номер страницы

            /*Объекты*/
            obDepartures: [],
            obOperators: [],
            obFlydates: [],
            obMeals: [],
            obSearchResults: [],

            /*Временные объекты*/
            obMealsSelected: [],
            obOperatorsSelected: [],
            obMealsName: [],
            obOperatorsName: [],

            /*Наименования*/
            touristString: "2 взр.",

            /*служебные*/
            toursNotFound: false,

            /*поиск*/
            searchString: "", //поисковая строка страны
            allOperatorsProcessed: true, //Все ТО обработали запрос
            allRowsCount: 0, //Общее количество результатов
            firstRowsIsDisplayed: false, //Первая партия результатов загружена
            minPrice: 0, //Минимальная найденная цена
            searchProgress: 0, //Прогрес поиска
            searchInAction: false, //Флаг того, что поиск стартовал
            nextPageRequestInAction: false, //Флаг того, что идет запрос на следующую страницу туров
            toursIsOver: false, //Флаг того, что больше туров нет, показаны все страницы

            /*FastOrder*/
            isShowFastOrder: false,
            fastFormPhone: "",
            fastFormName: "",
            fastFormNameHasError: false,
            fastFormPhoneHasError: false,
            fastFormSended: false,

            debug: false,
            queryParams: [],
            searchUrl: "",
            mobile: false,
            showAdditionalParams: false

        },

        created: function created() {
            var _this = this;

            this.init();
            this.queryParams = getAllUrlParams(window.location.toString());

            //Автостарт поиска
            if (_this.queryParams.start === 'y') {
                setTimeout(function () {
                    _this.startSearch();
                }, 2000);
            }

            //Заполняем параметры из request
            if (this.queryParams.datefrom !== undefined) {
                this.datefrom = this.queryParams.datefrom;
            }
            if (this.queryParams.dateto !== undefined) {
                this.dateto = this.queryParams.dateto;
            }
            if (this.queryParams.nightsfrom !== undefined) {
                this.nightsfrom = this.queryParams.nightsfrom;
            }
            if (this.queryParams.nightsto !== undefined) {
                this.nightsto = this.queryParams.nightsto;
            }
            if (this.queryParams.pricefrom !== undefined) {
                this.pricefrom = this.queryParams.pricefrom;
            }
            if (this.queryParams.priceto !== undefined) {
                this.priceto = this.queryParams.priceto;
            }

            if (this.queryParams.adults !== undefined) {
                this.adults = this.queryParams.adults;
            }
            if (this.queryParams.child !== undefined) {
                this.child = this.queryParams.child;
                if (this.queryParams.childage1 !== undefined) {
                    this.childage1 = this.queryParams.childage1;
                }
                if (this.queryParams.childage2 !== undefined) {
                    this.childage2 = this.queryParams.childage2;
                }
                if (this.queryParams.childage3 !== undefined) {
                    this.childage3 = this.queryParams.childage3;
                }
            }

            // dunno why v-on="resize: func" not working
            if (document.body.clientWidth <= 700) {
                _this.$set(_this, 'mobile', true);
            } else {
                _this.$set(_this, 'mobile', false);
            }
            global.window.addEventListener('resize', function () {
                if (document.body.clientWidth <= 700) {
                    _this.$set(_this, 'mobile', true);
                } else {
                    _this.$set(_this, 'mobile', false);
                }
            });
        },

        methods: {
            init: function init() {
                var _this = this;

                _this.getDepartures();
                _this.getOperators();
                _this.getFlydates();
                _this.getMeals();
            },

            getDepartures: function getDepartures() {
                var _this2 = this;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "departure" } }).then(function (response) {
                    _this2.obDepartures = response.body.lists.departures.departure;
                    _this2.obDepartures = _.sortBy(_this2.obDepartures, ['name']);

                    //Заполняем параметры из request
                    if (_this2.queryParams.departure !== undefined) {
                        _this2.departure = _this2.queryParams.departure;
                        _this2.cityFromName = _.find(_this2.obDepartures, ['id', _this2.queryParams.departure.toString()]) ? _.find(_this2.obDepartures, ['id', _this2.queryParams.departure.toString()]).namefrom : "Уфы";
                    } else if (BX.getCookie('NEWTRAVEL_USER_CITY') !== undefined) {
                        _this2.departure = BX.getCookie('NEWTRAVEL_USER_CITY');
                        _this2.cityFromName = _.find(_this2.obDepartures, ['id', BX.getCookie('NEWTRAVEL_USER_CITY').toString()]) ? _.find(_this2.obDepartures, ['id', BX.getCookie('NEWTRAVEL_USER_CITY').toString()]).namefrom : "Уфы";
                    }

                    //Подтвреждение города
                    // $('.departure_wrap').confirmCity({
                    //     departureModal: $('.cityFrom'),
                    //     departureId: this.departure
                    // });
                });
            },

            getOperators: function getOperators() {
                var _this3 = this;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "operator" } }).then(function (response) {
                    var _this = _this3;
                    _this3.obOperators = response.body.lists.operators.operator;

                    //Заполняем параметры из request
                    if (_this.queryParams.operators !== undefined) {
                        var query = _this3.arrayConvert(_this.queryParams.operators);
                        _this.operators = query.join();
                        _.forEach(query, function (value, key) {
                            _this.operatorSelect(_.find(_this.obOperators, ['id', value]) ? _.find(_this.obOperators, ['id', value]) : "");
                        });
                    }
                });
            },

            getFlydates: function getFlydates() {
                var _this4 = this;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "flydate", flydeparture: this.departure, flycountry: this.country } }).then(function (response) {
                    var darerangeselect = $('#dateRangeSelectContainer');
                    if (response.body.lists.flydates !== null) {
                        _this4.obFlydates = response.body.lists.flydates.flydate;
                    } else {
                        _this4.obFlydates = [];
                    }

                    if (darerangeselect.data('dateRangePicker') !== undefined) {
                        darerangeselect.data('dateRangePicker').destroy();
                    }
                    darerangeselect.dateRangePicker({
                        format: 'DD.MM.YYYY',
                        inline: true,
                        container: '#dateRangeSelectContainer',
                        alwaysOpen: true,
                        startOfWeek: 'monday',
                        singleMonth: 'auto',
                        showShortcuts: false,
                        selectForward: true,
                        stickyMonths: true,
                        separator: '-',
                        minDays: 1,
                        maxDays: 14,
                        beforeShowDay: function beforeShowDay(date) {
                            if ($.inArray(moment(date).format("DD.MM.YYYY"), searchApp.obFlydates) >= 0) {
                                return [true, "in-flydate", ""];
                            } else {
                                return [true, "", ""];
                            }
                        }
                    }).bind('datepicker-change', function (event, obj) {
                        searchApp.datefrom = moment(obj.date1).format("DD.MM.YYYY");
                        searchApp.dateto = moment(obj.date2).format("DD.MM.YYYY");
                        $('#dateRangeSelectContainer').parent().removeClass('show');
                    });
                });
            },

            getMeals: function getMeals() {
                var _this5 = this;

                var _this = this;
                _this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "meal" } }).then(function (response) {
                    _this.obMeals = response.body.lists.meals.meal;

                    //Заполняем параметры из request
                    if (_this.queryParams.meal !== undefined) {
                        var query = _this5.arrayConvert(_this.queryParams.regions);
                        _this.meal = query.join();
                        _.forEach(query, function (value, key) {
                            _this.mealsSelect(_.find(_this.obMeals, ['id', value]) ? _.find(_this.obMeals, ['id', value]) : "");
                        });
                    }
                });
            },

            /*ПОИСКОВЫЕ МЕТОДЫ*/

            //Начало поиска
            startSearch: function startSearch() {
                var _this6 = this;

                var $this = this;
                if (this.debug) {
                    console.log('============= Начинаем поиск =============');
                }

                this.searchInAction = true;
                this.obSearchResults = [];
                this.requestid = '';
                this.pageNumber = 1;
                this.firstRowsIsDisplayed = false;
                this.allRowsCount = 0;
                this.searchProgress = 0;
                this.queryParams = [];
                this.toursNotFound = false;

                var params = {
                    type: "search",
                    departure: this.departure,
                    country: this.country,
                    hotels: this.hotels,
                    meal: this.meal,
                    adults: this.adults,
                    child: this.child,
                    childage1: this.childage1,
                    childage2: this.childage2,
                    childage3: this.childage3,
                    nightsfrom: this.nightsfrom,
                    nightsto: this.nightsto,
                    pricefrom: this.pricefrom,
                    priceto: this.priceto,
                    datefrom: this.datefrom,
                    dateto: this.dateto,
                    operators: this.operators
                };

                $("html, body").animate({ scrollTop: $('#results').offset().top - 100 }, 1000);

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: params }).then(function (response) {
                    if (response.body.result.requestid !== undefined) {
                        _this6.requestid = response.body.result.requestid;
                        _this6.searchUrl = response.body.url;
                        history.pushState(_this6.requestid, "Поиск туров", "?start=Y&" + response.body.url);

                        setTimeout(function () {
                            $this.getStatus();
                        }, 3000);
                    } else {
                        if (_this6.debug) {
                            console.log("Ошибка startSearch");
                        }
                    }
                });
            },

            //Запрос состояние поиска
            getStatus: function getStatus() {
                var _this7 = this;

                if (this.debug) {
                    console.log('=========== Проверяем статус ==========');
                }
                var $this = this;
                var RowsCount = 0; //Общее количество результатов для цикла
                $this.getReaueIteration = 1;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "status", requestid: this.requestid, operatorstatus: 1 } }).then(function (response) {

                    if (response.body.data.status !== undefined) {
                        //Запрос прошел без ошибок

                        $this.searchProgress = response.body.data.status.progress; //Процент выполнения

                        RowsCount = response.body.data.status.hotelsfound;

                        if ($this.allRowsCount !== RowsCount && !$this.firstRowsIsDisplayed) {
                            //Если количество найденных результатов за цикл не равна уже найденным результатам и первая партия еще не отображена, тогда выводим первую партию результатов
                            $this.firstRowsIsDisplayed = true;
                            if (_this7.debug) {
                                console.log("==Выводим первые результаты на экран==");
                            }
                            $this.getResult();
                        }

                        if (parseInt($this.searchProgress) !== 100) {
                            //Повторяем проверку статуса
                            setTimeout(function () {
                                if (this.debug) {
                                    console.log("Не все операторы отработали, повторяем запрос статусов");
                                }
                                $this.getStatus();
                            }, 2000);
                        } else {
                            if (_this7.debug) {
                                console.log("==Все обработно! Урра!!!==");
                                console.log("==Выводим все результаты на экран==");
                            }
                            $this.searchProgress = 100;
                            $this.searchInAction = false;
                            $this.getResult();
                        }

                        $this.allRowsCount = response.body.data.status.hotelsfound; //Общее количество результатов
                    } else {
                        if (_this7.debug) {
                            console.log("Ошибка getStatus");
                        }
                    }
                });
            },

            //Получение результатов поиска
            getResult: function getResult() {
                var _this8 = this;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "result", requestid: this.requestid, page: this.pageNumber } }).then(function (response) {
                    if (response.body.data.status.state === 'finished' && parseInt(response.body.data.status.toursfound) === 0) {
                        _this8.toursNotFound = true;
                    }

                    if (response.body.data.result !== undefined) {
                        if (_this8.debug) {
                            console.log('======= Получите результаты ========');
                        }

                        if (_this8.pageNumber === 1) {
                            _this8.obSearchResults = response.body.data.result.hotel;
                        } else {
                            _this8.obSearchResults = _.concat(_this8.obSearchResults, response.body.data.result.hotel);
                            _this8.nextPageRequestInAction = false;
                        }

                        setTimeout(function () {
                            $('[data-toggle="popover"]').popover({
                                trigger: 'hover | click',
                                container: $('body')
                            });
                        }, 500);
                    } else {
                        if (_this8.pageNumber > 1) {
                            _this8.nextPageRequestInAction = false;
                            _this8.toursIsOver = true;
                        }
                        if (_this8.debug) {
                            console.log("Ошибка getResult");
                        }
                    }
                });
            },

            loadMoreTours: function loadMoreTours() {
                this.nextPageRequestInAction = true;
                this.pageNumber = this.pageNumber + 1;
                this.getResult();
            },

            //Питание для запроса
            mealsSelect: function mealsSelect() {
                var meal = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";

                if (meal !== "") {
                    !_.includes(this.obMealsSelected, meal.id) ? this.obMealsSelected.push(meal.id) : _.pull(this.obMealsSelected, meal.id);
                    !_.includes(this.obMealsName, meal.name) ? this.obMealsName.push(meal.name) : _.pull(this.obMealsName, meal.name);
                }
                this.meal = this.obMealsSelected.join();
            },

            //Туроператоры для запроса
            operatorSelect: function operatorSelect(operator) {
                !_.includes(this.obOperatorsSelected, operator.id) ? this.obOperatorsSelected.push(operator.id) : _.pull(this.obOperatorsSelected, operator.id);
                !_.includes(this.obOperatorsName, operator.name) ? this.obOperatorsName.push(operator.name) : _.pull(this.obOperatorsName, operator.name);
                this.operators = this.obOperatorsSelected.join();
            },

            /*Формировние строки с туристами*/
            setTouristsString: function setTouristsString() {
                var self = this;

                self.touristString = self.adults + " взр. ";
                if (self.child > 0) {
                    self.touristString += self.child + " реб.(";
                    for (var i = 1; i <= self.child; i++) {
                        self.touristString += self['childage' + i];
                        if (i < self.child) {
                            self.touristString += ' и ';
                        }
                    }
                    self.touristString += ")";
                }
            },

            fastOrder: function fastOrder() {
                var _this9 = this;

                this.fastFormSended = false;
                var sessidval = $("#sessid").val(),
                    tourid = $("input[name=fastTourId]").val(),
                    credit = $("input[name=credit]").val();

                this.fastFormPhoneHasError = this.fastFormPhone.length <= 0;
                this.fastFormNameHasError = this.fastFormName.length <= 0;

                if (!this.fastFormPhoneHasError && !this.fastFormNameHasError && tourid.length > 0) {

                    this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {
                        params: {
                            type: "fastorder",
                            tourid: tourid,
                            phone: this.fastFormPhone,
                            name: this.fastFormName,
                            sessid: sessidval,
                            credit: credit
                        }
                    }).then(function (response) {
                        if (response.body.ERROR === "" && response.body.STATUS) {
                            yaCounter24395911.reachGoal('FAST_ORDER_DETAIL_TOUR');
                            _this9.fastFormSended = true;

                            setTimeout(function () {
                                $('#fastOrderModal').modal('hide');
                            }, 1000);
                        }
                    });
                }
            },

            //Комвертируем слова в массив
            arrayConvert: function arrayConvert(query) {
                var arQuery = void 0;

                if (_.isString(query)) {
                    arQuery = [query];
                } else {
                    arQuery = query;
                }
                return arQuery;
            }

        },

        watch: {
            departure: function departure() {
                //this.getCountries();
                this.getFlydates();

                BX.setCookie('NEWTRAVEL_USER_CITY', this.departure, { path: '/' });
            },
            adults: function adults() {
                this.setTouristsString();
            },
            child: function child() {
                this.setTouristsString();
            },
            childage1: function childage1() {
                this.setTouristsString();
            },
            childage2: function childage2() {
                this.setTouristsString();
            },
            childage3: function childage3() {
                this.setTouristsString();
            }
        },

        filters: {
            displayAge: function displayAge(value) {
                if (!value) {
                    return "";
                } else if (parseInt(value) === 1) {
                    return "< 2 лет";
                } else {
                    var titles = ['год', 'года', 'лет'];
                    value = parseInt(value);
                    return value + " " + titles[value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2];
                }
            },

            displayAgeInt: function displayAgeInt(value) {
                if (parseInt(value) >= 2) {
                    return value;
                } else if (parseInt(value) <= 1) {
                    return "1";
                }
            },

            displayNights: function displayNights(value) {
                if (!value) {
                    return "";
                } else {
                    var titles = ['ночь', 'ночи', 'ночей'];
                    value = parseInt(value);
                    return value + " " + titles[value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2];
                }
            },

            shortName: function shortName(value, type) {
                if (value) {
                    if (value.length > 1) {
                        var titles = [];
                        if (type == 'region') {
                            titles = ['курорт', 'курорта', 'курортов'];
                        } else if (type == 'hotel') {
                            titles = ['отель', 'отеля', 'отелей'];
                        } else if (type == 'subregion') {
                            titles = ['поселок', 'поселка', 'поселков'];
                        }

                        value = value.length;
                        return value + " " + titles[value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2];
                    } else if (value.length == 1) {
                        return value.toString();
                    }
                }
            },

            formatPrice: function formatPrice(value) {
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
            }

        },

        components: {
            ComResults: _ComResults2.default,
            ComProgressBar: _ComProgressBar2.default,
            ComDeparture: _ComDeparture2.default
        }

    });

    $('.select-dropdown.no-close').click(function (event) {
        event.stopPropagation();
    });
});

function getAllUrlParams(url) {

    // извлекаем строку из URL или объекта window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // объект для хранения параметров
    var obj = {};

    // если есть строка запроса
    if (queryString) {

        // данные после знака # будут опущены
        queryString = queryString.split('#')[0];

        // разделяем параметры
        var arr = queryString.split('&');

        for (var i = 0; i < arr.length; i++) {
            // разделяем параметр на ключ => значение
            var a = arr[i].split('=');

            // обработка данных вида: list[]=thing1&list[]=thing2
            var paramNum = undefined;
            var paramName = a[0].replace(/\[\d*\]/, function (v) {
                paramNum = v.slice(1, -1);
                return '';
            });

            // передача значения параметра ('true' если значение не задано)
            var paramValue = typeof a[1] === 'undefined' ? true : a[1];

            // преобразование регистра
            paramName = paramName.toLowerCase();
            paramValue = paramValue.toLowerCase();

            // если ключ параметра уже задан
            if (obj[paramName]) {
                // преобразуем текущее значение в массив
                if (typeof obj[paramName] === 'string') {
                    obj[paramName] = [obj[paramName]];
                }
                // если не задан индекс...
                if (typeof paramNum === 'undefined') {
                    // помещаем значение в конец массива
                    obj[paramName].push(paramValue);
                }
                // если индекс задан...
                else {
                        // размещаем элемент по заданному индексу
                        obj[paramName][paramNum] = paramValue;
                    }
            }
            // если параметр не задан, делаем это вручную
            else {
                    obj[paramName] = paramValue;
                }
        }
    }

    return obj;
}
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(5)))

/***/ }),
/* 5 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_ComDeparture_vue__ = __webpack_require__(1);
/* empty harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_5c8a4b6c_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_ComDeparture_vue__ = __webpack_require__(7);
var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_ComDeparture_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_5c8a4b6c_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_ComDeparture_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src/ComDeparture.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5c8a4b6c", Component.options)
  } else {
    hotAPI.reload("data-v-5c8a4b6c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _c("div", { staticClass: "col-12" }, [
      _c("div", { staticClass: "h5" }, [
        _vm._v("Поиск туров в отель из\n\t\t\t"),
        _c("div", { staticClass: "dropdown d-inline-block" }, [
          _c(
            "a",
            {
              staticClass: "text-uppercase display-7 departure_wrap",
              attrs: {
                href: "javascript:void(0)",
                id: "departureValue",
                "data-toggle": "dropdown",
                "aria-haspopup": "true",
                "aria-expanded": "false"
              }
            },
            [_vm._v(_vm._s(_vm.cityFromName))]
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "dropdown-menu select-dropdown bg-light",
              attrs: { "aria-labelledby": "departureValue" }
            },
            [
              _c(
                "div",
                { staticClass: "scrollable-menu" },
                _vm._l(_vm.groupedDepartures, function(item, letter) {
                  return _c(
                    "span",
                    [
                      _c("h6", { staticClass: "dropdown-header" }, [
                        _vm._v(_vm._s(letter))
                      ]),
                      _vm._v(" "),
                      _vm._l(item, function(departureItem) {
                        return _c(
                          "span",
                          {
                            staticClass: "dropdown-item",
                            class: {
                              "bg-primary text-white":
                                departureItem.id == _vm.departure
                            },
                            on: {
                              click: function($event) {
                                return _vm.setDeparture(departureItem)
                              }
                            }
                          },
                          [_vm._v(_vm._s(departureItem.name))]
                        )
                      })
                    ],
                    2
                  )
                }),
                0
              )
            ]
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5c8a4b6c", esExports)
  }
}

/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_ComResults_vue__ = __webpack_require__(2);
/* empty harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_ec9bb524_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_ComResults_vue__ = __webpack_require__(9);
var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_ComResults_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_ec9bb524_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_ComResults_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src/ComResults.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-ec9bb524", Component.options)
  } else {
    hotAPI.reload("data-v-ec9bb524", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("ul", { staticClass: "list-unstyled" }, [
    _vm.resultsData.length > 0
      ? _c(
          "div",
          _vm._l(_vm.resultsData, function(hotel, index) {
            return _c(
              "li",
              { staticClass: "animated fadeInRight mb-2 pt-2 pb-2" },
              [
                _c("div", { staticClass: "card bg-light" }, [
                  _c(
                    "div",
                    {
                      staticClass: "p-3 tours-list bg-light",
                      attrs: { id: "tours-" + hotel.hotelcode }
                    },
                    [
                      _c(
                        "div",
                        { staticClass: "d-block" },
                        _vm._l(hotel.tours.tour, function(tour, key) {
                          return _c(
                            "div",
                            {
                              staticClass:
                                "row d-flex align-items-center bg-light",
                              class: {
                                hiddenTours: key > 2,
                                open: _vm.showHiddenTours
                              }
                            },
                            [
                              _c(
                                "div",
                                { staticClass: "col-12 text-muted mb-3" },
                                [_vm._v(_vm._s(tour.tourname))]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-6 col-md-2" }, [
                                _c("p", [
                                  _c("strong", [_vm._v(_vm._s(tour.flydate))])
                                ]),
                                _vm._v(" "),
                                _c("p", [
                                  _vm._v(_vm._s(tour.nights) + " ночей")
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-6 col-md-3" }, [
                                _c("p", [
                                  _c("strong", [_vm._v(_vm._s(tour.meal))])
                                ]),
                                _vm._v(" "),
                                _c(
                                  "p",
                                  { attrs: { title: hotel.mealrussian } },
                                  [_vm._v(_vm._s(tour.mealrussian))]
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-6 col-md-2" }, [
                                _c("p", [
                                  _c("strong", [_vm._v(_vm._s(tour.room))])
                                ]),
                                _vm._v(" "),
                                _c("p", { attrs: { title: hotel.placement } }, [
                                  _vm._v(_vm._s(tour.placement))
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-6 col-md-2" }, [
                                _c("p", [
                                  _c("strong", [
                                    _vm._v(_vm._s(tour.operatorname))
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("p", [
                                  _c("img", {
                                    attrs: {
                                      src:
                                        "https://tourvisor.ru/pics/operators/searchlogo/" +
                                        tour.operatorcode +
                                        ".gif",
                                      alt: ""
                                    }
                                  })
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-12 col-md-3" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "text-success text-left text-md-right mb-0"
                                  },
                                  [
                                    _vm._v(
                                      "\n\t\t\t\t\t\t\t\t\tза тур\n\t\t\t\t\t\t\t\t\t"
                                    ),
                                    _c(
                                      "span",
                                      {
                                        staticClass: "price h3",
                                        on: {
                                          click: function($event) {
                                            $event.preventDefault()
                                            return _vm.getDetail(tour.tourid)
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(
                                            _vm._f("formatPrice")(tour.price)
                                          )
                                        )
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "text-muted text-left text-md-right"
                                  },
                                  [
                                    _vm._v("\n\t\t\t\t\t\t\t\t\tбез скидки: "),
                                    _c("s", { staticClass: "h5" }, [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("getOldPrice")(tour.price)
                                        )
                                      )
                                    ])
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "text-danger text-left text-md-right mb-2 price",
                                    attrs: {
                                      "data-toggle": "modal",
                                      "data-fast-tour-id": tour.tourid,
                                      "data-info": "credit",
                                      "data-target": "#fastOrderModal"
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\n\t\t\t\t\t\t\t\t\tв кредит 6 мес.: "
                                    ),
                                    _c("span", { staticClass: "h5" }, [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("getCreditPrice")(tour.price)
                                        )
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _vm._m(0, true)
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "col-12 d-flex flex-column flex-md-row justify-content-end text-right"
                                },
                                [
                                  _c(
                                    "a",
                                    {
                                      staticClass:
                                        "btn btn-primary btn-sm ld-ext-left mx-1 mb-1",
                                      class: "detail-btn_" + tour.tourid,
                                      attrs: { href: "#" },
                                      on: {
                                        click: function($event) {
                                          $event.preventDefault()
                                          return _vm.getDetail(tour.tourid)
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n\t\t\t\t\t\t\t\t\tТур подробно\n\t\t\t\t\t\t\t\t\t"
                                      ),
                                      _c("div", {
                                        staticClass: "ld ld-ring ld-spin"
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "a",
                                    {
                                      staticClass:
                                        "btn btn-outline-orange btn-sm mx-1 mb-1",
                                      attrs: {
                                        href: "javascript:void(0)",
                                        "data-toggle": "modal",
                                        "data-fast-tour-id": tour.tourid,
                                        "data-target": "#fastOrderModal"
                                      }
                                    },
                                    [_vm._v("Заказать в 1 клик")]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _vm._m(1, true)
                            ]
                          )
                        }),
                        0
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 text-center" }, [
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-primary",
                            on: { click: _vm.toggleHiddenTours }
                          },
                          [
                            !_vm.showHiddenTours
                              ? _c("span", [
                                  _vm._v("Показать все туры в отель "),
                                  _c("i", { staticClass: "fa fa-chevron-down" })
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.showHiddenTours
                              ? _c("span", [
                                  _vm._v("Скрыть туры "),
                                  _c("i", { staticClass: "fa fa-chevron-up" })
                                ])
                              : _vm._e()
                          ]
                        )
                      ])
                    ]
                  )
                ])
              ]
            )
          }),
          0
        )
      : _c("div", [
          !_vm.toursNotFound
            ? _c(
                "div",
                {
                  staticClass:
                    "col-12 display-6 mt-5 mb-5 text-center text-success"
                },
                [
                  _vm._v(
                    '\n\t\t\tНажмите кнопку "Искать", для начала поиска\n\t\t'
                  )
                ]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.toursNotFound
            ? _c(
                "div",
                {
                  staticClass:
                    "col-12 display-7 mt-5 mb-5 text-center text-danger"
                },
                [
                  _vm._v(
                    '\n\t\t\tУпс! Извините, но по заданным параметрам туров не найдено. Попробуйте изменить парметры поиска и снова нажать кнопку "Найти" '
                  ),
                  _c("br"),
                  _vm._v(
                    "\n\t\t\tЛибо отставьте заявку на подбор тура для наших менеджеров.\n\t\t"
                  )
                ]
              )
            : _vm._e()
        ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("small", [_vm._v("(стоимость без первоначального взноса)")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12" }, [_c("hr")])
  }
]
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-ec9bb524", esExports)
  }
}

/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_ComProgressBar_vue__ = __webpack_require__(3);
/* empty harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_5b97981e_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_ComProgressBar_vue__ = __webpack_require__(11);
var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_ComProgressBar_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_5b97981e_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_ComProgressBar_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src/ComProgressBar.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5b97981e", Component.options)
  } else {
    hotAPI.reload("data-v-5b97981e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.searchInAction
    ? _c("div", { staticClass: "progress mt-3 mb-3" }, [
        _c(
          "div",
          {
            staticClass:
              "progress-bar progress-bar-striped progress-bar-animated bg-success d-flex justify-content-between",
            style: { width: _vm.searchProgress + "%" },
            attrs: {
              role: "progressbar",
              "aria-valuenow": _vm.searchProgress,
              "aria-valuemin": "0",
              "aria-valuemax": "100"
            }
          },
          [
            _c("span", { staticClass: "progress-type pl-2" }, [
              _vm._v("Поиск... Найдено туров: " + _vm._s(_vm.allRowsCount))
            ]),
            _vm._v(" "),
            _c("span", { staticClass: "progress-completed pr-2" }, [
              _vm._v(_vm._s(_vm.searchProgress) + "%")
            ])
          ]
        )
      ])
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5b97981e", esExports)
  }
}

/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3dlYnBhY2svYm9vdHN0cmFwIGM4OTZjMDdjODk2NzgyYThlZjRkIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcyIsIndlYnBhY2s6Ly8vQ29tRGVwYXJ0dXJlLnZ1ZSIsIndlYnBhY2s6Ly8vQ29tUmVzdWx0cy52dWUiLCJ3ZWJwYWNrOi8vL0NvbVByb2dyZXNzQmFyLnZ1ZSIsIndlYnBhY2s6Ly8vc3JjL3NlYXJjaC1mb3JtLmpzIiwid2VicGFjazovLy8od2VicGFjaykvYnVpbGRpbi9nbG9iYWwuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL0NvbURlcGFydHVyZS52dWUiLCJ3ZWJwYWNrOi8vLy4vc3JjL0NvbURlcGFydHVyZS52dWU/MTI3MiIsIndlYnBhY2s6Ly8vLi9zcmMvQ29tUmVzdWx0cy52dWUiLCJ3ZWJwYWNrOi8vLy4vc3JjL0NvbVJlc3VsdHMudnVlPzI4ZWMiLCJ3ZWJwYWNrOi8vLy4vc3JjL0NvbVByb2dyZXNzQmFyLnZ1ZSIsIndlYnBhY2s6Ly8vLi9zcmMvQ29tUHJvZ3Jlc3NCYXIudnVlPzFlMGEiXSwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgYzg5NmMwN2M4OTY3ODJhOGVmNGQiLCIvKiBnbG9iYWxzIF9fVlVFX1NTUl9DT05URVhUX18gKi9cblxuLy8gSU1QT1JUQU5UOiBEbyBOT1QgdXNlIEVTMjAxNSBmZWF0dXJlcyBpbiB0aGlzIGZpbGUuXG4vLyBUaGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGUuXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgZnVuY3Rpb25hbFRlbXBsYXRlLFxuICBpbmplY3RTdHlsZXMsXG4gIHNjb3BlSWQsXG4gIG1vZHVsZUlkZW50aWZpZXIgLyogc2VydmVyIG9ubHkgKi9cbikge1xuICB2YXIgZXNNb2R1bGVcbiAgdmFyIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyB8fCB7fVxuXG4gIC8vIEVTNiBtb2R1bGVzIGludGVyb3BcbiAgdmFyIHR5cGUgPSB0eXBlb2YgcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIGlmICh0eXBlID09PSAnb2JqZWN0JyB8fCB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgZXNNb2R1bGUgPSByYXdTY3JpcHRFeHBvcnRzXG4gICAgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICB9XG5cbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChjb21waWxlZFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5yZW5kZXIgPSBjb21waWxlZFRlbXBsYXRlLnJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gY29tcGlsZWRUZW1wbGF0ZS5zdGF0aWNSZW5kZXJGbnNcbiAgICBvcHRpb25zLl9jb21waWxlZCA9IHRydWVcbiAgfVxuXG4gIC8vIGZ1bmN0aW9uYWwgdGVtcGxhdGVcbiAgaWYgKGZ1bmN0aW9uYWxUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMuZnVuY3Rpb25hbCA9IHRydWVcbiAgfVxuXG4gIC8vIHNjb3BlZElkXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9IHNjb3BlSWRcbiAgfVxuXG4gIHZhciBob29rXG4gIGlmIChtb2R1bGVJZGVudGlmaWVyKSB7IC8vIHNlcnZlciBidWlsZFxuICAgIGhvb2sgPSBmdW5jdGlvbiAoY29udGV4dCkge1xuICAgICAgLy8gMi4zIGluamVjdGlvblxuICAgICAgY29udGV4dCA9XG4gICAgICAgIGNvbnRleHQgfHwgLy8gY2FjaGVkIGNhbGxcbiAgICAgICAgKHRoaXMuJHZub2RlICYmIHRoaXMuJHZub2RlLnNzckNvbnRleHQpIHx8IC8vIHN0YXRlZnVsXG4gICAgICAgICh0aGlzLnBhcmVudCAmJiB0aGlzLnBhcmVudC4kdm5vZGUgJiYgdGhpcy5wYXJlbnQuJHZub2RlLnNzckNvbnRleHQpIC8vIGZ1bmN0aW9uYWxcbiAgICAgIC8vIDIuMiB3aXRoIHJ1bkluTmV3Q29udGV4dDogdHJ1ZVxuICAgICAgaWYgKCFjb250ZXh0ICYmIHR5cGVvZiBfX1ZVRV9TU1JfQ09OVEVYVF9fICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICBjb250ZXh0ID0gX19WVUVfU1NSX0NPTlRFWFRfX1xuICAgICAgfVxuICAgICAgLy8gaW5qZWN0IGNvbXBvbmVudCBzdHlsZXNcbiAgICAgIGlmIChpbmplY3RTdHlsZXMpIHtcbiAgICAgICAgaW5qZWN0U3R5bGVzLmNhbGwodGhpcywgY29udGV4dClcbiAgICAgIH1cbiAgICAgIC8vIHJlZ2lzdGVyIGNvbXBvbmVudCBtb2R1bGUgaWRlbnRpZmllciBmb3IgYXN5bmMgY2h1bmsgaW5mZXJyZW5jZVxuICAgICAgaWYgKGNvbnRleHQgJiYgY29udGV4dC5fcmVnaXN0ZXJlZENvbXBvbmVudHMpIHtcbiAgICAgICAgY29udGV4dC5fcmVnaXN0ZXJlZENvbXBvbmVudHMuYWRkKG1vZHVsZUlkZW50aWZpZXIpXG4gICAgICB9XG4gICAgfVxuICAgIC8vIHVzZWQgYnkgc3NyIGluIGNhc2UgY29tcG9uZW50IGlzIGNhY2hlZCBhbmQgYmVmb3JlQ3JlYXRlXG4gICAgLy8gbmV2ZXIgZ2V0cyBjYWxsZWRcbiAgICBvcHRpb25zLl9zc3JSZWdpc3RlciA9IGhvb2tcbiAgfSBlbHNlIGlmIChpbmplY3RTdHlsZXMpIHtcbiAgICBob29rID0gaW5qZWN0U3R5bGVzXG4gIH1cblxuICBpZiAoaG9vaykge1xuICAgIHZhciBmdW5jdGlvbmFsID0gb3B0aW9ucy5mdW5jdGlvbmFsXG4gICAgdmFyIGV4aXN0aW5nID0gZnVuY3Rpb25hbFxuICAgICAgPyBvcHRpb25zLnJlbmRlclxuICAgICAgOiBvcHRpb25zLmJlZm9yZUNyZWF0ZVxuXG4gICAgaWYgKCFmdW5jdGlvbmFsKSB7XG4gICAgICAvLyBpbmplY3QgY29tcG9uZW50IHJlZ2lzdHJhdGlvbiBhcyBiZWZvcmVDcmVhdGUgaG9va1xuICAgICAgb3B0aW9ucy5iZWZvcmVDcmVhdGUgPSBleGlzdGluZ1xuICAgICAgICA/IFtdLmNvbmNhdChleGlzdGluZywgaG9vaylcbiAgICAgICAgOiBbaG9va11cbiAgICB9IGVsc2Uge1xuICAgICAgLy8gZm9yIHRlbXBsYXRlLW9ubHkgaG90LXJlbG9hZCBiZWNhdXNlIGluIHRoYXQgY2FzZSB0aGUgcmVuZGVyIGZuIGRvZXNuJ3RcbiAgICAgIC8vIGdvIHRocm91Z2ggdGhlIG5vcm1hbGl6ZXJcbiAgICAgIG9wdGlvbnMuX2luamVjdFN0eWxlcyA9IGhvb2tcbiAgICAgIC8vIHJlZ2lzdGVyIGZvciBmdW5jdGlvYWwgY29tcG9uZW50IGluIHZ1ZSBmaWxlXG4gICAgICBvcHRpb25zLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcldpdGhTdHlsZUluamVjdGlvbiAoaCwgY29udGV4dCkge1xuICAgICAgICBob29rLmNhbGwoY29udGV4dClcbiAgICAgICAgcmV0dXJuIGV4aXN0aW5nKGgsIGNvbnRleHQpXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBlc01vZHVsZTogZXNNb2R1bGUsXG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAwXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIjx0ZW1wbGF0ZT5cclxuXHQ8ZGl2IGNsYXNzPVwicm93XCI+XHJcblx0XHQ8ZGl2IGNsYXNzPVwiY29sLTEyXCI+XHJcblx0XHRcdDxkaXYgY2xhc3M9XCJoNVwiPtCf0L7QuNGB0Log0YLRg9GA0L7QsiDQsiDQvtGC0LXQu9GMINC40LdcclxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZHJvcGRvd24gZC1pbmxpbmUtYmxvY2tcIj5cclxuXHRcdFx0XHRcdDxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cInRleHQtdXBwZXJjYXNlIGRpc3BsYXktNyBkZXBhcnR1cmVfd3JhcFwiIGlkPVwiZGVwYXJ0dXJlVmFsdWVcIiBkYXRhLXRvZ2dsZT1cImRyb3Bkb3duXCIgYXJpYS1oYXNwb3B1cD1cInRydWVcIiBhcmlhLWV4cGFuZGVkPVwiZmFsc2VcIj57e2NpdHlGcm9tTmFtZX19PC9hPlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cImRyb3Bkb3duLW1lbnUgc2VsZWN0LWRyb3Bkb3duIGJnLWxpZ2h0XCIgYXJpYS1sYWJlbGxlZGJ5PVwiZGVwYXJ0dXJlVmFsdWVcIj5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cInNjcm9sbGFibGUtbWVudVwiPlxyXG5cdFx0XHRcdFx0XHRcdDxzcGFuIHYtZm9yPVwiKGl0ZW0sIGxldHRlcikgaW4gZ3JvdXBlZERlcGFydHVyZXNcIj5cclxuXHRcdFx0XHRcdFx0XHRcdDxoNiBjbGFzcz1cImRyb3Bkb3duLWhlYWRlclwiPnt7IGxldHRlcn19PC9oNj5cclxuXHRcdFx0XHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwiZHJvcGRvd24taXRlbVwiIDpjbGFzcz1cInsnYmctcHJpbWFyeSB0ZXh0LXdoaXRlJyA6IGRlcGFydHVyZUl0ZW0uaWQgPT0gZGVwYXJ0dXJlIH1cIiAgdi1mb3I9XCJkZXBhcnR1cmVJdGVtIGluIGl0ZW1cIiBAY2xpY2s9XCJzZXREZXBhcnR1cmUoZGVwYXJ0dXJlSXRlbSlcIj57e2RlcGFydHVyZUl0ZW0ubmFtZX19PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdDwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQ8L2Rpdj5cclxuXHQ8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcblxyXG48c2NyaXB0PlxyXG4gICAgZXhwb3J0IGRlZmF1bHQge1xyXG4gICAgICAgIG5hbWU6ICdjb20tZGVwYXJ0dXJlJyxcclxuICAgICAgICBwcm9wczogWydkZXBhcnR1cmVzRGF0YScsICdkZXBhcnR1cmUnXSxcclxuICAgICAgICBkYXRhKCkge1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgY2l0eUZyb21OYW1lOiBcItCj0YTRi1wiLFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgICAgIC8v0KPRgdGC0LDQvdCw0LLQu9C40LLQsNC10LwgSUQg0LPQvtGA0L7QtNCwXHJcbiAgICAgICAgICAgIHNldERlcGFydHVyZTogZnVuY3Rpb24gKGRlcGFydHVyZUl0ZW0pIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2l0eUZyb21OYW1lID0gZGVwYXJ0dXJlSXRlbS5uYW1lZnJvbTtcclxuICAgICAgICAgICAgICAgIHRoaXMuJHJvb3QuJGRhdGEuZGVwYXJ0dXJlID0gZGVwYXJ0dXJlSXRlbS5pZDtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgICAgIC8v0JPRgNGD0L/Qv9C40YDRg9C10Lwg0LPQvtGA0L7QtNCwINC/0L4g0LHRg9C60LLQsNC8XHJcbiAgICAgICAgICAgIGdyb3VwZWREZXBhcnR1cmVzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gXy5ncm91cEJ5KHRoaXMuZGVwYXJ0dXJlc0RhdGEsIGZ1bmN0aW9uIChzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHMubmFtZS5jaGFyQXQoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgd2F0Y2g6IHtcclxuICAgICAgICAgICAgZGVwYXJ0dXJlOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNpdHlGcm9tTmFtZSA9IF8uZmluZCh0aGlzLmRlcGFydHVyZXNEYXRhLCBbJ2lkJywgdGhpcy5kZXBhcnR1cmUudG9TdHJpbmcoKV0pID8gXy5maW5kKHRoaXMuZGVwYXJ0dXJlc0RhdGEsIFsnaWQnLCB0aGlzLmRlcGFydHVyZS50b1N0cmluZygpXSkubmFtZWZyb20gOiB0aGlzLmNpdHlGcm9tTmFtZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIENvbURlcGFydHVyZS52dWUiLCI8dGVtcGxhdGU+XHJcblx0PHVsIGNsYXNzPVwibGlzdC11bnN0eWxlZFwiPlxyXG5cdFx0PGRpdiB2LWlmPVwicmVzdWx0c0RhdGEubGVuZ3RoID4gMFwiPlxyXG5cdFx0XHQ8bGkgdi1mb3I9XCIoaG90ZWwsIGluZGV4KSBpbiByZXN1bHRzRGF0YVwiIGNsYXNzPVwiYW5pbWF0ZWQgZmFkZUluUmlnaHQgbWItMiBwdC0yIHBiLTJcIj5cclxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY2FyZCBiZy1saWdodFwiPlxyXG5cclxuXHRcdFx0XHRcdDwhLS3QodCf0JjQodCe0Jog0KLQo9Cg0J7QkiDQkiDQntCi0JXQm9CsLS0+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwicC0zIHRvdXJzLWxpc3QgYmctbGlnaHRcIiB2LWJpbmQ6aWQ9XCIndG91cnMtJyArIGhvdGVsLmhvdGVsY29kZVwiPlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZC1ibG9ja1wiPlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJyb3cgZC1mbGV4IGFsaWduLWl0ZW1zLWNlbnRlciBiZy1saWdodFwiIDpjbGFzcz1cInsnaGlkZGVuVG91cnMnIDoga2V5ID4gMiwgJ29wZW4nIDogc2hvd0hpZGRlblRvdXJzfVwiIHYtZm9yPVwiKHRvdXIsIGtleSkgaW4gaG90ZWwudG91cnMudG91clwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC0xMiB0ZXh0LW11dGVkIG1iLTNcIj57e3RvdXIudG91cm5hbWV9fTwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC02IGNvbC1tZC0yXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxwPjxzdHJvbmc+e3t0b3VyLmZseWRhdGV9fTwvc3Ryb25nPjwvcD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHA+e3t0b3VyLm5pZ2h0c319INC90L7Rh9C10Lk8L3A+XHJcblx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjb2wtNiBjb2wtbWQtM1wiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8cD48c3Ryb25nPnt7dG91ci5tZWFsfX08L3N0cm9uZz48L3A+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxwIDp0aXRsZT1cImhvdGVsLm1lYWxydXNzaWFuXCI+e3t0b3VyLm1lYWxydXNzaWFufX08L3A+XHJcblx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjb2wtNiBjb2wtbWQtMlwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8cD48c3Ryb25nPnt7dG91ci5yb29tfX08L3N0cm9uZz48L3A+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxwIDp0aXRsZT1cImhvdGVsLnBsYWNlbWVudFwiPnt7dG91ci5wbGFjZW1lbnR9fTwvcD5cclxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC02IGNvbC1tZC0yXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxwPjxzdHJvbmc+e3t0b3VyLm9wZXJhdG9ybmFtZX19PC9zdHJvbmc+PC9wPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8cD48aW1nIHYtYmluZDpzcmM9XCInaHR0cHM6Ly90b3Vydmlzb3IucnUvcGljcy9vcGVyYXRvcnMvc2VhcmNobG9nby8nK3RvdXIub3BlcmF0b3Jjb2RlKycuZ2lmJ1wiIGFsdD1cIlwiPjwvcD5cclxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC0xMiBjb2wtbWQtM1wiPlxyXG5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PCEtLSDQptCV0J3QqyAtLT5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cInRleHQtc3VjY2VzcyB0ZXh0LWxlZnQgdGV4dC1tZC1yaWdodCBtYi0wXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx00LfQsCDRgtGD0YBcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8c3BhbiBjbGFzcz1cInByaWNlIGgzXCIgQGNsaWNrLnByZXZlbnQ9XCJnZXREZXRhaWwodG91ci50b3VyaWQpXCI+e3t0b3VyLnByaWNlIHwgZm9ybWF0UHJpY2V9fTwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJ0ZXh0LW11dGVkIHRleHQtbGVmdCB0ZXh0LW1kLXJpZ2h0XCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx00LHQtdC3INGB0LrQuNC00LrQuDogPHMgY2xhc3M9XCJoNVwiPnt7dG91ci5wcmljZSB8IGdldE9sZFByaWNlfX08L3M+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwidGV4dC1kYW5nZXIgdGV4dC1sZWZ0IHRleHQtbWQtcmlnaHQgbWItMiBwcmljZVwiICBkYXRhLXRvZ2dsZT1cIm1vZGFsXCIgOmRhdGEtZmFzdC10b3VyLWlkPVwidG91ci50b3VyaWRcIiBkYXRhLWluZm89XCJjcmVkaXRcIiBkYXRhLXRhcmdldD1cIiNmYXN0T3JkZXJNb2RhbFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdNCyINC60YDQtdC00LjRgiA2INC80LXRgS46IDxzcGFuIGNsYXNzPVwiaDVcIj57e3RvdXIucHJpY2UgfCBnZXRDcmVkaXRQcmljZX19PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxwPjxzbWFsbD4o0YHRgtC+0LjQvNC+0YHRgtGMINCx0LXQtyDQv9C10YDQstC+0L3QsNGH0LDQu9GM0L3QvtCz0L4g0LLQt9C90L7RgdCwKTwvc21hbGw+PC9wPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cclxuXHRcdFx0XHRcdFx0XHRcdDwhLS0g0JrQndCe0J/QmtCYIC0tPlxyXG5cdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC0xMiBkLWZsZXggZmxleC1jb2x1bW4gZmxleC1tZC1yb3cganVzdGlmeS1jb250ZW50LWVuZCB0ZXh0LXJpZ2h0XCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxhIGhyZWY9XCIjXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgYnRuLXNtIGxkLWV4dC1sZWZ0IG14LTEgbWItMVwiIDpjbGFzcz1cIidkZXRhaWwtYnRuXycrdG91ci50b3VyaWRcIiBAY2xpY2sucHJldmVudD1cImdldERldGFpbCh0b3VyLnRvdXJpZClcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHTQotGD0YAg0L/QvtC00YDQvtCx0L3QvlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJsZCBsZC1yaW5nIGxkLXNwaW5cIj48L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC9hPlxyXG5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PCEtLSDQntCi0JrQm9Cu0KfQkNCV0Jwg0JTQntCR0JDQktCb0JXQndCY0JUg0JIg0JrQntCg0JfQmNCd0KMgLS0+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwhLS0gYSBjbGFzcz1cImJ0biBidG4tb3V0bGluZS1wcmltYXJ5IGJ0bi1zbSBteC0xIG1iLTFcIiBocmVmPVwiI1wiIEBjbGljay5wcmV2ZW50PVwiYWRkVG91cjJCYXNrZXQodG91ci50b3VyaWQsICRldmVudClcIj7QkiDQutC+0YDQt9C40L3RgzwvYSAtLT5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGEgY2xhc3M9XCJidG4gYnRuLW91dGxpbmUtb3JhbmdlIGJ0bi1zbSBteC0xIG1iLTFcIiBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgZGF0YS10b2dnbGU9XCJtb2RhbFwiIDpkYXRhLWZhc3QtdG91ci1pZD1cInRvdXIudG91cmlkXCIgZGF0YS10YXJnZXQ9XCIjZmFzdE9yZGVyTW9kYWxcIj7Ql9Cw0LrQsNC30LDRgtGMINCyIDEg0LrQu9C40Lo8L2E+XHJcblx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHJcblx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLTEyXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxocj5cclxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC0xMiB0ZXh0LWNlbnRlclwiPlxyXG5cdFx0XHRcdFx0XHRcdDxidXR0b24gY2xhc3M9XCJidG4gYnRuLXByaW1hcnlcIiBAY2xpY2s9XCJ0b2dnbGVIaWRkZW5Ub3Vyc1wiPlxyXG5cdFx0XHRcdFx0XHRcdFx0PHNwYW4gdi1pZj1cIiFzaG93SGlkZGVuVG91cnNcIj7Qn9C+0LrQsNC30LDRgtGMINCy0YHQtSDRgtGD0YDRiyDQsiDQvtGC0LXQu9GMIDxpIGNsYXNzPVwiZmEgZmEtY2hldnJvbi1kb3duXCI+PC9pPjwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHRcdDxzcGFuIHYtaWY9XCJzaG93SGlkZGVuVG91cnNcIj7QodC60YDRi9GC0Ywg0YLRg9GA0YsgPGkgY2xhc3M9XCJmYSBmYS1jaGV2cm9uLXVwXCI+PC9pPjwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHQ8L2J1dHRvbj5cclxuXHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHJcblx0XHRcdDwvbGk+XHJcblx0XHQ8L2Rpdj5cclxuXHRcdDxkaXYgdi1lbHNlPlxyXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLTEyIGRpc3BsYXktNiBtdC01IG1iLTUgdGV4dC1jZW50ZXIgdGV4dC1zdWNjZXNzXCIgdi1pZj1cIiF0b3Vyc05vdEZvdW5kXCI+XHJcblx0XHRcdFx00J3QsNC20LzQuNGC0LUg0LrQvdC+0L/QutGDIFwi0JjRgdC60LDRgtGMXCIsINC00LvRjyDQvdCw0YfQsNC70LAg0L/QvtC40YHQutCwXHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLTEyIGRpc3BsYXktNyBtdC01IG1iLTUgdGV4dC1jZW50ZXIgdGV4dC1kYW5nZXJcIiB2LWlmPVwidG91cnNOb3RGb3VuZFwiPlxyXG5cdFx0XHRcdNCj0L/RgSEg0JjQt9Cy0LjQvdC40YLQtSwg0L3QviDQv9C+INC30LDQtNCw0L3QvdGL0Lwg0L/QsNGA0LDQvNC10YLRgNCw0Lwg0YLRg9GA0L7QsiDQvdC1INC90LDQudC00LXQvdC+LiDQn9C+0L/RgNC+0LHRg9C50YLQtSDQuNC30LzQtdC90LjRgtGMINC/0LDRgNC80LXRgtGA0Ysg0L/QvtC40YHQutCwINC4INGB0L3QvtCy0LAg0L3QsNC20LDRgtGMINC60L3QvtC/0LrRgyBcItCd0LDQudGC0LhcIiA8YnI+XHJcblx0XHRcdFx00JvQuNCx0L4g0L7RgtGB0YLQsNCy0YzRgtC1INC30LDRj9Cy0LrRgyDQvdCwINC/0L7QtNCx0L7RgCDRgtGD0YDQsCDQtNC70Y8g0L3QsNGI0LjRhSDQvNC10L3QtdC00LbQtdGA0L7Qsi5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQ8L2Rpdj5cclxuXHQ8L3VsPlxyXG48L3RlbXBsYXRlPlxyXG5cclxuXHJcbjxzY3JpcHQ+XHJcbiAgICBleHBvcnQgZGVmYXVsdCB7XHJcbiAgICAgICAgbmFtZTogJ2NvbS1yZXN1bHRzJyxcclxuICAgICAgICBwcm9wczogWydyZXN1bHRzRGF0YScsICdzZWFyY2hVcmwnLCAndG91cnNOb3RGb3VuZCddLFxyXG4gICAgICAgIGRhdGEoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICBzaG93SGlkZGVuVG91cnM6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgb2JIb3RlbHNEZXNjcjoge30sXHJcbiAgICAgICAgICAgICAgICBvYkJhbm5lcnM6IHtcclxuLy8gICAgICAgICAgICAgICAgICAgIDA6IHtcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcItCR0LDQvdC90LXRgCAxXCIsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgICAgaW1nOiBcIlwiLFxyXG4vLyAgICAgICAgICAgICAgICAgICAgfSxcclxuLy8gICAgICAgICAgICAgICAgICAgIDE6IHtcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcItCR0LDQvdC90LXRgCAyXCIsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgICAgaW1nOiBcIlwiLFxyXG4vLyAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIG1ldGhvZHM6IHtcclxuXHJcbiAgICAgICAgICAgIC8v0JTQtdGC0LDQu9GM0L3QsNGPINCw0LrRgtGD0LDQu9C40LfQsNGG0LjRjywg0LLQt9GL0LLQsNC10YIg0LzQvtC00LDQu9GM0L3QvtC1INC+0LrQvdC+XHJcbiAgICAgICAgICAgIGdldERldGFpbDogZnVuY3Rpb24gKHRvdXJpZCkge1xyXG4gICAgICAgICAgICAgICAgdmFyIGRldGFpbFBhcmFtcyA9IHtcclxuICAgICAgICAgICAgICAgICAgICB0b3VyaWQ6IHRvdXJpZCxcclxuICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgJCgnLmRldGFpbC1idG5fJyArIHRvdXJpZCkuYWRkQ2xhc3MoJ3J1bm5pbmcnKTtcclxuXHJcbiAgICAgICAgICAgICAgICB2YXIgbXlNb2RhbCA9IG5ldyBqQm94KCdNb2RhbCcsIHtcclxuICAgICAgICAgICAgICAgICAgICBvbkNsb3NlOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5qQm94LU1vZGFsJykucmVtb3ZlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vJCgnLmpCb3gtb3ZlcmxheScpLnJlbW92ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcuZGV0YWlsLWJ0bl8nICsgdG91cmlkKS5yZW1vdmVDbGFzcygncnVubmluZycpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICBteU1vZGFsLm9wZW4oe1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNpdmVXaWR0aDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICByZXNwb25zaXZlSGVpZ2h0OiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiBcIjk1MFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogXCI5MDBcIixcclxuICAgICAgICAgICAgICAgICAgICBjbG9zZUJ1dHRvbjogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBibG9ja1Njcm9sbDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBhamF4OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVybDogJy9sb2NhbC9jb21wb25lbnRzL25ld3RyYXZlbC5zZWFyY2gvZnVsbC5zZWFyY2gudjIvdGVtcGxhdGVzLy5kZWZhdWx0L2RldGFpbC5waHAnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiBkZXRhaWxQYXJhbXMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlbG9hZDogJ3N0cmljdCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNwaW5uZXI6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuXHQgICAgICAgIHRvZ2dsZUhpZGRlblRvdXJzIDpmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNob3dIaWRkZW5Ub3VycyA9ICF0aGlzLnNob3dIaWRkZW5Ub3VycztcclxuICAgICAgICAgICAgICAgIGlmKCF0aGlzLnNob3dIaWRkZW5Ub3VycylcclxuICAgICAgICAgICAgICAgICQoJ2h0bWwsIGJvZHknKS5hbmltYXRlKHtzY3JvbGxUb3A6ICQoJyNyZXN1bHRzJykub2Zmc2V0KCkudG9wfSwgMTAwKTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIC8v0JTQvtCx0LDQstC70LXQvdC40LUg0YLRg9GA0LAg0LIg0LrQvtGA0LfQuNC90YNcclxuICAgICAgICAgICAgYWRkVG91cjJCYXNrZXQ6IGZ1bmN0aW9uICh0b3VyaWQsIGV2ZW50KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLiRodHRwLmdldCgnL2xvY2FsL21vZHVsZXMvbmV3dHJhdmVsLnNlYXJjaC9saWIvYWpheC5waHAnLCB7cGFyYW1zOiB7dHlwZTogXCJhZGRUb1RvdXJzQmFza2V0XCIsIHRvdXJpZDogdG91cmlkfX0pLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmJvZHkgPT09ICdzdWNjZXNzJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKGV2ZW50LnRhcmdldCkudGV4dCgn0JTQvtCx0LDQstC70LXQvdC+Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh3aW5kb3cudGJzICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy50YnMuZ2V0Q291bnQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICB9LFxyXG4gICAgICAgIGZpbHRlcnM6IHtcclxuICAgICAgICAgICAgZm9ybWF0UHJpY2U6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHZhbHVlLnRvU3RyaW5nKCkucmVwbGFjZSgvXFxCKD89KFxcZHszfSkrKD8hXFxkKSkvZywgXCIgXCIpICsgJyDQoCc7XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBnZXRPbGRQcmljZTogZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICB2YWx1ZSA9IHBhcnNlSW50KHZhbHVlKSArIHBhcnNlSW50KHZhbHVlICogMC4xMSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gTWF0aC5yb3VuZCh2YWx1ZSkudG9TdHJpbmcoKS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIiBcIikgKyAnINCgJztcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGdldENyZWRpdFByaWNlOiBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHZhbHVlID0gKHBhcnNlSW50KHZhbHVlKSArIHBhcnNlSW50KHZhbHVlICogMC4wNzE3KSkgLyA2O1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIE1hdGgucm91bmQodmFsdWUpLnRvU3RyaW5nKCkucmVwbGFjZSgvXFxCKD89KFxcZHszfSkrKD8hXFxkKSkvZywgXCIgXCIpICsgJyDQoCc7XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG5cclxuXHJcbiAgICAgICAgICAgIHNob3J0VGV4dDogZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgc2xpY2VkID0gdmFsdWUuc2xpY2UoMCwgMTAwKTtcclxuICAgICAgICAgICAgICAgIGlmIChzbGljZWQubGVuZ3RoIDwgdmFsdWUubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2xpY2VkICs9ICcuLi4nO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiBzbGljZWQ7XHJcblxyXG4gICAgICAgICAgICB9LFxyXG5cclxuXHJcbiAgICAgICAgICAgIGRpc3BsYXlOaWdodHM6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCF2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgdGl0bGVzID0gWyfQvdC+0YfRjCcsICfQvdC+0YfQuCcsICfQvdC+0YfQtdC5J107XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSBwYXJzZUludCh2YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHZhbHVlICsgXCIgXCIgKyB0aXRsZXNbKHZhbHVlICUgMTAgPT0gMSAmJiB2YWx1ZSAlIDEwMCAhPSAxMSA/IDAgOiB2YWx1ZSAlIDEwID49IDIgJiYgdmFsdWUgJSAxMCA8PSA0ICYmICh2YWx1ZSAlIDEwMCA8IDEwIHx8IHZhbHVlICUgMTAwID49IDIwKSA/IDEgOiAyKV07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gQ29tUmVzdWx0cy52dWUiLCI8dGVtcGxhdGU+XHJcblx0PGRpdiBjbGFzcz1cInByb2dyZXNzIG10LTMgbWItM1wiIHYtaWY9XCJzZWFyY2hJbkFjdGlvblwiPlxyXG5cdFx0PGRpdiBjbGFzcz1cInByb2dyZXNzLWJhciBwcm9ncmVzcy1iYXItc3RyaXBlZCBwcm9ncmVzcy1iYXItYW5pbWF0ZWQgYmctc3VjY2VzcyBkLWZsZXgganVzdGlmeS1jb250ZW50LWJldHdlZW5cIiByb2xlPVwicHJvZ3Jlc3NiYXJcIiA6c3R5bGU9XCJ7J3dpZHRoJyA6IHNlYXJjaFByb2dyZXNzICsgJyUnfVwiIDphcmlhLXZhbHVlbm93PVwic2VhcmNoUHJvZ3Jlc3NcIiBhcmlhLXZhbHVlbWluPVwiMFwiIGFyaWEtdmFsdWVtYXg9XCIxMDBcIj5cclxuXHRcdFx0PHNwYW4gY2xhc3M9XCJwcm9ncmVzcy10eXBlIHBsLTJcIj7Qn9C+0LjRgdC6Li4uINCd0LDQudC00LXQvdC+INGC0YPRgNC+0LI6IHt7YWxsUm93c0NvdW50fX08L3NwYW4+XHJcblx0XHRcdDxzcGFuIGNsYXNzPVwicHJvZ3Jlc3MtY29tcGxldGVkIHByLTJcIj57e3NlYXJjaFByb2dyZXNzfX0lPC9zcGFuPlxyXG5cdFx0PC9kaXY+XHJcblx0PC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG5cclxuPHNjcmlwdD5cclxuICAgIGV4cG9ydCBkZWZhdWx0IHtcclxuICAgICAgICBuYW1lOiAnY29tLXByb2dyZXNzLWJhcicsXHJcbiAgICAgICAgcHJvcHM6IFsnc2VhcmNoSW5BY3Rpb24nLCAnc2VhcmNoUHJvZ3Jlc3MnLCAnYWxsUm93c0NvdW50J10sXHJcbiAgICB9XHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBDb21Qcm9ncmVzc0Jhci52dWUiLCIndXNlIHN0cmljdCc7XHJcblxyXG5pbXBvcnQgQ29tRGVwYXJ0dXJlIGZyb20gJy4vQ29tRGVwYXJ0dXJlLnZ1ZSdcclxuaW1wb3J0IENvbVJlc3VsdHMgZnJvbSAnLi9Db21SZXN1bHRzLnZ1ZSdcclxuaW1wb3J0IENvbVByb2dyZXNzQmFyIGZyb20gJy4vQ29tUHJvZ3Jlc3NCYXIudnVlJ1xyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xyXG5cclxuXHJcbiAgICB3aW5kb3cuc2VhcmNoQXBwID0gbmV3IFZ1ZSh7XHJcbiAgICAgICAgZWw6ICcjc2VhcmNoJyxcclxuICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgIC8q0KLQtdC60YPRidC40LUg0YHQstC+0LnRgdGC0LLQsCovXHJcbiAgICAgICAgICAgIGRlcGFydHVyZTogXCI0XCIsIC8v0KLQtdC60YPRidC40Lkg0LPQvtGA0L7QtFxyXG4gICAgICAgICAgICBjb3VudHJ5OiBwYXJzZUludChhckhvdGVsUGFyYW1zLkNPVU5UUlkpID4gMCA/IGFySG90ZWxQYXJhbXMuQ09VTlRSWSA6IFwiMlwiLCAvL9Ci0LXQutGD0YnQsNGPINGB0YLRgNCw0L3QsFxyXG4gICAgICAgICAgICBob3RlbHM6IHBhcnNlSW50KGFySG90ZWxQYXJhbXMuSE9URUxDT0RFKSA+IDAgPyBhckhvdGVsUGFyYW1zLkhPVEVMQ09ERSA6IFwiMFwiLCAvL9Ce0YLQtdC70YxcclxuICAgICAgICAgICAgbWVhbDogXCJcIiwgLy/QktGL0LHRgNCw0L3QvdGL0LUg0YLQuNC/0Ysg0L/QuNGC0LDQvdC40Y9cclxuICAgICAgICAgICAgYWR1bHRzOiAyLCAvL9Ca0L7Qu9C40YfQtdGB0YLQstC+INCy0LfRgNC+0YHQu9GL0YVcclxuICAgICAgICAgICAgY2hpbGQ6IDAsIC8v0JrQvtC70LjRh9C10YHRgtCy0L4g0LTQtdGC0LXQuVxyXG4gICAgICAgICAgICBjaGlsZGFnZTE6IFwiMFwiLCAvL9CS0L7Qt9GA0LDRgdGCINGA0LXQsdC10L3QutCwIDFcclxuICAgICAgICAgICAgY2hpbGRhZ2UyOiBcIjBcIiwgLy/QktC+0LfRgNCw0YHRgiDRgNC10LHQtdC90LrQsCAyXHJcbiAgICAgICAgICAgIGNoaWxkYWdlMzogXCIwXCIsIC8v0JLQvtC30YDQsNGB0YIg0YDQtdCx0LXQvdC60LAgM1xyXG4gICAgICAgICAgICBuaWdodHNmcm9tOiA2LCAvL9Ca0L7Qu9C40YfQtdGB0YLQstC+INC90L7Rh9C10Lkg0L7RglxyXG4gICAgICAgICAgICBuaWdodHN0bzogMTQsIC8v0JrQvtC70LjRh9C10YHRgtCy0L4g0L3QvtGH0LXQuSDQtNC+XHJcbiAgICAgICAgICAgIHByaWNlZnJvbTogXCJcIiwgLy/QptC10L3QsCDQvtGCXHJcbiAgICAgICAgICAgIHByaWNldG86IFwiXCIsIC8v0KbQtdC90LAg0LTQvlxyXG4gICAgICAgICAgICBkYXRlZnJvbTogbW9tZW50KCkuYWRkKDEsICdkYXlzJykuZm9ybWF0KCdERC5NTS5ZWVlZJyksIC8v0JTQsNGC0LAg0LLRi9C70LXRgtCwINC+0YJcclxuICAgICAgICAgICAgZGF0ZXRvOiBtb21lbnQoKS5hZGQoMTQsICdkYXlzJykuZm9ybWF0KCdERC5NTS5ZWVlZJyksIC8v0JTQsNGC0LAg0LLRi9C70LXRgtCwINC00L5cclxuICAgICAgICAgICAgb3BlcmF0b3JzOiBcIlwiLCAvL9Ch0L/QuNGB0L7QuiDQvtC/0LXRgNCw0YLQvtGA0L7QslxyXG4gICAgICAgICAgICByZXF1ZXN0aWQ6IFwiXCIsIC8vSUQg0LfQsNC/0YDQvtGB0LBcclxuICAgICAgICAgICAgcGFnZU51bWJlcjogMSwgLy/QndC+0LzQtdGAINGB0YLRgNCw0L3QuNGG0YtcclxuXHJcbiAgICAgICAgICAgIC8q0J7QsdGK0LXQutGC0YsqL1xyXG4gICAgICAgICAgICBvYkRlcGFydHVyZXM6IFtdLFxyXG4gICAgICAgICAgICBvYk9wZXJhdG9yczogW10sXHJcbiAgICAgICAgICAgIG9iRmx5ZGF0ZXM6IFtdLFxyXG4gICAgICAgICAgICBvYk1lYWxzOiBbXSxcclxuICAgICAgICAgICAgb2JTZWFyY2hSZXN1bHRzOiBbXSxcclxuXHJcbiAgICAgICAgICAgIC8q0JLRgNC10LzQtdC90L3Ri9C1INC+0LHRitC10LrRgtGLKi9cclxuICAgICAgICAgICAgb2JNZWFsc1NlbGVjdGVkOiBbXSxcclxuICAgICAgICAgICAgb2JPcGVyYXRvcnNTZWxlY3RlZDogW10sXHJcbiAgICAgICAgICAgIG9iTWVhbHNOYW1lOiBbXSxcclxuICAgICAgICAgICAgb2JPcGVyYXRvcnNOYW1lOiBbXSxcclxuXHJcbiAgICAgICAgICAgIC8q0J3QsNC40LzQtdC90L7QstCw0L3QuNGPKi9cclxuICAgICAgICAgICAgdG91cmlzdFN0cmluZzogXCIyINCy0LfRgC5cIixcclxuXHJcbiAgICAgICAgICAgIC8q0YHQu9GD0LbQtdCx0L3Ri9C1Ki9cclxuICAgICAgICAgICAgdG91cnNOb3RGb3VuZDogZmFsc2UsXHJcblxyXG4gICAgICAgICAgICAvKtC/0L7QuNGB0LoqL1xyXG4gICAgICAgICAgICBzZWFyY2hTdHJpbmc6IFwiXCIsIC8v0L/QvtC40YHQutC+0LLQsNGPINGB0YLRgNC+0LrQsCDRgdGC0YDQsNC90YtcclxuICAgICAgICAgICAgYWxsT3BlcmF0b3JzUHJvY2Vzc2VkOiB0cnVlLCAvL9CS0YHQtSDQotCeINC+0LHRgNCw0LHQvtGC0LDQu9C4INC30LDQv9GA0L7RgVxyXG4gICAgICAgICAgICBhbGxSb3dzQ291bnQ6IDAsIC8v0J7QsdGJ0LXQtSDQutC+0LvQuNGH0LXRgdGC0LLQviDRgNC10LfRg9C70YzRgtCw0YLQvtCyXHJcbiAgICAgICAgICAgIGZpcnN0Um93c0lzRGlzcGxheWVkOiBmYWxzZSwgLy/Qn9C10YDQstCw0Y8g0L/QsNGA0YLQuNGPINGA0LXQt9GD0LvRjNGC0LDRgtC+0LIg0LfQsNCz0YDRg9C20LXQvdCwXHJcbiAgICAgICAgICAgIG1pblByaWNlOiAwLCAvL9Cc0LjQvdC40LzQsNC70YzQvdCw0Y8g0L3QsNC50LTQtdC90L3QsNGPINGG0LXQvdCwXHJcbiAgICAgICAgICAgIHNlYXJjaFByb2dyZXNzOiAwLCAvL9Cf0YDQvtCz0YDQtdGBINC/0L7QuNGB0LrQsFxyXG4gICAgICAgICAgICBzZWFyY2hJbkFjdGlvbjogZmFsc2UsIC8v0KTQu9Cw0LMg0YLQvtCz0L4sINGH0YLQviDQv9C+0LjRgdC6INGB0YLQsNGA0YLQvtCy0LDQu1xyXG4gICAgICAgICAgICBuZXh0UGFnZVJlcXVlc3RJbkFjdGlvbjogZmFsc2UsIC8v0KTQu9Cw0LMg0YLQvtCz0L4sINGH0YLQviDQuNC00LXRgiDQt9Cw0L/RgNC+0YEg0L3QsCDRgdC70LXQtNGD0Y7RidGD0Y4g0YHRgtGA0LDQvdC40YbRgyDRgtGD0YDQvtCyXHJcbiAgICAgICAgICAgIHRvdXJzSXNPdmVyOiBmYWxzZSwgLy/QpNC70LDQsyDRgtC+0LPQviwg0YfRgtC+INCx0L7Qu9GM0YjQtSDRgtGD0YDQvtCyINC90LXRgiwg0L/QvtC60LDQt9Cw0L3RiyDQstGB0LUg0YHRgtGA0LDQvdC40YbRi1xyXG5cclxuICAgICAgICAgICAgLypGYXN0T3JkZXIqL1xyXG4gICAgICAgICAgICBpc1Nob3dGYXN0T3JkZXI6IGZhbHNlLFxyXG4gICAgICAgICAgICBmYXN0Rm9ybVBob25lOiBcIlwiLFxyXG4gICAgICAgICAgICBmYXN0Rm9ybU5hbWU6IFwiXCIsXHJcbiAgICAgICAgICAgIGZhc3RGb3JtTmFtZUhhc0Vycm9yOiBmYWxzZSxcclxuICAgICAgICAgICAgZmFzdEZvcm1QaG9uZUhhc0Vycm9yOiBmYWxzZSxcclxuICAgICAgICAgICAgZmFzdEZvcm1TZW5kZWQ6IGZhbHNlLFxyXG5cclxuXHJcbiAgICAgICAgICAgIGRlYnVnOiBmYWxzZSxcclxuICAgICAgICAgICAgcXVlcnlQYXJhbXM6IFtdLFxyXG4gICAgICAgICAgICBzZWFyY2hVcmw6IFwiXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZTogZmFsc2UsXHJcbiAgICAgICAgICAgIHNob3dBZGRpdGlvbmFsUGFyYW1zOiBmYWxzZVxyXG5cclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBjcmVhdGVkOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGxldCBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmluaXQoKTtcclxuICAgICAgICAgICAgdGhpcy5xdWVyeVBhcmFtcyA9IGdldEFsbFVybFBhcmFtcyh3aW5kb3cubG9jYXRpb24udG9TdHJpbmcoKSk7XHJcblxyXG4gICAgICAgICAgICAvL9CQ0LLRgtC+0YHRgtCw0YDRgiDQv9C+0LjRgdC60LBcclxuICAgICAgICAgICAgaWYgKF90aGlzLnF1ZXJ5UGFyYW1zLnN0YXJ0ID09PSAneScpIHtcclxuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIF90aGlzLnN0YXJ0U2VhcmNoKCk7XHJcbiAgICAgICAgICAgICAgICB9LCAyMDAwKVxyXG4gICAgICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAgICAgLy/Ql9Cw0L/QvtC70L3Rj9C10Lwg0L/QsNGA0LDQvNC10YLRgNGLINC40LcgcmVxdWVzdFxyXG4gICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5kYXRlZnJvbSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRhdGVmcm9tID0gdGhpcy5xdWVyeVBhcmFtcy5kYXRlZnJvbTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5kYXRldG8gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kYXRldG8gPSB0aGlzLnF1ZXJ5UGFyYW1zLmRhdGV0bztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5uaWdodHNmcm9tICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubmlnaHRzZnJvbSA9IHRoaXMucXVlcnlQYXJhbXMubmlnaHRzZnJvbTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5uaWdodHN0byAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5pZ2h0c3RvID0gdGhpcy5xdWVyeVBhcmFtcy5uaWdodHN0bztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5wcmljZWZyb20gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcmljZWZyb20gPSB0aGlzLnF1ZXJ5UGFyYW1zLnByaWNlZnJvbTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5wcmljZXRvICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJpY2V0byA9IHRoaXMucXVlcnlQYXJhbXMucHJpY2V0bztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMucXVlcnlQYXJhbXMuYWR1bHRzICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWR1bHRzID0gdGhpcy5xdWVyeVBhcmFtcy5hZHVsdHM7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMucXVlcnlQYXJhbXMuY2hpbGQgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jaGlsZCA9IHRoaXMucXVlcnlQYXJhbXMuY2hpbGQ7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5jaGlsZGFnZTEgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2hpbGRhZ2UxID0gdGhpcy5xdWVyeVBhcmFtcy5jaGlsZGFnZTE7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5jaGlsZGFnZTIgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2hpbGRhZ2UyID0gdGhpcy5xdWVyeVBhcmFtcy5jaGlsZGFnZTI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5jaGlsZGFnZTMgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2hpbGRhZ2UzID0gdGhpcy5xdWVyeVBhcmFtcy5jaGlsZGFnZTM7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIGR1bm5vIHdoeSB2LW9uPVwicmVzaXplOiBmdW5jXCIgbm90IHdvcmtpbmdcclxuICAgICAgICAgICAgaWYgKGRvY3VtZW50LmJvZHkuY2xpZW50V2lkdGggPD0gNzAwKSB7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy4kc2V0KF90aGlzLCAnbW9iaWxlJywgdHJ1ZSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy4kc2V0KF90aGlzLCAnbW9iaWxlJywgZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGdsb2JhbC53aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGRvY3VtZW50LmJvZHkuY2xpZW50V2lkdGggPD0gNzAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMuJHNldChfdGhpcywgJ21vYmlsZScsIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBfdGhpcy4kc2V0KF90aGlzLCAnbW9iaWxlJywgZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIGxldCBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICAgICAgX3RoaXMuZ2V0RGVwYXJ0dXJlcygpO1xyXG4gICAgICAgICAgICAgICAgX3RoaXMuZ2V0T3BlcmF0b3JzKCk7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy5nZXRGbHlkYXRlcygpO1xyXG4gICAgICAgICAgICAgICAgX3RoaXMuZ2V0TWVhbHMoKTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGdldERlcGFydHVyZXM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuJGh0dHAuZ2V0KCcvbG9jYWwvbW9kdWxlcy9uZXd0cmF2ZWwuc2VhcmNoL2xpYi9hamF4LnBocCcsIHtwYXJhbXM6IHt0eXBlOiBcImRlcGFydHVyZVwifX0pLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9iRGVwYXJ0dXJlcyA9IHJlc3BvbnNlLmJvZHkubGlzdHMuZGVwYXJ0dXJlcy5kZXBhcnR1cmU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub2JEZXBhcnR1cmVzID0gXy5zb3J0QnkodGhpcy5vYkRlcGFydHVyZXMsIFsnbmFtZSddKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8v0JfQsNC/0L7Qu9C90Y/QtdC8INC/0LDRgNCw0LzQtdGC0YDRiyDQuNC3IHJlcXVlc3RcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucXVlcnlQYXJhbXMuZGVwYXJ0dXJlICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVwYXJ0dXJlID0gdGhpcy5xdWVyeVBhcmFtcy5kZXBhcnR1cmU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNpdHlGcm9tTmFtZSA9IF8uZmluZCh0aGlzLm9iRGVwYXJ0dXJlcywgWydpZCcsIHRoaXMucXVlcnlQYXJhbXMuZGVwYXJ0dXJlLnRvU3RyaW5nKCldKSA/IF8uZmluZCh0aGlzLm9iRGVwYXJ0dXJlcywgWydpZCcsIHRoaXMucXVlcnlQYXJhbXMuZGVwYXJ0dXJlLnRvU3RyaW5nKCldKS5uYW1lZnJvbSA6IFwi0KPRhNGLXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoQlguZ2V0Q29va2llKCdORVdUUkFWRUxfVVNFUl9DSVRZJykgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZXBhcnR1cmUgPSBCWC5nZXRDb29raWUoJ05FV1RSQVZFTF9VU0VSX0NJVFknKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2l0eUZyb21OYW1lID0gXy5maW5kKHRoaXMub2JEZXBhcnR1cmVzLCBbJ2lkJywgQlguZ2V0Q29va2llKCdORVdUUkFWRUxfVVNFUl9DSVRZJykudG9TdHJpbmcoKV0pID8gXy5maW5kKHRoaXMub2JEZXBhcnR1cmVzLCBbJ2lkJywgQlguZ2V0Q29va2llKCdORVdUUkFWRUxfVVNFUl9DSVRZJykudG9TdHJpbmcoKV0pLm5hbWVmcm9tIDogXCLQo9GE0YtcIjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLy/Qn9C+0LTRgtCy0YDQtdC20LTQtdC90LjQtSDQs9C+0YDQvtC00LBcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gJCgnLmRlcGFydHVyZV93cmFwJykuY29uZmlybUNpdHkoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgZGVwYXJ0dXJlTW9kYWw6ICQoJy5jaXR5RnJvbScpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgZGVwYXJ0dXJlSWQ6IHRoaXMuZGVwYXJ0dXJlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBnZXRPcGVyYXRvcnM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuJGh0dHAuZ2V0KCcvbG9jYWwvbW9kdWxlcy9uZXd0cmF2ZWwuc2VhcmNoL2xpYi9hamF4LnBocCcsIHtwYXJhbXM6IHt0eXBlOiBcIm9wZXJhdG9yXCJ9fSkudGhlbihyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IF90aGlzID0gdGhpcztcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9iT3BlcmF0b3JzID0gcmVzcG9uc2UuYm9keS5saXN0cy5vcGVyYXRvcnMub3BlcmF0b3I7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8v0JfQsNC/0L7Qu9C90Y/QtdC8INC/0LDRgNCw0LzQtdGC0YDRiyDQuNC3IHJlcXVlc3RcclxuICAgICAgICAgICAgICAgICAgICBpZiAoX3RoaXMucXVlcnlQYXJhbXMub3BlcmF0b3JzICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHF1ZXJ5ID0gdGhpcy5hcnJheUNvbnZlcnQoX3RoaXMucXVlcnlQYXJhbXMub3BlcmF0b3JzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgX3RoaXMub3BlcmF0b3JzID0gcXVlcnkuam9pbigpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfLmZvckVhY2gocXVlcnksIGZ1bmN0aW9uICh2YWx1ZSwga2V5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5vcGVyYXRvclNlbGVjdChfLmZpbmQoX3RoaXMub2JPcGVyYXRvcnMsIFsnaWQnLCB2YWx1ZV0pID8gXy5maW5kKF90aGlzLm9iT3BlcmF0b3JzLCBbJ2lkJywgdmFsdWVdKSA6IFwiXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGdldEZseWRhdGVzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLiRodHRwLmdldCgnL2xvY2FsL21vZHVsZXMvbmV3dHJhdmVsLnNlYXJjaC9saWIvYWpheC5waHAnLCB7cGFyYW1zOiB7dHlwZTogXCJmbHlkYXRlXCIsIGZseWRlcGFydHVyZTogdGhpcy5kZXBhcnR1cmUsIGZseWNvdW50cnk6IHRoaXMuY291bnRyeX19KS50aGVuKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgZGFyZXJhbmdlc2VsZWN0ID0gJCgnI2RhdGVSYW5nZVNlbGVjdENvbnRhaW5lcicpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5ib2R5Lmxpc3RzLmZseWRhdGVzICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub2JGbHlkYXRlcyA9IHJlc3BvbnNlLmJvZHkubGlzdHMuZmx5ZGF0ZXMuZmx5ZGF0ZTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9iRmx5ZGF0ZXMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoZGFyZXJhbmdlc2VsZWN0LmRhdGEoJ2RhdGVSYW5nZVBpY2tlcicpICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGFyZXJhbmdlc2VsZWN0LmRhdGEoJ2RhdGVSYW5nZVBpY2tlcicpLmRlc3Ryb3koKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgZGFyZXJhbmdlc2VsZWN0LmRhdGVSYW5nZVBpY2tlcih7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1hdDogJ0RELk1NLllZWVknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbmxpbmU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRhaW5lcjogJyNkYXRlUmFuZ2VTZWxlY3RDb250YWluZXInLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbHdheXNPcGVuOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdGFydE9mV2VlazogJ21vbmRheScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpbmdsZU1vbnRoOiAnYXV0bycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNob3dTaG9ydGN1dHM6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RGb3J3YXJkOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdGlja3lNb250aHM6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlcGFyYXRvcjogJy0nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5EYXlzOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXhEYXlzOiAxNCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmVmb3JlU2hvd0RheTogZnVuY3Rpb24gKGRhdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICgkLmluQXJyYXkobW9tZW50KGRhdGUpLmZvcm1hdChcIkRELk1NLllZWVlcIiksIHNlYXJjaEFwcC5vYkZseWRhdGVzKSA+PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFt0cnVlLCBcImluLWZseWRhdGVcIiwgXCJcIl07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbdHJ1ZSwgXCJcIiwgXCJcIl07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KS5iaW5kKCdkYXRlcGlja2VyLWNoYW5nZScsIGZ1bmN0aW9uIChldmVudCwgb2JqKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlYXJjaEFwcC5kYXRlZnJvbSA9IG1vbWVudChvYmouZGF0ZTEpLmZvcm1hdChcIkRELk1NLllZWVlcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlYXJjaEFwcC5kYXRldG8gPSBtb21lbnQob2JqLmRhdGUyKS5mb3JtYXQoXCJERC5NTS5ZWVlZXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcjZGF0ZVJhbmdlU2VsZWN0Q29udGFpbmVyJykucGFyZW50KCkucmVtb3ZlQ2xhc3MoJ3Nob3cnKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgZ2V0TWVhbHM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIGxldCBfdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy4kaHR0cC5nZXQoJy9sb2NhbC9tb2R1bGVzL25ld3RyYXZlbC5zZWFyY2gvbGliL2FqYXgucGhwJywge3BhcmFtczoge3R5cGU6IFwibWVhbFwifX0pLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIF90aGlzLm9iTWVhbHMgPSByZXNwb25zZS5ib2R5Lmxpc3RzLm1lYWxzLm1lYWw7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8v0JfQsNC/0L7Qu9C90Y/QtdC8INC/0LDRgNCw0LzQtdGC0YDRiyDQuNC3IHJlcXVlc3RcclxuICAgICAgICAgICAgICAgICAgICBpZiAoX3RoaXMucXVlcnlQYXJhbXMubWVhbCAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBxdWVyeSA9IHRoaXMuYXJyYXlDb252ZXJ0KF90aGlzLnF1ZXJ5UGFyYW1zLnJlZ2lvbnMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5tZWFsID0gcXVlcnkuam9pbigpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfLmZvckVhY2gocXVlcnksIGZ1bmN0aW9uICh2YWx1ZSwga2V5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5tZWFsc1NlbGVjdChfLmZpbmQoX3RoaXMub2JNZWFscywgWydpZCcsIHZhbHVlXSkgPyBfLmZpbmQoX3RoaXMub2JNZWFscywgWydpZCcsIHZhbHVlXSkgOiBcIlwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG5cclxuICAgICAgICAgICAgLyrQn9Ce0JjQodCa0J7QktCr0JUg0JzQldCi0J7QlNCrKi9cclxuXHJcbiAgICAgICAgICAgIC8v0J3QsNGH0LDQu9C+INC/0L7QuNGB0LrQsFxyXG4gICAgICAgICAgICBzdGFydFNlYXJjaDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgbGV0ICR0aGlzID0gdGhpcztcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlYnVnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT0g0J3QsNGH0LjQvdCw0LXQvCDQv9C+0LjRgdC6ID09PT09PT09PT09PT0nKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaEluQWN0aW9uID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMub2JTZWFyY2hSZXN1bHRzID0gW107XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlcXVlc3RpZCA9ICcnO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wYWdlTnVtYmVyID0gMTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZmlyc3RSb3dzSXNEaXNwbGF5ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWxsUm93c0NvdW50ID0gMDtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoUHJvZ3Jlc3MgPSAwO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5xdWVyeVBhcmFtcyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50b3Vyc05vdEZvdW5kID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IHBhcmFtcyA9IHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBcInNlYXJjaFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlcGFydHVyZTogdGhpcy5kZXBhcnR1cmUsXHJcbiAgICAgICAgICAgICAgICAgICAgY291bnRyeTogdGhpcy5jb3VudHJ5LFxyXG4gICAgICAgICAgICAgICAgICAgIGhvdGVsczogdGhpcy5ob3RlbHMsXHJcbiAgICAgICAgICAgICAgICAgICAgbWVhbDogdGhpcy5tZWFsLFxyXG4gICAgICAgICAgICAgICAgICAgIGFkdWx0czogdGhpcy5hZHVsdHMsXHJcbiAgICAgICAgICAgICAgICAgICAgY2hpbGQ6IHRoaXMuY2hpbGQsXHJcbiAgICAgICAgICAgICAgICAgICAgY2hpbGRhZ2UxOiB0aGlzLmNoaWxkYWdlMSxcclxuICAgICAgICAgICAgICAgICAgICBjaGlsZGFnZTI6IHRoaXMuY2hpbGRhZ2UyLFxyXG4gICAgICAgICAgICAgICAgICAgIGNoaWxkYWdlMzogdGhpcy5jaGlsZGFnZTMsXHJcbiAgICAgICAgICAgICAgICAgICAgbmlnaHRzZnJvbTogdGhpcy5uaWdodHNmcm9tLFxyXG4gICAgICAgICAgICAgICAgICAgIG5pZ2h0c3RvOiB0aGlzLm5pZ2h0c3RvLFxyXG4gICAgICAgICAgICAgICAgICAgIHByaWNlZnJvbTogdGhpcy5wcmljZWZyb20sXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpY2V0bzogdGhpcy5wcmljZXRvLFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGVmcm9tOiB0aGlzLmRhdGVmcm9tLFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGV0bzogdGhpcy5kYXRldG8sXHJcbiAgICAgICAgICAgICAgICAgICAgb3BlcmF0b3JzOiB0aGlzLm9wZXJhdG9ycyxcclxuICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgJChcImh0bWwsIGJvZHlcIikuYW5pbWF0ZSh7c2Nyb2xsVG9wOiAkKCcjcmVzdWx0cycpLm9mZnNldCgpLnRvcCAtIDEwMH0sIDEwMDApO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuJGh0dHAuZ2V0KCcvbG9jYWwvbW9kdWxlcy9uZXd0cmF2ZWwuc2VhcmNoL2xpYi9hamF4LnBocCcsIHtwYXJhbXM6IHBhcmFtc30pLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5ib2R5LnJlc3VsdC5yZXF1ZXN0aWQgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlcXVlc3RpZCA9IHJlc3BvbnNlLmJvZHkucmVzdWx0LnJlcXVlc3RpZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hVcmwgPSByZXNwb25zZS5ib2R5LnVybDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGlzdG9yeS5wdXNoU3RhdGUodGhpcy5yZXF1ZXN0aWQsIFwi0J/QvtC40YHQuiDRgtGD0YDQvtCyXCIsIFwiP3N0YXJ0PVkmXCIgKyByZXNwb25zZS5ib2R5LnVybCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0aGlzLmdldFN0YXR1cygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCAzMDAwKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5kZWJ1Zykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCLQntGI0LjQsdC60LAgc3RhcnRTZWFyY2hcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIC8v0JfQsNC/0YDQvtGBINGB0L7RgdGC0L7Rj9C90LjQtSDQv9C+0LjRgdC60LBcclxuICAgICAgICAgICAgZ2V0U3RhdHVzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5kZWJ1Zykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCc9PT09PT09PT09PSDQn9GA0L7QstC10YDRj9C10Lwg0YHRgtCw0YLRg9GBID09PT09PT09PT0nKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGxldCAkdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgICAgICAgICBsZXQgUm93c0NvdW50ID0gMDsgLy/QntCx0YnQtdC1INC60L7Qu9C40YfQtdGB0YLQstC+INGA0LXQt9GD0LvRjNGC0LDRgtC+0LIg0LTQu9GPINGG0LjQutC70LBcclxuICAgICAgICAgICAgICAgICR0aGlzLmdldFJlYXVlSXRlcmF0aW9uID0gMTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLiRodHRwLmdldCgnL2xvY2FsL21vZHVsZXMvbmV3dHJhdmVsLnNlYXJjaC9saWIvYWpheC5waHAnLCB7cGFyYW1zOiB7dHlwZTogXCJzdGF0dXNcIiwgcmVxdWVzdGlkOiB0aGlzLnJlcXVlc3RpZCwgb3BlcmF0b3JzdGF0dXM6IDF9fSkudGhlbihyZXNwb25zZSA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5ib2R5LmRhdGEuc3RhdHVzICE9PSB1bmRlZmluZWQpIHsgLy/Ql9Cw0L/RgNC+0YEg0L/RgNC+0YjQtdC7INCx0LXQtyDQvtGI0LjQsdC+0LpcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICR0aGlzLnNlYXJjaFByb2dyZXNzID0gcmVzcG9uc2UuYm9keS5kYXRhLnN0YXR1cy5wcm9ncmVzczsgLy/Qn9GA0L7RhtC10L3RgiDQstGL0L/QvtC70L3QtdC90LjRj1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgUm93c0NvdW50ID0gcmVzcG9uc2UuYm9keS5kYXRhLnN0YXR1cy5ob3RlbHNmb3VuZDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICgkdGhpcy5hbGxSb3dzQ291bnQgIT09IFJvd3NDb3VudCAmJiAhJHRoaXMuZmlyc3RSb3dzSXNEaXNwbGF5ZWQpIHsgLy/QldGB0LvQuCDQutC+0LvQuNGH0LXRgdGC0LLQviDQvdCw0LnQtNC10L3QvdGL0YUg0YDQtdC30YPQu9GM0YLQsNGC0L7QsiDQt9CwINGG0LjQutC7INC90LUg0YDQsNCy0L3QsCDRg9C20LUg0L3QsNC50LTQtdC90L3Ri9C8INGA0LXQt9GD0LvRjNGC0LDRgtCw0Lwg0Lgg0L/QtdGA0LLQsNGPINC/0LDRgNGC0LjRjyDQtdGJ0LUg0L3QtSDQvtGC0L7QsdGA0LDQttC10L3QsCwg0YLQvtCz0LTQsCDQstGL0LLQvtC00LjQvCDQv9C10YDQstGD0Y4g0L/QsNGA0YLQuNGOINGA0LXQt9GD0LvRjNGC0LDRgtC+0LJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0aGlzLmZpcnN0Um93c0lzRGlzcGxheWVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlYnVnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCI9PdCS0YvQstC+0LTQuNC8INC/0LXRgNCy0YvQtSDRgNC10LfRg9C70YzRgtCw0YLRiyDQvdCwINGN0LrRgNCw0L09PVwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0aGlzLmdldFJlc3VsdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocGFyc2VJbnQoJHRoaXMuc2VhcmNoUHJvZ3Jlc3MpICE9PSAxMDApIHsgLy/Qn9C+0LLRgtC+0YDRj9C10Lwg0L/RgNC+0LLQtdGA0LrRgyDRgdGC0LDRgtGD0YHQsFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuZGVidWcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCLQndC1INCy0YHQtSDQvtC/0LXRgNCw0YLQvtGA0Ysg0L7RgtGA0LDQsdC+0YLQsNC70LgsINC/0L7QstGC0L7RgNGP0LXQvCDQt9Cw0L/RgNC+0YEg0YHRgtCw0YLRg9GB0L7QslwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHRoaXMuZ2V0U3RhdHVzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCAyMDAwKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlYnVnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCI9PdCS0YHQtSDQvtCx0YDQsNCx0L7RgtC90L4hINCj0YDRgNCwISEhPT1cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCI9PdCS0YvQstC+0LTQuNC8INCy0YHQtSDRgNC10LfRg9C70YzRgtCw0YLRiyDQvdCwINGN0LrRgNCw0L09PVwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0aGlzLnNlYXJjaFByb2dyZXNzID0gMTAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHRoaXMuc2VhcmNoSW5BY3Rpb24gPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0aGlzLmdldFJlc3VsdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAkdGhpcy5hbGxSb3dzQ291bnQgPSByZXNwb25zZS5ib2R5LmRhdGEuc3RhdHVzLmhvdGVsc2ZvdW5kOyAvL9Ce0LHRidC10LUg0LrQvtC70LjRh9C10YHRgtCy0L4g0YDQtdC30YPQu9GM0YLQsNGC0L7QslxyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlYnVnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcItCe0YjQuNCx0LrQsCBnZXRTdGF0dXNcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIC8v0J/QvtC70YPRh9C10L3QuNC1INGA0LXQt9GD0LvRjNGC0LDRgtC+0LIg0L/QvtC40YHQutCwXHJcbiAgICAgICAgICAgIGdldFJlc3VsdDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy4kaHR0cC5nZXQoJy9sb2NhbC9tb2R1bGVzL25ld3RyYXZlbC5zZWFyY2gvbGliL2FqYXgucGhwJywge3BhcmFtczoge3R5cGU6IFwicmVzdWx0XCIsIHJlcXVlc3RpZDogdGhpcy5yZXF1ZXN0aWQsIHBhZ2U6IHRoaXMucGFnZU51bWJlcn19KS50aGVuKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuYm9keS5kYXRhLnN0YXR1cy5zdGF0ZSA9PT0gJ2ZpbmlzaGVkJyAmJiBwYXJzZUludChyZXNwb25zZS5ib2R5LmRhdGEuc3RhdHVzLnRvdXJzZm91bmQpID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudG91cnNOb3RGb3VuZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuYm9keS5kYXRhLnJlc3VsdCAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlYnVnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnPT09PT09PSDQn9C+0LvRg9GH0LjRgtC1INGA0LXQt9GD0LvRjNGC0LDRgtGLID09PT09PT09Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnBhZ2VOdW1iZXIgPT09IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub2JTZWFyY2hSZXN1bHRzID0gcmVzcG9uc2UuYm9keS5kYXRhLnJlc3VsdC5ob3RlbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub2JTZWFyY2hSZXN1bHRzID0gXy5jb25jYXQodGhpcy5vYlNlYXJjaFJlc3VsdHMsIHJlc3BvbnNlLmJvZHkuZGF0YS5yZXN1bHQuaG90ZWwpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5uZXh0UGFnZVJlcXVlc3RJbkFjdGlvbiA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIl0nKS5wb3BvdmVyKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmlnZ2VyOiAnaG92ZXIgfCBjbGljaycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGFpbmVyOiAkKCdib2R5JylcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCA1MDApXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnBhZ2VOdW1iZXIgPiAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5leHRQYWdlUmVxdWVzdEluQWN0aW9uID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRvdXJzSXNPdmVyID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5kZWJ1Zykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCLQntGI0LjQsdC60LAgZ2V0UmVzdWx0XCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBsb2FkTW9yZVRvdXJzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5leHRQYWdlUmVxdWVzdEluQWN0aW9uID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMucGFnZU51bWJlciA9IHRoaXMucGFnZU51bWJlciArIDE7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFJlc3VsdCgpO1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgLy/Qn9C40YLQsNC90LjQtSDQtNC70Y8g0LfQsNC/0YDQvtGB0LBcclxuICAgICAgICAgICAgbWVhbHNTZWxlY3Q6IGZ1bmN0aW9uIChtZWFsID0gXCJcIikge1xyXG4gICAgICAgICAgICAgICAgaWYgKG1lYWwgIT09IFwiXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAhXy5pbmNsdWRlcyh0aGlzLm9iTWVhbHNTZWxlY3RlZCwgbWVhbC5pZCkgPyB0aGlzLm9iTWVhbHNTZWxlY3RlZC5wdXNoKG1lYWwuaWQpIDogXy5wdWxsKHRoaXMub2JNZWFsc1NlbGVjdGVkLCBtZWFsLmlkKTtcclxuICAgICAgICAgICAgICAgICAgICAhXy5pbmNsdWRlcyh0aGlzLm9iTWVhbHNOYW1lLCBtZWFsLm5hbWUpID8gdGhpcy5vYk1lYWxzTmFtZS5wdXNoKG1lYWwubmFtZSkgOiBfLnB1bGwodGhpcy5vYk1lYWxzTmFtZSwgbWVhbC5uYW1lKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMubWVhbCA9IHRoaXMub2JNZWFsc1NlbGVjdGVkLmpvaW4oKTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIC8v0KLRg9GA0L7Qv9C10YDQsNGC0L7RgNGLINC00LvRjyDQt9Cw0L/RgNC+0YHQsFxyXG4gICAgICAgICAgICBvcGVyYXRvclNlbGVjdDogZnVuY3Rpb24gKG9wZXJhdG9yKSB7XHJcbiAgICAgICAgICAgICAgICAhXy5pbmNsdWRlcyh0aGlzLm9iT3BlcmF0b3JzU2VsZWN0ZWQsIG9wZXJhdG9yLmlkKSA/IHRoaXMub2JPcGVyYXRvcnNTZWxlY3RlZC5wdXNoKG9wZXJhdG9yLmlkKSA6IF8ucHVsbCh0aGlzLm9iT3BlcmF0b3JzU2VsZWN0ZWQsIG9wZXJhdG9yLmlkKTtcclxuICAgICAgICAgICAgICAgICFfLmluY2x1ZGVzKHRoaXMub2JPcGVyYXRvcnNOYW1lLCBvcGVyYXRvci5uYW1lKSA/IHRoaXMub2JPcGVyYXRvcnNOYW1lLnB1c2gob3BlcmF0b3IubmFtZSkgOiBfLnB1bGwodGhpcy5vYk9wZXJhdG9yc05hbWUsIG9wZXJhdG9yLm5hbWUpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vcGVyYXRvcnMgPSB0aGlzLm9iT3BlcmF0b3JzU2VsZWN0ZWQuam9pbigpO1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgLyrQpNC+0YDQvNC40YDQvtCy0L3QuNC1INGB0YLRgNC+0LrQuCDRgSDRgtGD0YDQuNGB0YLQsNC80LgqL1xyXG4gICAgICAgICAgICBzZXRUb3VyaXN0c1N0cmluZzogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgICAgIHNlbGYudG91cmlzdFN0cmluZyA9IHNlbGYuYWR1bHRzICsgXCIg0LLQt9GALiBcIjtcclxuICAgICAgICAgICAgICAgIGlmIChzZWxmLmNoaWxkID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGYudG91cmlzdFN0cmluZyArPSBzZWxmLmNoaWxkICsgXCIg0YDQtdCxLihcIjtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMTsgaSA8PSBzZWxmLmNoaWxkOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi50b3VyaXN0U3RyaW5nICs9IHNlbGZbJ2NoaWxkYWdlJyArIGldO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaSA8IHNlbGYuY2hpbGQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYudG91cmlzdFN0cmluZyArPSAnINC4ICdcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBzZWxmLnRvdXJpc3RTdHJpbmcgKz0gXCIpXCI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBmYXN0T3JkZXI6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZmFzdEZvcm1TZW5kZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGxldCBzZXNzaWR2YWwgPSAkKFwiI3Nlc3NpZFwiKS52YWwoKSxcclxuICAgICAgICAgICAgICAgICAgICB0b3VyaWQgPSAkKFwiaW5wdXRbbmFtZT1mYXN0VG91cklkXVwiKS52YWwoKSxcclxuICAgICAgICAgICAgICAgICAgICBjcmVkaXQgPSAkKFwiaW5wdXRbbmFtZT1jcmVkaXRdXCIpLnZhbCgpO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuZmFzdEZvcm1QaG9uZUhhc0Vycm9yID0gdGhpcy5mYXN0Rm9ybVBob25lLmxlbmd0aCA8PSAwO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mYXN0Rm9ybU5hbWVIYXNFcnJvciA9IHRoaXMuZmFzdEZvcm1OYW1lLmxlbmd0aCA8PSAwO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5mYXN0Rm9ybVBob25lSGFzRXJyb3IgJiYgIXRoaXMuZmFzdEZvcm1OYW1lSGFzRXJyb3IgJiYgdG91cmlkLmxlbmd0aCA+IDApIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kaHR0cC5nZXQoJy9sb2NhbC9tb2R1bGVzL25ld3RyYXZlbC5zZWFyY2gvbGliL2FqYXgucGhwJywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFwiZmFzdG9yZGVyXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b3VyaWQ6IHRvdXJpZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBob25lOiB0aGlzLmZhc3RGb3JtUGhvbmUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiB0aGlzLmZhc3RGb3JtTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlc3NpZDogc2Vzc2lkdmFsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3JlZGl0OiBjcmVkaXRcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuYm9keS5FUlJPUiA9PT0gXCJcIiAmJiByZXNwb25zZS5ib2R5LlNUQVRVUykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeWFDb3VudGVyMjQzOTU5MTEucmVhY2hHb2FsKCdGQVNUX09SREVSX0RFVEFJTF9UT1VSJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZhc3RGb3JtU2VuZGVkID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjZmFzdE9yZGVyTW9kYWwnKS5tb2RhbCgnaGlkZScpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgMTAwMClcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIC8v0JrQvtC80LLQtdGA0YLQuNGA0YPQtdC8INGB0LvQvtCy0LAg0LIg0LzQsNGB0YHQuNCyXHJcbiAgICAgICAgICAgIGFycmF5Q29udmVydDogZnVuY3Rpb24gKHF1ZXJ5KSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgYXJRdWVyeTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoXy5pc1N0cmluZyhxdWVyeSkpIHtcclxuICAgICAgICAgICAgICAgICAgICBhclF1ZXJ5ID0gW3F1ZXJ5XTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXJRdWVyeSA9IHF1ZXJ5O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGFyUXVlcnk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgd2F0Y2g6IHtcclxuICAgICAgICAgICAgZGVwYXJ0dXJlOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAvL3RoaXMuZ2V0Q291bnRyaWVzKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldEZseWRhdGVzKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgQlguc2V0Q29va2llKCdORVdUUkFWRUxfVVNFUl9DSVRZJywgdGhpcy5kZXBhcnR1cmUsIHtwYXRoOiAnLyd9KTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgYWR1bHRzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFRvdXJpc3RzU3RyaW5nKCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGNoaWxkOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFRvdXJpc3RzU3RyaW5nKCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGNoaWxkYWdlMTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRUb3VyaXN0c1N0cmluZygpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBjaGlsZGFnZTI6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0VG91cmlzdHNTdHJpbmcoKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgY2hpbGRhZ2UzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFRvdXJpc3RzU3RyaW5nKCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZmlsdGVyczoge1xyXG4gICAgICAgICAgICBkaXNwbGF5QWdlOiBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIGlmICghdmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJcIjtcclxuICAgICAgICAgICAgICAgIH1lbHNlIGlmKHBhcnNlSW50KHZhbHVlKSA9PT0gMSl7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwiPCAyINC70LXRglwiO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IHRpdGxlcyA9IFsn0LPQvtC0JywgJ9Cz0L7QtNCwJywgJ9C70LXRgiddO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlID0gcGFyc2VJbnQodmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB2YWx1ZSArIFwiIFwiICsgdGl0bGVzWyh2YWx1ZSAlIDEwID09IDEgJiYgdmFsdWUgJSAxMDAgIT0gMTEgPyAwIDogdmFsdWUgJSAxMCA+PSAyICYmIHZhbHVlICUgMTAgPD0gNCAmJiAodmFsdWUgJSAxMDAgPCAxMCB8fCB2YWx1ZSAlIDEwMCA+PSAyMCkgPyAxIDogMildO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgZGlzcGxheUFnZUludDogZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAocGFyc2VJbnQodmFsdWUpID49IDIgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHZhbHVlO1xyXG4gICAgICAgICAgICAgICAgfWVsc2UgaWYocGFyc2VJbnQodmFsdWUpIDw9IDEpe1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBcIjFcIjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGRpc3BsYXlOaWdodHM6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCF2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgdGl0bGVzID0gWyfQvdC+0YfRjCcsICfQvdC+0YfQuCcsICfQvdC+0YfQtdC5J107XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSBwYXJzZUludCh2YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHZhbHVlICsgXCIgXCIgKyB0aXRsZXNbKHZhbHVlICUgMTAgPT0gMSAmJiB2YWx1ZSAlIDEwMCAhPSAxMSA/IDAgOiB2YWx1ZSAlIDEwID49IDIgJiYgdmFsdWUgJSAxMCA8PSA0ICYmICh2YWx1ZSAlIDEwMCA8IDEwIHx8IHZhbHVlICUgMTAwID49IDIwKSA/IDEgOiAyKV07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBzaG9ydE5hbWU6IGZ1bmN0aW9uICh2YWx1ZSwgdHlwZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHZhbHVlLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHRpdGxlcyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodHlwZSA9PSAncmVnaW9uJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGVzID0gWyfQutGD0YDQvtGA0YInLCAn0LrRg9GA0L7RgNGC0LAnLCAn0LrRg9GA0L7RgNGC0L7QsiddO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGUgPT0gJ2hvdGVsJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGVzID0gWyfQvtGC0LXQu9GMJywgJ9C+0YLQtdC70Y8nLCAn0L7RgtC10LvQtdC5J107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZSA9PSAnc3VicmVnaW9uJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGVzID0gWyfQv9C+0YHQtdC70L7QuicsICfQv9C+0YHQtdC70LrQsCcsICfQv9C+0YHQtdC70LrQvtCyJ107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlID0gdmFsdWUubGVuZ3RoO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWUgKyBcIiBcIiArIHRpdGxlc1sodmFsdWUgJSAxMCA9PSAxICYmIHZhbHVlICUgMTAwICE9IDExID8gMCA6IHZhbHVlICUgMTAgPj0gMiAmJiB2YWx1ZSAlIDEwIDw9IDQgJiYgKHZhbHVlICUgMTAwIDwgMTAgfHwgdmFsdWUgJSAxMDAgPj0gMjApID8gMSA6IDIpXTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh2YWx1ZS5sZW5ndGggPT0gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWUudG9TdHJpbmcoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBmb3JtYXRQcmljZTogZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWUudG9TdHJpbmcoKS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIiBcIikgKyAnINCgJztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBjb21wb25lbnRzOiB7XHJcbiAgICAgICAgICAgIENvbVJlc3VsdHMsXHJcbiAgICAgICAgICAgIENvbVByb2dyZXNzQmFyLFxyXG4gICAgICAgICAgICBDb21EZXBhcnR1cmVcclxuICAgICAgICB9XHJcblxyXG4gICAgfSk7XHJcblxyXG4gICAgJCgnLnNlbGVjdC1kcm9wZG93bi5uby1jbG9zZScpLmNsaWNrKGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgfSk7XHJcblxyXG59KTtcclxuXHJcblxyXG5mdW5jdGlvbiBnZXRBbGxVcmxQYXJhbXModXJsKSB7XHJcblxyXG4gICAgLy8g0LjQt9Cy0LvQtdC60LDQtdC8INGB0YLRgNC+0LrRgyDQuNC3IFVSTCDQuNC70Lgg0L7QsdGK0LXQutGC0LAgd2luZG93XHJcbiAgICBsZXQgcXVlcnlTdHJpbmcgPSB1cmwgPyB1cmwuc3BsaXQoJz8nKVsxXSA6IHdpbmRvdy5sb2NhdGlvbi5zZWFyY2guc2xpY2UoMSk7XHJcblxyXG4gICAgLy8g0L7QsdGK0LXQutGCINC00LvRjyDRhdGA0LDQvdC10L3QuNGPINC/0LDRgNCw0LzQtdGC0YDQvtCyXHJcbiAgICBsZXQgb2JqID0ge307XHJcblxyXG4gICAgLy8g0LXRgdC70Lgg0LXRgdGC0Ywg0YHRgtGA0L7QutCwINC30LDQv9GA0L7RgdCwXHJcbiAgICBpZiAocXVlcnlTdHJpbmcpIHtcclxuXHJcbiAgICAgICAgLy8g0LTQsNC90L3Ri9C1INC/0L7RgdC70LUg0LfQvdCw0LrQsCAjINCx0YPQtNGD0YIg0L7Qv9GD0YnQtdC90YtcclxuICAgICAgICBxdWVyeVN0cmluZyA9IHF1ZXJ5U3RyaW5nLnNwbGl0KCcjJylbMF07XHJcblxyXG4gICAgICAgIC8vINGA0LDQt9C00LXQu9GP0LXQvCDQv9Cw0YDQsNC80LXRgtGA0YtcclxuICAgICAgICBsZXQgYXJyID0gcXVlcnlTdHJpbmcuc3BsaXQoJyYnKTtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhcnIubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgLy8g0YDQsNC30LTQtdC70Y/QtdC8INC/0LDRgNCw0LzQtdGC0YAg0L3QsCDQutC70Y7RhyA9PiDQt9C90LDRh9C10L3QuNC1XHJcbiAgICAgICAgICAgIGxldCBhID0gYXJyW2ldLnNwbGl0KCc9Jyk7XHJcblxyXG4gICAgICAgICAgICAvLyDQvtCx0YDQsNCx0L7RgtC60LAg0LTQsNC90L3Ri9GFINCy0LjQtNCwOiBsaXN0W109dGhpbmcxJmxpc3RbXT10aGluZzJcclxuICAgICAgICAgICAgbGV0IHBhcmFtTnVtID0gdW5kZWZpbmVkO1xyXG4gICAgICAgICAgICBsZXQgcGFyYW1OYW1lID0gYVswXS5yZXBsYWNlKC9cXFtcXGQqXFxdLywgZnVuY3Rpb24gKHYpIHtcclxuICAgICAgICAgICAgICAgIHBhcmFtTnVtID0gdi5zbGljZSgxLCAtMSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJyc7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8g0L/QtdGA0LXQtNCw0YfQsCDQt9C90LDRh9C10L3QuNGPINC/0LDRgNCw0LzQtdGC0YDQsCAoJ3RydWUnINC10YHQu9C4INC30L3QsNGH0LXQvdC40LUg0L3QtSDQt9Cw0LTQsNC90L4pXHJcbiAgICAgICAgICAgIGxldCBwYXJhbVZhbHVlID0gdHlwZW9mKGFbMV0pID09PSAndW5kZWZpbmVkJyA/IHRydWUgOiBhWzFdO1xyXG5cclxuICAgICAgICAgICAgLy8g0L/RgNC10L7QsdGA0LDQt9C+0LLQsNC90LjQtSDRgNC10LPQuNGB0YLRgNCwXHJcbiAgICAgICAgICAgIHBhcmFtTmFtZSA9IHBhcmFtTmFtZS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgICAgICBwYXJhbVZhbHVlID0gcGFyYW1WYWx1ZS50b0xvd2VyQ2FzZSgpO1xyXG5cclxuICAgICAgICAgICAgLy8g0LXRgdC70Lgg0LrQu9GO0Ycg0L/QsNGA0LDQvNC10YLRgNCwINGD0LbQtSDQt9Cw0LTQsNC9XHJcbiAgICAgICAgICAgIGlmIChvYmpbcGFyYW1OYW1lXSkge1xyXG4gICAgICAgICAgICAgICAgLy8g0L/RgNC10L7QsdGA0LDQt9GD0LXQvCDRgtC10LrRg9GJ0LXQtSDQt9C90LDRh9C10L3QuNC1INCyINC80LDRgdGB0LjQslxyXG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBvYmpbcGFyYW1OYW1lXSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgICAgICAgICBvYmpbcGFyYW1OYW1lXSA9IFtvYmpbcGFyYW1OYW1lXV07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAvLyDQtdGB0LvQuCDQvdC1INC30LDQtNCw0L0g0LjQvdC00LXQutGBLi4uXHJcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHBhcmFtTnVtID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vINC/0L7QvNC10YnQsNC10Lwg0LfQvdCw0YfQtdC90LjQtSDQsiDQutC+0L3QtdGGINC80LDRgdGB0LjQstCwXHJcbiAgICAgICAgICAgICAgICAgICAgb2JqW3BhcmFtTmFtZV0ucHVzaChwYXJhbVZhbHVlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vINC10YHQu9C4INC40L3QtNC10LrRgSDQt9Cw0LTQsNC9Li4uXHJcbiAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyDRgNCw0LfQvNC10YnQsNC10Lwg0Y3Qu9C10LzQtdC90YIg0L/QviDQt9Cw0LTQsNC90L3QvtC80YMg0LjQvdC00LXQutGB0YNcclxuICAgICAgICAgICAgICAgICAgICBvYmpbcGFyYW1OYW1lXVtwYXJhbU51bV0gPSBwYXJhbVZhbHVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vINC10YHQu9C4INC/0LDRgNCw0LzQtdGC0YAg0L3QtSDQt9Cw0LTQsNC9LCDQtNC10LvQsNC10Lwg0Y3RgtC+INCy0YDRg9GH0L3Rg9GOXHJcbiAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgb2JqW3BhcmFtTmFtZV0gPSBwYXJhbVZhbHVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBvYmo7XHJcbn1cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL3NlYXJjaC1mb3JtLmpzIiwidmFyIGc7XHJcblxyXG4vLyBUaGlzIHdvcmtzIGluIG5vbi1zdHJpY3QgbW9kZVxyXG5nID0gKGZ1bmN0aW9uKCkge1xyXG5cdHJldHVybiB0aGlzO1xyXG59KSgpO1xyXG5cclxudHJ5IHtcclxuXHQvLyBUaGlzIHdvcmtzIGlmIGV2YWwgaXMgYWxsb3dlZCAoc2VlIENTUClcclxuXHRnID0gZyB8fCBGdW5jdGlvbihcInJldHVybiB0aGlzXCIpKCkgfHwgKDEsZXZhbCkoXCJ0aGlzXCIpO1xyXG59IGNhdGNoKGUpIHtcclxuXHQvLyBUaGlzIHdvcmtzIGlmIHRoZSB3aW5kb3cgcmVmZXJlbmNlIGlzIGF2YWlsYWJsZVxyXG5cdGlmKHR5cGVvZiB3aW5kb3cgPT09IFwib2JqZWN0XCIpXHJcblx0XHRnID0gd2luZG93O1xyXG59XHJcblxyXG4vLyBnIGNhbiBzdGlsbCBiZSB1bmRlZmluZWQsIGJ1dCBub3RoaW5nIHRvIGRvIGFib3V0IGl0Li4uXHJcbi8vIFdlIHJldHVybiB1bmRlZmluZWQsIGluc3RlYWQgb2Ygbm90aGluZyBoZXJlLCBzbyBpdCdzXHJcbi8vIGVhc2llciB0byBoYW5kbGUgdGhpcyBjYXNlLiBpZighZ2xvYmFsKSB7IC4uLn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gZztcclxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gKHdlYnBhY2spL2J1aWxkaW4vZ2xvYmFsLmpzXG4vLyBtb2R1bGUgaWQgPSA1XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInZhciBkaXNwb3NlZCA9IGZhbHNlXG52YXIgbm9ybWFsaXplQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIilcbi8qIHNjcmlwdCAqL1xuZXhwb3J0ICogZnJvbSBcIiEhYmFiZWwtbG9hZGVyIS4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vQ29tRGVwYXJ0dXJlLnZ1ZVwiXG5pbXBvcnQgX192dWVfc2NyaXB0X18gZnJvbSBcIiEhYmFiZWwtbG9hZGVyIS4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vQ29tRGVwYXJ0dXJlLnZ1ZVwiXG4vKiB0ZW1wbGF0ZSAqL1xuaW1wb3J0IF9fdnVlX3RlbXBsYXRlX18gZnJvbSBcIiEhLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNWM4YTRiNmNcXFwiLFxcXCJoYXNTY29wZWRcXFwiOmZhbHNlLFxcXCJidWJsZVxcXCI6e1xcXCJ0cmFuc2Zvcm1zXFxcIjp7fX19IS4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Db21EZXBhcnR1cmUudnVlXCJcbi8qIHRlbXBsYXRlIGZ1bmN0aW9uYWwgKi9cbnZhciBfX3Z1ZV90ZW1wbGF0ZV9mdW5jdGlvbmFsX18gPSBmYWxzZVxuLyogc3R5bGVzICovXG52YXIgX192dWVfc3R5bGVzX18gPSBudWxsXG4vKiBzY29wZUlkICovXG52YXIgX192dWVfc2NvcGVJZF9fID0gbnVsbFxuLyogbW9kdWxlSWRlbnRpZmllciAoc2VydmVyIG9ubHkpICovXG52YXIgX192dWVfbW9kdWxlX2lkZW50aWZpZXJfXyA9IG51bGxcbnZhciBDb21wb25lbnQgPSBub3JtYWxpemVDb21wb25lbnQoXG4gIF9fdnVlX3NjcmlwdF9fLFxuICBfX3Z1ZV90ZW1wbGF0ZV9fLFxuICBfX3Z1ZV90ZW1wbGF0ZV9mdW5jdGlvbmFsX18sXG4gIF9fdnVlX3N0eWxlc19fLFxuICBfX3Z1ZV9zY29wZUlkX18sXG4gIF9fdnVlX21vZHVsZV9pZGVudGlmaWVyX19cbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwic3JjL0NvbURlcGFydHVyZS52dWVcIlxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNWM4YTRiNmNcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi01YzhhNGI2Y1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxuICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICBkaXNwb3NlZCA9IHRydWVcbiAgfSlcbn0pKCl9XG5cbmV4cG9ydCBkZWZhdWx0IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9Db21EZXBhcnR1cmUudnVlXG4vLyBtb2R1bGUgaWQgPSA2XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJyb3dcIiB9LCBbXG4gICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtMTJcIiB9LCBbXG4gICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImg1XCIgfSwgW1xuICAgICAgICBfdm0uX3YoXCLQn9C+0LjRgdC6INGC0YPRgNC+0LIg0LIg0L7RgtC10LvRjCDQuNC3XFxuXFx0XFx0XFx0XCIpLFxuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImRyb3Bkb3duIGQtaW5saW5lLWJsb2NrXCIgfSwgW1xuICAgICAgICAgIF9jKFxuICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcInRleHQtdXBwZXJjYXNlIGRpc3BsYXktNyBkZXBhcnR1cmVfd3JhcFwiLFxuICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgIGhyZWY6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsXG4gICAgICAgICAgICAgICAgaWQ6IFwiZGVwYXJ0dXJlVmFsdWVcIixcbiAgICAgICAgICAgICAgICBcImRhdGEtdG9nZ2xlXCI6IFwiZHJvcGRvd25cIixcbiAgICAgICAgICAgICAgICBcImFyaWEtaGFzcG9wdXBcIjogXCJ0cnVlXCIsXG4gICAgICAgICAgICAgICAgXCJhcmlhLWV4cGFuZGVkXCI6IFwiZmFsc2VcIlxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgW192bS5fdihfdm0uX3MoX3ZtLmNpdHlGcm9tTmFtZSkpXVxuICAgICAgICAgICksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcbiAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImRyb3Bkb3duLW1lbnUgc2VsZWN0LWRyb3Bkb3duIGJnLWxpZ2h0XCIsXG4gICAgICAgICAgICAgIGF0dHJzOiB7IFwiYXJpYS1sYWJlbGxlZGJ5XCI6IFwiZGVwYXJ0dXJlVmFsdWVcIiB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgW1xuICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgIHsgc3RhdGljQ2xhc3M6IFwic2Nyb2xsYWJsZS1tZW51XCIgfSxcbiAgICAgICAgICAgICAgICBfdm0uX2woX3ZtLmdyb3VwZWREZXBhcnR1cmVzLCBmdW5jdGlvbihpdGVtLCBsZXR0ZXIpIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybiBfYyhcbiAgICAgICAgICAgICAgICAgICAgXCJzcGFuXCIsXG4gICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcImg2XCIsIHsgc3RhdGljQ2xhc3M6IFwiZHJvcGRvd24taGVhZGVyXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KF92bS5fcyhsZXR0ZXIpKVxuICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgX3ZtLl9sKGl0ZW0sIGZ1bmN0aW9uKGRlcGFydHVyZUl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXCJzcGFuXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJkcm9wZG93bi1pdGVtXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3M6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiYmctcHJpbWFyeSB0ZXh0LXdoaXRlXCI6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlcGFydHVyZUl0ZW0uaWQgPT0gX3ZtLmRlcGFydHVyZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF92bS5zZXREZXBhcnR1cmUoZGVwYXJ0dXJlSXRlbSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoX3ZtLl9zKGRlcGFydHVyZUl0ZW0ubmFtZSkpXVxuICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgIDJcbiAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAwXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgIF1cbiAgICAgICAgICApXG4gICAgICAgIF0pXG4gICAgICBdKVxuICAgIF0pXG4gIF0pXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW11cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxudmFyIGVzRXhwb3J0cyA9IHsgcmVuZGVyOiByZW5kZXIsIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zIH1cbmV4cG9ydCBkZWZhdWx0IGVzRXhwb3J0c1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKSAgICAgIC5yZXJlbmRlcihcImRhdGEtdi01YzhhNGI2Y1wiLCBlc0V4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTVjOGE0YjZjXCIsXCJoYXNTY29wZWRcIjpmYWxzZSxcImJ1YmxlXCI6e1widHJhbnNmb3Jtc1wiOnt9fX0hLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vc3JjL0NvbURlcGFydHVyZS52dWVcbi8vIG1vZHVsZSBpZCA9IDdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwidmFyIGRpc3Bvc2VkID0gZmFsc2VcbnZhciBub3JtYWxpemVDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKVxuLyogc2NyaXB0ICovXG5leHBvcnQgKiBmcm9tIFwiISFiYWJlbC1sb2FkZXIhLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Db21SZXN1bHRzLnZ1ZVwiXG5pbXBvcnQgX192dWVfc2NyaXB0X18gZnJvbSBcIiEhYmFiZWwtbG9hZGVyIS4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vQ29tUmVzdWx0cy52dWVcIlxuLyogdGVtcGxhdGUgKi9cbmltcG9ydCBfX3Z1ZV90ZW1wbGF0ZV9fIGZyb20gXCIhIS4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LWVjOWJiNTI0XFxcIixcXFwiaGFzU2NvcGVkXFxcIjpmYWxzZSxcXFwiYnVibGVcXFwiOntcXFwidHJhbnNmb3Jtc1xcXCI6e319fSEuLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vQ29tUmVzdWx0cy52dWVcIlxuLyogdGVtcGxhdGUgZnVuY3Rpb25hbCAqL1xudmFyIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyA9IGZhbHNlXG4vKiBzdHlsZXMgKi9cbnZhciBfX3Z1ZV9zdHlsZXNfXyA9IG51bGxcbi8qIHNjb3BlSWQgKi9cbnZhciBfX3Z1ZV9zY29wZUlkX18gPSBudWxsXG4vKiBtb2R1bGVJZGVudGlmaWVyIChzZXJ2ZXIgb25seSkgKi9cbnZhciBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fID0gbnVsbFxudmFyIENvbXBvbmVudCA9IG5vcm1hbGl6ZUNvbXBvbmVudChcbiAgX192dWVfc2NyaXB0X18sXG4gIF9fdnVlX3RlbXBsYXRlX18sXG4gIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyxcbiAgX192dWVfc3R5bGVzX18sXG4gIF9fdnVlX3Njb3BlSWRfXyxcbiAgX192dWVfbW9kdWxlX2lkZW50aWZpZXJfX1xuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmMvQ29tUmVzdWx0cy52dWVcIlxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtZWM5YmI1MjRcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi1lYzliYjUyNFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxuICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICBkaXNwb3NlZCA9IHRydWVcbiAgfSlcbn0pKCl9XG5cbmV4cG9ydCBkZWZhdWx0IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9Db21SZXN1bHRzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gOFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwidWxcIiwgeyBzdGF0aWNDbGFzczogXCJsaXN0LXVuc3R5bGVkXCIgfSwgW1xuICAgIF92bS5yZXN1bHRzRGF0YS5sZW5ndGggPiAwXG4gICAgICA/IF9jKFxuICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgX3ZtLl9sKF92bS5yZXN1bHRzRGF0YSwgZnVuY3Rpb24oaG90ZWwsIGluZGV4KSB7XG4gICAgICAgICAgICByZXR1cm4gX2MoXG4gICAgICAgICAgICAgIFwibGlcIixcbiAgICAgICAgICAgICAgeyBzdGF0aWNDbGFzczogXCJhbmltYXRlZCBmYWRlSW5SaWdodCBtYi0yIHB0LTIgcGItMlwiIH0sXG4gICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNhcmQgYmctbGlnaHRcIiB9LCBbXG4gICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcInAtMyB0b3Vycy1saXN0IGJnLWxpZ2h0XCIsXG4gICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgaWQ6IFwidG91cnMtXCIgKyBob3RlbC5ob3RlbGNvZGUgfVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgeyBzdGF0aWNDbGFzczogXCJkLWJsb2NrXCIgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fbChob3RlbC50b3Vycy50b3VyLCBmdW5jdGlvbih0b3VyLCBrZXkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwicm93IGQtZmxleCBhbGlnbi1pdGVtcy1jZW50ZXIgYmctbGlnaHRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpZGRlblRvdXJzOiBrZXkgPiAyLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcGVuOiBfdm0uc2hvd0hpZGRlblRvdXJzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBzdGF0aWNDbGFzczogXCJjb2wtMTIgdGV4dC1tdXRlZCBtYi0zXCIgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW192bS5fdihfdm0uX3ModG91ci50b3VybmFtZSkpXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC02IGNvbC1tZC0yXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInBcIiwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwic3Ryb25nXCIsIFtfdm0uX3YoX3ZtLl9zKHRvdXIuZmx5ZGF0ZSkpXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwicFwiLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KF92bS5fcyh0b3VyLm5pZ2h0cykgKyBcIiDQvdC+0YfQtdC5XCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC02IGNvbC1tZC0zXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInBcIiwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwic3Ryb25nXCIsIFtfdm0uX3YoX3ZtLl9zKHRvdXIubWVhbCkpXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwicFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgYXR0cnM6IHsgdGl0bGU6IGhvdGVsLm1lYWxydXNzaWFuIH0gfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KF92bS5fcyh0b3VyLm1lYWxydXNzaWFuKSldXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLTYgY29sLW1kLTJcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwicFwiLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJzdHJvbmdcIiwgW192bS5fdihfdm0uX3ModG91ci5yb29tKSldKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJwXCIsIHsgYXR0cnM6IHsgdGl0bGU6IGhvdGVsLnBsYWNlbWVudCB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoX3ZtLl9zKHRvdXIucGxhY2VtZW50KSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLTYgY29sLW1kLTJcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwicFwiLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJzdHJvbmdcIiwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KF92bS5fcyh0b3VyLm9wZXJhdG9ybmFtZSkpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwicFwiLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpbWdcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3JjOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiaHR0cHM6Ly90b3Vydmlzb3IucnUvcGljcy9vcGVyYXRvcnMvc2VhcmNobG9nby9cIiArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG91ci5vcGVyYXRvcmNvZGUgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiLmdpZlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbHQ6IFwiXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtMTIgY29sLW1kLTNcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidGV4dC1zdWNjZXNzIHRleHQtbGVmdCB0ZXh0LW1kLXJpZ2h0IG1iLTBcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdNC30LAg0YLRg9GAXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJzcGFuXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJwcmljZSBoM1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfdm0uZ2V0RGV0YWlsKHRvdXIudG91cmlkKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fcyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9mKFwiZm9ybWF0UHJpY2VcIikodG91ci5wcmljZSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidGV4dC1tdXRlZCB0ZXh0LWxlZnQgdGV4dC1tZC1yaWdodFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCJcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHTQsdC10Lcg0YHQutC40LTQutC4OiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInNcIiwgeyBzdGF0aWNDbGFzczogXCJoNVwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fcyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fZihcImdldE9sZFByaWNlXCIpKHRvdXIucHJpY2UpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0ZXh0LWRhbmdlciB0ZXh0LWxlZnQgdGV4dC1tZC1yaWdodCBtYi0yIHByaWNlXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRhdGEtdG9nZ2xlXCI6IFwibW9kYWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkYXRhLWZhc3QtdG91ci1pZFwiOiB0b3VyLnRvdXJpZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkYXRhLWluZm9cIjogXCJjcmVkaXRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkYXRhLXRhcmdldFwiOiBcIiNmYXN0T3JkZXJNb2RhbFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx00LIg0LrRgNC10LTQuNGCIDYg0LzQtdGBLjogXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInNwYW5cIiwgeyBzdGF0aWNDbGFzczogXCJoNVwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fcyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fZihcImdldENyZWRpdFByaWNlXCIpKHRvdXIucHJpY2UpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX20oMCwgdHJ1ZSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImNvbC0xMiBkLWZsZXggZmxleC1jb2x1bW4gZmxleC1tZC1yb3cganVzdGlmeS1jb250ZW50LWVuZCB0ZXh0LXJpZ2h0XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiYnRuIGJ0bi1wcmltYXJ5IGJ0bi1zbSBsZC1leHQtbGVmdCBteC0xIG1iLTFcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3M6IFwiZGV0YWlsLWJ0bl9cIiArIHRvdXIudG91cmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBocmVmOiBcIiNcIiB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLmdldERldGFpbCh0b3VyLnRvdXJpZClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdNCi0YPRgCDQv9C+0LTRgNC+0LHQvdC+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImxkIGxkLXJpbmcgbGQtc3BpblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiYnRuIGJ0bi1vdXRsaW5lLW9yYW5nZSBidG4tc20gbXgtMSBtYi0xXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaHJlZjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRhdGEtdG9nZ2xlXCI6IFwibW9kYWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRhdGEtZmFzdC10b3VyLWlkXCI6IHRvdXIudG91cmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGF0YS10YXJnZXRcIjogXCIjZmFzdE9yZGVyTW9kYWxcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW192bS5fdihcItCX0LDQutCw0LfQsNGC0Ywg0LIgMSDQutC70LjQulwiKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9tKDEsIHRydWUpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgIDBcbiAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtMTIgdGV4dC1jZW50ZXJcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXCJidXR0b25cIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOiB7IGNsaWNrOiBfdm0udG9nZ2xlSGlkZGVuVG91cnMgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIV92bS5zaG93SGlkZGVuVG91cnNcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gX2MoXCJzcGFuXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCLQn9C+0LrQsNC30LDRgtGMINCy0YHQtSDRgtGD0YDRiyDQsiDQvtGC0LXQu9GMIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwgeyBzdGF0aWNDbGFzczogXCJmYSBmYS1jaGV2cm9uLWRvd25cIiB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBfdm0uX2UoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5zaG93SGlkZGVuVG91cnNcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gX2MoXCJzcGFuXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCLQodC60YDRi9GC0Ywg0YLRg9GA0YsgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcImZhIGZhLWNoZXZyb24tdXBcIiB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBfdm0uX2UoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgIF1cbiAgICAgICAgICAgIClcbiAgICAgICAgICB9KSxcbiAgICAgICAgICAwXG4gICAgICAgIClcbiAgICAgIDogX2MoXCJkaXZcIiwgW1xuICAgICAgICAgICFfdm0udG91cnNOb3RGb3VuZFxuICAgICAgICAgICAgPyBfYyhcbiAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICBcImNvbC0xMiBkaXNwbGF5LTYgbXQtNSBtYi01IHRleHQtY2VudGVyIHRleHQtc3VjY2Vzc1wiXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgICdcXG5cXHRcXHRcXHTQndCw0LbQvNC40YLQtSDQutC90L7Qv9C60YMgXCLQmNGB0LrQsNGC0YxcIiwg0LTQu9GPINC90LDRh9Cw0LvQsCDQv9C+0LjRgdC60LBcXG5cXHRcXHQnXG4gICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICApXG4gICAgICAgICAgICA6IF92bS5fZSgpLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX3ZtLnRvdXJzTm90Rm91bmRcbiAgICAgICAgICAgID8gX2MoXG4gICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgXCJjb2wtMTIgZGlzcGxheS03IG10LTUgbWItNSB0ZXh0LWNlbnRlciB0ZXh0LWRhbmdlclwiXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgICdcXG5cXHRcXHRcXHTQo9C/0YEhINCY0LfQstC40L3QuNGC0LUsINC90L4g0L/QviDQt9Cw0LTQsNC90L3Ri9C8INC/0LDRgNCw0LzQtdGC0YDQsNC8INGC0YPRgNC+0LIg0L3QtSDQvdCw0LnQtNC10L3Qvi4g0J/QvtC/0YDQvtCx0YPQudGC0LUg0LjQt9C80LXQvdC40YLRjCDQv9Cw0YDQvNC10YLRgNGLINC/0L7QuNGB0LrQsCDQuCDRgdC90L7QstCwINC90LDQttCw0YLRjCDQutC90L7Qv9C60YMgXCLQndCw0LnRgtC4XCIgJ1xuICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgIF9jKFwiYnJcIiksXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgIFwiXFxuXFx0XFx0XFx00JvQuNCx0L4g0L7RgtGB0YLQsNCy0YzRgtC1INC30LDRj9Cy0LrRgyDQvdCwINC/0L7QtNCx0L7RgCDRgtGD0YDQsCDQtNC70Y8g0L3QsNGI0LjRhSDQvNC10L3QtdC00LbQtdGA0L7Qsi5cXG5cXHRcXHRcIlxuICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgOiBfdm0uX2UoKVxuICAgICAgICBdKVxuICBdKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXCJwXCIsIFtcbiAgICAgIF9jKFwic21hbGxcIiwgW192bS5fdihcIijRgdGC0L7QuNC80L7RgdGC0Ywg0LHQtdC3INC/0LXRgNCy0L7QvdCw0YfQsNC70YzQvdC+0LPQviDQstC30L3QvtGB0LApXCIpXSlcbiAgICBdKVxuICB9LFxuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC0xMlwiIH0sIFtfYyhcImhyXCIpXSlcbiAgfVxuXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG52YXIgZXNFeHBvcnRzID0geyByZW5kZXI6IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnMgfVxuZXhwb3J0IGRlZmF1bHQgZXNFeHBvcnRzXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpICAgICAgLnJlcmVuZGVyKFwiZGF0YS12LWVjOWJiNTI0XCIsIGVzRXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtZWM5YmI1MjRcIixcImhhc1Njb3BlZFwiOmZhbHNlLFwiYnVibGVcIjp7XCJ0cmFuc2Zvcm1zXCI6e319fSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9zcmMvQ29tUmVzdWx0cy52dWVcbi8vIG1vZHVsZSBpZCA9IDlcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwidmFyIGRpc3Bvc2VkID0gZmFsc2VcbnZhciBub3JtYWxpemVDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKVxuLyogc2NyaXB0ICovXG5leHBvcnQgKiBmcm9tIFwiISFiYWJlbC1sb2FkZXIhLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Db21Qcm9ncmVzc0Jhci52dWVcIlxuaW1wb3J0IF9fdnVlX3NjcmlwdF9fIGZyb20gXCIhIWJhYmVsLWxvYWRlciEuLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0NvbVByb2dyZXNzQmFyLnZ1ZVwiXG4vKiB0ZW1wbGF0ZSAqL1xuaW1wb3J0IF9fdnVlX3RlbXBsYXRlX18gZnJvbSBcIiEhLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNWI5Nzk4MWVcXFwiLFxcXCJoYXNTY29wZWRcXFwiOmZhbHNlLFxcXCJidWJsZVxcXCI6e1xcXCJ0cmFuc2Zvcm1zXFxcIjp7fX19IS4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Db21Qcm9ncmVzc0Jhci52dWVcIlxuLyogdGVtcGxhdGUgZnVuY3Rpb25hbCAqL1xudmFyIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyA9IGZhbHNlXG4vKiBzdHlsZXMgKi9cbnZhciBfX3Z1ZV9zdHlsZXNfXyA9IG51bGxcbi8qIHNjb3BlSWQgKi9cbnZhciBfX3Z1ZV9zY29wZUlkX18gPSBudWxsXG4vKiBtb2R1bGVJZGVudGlmaWVyIChzZXJ2ZXIgb25seSkgKi9cbnZhciBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fID0gbnVsbFxudmFyIENvbXBvbmVudCA9IG5vcm1hbGl6ZUNvbXBvbmVudChcbiAgX192dWVfc2NyaXB0X18sXG4gIF9fdnVlX3RlbXBsYXRlX18sXG4gIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyxcbiAgX192dWVfc3R5bGVzX18sXG4gIF9fdnVlX3Njb3BlSWRfXyxcbiAgX192dWVfbW9kdWxlX2lkZW50aWZpZXJfX1xuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmMvQ29tUHJvZ3Jlc3NCYXIudnVlXCJcblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTViOTc5ODFlXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNWI5Nzk4MWVcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbiAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgZGlzcG9zZWQgPSB0cnVlXG4gIH0pXG59KSgpfVxuXG5leHBvcnQgZGVmYXVsdCBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvQ29tUHJvZ3Jlc3NCYXIudnVlXG4vLyBtb2R1bGUgaWQgPSAxMFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF92bS5zZWFyY2hJbkFjdGlvblxuICAgID8gX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJwcm9ncmVzcyBtdC0zIG1iLTNcIiB9LCBbXG4gICAgICAgIF9jKFxuICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAge1xuICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgIFwicHJvZ3Jlc3MtYmFyIHByb2dyZXNzLWJhci1zdHJpcGVkIHByb2dyZXNzLWJhci1hbmltYXRlZCBiZy1zdWNjZXNzIGQtZmxleCBqdXN0aWZ5LWNvbnRlbnQtYmV0d2VlblwiLFxuICAgICAgICAgICAgc3R5bGU6IHsgd2lkdGg6IF92bS5zZWFyY2hQcm9ncmVzcyArIFwiJVwiIH0sXG4gICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICByb2xlOiBcInByb2dyZXNzYmFyXCIsXG4gICAgICAgICAgICAgIFwiYXJpYS12YWx1ZW5vd1wiOiBfdm0uc2VhcmNoUHJvZ3Jlc3MsXG4gICAgICAgICAgICAgIFwiYXJpYS12YWx1ZW1pblwiOiBcIjBcIixcbiAgICAgICAgICAgICAgXCJhcmlhLXZhbHVlbWF4XCI6IFwiMTAwXCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIFtcbiAgICAgICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcInByb2dyZXNzLXR5cGUgcGwtMlwiIH0sIFtcbiAgICAgICAgICAgICAgX3ZtLl92KFwi0J/QvtC40YHQui4uLiDQndCw0LnQtNC10L3QviDRgtGD0YDQvtCyOiBcIiArIF92bS5fcyhfdm0uYWxsUm93c0NvdW50KSlcbiAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcInByb2dyZXNzLWNvbXBsZXRlZCBwci0yXCIgfSwgW1xuICAgICAgICAgICAgICBfdm0uX3YoX3ZtLl9zKF92bS5zZWFyY2hQcm9ncmVzcykgKyBcIiVcIilcbiAgICAgICAgICAgIF0pXG4gICAgICAgICAgXVxuICAgICAgICApXG4gICAgICBdKVxuICAgIDogX3ZtLl9lKClcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG52YXIgZXNFeHBvcnRzID0geyByZW5kZXI6IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnMgfVxuZXhwb3J0IGRlZmF1bHQgZXNFeHBvcnRzXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpICAgICAgLnJlcmVuZGVyKFwiZGF0YS12LTViOTc5ODFlXCIsIGVzRXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNWI5Nzk4MWVcIixcImhhc1Njb3BlZFwiOmZhbHNlLFwiYnVibGVcIjp7XCJ0cmFuc2Zvcm1zXCI6e319fSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9zcmMvQ29tUHJvZ3Jlc3NCYXIudnVlXG4vLyBtb2R1bGUgaWQgPSAxMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiXSwibWFwcGluZ3MiOiI7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQzdEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaEZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBdkJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQytEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBSEE7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUEE7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQW5EQTtBQXFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXJDQTtBQUNBO0FBekVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxRUE7QUFDQTtBQUNBO0FBRkE7Ozs7Ozs7QUNYQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQUE7QUFDQTs7O0FBQUE7QUFDQTs7Ozs7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFqRUE7QUFDQTtBQW1FQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFuQkE7QUFxQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFqQkE7QUFDQTtBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFEQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWxWQTtBQUNBO0FBb1ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBckJBO0FBQ0E7QUF1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF4REE7QUFDQTtBQTBEQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFoakJBO0FBQ0E7QUFzakJBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBaEJBO0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDbm9CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDcEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ3pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDbEZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ3pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDbFVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ3pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBIiwic291cmNlUm9vdCI6IiJ9