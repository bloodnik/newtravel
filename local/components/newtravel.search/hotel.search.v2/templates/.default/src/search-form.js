'use strict';

import ComDeparture from './ComDeparture.vue'
import ComResults from './ComResults.vue'
import ComProgressBar from './ComProgressBar.vue'

$(document).ready(function () {


    window.searchApp = new Vue({
        el: '#search',
        data: {
            /*Текущие свойства*/
            departure: "4", //Текущий город
            country: parseInt(arHotelParams.COUNTRY) > 0 ? arHotelParams.COUNTRY : "2", //Текущая страна
            hotels: parseInt(arHotelParams.HOTELCODE) > 0 ? arHotelParams.HOTELCODE : "0", //Отель
            meal: "", //Выбранные типы питания
            adults: 2, //Количество взрослых
            child: 0, //Количество детей
            childage1: "0", //Возраст ребенка 1
            childage2: "0", //Возраст ребенка 2
            childage3: "0", //Возраст ребенка 3
            nightsfrom: 6, //Количество ночей от
            nightsto: 14, //Количество ночей до
            pricefrom: "", //Цена от
            priceto: "", //Цена до
            datefrom: moment().add(1, 'days').format('DD.MM.YYYY'), //Дата вылета от
            dateto: moment().add(14, 'days').format('DD.MM.YYYY'), //Дата вылета до
            operators: "", //Список операторов
            requestid: "", //ID запроса
            pageNumber: 1, //Номер страницы

            /*Объекты*/
            obDepartures: [],
            obOperators: [],
            obFlydates: [],
            obMeals: [],
            obSearchResults: [],

            /*Временные объекты*/
            obMealsSelected: [],
            obOperatorsSelected: [],
            obMealsName: [],
            obOperatorsName: [],

            /*Наименования*/
            touristString: "2 взр.",

            /*служебные*/
            toursNotFound: false,

            /*поиск*/
            searchString: "", //поисковая строка страны
            allOperatorsProcessed: true, //Все ТО обработали запрос
            allRowsCount: 0, //Общее количество результатов
            firstRowsIsDisplayed: false, //Первая партия результатов загружена
            minPrice: 0, //Минимальная найденная цена
            searchProgress: 0, //Прогрес поиска
            searchInAction: false, //Флаг того, что поиск стартовал
            nextPageRequestInAction: false, //Флаг того, что идет запрос на следующую страницу туров
            toursIsOver: false, //Флаг того, что больше туров нет, показаны все страницы

            /*FastOrder*/
            isShowFastOrder: false,
            fastFormPhone: "",
            fastFormName: "",
            fastFormNameHasError: false,
            fastFormPhoneHasError: false,
            fastFormSended: false,


            debug: false,
            queryParams: [],
            searchUrl: "",
            mobile: false,
            showAdditionalParams: false

        },

        created: function () {
            let _this = this;

            this.init();
            this.queryParams = getAllUrlParams(window.location.toString());

            //Автостарт поиска
            if (_this.queryParams.start === 'y') {
                setTimeout(function () {
                    _this.startSearch();
                }, 2000)
            }


            //Заполняем параметры из request
            if (this.queryParams.datefrom !== undefined) {
                this.datefrom = this.queryParams.datefrom;
            }
            if (this.queryParams.dateto !== undefined) {
                this.dateto = this.queryParams.dateto;
            }
            if (this.queryParams.nightsfrom !== undefined) {
                this.nightsfrom = this.queryParams.nightsfrom;
            }
            if (this.queryParams.nightsto !== undefined) {
                this.nightsto = this.queryParams.nightsto;
            }
            if (this.queryParams.pricefrom !== undefined) {
                this.pricefrom = this.queryParams.pricefrom;
            }
            if (this.queryParams.priceto !== undefined) {
                this.priceto = this.queryParams.priceto;
            }

            if (this.queryParams.adults !== undefined) {
                this.adults = this.queryParams.adults;
            }
            if (this.queryParams.child !== undefined) {
                this.child = this.queryParams.child;
                if (this.queryParams.childage1 !== undefined) {
                    this.childage1 = this.queryParams.childage1;
                }
                if (this.queryParams.childage2 !== undefined) {
                    this.childage2 = this.queryParams.childage2;
                }
                if (this.queryParams.childage3 !== undefined) {
                    this.childage3 = this.queryParams.childage3;
                }
            }

            // dunno why v-on="resize: func" not working
            if (document.body.clientWidth <= 700) {
                _this.$set(_this, 'mobile', true);
            } else {
                _this.$set(_this, 'mobile', false);
            }
            global.window.addEventListener('resize', function () {
                if (document.body.clientWidth <= 700) {
                    _this.$set(_this, 'mobile', true);
                } else {
                    _this.$set(_this, 'mobile', false);
                }
            });
        },

        methods: {
            init: function () {
                let _this = this;

                _this.getDepartures();
                _this.getOperators();
                _this.getFlydates();
                _this.getMeals();
            },

            getDepartures: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "departure"}}).then(response => {
                        this.obDepartures = response.body.lists.departures.departure;
                        this.obDepartures = _.sortBy(this.obDepartures, ['name']);

                        //Заполняем параметры из request
                        if (this.queryParams.departure !== undefined) {
                            this.departure = this.queryParams.departure;
                            this.cityFromName = _.find(this.obDepartures, ['id', this.queryParams.departure.toString()]) ? _.find(this.obDepartures, ['id', this.queryParams.departure.toString()]).namefrom : "Уфы";
                        } else if (BX.getCookie('NEWTRAVEL_USER_CITY') !== undefined) {
                            this.departure = BX.getCookie('NEWTRAVEL_USER_CITY');
                            this.cityFromName = _.find(this.obDepartures, ['id', BX.getCookie('NEWTRAVEL_USER_CITY').toString()]) ? _.find(this.obDepartures, ['id', BX.getCookie('NEWTRAVEL_USER_CITY').toString()]).namefrom : "Уфы";
                        }

                        //Подтвреждение города
                        // $('.departure_wrap').confirmCity({
                        //     departureModal: $('.cityFrom'),
                        //     departureId: this.departure
                        // });
                    }
                );
            },

            getOperators: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "operator"}}).then(response => {
                    let _this = this;
                    this.obOperators = response.body.lists.operators.operator;

                    //Заполняем параметры из request
                    if (_this.queryParams.operators !== undefined) {
                        let query = this.arrayConvert(_this.queryParams.operators);
                        _this.operators = query.join();
                        _.forEach(query, function (value, key) {
                            _this.operatorSelect(_.find(_this.obOperators, ['id', value]) ? _.find(_this.obOperators, ['id', value]) : "");
                        });
                    }
                });
            },

            getFlydates: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "flydate", flydeparture: this.departure, flycountry: this.country}}).then(response => {
                    let darerangeselect = $('#dateRangeSelectContainer');
                    if (response.body.lists.flydates !== null) {
                        this.obFlydates = response.body.lists.flydates.flydate;
                    } else {
                        this.obFlydates = [];
                    }


                    if (darerangeselect.data('dateRangePicker') !== undefined) {
                        darerangeselect.data('dateRangePicker').destroy();
                    }
                    darerangeselect.dateRangePicker({
                        format: 'DD.MM.YYYY',
                        inline: true,
                        container: '#dateRangeSelectContainer',
                        alwaysOpen: true,
                        startOfWeek: 'monday',
                        singleMonth: 'auto',
                        showShortcuts: false,
                        selectForward: true,
                        stickyMonths: true,
                        separator: '-',
                        minDays: 1,
                        maxDays: 14,
                        beforeShowDay: function (date) {
                            if ($.inArray(moment(date).format("DD.MM.YYYY"), searchApp.obFlydates) >= 0) {
                                return [true, "in-flydate", ""];
                            } else {
                                return [true, "", ""];
                            }
                        }
                    }).bind('datepicker-change', function (event, obj) {
                        searchApp.datefrom = moment(obj.date1).format("DD.MM.YYYY");
                        searchApp.dateto = moment(obj.date2).format("DD.MM.YYYY");
                        $('#dateRangeSelectContainer').parent().removeClass('show');
                    });
                });
            },

            getMeals: function () {
                let _this = this;
                _this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "meal"}}).then(response => {
                    _this.obMeals = response.body.lists.meals.meal;

                    //Заполняем параметры из request
                    if (_this.queryParams.meal !== undefined) {
                        let query = this.arrayConvert(_this.queryParams.regions);
                        _this.meal = query.join();
                        _.forEach(query, function (value, key) {
                            _this.mealsSelect(_.find(_this.obMeals, ['id', value]) ? _.find(_this.obMeals, ['id', value]) : "");
                        });
                    }
                });
            },


            /*ПОИСКОВЫЕ МЕТОДЫ*/

            //Начало поиска
            startSearch: function () {
                let $this = this;
                if (this.debug) {
                    console.log('============= Начинаем поиск =============');
                }

                this.searchInAction = true;
                this.obSearchResults = [];
                this.requestid = '';
                this.pageNumber = 1;
                this.firstRowsIsDisplayed = false;
                this.allRowsCount = 0;
                this.searchProgress = 0;
                this.queryParams = [];
                this.toursNotFound = false;

                let params = {
                    type: "search",
                    departure: this.departure,
                    country: this.country,
                    hotels: this.hotels,
                    meal: this.meal,
                    adults: this.adults,
                    child: this.child,
                    childage1: this.childage1,
                    childage2: this.childage2,
                    childage3: this.childage3,
                    nightsfrom: this.nightsfrom,
                    nightsto: this.nightsto,
                    pricefrom: this.pricefrom,
                    priceto: this.priceto,
                    datefrom: this.datefrom,
                    dateto: this.dateto,
                    operators: this.operators,
                };

                $("html, body").animate({scrollTop: $('#results').offset().top - 100}, 1000);

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: params}).then(response => {
                    if (response.body.result.requestid !== undefined) {
                        this.requestid = response.body.result.requestid;
                        this.searchUrl = response.body.url;
                        history.pushState(this.requestid, "Поиск туров", "?start=Y&" + response.body.url);

                        setTimeout(function () {
                            $this.getStatus();
                        }, 3000);
                    } else {
                        if (this.debug) {
                            console.log("Ошибка startSearch");
                        }
                    }
                });
            },

            //Запрос состояние поиска
            getStatus: function () {
                if (this.debug) {
                    console.log('=========== Проверяем статус ==========');
                }
                let $this = this;
                let RowsCount = 0; //Общее количество результатов для цикла
                $this.getReaueIteration = 1;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "status", requestid: this.requestid, operatorstatus: 1}}).then(response => {

                    if (response.body.data.status !== undefined) { //Запрос прошел без ошибок

                        $this.searchProgress = response.body.data.status.progress; //Процент выполнения

                        RowsCount = response.body.data.status.hotelsfound;

                        if ($this.allRowsCount !== RowsCount && !$this.firstRowsIsDisplayed) { //Если количество найденных результатов за цикл не равна уже найденным результатам и первая партия еще не отображена, тогда выводим первую партию результатов
                            $this.firstRowsIsDisplayed = true;
                            if (this.debug) {
                                console.log("==Выводим первые результаты на экран==");
                            }
                            $this.getResult();
                        }

                        if (parseInt($this.searchProgress) !== 100) { //Повторяем проверку статуса
                            setTimeout(function () {
                                if (this.debug) {
                                    console.log("Не все операторы отработали, повторяем запрос статусов");
                                }
                                $this.getStatus();
                            }, 2000);
                        } else {
                            if (this.debug) {
                                console.log("==Все обработно! Урра!!!==");
                                console.log("==Выводим все результаты на экран==");
                            }
                            $this.searchProgress = 100;
                            $this.searchInAction = false;
                            $this.getResult();
                        }

                        $this.allRowsCount = response.body.data.status.hotelsfound; //Общее количество результатов
                    } else {
                        if (this.debug) {
                            console.log("Ошибка getStatus");
                        }
                    }
                });
            },

            //Получение результатов поиска
            getResult: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "result", requestid: this.requestid, page: this.pageNumber}}).then(response => {
                    if (response.body.data.status.state === 'finished' && parseInt(response.body.data.status.toursfound) === 0) {
                        this.toursNotFound = true;
                    }

                    if (response.body.data.result !== undefined) {
                        if (this.debug) {
                            console.log('======= Получите результаты ========');
                        }

                        if (this.pageNumber === 1) {
                            this.obSearchResults = response.body.data.result.hotel;
                        } else {
                            this.obSearchResults = _.concat(this.obSearchResults, response.body.data.result.hotel);
                            this.nextPageRequestInAction = false;
                        }

                        setTimeout(function () {
                            $('[data-toggle="popover"]').popover({
                                trigger: 'hover | click',
                                container: $('body')
                            });
                        }, 500)

                    } else {
                        if (this.pageNumber > 1) {
                            this.nextPageRequestInAction = false;
                            this.toursIsOver = true;
                        }
                        if (this.debug) {
                            console.log("Ошибка getResult");
                        }
                    }
                });
            },

            loadMoreTours: function () {
                this.nextPageRequestInAction = true;
                this.pageNumber = this.pageNumber + 1;
                this.getResult();
            },

            //Питание для запроса
            mealsSelect: function (meal = "") {
                if (meal !== "") {
                    !_.includes(this.obMealsSelected, meal.id) ? this.obMealsSelected.push(meal.id) : _.pull(this.obMealsSelected, meal.id);
                    !_.includes(this.obMealsName, meal.name) ? this.obMealsName.push(meal.name) : _.pull(this.obMealsName, meal.name);
                }
                this.meal = this.obMealsSelected.join();
            },

            //Туроператоры для запроса
            operatorSelect: function (operator) {
                !_.includes(this.obOperatorsSelected, operator.id) ? this.obOperatorsSelected.push(operator.id) : _.pull(this.obOperatorsSelected, operator.id);
                !_.includes(this.obOperatorsName, operator.name) ? this.obOperatorsName.push(operator.name) : _.pull(this.obOperatorsName, operator.name);
                this.operators = this.obOperatorsSelected.join();
            },

            /*Формировние строки с туристами*/
            setTouristsString: function () {
                let self = this;

                self.touristString = self.adults + " взр. ";
                if (self.child > 0) {
                    self.touristString += self.child + " реб.(";
                    for (let i = 1; i <= self.child; i++) {
                        self.touristString += self['childage' + i];
                        if (i < self.child) {
                            self.touristString += ' и '
                        }
                    }
                    self.touristString += ")";
                }
            },

            fastOrder: function () {
                this.fastFormSended = false;
                let sessidval = $("#sessid").val(),
                    tourid = $("input[name=fastTourId]").val(),
                    credit = $("input[name=credit]").val();

                this.fastFormPhoneHasError = this.fastFormPhone.length <= 0;
                this.fastFormNameHasError = this.fastFormName.length <= 0;

                if (!this.fastFormPhoneHasError && !this.fastFormNameHasError && tourid.length > 0) {

                    this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {
                        params: {
                            type: "fastorder",
                            tourid: tourid,
                            phone: this.fastFormPhone,
                            name: this.fastFormName,
                            sessid: sessidval,
                            credit: credit
                        }
                    }).then(response => {
                        if (response.body.ERROR === "" && response.body.STATUS) {
                            yaCounter24395911.reachGoal('FAST_ORDER_DETAIL_TOUR');
                            this.fastFormSended = true;

                            setTimeout(function () {
                                $('#fastOrderModal').modal('hide');
                            }, 1000)

                        }
                    });
                }
            },

            //Комвертируем слова в массив
            arrayConvert: function (query) {
                let arQuery;

                if (_.isString(query)) {
                    arQuery = [query];
                } else {
                    arQuery = query;
                }
                return arQuery;
            }

        },

        watch: {
            departure: function () {
                //this.getCountries();
                this.getFlydates();

                BX.setCookie('NEWTRAVEL_USER_CITY', this.departure, {path: '/'});
            },
            adults: function () {
                this.setTouristsString();
            },
            child: function () {
                this.setTouristsString();
            },
            childage1: function () {
                this.setTouristsString();
            },
            childage2: function () {
                this.setTouristsString();
            },
            childage3: function () {
                this.setTouristsString();
            },
        },

        filters: {
            displayAge: function (value) {
                if (!value) {
                    return "";
                }else if(parseInt(value) === 1){
                    return "< 2 лет";
                }
                else {
                    let titles = ['год', 'года', 'лет'];
                    value = parseInt(value);
                    return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                }
            },

            displayAgeInt: function (value) {
                if (parseInt(value) >= 2 ) {
                    return value;
                }else if(parseInt(value) <= 1){
                    return "1";
                }
            },

            displayNights: function (value) {
                if (!value) {
                    return "";
                } else {
                    let titles = ['ночь', 'ночи', 'ночей'];
                    value = parseInt(value);
                    return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                }
            },

            shortName: function (value, type) {
                if (value) {
                    if (value.length > 1) {
                        let titles = [];
                        if (type == 'region') {
                            titles = ['курорт', 'курорта', 'курортов'];
                        } else if (type == 'hotel') {
                            titles = ['отель', 'отеля', 'отелей'];
                        } else if (type == 'subregion') {
                            titles = ['поселок', 'поселка', 'поселков'];
                        }

                        value = value.length;
                        return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];

                    } else if (value.length == 1) {
                        return value.toString();
                    }
                }
            },

            formatPrice: function (value) {
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
            }

        },

        components: {
            ComResults,
            ComProgressBar,
            ComDeparture
        }

    });

    $('.select-dropdown.no-close').click(function (event) {
        event.stopPropagation();
    });

});


function getAllUrlParams(url) {

    // извлекаем строку из URL или объекта window
    let queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // объект для хранения параметров
    let obj = {};

    // если есть строка запроса
    if (queryString) {

        // данные после знака # будут опущены
        queryString = queryString.split('#')[0];

        // разделяем параметры
        let arr = queryString.split('&');

        for (let i = 0; i < arr.length; i++) {
            // разделяем параметр на ключ => значение
            let a = arr[i].split('=');

            // обработка данных вида: list[]=thing1&list[]=thing2
            let paramNum = undefined;
            let paramName = a[0].replace(/\[\d*\]/, function (v) {
                paramNum = v.slice(1, -1);
                return '';
            });

            // передача значения параметра ('true' если значение не задано)
            let paramValue = typeof(a[1]) === 'undefined' ? true : a[1];

            // преобразование регистра
            paramName = paramName.toLowerCase();
            paramValue = paramValue.toLowerCase();

            // если ключ параметра уже задан
            if (obj[paramName]) {
                // преобразуем текущее значение в массив
                if (typeof obj[paramName] === 'string') {
                    obj[paramName] = [obj[paramName]];
                }
                // если не задан индекс...
                if (typeof paramNum === 'undefined') {
                    // помещаем значение в конец массива
                    obj[paramName].push(paramValue);
                }
                // если индекс задан...
                else {
                    // размещаем элемент по заданному индексу
                    obj[paramName][paramNum] = paramValue;
                }
            }
            // если параметр не задан, делаем это вручную
            else {
                obj[paramName] = paramValue;
            }
        }
    }

    return obj;
}