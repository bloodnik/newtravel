<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Newtravel\Search\Tourvisor;
use Bitrix\Main\Loader;
use Bitrix\Main\Application;

class CSearchResult extends CBitrixComponent {

	//Запускаем компонент
	public function executeComponent() {
		$request = Application::getInstance()->getContext()->getRequest();

		// Подключение модуля
		Loader::includeModule("iblock");
		Loader::includeModule("catalog");
		Loader::includeModule("sale");
		Loader::includeModule("newtravel.search");

		$this->arResult["REQUEST_ID"] = $this->arParams["REQUEST_ID"];
		$tv                           = new Tourvisor();

		$cache_id = $this->arParams['REQUEST_ID'] . "_" . $this->arParams['PAGE'];

		if ($this->startResultCache($this->arParams['CACHE_TIME'], $cache_id)) {

			//Получаем баннеры
			$arSelect  = Array("ID", "NAME", "PREVIEW_PICTURE", "PREVIEW_TEXT");
			$arFilter  = array("IBLOCK_CODE" => "banner", "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$resBanner = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nTopCount" => 3), $arSelect);
			while ($arFields = $resBanner->GetNext()) {
				$this->arResult["BANNERS"][] = $arFields;
			}

			$onPage = $request->get("onpage")?:"25";
			$arData = $tv->getData("result", array("requestid" => $this->arParams['REQUEST_ID'], "page" => $this->arParams['PAGE'], "nodescription" => "0", "onpage"=>$onPage));

			$this->arResult["TOURS"]         = $arData["data"]["result"];
			$this->arResult["SEARCH_STATUS"] = $arData["data"]["status"];

			$arHotelCodes = array();
			foreach ($this->arResult["TOURS"]['hotel'] as $arHotel) {
				$arHotelCodes[] = $arHotel['hotelcode'];
			}

			$arHotelSpecial = array();
			$arSelect       = Array("ID", "NAME", "CODE", "PROPERTY_RECOMMEND", "PROPERTY_SALELEADER", "PROPERTY_TOP_TEN");
			$arFilter       = Array("IBLOCK_CODE" => "travel_hotel", "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "CODE" => $arHotelCodes);
			$res            = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while ($arFields = $res->GetNext()) {
				$arHotelSpecial[ $arFields['CODE'] ] = array(
					'RECOMMEND'  => $arFields['PROPERTY_RECOMMEND_VALUE'] == 'Да' ? true : false,
					'SALELEADER' => $arFields['PROPERTY_SALELEADER_VALUE'] == 'Да' ? true : false,
					'TOP_TEN'    => $arFields['PROPERTY_TOP_TEN_VALUE'] == 'Да' ? true : false,
				);
			}

			$this->arResult['HOTEL_SPECIAL'] = $arHotelSpecial;

			$this->includeComponentTemplate();

		}


		// Покупка тура
		$tourId = $request->get("tourid");
		if (isset($tourId) && ! empty($tourId)) {


			$arTour = $tv->getData("actualize", array("tourid" => $tourId, "flight" => 1));
			$arTour = $arTour["data"]["tour"];
			if (is_array($arTour) && ! empty($arTour)) {

				global $USER;

				//Очищаем корзину покупателя
				if (CModule::IncludeModule("sale")) {
					CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
				}


				// Добавляем тур в базу
				$el                 = new CIBlockElement;
				$PROP               = $arTour;
				$PROP["USER_ID"]    = $USER->GetID();
				$PROP["requestId"]  = $this->arResult["REQUEST_ID"];
				$PROP["tourid"]     = $tourId;
				$arLoadProductArray = Array(
					"MODIFIED_BY"       => $USER->GetID(),
					"IBLOCK_SECTION_ID" => false,
					"IBLOCK_ID"         => $this->arParams["TOURS_IBLOCK_ID"],
					"PROPERTY_VALUES"   => $PROP,
					"NAME"              => $arTour["departurename"] . "_" . $arTour["hotelregionname"] . "_" . $_REQUEST["tourid"],
					"tourname"          => $arTour["departurename"] . "_" . $arTour["hotelregionname"] . "_" . $_REQUEST["tourid"],
					"ACTIVE"            => "Y",
				);
				$PRODUCT_ID         = $el->Add($arLoadProductArray);

				// Проставляем количество
				$arFields = array(
					"ID"             => $PRODUCT_ID,
					"QUANTITY"       => 1,
					"QUANTITY_TRACE" => "N"
				);
				CCatalogProduct::Add($arFields);

				// Назначаем цену
				$arFields      = Array(
					"PRODUCT_ID"       => $PRODUCT_ID,
					"CATALOG_GROUP_ID" => 1,
					"PRICE"            => $arTour["price"],
					"CURRENCY"         => "RUB",
					"QUANTITY_FROM"    => false,
					"QUANTITY_TO"      => false
				);
				$PRODUCT_PRICE = CPrice::Add($arFields);

				// Добавляем тур в корзину
				$arFields          = array(
					"PRODUCT_ID"          => $PRODUCT_ID,
					"PRODUCT_PRICE_ID"    => $PRODUCT_PRICE,
					"PRICE"               => $arTour["price"],
					"CURRENCY"            => "RUB",
					"WEIGHT"              => 0,
					"QUANTITY"            => 1,
					"LID"                 => LANG,
					"DELAY"               => "N",
					"CAN_BUY"             => "Y",
					"NAME"                => $arTour["departurename"] . "_" . $arTour["hotelregionname"] . "_" . $tourId,
					"CALLBACK_FUNC"       => "CatalogBasketCallback",
					"MODULE"              => "catalog",
					"NOTES"               => "",
					"ORDER_CALLBACK_FUNC" => "MyBasketOrderCallback",
					"DETAIL_PAGE_URL"     => ""
				);
				$arFields["PROPS"] = array(
					0  => array(
						"NAME"  => "Количество звезд",
						"CODE"  => "STAR_NAME",
						"VALUE" => $arTour["hotelstars"]
					),
					1  => array(
						"NAME"  => "Количество взрослых",
						"CODE"  => "ADULTS",
						"VALUE" => $arTour["adults"]
					),
					2  => array(
						"NAME"  => "Дата вылета",
						"CODE"  => "CHECK_IN_DATE",
						"VALUE" => $arTour["flydate"],
						"SORT"  => "10"
					),
					3  => array(
						"NAME"  => "Ночей",
						"CODE"  => "NIGHTS",
						"VALUE" => $arTour["nights"],
						"SORT"  => "20"
					),
					4  => array(
						"NAME"  => "Страна",
						"CODE"  => "COUNTRY_NAME",
						"VALUE" => $arTour["countryname"]
					),
					5  => array(
						"NAME"  => "Курорт",
						"CODE"  => "RESORT_NAME",
						"VALUE" => $arTour["hotelregionname"],
						"SORT"  => "30"
					),
					6  => array(
						"NAME"  => "Отель",
						"CODE"  => "HOTEL_NAME",
						"VALUE" => $arTour["hotelname"]
					),
					7  => array(
						"NAME"  => "Рейтинг отеля",
						"CODE"  => "HOTEL_RATING",
						"VALUE" => $arTour["hotelrating"]
					),
					8  => array(
						"NAME"  => "Номер",
						"CODE"  => "HT_PLACE_DESCRIPTION",
						"VALUE" => $arTour["room"]
					),
					9  => array(
						"NAME"  => "Питание",
						"CODE"  => "MEAL_DESCRIPTION",
						"VALUE" => $arTour["meal"] . " " . $arTour["mealrussian"]
					),
					10 => array(
						"NAME"  => "Город вылета",
						"CODE"  => "CITY_FROM_NAME",
						"VALUE" => $arTour["departurename"]
					),
					11 => array(
						"NAME"  => "Количество детей",
						"CODE"  => "KIDS",
						"VALUE" => $arTour["child"]
					),
					12 => array(
						"NAME"  => "Размещение",
						"CODE"  => "PLACEMENT",
						"VALUE" => $arTour["placement"]
					),
					13 => array(
						"NAME"  => "TOUR ID",
						"CODE"  => "TOUR_ID",
						"VALUE" => $_REQUEST["tourid"]
					),

				);
				CSaleBasket::Add($arFields);

				$arResult["BUY"] = true;

			}
		}
	}
}

?>