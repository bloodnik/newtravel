<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


if(!CModule::IncludeModule("iblock"))
	return;

$arTypes = CIBlockParameters::GetIBlockTypes();

$arIBlocks=array();
$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => "Тип Инфоблока",
			"TYPE" => "LIST",
			"VALUES" => $arTypes,
			"DEFAULT" => "news",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => "ID каталога туров",
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => '',
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
		"REQUEST_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "Идентификатор запроса",
			"TYPE" => "STRING",
			"DEFAULT" => "1",
		),
		"COUNT" => Array(
			"PARENT" => "BASE",
			"NAME" => "Кол-во туров на странице",
			"TYPE" => "STRING",
			"DEFAULT" => "30",
		),
		"PAGE" => Array(
			"PARENT" => "BASE",
			"NAME" => "Номер запрашиваемой страницы",
			"TYPE" => "STRING",
			"DEFAULT" => "1",
		),
		"OFFER_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "Идентификатор предложения",
			"TYPE" => "STRING",
			"DEFAULT" => "1",
		),

		"CACHE_TIME"  =>  array("DEFAULT"=>36000000)
	),
);
?>