<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Newtravel\Search\Helpers;

\Bitrix\Main\Loader::includeModule('newtravel.search');
?>

<? if ( ! empty($arResult["TOURS"]["hotel"])): ?>
	<? foreach ($arResult["TOURS"]["hotel"] as $arHotel): ?>
		<div class="tours-list">
			<? foreach ($arHotel["tours"]["tour"] as $key => $arTour): ?>
				<div class="row <? if ($key >= 3): ?>hidden-tour<? endif; ?>" <? if ($key >= 3): ?>style="display: none;" <? endif; ?>>
					<div class="col-md-1 col-sm-1 hidden-xs">
						<img src="https://tourvisor.ru/pics/operators/searchlogo/<?=$arTour['operatorcode']?>.gif" alt="<?=$arTour['operatorname']?>"/>
					</div>
					<div class="col-md-2 col-sm-2 hidden-xs tourname">
						<?=$arTour["tourname"]?>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-4">
						<?=$arTour["flydate"]?><br>
						<?=$arTour["nights"]?> нч.
					</div>
					<div class="col-md-2 col-sm-2 col-xs-4 tourname">
						<?=$arTour["adults"]?> <i class="fa fa-male" aria-hidden="true"></i>
						<? if ($arTour["child"] > 0): ?>
							+ <?=$arTour["child"]?> <i class="fa fa-child" aria-hidden="true"></i>
						<? endif ?>
						<br>
						<?=$arTour["room"]?>
					</div>
					<div class="col-md-1 col-sm-2 col-xs-4">
						<?=$arTour["meal"]?> <?=$arTour["mealrussian"]?>
					</div>
					<div class="col-md-2 col-sm-2 price col-xs-8">
						<img src="https://tourvisor.ru/pics/operators/searchlogo/<?=$arTour['operatorcode']?>.gif" alt="<?=$arTour['operatorname']?>"
						     class="hidden-sm hidden-md hidden-lg"/>
											<span data-toggle="tooltip" data-placement="top" title='цена без скидки: <s><?=Helpers::getOldPrice($arTour["price"])?></s>'>
												<?=CurrencyFormat($arTour["price"], 'RUB')?>
											</span>
					</div>
					<div class="col-md-1 col-sm-1 col-xs-4" style="padding: 0;">
						<a href="/tours/tourdetail.php?tourid=<?=$arTour["tourid"]?>" onclick="yaCounter24395911.reachGoal('OPEN_TOUR_DETAIL_HOTEL')" data-toggle="modal"
						   data-target="#tourActulizeModal" class="btn btn-blue
											detailmodal_btn hidden-md hidden-lg">
							<i class="fa fa-chevron-right"></i>
						</a>
						<a href="/tours/tourdetail.php?tourid=<?=$arTour["tourid"]?>" onclick="yaCounter24395911.reachGoal('OPEN_TOUR_DETAIL_HOTEL')" data-toggle="modal"
						   data-target="#tourActulizeModal" class="btn btn-blue
											detailmodal_btn hidden-sm hidden-xs">
							Подробнее
						</a>
					</div>
				</div>
			<? endforeach; ?>
		</div>
	<? endforeach; ?>


<? elseif (empty($arResult["TOURS"]["hotel"]) && $arParams["IS_FINISH"] == "true"): //Если результать поиска пуст, выводим форму подбору тура?>
	<div class="col-xs-12 tours-section">
		<? $APPLICATION->IncludeComponent(
			"bitrix:form.result.new",
			"tour_help_notfound",
			Array(
				"AJAX_MODE"              => "Y",
				"SEF_MODE"               => "N",
				"WEB_FORM_ID"            => "6",
				"LIST_URL"               => "/tours",
				"EDIT_URL"               => "/tours",
				"SUCCESS_URL"            => "",
				"CHAIN_ITEM_TEXT"        => "",
				"CHAIN_ITEM_LINK"        => "",
				"IGNORE_CUSTOM_TEMPLATE" => "N",
				"USE_EXTENDED_ERRORS"    => "N",
				"CACHE_TYPE"             => "A",
				"CACHE_TIME"             => "3600",
				"VARIABLE_ALIASES"       => Array(
					"WEB_FORM_ID" => "WEB_FORM_ID",
					"RESULT_ID"   => "RESULT_ID"
				),
			),
			false
		); ?>

	</div>
<? endif; ?>

<script>
	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip({html: true});

		//обновлеям контент модального окна
		$(".detailmodal_btn").click(function (ev) {
			ev.preventDefault();
			var target = $(this).attr("href");
			$("#tourActulizeModal .modal-content").html('<div class="text-center" style="height: 200px; margin-top: 50px; color:#00AEEF"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');
			// load the url and show modal on success
			$("#tourActulizeModal .modal-content").load(target, function () {
				$("#tourActulizeModal").modal("show");
			});
		});

		$(".images").on("mouseout", function () {
			$("div[id^='fullImg']").css("display", "none");
		});
	});
</script>