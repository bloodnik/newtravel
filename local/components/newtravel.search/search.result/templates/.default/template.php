<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Newtravel\Search\Helpers;

\Bitrix\Main\Loader::includeModule("newtravel.search");

$this->setFrameMode(true);
$arBannersRange = array(
	2  => 0,
	7  => 1,
	13 => 2
);

?>

<? if ( ! empty($arResult["TOURS"]["hotel"])): ?>
	<div class="col-xs-12 text-center" style="margin-bottom: 5px">
		<a href="#" class="small hidden-xs" rel="nofollow" onclick="$('.tour-item-footer').slideToggle()">развернуть все туры</a>
		<span class="small" style="color: #cd0a0a; margin-left: 10px">цены указаны с учетом топливных сборов</span>
	</div>

	<div class="col-xs-12 tours-section">
		<!--Результаты поиска туров-->
		<div class="tour-list-wrap">
			<ul class="tour-list">
				<? foreach ($arResult["TOURS"]["hotel"] as $hotkey => $arHotel): ?>
					<li class="tour-item tour-item-<?=$arHotel['hotelcode']?>" data-hotel-code="<?=$arHotel['hotelcode']?>">
						<? if ($arResult['HOTEL_SPECIAL'][ $arHotel['hotelcode'] ]['RECOMMEND']): ?>
							<span class="special-badge recommend">Рекомендуем</span>
						<? endif; ?>
						<? if ($arResult['HOTEL_SPECIAL'][ $arHotel['hotelcode'] ]['SALELEADER']): ?>
							<span class="special-badge saleleader">Лидер продаж</span>
						<? endif; ?>
						<? if ($arResult['HOTEL_SPECIAL'][ $arHotel['hotelcode'] ]['TOP_TEN']): ?>
							<span class="special-badge top_ten">Топ-10</span>
						<? endif; ?>

						<!--Увеличенные изображения отеля-->
						<div id="fullImg_<?=$arHotel["hotelcode"]?>" style="z-index: 2; position: absolute; display: none;right: 1%;background-color: rgba(255, 255, 255, 0.8);width: 75%;height: 12%;">
							<img src="#" style="width: 400px;    margin-left: 10%;margin-top: 10%;"/>
						</div>

						<!--Шапка элемента тура-->
						<div class="col-xs-12 text-center col-md-2 col-sm-2 tour-img">
							<noindex>
								<a href="/hotel/<?=$arHotel["hotelcode"]?>/?start=Y" target="_blank" rel="nofollow" class="hotel-link">
									<img src="<?=$arHotel["picturelink"]?>" alt="<?=$arHotel["hotelname"]?>"/>
								</a>
							</noindex>
						</div>
						<div class="col-xs-12 col-md-7 col-sm-7">
							<div class="item-hotel-stars">
								<?
								switch ($arHotel['hotelstars']) {
									case "1":
										echo '<i class="fa fa-star"></i>';
										break;
									case "2":
										echo '<i class="fa fa-star"></i><i class="fa fa-star"></i>';
										break;
									case "3":
										echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
										break;
									case "4":
										echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
										break;
									case "5":
										echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
										break;
									default:
										echo "";
										break;
								}
								?>
							</div>
							<div class="item-hotel-name">
								<noindex><a target="_blank" href="/hotel/<?=$arHotel["hotelcode"]?>/?start=Y" rel="nofollow" class="hotel-link"><?=$arHotel["hotelname"]?></a></noindex>
							</div>
							<span class="tour-item-resort"><?=$arHotel["countryname"]?>, <?=$arHotel["regionname"]?></span>

							<div class="tour-item-info small">
								<p>
									<?=$arHotel["hoteldescription"]?>
								</p>
								<div class="tour-buttons">
									<noindex>
										<a href="javascript:void(0)" onclick="searchObject.openHotelDesc('<?=$arHotel["hotelcode"]?>')" rel="nofollow" class="hidden-xs btn btn-sm btn-white"
										   target="_blank">Об отеле</a>

										<a href="javascript:void(0)" onclick="$('.hotel_<?=$arHotel["hotelcode"]?>').slideUp();$('.tours_<?=$arHotel["hotelcode"]?>').slideToggle()"
										   class="btn btn-sm btn-white hidden-xs">Цены</a>
									</noindex>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-3 col-sm-3 right-group">
							<div class="row">
								<div class="col-md-12 col-sm-12 hidden-xs">
									<div class="row">
										<div class="item-rating text-left">
											<div class="small">
												Рейтинг
											</div>
											<span><?=$arHotel["hotelrating"]?></span>
										</div>
									</div>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="row">
										<div class="tour-item-min-price text-left small">
											Цена от:
											<span onclick="$('.hotel_<?=$arHotel["hotelcode"]?>').slideUp();
													$('.tours_<?=$arHotel["hotelcode"]?>').slideToggle();
													searchObject.setOpenedTours('.tours_<?=$arHotel["hotelcode"]?>')">
													<i style="display: block;font-size: 11px;">все цены с топливным сбором</i>
	                                                <small><?=$firstOldPrice = Helpers::getOldPrice($arHotel["price"])?></small><br>
												<?=CurrencyFormat($arHotel["price"], 'RUB')?>
												<i class="fa fa-chevron-down pull-right"></i>
                                                </span>
											за тур
										</div>
									</div>
								</div>
							</div>
						</div>

						<!--Подвал элемента тура-->
						<div class="col-md-12 tour-item-footer tours_<?=$arHotel["hotelcode"]?> col-xs-12" style="display: none">

							<div class="tours-list">
								<? foreach ($arHotel["tours"]["tour"] as $key => $arTour): ?>
									<div class="row">
										<div class="col-md-1 col-sm-1 hidden-xs">
											<img src="https://tourvisor.ru/pics/operators/searchlogo/<?=$arTour['operatorcode']?>.gif" alt="<?=$arTour['operatorname']?>"/>
										</div>
										<div class="col-md-2 col-sm-2 hidden-xs tourname" title="<?=$arTour["tourname"]?>">
											<?=$arTour["tourname"]?>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-4">
											<?=$arTour["flydate"]?><br>
											<?=$arTour["nights"]?> нч.
										</div>
										<div class="col-md-2 col-sm-2 col-xs-4 tourname" title="<?=$arTour["room"]?>">
											<?=$arTour["adults"]?> <i class="fa fa-male" aria-hidden="true"></i>
											<? if ($arTour["child"] > 0): ?>
												+ <?=$arTour["child"]?> <i class="fa fa-child" aria-hidden="true"></i>
											<? endif ?>
											<br>
											<?=$arTour["room"]?>
										</div>
										<div class="col-md-1 col-sm-2 col-xs-4 tourname" title="<?=$arTour["meal"]?> <?=$arTour["mealrussian"]?>">
											<?=$arTour["meal"]?> <?=$arTour["mealrussian"]?>
										</div>
										<div class="col-md-2 col-sm-2 price col-xs-8">
											<img src="https://tourvisor.ru/pics/operators/searchlogo/<?=$arTour['operatorcode']?>.gif" alt="<?=$arTour['operatorname']?>"
											     class="hidden-sm hidden-md hidden-lg"/>
											<span data-toggle="tooltip" data-placement="top" title='цена без скидки: <s><?=$key == 0 ? $firstOldPrice : Helpers::getOldPrice($arTour["price"])?></s>'>
												<a href="/tours/tourdetail.php?tourid=<?=$arTour["tourid"]?>" data-toggle="modal"
												   data-target="#tourActulizeModal">
													<?=CurrencyFormat($arTour["price"], 'RUB')?>
												</a>
											</span>
											<br>
											<a href="javascript:void(0)" class="add-to-basket__link" onclick="addToToursBasket(this, '<?=$arTour["tourid"]?>')">В корзину</a>
										</div>
										<div class="col-md-1 col-sm-1 col-xs-4" style="padding: 0;">
											<a href="/tours/tourdetail.php?tourid=<?=$arTour["tourid"]?>" onclick="yaCounter24395911.reachGoal('OPEN_TOUR_DETAIL')" data-toggle="modal"
											   data-target="#tourActulizeModal" class="btn btn-blue
											detailmodal_btn hidden-md hidden-lg">
												<i class="fa fa-chevron-right"></i>
											</a>
											<a href="/tours/tourdetail.php?tourid=<?=$arTour["tourid"]?>" onclick="yaCounter24395911.reachGoal('OPEN_TOUR_DETAIL')" data-toggle="modal"
											   data-target="#tourActulizeModal" class="btn btn-blue
											detailmodal_btn hidden-sm hidden-xs">
												Подробнее
											</a>
										</div>
									</div>
								<? endforeach; ?>
							</div>
							<div class="row text-center">
								<button class="btn btn-sm btn-white text-center"
								        style="position: relative;bottom: -5px;"
								        onclick="$('.tours_<?=$arHotel["hotelcode"]?>').slideUp(300);$.scrollTo('.tour-item-<?=$arHotel['hotelcode']?>', 300)">
									<i class="fa fa-chevreon-up"></i> свернуть
								</button>
							</div>
						</div>
						<!--/Подвал элемента тура-->

						<div class="col-md-12 tour-item-footer hotel_<?=$arHotel["hotelcode"]?> col-xs-12" style="display: none; padding-bottom: 15px">
							<hr/>
							<div class="col-md-3 images">

							</div>

							<div class="col-md-9 small">
								<p class="description"></p>
								<a target="_blank" href="/hotel/<?=$arHotel["hotelcode"]?>/?start=Y" rel="nofollow" class="hotel-link pull-right btn bnt-sm btn-blue" style="margin-bottom: 10px">Подробнее</a><br/>
							</div>
							<div class="col-md-12">
								<div id="mapintab_<?=$arHotel["hotelcode"]?>" style="height: 200px;"></div>
							</div>
						</div>
					</li>
					<? if (isset($arBannersRange[ $hotkey ])): ?>
						<? $pos1 = $arResult["BANNERS"][ $arBannersRange[ $hotkey ] ]["PREVIEW_TEXT"][0] === "#"; ?>
						<a href="<?=$arResult["BANNERS"][ $arBannersRange[ $hotkey ] ]["PREVIEW_TEXT"]?>" <?=! $pos1 ? "target='_blank'" : ""?>>
							<li class="tour-item banner" style="
									background: url('<?=CFile::GetPath($arResult["BANNERS"][ $arBannersRange[ $hotkey ] ]["PREVIEW_PICTURE"])?>');
									background-size: cover;
									background-position: 51%;"></li>
						</a>
					<? endif; ?>
				<? endforeach; ?>
			</ul>
		</div>
	</div>

<? elseif (empty($arResult["TOURS"]["hotel"]) && $arParams["IS_FINISH"] == "true"): //Если результаты поиска пустые, тогда показываем форму подбора тура?>
	<div class="col-xs-12 tours-section">
		<? $APPLICATION->IncludeComponent(
			"bitrix:form.result.new",
			"tour_help_notfound",
			Array(
				"AJAX_MODE"              => "Y",
				"SEF_MODE"               => "N",
				"WEB_FORM_ID"            => "6",
				"LIST_URL"               => "/tours",
				"EDIT_URL"               => "/tours",
				"SUCCESS_URL"            => "",
				"CHAIN_ITEM_TEXT"        => "",
				"CHAIN_ITEM_LINK"        => "",
				"IGNORE_CUSTOM_TEMPLATE" => "N",
				"USE_EXTENDED_ERRORS"    => "N",
				"CACHE_TYPE"             => "A",
				"CACHE_TIME"             => "3600",
				"VARIABLE_ALIASES"       => Array(
					"WEB_FORM_ID" => "WEB_FORM_ID",
					"RESULT_ID"   => "RESULT_ID"
				),
			),
			false
		); ?>
	</div>
<? endif; ?>

<script>
    $(document).ready(function () {
        //Открываем форму заказа звонка
        $('a[href="#callback"]').on('click', function () {
            $('#callBackModal').modal('show');
        });

        $('[data-toggle="tooltip"]').tooltip({html: true});

        //обновлеям контент модального окна
        $(".detailmodal_btn").click(function (ev) {
            ev.preventDefault();
            var target = $(this).attr("href");
            $("#tourActulizeModal .modal-content").html('<div class="text-center" style="height: 200px; margin-top: 50px; color:#00AEEF"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');
            // load the url and show modal on success
            $("#tourActulizeModal .modal-content").load(target, function () {
                $("#tourActulizeModal").modal("show");
            });
        });

        $(".images").on("mouseout", function () {
            $("div[id^='fullImg']").css("display", "none");
        });

    });

    function addToToursBasket(link, tourid) {
        $.get('/local/tools/newtravel.search.ajax.php', {type: "addToToursBasket", tourid: tourid}, function (response) {
            if(response === 'success' ){
                $(link).text('добавлено').addClass('text-success');
                if(window.tbs !== undefined){
                    window.tbs.getCount();
                }
            }
        });
    }
</script>