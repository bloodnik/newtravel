var search =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
    name: 'com-departure',
    props: ['departuresData', 'departure', 'hideRegular'],
    data() {
        return {
            regular: this.hideRegular,
            cityFromName: "Уфы",
            non_flight: {
                id: 99,
                name: "Без перелета",
                namefrom: "Без перелета"
            }
        };
    },
    methods: {
        setRegular: function () {
            this.$root.$data.hideRegular = this.regular;
        },

        //Устанавливаем ID города
        setDeparture: function (departureItem) {
            this.cityFromName = departureItem.namefrom;
            this.$root.$data.departure = departureItem.id;
        }
    },
    computed: {
        //Группируем города по буквам
        groupedDepartures: function () {
            return _.groupBy(this.departuresData, function (s) {
                return s.name.charAt();
            });
        }
    },
    watch: {
        departure: function () {
            this.cityFromName = _.find(this.departuresData, ['id', this.departure.toString()]) ? _.find(this.departuresData, ['id', this.departure.toString()]).namefrom : this.cityFromName;
        }
    }
});

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
    name: 'com-results',
    props: ['resultsData', 'searchUrl', 'toursNotFound', 'hideRegular'],
    data() {
        return {
            obHotelsDescr: {},
            obBanners: {
                //                    0: {
                //                        name: "Баннер 1",
                //                        img: "",
                //                    },
                //                    1: {
                //                        name: "Баннер 2",
                //                        img: "",
                //                    },
            }
        };
    },
    methods: {
        //Открывает и закрывает спиок туров
        openToursList: function (hotelId) {
            var tourList = $('#tours-' + hotelId);

            $('.hotel-description').slideUp();

            if (document.body.clientWidth <= 700) {
                $("html, body").animate({ scrollTop: $('#tours-' + hotelId).parent().offset().top + 300 }, 100);
            } else {
                $("html, body").animate({ scrollTop: $('#tours-' + hotelId).parent().offset().top - 50 }, 100);
            }

            if (tourList.hasClass('d-none')) {
                tourList.removeClass('d-none');
            } else {
                tourList.addClass('d-none');
            }

            //tourList.slideToggle();
        },

        openHotelDescr: function (hotelId) {
            var descrBlock = $('#descr-' + hotelId),
                hotelBtn = $('#hotelBtn-' + hotelId);

            if (!_.has(this.obHotelsDescr, hotelId)) {
                $(hotelBtn).addClass('running');

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "fullhotel", hotelcode: hotelId } }).then(function (response) {
                    if (response.body.data.hotel !== undefined) {
                        if (this.debug) {
                            console.log('======= Информация по отелю ========');
                        }
                        this.$set(this.obHotelsDescr, hotelId, response.body.data.hotel);

                        setTimeout(function () {
                            ymaps.ready(init);
                            function init() {
                                // Создание карты.
                                var myMap = new ymaps.Map("mapintab_" + hotelId, {
                                    center: [response.body.data.hotel.coord1, response.body.data.hotel.coord2],
                                    zoom: 16
                                });
                                var myPlacemark = new ymaps.Placemark([response.body.data.hotel.coord1, response.body.data.hotel.coord2], {
                                    hintContent: '',
                                    balloonContent: ''
                                }, {
                                    iconImageHref: '/local/templates/newtravel/images/mappin.png'
                                });
                                myMap.geoObjects.add(myPlacemark);
                            }

                            //                                var mapintab = new GMaps({
                            //                                    el: '#mapintab_' + hotelId,
                            //                                    height: 322,
                            //                                    width: 100,
                            //                                    scrollwheel: false,
                            //                                    lat: response.body.data.hotel.coord1,
                            //                                    lng: response.body.data.hotel.coord2
                            //                                });
                            //
                            //                                mapintab.addMarker({
                            //                                    lat: response.body.data.hotel.coord1,
                            //                                    lng: response.body.data.hotel.coord2,
                            //                                    clickable: false,
                            //                                    icon: "/local/templates/newtravel/images/mappin.png"
                            //                                });

                            $(".hotel-photos a").fancybox();

                            $("#hotelPhotos_" + hotelId).slick({
                                //autoplay: true,

                                arrows: false,
                                autoplaySpeed: 3000,
                                slidesToShow: 6,
                                slidesToScroll: 3,
                                responsive: [{
                                    breakpoint: 1024,
                                    settings: {
                                        slidesToShow: 4,
                                        slidesToScroll: 1,
                                        infinite: true,
                                        dots: false
                                    }
                                }, {
                                    breakpoint: 600,
                                    settings: {
                                        slidesToShow: 3,
                                        slidesToScroll: 2,
                                        dots: false
                                    }
                                }, {
                                    dots: false,
                                    arrows: false,
                                    breakpoint: 480,
                                    settings: {
                                        slidesToShow: 2,
                                        slidesToScroll: 1
                                    }
                                }]
                            });
                        }, 500);
                    } else {
                        if (this.debug) {
                            console.log("Ошибка fullhotel");
                        }
                    }
                    $(hotelBtn).removeClass('running');
                });
            }
            descrBlock.slideToggle();
        },

        //Детальная актуализация, взывает модальное окно
        getDetail: function (tourid) {
            var detailParams = {
                tourid: tourid
            };

            $('.detail-btn_' + tourid).addClass('running');

            var myModal = new jBox('Modal', {
                onClose: function () {
                    $('.jBox-Modal').remove();
                    //$('.jBox-overlay').remove();
                    $('.detail-btn_' + tourid).removeClass('running');
                }
            });

            myModal.open({
                responsiveWidth: true,
                responsiveHeight: true,
                width: "950",
                height: "900",
                closeButton: true,
                blockScroll: true,
                ajax: {
                    url: '/local/components/newtravel.search/full.search.v2/templates/.default/detail.php',
                    data: detailParams,
                    reload: 'strict',
                    spinner: true
                }
            });
        },

        //Добавление тура в корзину
        addTour2Basket: function (tourid, event) {
            this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "addToToursBasket", tourid: tourid } }).then(function (response) {
                if (response.body === 'success') {
                    $(event.target).text('Добавлено');
                    if (window.tbs !== undefined) {
                        window.tbs.getCount();
                    }
                }
            });
        }

    },
    computed: {
        grouppedResults: function () {
            var self = this;

            _.forEach(self.resultsData, function (hotel, key) {
                var minNightValue = _.minBy(hotel.tours.tour, function (o) {
                    return parseInt(o.nights);
                });
                var maxNightValue = _.maxBy(hotel.tours.tour, function (o) {
                    return parseInt(o.nights);
                });
                var minFlydateValue = _.minBy(hotel.tours.tour, function (o) {
                    return moment(o.flydate, 'DD.MM.YYYY').format('X');
                });
                var maxFlydateValue = _.maxBy(hotel.tours.tour, function (o) {
                    return moment(o.flydate, 'DD.MM.YYYY').format('X');
                });

                moment.locale('en');
                self.resultsData[key].minNight = minNightValue.nights;
                self.resultsData[key].maxNight = maxNightValue.nights;
                self.resultsData[key].minFlydate = moment(minFlydateValue.flydate, 'DD.MM.YYYY').format('DD.MM');
                self.resultsData[key].maxFlydate = moment(maxFlydateValue.flydate, 'DD.MM.YYYY').format('DD.MM');
                self.resultsData[key].type = "result";
            });

            //TODO Доделать баннеры
            //                var bannerPosition = [3, 7];
            //                _.forEach(bannerPosition, function (key) {
            //                    var newIndex = self.resultsData.push({type : 'banner', banner: self.obBanners[_.indexOf(bannerPosition, key)]});
            //                    var element = self.resultsData[newIndex-1];
            //                    self.resultsData.splice(newIndex-1, 1);
            //                    self.resultsData.splice(key, 0, element);
            //                });


            return self.resultsData;
        }
    },
    filters: {
        formatPrice: function (value) {
            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
        },

        getOldPrice: function (value) {
            value = parseInt(value) + parseInt(value * 0.11);
            return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
        },

        getCreditPrice: function (value) {
            value = (parseInt(value) + parseInt(value * 0.0717)) / 6;
            return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
        },

        shortText: function (value) {
            let sliced = value.slice(0, 100);
            if (sliced.length < value.length) {
                sliced += '...';
            }

            return sliced;
        },

        displayNights: function (value) {
            if (!value) {
                return "";
            } else {
                var titles = ['ночь', 'ночи', 'ночей'];
                value = parseInt(value);
                return value + " " + titles[value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2];
            }
        }

    }

});

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
    name: 'com-progress-bar',
    props: ['searchInAction', 'searchProgress', 'allRowsCount']
});

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

// import Vue from 'vue'
// import VueResource from 'vue-resource'
// Vue.use(VueResource);

__webpack_require__(6);

var _ComDeparture = __webpack_require__(11);

var _ComDeparture2 = _interopRequireDefault(_ComDeparture);

var _ComResults = __webpack_require__(13);

var _ComResults2 = _interopRequireDefault(_ComResults);

var _ComProgressBar = __webpack_require__(15);

var _ComProgressBar2 = _interopRequireDefault(_ComProgressBar);

var _vueHotelDatepicker = __webpack_require__(17);

var _vueHotelDatepicker2 = _interopRequireDefault(_vueHotelDatepicker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

$(document).ready(function () {

    window.searchApp = new Vue({
        el: '#search',
        data: {
            /*Текущие свойства*/
            departure: "4", //Текущий город
            country: "4", //Текущая страна
            regions: "", //Выбранные регионы
            subregions: "", //Выбранные субрегионы
            meal: "", //Выбранные типы питания
            stars: "", //Выбранные категории отеля
            hotels: "", //Выбранные отели
            adults: 2, //Количество взрослых
            child: 0, //Количество детей
            childage1: "0", //Возраст ребенка 1
            childage2: "0", //Возраст ребенка 2
            childage3: "0", //Возраст ребенка 3
            nightsfrom: 6, //Количество ночей от
            nightsto: 14, //Количество ночей до
            pricefrom: "", //Цена от
            priceto: "", //Цена до
            datefrom: moment().add(1, 'days').format('DD.MM.YYYY'), //Дата вылета от
            dateto: moment().add(14, 'days').format('DD.MM.YYYY'), //Дата вылета до
            operators: "", //Список операторов
            requestid: "", //ID запроса
            pageNumber: 1, //Номер страницы
            rating: "", //Рейтинг отеля
            hideRegular: true, //Рейтинг отеля

            /*Объекты*/
            obDepartures: [],
            obCountries: [],
            obRegions: [],
            obRegionsDisplay: [],
            obSubRegions: [],
            obStars: [],
            obOperators: [],
            obFlydates: [],
            obHotels: [],
            obMeals: [],
            obSearchResults: [],

            /*Временные объекты*/
            obMealsSelected: [],
            obRegionsSelected: {},
            obSubRegionsSelected: {},
            obHotelsSelected: {},
            obHotelsDescr: {},
            obOperatorsSelected: [],
            obMealsName: [],
            obOperatorsName: [],

            /*Наименования*/
            countryName: "Турция",
            regionsName: "",
            subRegionsName: "",
            hotelsName: "",
            touristString: "2 взр.",

            /*служебные*/
            pop_countries: [4, 1, 15, 5, 6, 20, 14, 47],
            toursNotFound: false,

            ruRU: {
                night: 'ночь',
                nights: 'ночей',
                'day-names': ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
                'check-in': 'С',
                'check-out': 'По',
                'month-names': ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
            },

            hotelSearchQuery: "",
            regionSearchQuery: "",
            countrySearchQuery: "",

            dateStart: null,
            dateEnd: null,

            /*поиск*/
            searchString: "", //поисковая строка страны
            allOperatorsProcessed: true, //Все ТО обработали запрос
            allRowsCount: 0, //Общее количество результатов
            firstRowsIsDisplayed: false, //Первая партия результатов загружена
            minPrice: 0, //Минимальная найденная цена
            searchProgress: 0, //Прогрес поиска
            searchInAction: false, //Флаг того, что поиск стартовал
            nextPageRequestInAction: false, //Флаг того, что идет запрос на следующую страницу туров
            toursIsOver: false, //Флаг того, что больше туров нет, показаны все страницы

            /*FastOrder*/
            isShowFastOrder: false,
            fastFormPhone: "",
            fastFormName: "",
            fastFormNameHasError: false,
            fastFormPhoneHasError: false,
            fastFormSended: false,

            debug: false,
            queryParams: [],
            searchUrl: "",
            mobile: false,
            showAdditionalParams: false

        },

        created: function created() {
            var _this = this;

            this.init();
            this.queryParams = getAllUrlParams(window.location.toString());

            //Автостарт поиска
            if (_this.queryParams.start === 'y') {
                setTimeout(function () {
                    _this.startSearch();
                }, 2000);
            }

            //Заполняем параметры из request
            if (this.queryParams.datefrom !== undefined) {
                this.datefrom = this.queryParams.datefrom;
            }
            if (this.queryParams.dateto !== undefined) {
                this.dateto = this.queryParams.dateto;
            }
            if (this.queryParams.nightsfrom !== undefined) {
                this.nightsfrom = this.queryParams.nightsfrom;
            }
            if (this.queryParams.nightsto !== undefined) {
                this.nightsto = this.queryParams.nightsto;
            }
            if (this.queryParams.pricefrom !== undefined) {
                this.pricefrom = this.queryParams.pricefrom;
            }
            if (this.queryParams.priceto !== undefined) {
                this.priceto = this.queryParams.priceto;
            }
            if (this.queryParams.rating !== undefined) {
                this.rating = this.queryParams.rating;
            }

            if (this.queryParams.adults !== undefined) {
                this.adults = this.queryParams.adults;
            }
            if (this.queryParams.child !== undefined) {
                this.child = this.queryParams.child;
                if (this.queryParams.childage1 !== undefined) {
                    this.childage1 = this.queryParams.childage1;
                }
                if (this.queryParams.childage2 !== undefined) {
                    this.childage2 = this.queryParams.childage2;
                }
                if (this.queryParams.childage3 !== undefined) {
                    this.childage3 = this.queryParams.childage3;
                }
            }
            if (this.queryParams.hideregular !== undefined) {
                this.hideRegular = this.queryParams.hideregular === 1;
            }

            this.dateStart = moment(this.datefrom, 'DD.MM.YYYY')._d;
            this.dateEnd = moment(this.dateto, 'DD.MM.YYYY')._d;

            // dunno why v-on="resize: func" not working
            if (document.body.clientWidth <= 700) {
                _this.$set(_this, 'mobile', true);
            } else {
                _this.$set(_this, 'mobile', false);
            }
            global.window.addEventListener('resize', function () {
                if (document.body.clientWidth <= 700) {
                    _this.$set(_this, 'mobile', true);
                } else {
                    _this.$set(_this, 'mobile', false);
                }
            });
        },

        methods: {
            init: function init() {
                var _this = this;

                _this.getDepartures();
                _this.getCountries();
                _this.getRegions();
                _this.getOperators();
                _this.getStars();
                _this.getFlydates();
                _this.getHotels();
                _this.getMeals();
            },

            getDepartures: function getDepartures() {
                var _this2 = this;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "departure" } }).then(function (response) {
                    _this2.obDepartures = response.body.lists.departures.departure;
                    _this2.obDepartures = _.sortBy(_this2.obDepartures, ['name']);

                    //Заполняем параметры из request
                    if (_this2.queryParams.departure !== undefined) {
                        _this2.departure = _this2.queryParams.departure;
                        _this2.cityFromName = _.find(_this2.obDepartures, ['id', _this2.queryParams.departure.toString()]) ? _.find(_this2.obDepartures, ['id', _this2.queryParams.departure.toString()]).namefrom : "Уфы";
                    } else if (BX.getCookie('NEWTRAVEL_USER_CITY') !== undefined) {
                        _this2.departure = BX.getCookie('NEWTRAVEL_USER_CITY');
                        _this2.cityFromName = _.find(_this2.obDepartures, ['id', BX.getCookie('NEWTRAVEL_USER_CITY').toString()]) ? _.find(_this2.obDepartures, ['id', BX.getCookie('NEWTRAVEL_USER_CITY').toString()]).namefrom : "Уфы";
                    }

                    //Подтвреждение города
                    // $('.departure_wrap').confirmCity({
                    //     departureModal: $('.cityFrom'),
                    //     departureId: this.departure
                    // });
                });
            },

            getCountries: function getCountries() {
                var _this3 = this;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "country", cndep: this.departure } }).then(function (response) {
                    if (response.body.lists.countries.country !== null) {
                        _this3.obCountries = response.body.lists.countries.country;
                        _this3.obCountries = _.sortBy(_this3.obCountries, ['name']);

                        //Заполняем параметры из request
                        if (_this3.queryParams.country !== undefined) {
                            _this3.country = _this3.queryParams.country;
                            _this3.countryName = _.find(_this3.obCountries, ['id', _this3.queryParams.country.toString()]) ? _.find(_this3.obCountries, ['id', _this3.queryParams.country.toString()]).name : "Турция";
                        }
                    }
                });
            },

            getRegions: function getRegions() {
                var _this4 = this;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "region", regcountry: this.country } }).then(function (response) {
                    var _this = _this4;

                    _this.obRegions = response.body.lists.regions.region;

                    //Заполняем параметры из request
                    if (_this.queryParams.regions !== undefined) {
                        var query = _this4.arrayConvert(_this.queryParams.regions);
                        _this.regions = query.join();
                        _.forEach(query, function (value, key) {
                            if (!_.find(_this.obRegions, ['id', value])) {
                                return;
                            }
                            _this.obRegionsSelected[value] = _.find(_this.obRegions, ['id', value]) ? _.find(_this.obRegions, ['id', value]) : "";
                        });
                        _this.regionsSelect();
                    }

                    _this.getSubRegions();
                });
            },

            getSubRegions: function getSubRegions() {
                var _this5 = this;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "subregion", regcountry: this.country } }).then(function (response) {
                    var _this = _this5;

                    setTimeout(function () {
                        _this.obRegionsDisplay = _this.obRegions;

                        _this.obSubRegions = response.body.lists.subregions.subregion;

                        _.forEach(_this.obSubRegions, function (value, key) {
                            //Ищем индекс родительского региона в массиве obRegions
                            var finded = _.findIndex(_this.obRegions, function (o) {
                                return o.id === value.parentregion;
                            });

                            //Если у данного родительского региона нет субрегионов, тогда добавляем ему объект subregions
                            if (_.isNil(_this.obRegions[parseInt(finded)].subregions)) {
                                _this.obRegions[parseInt(finded)].subregions = {};
                            }

                            //Заполняем объект subregions
                            _this.$set(_this.obRegions[parseInt(finded)].subregions, key, value);

                            _this.obRegionsDisplay = _this.obRegions;
                        });

                        //Заполняем параметры из request
                        if (_this.queryParams.subregions !== undefined) {
                            var query = this.arrayConvert(_this.queryParams.subregions);
                            _this.subregions = query.join();
                            _.forEach(query, function (value, key) {
                                if (!_.find(_this.obSubRegions, ['id', value])) {
                                    return;
                                }
                                _this.obSubRegionsSelected[value] = _.find(_this.obSubRegions, ['id', value]) ? _.find(_this.obSubRegions, ['id', value]) : "";
                            });
                            _this.subRegionsSelect();
                        }
                    }, 500);
                });
            },

            getStars: function getStars() {
                var _this6 = this;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "stars" } }).then(function (response) {
                    var _this = _this6;
                    _this6.obStars = response.body.lists.stars.star;

                    //Заполняем параметры из request
                    if (_this.queryParams.stars !== undefined) {
                        _this.stars = _this.queryParams.stars;
                    }
                });
            },

            getOperators: function getOperators() {
                var _this7 = this;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "operator" } }).then(function (response) {
                    var _this = _this7;
                    _this7.obOperators = response.body.lists.operators.operator;

                    //Заполняем параметры из request
                    if (_this.queryParams.operators !== undefined) {
                        var query = _this7.arrayConvert(_this.queryParams.operators);
                        _this.operators = query.join();
                        _.forEach(query, function (value, key) {
                            _this.operatorSelect(_.find(_this.obOperators, ['id', value]) ? _.find(_this.obOperators, ['id', value]) : "");
                        });
                    }
                });
            },

            getFlydates: function getFlydates() {
                var _this8 = this;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "flydate", flydeparture: this.departure, flycountry: this.country } }).then(function (response) {
                    if (response.body.lists.flydates !== null) {
                        _this8.obFlydates = response.body.lists.flydates.flydate;
                        _this8.datepickerAttrs[0].dates = _this8.obFlydates;
                    } else {
                        _this8.datepickerAttrs[0].dates = [];
                    }
                });
            },

            checkInChange: function checkInChange(date) {
                console.log(date);
                this.datefrom = moment(date).format('DD.MM.YYYY');
            },
            checkOutChange: function checkOutChange(date) {
                this.dateto = moment(date).format('DD.MM.YYYY');
            },

            getHotels: function getHotels() {
                var _this9 = this;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "hotel", hotcountry: this.country, hotregion: this.regions, hotstars: this.stars, hotrating: this.rating } }).then(function (response) {
                    var _this = _this9;
                    _this.obHotels = response.body.lists.hotels.hotel;

                    //Заполняем параметры из request
                    if (_this.queryParams.hotels !== undefined) {
                        var query = _this9.arrayConvert(_this.queryParams.hotels);

                        _this.hotels = query.join();
                        _.forEach(query, function (value, key) {
                            if (!_.find(_this.obHotels, ['id', value])) {
                                return;
                            }
                            _this.obHotelsSelected[value] = _.find(_this.obHotels, ['id', value]) ? _.find(_this.obHotels, ['id', value]) : "";
                        });
                        _this.hotelsSelect();
                    }
                });
            },

            getMeals: function getMeals() {
                var _this10 = this;

                var _this = this;
                _this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "meal" } }).then(function (response) {
                    _this.obMeals = response.body.lists.meals.meal;

                    //Заполняем параметры из request
                    if (_this.queryParams.meal !== undefined) {
                        var query = _this10.arrayConvert(_this.queryParams.regions);
                        _this.meal = query.join();
                        _.forEach(query, function (value, key) {
                            _this.mealsSelect(_.find(_this.obMeals, ['id', value]) ? _.find(_this.obMeals, ['id', value]) : "");
                        });
                    }
                });
            },

            /*ПОИСКОВЫЕ МЕТОДЫ*/

            //Начало поиска
            startSearch: function startSearch() {
                var _this11 = this;

                var $this = this;
                if (this.debug) {
                    console.log('============= Начинаем поиск =============');
                }

                this.searchInAction = true;
                this.obSearchResults = [];
                this.requestid = '';
                this.pageNumber = 1;
                this.firstRowsIsDisplayed = false;
                this.allRowsCount = 0;
                this.searchProgress = 0;
                this.queryParams = [];
                this.toursNotFound = false;

                var params = {
                    type: "search",
                    departure: this.departure,
                    country: this.country,
                    regions: this.regions,
                    subregions: this.subregions,
                    meal: this.meal,
                    stars: this.stars,
                    hotels: this.hotels,
                    adults: this.adults,
                    child: this.child,
                    childage1: this.childage1,
                    childage2: this.childage2,
                    childage3: this.childage3,
                    nightsfrom: this.nightsfrom,
                    nightsto: this.nightsto,
                    pricefrom: this.pricefrom,
                    priceto: this.priceto,
                    datefrom: this.datefrom,
                    dateto: this.dateto,
                    operators: this.operators,
                    rating: this.rating,
                    hideregular: this.hideRegular ? 1 : 0
                };

                $("html, body").animate({ scrollTop: $('#results').offset().top - 100 }, 1000);

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: params }).then(function (response) {
                    if (response.body.result.requestid !== undefined) {
                        _this11.requestid = response.body.result.requestid;
                        _this11.searchUrl = response.body.url;
                        history.pushState(_this11.requestid, "Поиск туров", "?start=Y&" + response.body.url);

                        setTimeout(function () {
                            $this.getStatus();
                        }, 3000);
                    } else {
                        if (_this11.debug) {
                            console.log("Ошибка startSearch");
                        }
                    }
                });
            },

            //Запрос состояние поиска
            getStatus: function getStatus() {
                var _this12 = this;

                if (this.debug) {
                    console.log('=========== Проверяем статус ==========');
                }
                var $this = this;
                var RowsCount = 0; //Общее количество результатов для цикла
                $this.getReaueIteration = 1;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "status", requestid: this.requestid, operatorstatus: 1 } }).then(function (response) {

                    if (response.body.data.status !== undefined) {
                        //Запрос прошел без ошибок

                        $this.searchProgress = response.body.data.status.progress; //Процент выполнения

                        RowsCount = response.body.data.status.hotelsfound;

                        if ($this.allRowsCount !== RowsCount && !$this.firstRowsIsDisplayed) {
                            //Если количество найденных результатов за цикл не равна уже найденным результатам и первая партия еще не отображена, тогда выводим первую партию результатов
                            $this.firstRowsIsDisplayed = true;
                            if (_this12.debug) {
                                console.log("==Выводим первые результаты на экран==");
                            }
                            $this.getResult();
                        }

                        if (parseInt($this.searchProgress) !== 100) {
                            //Повторяем проверку статуса
                            setTimeout(function () {
                                if (this.debug) {
                                    console.log("Не все операторы отработали, повторяем запрос статусов");
                                }
                                $this.getStatus();
                            }, 2000);
                        } else {
                            if (_this12.debug) {
                                console.log("==Все обработно! Урра!!!==");
                                console.log("==Выводим все результаты на экран==");
                            }
                            $this.searchProgress = 100;
                            $this.searchInAction = false;
                            $this.getResult();
                        }

                        $this.allRowsCount = response.body.data.status.hotelsfound; //Общее количество результатов
                    } else {
                        if (_this12.debug) {
                            console.log("Ошибка getStatus");
                        }
                    }
                });
            },

            //Получение результатов поиска
            getResult: function getResult() {
                var _this13 = this;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "result", requestid: this.requestid, page: this.pageNumber } }).then(function (response) {
                    if (response.body.data.status.state === 'finished' && parseInt(response.body.data.status.toursfound) === 0) {
                        _this13.toursNotFound = true;
                    }

                    if (response.body.data.result !== undefined) {
                        if (_this13.debug) {
                            console.log('======= Получите результаты ========');
                        }

                        if (_this13.pageNumber === 1) {
                            _this13.obSearchResults = response.body.data.result.hotel;
                        } else {
                            _this13.obSearchResults = _.concat(_this13.obSearchResults, response.body.data.result.hotel);
                            _this13.nextPageRequestInAction = false;
                        }

                        setTimeout(function () {
                            $('[data-toggle="popover"]').popover({
                                trigger: 'hover | click',
                                container: $('body')
                            });
                        }, 500);
                    } else {
                        if (_this13.pageNumber > 1) {
                            _this13.nextPageRequestInAction = false;
                            _this13.toursIsOver = true;
                        }
                        if (_this13.debug) {
                            console.log("Ошибка getResult");
                        }
                    }
                });
            },

            loadMoreTours: function loadMoreTours() {
                this.nextPageRequestInAction = true;
                this.pageNumber = this.pageNumber + 1;
                this.getResult();
            },

            /*Доп. методы*/
            /**
             * @return {string}
             */
            FlagClass: function FlagClass(index) {
                return "values__image flag-ui_narrowtpl_flags_30x20_" + index;
            },

            /**
             * Фильтр популярных стран
             * @return {boolean}
             */
            IsPopularCountry: function IsPopularCountry(country) {
                return _.includes(this.pop_countries, parseInt(country));
            },

            regionsSelect: function regionsSelect() {
                var region = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";

                var self = this;

                if (region !== "") {
                    !_.find(self.obRegionsSelected, ['id', region.id]) ? self.obRegionsSelected[region.id.toString()] = { id: region.id, name: region.name } : delete self.obRegionsSelected[region.id.toString()];

                    //Очищаем все субрегионы при выборе родительского региона
                    var finded = _.findIndex(self.obRegionsDisplay, function (o) {
                        return o.id === region.id;
                    });
                    if (!_.isNil(self.obRegionsDisplay[finded].subregions)) {
                        _.forEach(self.obRegionsDisplay[finded].subregions, function (value, key) {
                            delete self.obSubRegionsSelected[value.id];
                        });
                        self.subRegionsSelect();
                    }
                }
                self.regions = _.map(self.obRegionsSelected, 'id').join();
                self.regionsName = _.map(self.obRegionsSelected, 'name');
            },

            subRegionsSelect: function subRegionsSelect() {
                var subregion = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";


                //Очищаем родительский регион если выбран субрегион
                if (this.obRegionsSelected[subregion.parentregion] !== undefined) {
                    delete this.obRegionsSelected[subregion.parentregion];
                    this.regionsSelect();
                }

                if (subregion !== "") {
                    !_.find(this.obSubRegionsSelected, ['id', subregion.id]) ? this.obSubRegionsSelected[subregion.id] = { id: subregion.id, name: subregion.name } : delete this.obSubRegionsSelected[subregion.id];
                }
                this.subregions = _.map(this.obSubRegionsSelected, 'id').join();
                this.subRegionsName = _.map(this.obSubRegionsSelected, 'name');
            },

            hotelsSelect: function hotelsSelect() {
                var hotel = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";

                if (hotel !== "") {
                    !_.find(this.obHotelsSelected, ['id', hotel.id]) ? this.obHotelsSelected[hotel.id] = { id: hotel.id, name: hotel.name } : delete this.obHotelsSelected[hotel.id];
                }
                this.hotels = _.map(this.obHotelsSelected, 'id').join();
                this.hotelsName = _.map(this.obHotelsSelected, 'name');
            },

            //Питание для запроса
            mealsSelect: function mealsSelect() {
                var meal = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";

                if (meal !== "") {
                    !_.includes(this.obMealsSelected, meal.id) ? this.obMealsSelected.push(meal.id) : _.pull(this.obMealsSelected, meal.id);
                    !_.includes(this.obMealsName, meal.name) ? this.obMealsName.push(meal.name) : _.pull(this.obMealsName, meal.name);
                }
                this.meal = this.obMealsSelected.join();
            },

            //Туроператоры для запроса
            operatorSelect: function operatorSelect(operator) {
                !_.includes(this.obOperatorsSelected, operator.id) ? this.obOperatorsSelected.push(operator.id) : _.pull(this.obOperatorsSelected, operator.id);
                !_.includes(this.obOperatorsName, operator.name) ? this.obOperatorsName.push(operator.name) : _.pull(this.obOperatorsName, operator.name);
                this.operators = this.obOperatorsSelected.join();
            },

            /*Формировние строки с туристами*/
            setTouristsString: function setTouristsString() {
                var self = this;

                self.touristString = self.adults + " взр. ";
                if (self.child > 0) {
                    self.touristString += self.child + " реб.(";
                    for (var i = 1; i <= self.child; i++) {
                        self.touristString += self['childage' + i];
                        if (i < self.child) {
                            self.touristString += ' и ';
                        }
                    }
                    self.touristString += ")";
                }
            },

            fastOrder: function fastOrder() {
                var _this14 = this;

                this.fastFormSended = false;
                var sessidval = $("#sessid").val(),
                    tourid = $("input[name=fastTourId]").val(),
                    credit = $("input[name=credit]").val();

                this.fastFormPhoneHasError = this.fastFormPhone.length <= 0;
                this.fastFormNameHasError = this.fastFormName.length <= 0;

                if (!this.fastFormPhoneHasError && !this.fastFormNameHasError && tourid.length > 0) {

                    this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {
                        params: {
                            type: "fastorder",
                            tourid: tourid,
                            phone: this.fastFormPhone,
                            name: this.fastFormName,
                            sessid: sessidval,
                            credit: credit
                        }
                    }).then(function (response) {
                        if (response.body.ERROR === "" && response.body.STATUS) {
                            yaCounter24395911.reachGoal('FAST_ORDER_DETAIL_TOUR');
                            _this14.fastFormSended = true;

                            setTimeout(function () {
                                $('#fastOrderModal').modal('hide');
                            }, 1000);
                        }
                    });
                }
            },

            //Комвертируем слова в массив
            arrayConvert: function arrayConvert(query) {
                var arQuery = void 0;

                if (_.isString(query)) {
                    arQuery = [query];
                } else {
                    arQuery = query;
                }
                return arQuery;
            },

            closeDropdown: function closeDropdown(target) {
                $('#' + target).dropdown('toggle');
            }

        },

        watch: {
            departure: function departure() {
                this.getCountries();
                this.getRegions();
                this.getHotels();
                this.getFlydates();

                BX.setCookie('NEWTRAVEL_USER_CITY', this.departure, { path: '/' });
            },
            country: function country(newCountry) {
                this.getRegions();
                this.getHotels();
                this.getFlydates();
                this.obRegionsSelected = {};
                this.regionsName = "";
                this.regions = "";
                this.obSubRegionsSelected = {};
                this.subregions = "";
                this.subRegionsName = "";
                this.obHotelsSelected = {};
                this.hotelsName = "";
                this.hotels = "";
            },
            regions: function regions() {
                this.getHotels();
            },
            stars: function stars() {
                this.getHotels();
            },
            rating: function rating() {
                this.getHotels();
            },
            adults: function adults() {
                this.setTouristsString();
            },
            child: function child() {
                this.setTouristsString();
            },
            childage1: function childage1() {
                this.setTouristsString();
            },
            childage2: function childage2() {
                this.setTouristsString();
            },
            childage3: function childage3() {
                this.setTouristsString();
            }
        },

        computed: {
            // groupedDepartures: function () {
            //     return _.groupBy(this.obDepartures, function (s) {
            //         return s.name.charAt();
            //     });
            // },
            groupedCountries: function groupedCountries() {
                return _.groupBy(this.obCountries, function (s) {
                    return s.name.charAt();
                });
            },
            filteredHotels: function filteredHotels() {
                var self = this,
                    filtered = [];

                if (self.obHotels !== null) {
                    filtered = self.obHotels.filter(function (hotel) {
                        return hotel.name.toLowerCase().indexOf(self.hotelSearchQuery.toLowerCase()) !== -1;
                    });
                }

                return filtered.slice(0, 300);
            },
            filteredRegions: function filteredRegions() {
                var self = this;
                return self.obRegionsDisplay.filter(function (region) {
                    return region.name.toLowerCase().indexOf(self.regionSearchQuery.toLowerCase()) !== -1;
                });
            },
            filteredCountries: function filteredCountries() {
                var self = this;
                return self.obCountries.filter(function (country) {
                    return country.name.toLowerCase().indexOf(self.countrySearchQuery.toLowerCase()) !== -1;
                });
            }
        },

        filters: {
            displayAge: function displayAge(value) {
                if (!value) {
                    return "";
                } else if (parseInt(value) === 1) {
                    return "< 2 лет";
                } else {
                    var titles = ['год', 'года', 'лет'];
                    value = parseInt(value);
                    return value + " " + titles[value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2];
                }
            },

            displayAgeInt: function displayAgeInt(value) {
                if (parseInt(value) >= 2) {
                    return value;
                } else if (parseInt(value) <= 1) {
                    return "1";
                }
            },

            displayNights: function displayNights(value) {
                if (!value) {
                    return "";
                } else {
                    var titles = ['ночь', 'ночи', 'ночей'];
                    value = parseInt(value);
                    return value + " " + titles[value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2];
                }
            },

            shortName: function shortName(value, type) {
                if (value) {
                    if (value.length > 1) {
                        var titles = [];
                        if (type == 'region') {
                            titles = ['курорт', 'курорта', 'курортов'];
                        } else if (type == 'hotel') {
                            titles = ['отель', 'отеля', 'отелей'];
                        } else if (type == 'subregion') {
                            titles = ['поселок', 'поселка', 'поселков'];
                        }

                        value = value.length;
                        return value + " " + titles[value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2];
                    } else if (value.length == 1) {
                        return value.toString();
                    }
                }
            },

            ratingDisplayed: function ratingDisplayed(value) {
                var rating = 'Любой';
                switch (value) {
                    case 5:
                        rating = 'от ' + 4.5;
                        break;
                    case 4:
                        rating = 'от ' + 4;
                        break;
                    case 3:
                        rating = 'от ' + 3.5;
                        break;
                    case 2:
                        rating = 'от ' + 3;
                        break;
                    case 0:
                        rating = 'Любой';
                        break;
                }
                return rating;
            },

            formatPrice: function formatPrice(value) {
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
            }

        },

        components: {
            ComResults: _ComResults2.default,
            ComProgressBar: _ComProgressBar2.default,
            ComDeparture: _ComDeparture2.default,
            HotelDatePicker: _vueHotelDatepicker2.default
        }

    });

    $('.select-dropdown.no-close').click(function (event) {
        event.stopPropagation();
    });
});

function getAllUrlParams(url) {

    // извлекаем строку из URL или объекта window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // объект для хранения параметров
    var obj = {};

    // если есть строка запроса
    if (queryString) {

        // данные после знака # будут опущены
        queryString = queryString.split('#')[0];

        // разделяем параметры
        var arr = queryString.split('&');

        for (var i = 0; i < arr.length; i++) {
            // разделяем параметр на ключ => значение
            var a = arr[i].split('=');

            // обработка данных вида: list[]=thing1&list[]=thing2
            var paramNum = undefined;
            var paramName = a[0].replace(/\[\d*\]/, function (v) {
                paramNum = v.slice(1, -1);
                return '';
            });

            // передача значения параметра ('true' если значение не задано)
            var paramValue = typeof a[1] === 'undefined' ? true : a[1];

            // преобразование регистра
            paramName = paramName.toLowerCase();
            paramValue = paramValue.toLowerCase();

            // если ключ параметра уже задан
            if (obj[paramName]) {
                // преобразуем текущее значение в массив
                if (typeof obj[paramName] === 'string') {
                    obj[paramName] = [obj[paramName]];
                }
                // если не задан индекс...
                if (typeof paramNum === 'undefined') {
                    // помещаем значение в конец массива
                    obj[paramName].push(paramValue);
                }
                // если индекс задан...
                else {
                        // размещаем элемент по заданному индексу
                        obj[paramName][paramNum] = paramValue;
                    }
            }
            // если параметр не задан, делаем это вручную
            else {
                    obj[paramName] = paramValue;
                }
        }
    }

    return obj;
}
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(5)))

/***/ }),
/* 5 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(7);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(9)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../css-loader/index.js!./v-calendar.min.css", function() {
		var newContent = require("!!../../css-loader/index.js!./v-calendar.min.css");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(false);
// imports


// module
exports.push([module.i, "@-webkit-keyframes scaleEnter-data-v-bc55024c{0%{-webkit-transform:scaleX(.7) scaleY(.7);transform:scaleX(.7) scaleY(.7);opacity:.3}90%{-webkit-transform:scaleX(1.1) scaleY(1.1);transform:scaleX(1.1) scaleY(1.1)}95%{-webkit-transform:scaleX(.95) scaleY(.95);transform:scaleX(.95) scaleY(.95)}to{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1);opacity:1}}@keyframes scaleEnter-data-v-bc55024c{0%{-webkit-transform:scaleX(.7) scaleY(.7);transform:scaleX(.7) scaleY(.7);opacity:.3}90%{-webkit-transform:scaleX(1.1) scaleY(1.1);transform:scaleX(1.1) scaleY(1.1)}95%{-webkit-transform:scaleX(.95) scaleY(.95);transform:scaleX(.95) scaleY(.95)}to{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1);opacity:1}}@-webkit-keyframes scaleLeave-data-v-bc55024c{0%{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1)}60%{-webkit-transform:scaleX(1.18) scaleY(1.18);transform:scaleX(1.18) scaleY(1.18);opacity:.2}to{-webkit-transform:scaleX(1.15) scaleY(1.18);transform:scaleX(1.15) scaleY(1.18);opacity:0}}@keyframes scaleLeave-data-v-bc55024c{0%{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1)}60%{-webkit-transform:scaleX(1.18) scaleY(1.18);transform:scaleX(1.18) scaleY(1.18);opacity:.2}to{-webkit-transform:scaleX(1.15) scaleY(1.18);transform:scaleX(1.15) scaleY(1.18);opacity:0}}@-webkit-keyframes slideRightScaleEnter-data-v-bc55024c{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@keyframes slideRightScaleEnter-data-v-bc55024c{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@-webkit-keyframes slideRightTranslateEnter-data-v-bc55024c{0%{-webkit-transform:translateX(-6px);transform:translateX(-6px)}60%{-webkit-transform:translateX(2px);transform:translateX(2px)}}@keyframes slideRightTranslateEnter-data-v-bc55024c{0%{-webkit-transform:translateX(-6px);transform:translateX(-6px)}60%{-webkit-transform:translateX(2px);transform:translateX(2px)}}@-webkit-keyframes slideLeftScaleEnter-data-v-bc55024c{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@keyframes slideLeftScaleEnter-data-v-bc55024c{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@-webkit-keyframes slideLeftTranslateEnter-data-v-bc55024c{0%{-webkit-transform:translateX(6px);transform:translateX(6px)}60%{-webkit-transform:translateX(-2px);transform:translateX(-2px)}}@keyframes slideLeftTranslateEnter-data-v-bc55024c{0%{-webkit-transform:translateX(6px);transform:translateX(6px)}60%{-webkit-transform:translateX(-2px);transform:translateX(-2px)}}.c-pane-container[data-v-bc55024c]{-ms-flex-negative:1;flex-shrink:1;display:-webkit-inline-box;display:-ms-inline-flexbox;display:inline-flex;font-family:BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:400;line-height:1.5;color:#393d46;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;box-sizing:border-box}.c-pane-container.is-expanded[data-v-bc55024c]{width:100%}.c-pane-container.is-vertical[data-v-bc55024c]{-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column}.c-pane-container[data-v-bc55024c] *{box-sizing:inherit}.c-pane-container[data-v-bc55024c] :focus{outline:none}.c-pane-divider[data-v-bc55024c]{width:1px;border:1px inset;border-color:#fafafa}@-webkit-keyframes scaleEnter-data-v-2083cb72{0%{-webkit-transform:scaleX(.7) scaleY(.7);transform:scaleX(.7) scaleY(.7);opacity:.3}90%{-webkit-transform:scaleX(1.1) scaleY(1.1);transform:scaleX(1.1) scaleY(1.1)}95%{-webkit-transform:scaleX(.95) scaleY(.95);transform:scaleX(.95) scaleY(.95)}to{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1);opacity:1}}@-webkit-keyframes scaleLeave-data-v-2083cb72{0%{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1)}60%{-webkit-transform:scaleX(1.18) scaleY(1.18);transform:scaleX(1.18) scaleY(1.18);opacity:.2}to{-webkit-transform:scaleX(1.15) scaleY(1.18);transform:scaleX(1.15) scaleY(1.18);opacity:0}}@-webkit-keyframes slideRightScaleEnter-data-v-2083cb72{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@-webkit-keyframes slideRightTranslateEnter-data-v-2083cb72{0%{-webkit-transform:translateX(-6px);transform:translateX(-6px)}60%{-webkit-transform:translateX(2px);transform:translateX(2px)}}@-webkit-keyframes slideLeftScaleEnter-data-v-2083cb72{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@-webkit-keyframes slideLeftTranslateEnter-data-v-2083cb72{0%{-webkit-transform:translateX(6px);transform:translateX(6px)}60%{-webkit-transform:translateX(-2px);transform:translateX(-2px)}}@keyframes scaleEnter-data-v-2083cb72{0%{-webkit-transform:scaleX(.7) scaleY(.7);transform:scaleX(.7) scaleY(.7);opacity:.3}90%{-webkit-transform:scaleX(1.1) scaleY(1.1);transform:scaleX(1.1) scaleY(1.1)}95%{-webkit-transform:scaleX(.95) scaleY(.95);transform:scaleX(.95) scaleY(.95)}to{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1);opacity:1}}@keyframes scaleLeave-data-v-2083cb72{0%{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1)}60%{-webkit-transform:scaleX(1.18) scaleY(1.18);transform:scaleX(1.18) scaleY(1.18);opacity:.2}to{-webkit-transform:scaleX(1.15) scaleY(1.18);transform:scaleX(1.15) scaleY(1.18);opacity:0}}@keyframes slideRightScaleEnter-data-v-2083cb72{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@keyframes slideRightTranslateEnter-data-v-2083cb72{0%{-webkit-transform:translateX(-6px);transform:translateX(-6px)}60%{-webkit-transform:translateX(2px);transform:translateX(2px)}}@keyframes slideLeftScaleEnter-data-v-2083cb72{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@keyframes slideLeftTranslateEnter-data-v-2083cb72{0%{-webkit-transform:translateX(6px);transform:translateX(6px)}60%{-webkit-transform:translateX(-2px);transform:translateX(-2px)}}.c-pane[data-v-2083cb72]{-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;-ms-flex-negative:1;flex-shrink:1;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:stretch;-ms-flex-align:stretch;align-items:stretch}.c-horizontal-divider[data-v-2083cb72]{-ms-flex-item-align:center;align-self:center}.c-header[data-v-2083cb72]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:stretch;-ms-flex-align:stretch;align-items:stretch;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;padding:10px}.c-header .c-arrow-layout[data-v-2083cb72]{min-width:26px}.c-header .c-arrow-layout .c-arrow[data-v-2083cb72],.c-header .c-arrow-layout[data-v-2083cb72]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;margin:0;padding:0}.c-header .c-arrow-layout .c-arrow[data-v-2083cb72]{font-size:1.6rem;transition:fill-opacity .3s ease-in-out;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.c-header .c-arrow-layout .c-arrow[data-v-2083cb72]:hover{fill-opacity:.5}.c-header .c-title-layout[data-v-2083cb72]{display:-webkit-inline-box;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1}.c-header .c-title-layout .c-title-popover .c-title-anchor[data-v-2083cb72],.c-header .c-title-layout .c-title-popover[data-v-2083cb72]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:inherit;-ms-flex-pack:inherit;justify-content:inherit}.c-header .c-title-layout .c-title-popover .c-title-anchor .c-title[data-v-2083cb72]{font-weight:400;font-size:1.15rem;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;white-space:nowrap}.c-header .c-title-layout.align-left[data-v-2083cb72]{-webkit-box-ordinal-group:0;-ms-flex-order:-1;order:-1;-webkit-box-pack:start;-ms-flex-pack:start;justify-content:flex-start}.c-header .c-title-layout.align-right[data-v-2083cb72]{-webkit-box-ordinal-group:2;-ms-flex-order:1;order:1;-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end}.c-header .c-arrow.c-disabled[data-v-2083cb72]{cursor:not-allowed;pointer-events:none;opacity:.2}.c-weekdays[data-v-2083cb72]{display:-webkit-box;display:-ms-flexbox;display:flex;padding:0 5px;color:#9499a8;font-size:.9rem;font-weight:500}.c-weekday[data-v-2083cb72]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;margin:0;padding:0;-webkit-box-flex:1;-ms-flex:1;flex:1;cursor:default}.c-weeks[data-v-2083cb72]{-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;padding:5px 5px 7px}.c-weeks-rows-wrapper[data-v-2083cb72]{position:relative}.c-weeks-rows[data-v-2083cb72]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;width:100%}.title-fade-enter-active[data-v-2083cb72],.title-fade-leave-active[data-v-2083cb72],.title-slide-down-enter-active[data-v-2083cb72],.title-slide-down-leave-active[data-v-2083cb72],.title-slide-left-enter-active[data-v-2083cb72],.title-slide-left-leave-active[data-v-2083cb72],.title-slide-right-enter-active[data-v-2083cb72],.title-slide-right-leave-active[data-v-2083cb72],.title-slide-up-enter-active[data-v-2083cb72],.title-slide-up-leave-active[data-v-2083cb72]{transition:all .25s ease-in-out}.title-fade-leave-active[data-v-2083cb72],.title-none-leave-active[data-v-2083cb72],.title-slide-down-leave-active[data-v-2083cb72],.title-slide-left-leave-active[data-v-2083cb72],.title-slide-right-leave-active[data-v-2083cb72],.title-slide-up-leave-active[data-v-2083cb72]{position:absolute}.title-none-enter-active[data-v-2083cb72],.title-none-leave-active[data-v-2083cb72]{transition-duration:0s}.title-slide-left-enter[data-v-2083cb72],.title-slide-right-leave-to[data-v-2083cb72]{opacity:0;-webkit-transform:translateX(25px);transform:translateX(25px)}.title-slide-left-leave-to[data-v-2083cb72],.title-slide-right-enter[data-v-2083cb72]{opacity:0;-webkit-transform:translateX(-25px);transform:translateX(-25px)}.title-slide-down-leave-to[data-v-2083cb72],.title-slide-up-enter[data-v-2083cb72]{opacity:0;-webkit-transform:translateY(20px);transform:translateY(20px)}.title-slide-down-enter[data-v-2083cb72],.title-slide-up-leave-to[data-v-2083cb72]{opacity:0;-webkit-transform:translateY(-20px);transform:translateY(-20px)}.weeks-fade-enter-active[data-v-2083cb72],.weeks-fade-leave-active[data-v-2083cb72],.weeks-slide-down-enter-active[data-v-2083cb72],.weeks-slide-down-leave-active[data-v-2083cb72],.weeks-slide-left-enter-active[data-v-2083cb72],.weeks-slide-left-leave-active[data-v-2083cb72],.weeks-slide-right-enter-active[data-v-2083cb72],.weeks-slide-right-leave-active[data-v-2083cb72],.weeks-slide-up-enter-active[data-v-2083cb72],.weeks-slide-up-leave-active[data-v-2083cb72]{transition:all .25s ease-in-out}.weeks-fade-leave-active[data-v-2083cb72],.weeks-none-leave-active[data-v-2083cb72],.weeks-slide-down-leave-active[data-v-2083cb72],.weeks-slide-left-leave-active[data-v-2083cb72],.weeks-slide-right-leave-active[data-v-2083cb72],.weeks-slide-up-leave-active[data-v-2083cb72]{position:absolute}.weeks-none-enter-active[data-v-2083cb72],.weeks-none-leave-active[data-v-2083cb72]{transition-duration:0s}.weeks-slide-left-enter[data-v-2083cb72],.weeks-slide-right-leave-to[data-v-2083cb72]{opacity:0;-webkit-transform:translateX(20px);transform:translateX(20px)}.weeks-slide-left-leave-to[data-v-2083cb72],.weeks-slide-right-enter[data-v-2083cb72]{opacity:0;-webkit-transform:translateX(-20px);transform:translateX(-20px)}.weeks-slide-down-leave-to[data-v-2083cb72],.weeks-slide-up-enter[data-v-2083cb72]{opacity:0;-webkit-transform:translateY(20px);transform:translateY(20px)}.weeks-slide-down-enter[data-v-2083cb72],.weeks-slide-up-leave-to[data-v-2083cb72]{opacity:0;-webkit-transform:translateY(-20px);transform:translateY(-20px)}.title-fade-enter[data-v-2083cb72],.title-fade-leave-to[data-v-2083cb72],.title-none-enter[data-v-2083cb72],.title-none-leave-to[data-v-2083cb72],.weeks-fade-enter[data-v-2083cb72],.weeks-fade-leave-to[data-v-2083cb72],.weeks-none-enter[data-v-2083cb72],.weeks-none-leave-to[data-v-2083cb72]{opacity:0}@-webkit-keyframes scaleEnter-data-v-1ad2436f{0%{-webkit-transform:scaleX(.7) scaleY(.7);transform:scaleX(.7) scaleY(.7);opacity:.3}90%{-webkit-transform:scaleX(1.1) scaleY(1.1);transform:scaleX(1.1) scaleY(1.1)}95%{-webkit-transform:scaleX(.95) scaleY(.95);transform:scaleX(.95) scaleY(.95)}to{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1);opacity:1}}@keyframes scaleEnter-data-v-1ad2436f{0%{-webkit-transform:scaleX(.7) scaleY(.7);transform:scaleX(.7) scaleY(.7);opacity:.3}90%{-webkit-transform:scaleX(1.1) scaleY(1.1);transform:scaleX(1.1) scaleY(1.1)}95%{-webkit-transform:scaleX(.95) scaleY(.95);transform:scaleX(.95) scaleY(.95)}to{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1);opacity:1}}@-webkit-keyframes scaleLeave-data-v-1ad2436f{0%{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1)}60%{-webkit-transform:scaleX(1.18) scaleY(1.18);transform:scaleX(1.18) scaleY(1.18);opacity:.2}to{-webkit-transform:scaleX(1.15) scaleY(1.18);transform:scaleX(1.15) scaleY(1.18);opacity:0}}@keyframes scaleLeave-data-v-1ad2436f{0%{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1)}60%{-webkit-transform:scaleX(1.18) scaleY(1.18);transform:scaleX(1.18) scaleY(1.18);opacity:.2}to{-webkit-transform:scaleX(1.15) scaleY(1.18);transform:scaleX(1.15) scaleY(1.18);opacity:0}}@-webkit-keyframes slideRightScaleEnter-data-v-1ad2436f{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@keyframes slideRightScaleEnter-data-v-1ad2436f{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@-webkit-keyframes slideRightTranslateEnter-data-v-1ad2436f{0%{-webkit-transform:translateX(-6px);transform:translateX(-6px)}60%{-webkit-transform:translateX(2px);transform:translateX(2px)}}@keyframes slideRightTranslateEnter-data-v-1ad2436f{0%{-webkit-transform:translateX(-6px);transform:translateX(-6px)}60%{-webkit-transform:translateX(2px);transform:translateX(2px)}}@-webkit-keyframes slideLeftScaleEnter-data-v-1ad2436f{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@keyframes slideLeftScaleEnter-data-v-1ad2436f{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@-webkit-keyframes slideLeftTranslateEnter-data-v-1ad2436f{0%{-webkit-transform:translateX(6px);transform:translateX(6px)}60%{-webkit-transform:translateX(-2px);transform:translateX(-2px)}}@keyframes slideLeftTranslateEnter-data-v-1ad2436f{0%{-webkit-transform:translateX(6px);transform:translateX(6px)}60%{-webkit-transform:translateX(-2px);transform:translateX(-2px)}}.popover-container[data-v-1ad2436f]{position:relative;outline:none}.popover-container.expanded[data-v-1ad2436f]{display:block}.popover-origin[data-v-1ad2436f]{position:absolute;-webkit-transform-origin:top center;transform-origin:top center;z-index:10;pointer-events:none}.popover-origin.direction-top[data-v-1ad2436f]{bottom:100%}.popover-origin.direction-bottom[data-v-1ad2436f]{top:100%}.popover-origin.direction-left[data-v-1ad2436f]{top:0;right:100%}.popover-origin.direction-right[data-v-1ad2436f]{top:0;left:100%}.popover-origin.direction-bottom.align-left[data-v-1ad2436f],.popover-origin.direction-top.align-left[data-v-1ad2436f]{left:0}.popover-origin.direction-bottom.align-center[data-v-1ad2436f],.popover-origin.direction-top.align-center[data-v-1ad2436f]{left:50%;-webkit-transform:translateX(-50%);transform:translateX(-50%)}.popover-origin.direction-bottom.align-right[data-v-1ad2436f],.popover-origin.direction-top.align-right[data-v-1ad2436f]{right:0}.popover-origin.direction-left.align-top[data-v-1ad2436f],.popover-origin.direction-right.align-top[data-v-1ad2436f]{top:0}.popover-origin.direction-left.align-middle[data-v-1ad2436f],.popover-origin.direction-right.align-middle[data-v-1ad2436f]{top:50%;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.popover-origin.direction-left.align-bottom[data-v-1ad2436f],.popover-origin.direction-right.align-bottom[data-v-1ad2436f]{top:auto;bottom:0}.popover-origin .popover-content-wrapper[data-v-1ad2436f]{position:relative;outline:none}.popover-origin .popover-content-wrapper.interactive[data-v-1ad2436f]{pointer-events:all}.popover-origin .popover-content-wrapper .popover-content[data-v-1ad2436f]{position:relative;background-color:#fafafa;border:1px solid rgba(34,36,38,.15);border-radius:5px;box-shadow:0 1px 2px 0 rgba(34,36,38,.15);padding:4px}.popover-origin .popover-content-wrapper .popover-content[data-v-1ad2436f]:after{display:block;position:absolute;background:inherit;border:inherit;border-width:1px 1px 0 0;width:12px;height:12px;content:\"\"}.popover-origin .popover-content-wrapper .popover-content.direction-bottom[data-v-1ad2436f]:after{top:0;border-width:1px 1px 0 0}.popover-origin .popover-content-wrapper .popover-content.direction-top[data-v-1ad2436f]:after{top:100%;border-width:0 0 1px 1px}.popover-origin .popover-content-wrapper .popover-content.direction-left[data-v-1ad2436f]:after{left:100%;border-width:0 1px 1px 0}.popover-origin .popover-content-wrapper .popover-content.direction-right[data-v-1ad2436f]:after{left:0;border-width:1px 0 0 1px}.popover-origin .popover-content-wrapper .popover-content.align-left[data-v-1ad2436f]:after{left:20px;-webkit-transform:translateY(-50%) translateX(-50%) rotate(-45deg);transform:translateY(-50%) translateX(-50%) rotate(-45deg)}.popover-origin .popover-content-wrapper .popover-content.align-right[data-v-1ad2436f]:after{right:20px;-webkit-transform:translateY(-50%) translateX(50%) rotate(-45deg);transform:translateY(-50%) translateX(50%) rotate(-45deg)}.popover-origin .popover-content-wrapper .popover-content.align-center[data-v-1ad2436f]:after{left:50%;-webkit-transform:translateY(-50%) translateX(-50%) rotate(-45deg);transform:translateY(-50%) translateX(-50%) rotate(-45deg)}.popover-origin .popover-content-wrapper .popover-content.align-top[data-v-1ad2436f]:after{top:18px;-webkit-transform:translateY(-50%) translateX(-50%) rotate(-45deg);transform:translateY(-50%) translateX(-50%) rotate(-45deg)}.popover-origin .popover-content-wrapper .popover-content.align-middle[data-v-1ad2436f]:after{top:50%;-webkit-transform:translateY(-50%) translateX(-50%) rotate(-45deg);transform:translateY(-50%) translateX(-50%) rotate(-45deg)}.popover-origin .popover-content-wrapper .popover-content.align-bottom[data-v-1ad2436f]:after{bottom:18px;-webkit-transform:translateY(50%) translateX(-50%) rotate(-45deg);transform:translateY(50%) translateX(-50%) rotate(-45deg)}.fade-enter-active[data-v-1ad2436f],.fade-leave-active[data-v-1ad2436f],.slide-fade-enter-active[data-v-1ad2436f],.slide-fade-leave-active[data-v-1ad2436f]{transition:all .14s ease-in-out}.fade-enter[data-v-1ad2436f],.fade-leave-to[data-v-1ad2436f],.slide-fade-enter[data-v-1ad2436f],.slide-fade-leave-to[data-v-1ad2436f]{opacity:0}.slide-fade-enter.direction-bottom[data-v-1ad2436f],.slide-fade-leave-to.direction-bottom[data-v-1ad2436f]{-webkit-transform:translateY(-15px);transform:translateY(-15px)}.slide-fade-enter.direction-top[data-v-1ad2436f],.slide-fade-leave-to.direction-top[data-v-1ad2436f]{-webkit-transform:translateY(15px);transform:translateY(15px)}.slide-fade-enter.direction-left[data-v-1ad2436f],.slide-fade-leave-to.direction-left[data-v-1ad2436f]{-webkit-transform:translateX(15px);transform:translateX(15px)}.slide-fade-enter.direction-right[data-v-1ad2436f],.slide-fade-leave-to.direction-right[data-v-1ad2436f]{-webkit-transform:translateX(-15px);transform:translateX(-15px)}.c-week[data-v-28896542]{-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;display:-webkit-box;display:-ms-flexbox;display:flex}@-webkit-keyframes scaleEnter-data-v-3db80f80{0%{-webkit-transform:scaleX(.7) scaleY(.7);transform:scaleX(.7) scaleY(.7);opacity:.3}90%{-webkit-transform:scaleX(1.1) scaleY(1.1);transform:scaleX(1.1) scaleY(1.1)}95%{-webkit-transform:scaleX(.95) scaleY(.95);transform:scaleX(.95) scaleY(.95)}to{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1);opacity:1}}@-webkit-keyframes scaleLeave-data-v-3db80f80{0%{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1)}60%{-webkit-transform:scaleX(1.18) scaleY(1.18);transform:scaleX(1.18) scaleY(1.18);opacity:.2}to{-webkit-transform:scaleX(1.15) scaleY(1.18);transform:scaleX(1.15) scaleY(1.18);opacity:0}}@-webkit-keyframes slideRightScaleEnter-data-v-3db80f80{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@-webkit-keyframes slideRightTranslateEnter-data-v-3db80f80{0%{-webkit-transform:translateX(-6px);transform:translateX(-6px)}60%{-webkit-transform:translateX(2px);transform:translateX(2px)}}@-webkit-keyframes slideLeftScaleEnter-data-v-3db80f80{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@-webkit-keyframes slideLeftTranslateEnter-data-v-3db80f80{0%{-webkit-transform:translateX(6px);transform:translateX(6px)}60%{-webkit-transform:translateX(-2px);transform:translateX(-2px)}}@keyframes scaleEnter-data-v-3db80f80{0%{-webkit-transform:scaleX(.7) scaleY(.7);transform:scaleX(.7) scaleY(.7);opacity:.3}90%{-webkit-transform:scaleX(1.1) scaleY(1.1);transform:scaleX(1.1) scaleY(1.1)}95%{-webkit-transform:scaleX(.95) scaleY(.95);transform:scaleX(.95) scaleY(.95)}to{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1);opacity:1}}@keyframes scaleLeave-data-v-3db80f80{0%{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1)}60%{-webkit-transform:scaleX(1.18) scaleY(1.18);transform:scaleX(1.18) scaleY(1.18);opacity:.2}to{-webkit-transform:scaleX(1.15) scaleY(1.18);transform:scaleX(1.15) scaleY(1.18);opacity:0}}@keyframes slideRightScaleEnter-data-v-3db80f80{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@keyframes slideRightTranslateEnter-data-v-3db80f80{0%{-webkit-transform:translateX(-6px);transform:translateX(-6px)}60%{-webkit-transform:translateX(2px);transform:translateX(2px)}}@keyframes slideLeftScaleEnter-data-v-3db80f80{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@keyframes slideLeftTranslateEnter-data-v-3db80f80{0%{-webkit-transform:translateX(6px);transform:translateX(6px)}60%{-webkit-transform:translateX(-2px);transform:translateX(-2px)}}.c-day-popover[data-v-3db80f80]{-webkit-box-flex:1;-ms-flex:1;flex:1}.c-day[data-v-3db80f80]{position:relative;min-height:28px;z-index:1}.c-day-layer[data-v-3db80f80]{position:absolute;left:0;right:0;top:0;bottom:0;pointer-events:none}.c-day-box-center-center[data-v-3db80f80]{-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-transform-origin:50% 50%;transform-origin:50% 50%}.c-day-box-center-center[data-v-3db80f80],.c-day-box-left-center[data-v-3db80f80]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;margin:0;padding:0;height:100%}.c-day-box-left-center[data-v-3db80f80]{-webkit-box-pack:start;-ms-flex-pack:start;justify-content:flex-start;-webkit-transform-origin:0 50%;transform-origin:0 50%}.c-day-box-right-center[data-v-3db80f80]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end;-webkit-box-align:center;-ms-flex-align:center;align-items:center;margin:0;padding:0;height:100%;-webkit-transform-origin:100% 50%;transform-origin:100% 50%}.c-day-box-center-bottom[data-v-3db80f80]{-webkit-box-align:end;-ms-flex-align:end;align-items:flex-end;margin:0;padding:0}.c-day-box-center-bottom[data-v-3db80f80],.c-day-content-wrapper[data-v-3db80f80]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center}.c-day-content-wrapper[data-v-3db80f80]{-webkit-box-align:center;-ms-flex-align:center;align-items:center;pointer-events:all;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:default}.c-day-content[data-v-3db80f80]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;width:1.8rem;height:1.8rem;font-size:.9rem;font-weight:400;line-height:1;border-radius:50%;transition:all .18s ease-in-out;margin:.1rem .08rem}.c-day-backgrounds[data-v-3db80f80]{overflow:hidden;pointer-events:none;z-index:-1;-webkit-backface-visibility:hidden;backface-visibility:hidden}.c-day-background[data-v-3db80f80]{transition:height .13s ease-in-out,background-color .13s ease-in-out}.shift-left[data-v-3db80f80]{margin-left:-1px}.shift-right[data-v-3db80f80]{margin-right:-1px}.shift-left-right[data-v-3db80f80]{margin:0 -1px}.c-day-dots[data-v-3db80f80]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;margin:0;padding:0}.c-day-dot[data-v-3db80f80]{width:5px;height:5px;border-radius:50%;background-color:#66b3cc;transition:all .18s ease-in-out}.c-day-dot[data-v-3db80f80]:not(:last-child){margin-right:3px}.c-day-bars[data-v-3db80f80]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:start;-ms-flex-pack:start;justify-content:flex-start;-webkit-box-align:center;-ms-flex-align:center;align-items:center;margin:0;padding:0;width:75%}.c-day-bar[data-v-3db80f80]{-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;height:3px;background-color:#66b3cc;transition:all .18s ease-in-out}.c-day-popover-content[data-v-3db80f80]{font-size:.8rem;font-weight:400}.background-enter-active.c-day-fade-enter[data-v-3db80f80]{transition:opacity .2s ease-in-out}.background-enter-active.c-day-slide-right-scale-enter[data-v-3db80f80]{-webkit-animation:slideRightScaleEnter-data-v-3db80f80 .16s ease-in-out;animation:slideRightScaleEnter-data-v-3db80f80 .16s ease-in-out}.background-enter-active.c-day-slide-right-translate-enter[data-v-3db80f80]{-webkit-animation:slideRightTranslateEnter-data-v-3db80f80 .16s ease-in-out;animation:slideRightTranslateEnter-data-v-3db80f80 .16s ease-in-out}.background-enter-active.c-day-slide-left-scale-enter[data-v-3db80f80]{-webkit-animation:slideLeftScaleEnter-data-v-3db80f80 .16s ease-in-out;animation:slideLeftScaleEnter-data-v-3db80f80 .16s ease-in-out}.background-enter-active.c-day-slide-left-translate-enter[data-v-3db80f80]{-webkit-animation:slideLeftTranslateEnter-data-v-3db80f80 .16s ease-in-out;animation:slideLeftTranslateEnter-data-v-3db80f80 .16s ease-in-out}.background-enter-active.c-day-scale-enter[data-v-3db80f80]{-webkit-animation:scaleEnter-data-v-3db80f80 .16s ease-in-out;animation:scaleEnter-data-v-3db80f80 .16s ease-in-out}.background-leave-active.c-day-fade-leave[data-v-3db80f80]{transition:opacity .2s ease-in-out}.background-leave-active.c-day-scale-leave[data-v-3db80f80]{-webkit-animation:scaleLeave-data-v-3db80f80 .2s ease-in-out;animation:scaleLeave-data-v-3db80f80 .2s ease-in-out}.background-enter.c-day-fade-enter[data-v-3db80f80],.background-leave-to.c-day-fade-leave[data-v-3db80f80]{opacity:0}@-webkit-keyframes scaleEnter-data-v-54b1f93b{0%{-webkit-transform:scaleX(.7) scaleY(.7);transform:scaleX(.7) scaleY(.7);opacity:.3}90%{-webkit-transform:scaleX(1.1) scaleY(1.1);transform:scaleX(1.1) scaleY(1.1)}95%{-webkit-transform:scaleX(.95) scaleY(.95);transform:scaleX(.95) scaleY(.95)}to{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1);opacity:1}}@keyframes scaleEnter-data-v-54b1f93b{0%{-webkit-transform:scaleX(.7) scaleY(.7);transform:scaleX(.7) scaleY(.7);opacity:.3}90%{-webkit-transform:scaleX(1.1) scaleY(1.1);transform:scaleX(1.1) scaleY(1.1)}95%{-webkit-transform:scaleX(.95) scaleY(.95);transform:scaleX(.95) scaleY(.95)}to{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1);opacity:1}}@-webkit-keyframes scaleLeave-data-v-54b1f93b{0%{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1)}60%{-webkit-transform:scaleX(1.18) scaleY(1.18);transform:scaleX(1.18) scaleY(1.18);opacity:.2}to{-webkit-transform:scaleX(1.15) scaleY(1.18);transform:scaleX(1.15) scaleY(1.18);opacity:0}}@keyframes scaleLeave-data-v-54b1f93b{0%{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1)}60%{-webkit-transform:scaleX(1.18) scaleY(1.18);transform:scaleX(1.18) scaleY(1.18);opacity:.2}to{-webkit-transform:scaleX(1.15) scaleY(1.18);transform:scaleX(1.15) scaleY(1.18);opacity:0}}@-webkit-keyframes slideRightScaleEnter-data-v-54b1f93b{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@keyframes slideRightScaleEnter-data-v-54b1f93b{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@-webkit-keyframes slideRightTranslateEnter-data-v-54b1f93b{0%{-webkit-transform:translateX(-6px);transform:translateX(-6px)}60%{-webkit-transform:translateX(2px);transform:translateX(2px)}}@keyframes slideRightTranslateEnter-data-v-54b1f93b{0%{-webkit-transform:translateX(-6px);transform:translateX(-6px)}60%{-webkit-transform:translateX(2px);transform:translateX(2px)}}@-webkit-keyframes slideLeftScaleEnter-data-v-54b1f93b{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@keyframes slideLeftScaleEnter-data-v-54b1f93b{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@-webkit-keyframes slideLeftTranslateEnter-data-v-54b1f93b{0%{-webkit-transform:translateX(6px);transform:translateX(6px)}60%{-webkit-transform:translateX(-2px);transform:translateX(-2px)}}@keyframes slideLeftTranslateEnter-data-v-54b1f93b{0%{-webkit-transform:translateX(6px);transform:translateX(6px)}60%{-webkit-transform:translateX(-2px);transform:translateX(-2px)}}.c-day-popover-row[data-v-54b1f93b]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;padding:2px 5px;transition:all .18s ease-in-out}.c-day-popover-row.selectable[data-v-54b1f93b]{cursor:pointer}.c-day-popover-row.selectable[data-v-54b1f93b]:hover{background-color:rgba(0,0,0,.1)}.c-day-popover-row[data-v-54b1f93b]:not(:first-child){margin-top:3px}.c-day-popover-row .c-day-popover-indicator[data-v-54b1f93b]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-flex:0;-ms-flex-positive:0;flex-grow:0;width:15px;margin-right:3px}.c-day-popover-row .c-day-popover-indicator span[data-v-54b1f93b]{transition:all .18s ease-in-out}.c-day-popover-row .c-day-popover-content[data-v-54b1f93b]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-ms-flex-wrap:none;flex-wrap:none;-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;transition:all .18s ease-in-out}@-webkit-keyframes scaleEnter-data-v-81948efe{0%{-webkit-transform:scaleX(.7) scaleY(.7);transform:scaleX(.7) scaleY(.7);opacity:.3}90%{-webkit-transform:scaleX(1.1) scaleY(1.1);transform:scaleX(1.1) scaleY(1.1)}95%{-webkit-transform:scaleX(.95) scaleY(.95);transform:scaleX(.95) scaleY(.95)}to{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1);opacity:1}}@-webkit-keyframes scaleLeave-data-v-81948efe{0%{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1)}60%{-webkit-transform:scaleX(1.18) scaleY(1.18);transform:scaleX(1.18) scaleY(1.18);opacity:.2}to{-webkit-transform:scaleX(1.15) scaleY(1.18);transform:scaleX(1.15) scaleY(1.18);opacity:0}}@-webkit-keyframes slideRightScaleEnter-data-v-81948efe{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@-webkit-keyframes slideRightTranslateEnter-data-v-81948efe{0%{-webkit-transform:translateX(-6px);transform:translateX(-6px)}60%{-webkit-transform:translateX(2px);transform:translateX(2px)}}@-webkit-keyframes slideLeftScaleEnter-data-v-81948efe{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@-webkit-keyframes slideLeftTranslateEnter-data-v-81948efe{0%{-webkit-transform:translateX(6px);transform:translateX(6px)}60%{-webkit-transform:translateX(-2px);transform:translateX(-2px)}}@keyframes scaleEnter-data-v-81948efe{0%{-webkit-transform:scaleX(.7) scaleY(.7);transform:scaleX(.7) scaleY(.7);opacity:.3}90%{-webkit-transform:scaleX(1.1) scaleY(1.1);transform:scaleX(1.1) scaleY(1.1)}95%{-webkit-transform:scaleX(.95) scaleY(.95);transform:scaleX(.95) scaleY(.95)}to{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1);opacity:1}}@keyframes scaleLeave-data-v-81948efe{0%{-webkit-transform:scaleX(1) scaleY(1);transform:scaleX(1) scaleY(1)}60%{-webkit-transform:scaleX(1.18) scaleY(1.18);transform:scaleX(1.18) scaleY(1.18);opacity:.2}to{-webkit-transform:scaleX(1.15) scaleY(1.18);transform:scaleX(1.15) scaleY(1.18);opacity:0}}@keyframes slideRightScaleEnter-data-v-81948efe{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@keyframes slideRightTranslateEnter-data-v-81948efe{0%{-webkit-transform:translateX(-6px);transform:translateX(-6px)}60%{-webkit-transform:translateX(2px);transform:translateX(2px)}}@keyframes slideLeftScaleEnter-data-v-81948efe{0%{-webkit-transform:scaleX(0);transform:scaleX(0)}60%{-webkit-transform:scaleX(1.08);transform:scaleX(1.08)}}@keyframes slideLeftTranslateEnter-data-v-81948efe{0%{-webkit-transform:translateX(6px);transform:translateX(6px)}60%{-webkit-transform:translateX(-2px);transform:translateX(-2px)}}.c-nav[data-v-81948efe]{transition:height 5s ease-in-out;color:#333}.c-header[data-v-81948efe]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-ms-flex-align:center;align-items:center;border-bottom:1px solid #dadada;padding:3px 0}.c-arrow-layout[data-v-81948efe]{min-width:26px}.c-arrow-layout[data-v-81948efe],.c-arrow[data-v-81948efe]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;margin:0;padding:0}.c-arrow[data-v-81948efe]{font-size:1.6rem;transition:fill-opacity .3s ease-in-out;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.c-arrow[data-v-81948efe]:hover{fill-opacity:.5}.c-title[data-v-81948efe]{font-weight:500;transition:all .25s ease-in-out}.c-table-cell[data-v-81948efe],.c-title[data-v-81948efe]{font-size:.9rem;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.c-table-cell[data-v-81948efe]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;height:100%;position:relative;font-weight:400;background-color:#fff;transition:all .1s ease-in-out}.c-table-cell[data-v-81948efe]:hover{background-color:#f0f0f0}.c-disabled[data-v-81948efe]{opacity:.2;cursor:not-allowed;pointer-events:none}.c-disabled[data-v-81948efe]:hover{background-color:transparent}.c-active[data-v-81948efe]{background-color:#f0f0f0;font-weight:600}.c-indicators[data-v-81948efe]{position:absolute;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;bottom:5px;width:100%;transition:all .1s ease-in-out}.c-indicators .c-indicator[data-v-81948efe]{width:5px;height:5px;border-radius:50%}.c-indicators .c-indicator[data-v-81948efe]:not(:first-child){margin-left:3px}.c-table[data-v-81948efe]{table-layout:fixed;width:100%;border-collapse:collapse}.c-table tr td[data-v-81948efe]{border:1px solid #dadada;width:60px;height:34px}.c-table tr td[data-v-81948efe]:first-child{border-left:0}.c-table tr td[data-v-81948efe]:last-child{border-right:0}.c-table tr:first-child td[data-v-81948efe]{border-top:0}.c-table tr:last-child td[data-v-81948efe]{border-bottom:0}.indicators-enter-active[data-v-81948efe],.indicators-leave-active[data-v-81948efe]{transition:all .1s ease-in-out}.indicators-enter[data-v-81948efe],.indicators-leave-to[data-v-81948efe]{opacity:0}.svg-icon[data-v-12e91ab4]{display:inline-block;stroke:currentColor;stroke-width:0}.svg-icon path[data-v-12e91ab4]{fill:currentColor}.date-label[data-v-6c331e62]{text-align:center}.days-nights[data-v-6c331e62]{-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;margin-top:3px}.days-nights .days[data-v-6c331e62],.days-nights .nights[data-v-6c331e62],.days-nights[data-v-6c331e62]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.days-nights .days[data-v-6c331e62],.days-nights .nights[data-v-6c331e62]{font-weight:700}.days-nights .days[data-v-6c331e62]:not(:first-child),.days-nights .nights[data-v-6c331e62]:not(:first-child){margin-left:13px}.days-nights .vc-moon-o[data-v-6c331e62],.days-nights .vc-sun-o[data-v-6c331e62]{margin-right:5px;width:16px;height:16px}.days-nights .vc-sun-o[data-v-6c331e62]{color:#ffb366}.days-nights .vc-moon-o[data-v-6c331e62]{color:#4d4d64}", ""]);

// exports


/***/ }),
/* 8 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target) {
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(10);

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertInto + " " + options.insertAt.before);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = options.transform(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),
/* 10 */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_ComDeparture_vue__ = __webpack_require__(1);
/* empty harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_eb335cce_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_ComDeparture_vue__ = __webpack_require__(12);
var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_ComDeparture_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_eb335cce_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_ComDeparture_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\ComDeparture.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-eb335cce", Component.options)
  } else {
    hotAPI.reload("data-v-eb335cce", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _c("div", { staticClass: "col-lg-3 col-sm-6" }, [
      _c("div", { staticClass: "h5" }, [
        _vm._v("Поиск туров из\n\t\t\t"),
        _c("div", { staticClass: "dropdown d-inline-block" }, [
          _c(
            "a",
            {
              staticClass: "text-uppercase display-7 departure_wrap",
              attrs: {
                href: "javascript:void(0)",
                id: "departureValue",
                "data-toggle": "dropdown",
                "aria-haspopup": "true",
                "aria-expanded": "false"
              }
            },
            [_vm._v(_vm._s(_vm.cityFromName))]
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "dropdown-menu select-dropdown bg-light",
              attrs: { "aria-labelledby": "departureValue" }
            },
            [
              _c(
                "div",
                { staticClass: "scrollable-menu" },
                [
                  _c(
                    "span",
                    {
                      staticClass: "dropdown-item",
                      class: { "bg-primary text-white": _vm.departure == 99 },
                      on: {
                        click: function($event) {
                          _vm.setDeparture(_vm.non_flight)
                        }
                      }
                    },
                    [_vm._v("Без перелета")]
                  ),
                  _vm._v(" "),
                  _vm._l(_vm.groupedDepartures, function(item, letter) {
                    return _c(
                      "span",
                      [
                        _c("h6", { staticClass: "dropdown-header" }, [
                          _vm._v(_vm._s(letter))
                        ]),
                        _vm._v(" "),
                        _vm._l(item, function(departureItem) {
                          return departureItem.id != 99
                            ? _c(
                                "span",
                                {
                                  staticClass: "dropdown-item",
                                  class: {
                                    "bg-primary text-white":
                                      departureItem.id == _vm.departure
                                  },
                                  on: {
                                    click: function($event) {
                                      _vm.setDeparture(departureItem)
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    _vm._s(departureItem.name) +
                                      "\n\t\t\t\t\t\t\t"
                                  )
                                ]
                              )
                            : _vm._e()
                        })
                      ],
                      2
                    )
                  })
                ],
                2
              )
            ]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col-lg-6 col-sm-6 pt-2" }, [
      _c("div", { staticClass: "form-check" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.regular,
              expression: "regular"
            }
          ],
          staticClass: "form-check-input",
          attrs: { type: "checkbox", value: "", id: "hideRegular" },
          domProps: {
            checked: Array.isArray(_vm.regular)
              ? _vm._i(_vm.regular, "") > -1
              : _vm.regular
          },
          on: {
            change: [
              function($event) {
                var $$a = _vm.regular,
                  $$el = $event.target,
                  $$c = $$el.checked ? true : false
                if (Array.isArray($$a)) {
                  var $$v = "",
                    $$i = _vm._i($$a, $$v)
                  if ($$el.checked) {
                    $$i < 0 && (_vm.regular = $$a.concat([$$v]))
                  } else {
                    $$i > -1 &&
                      (_vm.regular = $$a
                        .slice(0, $$i)
                        .concat($$a.slice($$i + 1)))
                  }
                } else {
                  _vm.regular = $$c
                }
              },
              function($event) {
                _vm.setRegular()
              }
            ]
          }
        }),
        _vm._v(" "),
        _c(
          "label",
          {
            staticClass: "form-check-label pl-1",
            attrs: { for: "hideRegular" }
          },
          [_vm._v("\n\t\t\t\tСкрыть регулярные рейсы\n\t\t\t")]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-eb335cce", esExports)
  }
}

/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_ComResults_vue__ = __webpack_require__(2);
/* empty harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_5543d2db_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_ComResults_vue__ = __webpack_require__(14);
var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_ComResults_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_5543d2db_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_ComResults_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\ComResults.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5543d2db", Component.options)
  } else {
    hotAPI.reload("data-v-5543d2db", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("ul", { staticClass: "list-unstyled" }, [
    _vm.resultsData.length > 0
      ? _c(
          "div",
          _vm._l(_vm.grouppedResults, function(hotel, index) {
            return _c(
              "li",
              { staticClass: "animated fadeInRight mb-2 pt-2 pb-2" },
              [
                hotel.type === "result"
                  ? _c("div", { staticClass: "card bg-light" }, [
                      _c(
                        "div",
                        {
                          staticClass: "card-body bg-white row no-gutters",
                          staticStyle: {
                            "box-shadow": "0px 1px 3px 1px #bfbfbf",
                            "z-index": "1"
                          }
                        },
                        [
                          _vm._m(0, true),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "col-auto col-md-6 col-xl-auto text-center mb-3 mb-md-0 align-self-center pr-3"
                            },
                            [
                              _c("img", {
                                attrs: { src: hotel.picturelink, alt: "" }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "col-12 col-md-6 col-xl-3  align-self-center pr-3"
                            },
                            [
                              _c("div", { staticClass: "col pl-0" }, [
                                _c("h4", { staticClass: "display-8 mb-0" }, [
                                  _c(
                                    "a",
                                    {
                                      attrs: {
                                        target: "_blank",
                                        href:
                                          "/hotel/" +
                                          hotel.hotelcode +
                                          "/?" +
                                          _vm.searchUrl
                                      }
                                    },
                                    [_vm._v(_vm._s(hotel.hotelname))]
                                  )
                                ])
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col pl-0" },
                                _vm._l(parseInt(hotel.hotelstars), function(n) {
                                  return _c("i", {
                                    staticClass: "fa fa-star text-warning"
                                  })
                                })
                              ),
                              _vm._v(" "),
                              parseInt(hotel.hotelrating) > 0
                                ? _c("div", { staticClass: "col pl-0" }, [
                                    _c(
                                      "span",
                                      {
                                        class: {
                                          "text-success":
                                            hotel.hotelrating >= 4,
                                          "text-warning":
                                            hotel.hotelrating >= 3 &&
                                            hotel.hotelrating < 4,
                                          "text-danger": hotel.hotelrating < 3
                                        }
                                      },
                                      [
                                        _c("i", {
                                          staticClass: "fa",
                                          class: {
                                            "fa-thumbs-up":
                                              hotel.hotelrating >= 4,
                                            "fa-thumbs-o-up":
                                              hotel.hotelrating >= 3 &&
                                              hotel.hotelrating < 4,
                                            "fa-thumbs-down":
                                              hotel.hotelrating < 3
                                          }
                                        }),
                                        _vm._v(
                                          "\n\t\t\t\t\t\t\t\t" +
                                            _vm._s(hotel.hotelrating)
                                        )
                                      ]
                                    ),
                                    _vm._v("/5\n\t\t\t\t\t\t")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _c("div", { staticClass: "col pl-0" }, [
                                _c("small", { staticClass: "text-muted" }, [
                                  _vm._v(
                                    _vm._s(hotel.regionname) +
                                      ", " +
                                      _vm._s(hotel.countryname)
                                  )
                                ])
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "col-12 col-md-6 col-xl-3 align-self-start align-self-md-end align-self-xl-center pr-3"
                            },
                            [
                              _c("dl", [
                                _c("dt", [
                                  _vm._v(
                                    "Вылеты " + _vm._s(hotel.minFlydate) + " "
                                  ),
                                  hotel.minFlydate !== hotel.maxFlydate
                                    ? _c("span", [
                                        _vm._v(" — " + _vm._s(hotel.maxFlydate))
                                      ])
                                    : _vm._e()
                                ]),
                                _vm._v(" "),
                                _c("dd", [
                                  hotel.minNight == hotel.maxNight
                                    ? _c("span", [
                                        _vm._v(
                                          "на " +
                                            _vm._s(
                                              _vm._f("displayNights")(
                                                hotel.minNight
                                              )
                                            )
                                        )
                                      ])
                                    : _vm._e(),
                                  _vm._v(" "),
                                  hotel.minNight != hotel.maxNight
                                    ? _c("span", [
                                        _vm._v("на " + _vm._s(hotel.minNight))
                                      ])
                                    : _vm._e(),
                                  _vm._v(" "),
                                  hotel.minNight !== hotel.maxNight
                                    ? _c("span", [
                                        _vm._v(
                                          " —  " +
                                            _vm._s(
                                              _vm._f("displayNights")(
                                                hotel.maxNight
                                              )
                                            )
                                        )
                                      ])
                                    : _vm._e(),
                                  _vm._v(" "),
                                  _c("br"),
                                  _vm._v(" "),
                                  _c("span", [
                                    _vm._v(
                                      _vm._s(hotel.tours.tour[0].adults) +
                                        " взр. "
                                    ),
                                    parseInt(hotel.tours.tour[0].child) > 0
                                      ? _c("span", [
                                          _vm._v(
                                            "+ " +
                                              _vm._s(
                                                hotel.tours.tour[0].child
                                              ) +
                                              " реб."
                                          )
                                        ])
                                      : _vm._e()
                                  ])
                                ])
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "d-none d-xl-block col-xl-2 align-self-start align-self-md-end align-self-xl-center pr-3 hotel-mini-description",
                              attrs: {
                                "data-placement": "bottom",
                                "data-toggle": "popover",
                                title: "Описание",
                                "data-content": hotel.hoteldescription
                              }
                            },
                            [
                              _vm._v(
                                "\n\t\t\t\t\t\t" +
                                  _vm._s(
                                    _vm._f("shortText")(hotel.hoteldescription)
                                  ) +
                                  "\n\t\t\t\t\t"
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "col-12 col-md-6 col-xl-auto text-right align-self-start ml-md-auto"
                            },
                            [
                              _vm._m(1, true),
                              _vm._v(" "),
                              _c("div", { staticClass: "text-muted h5 mb-0" }, [
                                _c("small", [_vm._v("без скидки")]),
                                _vm._v(" "),
                                _c("s", [
                                  _vm._v(
                                    _vm._s(_vm._f("getOldPrice")(hotel.price))
                                  )
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "text-danger" }, [
                                _c("span", { staticClass: "h5" }, [
                                  _vm._v(
                                    _vm._s(
                                      _vm._f("getCreditPrice")(hotel.price)
                                    )
                                  )
                                ]),
                                _vm._v(" "),
                                _c("small", [_vm._v("в кредит 6 мес.")])
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass: "text-primary h5",
                                  on: {
                                    click: function($event) {
                                      _vm.openToursList(hotel.hotelcode)
                                    }
                                  }
                                },
                                [
                                  _c(
                                    "a",
                                    { attrs: { href: "javascript:void(0)" } },
                                    [
                                      _c("span", [
                                        _vm._v(
                                          _vm._s(
                                            _vm._f("formatPrice")(hotel.price)
                                          )
                                        )
                                      ]),
                                      _vm._v(" за тур ")
                                    ]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "mt-2" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass:
                                      "ld-ext-left btn btn-info btn-block",
                                    attrs: {
                                      href: "javascript:void(0);",
                                      id: "hotelBtn-" + hotel.hotelcode
                                    },
                                    on: {
                                      click: function($event) {
                                        _vm.openHotelDescr(
                                          hotel.hotelcode,
                                          this
                                        )
                                      }
                                    }
                                  },
                                  [
                                    _c("span", [_vm._v("Об отеле")]),
                                    _vm._v(" "),
                                    _c("div", {
                                      staticClass: "ld ld-ring ld-spin"
                                    })
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "mt-2" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "btn btn-orange btn-block",
                                    attrs: { href: "javascript:void(0);" },
                                    on: {
                                      click: function($event) {
                                        _vm.openToursList(hotel.hotelcode)
                                      }
                                    }
                                  },
                                  [
                                    _vm._v("Все варианты "),
                                    _c("i", {
                                      staticClass: "fa fa-chevron-down"
                                    })
                                  ]
                                )
                              ])
                            ]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _vm.obHotelsDescr[hotel.hotelcode] !== undefined
                        ? _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value:
                                    _vm.obHotelsDescr[hotel.hotelcode] !==
                                    undefined,
                                  expression:
                                    "obHotelsDescr[hotel.hotelcode] !== undefined"
                                }
                              ],
                              staticClass: "p-3 hotel-description",
                              staticStyle: { display: "none" },
                              attrs: { id: "descr-" + hotel.hotelcode }
                            },
                            [
                              parseInt(
                                _vm.obHotelsDescr[hotel.hotelcode].imagescount
                              ) > 0
                                ? _c(
                                    "div",
                                    {
                                      staticClass:
                                        "row hotel-photos d-flex justify-content-between",
                                      attrs: {
                                        id: "hotelPhotos_" + hotel.hotelcode
                                      }
                                    },
                                    _vm._l(
                                      _vm.obHotelsDescr[hotel.hotelcode].images
                                        .image,
                                      function(image) {
                                        return _c("div", {}, [
                                          _c(
                                            "a",
                                            {
                                              attrs: {
                                                href: image,
                                                "data-fancybox":
                                                  "gallery1" + hotel.hotelcode
                                              }
                                            },
                                            [
                                              _c("img", {
                                                staticClass: "img-thumbnail",
                                                attrs: { src: image, alt: "" }
                                              })
                                            ]
                                          )
                                        ])
                                      }
                                    )
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              _c("div", { staticClass: "row" }, [
                                _c("div", { staticClass: "col-12" }, [
                                  _c(
                                    "p",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value:
                                            _vm.obHotelsDescr[hotel.hotelcode]
                                              .description,
                                          expression:
                                            "obHotelsDescr[hotel.hotelcode].description"
                                        }
                                      ]
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.obHotelsDescr[hotel.hotelcode]
                                            .description
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "p",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: !_vm.obHotelsDescr[
                                            hotel.hotelcode
                                          ].description,
                                          expression:
                                            "!obHotelsDescr[hotel.hotelcode].description"
                                        }
                                      ]
                                    },
                                    [
                                      _vm._v(
                                        "Извините, описание отеля недоступно"
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("ul", { staticClass: "list-unstyled" }, [
                                    _c(
                                      "li",
                                      {
                                        directives: [
                                          {
                                            name: "show",
                                            rawName: "v-show",
                                            value:
                                              _vm.obHotelsDescr[hotel.hotelcode]
                                                .inroom,
                                            expression:
                                              "obHotelsDescr[hotel.hotelcode].inroom"
                                          }
                                        ]
                                      },
                                      [
                                        _c("strong", [_vm._v("В комнате:")]),
                                        _vm._v(
                                          " " +
                                            _vm._s(
                                              _vm.obHotelsDescr[hotel.hotelcode]
                                                .inroom
                                            )
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "li",
                                      {
                                        directives: [
                                          {
                                            name: "show",
                                            rawName: "v-show",
                                            value:
                                              _vm.obHotelsDescr[hotel.hotelcode]
                                                .territory,
                                            expression:
                                              "obHotelsDescr[hotel.hotelcode].territory"
                                          }
                                        ]
                                      },
                                      [
                                        _c("strong", [_vm._v("Территория:")]),
                                        _vm._v(
                                          " " +
                                            _vm._s(
                                              _vm.obHotelsDescr[hotel.hotelcode]
                                                .territory
                                            )
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "li",
                                      {
                                        directives: [
                                          {
                                            name: "show",
                                            rawName: "v-show",
                                            value:
                                              _vm.obHotelsDescr[hotel.hotelcode]
                                                .beach,
                                            expression:
                                              "obHotelsDescr[hotel.hotelcode].beach"
                                          }
                                        ]
                                      },
                                      [
                                        _c("strong", [_vm._v("Пляж:")]),
                                        _vm._v(
                                          " " +
                                            _vm._s(
                                              _vm.obHotelsDescr[hotel.hotelcode]
                                                .beach
                                            )
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "li",
                                      {
                                        directives: [
                                          {
                                            name: "show",
                                            rawName: "v-show",
                                            value:
                                              _vm.obHotelsDescr[hotel.hotelcode]
                                                .placement,
                                            expression:
                                              "obHotelsDescr[hotel.hotelcode].placement"
                                          }
                                        ]
                                      },
                                      [
                                        _c("strong", [_vm._v("Расположение:")]),
                                        _vm._v(
                                          " " +
                                            _vm._s(
                                              _vm.obHotelsDescr[hotel.hotelcode]
                                                .placement
                                            )
                                        )
                                      ]
                                    )
                                  ])
                                ])
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "row d-flex row bg-info pt-2 pb-2 text-white align-items-center mt-3"
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "col-12 col-lg-9 display-8 mb-2 mb-lg-0"
                                    },
                                    [
                                      _vm._v(
                                        "Есть вопросы по отелю? Задай их нашему эксперту"
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "col-12 col-lg-3" },
                                    [
                                      _c(
                                        "a",
                                        {
                                          staticClass:
                                            "btn btn-primary btn-block",
                                          attrs: {
                                            href: "javascript:void(0)",
                                            "data-hotel-id": hotel.hotelcode,
                                            "data-toggle": "modal",
                                            "data-target":
                                              "#expertQuestionModal"
                                          }
                                        },
                                        [_vm._v("Задать вопрос")]
                                      )
                                    ]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("br"),
                              _vm._v(" "),
                              _c("div", {
                                staticStyle: { height: "200px" },
                                attrs: { id: "mapintab_" + hotel.hotelcode }
                              })
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "p-3 tours-list d-none bg-darker",
                          attrs: { id: "tours-" + hotel.hotelcode }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "d-block" },
                            [
                              _vm._l(hotel.tours.tour, function(tour) {
                                return _c(
                                  "div",
                                  {
                                    staticClass:
                                      "row d-flex align-items-center bg-darker"
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "col-12 text-muted mb-3" },
                                      [_vm._v(_vm._s(tour.tourname))]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "col-6 col-md-2" },
                                      [
                                        _c("p", [
                                          _c("strong", [
                                            _vm._v(_vm._s(tour.flydate))
                                          ])
                                        ]),
                                        _vm._v(" "),
                                        _c("p", [
                                          _vm._v(_vm._s(tour.nights) + " ночей")
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "col-6 col-md-3" },
                                      [
                                        _c("p", [
                                          _c("strong", [
                                            _vm._v(_vm._s(tour.meal))
                                          ])
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "p",
                                          {
                                            attrs: { title: hotel.mealrussian }
                                          },
                                          [_vm._v(_vm._s(tour.mealrussian))]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "col-6 col-md-2" },
                                      [
                                        _c("p", [
                                          _c("strong", [
                                            _vm._v(_vm._s(tour.room))
                                          ])
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "p",
                                          { attrs: { title: hotel.placement } },
                                          [_vm._v(_vm._s(tour.placement))]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "col-6 col-md-2" },
                                      [
                                        _c("p", [
                                          _c("strong", [
                                            _vm._v(_vm._s(tour.operatorname))
                                          ])
                                        ]),
                                        _vm._v(" "),
                                        _c("p", [
                                          _c("img", {
                                            attrs: {
                                              src:
                                                "https://tourvisor.ru/pics/operators/searchlogo/" +
                                                tour.operatorcode +
                                                ".gif",
                                              alt: ""
                                            }
                                          })
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "col-12 col-md-3" },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "text-success text-left text-md-right mb-0"
                                          },
                                          [
                                            _vm._v(
                                              "\n\t\t\t\t\t\t\t\t\tза тур\n\t\t\t\t\t\t\t\t\t"
                                            ),
                                            _c(
                                              "span",
                                              {
                                                staticClass: "price h3",
                                                on: {
                                                  click: function($event) {
                                                    $event.preventDefault()
                                                    _vm.getDetail(tour.tourid)
                                                  }
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    _vm._f("formatPrice")(
                                                      tour.price
                                                    )
                                                  )
                                                )
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "text-muted text-left text-md-right"
                                          },
                                          [
                                            _vm._v(
                                              "\n\t\t\t\t\t\t\t\t\tбез скидки: "
                                            ),
                                            _c("s", { staticClass: "h5" }, [
                                              _vm._v(
                                                _vm._s(
                                                  _vm._f("getOldPrice")(
                                                    tour.price
                                                  )
                                                )
                                              )
                                            ])
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "text-danger text-left text-md-right price",
                                            attrs: {
                                              "data-toggle": "modal",
                                              "data-fast-tour-id": tour.tourid,
                                              "data-info": "credit",
                                              "data-target": "#fastOrderModal"
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "\n\t\t\t\t\t\t\t\t\tв кредит 6 мес.: "
                                            ),
                                            _c("span", { staticClass: "h5" }, [
                                              _vm._v(
                                                _vm._s(
                                                  _vm._f("getCreditPrice")(
                                                    tour.price
                                                  )
                                                )
                                              )
                                            ]),
                                            _vm._v(" "),
                                            _vm._m(2, true)
                                          ]
                                        ),
                                        _vm._v(" "),
                                        !_vm.hideRegular
                                          ? _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "text-info small text-left text-md-right mb-2"
                                              },
                                              [
                                                _c("i", [
                                                  _vm._v(
                                                    "цена может быть не окончательной, необходимо обратиться в офис"
                                                  )
                                                ])
                                              ]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _c(
                                          "a",
                                          {
                                            staticClass:
                                              "btn btn-primary btn-block btn-sm ld-ext-left",
                                            class: "detail-btn_" + tour.tourid,
                                            attrs: { href: "#" },
                                            on: {
                                              click: function($event) {
                                                $event.preventDefault()
                                                _vm.getDetail(tour.tourid)
                                              }
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "\n\t\t\t\t\t\t\t\t\tТур подробно\n\t\t\t\t\t\t\t\t\t"
                                            ),
                                            _c("div", {
                                              staticClass: "ld ld-ring ld-spin"
                                            })
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "a",
                                          {
                                            staticClass:
                                              "btn btn-outline-orange btn-sm btn-block",
                                            attrs: {
                                              href: "javascript:void(0)",
                                              "data-toggle": "modal",
                                              "data-fast-tour-id": tour.tourid,
                                              "data-target": "#fastOrderModal"
                                            }
                                          },
                                          [_vm._v("Заказать в 1 клик")]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _vm._m(3, true)
                                  ]
                                )
                              }),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass: "text-center text-info",
                                  on: {
                                    click: function($event) {
                                      _vm.openToursList(hotel.hotelcode)
                                    }
                                  }
                                },
                                [_vm._m(4, true)]
                              )
                            ],
                            2
                          )
                        ]
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                hotel.type === "banner"
                  ? _c("div", { staticClass: "card bg-dark" }, [
                      _vm._v(_vm._s(hotel.banner.name))
                    ])
                  : _vm._e()
              ]
            )
          })
        )
      : _c("div", [
          !_vm.toursNotFound
            ? _c(
                "div",
                {
                  staticClass:
                    "col-12 display-6 mt-5 mb-5 text-center text-success"
                },
                [
                  _vm._v(
                    '\n\t\t\tНажмите кнопку "Искать", для начала работы поиска\n\t\t'
                  )
                ]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.toursNotFound
            ? _c(
                "div",
                {
                  staticClass:
                    "col-12 display-7 mt-5 mb-5 text-center text-danger"
                },
                [
                  _vm._v(
                    '\n\t\t\tУпс! Извините, но по заданным параметрам туров не найдено. Попробуйте изменить парметры поиска и снова нажать кнопку "Найти" '
                  ),
                  _c("br"),
                  _vm._v(
                    "\n\t\t\tЛибо отставьте заявку на подбор тура для наших менеджеров.\n\t\t"
                  )
                ]
              )
            : _vm._e()
        ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "col-12 text-success small text-md-right font-italic d-none d-xl-block"
      },
      [
        _c("i", { staticClass: "fa fa-info" }),
        _vm._v(" все цены с топливным сбором")
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "text-success small font-italic d-block d-xl-none" },
      [
        _c("i", { staticClass: "fa fa-info" }),
        _vm._v(" все цены с топливным сбором")
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("small", [_vm._v("(стоимость без первоначального взноса)")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12" }, [_c("hr")])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { attrs: { href: "javascript:void(0)" } }, [
      _c("i", { staticClass: "fa fa-chevron-up" }),
      _vm._v(" свернуть")
    ])
  }
]
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5543d2db", esExports)
  }
}

/***/ }),
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_ComProgressBar_vue__ = __webpack_require__(3);
/* empty harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_faea71ea_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_ComProgressBar_vue__ = __webpack_require__(16);
var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_ComProgressBar_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_faea71ea_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_ComProgressBar_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\ComProgressBar.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-faea71ea", Component.options)
  } else {
    hotAPI.reload("data-v-faea71ea", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.searchInAction
    ? _c("div", { staticClass: "progress mt-3 mb-3" }, [
        _c(
          "div",
          {
            staticClass:
              "progress-bar progress-bar-striped progress-bar-animated bg-success d-flex justify-content-between",
            style: { width: _vm.searchProgress + "%" },
            attrs: {
              role: "progressbar",
              "aria-valuenow": _vm.searchProgress,
              "aria-valuemin": "0",
              "aria-valuemax": "100"
            }
          },
          [
            _c("span", { staticClass: "progress-type pl-2" }, [
              _vm._v("Поиск... Найдено туров: " + _vm._s(_vm.allRowsCount))
            ]),
            _vm._v(" "),
            _c("span", { staticClass: "progress-completed pr-2" }, [
              _vm._v(_vm._s(_vm.searchProgress) + "%")
            ])
          ]
        )
      ])
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-faea71ea", esExports)
  }
}

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

!function(t,e){ true?module.exports=e():"function"==typeof define&&define.amd?define([],e):"object"==typeof exports?exports.HotelDatePicker=e():t.HotelDatePicker=e()}("undefined"!=typeof self?self:this,function(){return function(t){function e(i){if(n[i])return n[i].exports;var o=n[i]={i:i,l:!1,exports:{}};return t[i].call(o.exports,o,o.exports,e),o.l=!0,o.exports}var n={};return e.m=t,e.c=n,e.d=function(t,n,i){e.o(t,n)||Object.defineProperty(t,n,{configurable:!1,enumerable:!0,get:i})},e.n=function(t){var n=t&&t.__esModule?function(){return t.default}:function(){return t};return e.d(n,"a",n),n},e.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},e.p="/",e(e.s=16)}([function(t,e){var n=t.exports="undefined"!=typeof window&&window.Math==Math?window:"undefined"!=typeof self&&self.Math==Math?self:Function("return this")();"number"==typeof __g&&(__g=n)},function(t,e){t.exports=function(t){return"object"==typeof t?null!==t:"function"==typeof t}},function(t,e,n){t.exports=!n(3)(function(){return 7!=Object.defineProperty({},"a",{get:function(){return 7}}).a})},function(t,e){t.exports=function(t){try{return!!t()}catch(t){return!0}}},function(t,e){t.exports="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjE0IiB2aWV3Qm94PSIwIDAgOCAxNCI+CiAgICA8cGF0aCBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIHN0cm9rZT0iIzAwQ0E5RCIgc3Ryb2tlLXdpZHRoPSIxLjUiIGQ9Ik0xLjM2OCAxTDYuNjYgNy4wOTIgMSAxMy41MTIiLz4KPC9zdmc+Cg=="},function(t,e){t.exports=function(t,e,n,i,o,a){var r,s=t=t||{},c=typeof t.default;"object"!==c&&"function"!==c||(r=t,s=t.default);var u="function"==typeof s?s.options:s;e&&(u.render=e.render,u.staticRenderFns=e.staticRenderFns,u._compiled=!0),n&&(u.functional=!0),o&&(u._scopeId=o);var d;if(a?(d=function(t){t=t||this.$vnode&&this.$vnode.ssrContext||this.parent&&this.parent.$vnode&&this.parent.$vnode.ssrContext,t||"undefined"==typeof __VUE_SSR_CONTEXT__||(t=__VUE_SSR_CONTEXT__),i&&i.call(this,t),t&&t._registeredComponents&&t._registeredComponents.add(a)},u._ssrRegister=d):i&&(d=i),d){var h=u.functional,l=h?u.render:u.beforeCreate;h?(u._injectStyles=d,u.render=function(t,e){return d.call(e),l(t,e)}):u.beforeCreate=l?[].concat(l,d):[d]}return{esModule:r,exports:s,options:u}}},function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{default:t}}Object.defineProperty(e,"__esModule",{value:!0});var o=n(7),a=i(o),r=n(52),s=n(13),c=i(s),u=n(53),d=i(u),h=n(15),l=i(h),p={night:"Night",nights:"Nights","day-names":["Sun","Mon","Tue","Wed","Thur","Fri","Sat"],"check-in":"Check-in","check-out":"Check-out","month-names":["January","February","March","April","May","June","July","August","September","October","November","December"]};e.default={name:"HotelDatePicker",directives:{"on-click-outside":r.directive},components:{Day:d.default},props:{value:{type:String},startingDateValue:{default:null,type:Date},endingDateValue:{default:null,type:Date},format:{default:"YYYY-MM-DD",type:String},startDate:{default:function(){return new Date},type:[Date,String]},endDate:{default:1/0,type:[Date,String,Number]},firstDayOfWeek:{default:0,type:Number},minNights:{default:1,type:Number},maxNights:{default:null,type:Number},disabledDates:{default:function(){return[]},type:Array},disabledDaysOfWeek:{default:function(){return[]},type:Array},allowedRanges:{default:function(){return[]},type:Array},hoveringTooltip:{default:!0,type:[Boolean,Function]},tooltipMessage:{default:null,type:String},i18n:{default:function(){return p},type:Object},enableCheckout:{default:!1,type:Boolean},singleDaySelection:{default:!1,type:Boolean},showYear:{default:!1,type:Boolean}},data:function(){return{hoveringDate:null,checkIn:this.startingDateValue,checkOut:this.endingDateValue,currentDate:new Date,months:[],activeMonthIndex:0,nextDisabledDate:null,show:!0,isOpen:!1,xDown:null,yDown:null,xUp:null,yUp:null,sortedDisabledDates:null,screenSize:this.handleWindowResize()}},watch:{isOpen:function(t){if("desktop"!==this.screenSize){var e=document.querySelector("body").classList;t?e.add("-overflow-hidden"):e.remove("-overflow-hidden")}},checkIn:function(t){this.$emit("check-in-changed",t)},checkOut:function(t){null!==this.checkOut&&null!==this.checkOut&&(this.hoveringDate=null,this.nextDisabledDate=null,this.show=!0,this.parseDisabledDates(),this.reRender(),this.isOpen=!1),this.$emit("check-out-changed",t)}},methods:(0,a.default)({},l.default,{handleWindowResize:function(){return window.innerWidth<480?this.screenSize="smartphone":window.innerWidth>=480&&window.innerWidth<768?this.screenSize="tablet":window.innerWidth>=768&&(this.screenSize="desktop"),this.screenSize},onElementHeightChange:function(t,e){var n=t.clientHeight,i=n;!function o(){i=t.clientHeight,n!==i&&e(),n=i,t.onElementHeightChangeTimer&&clearTimeout(t.onElementHeightChangeTimer),t.onElementHeightChangeTimer=setTimeout(o,1e3)}()},emitHeighChangeEvent:function(){this.$emit("heightChanged")},reRender:function(){var t=this;this.show=!1,this.$nextTick(function(){t.show=!0})},clearSelection:function(){this.hoveringDate=null,this.checkIn=null,this.checkOut=null,this.nextDisabledDate=null,this.show=!0,this.parseDisabledDates(),this.reRender()},hideDatepicker:function(){this.isOpen=!1},showDatepicker:function(){this.isOpen=!0},toggleDatepicker:function(){this.isOpen=!this.isOpen},handleDayClick:function(t){null==this.checkIn&&0==this.singleDaySelection?this.checkIn=t.date:1==this.singleDaySelection?(this.checkIn=t.date,this.checkOut=t.date):null!==this.checkIn&&null==this.checkOut?this.checkOut=t.date:(this.checkOut=null,this.checkIn=t.date),this.nextDisabledDate=t.nextDisabledDate},renderPreviousMonth:function(){this.activeMonthIndex>=1&&this.activeMonthIndex--},renderNextMonth:function(){if(this.activeMonthIndex<this.months.length-2)return void this.activeMonthIndex++;var t=void 0;t="desktop"!==this.screenSize?this.months[this.months.length-1].days.filter(function(t){return!0===t.belongsToThisMonth}):this.months[this.activeMonthIndex+1].days.filter(function(t){return!0===t.belongsToThisMonth}),this.endDate!==1/0&&c.default.format(t[0].date,"YYYYMM")==c.default.format(new Date(this.endDate),"YYYYMM")||(this.createMonth(this.getNextMonth(t[0].date)),this.activeMonthIndex++)},setCheckIn:function(t){this.checkIn=t},setCheckOut:function(t){this.checkOut=t},getDay:function(t){return c.default.format(t,"D")},getMonth:function(t){return this.i18n["month-names"][c.default.format(t,"M")-1]+(this.showYear?c.default.format(t," YYYY"):"")},formatDate:function(t){return c.default.format(t,this.format)},createMonth:function(t){for(var e=this.getFirstDay(t,this.firstDayOfWeek),n={days:[]},i=0;i<42;i++)n.days.push({date:this.addDays(e,i),belongsToThisMonth:this.addDays(e,i).getMonth()===t.getMonth(),isInRange:!1});this.months.push(n)},parseDisabledDates:function(){for(var t=[],e=0;e<this.disabledDates.length;e++)t[e]=new Date(this.disabledDates[e]);t.sort(function(t,e){return t-e}),this.sortedDisabledDates=t}}),beforeMount:function(){c.default.i18n={dayNames:this.i18n["day-names"],dayNamesShort:this.shortenString(this.i18n["day-names"],3),monthNames:this.i18n["month-names"],monthNamesShort:this.shortenString(this.i18n["month-names"],3),amPm:["am","pm"],DoFn:function(t){return t+["th","st","nd","rd"][t%10>3?0:(t-t%10!=10)*t%10]}},this.createMonth(new Date(this.startDate)),this.createMonth(this.getNextMonth(new Date(this.startDate))),this.parseDisabledDates()},mounted:function(){var t=this;document.addEventListener("touchstart",this.handleTouchStart,!1),document.addEventListener("touchmove",this.handleTouchMove,!1),window.addEventListener("resize",this.handleWindowResize),this.onElementHeightChange(document.body,function(){t.emitHeighChangeEvent()})},destroyed:function(){window.removeEventListener("touchstart",this.handleTouchStart),window.removeEventListener("touchmove",this.handleTouchMove),window.removeEventListener("resize",this.handleWindowResize)}}},function(t,e,n){"use strict";e.__esModule=!0;var i=n(24),o=function(t){return t&&t.__esModule?t:{default:t}}(i);e.default=o.default||function(t){for(var e=1;e<arguments.length;e++){var n=arguments[e];for(var i in n)Object.prototype.hasOwnProperty.call(n,i)&&(t[i]=n[i])}return t}},function(t,e){var n=t.exports={version:"2.5.1"};"number"==typeof __e&&(__e=n)},function(t,e,n){var i=n(10),o=n(11);t.exports=function(t){return i(o(t))}},function(t,e,n){var i=n(41);t.exports=Object("z").propertyIsEnumerable(0)?Object:function(t){return"String"==i(t)?t.split(""):Object(t)}},function(t,e){t.exports=function(t){if(void 0==t)throw TypeError("Can't call method on  "+t);return t}},function(t,e){var n=Math.ceil,i=Math.floor;t.exports=function(t){return isNaN(t=+t)?0:(t>0?i:n)(t)}},function(t,e,n){var i;!function(o){"use strict";function a(t,e){for(var n=[],i=0,o=t.length;i<o;i++)n.push(t[i].substr(0,e));return n}function r(t){return function(e,n,i){var o=i[t].indexOf(n.charAt(0).toUpperCase()+n.substr(1).toLowerCase());~o&&(e.month=o)}}function s(t,e){for(t=String(t),e=e||2;t.length<e;)t="0"+t;return t}var c={},u=/d{1,4}|M{1,4}|YY(?:YY)?|S{1,3}|Do|ZZ|([HhMsDm])\1?|[aA]|"[^"]*"|'[^']*'/g,d=/\d\d?/,h=/\d{3}/,l=/\d{4}/,p=/[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,f=/\[([^]*?)\]/gm,m=function(){},g=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],y=["January","February","March","April","May","June","July","August","September","October","November","December"],D=a(y,3),k=a(g,3);c.i18n={dayNamesShort:k,dayNames:g,monthNamesShort:D,monthNames:y,amPm:["am","pm"],DoFn:function(t){return t+["th","st","nd","rd"][t%10>3?0:(t-t%10!=10)*t%10]}};var x={D:function(t){return t.getDate()},DD:function(t){return s(t.getDate())},Do:function(t,e){return e.DoFn(t.getDate())},d:function(t){return t.getDay()},dd:function(t){return s(t.getDay())},ddd:function(t,e){return e.dayNamesShort[t.getDay()]},dddd:function(t,e){return e.dayNames[t.getDay()]},M:function(t){return t.getMonth()+1},MM:function(t){return s(t.getMonth()+1)},MMM:function(t,e){return e.monthNamesShort[t.getMonth()]},MMMM:function(t,e){return e.monthNames[t.getMonth()]},YY:function(t){return String(t.getFullYear()).substr(2)},YYYY:function(t){return t.getFullYear()},h:function(t){return t.getHours()%12||12},hh:function(t){return s(t.getHours()%12||12)},H:function(t){return t.getHours()},HH:function(t){return s(t.getHours())},m:function(t){return t.getMinutes()},mm:function(t){return s(t.getMinutes())},s:function(t){return t.getSeconds()},ss:function(t){return s(t.getSeconds())},S:function(t){return Math.round(t.getMilliseconds()/100)},SS:function(t){return s(Math.round(t.getMilliseconds()/10),2)},SSS:function(t){return s(t.getMilliseconds(),3)},a:function(t,e){return t.getHours()<12?e.amPm[0]:e.amPm[1]},A:function(t,e){return t.getHours()<12?e.amPm[0].toUpperCase():e.amPm[1].toUpperCase()},ZZ:function(t){var e=t.getTimezoneOffset();return(e>0?"-":"+")+s(100*Math.floor(Math.abs(e)/60)+Math.abs(e)%60,4)}},b={D:[d,function(t,e){t.day=e}],Do:[new RegExp(d.source+p.source),function(t,e){t.day=parseInt(e,10)}],M:[d,function(t,e){t.month=e-1}],YY:[d,function(t,e){var n=new Date,i=+(""+n.getFullYear()).substr(0,2);t.year=""+(e>68?i-1:i)+e}],h:[d,function(t,e){t.hour=e}],m:[d,function(t,e){t.minute=e}],s:[d,function(t,e){t.second=e}],YYYY:[l,function(t,e){t.year=e}],S:[/\d/,function(t,e){t.millisecond=100*e}],SS:[/\d{2}/,function(t,e){t.millisecond=10*e}],SSS:[h,function(t,e){t.millisecond=e}],d:[d,m],ddd:[p,m],MMM:[p,r("monthNamesShort")],MMMM:[p,r("monthNames")],a:[p,function(t,e,n){var i=e.toLowerCase();i===n.amPm[0]?t.isPm=!1:i===n.amPm[1]&&(t.isPm=!0)}],ZZ:[/([\+\-]\d\d:?\d\d|Z)/,function(t,e){"Z"===e&&(e="+00:00");var n,i=(e+"").match(/([\+\-]|\d\d)/gi);i&&(n=60*i[1]+parseInt(i[2],10),t.timezoneOffset="+"===i[0]?n:-n)}]};b.dd=b.d,b.dddd=b.ddd,b.DD=b.D,b.mm=b.m,b.hh=b.H=b.HH=b.h,b.MM=b.M,b.ss=b.s,b.A=b.a,c.masks={default:"ddd MMM DD YYYY HH:mm:ss",shortDate:"M/D/YY",mediumDate:"MMM D, YYYY",longDate:"MMMM D, YYYY",fullDate:"dddd, MMMM D, YYYY",shortTime:"HH:mm",mediumTime:"HH:mm:ss",longTime:"HH:mm:ss.SSS"},c.format=function(t,e,n){var i=n||c.i18n;if("number"==typeof t&&(t=new Date(t)),"[object Date]"!==Object.prototype.toString.call(t)||isNaN(t.getTime()))throw new Error("Invalid Date in fecha.format");e=c.masks[e]||e||c.masks.default;var o=[];return e=e.replace(f,function(t,e){return o.push(e),"??"}),e=e.replace(u,function(e){return e in x?x[e](t,i):e.slice(1,e.length-1)}),e.replace(/\?\?/g,function(){return o.shift()})},c.parse=function(t,e,n){var i=n||c.i18n;if("string"!=typeof e)throw new Error("Invalid format in fecha.parse");if(e=c.masks[e]||e,t.length>1e3)return!1;var o=!0,a={};if(e.replace(u,function(e){if(b[e]){var n=b[e],r=t.search(n[0]);~r?t.replace(n[0],function(e){return n[1](a,e,i),t=t.substr(r+e.length),e}):o=!1}return b[e]?"":e.slice(1,e.length-1)}),!o)return!1;var r=new Date;!0===a.isPm&&null!=a.hour&&12!=+a.hour?a.hour=+a.hour+12:!1===a.isPm&&12==+a.hour&&(a.hour=0);var s;return null!=a.timezoneOffset?(a.minute=+(a.minute||0)-+a.timezoneOffset,s=new Date(Date.UTC(a.year||r.getFullYear(),a.month||0,a.day||1,a.hour||0,a.minute||0,a.second||0,a.millisecond||0))):s=new Date(a.year||r.getFullYear(),a.month||0,a.day||1,a.hour||0,a.minute||0,a.second||0,a.millisecond||0),s},void 0!==t&&t.exports?t.exports=c:void 0!==(i=function(){return c}.call(e,n,e,t))&&(t.exports=i)}()},function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{default:t}}Object.defineProperty(e,"__esModule",{value:!0});var o=n(7),a=i(o),r=n(13),s=i(r),c=n(15),u=i(c);e.default={name:"Day",props:{sortedDisabledDates:{type:Array},options:{type:Object},checkIn:{type:Date},checkOut:{type:Date},hoveringDate:{type:Date},mounseOverFunction:{type:Function},belongsToThisMonth:{type:Boolean},activeMonthIndex:{type:Number},date:{type:Date},dayNumber:{type:String},nextDisabledDate:{type:[Date,Number,String]},hoveringTooltip:{default:!0,type:Boolean},tooltipMessage:{default:null,type:String}},data:function(){return{isHighlighted:!1,isDisabled:!1,allowedCheckoutDays:[]}},computed:{nightsCount:function(){return this.countDays(this.checkIn,this.hoveringDate)},tooltipMessageDisplay:function(){return this.tooltipMessage?this.tooltipMessage:this.nightsCount+" "+(1!==this.nightsCount?this.options.i18n.nights:this.options.i18n.night)},showTooltip:function(){return!this.isDisabled&&this.date==this.hoveringDate&&null!==this.checkIn&&null==this.checkOut},dayClass:function(){var t=this;return this.belongsToThisMonth?!this.isDisabled&&1==this.compareDay(this.date,this.checkIn)&&this.options.minNights>0&&-1==this.compareDay(this.date,this.addDays(this.checkIn,this.options.minNights))?"datepicker__month-day--selected datepicker__month-day--out-of-range":0===this.options.allowedRanges.length||this.isDisabled||null===this.checkIn||null!=this.checkOut?null!==this.checkIn&&s.default.format(this.checkIn,"YYYYMMDD")==s.default.format(this.date,"YYYYMMDD")?0==this.options.minNights?"datepicker__month-day--first-day-selected":"datepicker__month-day--disabled datepicker__month-day--first-day-selected":null!==this.checkOut&&s.default.format(this.checkOut,"YYYYMMDD")==s.default.format(this.date,"YYYYMMDD")?"datepicker__month-day--disabled datepicker__month-day--last-day-selected":this.isHighlighted&&!this.isDisabled?" datepicker__month-day--selected":this.isDisabled?"datepicker__month-day--disabled":void 0:this.allowedCheckoutDays.some(function(e){return 0==t.compareDay(e,t.date)&&!t.isHighlighted})?"datepicker__month-day--allowed-checkout":this.allowedCheckoutDays.some(function(e){return 0==t.compareDay(e,t.date)&&t.isHighlighted})?"datepicker__month-day--selected datepicker__month-day--allowed-checkout":!this.allowedCheckoutDays.some(function(e){return 0==t.compareDay(e,t.date)})&&this.isHighlighted?"datepicker__month-day--out-of-range datepicker__month-day--selected":"datepicker__month-day datepicker__month-day--out-of-range":this.belongsToThisMonth?"datepicker__month-day--valid":"datepicker__month-day--hidden"}},watch:{hoveringDate:function(t){null!==this.checkIn&&null==this.checkOut&&0==this.isDisabled&&(this.isDateLessOrEquals(this.checkIn,this.date)&&this.isDateLessOrEquals(this.date,this.hoveringDate)?this.isHighlighted=!0:this.isHighlighted=!1),null!==this.checkIn&&null==this.checkOut&&this.allowedCheckoutDays.length},activeMonthIndex:function(t){if(this.checkIfDisabled(),this.checkIfHighlighted(),null!==this.checkIn&&null!==this.checkOut)this.isDateLessOrEquals(this.checkIn,this.date)&&this.isDateLessOrEquals(this.date,this.checkOut)?this.isHighlighted=!0:this.isHighlighted=!1;else{if(null===this.checkIn||null!=this.checkOut)return;this.disableNextDays()}},nextDisabledDate:function(){this.disableNextDays()},checkIn:function(t){this.createAllowedCheckoutDays(t)}},methods:(0,a.default)({},u.default,{compareDay:function(t,e){var n=s.default.format(new Date(t),"YYYYMMDD"),i=s.default.format(new Date(e),"YYYYMMDD");return n>i?1:n==i?0:n<i?-1:void 0},dayClicked:function(t){if(!this.isDisabled){0!==this.options.allowedRanges.length&&this.createAllowedCheckoutDays(t);var e=(this.options.maxNights?this.addDays(this.date,this.options.maxNights):null)||this.allowedCheckoutDays[this.allowedCheckoutDays.length-1]||this.getNextDate(this.sortedDisabledDates,this.date)||this.nextDateByDayOfWeekArray(this.options.disabledDaysOfWeek,this.date)||1/0;this.options.enableCheckout&&(e=1/0),this.$emit("dayClicked",{date:t,nextDisabledDate:e})}},compareEndDay:function(){if(this.options.endDate!==1/0)return 1==this.compareDay(this.date,this.options.endDate)},checkIfDisabled:function(){var t=this;this.isDisabled=(this.sortedDisabledDates?this.sortedDisabledDates.some(function(e){return 0==t.compareDay(e,t.date)}):null)||-1==this.compareDay(this.date,this.options.startDate)||this.compareEndDay()||this.options.disabledDaysOfWeek.some(function(e){return e==s.default.format(t.date,"dddd")}),this.options.enableCheckout&&1==this.compareDay(this.date,this.checkIn)&&-1==this.compareDay(this.date,this.checkOut)&&(this.isDisabled=!1)},checkIfHighlighted:function(){null!==this.checkIn&&null!==this.checkOut&&0==this.isDisabled&&(this.isDateLessOrEquals(this.checkIn,this.date)&&this.isDateLessOrEquals(this.date,this.checkOut)?this.isHighlighted=!0:this.isHighlighted=!1)},createAllowedCheckoutDays:function(t){var e=this;this.allowedCheckoutDays=[],this.options.allowedRanges.forEach(function(n){return e.allowedCheckoutDays.push(e.addDays(t,n))}),this.allowedCheckoutDays.sort(function(t,e){return t-e})},disableNextDays:function(){this.isDateLessOrEquals(this.date,this.nextDisabledDate)||this.nextDisabledDate===1/0?this.isDateLessOrEquals(this.date,this.checkIn)&&(this.isDisabled=!0):this.isDisabled=!0,0==this.compareDay(this.date,this.checkIn)&&0==this.options.minNights&&(this.isDisabled=!1),this.isDateLessOrEquals(this.checkIn,this.date)&&this.options.enableCheckout&&(this.isDisabled=!1)}}),beforeMount:function(){this.checkIfDisabled(),this.checkIfHighlighted()}}},function(t,e,n){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.default={getNextDate:function(t,e){var n=new Date(e),i=1/0;return t.forEach(function(t){var e=new Date(t);e>=n&&e<i&&(i=t)}),i===1/0?null:i},nextDateByDayOfWeek:function(t,e){e=new Date(e),t=t.toLowerCase();for(var n=["sunday","monday","tuesday","wednesday","thursday","friday","saturday"],i=e.getDay(),o=7;o--;)if(t===n[o]){t=o<=i?o+7:o;break}var a=t-i;return e.setDate(e.getDate()+a)},nextDateByDayOfWeekArray:function(t,e){for(var n=[],i=0;i<t.length;i++)n.push(new Date(this.nextDateByDayOfWeek(t[i],e)));return this.getNextDate(n,e)},isDateLessOrEquals:function(t,e){return new Date(t)<=new Date(e)},countDays:function(t,e){var n=new Date(t),i=new Date(e);return Math.round(Math.abs((n.getTime()-i.getTime())/864e5))},addDays:function(t,e){var n=new Date(t);return n.setDate(n.getDate()+e),n},getFirstDay:function(t,e){var n=this.getFirstDayOfMonth(t),i=0;return e>0&&(i=0===n.getDay()?-7+e:e),new Date(n.setDate(n.getDate()-(n.getDay()-i)))},getFirstDayOfMonth:function(t){return new Date(t.getFullYear(),t.getMonth(),1)},getNextMonth:function(t){return 11==t.getMonth()?new Date(t.getFullYear()+1,0,1):new Date(t.getFullYear(),t.getMonth()+1,1)},swipeAfterScroll:function(t){if("desktop"!==this.screenSize&&this.isOpen){var e=document.getElementById("swiperWrapper");if(e.scrollHeight>e.clientHeight)if(e.scrollTop===e.scrollHeight-e.offsetHeight)this.renderNextMonth();else{if(0!==e.scrollTop)return;this.renderPreviousMonth()}else"up"==t?this.renderNextMonth():"down"==t&&this.renderPreviousMonth()}},handleTouchStart:function(t){this.xDown=t.touches[0].clientX,this.yDown=t.touches[0].clientY},handleTouchMove:function(t){if(this.xDown&&this.yDown){this.xUp=t.touches[0].clientX,this.yUp=t.touches[0].clientY;var e=this.xDown-this.xUp,n=this.yDown-this.yUp;Math.abs(e)>Math.abs(n)||(n>0?this.swipeAfterScroll("up"):this.swipeAfterScroll("down")),this.xDown=null,this.yDown=null}},shortenString:function(t,e){for(var n=[],i=0,o=t.length;i<o;i++)n.push(t[i].substr(0,e));return n}}},function(t,e,n){"use strict";function i(t){c||n(17)}Object.defineProperty(e,"__esModule",{value:!0});var o=n(6),a=n.n(o);for(var r in o)"default"!==r&&function(t){n.d(e,t,function(){return o[t]})}(r);var s=n(55),c=!1,u=n(5),d=i,h=u(a.a,s.a,!1,d,null,null);h.options.__file="src\\components\\DatePicker.vue",e.default=h.exports},function(t,e,n){var i=n(18);"string"==typeof i&&(i=[[t.i,i,""]]),i.locals&&(t.exports=i.locals);n(22)("ade00f98",i,!1,{})},function(t,e,n){e=t.exports=n(19)(void 0),e.push([t.i,"\n.square{width:14.28571%;float:left\n}\n@media screen and (min-width:768px){\n.square{cursor:pointer\n}\n}\n*,:after,:before{-webkit-box-sizing:border-box;box-sizing:border-box\n}\n.datepicker{-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;transition:all .2s ease-in-out;background-color:#fff;color:#424b53;font-size:16px;line-height:14px;overflow:hidden;left:0;top:48px;position:absolute;z-index:2\n}\n.datepicker button.next--mobile{background:none;border:1px solid #d7d9e2;float:none;height:50px;width:100%;position:relative;background-position:50%;-webkit-appearance:none;-moz-appearance:none;appearance:none;overflow:hidden;position:fixed;bottom:0;left:0;outline:none;-webkit-box-shadow:0 5px 30px 10px rgba(0,0,0,.08);box-shadow:0 5px 30px 10px rgba(0,0,0,.08);background:#fff\n}\n.datepicker button.next--mobile:after{background:transparent url("+n(4)+') no-repeat 50%/8px;-webkit-transform:rotate(90deg);-ms-transform:rotate(90deg);transform:rotate(90deg);content:"";position:absolute;width:200%;height:200%;top:-50%;left:-50%\n}\n.datepicker--closed{-webkit-box-shadow:0 15px 30px 10px transparent;box-shadow:0 15px 30px 10px transparent;max-height:0\n}\n.datepicker--open{-webkit-box-shadow:0 15px 30px 10px rgba(0,0,0,.08);box-shadow:0 15px 30px 10px rgba(0,0,0,.08);max-height:900px\n}\n@media screen and (max-width:767px){\n.datepicker--open{-webkit-box-shadow:none;box-shadow:none;height:100%;left:0;right:0;bottom:0;-webkit-overflow-scrolling:touch!important;position:fixed;top:0;width:100%\n}\n}\n.datepicker__wrapper{position:relative;display:inline-block;width:100%;height:48px;background:#fff url('+n(20)+") no-repeat 17px/16px\n}\n.datepicker__input{background:transparent;height:48px;color:#35343d;font-size:12px;outline:none;padding:4px 30px 2px;width:100%;word-spacing:5px;border:0\n}\n.datepicker__input:focus{outline:none\n}\n.datepicker__input:-moz-placeholder,.datepicker__input:-ms-input-placeholder,.datepicker__input::-moz-placeholder,.datepicker__input::-webkit-input-placeholder{color:#35343d\n}\n.datepicker__dummy-wrapper{border:1px solid #d7d9e2;cursor:pointer;display:block;float:left;width:100%;height:100%\n}\n.datepicker__dummy-wrapper--no-border.datepicker__dummy-wrapper{margin-top:15px;border:0\n}\n.datepicker__dummy-wrapper--is-active{border:1px solid #00ca9d\n}\n.datepicker__dummy-input{color:#35343d;padding-top:0;font-size:14px;float:left;height:48px;line-height:3.1;text-align:left;text-indent:5px;width:-webkit-calc(50% + 4px);width:calc(50% + 4px)\n}\n@media screen and (max-width:479px){\n.datepicker__dummy-input{text-indent:0;text-align:center\n}\n}\n.datepicker__dummy-input:first-child{background:transparent url("+n(21)+") no-repeat 100%/8px;width:-webkit-calc(50% - 4px);width:calc(50% - 4px);text-indent:20px\n}\n.datepicker__dummy-input--is-active{color:#00ca9d\n}\n.datepicker__dummy-input--is-active::-webkit-input-placeholder{color:#00ca9d\n}\n.datepicker__dummy-input--is-active::placeholder{color:#00ca9d\n}\n.datepicker__dummy-input--is-active::-moz-placeholder{color:#00ca9d\n}\n.datepicker__dummy-input--is-active:-ms-input-placeholder{color:#00ca9d\n}\n.datepicker__dummy-input--is-active:-moz-placeholder{color:#00ca9d\n}\n.datepicker__dummy-input--single-date:first-child{width:100%;background:none;text-align:left\n}\n.datepicker__month-day{visibility:visible;will-change:auto;text-align:center;margin:0;border:0;height:40px;padding-top:15px\n}\n.datepicker__month-day--invalid-range{background-color:rgba(0,202,157,.3);color:#f3f5f8;cursor:not-allowed;position:relative\n}\n.datepicker__month-day--invalid{color:#f3f5f8;cursor:not-allowed\n}\n.datepicker__month-day--allowed-checkout:hover,.datepicker__month-day--valid:hover{background-color:#fff;color:#00ca9d;z-index:1;position:relative;-webkit-box-shadow:0 0 10px 3px rgba(66,75,83,.4);box-shadow:0 0 10px 3px rgba(66,75,83,.4)\n}\n.datepicker__month-day--disabled{opacity:.25;cursor:not-allowed;pointer-events:none;position:relative\n}\n.datepicker__month-day--selected{background-color:rgba(0,202,157,.5);color:#fff\n}\n.datepicker__month-day--selected:hover{background-color:#fff;color:#00ca9d;z-index:1;position:relative;-webkit-box-shadow:0 0 10px 3px rgba(66,75,83,.4);box-shadow:0 0 10px 3px rgba(66,75,83,.4)\n}\n.datepicker__month-day--today{background-color:#d7d9e2;color:#999\n}\n.datepicker__month-day--first-day-selected,.datepicker__month-day--last-day-selected{background:#00ca9d;color:#fff\n}\n.datepicker__month-day--allowed-checkout{color:#999\n}\n.datepicker__month-day--out-of-range{color:#f3f5f8;cursor:not-allowed;position:relative;pointer-events:none\n}\n.datepicker__month-day--valid{cursor:pointer;color:#999\n}\n.datepicker__month-day--hidden{opacity:.25;pointer-events:none;color:#fff\n}\n.datepicker__month-button{background:transparent url("+n(4)+') no-repeat 100%/8px;cursor:pointer;display:inline-block;height:60px;width:60px\n}\n.datepicker__month-button--prev{-webkit-transform:rotateY(180deg);transform:rotateY(180deg)\n}\n.datepicker__month-button--next{float:right\n}\n.datepicker__month-button--locked{opacity:.2;cursor:not-allowed\n}\n.datepicker__inner{padding:20px;float:left\n}\n@media screen and (max-width:767px){\n.datepicker__inner{padding:0\n}\n}\n@media screen and (min-width:768px){\n.datepicker__months{width:650px\n}\n}\n@media screen and (max-width:767px){\n.datepicker__months{margin-top:92px;height:-webkit-calc(100% - 92px);height:calc(100% - 92px);position:absolute;left:0;top:0;overflow:scroll;right:0;bottom:0;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:start;-webkit-justify-content:flex-start;-ms-flex-pack:start;justify-content:flex-start\n}\n}\n.datepicker__months:before{background:#d7d9e2;bottom:0;content:"";display:block;left:50%;position:absolute;top:0;width:1px\n}\n@media screen and (max-width:767px){\n.datepicker__months:before{display:none\n}\n}\n.datepicker__month{font-size:12px;float:left;width:50%;padding-right:10px\n}\n@media screen and (max-width:767px){\n.datepicker__month{width:100%;padding-right:0;padding-top:60px\n}\n.datepicker__month:last-of-type{margin-bottom:65px\n}\n}\n@media screen and (min-width:768px){\n.datepicker__month:last-of-type{padding-right:0;padding-left:10px\n}\n}\n.datepicker__month-caption{height:2.5em;vertical-align:middle\n}\n.datepicker__month-name{font-size:16px;font-weight:500;margin-top:-40px;padding-bottom:17px;pointer-events:none;text-align:center\n}\n@media screen and (max-width:767px){\n.datepicker__month-name{margin-top:-25px;margin-bottom:0;position:absolute;width:100%\n}\n}\n.datepicker__week-days{height:2em;vertical-align:middle\n}\n.datepicker__week-row{border-bottom:5px solid #fff;height:38px\n}\n@media screen and (max-width:767px){\n.datepicker__week-row{-webkit-box-shadow:0 13px 18px -8px rgba(0,0,0,.07);box-shadow:0 13px 18px -8px rgba(0,0,0,.07);height:25px;left:0;top:65px;position:absolute;width:100%\n}\n}\n.datepicker__week-name{width:14.28571%;float:left;font-size:12px;font-weight:400;color:#999;text-align:center\n}\n.datepicker__close-button{font-size:21px;margin-top:0;z-index:4;position:fixed;left:7px;top:5px\n}\n.datepicker__clear-button,.datepicker__close-button{appearence:none;background:transparent;border:0;color:#00ca9d;cursor:pointer;font-weight:700;outline:0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)\n}\n.datepicker__clear-button{font-size:25px;height:40px;margin:4px -2px 0 0;padding:0;position:absolute;right:0;top:0;width:40px\n}\n.datepicker__tooltip{background-color:#2d3047;-webkit-border-radius:2px;border-radius:2px;color:#fff;font-size:11px;margin-left:5px;margin-top:-22px;padding:5px 10px;position:absolute;z-index:3\n}\n.datepicker__tooltip:after{border-left:4px solid transparent;border-right:4px solid transparent;border-top:4px solid #2d3047;bottom:-4px;content:"";left:50%;margin-left:-4px;position:absolute\n}\n.-overflow-hidden{overflow:hidden\n}\n.-is-hidden{display:none\n}\n@media screen and (max-width:767px){\n.-hide-up-to-tablet{display:none\n}\n}\n@media screen and (min-width:768px){\n.-hide-on-desktop{display:none\n}\n}',""])},function(t,e){function n(t,e){var n=t[1]||"",o=t[3];if(!o)return n;if(e&&"function"==typeof btoa){var a=i(o);return[n].concat(o.sources.map(function(t){return"/*# sourceURL="+o.sourceRoot+t+" */"})).concat([a]).join("\n")}return[n].join("\n")}function i(t){return"/*# sourceMappingURL=data:application/json;charset=utf-8;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(t))))+" */"}t.exports=function(t){var e=[];return e.toString=function(){return this.map(function(e){var i=n(e,t);return e[2]?"@media "+e[2]+"{"+i+"}":i}).join("")},e.i=function(t,n){"string"==typeof t&&(t=[[null,t,""]]);for(var i={},o=0;o<this.length;o++){var a=this[o][0];"number"==typeof a&&(i[a]=!0)}for(o=0;o<t.length;o++){var r=t[o];"number"==typeof r[0]&&i[r[0]]||(n&&!r[2]?r[2]=n:n&&(r[2]="("+r[2]+") and ("+n+")"),e.push(r))}},e}},function(t,e){t.exports="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMTYiIGhlaWdodD0iMTgiIHZpZXdCb3g9IjAgMCAxNiAxOCI+CiAgICA8ZGVmcz4KICAgICAgICA8cGF0aCBpZD0iYSIgZD0iTTAgMTcuMzExaDE1Ljc1NFYuMTE1SDB6Ii8+CiAgICA8L2RlZnM+CiAgICA8ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPgogICAgICAgIDxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgLjE0OCkiPgogICAgICAgICAgICA8bWFzayBpZD0iYiIgZmlsbD0iI2ZmZiI+CiAgICAgICAgICAgICAgICA8dXNlIHhsaW5rOmhyZWY9IiNhIi8+CiAgICAgICAgICAgIDwvbWFzaz4KICAgICAgICAgICAgPHBhdGggZmlsbD0iIzAwQ0E5RCIgZD0iTTE0LjQ2NSAxNi4yMTVIMS4yOWEuMTk2LjE5NiAwIDAgMS0uMTk0LS4xOTN2LTguNjloMTMuNTYydjguNjlhLjE5Ni4xOTYgMCAwIDEtLjE5My4xOTN6TTEuMjkgMi45NThoMS4wMTN2LjU2M2MwIC44MTcuNjY0IDEuNDgyIDEuNDgxIDEuNDgyLjgxOCAwIDEuNDgyLS42NjUgMS40ODItMS40ODJ2LS41NjNoNS4wNDZ2LjU2M2MwIC44MTcuNjY1IDEuNDgyIDEuNDgyIDEuNDgyczEuNDgyLS42NjUgMS40ODItMS40ODJ2LS41NjNoMS4xODljLjEwNSAwIC4xOTMuMDg5LjE5My4xOTN2My4xNDJIMS4wOTZWMy4xNTFjMC0uMTA0LjA4OS0uMTkzLjE5NC0uMTkzek0zLjM0IDEuNTk2YS40NDQuNDQ0IDAgMCAxIC44ODcgMHYxLjkyNWEuNDQ0LjQ0NCAwIDAgMS0uODg3IDBWMS41OTZ6bTguMDEgMGEuNDQ0LjQ0NCAwIDAgMSAuODg2IDB2MS45MjVhLjQ0NC40NDQgMCAwIDEtLjg4NyAwVjEuNTk2em0zLjExNC4yNjZoLTEuMTl2LS4yNjZjMC0uODE3LS42NjQtMS40ODItMS40ODEtMS40ODItLjgxNyAwLTEuNDgyLjY2NS0xLjQ4MiAxLjQ4MnYuMjY2SDUuMjY2di0uMjY2QzUuMjY2Ljc4IDQuNjAyLjExNCAzLjc4NC4xMTRjLS44MTcgMC0xLjQ4MS42NjUtMS40ODEgMS40ODJ2LjI2NkgxLjI5Qy41NzkgMS44NjIgMCAyLjQ0IDAgMy4xNTJ2MTIuODdjMCAuNzExLjU3OSAxLjI5IDEuMjkgMS4yOWgxMy4xNzVhMS4yOSAxLjI5IDAgMCAwIDEuMjktMS4yOVYzLjE1MmExLjI5IDEuMjkgMCAwIDAtMS4yOS0xLjI5eiIgbWFzaz0idXJsKCNiKSIvPgogICAgICAgIDwvZz4KICAgICAgICA8cGF0aCBmaWxsPSIjMDBDQTlEIiBkPSJNMy4xOTQgMTAuOTk4aDEuMzU3VjkuNjQySDMuMTk0ek01Ljg2NCAxMC45OThoMS4zNTdWOS42NDJINS44NjR6TTguNTM0IDEwLjk5OEg5Ljg5VjkuNjQySDguNTM0ek0xMS4yMDQgMTAuOTk4aDEuMzU2VjkuNjQyaC0xLjM1NnpNMy4xOTQgMTMuOTRoMS4zNTd2LTEuMzU3SDMuMTk0ek01Ljg2NCAxMy45NGgxLjM1N3YtMS4zNTdINS44NjR6TTguNTM0IDEzLjk0SDkuODl2LTEuMzU3SDguNTM0ek0xMS4yMDQgMTMuOTRoMS4zNTZ2LTEuMzU3aC0xLjM1NnoiLz4KICAgIDwvZz4KPC9zdmc+Cg=="},function(t,e){t.exports="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjE4IiB2aWV3Qm94PSIwIDAgOCAxOCI+CiAgICA8cGF0aCBmaWxsPSIjOTU5OUFBIiBmaWxsLXJ1bGU9Im5vbnplcm8iIGQ9Ik0uMTE5LjcxOGw3LjE1OCA3LjQwNy0uMDMzLS41NTEtNi43MzcgOC44ODlhLjQyNS40MjUgMCAwIDAgLjA4LjU5My40Mi40MiAwIDAgMCAuNTktLjA4bDYuNzM3LTguODg5YS40MjUuNDI1IDAgMCAwLS4wMzMtLjU1MUwuNzIzLjEyOEEuNDIuNDIgMCAwIDAgLjEyOC4xMmEuNDI1LjQyNSAwIDAgMC0uMDA5LjU5OHoiLz4KPC9zdmc+Cg=="},function(t,e,n){function i(t){for(var e=0;e<t.length;e++){var n=t[e],i=d[n.id];if(i){i.refs++;for(var o=0;o<i.parts.length;o++)i.parts[o](n.parts[o]);for(;o<n.parts.length;o++)i.parts.push(a(n.parts[o]));i.parts.length>n.parts.length&&(i.parts.length=n.parts.length)}else{for(var r=[],o=0;o<n.parts.length;o++)r.push(a(n.parts[o]));d[n.id]={id:n.id,refs:1,parts:r}}}}function o(){var t=document.createElement("style");return t.type="text/css",h.appendChild(t),t}function a(t){var e,n,i=document.querySelector("style["+y+'~="'+t.id+'"]');if(i){if(f)return m;i.parentNode.removeChild(i)}if(D){var a=p++;i=l||(l=o()),e=r.bind(null,i,a,!1),n=r.bind(null,i,a,!0)}else i=o(),e=s.bind(null,i),n=function(){i.parentNode.removeChild(i)};return e(t),function(i){if(i){if(i.css===t.css&&i.media===t.media&&i.sourceMap===t.sourceMap)return;e(t=i)}else n()}}function r(t,e,n,i){var o=n?"":i.css;if(t.styleSheet)t.styleSheet.cssText=k(e,o);else{var a=document.createTextNode(o),r=t.childNodes;r[e]&&t.removeChild(r[e]),r.length?t.insertBefore(a,r[e]):t.appendChild(a)}}function s(t,e){var n=e.css,i=e.media,o=e.sourceMap;if(i&&t.setAttribute("media",i),g.ssrId&&t.setAttribute(y,e.id),o&&(n+="\n/*# sourceURL="+o.sources[0]+" */",n+="\n/*# sourceMappingURL=data:application/json;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(o))))+" */"),t.styleSheet)t.styleSheet.cssText=n;else{for(;t.firstChild;)t.removeChild(t.firstChild);t.appendChild(document.createTextNode(n))}}var c="undefined"!=typeof document;if("undefined"!=typeof DEBUG&&DEBUG&&!c)throw new Error("vue-style-loader cannot be used in a non-browser environment. Use { target: 'node' } in your Webpack config to indicate a server-rendering environment.");var u=n(23),d={},h=c&&(document.head||document.getElementsByTagName("head")[0]),l=null,p=0,f=!1,m=function(){},g=null,y="data-vue-ssr-id",D="undefined"!=typeof navigator&&/msie [6-9]\b/.test(navigator.userAgent.toLowerCase());t.exports=function(t,e,n,o){f=n,g=o||{};var a=u(t,e);return i(a),function(e){for(var n=[],o=0;o<a.length;o++){var r=a[o],s=d[r.id];s.refs--,n.push(s)}e?(a=u(t,e),i(a)):a=[];for(var o=0;o<n.length;o++){var s=n[o];if(0===s.refs){for(var c=0;c<s.parts.length;c++)s.parts[c]();delete d[s.id]}}}};var k=function(){var t=[];return function(e,n){return t[e]=n,t.filter(Boolean).join("\n")}}()},function(t,e){t.exports=function(t,e){for(var n=[],i={},o=0;o<e.length;o++){var a=e[o],r=a[0],s=a[1],c=a[2],u=a[3],d={id:t+":"+o,css:s,media:c,sourceMap:u};i[r]?i[r].parts.push(d):n.push(i[r]={id:r,parts:[d]})}return n}},function(t,e,n){t.exports={default:n(25),__esModule:!0}},function(t,e,n){n(26),t.exports=n(8).Object.assign},function(t,e,n){var i=n(27);i(i.S+i.F,"Object",{assign:n(37)})},function(t,e,n){var i=n(0),o=n(8),a=n(28),r=n(30),s=function(t,e,n){var c,u,d,h=t&s.F,l=t&s.G,p=t&s.S,f=t&s.P,m=t&s.B,g=t&s.W,y=l?o:o[e]||(o[e]={}),D=y.prototype,k=l?i:p?i[e]:(i[e]||{}).prototype;l&&(n=e);for(c in n)(u=!h&&k&&void 0!==k[c])&&c in y||(d=u?k[c]:n[c],y[c]=l&&"function"!=typeof k[c]?n[c]:m&&u?a(d,i):g&&k[c]==d?function(t){var e=function(e,n,i){if(this instanceof t){switch(arguments.length){case 0:return new t;case 1:return new t(e);case 2:return new t(e,n)}return new t(e,n,i)}return t.apply(this,arguments)};return e.prototype=t.prototype,e}(d):f&&"function"==typeof d?a(Function.call,d):d,f&&((y.virtual||(y.virtual={}))[c]=d,t&s.R&&D&&!D[c]&&r(D,c,d)))};s.F=1,s.G=2,s.S=4,s.P=8,s.B=16,s.W=32,s.U=64,s.R=128,t.exports=s},function(t,e,n){var i=n(29);t.exports=function(t,e,n){if(i(t),void 0===e)return t;switch(n){case 1:return function(n){return t.call(e,n)};case 2:return function(n,i){return t.call(e,n,i)};case 3:return function(n,i,o){return t.call(e,n,i,o)}}return function(){return t.apply(e,arguments)}}},function(t,e){t.exports=function(t){if("function"!=typeof t)throw TypeError(t+" is not a function!");return t}},function(t,e,n){var i=n(31),o=n(36);t.exports=n(2)?function(t,e,n){return i.f(t,e,o(1,n))}:function(t,e,n){return t[e]=n,t}},function(t,e,n){var i=n(32),o=n(33),a=n(35),r=Object.defineProperty;e.f=n(2)?Object.defineProperty:function(t,e,n){if(i(t),e=a(e,!0),i(n),o)try{return r(t,e,n)}catch(t){}if("get"in n||"set"in n)throw TypeError("Accessors not supported!");return"value"in n&&(t[e]=n.value),t}},function(t,e,n){var i=n(1);t.exports=function(t){if(!i(t))throw TypeError(t+" is not an object!");return t}},function(t,e,n){t.exports=!n(2)&&!n(3)(function(){return 7!=Object.defineProperty(n(34)("div"),"a",{get:function(){return 7}}).a})},function(t,e,n){var i=n(1),o=n(0).document,a=i(o)&&i(o.createElement);t.exports=function(t){return a?o.createElement(t):{}}},function(t,e,n){var i=n(1);t.exports=function(t,e){if(!i(t))return t;var n,o;if(e&&"function"==typeof(n=t.toString)&&!i(o=n.call(t)))return o;if("function"==typeof(n=t.valueOf)&&!i(o=n.call(t)))return o;if(!e&&"function"==typeof(n=t.toString)&&!i(o=n.call(t)))return o;throw TypeError("Can't convert object to primitive value")}},function(t,e){t.exports=function(t,e){return{enumerable:!(1&t),configurable:!(2&t),writable:!(4&t),value:e}}},function(t,e,n){"use strict";var i=n(38),o=n(49),a=n(50),r=n(51),s=n(10),c=Object.assign;t.exports=!c||n(3)(function(){var t={},e={},n=Symbol(),i="abcdefghijklmnopqrst";return t[n]=7,i.split("").forEach(function(t){e[t]=t}),7!=c({},t)[n]||Object.keys(c({},e)).join("")!=i})?function(t,e){for(var n=r(t),c=arguments.length,u=1,d=o.f,h=a.f;c>u;)for(var l,p=s(arguments[u++]),f=d?i(p).concat(d(p)):i(p),m=f.length,g=0;m>g;)h.call(p,l=f[g++])&&(n[l]=p[l]);return n}:c},function(t,e,n){var i=n(39),o=n(48);t.exports=Object.keys||function(t){return i(t,o)}},function(t,e,n){var i=n(40),o=n(9),a=n(42)(!1),r=n(45)("IE_PROTO");t.exports=function(t,e){var n,s=o(t),c=0,u=[];for(n in s)n!=r&&i(s,n)&&u.push(n);for(;e.length>c;)i(s,n=e[c++])&&(~a(u,n)||u.push(n));return u}},function(t,e){var n={}.hasOwnProperty;t.exports=function(t,e){return n.call(t,e)}},function(t,e){var n={}.toString;t.exports=function(t){return n.call(t).slice(8,-1)}},function(t,e,n){var i=n(9),o=n(43),a=n(44);t.exports=function(t){return function(e,n,r){var s,c=i(e),u=o(c.length),d=a(r,u);if(t&&n!=n){for(;u>d;)if((s=c[d++])!=s)return!0}else for(;u>d;d++)if((t||d in c)&&c[d]===n)return t||d||0;return!t&&-1}}},function(t,e,n){var i=n(12),o=Math.min;t.exports=function(t){return t>0?o(i(t),9007199254740991):0}},function(t,e,n){var i=n(12),o=Math.max,a=Math.min;t.exports=function(t,e){return t=i(t),t<0?o(t+e,0):a(t,e)}},function(t,e,n){var i=n(46)("keys"),o=n(47);t.exports=function(t){return i[t]||(i[t]=o(t))}},function(t,e,n){var i=n(0),o=i["__core-js_shared__"]||(i["__core-js_shared__"]={});t.exports=function(t){return o[t]||(o[t]={})}},function(t,e){var n=0,i=Math.random();t.exports=function(t){return"Symbol(".concat(void 0===t?"":t,")_",(++n+i).toString(36))}},function(t,e){t.exports="constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")},function(t,e){e.f=Object.getOwnPropertySymbols},function(t,e){e.f={}.propertyIsEnumerable},function(t,e,n){var i=n(11);t.exports=function(t){return Object(i(t))}},function(t,e,n){!function(t,n){n(e)}(0,function(t){"use strict";function e(t,e,n){return t.addEventListener(e,n,!1),{destroy:function(){return t.removeEventListener(e,n,!1)}}}function n(t,n){var i=!1,o=e(t,"mouseenter",function(){i=!0}),a=e(t,"mouseleave",function(){i=!1});return{el:t,check:function(t){i||n(t)},destroy:function(){o.destroy(),a.destroy()}}}function i(t,e){return{el:t,check:function(n){t.contains(n.target)||e(n)},destroy:function(){}}}function o(t,o){var a=o.value,u=o.modifiers;r(t),c||(c=e(document.documentElement,"click",function(t){s.forEach(function(e){return e.check(t)})})),setTimeout(function(){s.push(u.static?i(t,a):n(t,a))},0)}function a(t,e){e.value!==e.oldValue&&o(t,e)}function r(t){for(var e=s.length-1;e>=0;)s[e].el===t&&(s[e].destroy(),s.splice(e,1)),e-=1;0===s.length&&c&&(c.destroy(),c=null)}var s=[],c=void 0,u={bind:o,unbind:r,update:a},d={directives:{"on-click-outside":u}};t.directive=u,t.mixin=d,Object.defineProperty(t,"__esModule",{value:!0})})},function(t,e,n){"use strict";Object.defineProperty(e,"__esModule",{value:!0});var i=n(14),o=n.n(i);for(var a in i)"default"!==a&&function(t){n.d(e,t,function(){return i[t]})}(a);var r=n(54),s=n(5),c=s(o.a,r.a,!1,null,null,null);c.options.__file="src\\components\\Day.vue",e.default=c.exports},function(t,e,n){"use strict";var i=function(){var t=this,e=t.$createElement,n=t._self._c||e;return n("div",[n("span"),t.showTooltip&&this.options.hoveringTooltip?n("div",{staticClass:"datepicker__tooltip",domProps:{innerHTML:t._s(t.tooltipMessageDisplay)}}):t._e(),n("div",{staticClass:"datepicker__month-day",class:t.dayClass,domProps:{textContent:t._s(""+t.dayNumber)},on:{click:function(e){t.dayClicked(t.date)}}})])},o=[];i._withStripped=!0;var a={render:i,staticRenderFns:o};e.a=a},function(t,e,n){"use strict";var i=function(){var t=this,e=t.$createElement,n=t._self._c||e;return t.show?n("div",{directives:[{name:"on-click-outside",rawName:"v-on-click-outside",value:t.hideDatepicker,expression:"hideDatepicker"}],staticClass:"datepicker__wrapper"},[t.isOpen?n("div",{staticClass:"datepicker__close-button -hide-on-desktop",on:{click:t.hideDatepicker}},[t._v("＋")]):t._e(),n("div",{staticClass:"datepicker__dummy-wrapper",class:t.isOpen?"datepicker__dummy-wrapper--is-active":"",on:{click:function(e){t.isOpen=!t.isOpen}}},[n("button",{staticClass:"datepicker__dummy-input datepicker__input",class:(t.isOpen&&null==t.checkIn?"datepicker__dummy-input--is-active":"")+" "+(t.singleDaySelection?"datepicker__dummy-input--single-date":""),attrs:{"data-qa":"datepickerInput",type:"button"},domProps:{textContent:t._s(""+(t.checkIn?t.formatDate(t.checkIn):t.i18n["check-in"]))}}),t.singleDaySelection?t._e():n("button",{staticClass:"datepicker__dummy-input datepicker__input",class:t.isOpen&&null==t.checkOut&&null!==t.checkIn?"datepicker__dummy-input--is-active":"",attrs:{type:"button"},domProps:{textContent:t._s(""+(t.checkOut?t.formatDate(t.checkOut):t.i18n["check-out"]))}})]),n("button",{staticClass:"datepicker__clear-button",attrs:{type:"button"},on:{click:t.clearSelection}},[t._v("＋")]),n("div",{staticClass:"datepicker",class:t.isOpen?"datepicker--open":"datepicker--closed"},[n("div",{staticClass:"-hide-on-desktop"},[t.isOpen?n("div",{staticClass:"datepicker__dummy-wrapper datepicker__dummy-wrapper--no-border",class:t.isOpen?"datepicker__dummy-wrapper--is-active":"",on:{click:function(e){t.isOpen=!t.isOpen}}},[n("button",{staticClass:"datepicker__dummy-input datepicker__input",class:t.isOpen&&null==t.checkIn?"datepicker__dummy-input--is-active":"",attrs:{type:"button"},domProps:{textContent:t._s(""+(t.checkIn?t.formatDate(t.checkIn):t.i18n["check-in"]))}}),n("button",{staticClass:"datepicker__dummy-input datepicker__input",class:t.isOpen&&null==t.checkOut&&null!==t.checkIn?"datepicker__dummy-input--is-active":"",attrs:{type:"button"},domProps:{textContent:t._s(""+(t.checkOut?t.formatDate(t.checkOut):t.i18n["check-out"]))}})]):t._e()]),n("div",{staticClass:"datepicker__inner"},[n("div",{staticClass:"datepicker__header"},[n("span",{staticClass:"datepicker__month-button datepicker__month-button--prev -hide-up-to-tablet",on:{click:t.renderPreviousMonth}}),n("span",{staticClass:"datepicker__month-button datepicker__month-button--next -hide-up-to-tablet",on:{click:t.renderNextMonth}})]),"desktop"==t.screenSize?n("div",{staticClass:"datepicker__months"},t._l([0,1],function(e){return n("div",{key:e,staticClass:"datepicker__month"},[n("h1",{staticClass:"datepicker__month-name",domProps:{textContent:t._s(t.getMonth(t.months[t.activeMonthIndex+e].days[15].date))}}),n("div",{staticClass:"datepicker__week-row -hide-up-to-tablet"},t._l(t.i18n["day-names"],function(e){return n("div",{staticClass:"datepicker__week-name",domProps:{textContent:t._s(e)}})})),t._l(t.months[t.activeMonthIndex+e].days,function(e){return n("div",{staticClass:"square",on:{mouseover:function(n){t.hoveringDate=e.date}}},[n("Day",{attrs:{options:t.$props,date:e.date,sortedDisabledDates:t.sortedDisabledDates,nextDisabledDate:t.nextDisabledDate,activeMonthIndex:t.activeMonthIndex,hoveringDate:t.hoveringDate,tooltipMessage:t.tooltipMessage,dayNumber:t.getDay(e.date),belongsToThisMonth:e.belongsToThisMonth,checkIn:t.checkIn,checkOut:t.checkOut},on:{dayClicked:function(e){t.handleDayClick(e)}}})],1)})],2)})):t._e(),"desktop"!==t.screenSize&&t.isOpen?n("div",[n("div",{staticClass:"datepicker__week-row"},t._l(this.i18n["day-names"],function(e){return n("div",{staticClass:"datepicker__week-name",domProps:{textContent:t._s(e)}})})),n("div",{staticClass:"datepicker__months",attrs:{id:"swiperWrapper"}},[t._l(t.months,function(e,i){return n("div",{key:i,staticClass:"datepicker__month"},[n("h1",{staticClass:"datepicker__month-name",domProps:{textContent:t._s(t.getMonth(t.months[i].days[15].date))}}),n("div",{staticClass:"datepicker__week-row -hide-up-to-tablet"},t._l(t.i18n["day-names"],function(e){return n("div",{staticClass:"datepicker__week-name",domProps:{textContent:t._s(e)}})})),t._l(t.months[i].days,function(e,i){return n("div",{key:i,staticClass:"square",on:{mouseover:function(n){t.hoveringDate=e.date}}},[n("Day",{attrs:{options:t.$props,date:e.date,sortedDisabledDates:t.sortedDisabledDates,nextDisabledDate:t.nextDisabledDate,activeMonthIndex:t.activeMonthIndex,hoveringDate:t.hoveringDate,tooltipMessage:t.tooltipMessage,dayNumber:t.getDay(e.date),belongsToThisMonth:e.belongsToThisMonth,checkIn:t.checkIn,checkOut:t.checkOut},on:{dayClicked:function(e){t.handleDayClick(e)}}})],1)})],2)}),n("button",{staticClass:"next--mobile",attrs:{type:"button"},on:{click:t.renderNextMonth}})],2)]):t._e()])])]):t._e()},o=[];i._withStripped=!0;var a={render:i,staticRenderFns:o};e.a=a}])});
//# sourceMappingURL=vue-hotel-datepicker.min.js.map

/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3dlYnBhY2svYm9vdHN0cmFwIDAzMTExN2JmZDM0MjViNDMyNTA2Iiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcyIsIndlYnBhY2s6Ly8vQ29tRGVwYXJ0dXJlLnZ1ZSIsIndlYnBhY2s6Ly8vQ29tUmVzdWx0cy52dWUiLCJ3ZWJwYWNrOi8vL0NvbVByb2dyZXNzQmFyLnZ1ZSIsIndlYnBhY2s6Ly8vc3JjL3NlYXJjaC1mb3JtLmpzIiwid2VicGFjazovLy8od2VicGFjaykvYnVpbGRpbi9nbG9iYWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3YtY2FsZW5kYXIvbGliL3YtY2FsZW5kYXIubWluLmNzcz81MDU0Iiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy92LWNhbGVuZGFyL2xpYi92LWNhbGVuZGFyLm1pbi5jc3MiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlcy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvc3R5bGUtbG9hZGVyL2xpYi91cmxzLmpzIiwid2VicGFjazovLy8uL3NyYy9Db21EZXBhcnR1cmUudnVlIiwid2VicGFjazovLy8uL3NyYy9Db21EZXBhcnR1cmUudnVlPzEyZmMiLCJ3ZWJwYWNrOi8vLy4vc3JjL0NvbVJlc3VsdHMudnVlIiwid2VicGFjazovLy8uL3NyYy9Db21SZXN1bHRzLnZ1ZT9lNDhhIiwid2VicGFjazovLy8uL3NyYy9Db21Qcm9ncmVzc0Jhci52dWUiLCJ3ZWJwYWNrOi8vLy4vc3JjL0NvbVByb2dyZXNzQmFyLnZ1ZT80OWZkIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy92dWUtaG90ZWwtZGF0ZXBpY2tlci9kaXN0L3Z1ZS1ob3RlbC1kYXRlcGlja2VyLm1pbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCAwMzExMTdiZmQzNDI1YjQzMjUwNiIsIi8qIGdsb2JhbHMgX19WVUVfU1NSX0NPTlRFWFRfXyAqL1xuXG4vLyBJTVBPUlRBTlQ6IERvIE5PVCB1c2UgRVMyMDE1IGZlYXR1cmVzIGluIHRoaXMgZmlsZS5cbi8vIFRoaXMgbW9kdWxlIGlzIGEgcnVudGltZSB1dGlsaXR5IGZvciBjbGVhbmVyIGNvbXBvbmVudCBtb2R1bGUgb3V0cHV0IGFuZCB3aWxsXG4vLyBiZSBpbmNsdWRlZCBpbiB0aGUgZmluYWwgd2VicGFjayB1c2VyIGJ1bmRsZS5cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBmdW5jdGlvbmFsVGVtcGxhdGUsXG4gIGluamVjdFN0eWxlcyxcbiAgc2NvcGVJZCxcbiAgbW9kdWxlSWRlbnRpZmllciAvKiBzZXJ2ZXIgb25seSAqL1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICAgIG9wdGlvbnMuX2NvbXBpbGVkID0gdHJ1ZVxuICB9XG5cbiAgLy8gZnVuY3Rpb25hbCB0ZW1wbGF0ZVxuICBpZiAoZnVuY3Rpb25hbFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5mdW5jdGlvbmFsID0gdHJ1ZVxuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgdmFyIGhvb2tcbiAgaWYgKG1vZHVsZUlkZW50aWZpZXIpIHsgLy8gc2VydmVyIGJ1aWxkXG4gICAgaG9vayA9IGZ1bmN0aW9uIChjb250ZXh0KSB7XG4gICAgICAvLyAyLjMgaW5qZWN0aW9uXG4gICAgICBjb250ZXh0ID1cbiAgICAgICAgY29udGV4dCB8fCAvLyBjYWNoZWQgY2FsbFxuICAgICAgICAodGhpcy4kdm5vZGUgJiYgdGhpcy4kdm5vZGUuc3NyQ29udGV4dCkgfHwgLy8gc3RhdGVmdWxcbiAgICAgICAgKHRoaXMucGFyZW50ICYmIHRoaXMucGFyZW50LiR2bm9kZSAmJiB0aGlzLnBhcmVudC4kdm5vZGUuc3NyQ29udGV4dCkgLy8gZnVuY3Rpb25hbFxuICAgICAgLy8gMi4yIHdpdGggcnVuSW5OZXdDb250ZXh0OiB0cnVlXG4gICAgICBpZiAoIWNvbnRleHQgJiYgdHlwZW9mIF9fVlVFX1NTUl9DT05URVhUX18gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIGNvbnRleHQgPSBfX1ZVRV9TU1JfQ09OVEVYVF9fXG4gICAgICB9XG4gICAgICAvLyBpbmplY3QgY29tcG9uZW50IHN0eWxlc1xuICAgICAgaWYgKGluamVjdFN0eWxlcykge1xuICAgICAgICBpbmplY3RTdHlsZXMuY2FsbCh0aGlzLCBjb250ZXh0KVxuICAgICAgfVxuICAgICAgLy8gcmVnaXN0ZXIgY29tcG9uZW50IG1vZHVsZSBpZGVudGlmaWVyIGZvciBhc3luYyBjaHVuayBpbmZlcnJlbmNlXG4gICAgICBpZiAoY29udGV4dCAmJiBjb250ZXh0Ll9yZWdpc3RlcmVkQ29tcG9uZW50cykge1xuICAgICAgICBjb250ZXh0Ll9yZWdpc3RlcmVkQ29tcG9uZW50cy5hZGQobW9kdWxlSWRlbnRpZmllcilcbiAgICAgIH1cbiAgICB9XG4gICAgLy8gdXNlZCBieSBzc3IgaW4gY2FzZSBjb21wb25lbnQgaXMgY2FjaGVkIGFuZCBiZWZvcmVDcmVhdGVcbiAgICAvLyBuZXZlciBnZXRzIGNhbGxlZFxuICAgIG9wdGlvbnMuX3NzclJlZ2lzdGVyID0gaG9va1xuICB9IGVsc2UgaWYgKGluamVjdFN0eWxlcykge1xuICAgIGhvb2sgPSBpbmplY3RTdHlsZXNcbiAgfVxuXG4gIGlmIChob29rKSB7XG4gICAgdmFyIGZ1bmN0aW9uYWwgPSBvcHRpb25zLmZ1bmN0aW9uYWxcbiAgICB2YXIgZXhpc3RpbmcgPSBmdW5jdGlvbmFsXG4gICAgICA/IG9wdGlvbnMucmVuZGVyXG4gICAgICA6IG9wdGlvbnMuYmVmb3JlQ3JlYXRlXG5cbiAgICBpZiAoIWZ1bmN0aW9uYWwpIHtcbiAgICAgIC8vIGluamVjdCBjb21wb25lbnQgcmVnaXN0cmF0aW9uIGFzIGJlZm9yZUNyZWF0ZSBob29rXG4gICAgICBvcHRpb25zLmJlZm9yZUNyZWF0ZSA9IGV4aXN0aW5nXG4gICAgICAgID8gW10uY29uY2F0KGV4aXN0aW5nLCBob29rKVxuICAgICAgICA6IFtob29rXVxuICAgIH0gZWxzZSB7XG4gICAgICAvLyBmb3IgdGVtcGxhdGUtb25seSBob3QtcmVsb2FkIGJlY2F1c2UgaW4gdGhhdCBjYXNlIHRoZSByZW5kZXIgZm4gZG9lc24ndFxuICAgICAgLy8gZ28gdGhyb3VnaCB0aGUgbm9ybWFsaXplclxuICAgICAgb3B0aW9ucy5faW5qZWN0U3R5bGVzID0gaG9va1xuICAgICAgLy8gcmVnaXN0ZXIgZm9yIGZ1bmN0aW9hbCBjb21wb25lbnQgaW4gdnVlIGZpbGVcbiAgICAgIG9wdGlvbnMucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyV2l0aFN0eWxlSW5qZWN0aW9uIChoLCBjb250ZXh0KSB7XG4gICAgICAgIGhvb2suY2FsbChjb250ZXh0KVxuICAgICAgICByZXR1cm4gZXhpc3RpbmcoaCwgY29udGV4dClcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDBcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiPHRlbXBsYXRlPlxyXG5cdDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuXHRcdDxkaXYgY2xhc3M9XCJjb2wtbGctMyBjb2wtc20tNlwiPlxyXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiaDVcIj7Qn9C+0LjRgdC6INGC0YPRgNC+0LIg0LjQt1xyXG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJkcm9wZG93biBkLWlubGluZS1ibG9ja1wiPlxyXG5cdFx0XHRcdFx0PGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGNsYXNzPVwidGV4dC11cHBlcmNhc2UgZGlzcGxheS03IGRlcGFydHVyZV93cmFwXCIgaWQ9XCJkZXBhcnR1cmVWYWx1ZVwiIGRhdGEtdG9nZ2xlPVwiZHJvcGRvd25cIiBhcmlhLWhhc3BvcHVwPVwidHJ1ZVwiIGFyaWEtZXhwYW5kZWQ9XCJmYWxzZVwiPnt7Y2l0eUZyb21OYW1lfX08L2E+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZHJvcGRvd24tbWVudSBzZWxlY3QtZHJvcGRvd24gYmctbGlnaHRcIiBhcmlhLWxhYmVsbGVkYnk9XCJkZXBhcnR1cmVWYWx1ZVwiPlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwic2Nyb2xsYWJsZS1tZW51XCI+XHJcblx0XHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJkcm9wZG93bi1pdGVtXCIgOmNsYXNzPVwieydiZy1wcmltYXJ5IHRleHQtd2hpdGUnIDogZGVwYXJ0dXJlID09IDk5IH1cIiBAY2xpY2s9XCJzZXREZXBhcnR1cmUobm9uX2ZsaWdodClcIj7QkdC10Lcg0L/QtdGA0LXQu9C10YLQsDwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHQ8c3BhbiB2LWZvcj1cIihpdGVtLCBsZXR0ZXIpIGluIGdyb3VwZWREZXBhcnR1cmVzXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8aDYgY2xhc3M9XCJkcm9wZG93bi1oZWFkZXJcIj57eyBsZXR0ZXJ9fTwvaDY+XHJcblx0XHRcdFx0XHRcdFx0XHQ8c3BhbiBjbGFzcz1cImRyb3Bkb3duLWl0ZW1cIiA6Y2xhc3M9XCJ7J2JnLXByaW1hcnkgdGV4dC13aGl0ZScgOiBkZXBhcnR1cmVJdGVtLmlkID09IGRlcGFydHVyZSB9XCIgIHYtZm9yPVwiZGVwYXJ0dXJlSXRlbSBpbiBpdGVtXCIgQGNsaWNrPVwic2V0RGVwYXJ0dXJlKGRlcGFydHVyZUl0ZW0pXCIgdi1pZj1cImRlcGFydHVyZUl0ZW0uaWQgIT0gOTlcIj57e2RlcGFydHVyZUl0ZW0ubmFtZX19XHJcblx0XHRcdFx0XHRcdFx0XHQ8L3NwYW4+XHJcblx0XHRcdFx0XHRcdFx0PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHQ8L2Rpdj5cclxuXHRcdDwvZGl2PlxyXG5cdFx0PGRpdiBjbGFzcz1cImNvbC1sZy02IGNvbC1zbS02IHB0LTJcIj5cclxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tY2hlY2tcIj5cclxuXHRcdFx0XHQ8aW5wdXQgY2xhc3M9XCJmb3JtLWNoZWNrLWlucHV0XCIgdHlwZT1cImNoZWNrYm94XCIgdmFsdWU9XCJcIiBpZD1cImhpZGVSZWd1bGFyXCIgdi1tb2RlbD1cInJlZ3VsYXJcIiBAY2hhbmdlPVwic2V0UmVndWxhcigpXCIgLz5cclxuXHRcdFx0XHQ8bGFiZWwgY2xhc3M9XCJmb3JtLWNoZWNrLWxhYmVsIHBsLTFcIiBmb3I9XCJoaWRlUmVndWxhclwiPlxyXG5cdFx0XHRcdFx00KHQutGA0YvRgtGMINGA0LXQs9GD0LvRj9GA0L3Ri9C1INGA0LXQudGB0YtcclxuXHRcdFx0XHQ8L2xhYmVsPlxyXG5cdFx0XHQ8L2Rpdj5cclxuXHRcdDwvZGl2PlxyXG5cdDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuXHJcbjxzY3JpcHQ+XHJcbiAgICBleHBvcnQgZGVmYXVsdCB7XHJcbiAgICAgICAgbmFtZTogJ2NvbS1kZXBhcnR1cmUnLFxyXG4gICAgICAgIHByb3BzOiBbJ2RlcGFydHVyZXNEYXRhJywgJ2RlcGFydHVyZScsICdoaWRlUmVndWxhciddLFxyXG4gICAgICAgIGRhdGEoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICByZWd1bGFyOiB0aGlzLmhpZGVSZWd1bGFyLFxyXG4gICAgICAgICAgICAgICAgY2l0eUZyb21OYW1lOiBcItCj0YTRi1wiLFxyXG4gICAgICAgICAgICAgICAgbm9uX2ZsaWdodDoge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiAgICAgICA5OSxcclxuXHQgICAgICAgICAgICAgICAgbmFtZTogICAgIFwi0JHQtdC3INC/0LXRgNC10LvQtdGC0LBcIixcclxuXHQgICAgICAgICAgICAgICAgbmFtZWZyb206IFwi0JHQtdC3INC/0LXRgNC10LvQtdGC0LBcIlxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgICAgIHNldFJlZ3VsYXI6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuJHJvb3QuJGRhdGEuaGlkZVJlZ3VsYXIgPSB0aGlzLnJlZ3VsYXI7XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICAvL9Cj0YHRgtCw0L3QsNCy0LvQuNCy0LDQtdC8IElEINCz0L7RgNC+0LTQsFxyXG4gICAgICAgICAgICBzZXREZXBhcnR1cmU6IGZ1bmN0aW9uIChkZXBhcnR1cmVJdGVtKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNpdHlGcm9tTmFtZSA9IGRlcGFydHVyZUl0ZW0ubmFtZWZyb207XHJcbiAgICAgICAgICAgICAgICB0aGlzLiRyb290LiRkYXRhLmRlcGFydHVyZSA9IGRlcGFydHVyZUl0ZW0uaWQ7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjb21wdXRlZDoge1xyXG4gICAgICAgICAgICAvL9CT0YDRg9C/0L/QuNGA0YPQtdC8INCz0L7RgNC+0LTQsCDQv9C+INCx0YPQutCy0LDQvFxyXG4gICAgICAgICAgICBncm91cGVkRGVwYXJ0dXJlczogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIF8uZ3JvdXBCeSh0aGlzLmRlcGFydHVyZXNEYXRhLCBmdW5jdGlvbiAocykge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzLm5hbWUuY2hhckF0KCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICB9LFxyXG4gICAgICAgIHdhdGNoOiB7XHJcbiAgICAgICAgICAgIGRlcGFydHVyZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jaXR5RnJvbU5hbWUgPSBfLmZpbmQodGhpcy5kZXBhcnR1cmVzRGF0YSwgWydpZCcsIHRoaXMuZGVwYXJ0dXJlLnRvU3RyaW5nKCldKSA/IF8uZmluZCh0aGlzLmRlcGFydHVyZXNEYXRhLCBbJ2lkJywgdGhpcy5kZXBhcnR1cmUudG9TdHJpbmcoKV0pLm5hbWVmcm9tIDogdGhpcy5jaXR5RnJvbU5hbWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBDb21EZXBhcnR1cmUudnVlIiwiPHRlbXBsYXRlPlxyXG5cdDx1bCBjbGFzcz1cImxpc3QtdW5zdHlsZWRcIj5cclxuXHRcdDxkaXYgdi1pZj1cInJlc3VsdHNEYXRhLmxlbmd0aCA+IDBcIj5cclxuXHRcdFx0PGxpIHYtZm9yPVwiKGhvdGVsLCBpbmRleCkgaW4gZ3JvdXBwZWRSZXN1bHRzXCIgY2xhc3M9XCJhbmltYXRlZCBmYWRlSW5SaWdodCBtYi0yIHB0LTIgcGItMlwiPlxyXG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJjYXJkIGJnLWxpZ2h0XCIgdi1pZj1cImhvdGVsLnR5cGUgPT09ICdyZXN1bHQnXCI+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY2FyZC1ib2R5IGJnLXdoaXRlIHJvdyBuby1ndXR0ZXJzXCIgc3R5bGU9XCJib3gtc2hhZG93OiAwcHggMXB4IDNweCAxcHggI2JmYmZiZjsgei1pbmRleDogMTtcIj5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC0xMiB0ZXh0LXN1Y2Nlc3Mgc21hbGwgdGV4dC1tZC1yaWdodCBmb250LWl0YWxpYyBkLW5vbmUgZC14bC1ibG9ja1wiPjxpIGNsYXNzPVwiZmEgZmEtaW5mb1wiPjwvaT4g0LLRgdC1INGG0LXQvdGLINGBINGC0L7Qv9C70LjQstC90YvQvCDRgdCx0L7RgNC+0Lw8L2Rpdj5cclxuXHJcblx0XHRcdFx0XHRcdDwhLS3QpNCe0KLQniDQntCi0JXQm9CvLS0+XHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjb2wtYXV0byBjb2wtbWQtNiBjb2wteGwtYXV0byB0ZXh0LWNlbnRlciBtYi0zIG1iLW1kLTAgYWxpZ24tc2VsZi1jZW50ZXIgcHItM1wiPlxyXG5cdFx0XHRcdFx0XHRcdDxpbWcgOnNyYz1cImhvdGVsLnBpY3R1cmVsaW5rXCIgYWx0PVwiXCI+XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cclxuXHRcdFx0XHRcdFx0PCEtLdCd0JDQl9CS0JDQndCY0JUg0J7QotCV0JvQry0tPlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLTEyIGNvbC1tZC02IGNvbC14bC0zICBhbGlnbi1zZWxmLWNlbnRlciBwci0zXCI+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbCBwbC0wXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8aDQgY2xhc3M9XCJkaXNwbGF5LTggbWItMFwiPjxhIHRhcmdldD1cIl9ibGFua1wiIHYtYmluZDpocmVmPVwiJy9ob3RlbC8nK2hvdGVsLmhvdGVsY29kZSArICcvPycgKyBzZWFyY2hVcmxcIj57e2hvdGVsLmhvdGVsbmFtZX19PC9hPjwvaDQ+XHJcblx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbCBwbC0wXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8aSBjbGFzcz1cImZhIGZhLXN0YXIgdGV4dC13YXJuaW5nXCIgdi1mb3I9XCJuIGluIHBhcnNlSW50KGhvdGVsLmhvdGVsc3RhcnMpXCI+PC9pPlxyXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjb2wgcGwtMFwiIHYtaWY9XCJwYXJzZUludChob3RlbC5ob3RlbHJhdGluZykgPiAwXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8c3BhbiA6Y2xhc3M9XCJ7J3RleHQtc3VjY2VzcycgOiBob3RlbC5ob3RlbHJhdGluZyA+PSA0LCAndGV4dC13YXJuaW5nJzogaG90ZWwuaG90ZWxyYXRpbmcgPj0gMyAmJiBob3RlbC5ob3RlbHJhdGluZyA8IDQsICd0ZXh0LWRhbmdlcicgOiBob3RlbC5ob3RlbHJhdGluZyA8IDN9XCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxpIGNsYXNzPVwiZmFcIiA6Y2xhc3M9XCJ7J2ZhLXRodW1icy11cCcgOiBob3RlbC5ob3RlbHJhdGluZyA+PSA0LCAnZmEtdGh1bWJzLW8tdXAnOiBob3RlbC5ob3RlbHJhdGluZyA+PSAzICYmIGhvdGVsLmhvdGVscmF0aW5nIDwgNCwgJ2ZhLXRodW1icy1kb3duJyA6IGhvdGVsLmhvdGVscmF0aW5nIDwgM31cIj48L2k+XHJcblx0XHRcdFx0XHRcdFx0XHRcdHt7aG90ZWwuaG90ZWxyYXRpbmd9fTwvc3Bhbj4vNVxyXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjb2wgcGwtMFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0PHNtYWxsIGNsYXNzPVwidGV4dC1tdXRlZFwiPnt7aG90ZWwucmVnaW9ubmFtZX19LCB7e2hvdGVsLmNvdW50cnluYW1lfX08L3NtYWxsPlxyXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHJcblx0XHRcdFx0XHRcdDwhLS3QktCr0JvQldCi0KssINCd0J7Qp9CYLCDQotCj0KDQmNCh0KLQqy0tPlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLTEyIGNvbC1tZC02IGNvbC14bC0zIGFsaWduLXNlbGYtc3RhcnQgYWxpZ24tc2VsZi1tZC1lbmQgYWxpZ24tc2VsZi14bC1jZW50ZXIgcHItM1wiPlxyXG5cdFx0XHRcdFx0XHRcdDxkbD5cclxuXHRcdFx0XHRcdFx0XHRcdDxkdD7QktGL0LvQtdGC0Ysge3tob3RlbC5taW5GbHlkYXRlfX0mbmJzcDs8c3BhbiB2LWlmPVwiaG90ZWwubWluRmx5ZGF0ZSAhPT1ob3RlbC5tYXhGbHlkYXRlXCI+IOKAlCZuYnNwO3t7aG90ZWwubWF4Rmx5ZGF0ZX19PC9zcGFuPjwvZHQ+XHJcblx0XHRcdFx0XHRcdFx0XHQ8ZGQ+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxzcGFuIHYtaWY9XCJob3RlbC5taW5OaWdodCA9PSBob3RlbC5tYXhOaWdodFwiPtC90LAge3tob3RlbC5taW5OaWdodCB8IGRpc3BsYXlOaWdodHN9fTwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHNwYW4gdi1pZj1cImhvdGVsLm1pbk5pZ2h0ICE9IGhvdGVsLm1heE5pZ2h0XCI+0L3QsCB7e2hvdGVsLm1pbk5pZ2h0fX08L3NwYW4+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxzcGFuIHYtaWY9XCJob3RlbC5taW5OaWdodCAhPT0gaG90ZWwubWF4TmlnaHRcIj4mbmJzcDvigJQmbmJzcDsge3tob3RlbC5tYXhOaWdodCB8IGRpc3BsYXlOaWdodHN9fTwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGJyPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8c3Bhbj57e2hvdGVsLnRvdXJzLnRvdXJbMF0uYWR1bHRzfX0g0LLQt9GALiA8c3BhbiB2LWlmPVwicGFyc2VJbnQoaG90ZWwudG91cnMudG91clswXS5jaGlsZCkgPiAwXCI+KyB7e2hvdGVsLnRvdXJzLnRvdXJbMF0uY2hpbGR9fSDRgNC10LEuPC9zcGFuPiAgPC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdFx0PC9kZD5cclxuXHRcdFx0XHRcdFx0XHQ8L2RsPlxyXG5cdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHJcblx0XHRcdFx0XHRcdDwhLS3QmtCg0JDQotCa0J7QlSDQntCf0JjQodCQ0J3QmNCVINCe0KLQldCb0K8tLT5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImQtbm9uZSBkLXhsLWJsb2NrIGNvbC14bC0yIGFsaWduLXNlbGYtc3RhcnQgYWxpZ24tc2VsZi1tZC1lbmQgYWxpZ24tc2VsZi14bC1jZW50ZXIgcHItMyBob3RlbC1taW5pLWRlc2NyaXB0aW9uXCIgZGF0YS1wbGFjZW1lbnQ9XCJib3R0b21cIiBkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIiB0aXRsZT1cItCe0L/QuNGB0LDQvdC40LVcIlxyXG5cdFx0XHRcdFx0XHQgICAgIDpkYXRhLWNvbnRlbnQ9XCJob3RlbC5ob3RlbGRlc2NyaXB0aW9uXCI+XHJcblx0XHRcdFx0XHRcdFx0e3tob3RlbC5ob3RlbGRlc2NyaXB0aW9uIHwgc2hvcnRUZXh0fX1cclxuXHRcdFx0XHRcdFx0PC9kaXY+XHJcblxyXG5cdFx0XHRcdFx0XHQ8IS0t0KbQldCd0JAtLT5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC0xMiBjb2wtbWQtNiBjb2wteGwtYXV0byB0ZXh0LXJpZ2h0IGFsaWduLXNlbGYtc3RhcnQgbWwtbWQtYXV0b1wiPlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJ0ZXh0LXN1Y2Nlc3Mgc21hbGwgZm9udC1pdGFsaWMgZC1ibG9jayBkLXhsLW5vbmVcIj48aSBjbGFzcz1cImZhIGZhLWluZm9cIj48L2k+INCy0YHQtSDRhtC10L3RiyDRgSDRgtC+0L/Qu9C40LLQvdGL0Lwg0YHQsdC+0YDQvtC8PC9kaXY+XHJcblxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJ0ZXh0LW11dGVkIGg1IG1iLTBcIj5cclxuXHRcdFx0XHRcdFx0XHRcdDxzbWFsbD7QsdC10Lcg0YHQutC40LTQutC4PC9zbWFsbD5cclxuXHRcdFx0XHRcdFx0XHRcdDxzPnt7aG90ZWwucHJpY2UgfCBnZXRPbGRQcmljZX19PC9zPlxyXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJoNVwiPnt7aG90ZWwucHJpY2UgfCBnZXRDcmVkaXRQcmljZX19PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdFx0PHNtYWxsPtCyINC60YDQtdC00LjRgiA2INC80LXRgS48L3NtYWxsPlxyXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwidGV4dC1wcmltYXJ5IGg1XCIgQGNsaWNrPVwib3BlblRvdXJzTGlzdChob3RlbC5ob3RlbGNvZGUpXCI+PGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiPjxzcGFuPnt7aG90ZWwucHJpY2UgfCBmb3JtYXRQcmljZX19PC9zcGFuPiDQt9CwINGC0YPRgCA8L2E+PC9kaXY+XHJcblxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJtdC0yXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApO1wiIGNsYXNzPVwibGQtZXh0LWxlZnQgYnRuIGJ0bi1pbmZvIGJ0bi1ibG9ja1wiIEBjbGljaz1cIm9wZW5Ib3RlbERlc2NyKGhvdGVsLmhvdGVsY29kZSwgdGhpcylcIiA6aWQ9XCInaG90ZWxCdG4tJyArIGhvdGVsLmhvdGVsY29kZVwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8c3Bhbj7QntCxINC+0YLQtdC70LU8L3NwYW4+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJsZCBsZC1yaW5nIGxkLXNwaW5cIj48L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdDwvYT5cclxuXHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibXQtMlwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0PGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKTtcIiBjbGFzcz1cImJ0biBidG4tb3JhbmdlIGJ0bi1ibG9ja1wiIEBjbGljaz1cIm9wZW5Ub3Vyc0xpc3QoaG90ZWwuaG90ZWxjb2RlKVwiPtCS0YHQtSDQstCw0YDQuNCw0L3RgtGLIDxpIGNsYXNzPVwiZmEgZmEtY2hldnJvbi1kb3duXCI+PC9pPjwvYT5cclxuXHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHJcblx0XHRcdFx0XHQ8IS0t0J7Qn9CY0KHQkNCd0JjQlSDQntCi0JXQm9CvLS0+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwicC0zIGhvdGVsLWRlc2NyaXB0aW9uXCIgc3R5bGU9XCJkaXNwbGF5OiBub25lO1wiIHYtYmluZDppZD1cIidkZXNjci0nICsgaG90ZWwuaG90ZWxjb2RlXCIgdi1pZj1cIm9iSG90ZWxzRGVzY3JbaG90ZWwuaG90ZWxjb2RlXSAhPT0gdW5kZWZpbmVkXCIgdi1zaG93PVwib2JIb3RlbHNEZXNjcltob3RlbC5ob3RlbGNvZGVdICE9PSB1bmRlZmluZWRcIj5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cInJvdyBob3RlbC1waG90b3MgZC1mbGV4IGp1c3RpZnktY29udGVudC1iZXR3ZWVuXCIgOmlkPVwiJ2hvdGVsUGhvdG9zXycgKyBob3RlbC5ob3RlbGNvZGVcIiB2LWlmPVwicGFyc2VJbnQob2JIb3RlbHNEZXNjcltob3RlbC5ob3RlbGNvZGVdLmltYWdlc2NvdW50KSA+IDBcIj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiXCIgdi1mb3I9XCJpbWFnZSBpbiBvYkhvdGVsc0Rlc2NyW2hvdGVsLmhvdGVsY29kZV0uaW1hZ2VzLmltYWdlXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8YSA6aHJlZj1cImltYWdlXCIgOmRhdGEtZmFuY3lib3g9XCInZ2FsbGVyeTEnK2hvdGVsLmhvdGVsY29kZVwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8aW1nIGNsYXNzPVwiaW1nLXRodW1ibmFpbFwiIDpzcmM9XCJpbWFnZVwiIGFsdD1cIlwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0PC9hPlxyXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cInJvd1wiPlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjb2wtMTJcIj5cclxuXHRcdFx0XHRcdFx0XHRcdDxwIHYtc2hvdz1cIm9iSG90ZWxzRGVzY3JbaG90ZWwuaG90ZWxjb2RlXS5kZXNjcmlwdGlvblwiPnt7b2JIb3RlbHNEZXNjcltob3RlbC5ob3RlbGNvZGVdLmRlc2NyaXB0aW9ufX08L3A+XHJcblx0XHRcdFx0XHRcdFx0XHQ8cCB2LXNob3c9XCIhb2JIb3RlbHNEZXNjcltob3RlbC5ob3RlbGNvZGVdLmRlc2NyaXB0aW9uXCI+0JjQt9Cy0LjQvdC40YLQtSwg0L7Qv9C40YHQsNC90LjQtSDQvtGC0LXQu9GPINC90LXQtNC+0YHRgtGD0L/QvdC+PC9wPlxyXG5cdFx0XHRcdFx0XHRcdFx0PHVsIGNsYXNzPVwibGlzdC11bnN0eWxlZFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8bGkgdi1zaG93PVwib2JIb3RlbHNEZXNjcltob3RlbC5ob3RlbGNvZGVdLmlucm9vbVwiPjxzdHJvbmc+0JIg0LrQvtC80L3QsNGC0LU6PC9zdHJvbmc+IHt7b2JIb3RlbHNEZXNjcltob3RlbC5ob3RlbGNvZGVdLmlucm9vbX19PC9saT5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGxpIHYtc2hvdz1cIm9iSG90ZWxzRGVzY3JbaG90ZWwuaG90ZWxjb2RlXS50ZXJyaXRvcnlcIj48c3Ryb25nPtCi0LXRgNGA0LjRgtC+0YDQuNGPOjwvc3Ryb25nPiB7e29iSG90ZWxzRGVzY3JbaG90ZWwuaG90ZWxjb2RlXS50ZXJyaXRvcnl9fTwvbGk+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxsaSB2LXNob3c9XCJvYkhvdGVsc0Rlc2NyW2hvdGVsLmhvdGVsY29kZV0uYmVhY2hcIj48c3Ryb25nPtCf0LvRj9C2Ojwvc3Ryb25nPiB7e29iSG90ZWxzRGVzY3JbaG90ZWwuaG90ZWxjb2RlXS5iZWFjaH19PC9saT5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGxpIHYtc2hvdz1cIm9iSG90ZWxzRGVzY3JbaG90ZWwuaG90ZWxjb2RlXS5wbGFjZW1lbnRcIj48c3Ryb25nPtCg0LDRgdC/0L7Qu9C+0LbQtdC90LjQtTo8L3N0cm9uZz4ge3tvYkhvdGVsc0Rlc2NyW2hvdGVsLmhvdGVsY29kZV0ucGxhY2VtZW50fX08L2xpPlxyXG5cdFx0XHRcdFx0XHRcdFx0PC91bD5cclxuXHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJyb3cgZC1mbGV4IHJvdyBiZy1pbmZvIHB0LTIgcGItMiB0ZXh0LXdoaXRlIGFsaWduLWl0ZW1zLWNlbnRlciBtdC0zXCI+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC0xMiBjb2wtbGctOSBkaXNwbGF5LTggbWItMiBtYi1sZy0wXCI+0JXRgdGC0Ywg0LLQvtC/0YDQvtGB0Ysg0L/QviDQvtGC0LXQu9GOPyDQl9Cw0LTQsNC5INC40YUg0L3QsNGI0LXQvNGDINGN0LrRgdC/0LXRgNGC0YM8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLTEyIGNvbC1sZy0zXCI+PGEgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgYnRuLWJsb2NrXCIgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIDpkYXRhLWhvdGVsLWlkPVwiaG90ZWwuaG90ZWxjb2RlXCIgZGF0YS10b2dnbGU9XCJtb2RhbFwiIGRhdGEtdGFyZ2V0PVwiI2V4cGVydFF1ZXN0aW9uTW9kYWxcIj7Ql9Cw0LTQsNGC0Ywg0LLQvtC/0YDQvtGBPC9hPjwvZGl2PlxyXG5cdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0PGJyPlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IDppZD1cIidtYXBpbnRhYl8nK2hvdGVsLmhvdGVsY29kZVwiIHN0eWxlPVwiaGVpZ2h0OiAyMDBweDtcIj48L2Rpdj5cclxuXHRcdFx0XHRcdDwvZGl2PlxyXG5cclxuXHRcdFx0XHRcdDwhLS3QodCf0JjQodCe0Jog0KLQo9Cg0J7QkiDQkiDQntCi0JXQm9CsLS0+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwicC0zIHRvdXJzLWxpc3QgZC1ub25lIGJnLWRhcmtlclwiIHYtYmluZDppZD1cIid0b3Vycy0nICsgaG90ZWwuaG90ZWxjb2RlXCI+XHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJkLWJsb2NrXCI+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cInJvdyBkLWZsZXggYWxpZ24taXRlbXMtY2VudGVyIGJnLWRhcmtlclwiIHYtZm9yPVwidG91ciBpbiBob3RlbC50b3Vycy50b3VyXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLTEyIHRleHQtbXV0ZWQgbWItM1wiPnt7dG91ci50b3VybmFtZX19PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLTYgY29sLW1kLTJcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHA+PHN0cm9uZz57e3RvdXIuZmx5ZGF0ZX19PC9zdHJvbmc+PC9wPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8cD57e3RvdXIubmlnaHRzfX0g0L3QvtGH0LXQuTwvcD5cclxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC02IGNvbC1tZC0zXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxwPjxzdHJvbmc+e3t0b3VyLm1lYWx9fTwvc3Ryb25nPjwvcD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHAgOnRpdGxlPVwiaG90ZWwubWVhbHJ1c3NpYW5cIj57e3RvdXIubWVhbHJ1c3NpYW59fTwvcD5cclxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC02IGNvbC1tZC0yXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxwPjxzdHJvbmc+e3t0b3VyLnJvb219fTwvc3Ryb25nPjwvcD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHAgOnRpdGxlPVwiaG90ZWwucGxhY2VtZW50XCI+e3t0b3VyLnBsYWNlbWVudH19PC9wPlxyXG5cdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLTYgY29sLW1kLTJcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHA+PHN0cm9uZz57e3RvdXIub3BlcmF0b3JuYW1lfX08L3N0cm9uZz48L3A+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxwPjxpbWcgdi1iaW5kOnNyYz1cIidodHRwczovL3RvdXJ2aXNvci5ydS9waWNzL29wZXJhdG9ycy9zZWFyY2hsb2dvLycrdG91ci5vcGVyYXRvcmNvZGUrJy5naWYnXCIgYWx0PVwiXCI+PC9wPlxyXG5cdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLTEyIGNvbC1tZC0zXCI+XHJcblxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8IS0tINCm0JXQndCrIC0tPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwidGV4dC1zdWNjZXNzIHRleHQtbGVmdCB0ZXh0LW1kLXJpZ2h0IG1iLTBcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHTQt9CwINGC0YPRgFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwicHJpY2UgaDNcIiBAY2xpY2sucHJldmVudD1cImdldERldGFpbCh0b3VyLnRvdXJpZClcIj57e3RvdXIucHJpY2UgfCBmb3JtYXRQcmljZX19PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cInRleHQtbXV0ZWQgdGV4dC1sZWZ0IHRleHQtbWQtcmlnaHRcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHTQsdC10Lcg0YHQutC40LTQutC4OiA8cyBjbGFzcz1cImg1XCI+e3t0b3VyLnByaWNlIHwgZ2V0T2xkUHJpY2V9fTwvcz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJ0ZXh0LWRhbmdlciB0ZXh0LWxlZnQgdGV4dC1tZC1yaWdodCBwcmljZVwiICBkYXRhLXRvZ2dsZT1cIm1vZGFsXCIgOmRhdGEtZmFzdC10b3VyLWlkPVwidG91ci50b3VyaWRcIiBkYXRhLWluZm89XCJjcmVkaXRcIiBkYXRhLXRhcmdldD1cIiNmYXN0T3JkZXJNb2RhbFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdNCyINC60YDQtdC00LjRgiA2INC80LXRgS46IDxzcGFuIGNsYXNzPVwiaDVcIj57e3RvdXIucHJpY2UgfCBnZXRDcmVkaXRQcmljZX19PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxwPjxzbWFsbD4o0YHRgtC+0LjQvNC+0YHRgtGMINCx0LXQtyDQv9C10YDQstC+0L3QsNGH0LDQu9GM0L3QvtCz0L4g0LLQt9C90L7RgdCwKTwvc21hbGw+PC9wPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cInRleHQtaW5mbyBzbWFsbCB0ZXh0LWxlZnQgdGV4dC1tZC1yaWdodCBtYi0yXCIgdi1pZj1cIiFoaWRlUmVndWxhclwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxpPtGG0LXQvdCwINC80L7QttC10YIg0LHRi9GC0Ywg0L3QtSDQvtC60L7QvdGH0LDRgtC10LvRjNC90L7QuSwg0L3QtdC+0LHRhdC+0LTQuNC80L4g0L7QsdGA0LDRgtC40YLRjNGB0Y8g0LIg0L7RhNC40YE8L2k+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cclxuXHJcblx0XHRcdFx0XHRcdFx0XHRcdDwhLS0g0JrQndCe0J/QmtCYIC0tPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8YSBocmVmPVwiI1wiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IGJ0bi1ibG9jayBidG4tc20gbGQtZXh0LWxlZnRcIiA6Y2xhc3M9XCInZGV0YWlsLWJ0bl8nK3RvdXIudG91cmlkXCIgQGNsaWNrLnByZXZlbnQ9XCJnZXREZXRhaWwodG91ci50b3VyaWQpXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx00KLRg9GAINC/0L7QtNGA0L7QsdC90L5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibGQgbGQtcmluZyBsZC1zcGluXCI+PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvYT5cclxuXHJcblx0XHRcdFx0XHRcdFx0XHRcdDwhLS0g0J7QotCa0JvQrtCn0JDQldCcINCU0J7QkdCQ0JLQm9CV0J3QmNCVINCSINCa0J7QoNCX0JjQndCjIC0tPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8IS0tIGEgY2xhc3M9XCJidG4gYnRuLW91dGxpbmUtcHJpbWFyeSBidG4tYmxvY2sgYnRuLXNtXCIgaHJlZj1cIiNcIiBAY2xpY2sucHJldmVudD1cImFkZFRvdXIyQmFza2V0KHRvdXIudG91cmlkLCAkZXZlbnQpXCI+0JIg0LrQvtGA0LfQuNC90YM8L2EtLT5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGEgY2xhc3M9XCJidG4gYnRuLW91dGxpbmUtb3JhbmdlIGJ0bi1zbSBidG4tYmxvY2tcIiBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgZGF0YS10b2dnbGU9XCJtb2RhbFwiIDpkYXRhLWZhc3QtdG91ci1pZD1cInRvdXIudG91cmlkXCIgZGF0YS10YXJnZXQ9XCIjZmFzdE9yZGVyTW9kYWxcIj7Ql9Cw0LrQsNC30LDRgtGMINCyIDEg0LrQu9C40Lo8L2E+XHJcblxyXG5cdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLTEyXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxocj5cclxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJ0ZXh0LWNlbnRlciB0ZXh0LWluZm9cIiBAY2xpY2s9XCJvcGVuVG91cnNMaXN0KGhvdGVsLmhvdGVsY29kZSlcIj48YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCI+PGkgY2xhc3M9XCJmYSBmYS1jaGV2cm9uLXVwXCI+PC9pPiDRgdCy0LXRgNC90YPRgtGMPC9hPjwvZGl2PlxyXG5cdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdDwvZGl2PlxyXG5cclxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY2FyZCBiZy1kYXJrXCIgdi1pZj1cImhvdGVsLnR5cGUgPT09ICdiYW5uZXInXCI+e3tob3RlbC5iYW5uZXIubmFtZX19PC9kaXY+XHJcblx0XHRcdDwvbGk+XHJcblx0XHQ8L2Rpdj5cclxuXHRcdDxkaXYgdi1lbHNlPlxyXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLTEyIGRpc3BsYXktNiBtdC01IG1iLTUgdGV4dC1jZW50ZXIgdGV4dC1zdWNjZXNzXCIgdi1pZj1cIiF0b3Vyc05vdEZvdW5kXCI+XHJcblx0XHRcdFx00J3QsNC20LzQuNGC0LUg0LrQvdC+0L/QutGDIFwi0JjRgdC60LDRgtGMXCIsINC00LvRjyDQvdCw0YfQsNC70LAg0YDQsNCx0L7RgtGLINC/0L7QuNGB0LrQsFxyXG5cdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PGRpdiBjbGFzcz1cImNvbC0xMiBkaXNwbGF5LTcgbXQtNSBtYi01IHRleHQtY2VudGVyIHRleHQtZGFuZ2VyXCIgdi1pZj1cInRvdXJzTm90Rm91bmRcIj5cclxuXHRcdFx0XHTQo9C/0YEhINCY0LfQstC40L3QuNGC0LUsINC90L4g0L/QviDQt9Cw0LTQsNC90L3Ri9C8INC/0LDRgNCw0LzQtdGC0YDQsNC8INGC0YPRgNC+0LIg0L3QtSDQvdCw0LnQtNC10L3Qvi4g0J/QvtC/0YDQvtCx0YPQudGC0LUg0LjQt9C80LXQvdC40YLRjCDQv9Cw0YDQvNC10YLRgNGLINC/0L7QuNGB0LrQsCDQuCDRgdC90L7QstCwINC90LDQttCw0YLRjCDQutC90L7Qv9C60YMgXCLQndCw0LnRgtC4XCIgPGJyPlxyXG5cdFx0XHRcdNCb0LjQsdC+INC+0YLRgdGC0LDQstGM0YLQtSDQt9Cw0Y/QstC60YMg0L3QsCDQv9C+0LTQsdC+0YAg0YLRg9GA0LAg0LTQu9GPINC90LDRiNC40YUg0LzQtdC90LXQtNC20LXRgNC+0LIuXHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0PC9kaXY+XHJcblx0PC91bD5cclxuPC90ZW1wbGF0ZT5cclxuXHJcblxyXG48c2NyaXB0PlxyXG4gICAgZXhwb3J0IGRlZmF1bHQge1xyXG4gICAgICAgIG5hbWU6ICdjb20tcmVzdWx0cycsXHJcbiAgICAgICAgcHJvcHM6IFsncmVzdWx0c0RhdGEnLCAnc2VhcmNoVXJsJywgJ3RvdXJzTm90Rm91bmQnLCAnaGlkZVJlZ3VsYXInXSxcclxuICAgICAgICBkYXRhKCkge1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgb2JIb3RlbHNEZXNjcjoge30sXHJcbiAgICAgICAgICAgICAgICBvYkJhbm5lcnM6IHtcclxuLy8gICAgICAgICAgICAgICAgICAgIDA6IHtcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcItCR0LDQvdC90LXRgCAxXCIsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgICAgaW1nOiBcIlwiLFxyXG4vLyAgICAgICAgICAgICAgICAgICAgfSxcclxuLy8gICAgICAgICAgICAgICAgICAgIDE6IHtcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcItCR0LDQvdC90LXRgCAyXCIsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgICAgaW1nOiBcIlwiLFxyXG4vLyAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIG1ldGhvZHM6IHtcclxuICAgICAgICAgICAgLy/QntGC0LrRgNGL0LLQsNC10YIg0Lgg0LfQsNC60YDRi9Cy0LDQtdGCINGB0L/QuNC+0Log0YLRg9GA0L7QslxyXG4gICAgICAgICAgICBvcGVuVG91cnNMaXN0OiBmdW5jdGlvbiAoaG90ZWxJZCkge1xyXG4gICAgICAgICAgICAgICAgdmFyIHRvdXJMaXN0ID0gJCgnI3RvdXJzLScgKyBob3RlbElkKTtcclxuXHJcbiAgICAgICAgICAgICAgICAkKCcuaG90ZWwtZGVzY3JpcHRpb24nKS5zbGlkZVVwKCk7XHJcblxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChkb2N1bWVudC5ib2R5LmNsaWVudFdpZHRoIDw9IDcwMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICQoXCJodG1sLCBib2R5XCIpLmFuaW1hdGUoe3Njcm9sbFRvcDogJCgnI3RvdXJzLScgKyBob3RlbElkKS5wYXJlbnQoKS5vZmZzZXQoKS50b3AgKyAzMDB9LCAxMDApO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAkKFwiaHRtbCwgYm9keVwiKS5hbmltYXRlKHtzY3JvbGxUb3A6ICQoJyN0b3Vycy0nICsgaG90ZWxJZCkucGFyZW50KCkub2Zmc2V0KCkudG9wIC0gNTB9LCAxMDApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmICh0b3VyTGlzdC5oYXNDbGFzcygnZC1ub25lJykpIHtcclxuICAgICAgICAgICAgICAgICAgICB0b3VyTGlzdC5yZW1vdmVDbGFzcygnZC1ub25lJyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRvdXJMaXN0LmFkZENsYXNzKCdkLW5vbmUnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvL3RvdXJMaXN0LnNsaWRlVG9nZ2xlKCk7XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBvcGVuSG90ZWxEZXNjcjogZnVuY3Rpb24gKGhvdGVsSWQpIHtcclxuICAgICAgICAgICAgICAgIHZhciBkZXNjckJsb2NrID0gJCgnI2Rlc2NyLScgKyBob3RlbElkKSxcclxuICAgICAgICAgICAgICAgICAgICBob3RlbEJ0biA9ICQoJyNob3RlbEJ0bi0nICsgaG90ZWxJZCk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCFfLmhhcyh0aGlzLm9iSG90ZWxzRGVzY3IsIGhvdGVsSWQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJChob3RlbEJ0bikuYWRkQ2xhc3MoJ3J1bm5pbmcnKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kaHR0cC5nZXQoJy9sb2NhbC9tb2R1bGVzL25ld3RyYXZlbC5zZWFyY2gvbGliL2FqYXgucGhwJywge3BhcmFtczoge3R5cGU6IFwiZnVsbGhvdGVsXCIsIGhvdGVsY29kZTogaG90ZWxJZH19KS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuYm9keS5kYXRhLmhvdGVsICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlYnVnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT0g0JjQvdGE0L7RgNC80LDRhtC40Y8g0L/QviDQvtGC0LXQu9GOID09PT09PT09Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRzZXQodGhpcy5vYkhvdGVsc0Rlc2NyLCBob3RlbElkLCByZXNwb25zZS5ib2R5LmRhdGEuaG90ZWwpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHltYXBzLnJlYWR5KGluaXQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uIGluaXQoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vINCh0L7Qt9C00LDQvdC40LUg0LrQsNGA0YLRiy5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG15TWFwID0gbmV3IHltYXBzLk1hcChcIm1hcGludGFiX1wiICsgaG90ZWxJZCwge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2VudGVyOiBbcmVzcG9uc2UuYm9keS5kYXRhLmhvdGVsLmNvb3JkMSwgcmVzcG9uc2UuYm9keS5kYXRhLmhvdGVsLmNvb3JkMl0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB6b29tOiAxNlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG15UGxhY2VtYXJrID0gbmV3IHltYXBzLlBsYWNlbWFyayhbcmVzcG9uc2UuYm9keS5kYXRhLmhvdGVsLmNvb3JkMSwgcmVzcG9uc2UuYm9keS5kYXRhLmhvdGVsLmNvb3JkMl0sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpbnRDb250ZW50OiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhbGxvb25Db250ZW50OiAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpY29uSW1hZ2VIcmVmOiAnL2xvY2FsL3RlbXBsYXRlcy9uZXd0cmF2ZWwvaW1hZ2VzL21hcHBpbi5wbmcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbXlNYXAuZ2VvT2JqZWN0cy5hZGQobXlQbGFjZW1hcmspO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcblxyXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG1hcGludGFiID0gbmV3IEdNYXBzKHtcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbDogJyNtYXBpbnRhYl8nICsgaG90ZWxJZCxcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDMyMixcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwLFxyXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjcm9sbHdoZWVsOiBmYWxzZSxcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYXQ6IHJlc3BvbnNlLmJvZHkuZGF0YS5ob3RlbC5jb29yZDEsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbG5nOiByZXNwb25zZS5ib2R5LmRhdGEuaG90ZWwuY29vcmQyXHJcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuLy9cclxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hcGludGFiLmFkZE1hcmtlcih7XHJcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF0OiByZXNwb25zZS5ib2R5LmRhdGEuaG90ZWwuY29vcmQxLFxyXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxuZzogcmVzcG9uc2UuYm9keS5kYXRhLmhvdGVsLmNvb3JkMixcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGlja2FibGU6IGZhbHNlLFxyXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGljb246IFwiL2xvY2FsL3RlbXBsYXRlcy9uZXd0cmF2ZWwvaW1hZ2VzL21hcHBpbi5wbmdcIlxyXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIuaG90ZWwtcGhvdG9zIGFcIikuZmFuY3lib3goKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNob3RlbFBob3Rvc19cIiArIGhvdGVsSWQpLnNsaWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9hdXRvcGxheTogdHJ1ZSxcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFycm93czogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9wbGF5U3BlZWQ6IDMwMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogNixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNpdmU6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVha3BvaW50OiAxMDI0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogNCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGluZmluaXRlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb3RzOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDYwMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb3RzOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvdHM6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFycm93czogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWtwb2ludDogNDgwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDFcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgNTAwKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlYnVnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCLQntGI0LjQsdC60LAgZnVsbGhvdGVsXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoaG90ZWxCdG4pLnJlbW92ZUNsYXNzKCdydW5uaW5nJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBkZXNjckJsb2NrLnNsaWRlVG9nZ2xlKCk7XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICAvL9CU0LXRgtCw0LvRjNC90LDRjyDQsNC60YLRg9Cw0LvQuNC30LDRhtC40Y8sINCy0LfRi9Cy0LDQtdGCINC80L7QtNCw0LvRjNC90L7QtSDQvtC60L3QvlxyXG4gICAgICAgICAgICBnZXREZXRhaWw6IGZ1bmN0aW9uICh0b3VyaWQpIHtcclxuICAgICAgICAgICAgICAgIHZhciBkZXRhaWxQYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdG91cmlkOiB0b3VyaWQsXHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgICQoJy5kZXRhaWwtYnRuXycgKyB0b3VyaWQpLmFkZENsYXNzKCdydW5uaW5nJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgdmFyIG15TW9kYWwgPSBuZXcgakJveCgnTW9kYWwnLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgb25DbG9zZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcuakJveC1Nb2RhbCcpLnJlbW92ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyQoJy5qQm94LW92ZXJsYXknKS5yZW1vdmUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJCgnLmRldGFpbC1idG5fJyArIHRvdXJpZCkucmVtb3ZlQ2xhc3MoJ3J1bm5pbmcnKTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgbXlNb2RhbC5vcGVuKHtcclxuICAgICAgICAgICAgICAgICAgICByZXNwb25zaXZlV2lkdGg6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2l2ZUhlaWdodDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogXCI5NTBcIixcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IFwiOTAwXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgY2xvc2VCdXR0b246IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgYmxvY2tTY3JvbGw6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgYWpheDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1cmw6ICcvbG9jYWwvY29tcG9uZW50cy9uZXd0cmF2ZWwuc2VhcmNoL2Z1bGwuc2VhcmNoLnYyL3RlbXBsYXRlcy8uZGVmYXVsdC9kZXRhaWwucGhwJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogZGV0YWlsUGFyYW1zLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWxvYWQ6ICdzdHJpY3QnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzcGlubmVyOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIC8v0JTQvtCx0LDQstC70LXQvdC40LUg0YLRg9GA0LAg0LIg0LrQvtGA0LfQuNC90YNcclxuICAgICAgICAgICAgYWRkVG91cjJCYXNrZXQ6IGZ1bmN0aW9uICh0b3VyaWQsIGV2ZW50KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLiRodHRwLmdldCgnL2xvY2FsL21vZHVsZXMvbmV3dHJhdmVsLnNlYXJjaC9saWIvYWpheC5waHAnLCB7cGFyYW1zOiB7dHlwZTogXCJhZGRUb1RvdXJzQmFza2V0XCIsIHRvdXJpZDogdG91cmlkfX0pLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmJvZHkgPT09ICdzdWNjZXNzJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKGV2ZW50LnRhcmdldCkudGV4dCgn0JTQvtCx0LDQstC70LXQvdC+Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh3aW5kb3cudGJzICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy50YnMuZ2V0Q291bnQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICB9LFxyXG4gICAgICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgICAgIGdyb3VwcGVkUmVzdWx0czogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgICAgIF8uZm9yRWFjaChzZWxmLnJlc3VsdHNEYXRhLCBmdW5jdGlvbiAoaG90ZWwsIGtleSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBtaW5OaWdodFZhbHVlID0gXy5taW5CeShob3RlbC50b3Vycy50b3VyLCBmdW5jdGlvbiAobykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcGFyc2VJbnQoby5uaWdodHMpXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIG1heE5pZ2h0VmFsdWUgPSBfLm1heEJ5KGhvdGVsLnRvdXJzLnRvdXIsIGZ1bmN0aW9uIChvKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBwYXJzZUludChvLm5pZ2h0cylcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgbWluRmx5ZGF0ZVZhbHVlID0gXy5taW5CeShob3RlbC50b3Vycy50b3VyLCBmdW5jdGlvbiAobykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gbW9tZW50KG8uZmx5ZGF0ZSwgJ0RELk1NLllZWVknKS5mb3JtYXQoJ1gnKVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBtYXhGbHlkYXRlVmFsdWUgPSBfLm1heEJ5KGhvdGVsLnRvdXJzLnRvdXIsIGZ1bmN0aW9uIChvKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBtb21lbnQoby5mbHlkYXRlLCAnREQuTU0uWVlZWScpLmZvcm1hdCgnWCcpXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIG1vbWVudC5sb2NhbGUoJ2VuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5yZXN1bHRzRGF0YVtrZXldLm1pbk5pZ2h0ID0gbWluTmlnaHRWYWx1ZS5uaWdodHM7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5yZXN1bHRzRGF0YVtrZXldLm1heE5pZ2h0ID0gbWF4TmlnaHRWYWx1ZS5uaWdodHM7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5yZXN1bHRzRGF0YVtrZXldLm1pbkZseWRhdGUgPSBtb21lbnQobWluRmx5ZGF0ZVZhbHVlLmZseWRhdGUsICdERC5NTS5ZWVlZJykuZm9ybWF0KCdERC5NTScpO1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGYucmVzdWx0c0RhdGFba2V5XS5tYXhGbHlkYXRlID0gbW9tZW50KG1heEZseWRhdGVWYWx1ZS5mbHlkYXRlLCAnREQuTU0uWVlZWScpLmZvcm1hdCgnREQuTU0nKTtcclxuICAgICAgICAgICAgICAgICAgICBzZWxmLnJlc3VsdHNEYXRhW2tleV0udHlwZSA9IFwicmVzdWx0XCI7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcblxyXG4gICAgICAgICAgICAgICAgLy9UT0RPINCU0L7QtNC10LvQsNGC0Ywg0LHQsNC90L3QtdGA0YtcclxuLy8gICAgICAgICAgICAgICAgdmFyIGJhbm5lclBvc2l0aW9uID0gWzMsIDddO1xyXG4vLyAgICAgICAgICAgICAgICBfLmZvckVhY2goYmFubmVyUG9zaXRpb24sIGZ1bmN0aW9uIChrZXkpIHtcclxuLy8gICAgICAgICAgICAgICAgICAgIHZhciBuZXdJbmRleCA9IHNlbGYucmVzdWx0c0RhdGEucHVzaCh7dHlwZSA6ICdiYW5uZXInLCBiYW5uZXI6IHNlbGYub2JCYW5uZXJzW18uaW5kZXhPZihiYW5uZXJQb3NpdGlvbiwga2V5KV19KTtcclxuLy8gICAgICAgICAgICAgICAgICAgIHZhciBlbGVtZW50ID0gc2VsZi5yZXN1bHRzRGF0YVtuZXdJbmRleC0xXTtcclxuLy8gICAgICAgICAgICAgICAgICAgIHNlbGYucmVzdWx0c0RhdGEuc3BsaWNlKG5ld0luZGV4LTEsIDEpO1xyXG4vLyAgICAgICAgICAgICAgICAgICAgc2VsZi5yZXN1bHRzRGF0YS5zcGxpY2Uoa2V5LCAwLCBlbGVtZW50KTtcclxuLy8gICAgICAgICAgICAgICAgfSk7XHJcblxyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiBzZWxmLnJlc3VsdHNEYXRhO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZmlsdGVyczoge1xyXG4gICAgICAgICAgICBmb3JtYXRQcmljZTogZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWUudG9TdHJpbmcoKS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIiBcIikgKyAnINCgJztcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGdldE9sZFByaWNlOiBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHZhbHVlID0gcGFyc2VJbnQodmFsdWUpICsgcGFyc2VJbnQodmFsdWUgKiAwLjExKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBNYXRoLnJvdW5kKHZhbHVlKS50b1N0cmluZygpLnJlcGxhY2UoL1xcQig/PShcXGR7M30pKyg/IVxcZCkpL2csIFwiIFwiKSArICcg0KAnO1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgZ2V0Q3JlZGl0UHJpY2U6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgdmFsdWUgPSAocGFyc2VJbnQodmFsdWUpICsgcGFyc2VJbnQodmFsdWUgKiAwLjA3MTcpKSAvIDY7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gTWF0aC5yb3VuZCh2YWx1ZSkudG9TdHJpbmcoKS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIiBcIikgKyAnINCgJztcclxuICAgICAgICAgICAgfSxcclxuXHJcblxyXG5cclxuICAgICAgICAgICAgc2hvcnRUZXh0OiBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIGxldCBzbGljZWQgPSB2YWx1ZS5zbGljZSgwLCAxMDApO1xyXG4gICAgICAgICAgICAgICAgaWYgKHNsaWNlZC5sZW5ndGggPCB2YWx1ZS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICBzbGljZWQgKz0gJy4uLic7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHNsaWNlZDtcclxuXHJcbiAgICAgICAgICAgIH0sXHJcblxyXG5cclxuICAgICAgICAgICAgZGlzcGxheU5pZ2h0czogZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwiXCI7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciB0aXRsZXMgPSBbJ9C90L7Rh9GMJywgJ9C90L7Rh9C4JywgJ9C90L7Rh9C10LknXTtcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZSA9IHBhcnNlSW50KHZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWUgKyBcIiBcIiArIHRpdGxlc1sodmFsdWUgJSAxMCA9PSAxICYmIHZhbHVlICUgMTAwICE9IDExID8gMCA6IHZhbHVlICUgMTAgPj0gMiAmJiB2YWx1ZSAlIDEwIDw9IDQgJiYgKHZhbHVlICUgMTAwIDwgMTAgfHwgdmFsdWUgJSAxMDAgPj0gMjApID8gMSA6IDIpXTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuXHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBDb21SZXN1bHRzLnZ1ZSIsIjx0ZW1wbGF0ZT5cclxuXHQ8ZGl2IGNsYXNzPVwicHJvZ3Jlc3MgbXQtMyBtYi0zXCIgdi1pZj1cInNlYXJjaEluQWN0aW9uXCI+XHJcblx0XHQ8ZGl2IGNsYXNzPVwicHJvZ3Jlc3MtYmFyIHByb2dyZXNzLWJhci1zdHJpcGVkIHByb2dyZXNzLWJhci1hbmltYXRlZCBiZy1zdWNjZXNzIGQtZmxleCBqdXN0aWZ5LWNvbnRlbnQtYmV0d2VlblwiIHJvbGU9XCJwcm9ncmVzc2JhclwiIDpzdHlsZT1cInsnd2lkdGgnIDogc2VhcmNoUHJvZ3Jlc3MgKyAnJSd9XCIgOmFyaWEtdmFsdWVub3c9XCJzZWFyY2hQcm9ncmVzc1wiIGFyaWEtdmFsdWVtaW49XCIwXCIgYXJpYS12YWx1ZW1heD1cIjEwMFwiPlxyXG5cdFx0XHQ8c3BhbiBjbGFzcz1cInByb2dyZXNzLXR5cGUgcGwtMlwiPtCf0L7QuNGB0LouLi4g0J3QsNC50LTQtdC90L4g0YLRg9GA0L7Qsjoge3thbGxSb3dzQ291bnR9fTwvc3Bhbj5cclxuXHRcdFx0PHNwYW4gY2xhc3M9XCJwcm9ncmVzcy1jb21wbGV0ZWQgcHItMlwiPnt7c2VhcmNoUHJvZ3Jlc3N9fSU8L3NwYW4+XHJcblx0XHQ8L2Rpdj5cclxuXHQ8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcblxyXG48c2NyaXB0PlxyXG4gICAgZXhwb3J0IGRlZmF1bHQge1xyXG4gICAgICAgIG5hbWU6ICdjb20tcHJvZ3Jlc3MtYmFyJyxcclxuICAgICAgICBwcm9wczogWydzZWFyY2hJbkFjdGlvbicsICdzZWFyY2hQcm9ncmVzcycsICdhbGxSb3dzQ291bnQnXSxcclxuICAgIH1cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIENvbVByb2dyZXNzQmFyLnZ1ZSIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIGltcG9ydCBWdWUgZnJvbSAndnVlJ1xyXG4vLyBpbXBvcnQgVnVlUmVzb3VyY2UgZnJvbSAndnVlLXJlc291cmNlJ1xyXG4vLyBWdWUudXNlKFZ1ZVJlc291cmNlKTtcclxuaW1wb3J0ICd2LWNhbGVuZGFyL2xpYi92LWNhbGVuZGFyLm1pbi5jc3MnO1xyXG5cclxuaW1wb3J0IENvbURlcGFydHVyZSBmcm9tICcuL0NvbURlcGFydHVyZS52dWUnXHJcbmltcG9ydCBDb21SZXN1bHRzIGZyb20gJy4vQ29tUmVzdWx0cy52dWUnXHJcbmltcG9ydCBDb21Qcm9ncmVzc0JhciBmcm9tICcuL0NvbVByb2dyZXNzQmFyLnZ1ZSdcclxuaW1wb3J0IEhvdGVsRGF0ZVBpY2tlciBmcm9tICd2dWUtaG90ZWwtZGF0ZXBpY2tlcidcclxuXHJcblxyXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XHJcblxyXG4gICAgd2luZG93LnNlYXJjaEFwcCA9IG5ldyBWdWUoe1xyXG4gICAgICAgIGVsOiAnI3NlYXJjaCcsXHJcbiAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAvKtCi0LXQutGD0YnQuNC1INGB0LLQvtC50YHRgtCy0LAqL1xyXG4gICAgICAgICAgICBkZXBhcnR1cmU6IFwiNFwiLCAvL9Ci0LXQutGD0YnQuNC5INCz0L7RgNC+0LRcclxuICAgICAgICAgICAgY291bnRyeTogXCI0XCIsIC8v0KLQtdC60YPRidCw0Y8g0YHRgtGA0LDQvdCwXHJcbiAgICAgICAgICAgIHJlZ2lvbnM6IFwiXCIsIC8v0JLRi9Cx0YDQsNC90L3Ri9C1INGA0LXQs9C40L7QvdGLXHJcbiAgICAgICAgICAgIHN1YnJlZ2lvbnM6IFwiXCIsIC8v0JLRi9Cx0YDQsNC90L3Ri9C1INGB0YPQsdGA0LXQs9C40L7QvdGLXHJcbiAgICAgICAgICAgIG1lYWw6IFwiXCIsIC8v0JLRi9Cx0YDQsNC90L3Ri9C1INGC0LjQv9GLINC/0LjRgtCw0L3QuNGPXHJcbiAgICAgICAgICAgIHN0YXJzOiBcIlwiLCAvL9CS0YvQsdGA0LDQvdC90YvQtSDQutCw0YLQtdCz0L7RgNC40Lgg0L7RgtC10LvRj1xyXG4gICAgICAgICAgICBob3RlbHM6IFwiXCIsIC8v0JLRi9Cx0YDQsNC90L3Ri9C1INC+0YLQtdC70LhcclxuICAgICAgICAgICAgYWR1bHRzOiAyLCAvL9Ca0L7Qu9C40YfQtdGB0YLQstC+INCy0LfRgNC+0YHQu9GL0YVcclxuICAgICAgICAgICAgY2hpbGQ6IDAsIC8v0JrQvtC70LjRh9C10YHRgtCy0L4g0LTQtdGC0LXQuVxyXG4gICAgICAgICAgICBjaGlsZGFnZTE6IFwiMFwiLCAvL9CS0L7Qt9GA0LDRgdGCINGA0LXQsdC10L3QutCwIDFcclxuICAgICAgICAgICAgY2hpbGRhZ2UyOiBcIjBcIiwgLy/QktC+0LfRgNCw0YHRgiDRgNC10LHQtdC90LrQsCAyXHJcbiAgICAgICAgICAgIGNoaWxkYWdlMzogXCIwXCIsIC8v0JLQvtC30YDQsNGB0YIg0YDQtdCx0LXQvdC60LAgM1xyXG4gICAgICAgICAgICBuaWdodHNmcm9tOiA2LCAvL9Ca0L7Qu9C40YfQtdGB0YLQstC+INC90L7Rh9C10Lkg0L7RglxyXG4gICAgICAgICAgICBuaWdodHN0bzogMTQsIC8v0JrQvtC70LjRh9C10YHRgtCy0L4g0L3QvtGH0LXQuSDQtNC+XHJcbiAgICAgICAgICAgIHByaWNlZnJvbTogXCJcIiwgLy/QptC10L3QsCDQvtGCXHJcbiAgICAgICAgICAgIHByaWNldG86IFwiXCIsIC8v0KbQtdC90LAg0LTQvlxyXG4gICAgICAgICAgICBkYXRlZnJvbTogbW9tZW50KCkuYWRkKDEsICdkYXlzJykuZm9ybWF0KCdERC5NTS5ZWVlZJyksIC8v0JTQsNGC0LAg0LLRi9C70LXRgtCwINC+0YJcclxuICAgICAgICAgICAgZGF0ZXRvOiBtb21lbnQoKS5hZGQoMTQsICdkYXlzJykuZm9ybWF0KCdERC5NTS5ZWVlZJyksIC8v0JTQsNGC0LAg0LLRi9C70LXRgtCwINC00L5cclxuICAgICAgICAgICAgb3BlcmF0b3JzOiBcIlwiLCAvL9Ch0L/QuNGB0L7QuiDQvtC/0LXRgNCw0YLQvtGA0L7QslxyXG4gICAgICAgICAgICByZXF1ZXN0aWQ6IFwiXCIsIC8vSUQg0LfQsNC/0YDQvtGB0LBcclxuICAgICAgICAgICAgcGFnZU51bWJlcjogMSwgLy/QndC+0LzQtdGAINGB0YLRgNCw0L3QuNGG0YtcclxuICAgICAgICAgICAgcmF0aW5nOiBcIlwiLCAvL9Cg0LXQudGC0LjQvdCzINC+0YLQtdC70Y9cclxuICAgICAgICAgICAgaGlkZVJlZ3VsYXI6IHRydWUsIC8v0KDQtdC50YLQuNC90LMg0L7RgtC10LvRj1xyXG5cclxuICAgICAgICAgICAgLyrQntCx0YrQtdC60YLRiyovXHJcbiAgICAgICAgICAgIG9iRGVwYXJ0dXJlczogW10sXHJcbiAgICAgICAgICAgIG9iQ291bnRyaWVzOiBbXSxcclxuICAgICAgICAgICAgb2JSZWdpb25zOiBbXSxcclxuICAgICAgICAgICAgb2JSZWdpb25zRGlzcGxheTogW10sXHJcbiAgICAgICAgICAgIG9iU3ViUmVnaW9uczogW10sXHJcbiAgICAgICAgICAgIG9iU3RhcnM6IFtdLFxyXG4gICAgICAgICAgICBvYk9wZXJhdG9yczogW10sXHJcbiAgICAgICAgICAgIG9iRmx5ZGF0ZXM6IFtdLFxyXG4gICAgICAgICAgICBvYkhvdGVsczogW10sXHJcbiAgICAgICAgICAgIG9iTWVhbHM6IFtdLFxyXG4gICAgICAgICAgICBvYlNlYXJjaFJlc3VsdHM6IFtdLFxyXG5cclxuICAgICAgICAgICAgLyrQktGA0LXQvNC10L3QvdGL0LUg0L7QsdGK0LXQutGC0YsqL1xyXG4gICAgICAgICAgICBvYk1lYWxzU2VsZWN0ZWQ6IFtdLFxyXG4gICAgICAgICAgICBvYlJlZ2lvbnNTZWxlY3RlZDoge30sXHJcbiAgICAgICAgICAgIG9iU3ViUmVnaW9uc1NlbGVjdGVkOiB7fSxcclxuICAgICAgICAgICAgb2JIb3RlbHNTZWxlY3RlZDoge30sXHJcbiAgICAgICAgICAgIG9iSG90ZWxzRGVzY3I6IHt9LFxyXG4gICAgICAgICAgICBvYk9wZXJhdG9yc1NlbGVjdGVkOiBbXSxcclxuICAgICAgICAgICAgb2JNZWFsc05hbWU6IFtdLFxyXG4gICAgICAgICAgICBvYk9wZXJhdG9yc05hbWU6IFtdLFxyXG5cclxuICAgICAgICAgICAgLyrQndCw0LjQvNC10L3QvtCy0LDQvdC40Y8qL1xyXG4gICAgICAgICAgICBjb3VudHJ5TmFtZTogXCLQotGD0YDRhtC40Y9cIixcclxuICAgICAgICAgICAgcmVnaW9uc05hbWU6IFwiXCIsXHJcbiAgICAgICAgICAgIHN1YlJlZ2lvbnNOYW1lOiBcIlwiLFxyXG4gICAgICAgICAgICBob3RlbHNOYW1lOiBcIlwiLFxyXG4gICAgICAgICAgICB0b3VyaXN0U3RyaW5nOiBcIjIg0LLQt9GALlwiLFxyXG5cclxuICAgICAgICAgICAgLyrRgdC70YPQttC10LHQvdGL0LUqL1xyXG4gICAgICAgICAgICBwb3BfY291bnRyaWVzOiBbNCwxLDE1LDUsNiwyMCwxNCw0N10sXHJcbiAgICAgICAgICAgIHRvdXJzTm90Rm91bmQ6IGZhbHNlLFxyXG5cclxuICAgICAgICAgICAgcnVSVToge1xyXG4gICAgICAgICAgICAgICAgbmlnaHQ6ICfQvdC+0YfRjCcsXHJcbiAgICAgICAgICAgICAgICBuaWdodHM6ICfQvdC+0YfQtdC5JyxcclxuICAgICAgICAgICAgICAgICdkYXktbmFtZXMnOiBbJ9Cf0L0nLCAn0JLRgicsICfQodGAJywgJ9Cn0YInLCAn0J/RgicsICfQodCxJywgJ9CS0YEnXSxcclxuICAgICAgICAgICAgICAgICdjaGVjay1pbic6ICfQoScsXHJcbiAgICAgICAgICAgICAgICAnY2hlY2stb3V0JzogJ9Cf0L4nLFxyXG4gICAgICAgICAgICAgICAgJ21vbnRoLW5hbWVzJzogWyfQr9C90LLQsNGA0YwnLCAn0KTQtdCy0YDQsNC70YwnLCAn0JzQsNGA0YInLCAn0JDQv9GA0LXQu9GMJywgJ9Cc0LDQuScsICfQmNGO0L3RjCcsICfQmNGO0LvRjCcsICfQkNCy0LPRg9GB0YInLCAn0KHQtdC90YLRj9Cx0YDRjCcsICfQntC60YLRj9Cx0YDRjCcsICfQndC+0Y/QsdGA0YwnLCAn0JTQtdC60LDQsdGA0YwnXSxcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGhvdGVsU2VhcmNoUXVlcnk6IFwiXCIsXHJcbiAgICAgICAgICAgIHJlZ2lvblNlYXJjaFF1ZXJ5OiBcIlwiLFxyXG4gICAgICAgICAgICBjb3VudHJ5U2VhcmNoUXVlcnk6IFwiXCIsXHJcblxyXG4gICAgICAgICAgICBkYXRlU3RhcnQ6IG51bGwsXHJcbiAgICAgICAgICAgIGRhdGVFbmQ6IG51bGwsXHJcblxyXG4gICAgICAgICAgICAvKtC/0L7QuNGB0LoqL1xyXG4gICAgICAgICAgICBzZWFyY2hTdHJpbmc6IFwiXCIsIC8v0L/QvtC40YHQutC+0LLQsNGPINGB0YLRgNC+0LrQsCDRgdGC0YDQsNC90YtcclxuICAgICAgICAgICAgYWxsT3BlcmF0b3JzUHJvY2Vzc2VkOiB0cnVlLCAvL9CS0YHQtSDQotCeINC+0LHRgNCw0LHQvtGC0LDQu9C4INC30LDQv9GA0L7RgVxyXG4gICAgICAgICAgICBhbGxSb3dzQ291bnQ6IDAsIC8v0J7QsdGJ0LXQtSDQutC+0LvQuNGH0LXRgdGC0LLQviDRgNC10LfRg9C70YzRgtCw0YLQvtCyXHJcbiAgICAgICAgICAgIGZpcnN0Um93c0lzRGlzcGxheWVkOiBmYWxzZSwgLy/Qn9C10YDQstCw0Y8g0L/QsNGA0YLQuNGPINGA0LXQt9GD0LvRjNGC0LDRgtC+0LIg0LfQsNCz0YDRg9C20LXQvdCwXHJcbiAgICAgICAgICAgIG1pblByaWNlOiAwLCAvL9Cc0LjQvdC40LzQsNC70YzQvdCw0Y8g0L3QsNC50LTQtdC90L3QsNGPINGG0LXQvdCwXHJcbiAgICAgICAgICAgIHNlYXJjaFByb2dyZXNzOiAwLCAvL9Cf0YDQvtCz0YDQtdGBINC/0L7QuNGB0LrQsFxyXG4gICAgICAgICAgICBzZWFyY2hJbkFjdGlvbjogZmFsc2UsIC8v0KTQu9Cw0LMg0YLQvtCz0L4sINGH0YLQviDQv9C+0LjRgdC6INGB0YLQsNGA0YLQvtCy0LDQu1xyXG4gICAgICAgICAgICBuZXh0UGFnZVJlcXVlc3RJbkFjdGlvbjogZmFsc2UsIC8v0KTQu9Cw0LMg0YLQvtCz0L4sINGH0YLQviDQuNC00LXRgiDQt9Cw0L/RgNC+0YEg0L3QsCDRgdC70LXQtNGD0Y7RidGD0Y4g0YHRgtGA0LDQvdC40YbRgyDRgtGD0YDQvtCyXHJcbiAgICAgICAgICAgIHRvdXJzSXNPdmVyOiBmYWxzZSwgLy/QpNC70LDQsyDRgtC+0LPQviwg0YfRgtC+INCx0L7Qu9GM0YjQtSDRgtGD0YDQvtCyINC90LXRgiwg0L/QvtC60LDQt9Cw0L3RiyDQstGB0LUg0YHRgtGA0LDQvdC40YbRi1xyXG5cclxuICAgICAgICAgICAgLypGYXN0T3JkZXIqL1xyXG4gICAgICAgICAgICBpc1Nob3dGYXN0T3JkZXI6IGZhbHNlLFxyXG4gICAgICAgICAgICBmYXN0Rm9ybVBob25lOiBcIlwiLFxyXG4gICAgICAgICAgICBmYXN0Rm9ybU5hbWU6IFwiXCIsXHJcbiAgICAgICAgICAgIGZhc3RGb3JtTmFtZUhhc0Vycm9yOiBmYWxzZSxcclxuICAgICAgICAgICAgZmFzdEZvcm1QaG9uZUhhc0Vycm9yOiBmYWxzZSxcclxuICAgICAgICAgICAgZmFzdEZvcm1TZW5kZWQ6IGZhbHNlLFxyXG5cclxuXHJcbiAgICAgICAgICAgIGRlYnVnOiBmYWxzZSxcclxuICAgICAgICAgICAgcXVlcnlQYXJhbXM6IFtdLFxyXG4gICAgICAgICAgICBzZWFyY2hVcmw6IFwiXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZTogZmFsc2UsXHJcbiAgICAgICAgICAgIHNob3dBZGRpdGlvbmFsUGFyYW1zOiBmYWxzZSxcclxuXHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgY3JlYXRlZDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBsZXQgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5pbml0KCk7XHJcbiAgICAgICAgICAgIHRoaXMucXVlcnlQYXJhbXMgPSBnZXRBbGxVcmxQYXJhbXMod2luZG93LmxvY2F0aW9uLnRvU3RyaW5nKCkpO1xyXG5cclxuICAgICAgICAgICAgLy/QkNCy0YLQvtGB0YLQsNGA0YIg0L/QvtC40YHQutCwXHJcbiAgICAgICAgICAgIGlmIChfdGhpcy5xdWVyeVBhcmFtcy5zdGFydCA9PT0gJ3knKSB7XHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICBfdGhpcy5zdGFydFNlYXJjaCgpO1xyXG4gICAgICAgICAgICAgICAgfSwgMjAwMClcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy/Ql9Cw0L/QvtC70L3Rj9C10Lwg0L/QsNGA0LDQvNC10YLRgNGLINC40LcgcmVxdWVzdFxyXG4gICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5kYXRlZnJvbSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRhdGVmcm9tID0gdGhpcy5xdWVyeVBhcmFtcy5kYXRlZnJvbTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5kYXRldG8gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kYXRldG8gPSB0aGlzLnF1ZXJ5UGFyYW1zLmRhdGV0bztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5uaWdodHNmcm9tICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubmlnaHRzZnJvbSA9IHRoaXMucXVlcnlQYXJhbXMubmlnaHRzZnJvbTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5uaWdodHN0byAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5pZ2h0c3RvID0gdGhpcy5xdWVyeVBhcmFtcy5uaWdodHN0bztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5wcmljZWZyb20gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcmljZWZyb20gPSB0aGlzLnF1ZXJ5UGFyYW1zLnByaWNlZnJvbTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5wcmljZXRvICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJpY2V0byA9IHRoaXMucXVlcnlQYXJhbXMucHJpY2V0bztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5yYXRpbmcgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yYXRpbmcgPSB0aGlzLnF1ZXJ5UGFyYW1zLnJhdGluZztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMucXVlcnlQYXJhbXMuYWR1bHRzICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWR1bHRzID0gdGhpcy5xdWVyeVBhcmFtcy5hZHVsdHM7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMucXVlcnlQYXJhbXMuY2hpbGQgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jaGlsZCA9IHRoaXMucXVlcnlQYXJhbXMuY2hpbGQ7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5jaGlsZGFnZTEgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2hpbGRhZ2UxID0gdGhpcy5xdWVyeVBhcmFtcy5jaGlsZGFnZTE7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5jaGlsZGFnZTIgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2hpbGRhZ2UyID0gdGhpcy5xdWVyeVBhcmFtcy5jaGlsZGFnZTI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5jaGlsZGFnZTMgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2hpbGRhZ2UzID0gdGhpcy5xdWVyeVBhcmFtcy5jaGlsZGFnZTM7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMucXVlcnlQYXJhbXMuaGlkZXJlZ3VsYXIgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5oaWRlUmVndWxhciA9IHRoaXMucXVlcnlQYXJhbXMuaGlkZXJlZ3VsYXIgPT09IDE7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuZGF0ZVN0YXJ0ID0gbW9tZW50KHRoaXMuZGF0ZWZyb20sICdERC5NTS5ZWVlZJykuX2Q7XHJcbiAgICAgICAgICAgIHRoaXMuZGF0ZUVuZCA9IG1vbWVudCh0aGlzLmRhdGV0bywgJ0RELk1NLllZWVknKS5fZDtcclxuXHJcbiAgICAgICAgICAgIC8vIGR1bm5vIHdoeSB2LW9uPVwicmVzaXplOiBmdW5jXCIgbm90IHdvcmtpbmdcclxuICAgICAgICAgICAgaWYgKGRvY3VtZW50LmJvZHkuY2xpZW50V2lkdGggPD0gNzAwKSB7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy4kc2V0KF90aGlzLCAnbW9iaWxlJywgdHJ1ZSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy4kc2V0KF90aGlzLCAnbW9iaWxlJywgZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGdsb2JhbC53aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGRvY3VtZW50LmJvZHkuY2xpZW50V2lkdGggPD0gNzAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMuJHNldChfdGhpcywgJ21vYmlsZScsIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBfdGhpcy4kc2V0KF90aGlzLCAnbW9iaWxlJywgZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIGxldCBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICAgICAgX3RoaXMuZ2V0RGVwYXJ0dXJlcygpO1xyXG4gICAgICAgICAgICAgICAgX3RoaXMuZ2V0Q291bnRyaWVzKCk7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy5nZXRSZWdpb25zKCk7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy5nZXRPcGVyYXRvcnMoKTtcclxuICAgICAgICAgICAgICAgIF90aGlzLmdldFN0YXJzKCk7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy5nZXRGbHlkYXRlcygpO1xyXG4gICAgICAgICAgICAgICAgX3RoaXMuZ2V0SG90ZWxzKCk7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy5nZXRNZWFscygpO1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgZ2V0RGVwYXJ0dXJlczogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy4kaHR0cC5nZXQoJy9sb2NhbC9tb2R1bGVzL25ld3RyYXZlbC5zZWFyY2gvbGliL2FqYXgucGhwJywge3BhcmFtczoge3R5cGU6IFwiZGVwYXJ0dXJlXCJ9fSkudGhlbihyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub2JEZXBhcnR1cmVzID0gcmVzcG9uc2UuYm9keS5saXN0cy5kZXBhcnR1cmVzLmRlcGFydHVyZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vYkRlcGFydHVyZXMgPSBfLnNvcnRCeSh0aGlzLm9iRGVwYXJ0dXJlcywgWyduYW1lJ10pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLy/Ql9Cw0L/QvtC70L3Rj9C10Lwg0L/QsNGA0LDQvNC10YLRgNGLINC40LcgcmVxdWVzdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5xdWVyeVBhcmFtcy5kZXBhcnR1cmUgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZXBhcnR1cmUgPSB0aGlzLnF1ZXJ5UGFyYW1zLmRlcGFydHVyZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2l0eUZyb21OYW1lID0gXy5maW5kKHRoaXMub2JEZXBhcnR1cmVzLCBbJ2lkJywgdGhpcy5xdWVyeVBhcmFtcy5kZXBhcnR1cmUudG9TdHJpbmcoKV0pID8gXy5maW5kKHRoaXMub2JEZXBhcnR1cmVzLCBbJ2lkJywgdGhpcy5xdWVyeVBhcmFtcy5kZXBhcnR1cmUudG9TdHJpbmcoKV0pLm5hbWVmcm9tIDogXCLQo9GE0YtcIjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChCWC5nZXRDb29raWUoJ05FV1RSQVZFTF9VU0VSX0NJVFknKSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlcGFydHVyZSA9IEJYLmdldENvb2tpZSgnTkVXVFJBVkVMX1VTRVJfQ0lUWScpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jaXR5RnJvbU5hbWUgPSBfLmZpbmQodGhpcy5vYkRlcGFydHVyZXMsIFsnaWQnLCBCWC5nZXRDb29raWUoJ05FV1RSQVZFTF9VU0VSX0NJVFknKS50b1N0cmluZygpXSkgPyBfLmZpbmQodGhpcy5vYkRlcGFydHVyZXMsIFsnaWQnLCBCWC5nZXRDb29raWUoJ05FV1RSQVZFTF9VU0VSX0NJVFknKS50b1N0cmluZygpXSkubmFtZWZyb20gOiBcItCj0YTRi1wiO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL9Cf0L7QtNGC0LLRgNC10LbQtNC10L3QuNC1INCz0L7RgNC+0LTQsFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAkKCcuZGVwYXJ0dXJlX3dyYXAnKS5jb25maXJtQ2l0eSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICBkZXBhcnR1cmVNb2RhbDogJCgnLmNpdHlGcm9tJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICBkZXBhcnR1cmVJZDogdGhpcy5kZXBhcnR1cmVcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGdldENvdW50cmllczogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy4kaHR0cC5nZXQoJy9sb2NhbC9tb2R1bGVzL25ld3RyYXZlbC5zZWFyY2gvbGliL2FqYXgucGhwJywge3BhcmFtczoge3R5cGU6IFwiY291bnRyeVwiLCBjbmRlcDogdGhpcy5kZXBhcnR1cmV9fSkudGhlbihyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmJvZHkubGlzdHMuY291bnRyaWVzLmNvdW50cnkgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vYkNvdW50cmllcyA9IHJlc3BvbnNlLmJvZHkubGlzdHMuY291bnRyaWVzLmNvdW50cnk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub2JDb3VudHJpZXMgPSBfLnNvcnRCeSh0aGlzLm9iQ291bnRyaWVzLCBbJ25hbWUnXSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL9CX0LDQv9C+0LvQvdGP0LXQvCDQv9Cw0YDQsNC80LXRgtGA0Ysg0LjQtyByZXF1ZXN0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnF1ZXJ5UGFyYW1zLmNvdW50cnkgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb3VudHJ5ID0gdGhpcy5xdWVyeVBhcmFtcy5jb3VudHJ5O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb3VudHJ5TmFtZSA9IF8uZmluZCh0aGlzLm9iQ291bnRyaWVzLCBbJ2lkJywgdGhpcy5xdWVyeVBhcmFtcy5jb3VudHJ5LnRvU3RyaW5nKCldKSA/IF8uZmluZCh0aGlzLm9iQ291bnRyaWVzLCBbJ2lkJywgdGhpcy5xdWVyeVBhcmFtcy5jb3VudHJ5LnRvU3RyaW5nKCldKS5uYW1lIDogXCLQotGD0YDRhtC40Y9cIjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGdldFJlZ2lvbnM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuJGh0dHAuZ2V0KCcvbG9jYWwvbW9kdWxlcy9uZXd0cmF2ZWwuc2VhcmNoL2xpYi9hamF4LnBocCcsIHtwYXJhbXM6IHt0eXBlOiBcInJlZ2lvblwiLCByZWdjb3VudHJ5OiB0aGlzLmNvdW50cnl9fSkudGhlbihyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IF90aGlzID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMub2JSZWdpb25zID0gcmVzcG9uc2UuYm9keS5saXN0cy5yZWdpb25zLnJlZ2lvbjtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy/Ql9Cw0L/QvtC70L3Rj9C10Lwg0L/QsNGA0LDQvNC10YLRgNGLINC40LcgcmVxdWVzdFxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChfdGhpcy5xdWVyeVBhcmFtcy5yZWdpb25zICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHF1ZXJ5ID0gdGhpcy5hcnJheUNvbnZlcnQoX3RoaXMucXVlcnlQYXJhbXMucmVnaW9ucyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF90aGlzLnJlZ2lvbnMgPSBxdWVyeS5qb2luKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF8uZm9yRWFjaChxdWVyeSwgZnVuY3Rpb24gKHZhbHVlLCBrZXkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghXy5maW5kKF90aGlzLm9iUmVnaW9ucywgWydpZCcsIHZhbHVlXSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5vYlJlZ2lvbnNTZWxlY3RlZFt2YWx1ZV0gPSBfLmZpbmQoX3RoaXMub2JSZWdpb25zLCBbJ2lkJywgdmFsdWVdKSA/IF8uZmluZChfdGhpcy5vYlJlZ2lvbnMsIFsnaWQnLCB2YWx1ZV0pIDogXCJcIjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF90aGlzLnJlZ2lvbnNTZWxlY3QoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIF90aGlzLmdldFN1YlJlZ2lvbnMoKTtcclxuXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGdldFN1YlJlZ2lvbnM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuJGh0dHAuZ2V0KCcvbG9jYWwvbW9kdWxlcy9uZXd0cmF2ZWwuc2VhcmNoL2xpYi9hamF4LnBocCcsIHtwYXJhbXM6IHt0eXBlOiBcInN1YnJlZ2lvblwiLCByZWdjb3VudHJ5OiB0aGlzLmNvdW50cnl9fSkudGhlbihyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IF90aGlzID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF90aGlzLm9iUmVnaW9uc0Rpc3BsYXkgPSBfdGhpcy5vYlJlZ2lvbnM7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5vYlN1YlJlZ2lvbnMgPSByZXNwb25zZS5ib2R5Lmxpc3RzLnN1YnJlZ2lvbnMuc3VicmVnaW9uO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgXy5mb3JFYWNoKF90aGlzLm9iU3ViUmVnaW9ucywgZnVuY3Rpb24gKHZhbHVlLCBrZXkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8v0JjRidC10Lwg0LjQvdC00LXQutGBINGA0L7QtNC40YLQtdC70YzRgdC60L7Qs9C+INGA0LXQs9C40L7QvdCwINCyINC80LDRgdGB0LjQstC1IG9iUmVnaW9uc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGZpbmRlZCA9IF8uZmluZEluZGV4KF90aGlzLm9iUmVnaW9ucywgZnVuY3Rpb24gKG8pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gby5pZCA9PT0gdmFsdWUucGFyZW50cmVnaW9uO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy/QldGB0LvQuCDRgyDQtNCw0L3QvdC+0LPQviDRgNC+0LTQuNGC0LXQu9GM0YHQutC+0LPQviDRgNC10LPQuNC+0L3QsCDQvdC10YIg0YHRg9Cx0YDQtdCz0LjQvtC90L7Qsiwg0YLQvtCz0LTQsCDQtNC+0LHQsNCy0LvRj9C10Lwg0LXQvNGDINC+0LHRitC10LrRgiBzdWJyZWdpb25zXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoXy5pc05pbChfdGhpcy5vYlJlZ2lvbnNbcGFyc2VJbnQoZmluZGVkKV0uc3VicmVnaW9ucykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5vYlJlZ2lvbnNbcGFyc2VJbnQoZmluZGVkKV0uc3VicmVnaW9ucyA9IHt9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8v0JfQsNC/0L7Qu9C90Y/QtdC8INC+0LHRitC10LrRgiBzdWJyZWdpb25zXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy4kc2V0KF90aGlzLm9iUmVnaW9uc1twYXJzZUludChmaW5kZWQpXS5zdWJyZWdpb25zLCBrZXksIHZhbHVlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5vYlJlZ2lvbnNEaXNwbGF5ID0gX3RoaXMub2JSZWdpb25zO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8v0JfQsNC/0L7Qu9C90Y/QtdC8INC/0LDRgNCw0LzQtdGC0YDRiyDQuNC3IHJlcXVlc3RcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKF90aGlzLnF1ZXJ5UGFyYW1zLnN1YnJlZ2lvbnMgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHF1ZXJ5ID0gdGhpcy5hcnJheUNvbnZlcnQoX3RoaXMucXVlcnlQYXJhbXMuc3VicmVnaW9ucyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5zdWJyZWdpb25zID0gcXVlcnkuam9pbigpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXy5mb3JFYWNoKHF1ZXJ5LCBmdW5jdGlvbiAodmFsdWUsIGtleSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghXy5maW5kKF90aGlzLm9iU3ViUmVnaW9ucywgWydpZCcsIHZhbHVlXSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5vYlN1YlJlZ2lvbnNTZWxlY3RlZFt2YWx1ZV0gPSBfLmZpbmQoX3RoaXMub2JTdWJSZWdpb25zLCBbJ2lkJywgdmFsdWVdKSA/IF8uZmluZChfdGhpcy5vYlN1YlJlZ2lvbnMsIFsnaWQnLCB2YWx1ZV0pIDogXCJcIjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX3RoaXMuc3ViUmVnaW9uc1NlbGVjdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgNTAwKTtcclxuXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGdldFN0YXJzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLiRodHRwLmdldCgnL2xvY2FsL21vZHVsZXMvbmV3dHJhdmVsLnNlYXJjaC9saWIvYWpheC5waHAnLCB7cGFyYW1zOiB7dHlwZTogXCJzdGFyc1wifX0pLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBfdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vYlN0YXJzID0gcmVzcG9uc2UuYm9keS5saXN0cy5zdGFycy5zdGFyO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvL9CX0LDQv9C+0LvQvdGP0LXQvCDQv9Cw0YDQsNC80LXRgtGA0Ysg0LjQtyByZXF1ZXN0XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKF90aGlzLnF1ZXJ5UGFyYW1zLnN0YXJzICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgX3RoaXMuc3RhcnMgPSBfdGhpcy5xdWVyeVBhcmFtcy5zdGFyc1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGdldE9wZXJhdG9yczogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy4kaHR0cC5nZXQoJy9sb2NhbC9tb2R1bGVzL25ld3RyYXZlbC5zZWFyY2gvbGliL2FqYXgucGhwJywge3BhcmFtczoge3R5cGU6IFwib3BlcmF0b3JcIn19KS50aGVuKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgX3RoaXMgPSB0aGlzO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub2JPcGVyYXRvcnMgPSByZXNwb25zZS5ib2R5Lmxpc3RzLm9wZXJhdG9ycy5vcGVyYXRvcjtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy/Ql9Cw0L/QvtC70L3Rj9C10Lwg0L/QsNGA0LDQvNC10YLRgNGLINC40LcgcmVxdWVzdFxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChfdGhpcy5xdWVyeVBhcmFtcy5vcGVyYXRvcnMgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgcXVlcnkgPSB0aGlzLmFycmF5Q29udmVydChfdGhpcy5xdWVyeVBhcmFtcy5vcGVyYXRvcnMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5vcGVyYXRvcnMgPSBxdWVyeS5qb2luKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF8uZm9yRWFjaChxdWVyeSwgZnVuY3Rpb24gKHZhbHVlLCBrZXkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF90aGlzLm9wZXJhdG9yU2VsZWN0KF8uZmluZChfdGhpcy5vYk9wZXJhdG9ycywgWydpZCcsIHZhbHVlXSkgPyBfLmZpbmQoX3RoaXMub2JPcGVyYXRvcnMsIFsnaWQnLCB2YWx1ZV0pIDogXCJcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgZ2V0Rmx5ZGF0ZXM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuJGh0dHAuZ2V0KCcvbG9jYWwvbW9kdWxlcy9uZXd0cmF2ZWwuc2VhcmNoL2xpYi9hamF4LnBocCcsIHtwYXJhbXM6IHt0eXBlOiBcImZseWRhdGVcIiwgZmx5ZGVwYXJ0dXJlOiB0aGlzLmRlcGFydHVyZSwgZmx5Y291bnRyeTogdGhpcy5jb3VudHJ5fX0pLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5ib2R5Lmxpc3RzLmZseWRhdGVzICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub2JGbHlkYXRlcyA9IHJlc3BvbnNlLmJvZHkubGlzdHMuZmx5ZGF0ZXMuZmx5ZGF0ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kYXRlcGlja2VyQXR0cnNbMF0uZGF0ZXMgPSB0aGlzLm9iRmx5ZGF0ZXM7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0ZXBpY2tlckF0dHJzWzBdLmRhdGVzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBjaGVja0luQ2hhbmdlOiBmdW5jdGlvbiAoZGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0ZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRhdGVmcm9tID0gbW9tZW50KGRhdGUpLmZvcm1hdCgnREQuTU0uWVlZWScpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBjaGVja091dENoYW5nZTogZnVuY3Rpb24gKGRhdGUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGF0ZXRvID0gbW9tZW50KGRhdGUpLmZvcm1hdCgnREQuTU0uWVlZWScpXHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBnZXRIb3RlbHM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuJGh0dHAuZ2V0KCcvbG9jYWwvbW9kdWxlcy9uZXd0cmF2ZWwuc2VhcmNoL2xpYi9hamF4LnBocCcsIHtwYXJhbXM6IHt0eXBlOiBcImhvdGVsXCIsIGhvdGNvdW50cnk6IHRoaXMuY291bnRyeSwgaG90cmVnaW9uOiB0aGlzLnJlZ2lvbnMsIGhvdHN0YXJzOiB0aGlzLnN0YXJzLCBob3RyYXRpbmc6IHRoaXMucmF0aW5nfX0pLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBfdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMub2JIb3RlbHMgPSByZXNwb25zZS5ib2R5Lmxpc3RzLmhvdGVscy5ob3RlbDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy/Ql9Cw0L/QvtC70L3Rj9C10Lwg0L/QsNGA0LDQvNC10YLRgNGLINC40LcgcmVxdWVzdFxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChfdGhpcy5xdWVyeVBhcmFtcy5ob3RlbHMgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgcXVlcnkgPSB0aGlzLmFycmF5Q29udmVydChfdGhpcy5xdWVyeVBhcmFtcy5ob3RlbHMpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgX3RoaXMuaG90ZWxzID0gcXVlcnkuam9pbigpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfLmZvckVhY2gocXVlcnksIGZ1bmN0aW9uICh2YWx1ZSwga2V5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIV8uZmluZChfdGhpcy5vYkhvdGVscywgWydpZCcsIHZhbHVlXSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5vYkhvdGVsc1NlbGVjdGVkW3ZhbHVlXSA9IF8uZmluZChfdGhpcy5vYkhvdGVscywgWydpZCcsIHZhbHVlXSkgPyBfLmZpbmQoX3RoaXMub2JIb3RlbHMsIFsnaWQnLCB2YWx1ZV0pIDogXCJcIjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF90aGlzLmhvdGVsc1NlbGVjdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgZ2V0TWVhbHM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIGxldCBfdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy4kaHR0cC5nZXQoJy9sb2NhbC9tb2R1bGVzL25ld3RyYXZlbC5zZWFyY2gvbGliL2FqYXgucGhwJywge3BhcmFtczoge3R5cGU6IFwibWVhbFwifX0pLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIF90aGlzLm9iTWVhbHMgPSByZXNwb25zZS5ib2R5Lmxpc3RzLm1lYWxzLm1lYWw7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8v0JfQsNC/0L7Qu9C90Y/QtdC8INC/0LDRgNCw0LzQtdGC0YDRiyDQuNC3IHJlcXVlc3RcclxuICAgICAgICAgICAgICAgICAgICBpZiAoX3RoaXMucXVlcnlQYXJhbXMubWVhbCAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBxdWVyeSA9IHRoaXMuYXJyYXlDb252ZXJ0KF90aGlzLnF1ZXJ5UGFyYW1zLnJlZ2lvbnMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5tZWFsID0gcXVlcnkuam9pbigpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfLmZvckVhY2gocXVlcnksIGZ1bmN0aW9uICh2YWx1ZSwga2V5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5tZWFsc1NlbGVjdChfLmZpbmQoX3RoaXMub2JNZWFscywgWydpZCcsIHZhbHVlXSkgPyBfLmZpbmQoX3RoaXMub2JNZWFscywgWydpZCcsIHZhbHVlXSkgOiBcIlwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG5cclxuICAgICAgICAgICAgLyrQn9Ce0JjQodCa0J7QktCr0JUg0JzQldCi0J7QlNCrKi9cclxuXHJcbiAgICAgICAgICAgIC8v0J3QsNGH0LDQu9C+INC/0L7QuNGB0LrQsFxyXG4gICAgICAgICAgICBzdGFydFNlYXJjaDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgbGV0ICR0aGlzID0gdGhpcztcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlYnVnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT0g0J3QsNGH0LjQvdCw0LXQvCDQv9C+0LjRgdC6ID09PT09PT09PT09PT0nKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaEluQWN0aW9uID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMub2JTZWFyY2hSZXN1bHRzID0gW107XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlcXVlc3RpZCA9ICcnO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wYWdlTnVtYmVyID0gMTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZmlyc3RSb3dzSXNEaXNwbGF5ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWxsUm93c0NvdW50ID0gMDtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoUHJvZ3Jlc3MgPSAwO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5xdWVyeVBhcmFtcyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50b3Vyc05vdEZvdW5kID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IHBhcmFtcyA9IHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBcInNlYXJjaFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlcGFydHVyZTogdGhpcy5kZXBhcnR1cmUsXHJcbiAgICAgICAgICAgICAgICAgICAgY291bnRyeTogdGhpcy5jb3VudHJ5LFxyXG4gICAgICAgICAgICAgICAgICAgIHJlZ2lvbnM6IHRoaXMucmVnaW9ucyxcclxuICAgICAgICAgICAgICAgICAgICBzdWJyZWdpb25zOiB0aGlzLnN1YnJlZ2lvbnMsXHJcbiAgICAgICAgICAgICAgICAgICAgbWVhbDogdGhpcy5tZWFsLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXJzOiB0aGlzLnN0YXJzLFxyXG4gICAgICAgICAgICAgICAgICAgIGhvdGVsczogdGhpcy5ob3RlbHMsXHJcbiAgICAgICAgICAgICAgICAgICAgYWR1bHRzOiB0aGlzLmFkdWx0cyxcclxuICAgICAgICAgICAgICAgICAgICBjaGlsZDogdGhpcy5jaGlsZCxcclxuICAgICAgICAgICAgICAgICAgICBjaGlsZGFnZTE6IHRoaXMuY2hpbGRhZ2UxLFxyXG4gICAgICAgICAgICAgICAgICAgIGNoaWxkYWdlMjogdGhpcy5jaGlsZGFnZTIsXHJcbiAgICAgICAgICAgICAgICAgICAgY2hpbGRhZ2UzOiB0aGlzLmNoaWxkYWdlMyxcclxuICAgICAgICAgICAgICAgICAgICBuaWdodHNmcm9tOiB0aGlzLm5pZ2h0c2Zyb20sXHJcbiAgICAgICAgICAgICAgICAgICAgbmlnaHRzdG86IHRoaXMubmlnaHRzdG8sXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpY2Vmcm9tOiB0aGlzLnByaWNlZnJvbSxcclxuICAgICAgICAgICAgICAgICAgICBwcmljZXRvOiB0aGlzLnByaWNldG8sXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0ZWZyb206IHRoaXMuZGF0ZWZyb20sXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0ZXRvOiB0aGlzLmRhdGV0byxcclxuICAgICAgICAgICAgICAgICAgICBvcGVyYXRvcnM6IHRoaXMub3BlcmF0b3JzLFxyXG4gICAgICAgICAgICAgICAgICAgIHJhdGluZzogdGhpcy5yYXRpbmcsXHJcbiAgICAgICAgICAgICAgICAgICAgaGlkZXJlZ3VsYXI6IHRoaXMuaGlkZVJlZ3VsYXIgPyAxIDogMCxcclxuICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgJChcImh0bWwsIGJvZHlcIikuYW5pbWF0ZSh7c2Nyb2xsVG9wOiAkKCcjcmVzdWx0cycpLm9mZnNldCgpLnRvcCAtIDEwMH0sIDEwMDApO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuJGh0dHAuZ2V0KCcvbG9jYWwvbW9kdWxlcy9uZXd0cmF2ZWwuc2VhcmNoL2xpYi9hamF4LnBocCcsIHtwYXJhbXM6IHBhcmFtc30pLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5ib2R5LnJlc3VsdC5yZXF1ZXN0aWQgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlcXVlc3RpZCA9IHJlc3BvbnNlLmJvZHkucmVzdWx0LnJlcXVlc3RpZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hVcmwgPSByZXNwb25zZS5ib2R5LnVybDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGlzdG9yeS5wdXNoU3RhdGUodGhpcy5yZXF1ZXN0aWQsIFwi0J/QvtC40YHQuiDRgtGD0YDQvtCyXCIsIFwiP3N0YXJ0PVkmXCIgKyByZXNwb25zZS5ib2R5LnVybCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0aGlzLmdldFN0YXR1cygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCAzMDAwKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5kZWJ1Zykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCLQntGI0LjQsdC60LAgc3RhcnRTZWFyY2hcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIC8v0JfQsNC/0YDQvtGBINGB0L7RgdGC0L7Rj9C90LjQtSDQv9C+0LjRgdC60LBcclxuICAgICAgICAgICAgZ2V0U3RhdHVzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5kZWJ1Zykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCc9PT09PT09PT09PSDQn9GA0L7QstC10YDRj9C10Lwg0YHRgtCw0YLRg9GBID09PT09PT09PT0nKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGxldCAkdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgICAgICAgICBsZXQgUm93c0NvdW50ID0gMDsgLy/QntCx0YnQtdC1INC60L7Qu9C40YfQtdGB0YLQstC+INGA0LXQt9GD0LvRjNGC0LDRgtC+0LIg0LTQu9GPINGG0LjQutC70LBcclxuICAgICAgICAgICAgICAgICR0aGlzLmdldFJlYXVlSXRlcmF0aW9uID0gMTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLiRodHRwLmdldCgnL2xvY2FsL21vZHVsZXMvbmV3dHJhdmVsLnNlYXJjaC9saWIvYWpheC5waHAnLCB7cGFyYW1zOiB7dHlwZTogXCJzdGF0dXNcIiwgcmVxdWVzdGlkOiB0aGlzLnJlcXVlc3RpZCwgb3BlcmF0b3JzdGF0dXM6IDF9fSkudGhlbihyZXNwb25zZSA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5ib2R5LmRhdGEuc3RhdHVzICE9PSB1bmRlZmluZWQpIHsgLy/Ql9Cw0L/RgNC+0YEg0L/RgNC+0YjQtdC7INCx0LXQtyDQvtGI0LjQsdC+0LpcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICR0aGlzLnNlYXJjaFByb2dyZXNzID0gcmVzcG9uc2UuYm9keS5kYXRhLnN0YXR1cy5wcm9ncmVzczsgLy/Qn9GA0L7RhtC10L3RgiDQstGL0L/QvtC70L3QtdC90LjRj1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgUm93c0NvdW50ID0gcmVzcG9uc2UuYm9keS5kYXRhLnN0YXR1cy5ob3RlbHNmb3VuZDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICgkdGhpcy5hbGxSb3dzQ291bnQgIT09IFJvd3NDb3VudCAmJiAhJHRoaXMuZmlyc3RSb3dzSXNEaXNwbGF5ZWQpIHsgLy/QldGB0LvQuCDQutC+0LvQuNGH0LXRgdGC0LLQviDQvdCw0LnQtNC10L3QvdGL0YUg0YDQtdC30YPQu9GM0YLQsNGC0L7QsiDQt9CwINGG0LjQutC7INC90LUg0YDQsNCy0L3QsCDRg9C20LUg0L3QsNC50LTQtdC90L3Ri9C8INGA0LXQt9GD0LvRjNGC0LDRgtCw0Lwg0Lgg0L/QtdGA0LLQsNGPINC/0LDRgNGC0LjRjyDQtdGJ0LUg0L3QtSDQvtGC0L7QsdGA0LDQttC10L3QsCwg0YLQvtCz0LTQsCDQstGL0LLQvtC00LjQvCDQv9C10YDQstGD0Y4g0L/QsNGA0YLQuNGOINGA0LXQt9GD0LvRjNGC0LDRgtC+0LJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0aGlzLmZpcnN0Um93c0lzRGlzcGxheWVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlYnVnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCI9PdCS0YvQstC+0LTQuNC8INC/0LXRgNCy0YvQtSDRgNC10LfRg9C70YzRgtCw0YLRiyDQvdCwINGN0LrRgNCw0L09PVwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0aGlzLmdldFJlc3VsdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocGFyc2VJbnQoJHRoaXMuc2VhcmNoUHJvZ3Jlc3MpICE9PSAxMDApIHsgLy/Qn9C+0LLRgtC+0YDRj9C10Lwg0L/RgNC+0LLQtdGA0LrRgyDRgdGC0LDRgtGD0YHQsFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuZGVidWcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCLQndC1INCy0YHQtSDQvtC/0LXRgNCw0YLQvtGA0Ysg0L7RgtGA0LDQsdC+0YLQsNC70LgsINC/0L7QstGC0L7RgNGP0LXQvCDQt9Cw0L/RgNC+0YEg0YHRgtCw0YLRg9GB0L7QslwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHRoaXMuZ2V0U3RhdHVzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCAyMDAwKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlYnVnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCI9PdCS0YHQtSDQvtCx0YDQsNCx0L7RgtC90L4hINCj0YDRgNCwISEhPT1cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCI9PdCS0YvQstC+0LTQuNC8INCy0YHQtSDRgNC10LfRg9C70YzRgtCw0YLRiyDQvdCwINGN0LrRgNCw0L09PVwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0aGlzLnNlYXJjaFByb2dyZXNzID0gMTAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHRoaXMuc2VhcmNoSW5BY3Rpb24gPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0aGlzLmdldFJlc3VsdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAkdGhpcy5hbGxSb3dzQ291bnQgPSByZXNwb25zZS5ib2R5LmRhdGEuc3RhdHVzLmhvdGVsc2ZvdW5kOyAvL9Ce0LHRidC10LUg0LrQvtC70LjRh9C10YHRgtCy0L4g0YDQtdC30YPQu9GM0YLQsNGC0L7QslxyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlYnVnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcItCe0YjQuNCx0LrQsCBnZXRTdGF0dXNcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIC8v0J/QvtC70YPRh9C10L3QuNC1INGA0LXQt9GD0LvRjNGC0LDRgtC+0LIg0L/QvtC40YHQutCwXHJcbiAgICAgICAgICAgIGdldFJlc3VsdDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy4kaHR0cC5nZXQoJy9sb2NhbC9tb2R1bGVzL25ld3RyYXZlbC5zZWFyY2gvbGliL2FqYXgucGhwJywge3BhcmFtczoge3R5cGU6IFwicmVzdWx0XCIsIHJlcXVlc3RpZDogdGhpcy5yZXF1ZXN0aWQsIHBhZ2U6IHRoaXMucGFnZU51bWJlcn19KS50aGVuKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuYm9keS5kYXRhLnN0YXR1cy5zdGF0ZSA9PT0gJ2ZpbmlzaGVkJyAmJiBwYXJzZUludChyZXNwb25zZS5ib2R5LmRhdGEuc3RhdHVzLnRvdXJzZm91bmQpID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudG91cnNOb3RGb3VuZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuYm9keS5kYXRhLnJlc3VsdCAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlYnVnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnPT09PT09PSDQn9C+0LvRg9GH0LjRgtC1INGA0LXQt9GD0LvRjNGC0LDRgtGLID09PT09PT09Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnBhZ2VOdW1iZXIgPT09IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub2JTZWFyY2hSZXN1bHRzID0gcmVzcG9uc2UuYm9keS5kYXRhLnJlc3VsdC5ob3RlbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub2JTZWFyY2hSZXN1bHRzID0gXy5jb25jYXQodGhpcy5vYlNlYXJjaFJlc3VsdHMsIHJlc3BvbnNlLmJvZHkuZGF0YS5yZXN1bHQuaG90ZWwpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5uZXh0UGFnZVJlcXVlc3RJbkFjdGlvbiA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIl0nKS5wb3BvdmVyKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmlnZ2VyOiAnaG92ZXIgfCBjbGljaycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGFpbmVyOiAkKCdib2R5JylcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCA1MDApXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnBhZ2VOdW1iZXIgPiAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5leHRQYWdlUmVxdWVzdEluQWN0aW9uID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRvdXJzSXNPdmVyID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5kZWJ1Zykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCLQntGI0LjQsdC60LAgZ2V0UmVzdWx0XCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBsb2FkTW9yZVRvdXJzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5leHRQYWdlUmVxdWVzdEluQWN0aW9uID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMucGFnZU51bWJlciA9IHRoaXMucGFnZU51bWJlciArIDE7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFJlc3VsdCgpO1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgLyrQlNC+0L8uINC80LXRgtC+0LTRiyovXHJcbiAgICAgICAgICAgIC8qKlxyXG4gICAgICAgICAgICAgKiBAcmV0dXJuIHtzdHJpbmd9XHJcbiAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICBGbGFnQ2xhc3M6IGZ1bmN0aW9uIChpbmRleCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwidmFsdWVzX19pbWFnZSBmbGFnLXVpX25hcnJvd3RwbF9mbGFnc18zMHgyMF9cIiArIGluZGV4O1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuXHJcbiAgICAgICAgICAgIC8qKlxyXG4gICAgICAgICAgICAgKiDQpNC40LvRjNGC0YAg0L/QvtC/0YPQu9GP0YDQvdGL0YUg0YHRgtGA0LDQvVxyXG4gICAgICAgICAgICAgKiBAcmV0dXJuIHtib29sZWFufVxyXG4gICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgSXNQb3B1bGFyQ291bnRyeTogZnVuY3Rpb24gKGNvdW50cnkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBfLmluY2x1ZGVzKHRoaXMucG9wX2NvdW50cmllcywgcGFyc2VJbnQoY291bnRyeSkpO1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgcmVnaW9uc1NlbGVjdDogZnVuY3Rpb24gKHJlZ2lvbiA9IFwiXCIpIHtcclxuICAgICAgICAgICAgICAgIGxldCBzZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAocmVnaW9uICE9PSBcIlwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgIV8uZmluZChzZWxmLm9iUmVnaW9uc1NlbGVjdGVkLCBbJ2lkJywgcmVnaW9uLmlkXSkgPyBzZWxmLm9iUmVnaW9uc1NlbGVjdGVkW3JlZ2lvbi5pZC50b1N0cmluZygpXSA9IHtpZDogcmVnaW9uLmlkLCBuYW1lOiByZWdpb24ubmFtZX0gOiBkZWxldGUgc2VsZi5vYlJlZ2lvbnNTZWxlY3RlZFtyZWdpb24uaWQudG9TdHJpbmcoKV07XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8v0J7Rh9C40YnQsNC10Lwg0LLRgdC1INGB0YPQsdGA0LXQs9C40L7QvdGLINC/0YDQuCDQstGL0LHQvtGA0LUg0YDQvtC00LjRgtC10LvRjNGB0LrQvtCz0L4g0YDQtdCz0LjQvtC90LBcclxuICAgICAgICAgICAgICAgICAgICBsZXQgZmluZGVkID0gXy5maW5kSW5kZXgoc2VsZi5vYlJlZ2lvbnNEaXNwbGF5LCBmdW5jdGlvbiAobykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gby5pZCA9PT0gcmVnaW9uLmlkO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghXy5pc05pbChzZWxmLm9iUmVnaW9uc0Rpc3BsYXlbZmluZGVkXS5zdWJyZWdpb25zKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfLmZvckVhY2goc2VsZi5vYlJlZ2lvbnNEaXNwbGF5W2ZpbmRlZF0uc3VicmVnaW9ucywgZnVuY3Rpb24gKHZhbHVlLCBrZXkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlbGV0ZSBzZWxmLm9iU3ViUmVnaW9uc1NlbGVjdGVkW3ZhbHVlLmlkXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi5zdWJSZWdpb25zU2VsZWN0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgc2VsZi5yZWdpb25zID0gXy5tYXAoc2VsZi5vYlJlZ2lvbnNTZWxlY3RlZCwgJ2lkJykuam9pbigpO1xyXG4gICAgICAgICAgICAgICAgc2VsZi5yZWdpb25zTmFtZSA9IF8ubWFwKHNlbGYub2JSZWdpb25zU2VsZWN0ZWQsICduYW1lJyk7XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBzdWJSZWdpb25zU2VsZWN0OiBmdW5jdGlvbiAoc3VicmVnaW9uID0gXCJcIikge1xyXG5cclxuICAgICAgICAgICAgICAgIC8v0J7Rh9C40YnQsNC10Lwg0YDQvtC00LjRgtC10LvRjNGB0LrQuNC5INGA0LXQs9C40L7QvSDQtdGB0LvQuCDQstGL0LHRgNCw0L0g0YHRg9Cx0YDQtdCz0LjQvtC9XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5vYlJlZ2lvbnNTZWxlY3RlZFtzdWJyZWdpb24ucGFyZW50cmVnaW9uXSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVsZXRlIHRoaXMub2JSZWdpb25zU2VsZWN0ZWRbc3VicmVnaW9uLnBhcmVudHJlZ2lvbl07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZWdpb25zU2VsZWN0KCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHN1YnJlZ2lvbiAhPT0gXCJcIikge1xyXG4gICAgICAgICAgICAgICAgICAgICFfLmZpbmQodGhpcy5vYlN1YlJlZ2lvbnNTZWxlY3RlZCwgWydpZCcsIHN1YnJlZ2lvbi5pZF0pID8gdGhpcy5vYlN1YlJlZ2lvbnNTZWxlY3RlZFtzdWJyZWdpb24uaWRdID0ge2lkOiBzdWJyZWdpb24uaWQsIG5hbWU6IHN1YnJlZ2lvbi5uYW1lfSA6IGRlbGV0ZSB0aGlzLm9iU3ViUmVnaW9uc1NlbGVjdGVkW3N1YnJlZ2lvbi5pZF07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN1YnJlZ2lvbnMgPSBfLm1hcCh0aGlzLm9iU3ViUmVnaW9uc1NlbGVjdGVkLCAnaWQnKS5qb2luKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN1YlJlZ2lvbnNOYW1lID0gXy5tYXAodGhpcy5vYlN1YlJlZ2lvbnNTZWxlY3RlZCwgJ25hbWUnKTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGhvdGVsc1NlbGVjdDogZnVuY3Rpb24gKGhvdGVsID0gXCJcIikge1xyXG4gICAgICAgICAgICAgICAgaWYgKGhvdGVsICE9PSBcIlwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgIV8uZmluZCh0aGlzLm9iSG90ZWxzU2VsZWN0ZWQsIFsnaWQnLCBob3RlbC5pZF0pID8gdGhpcy5vYkhvdGVsc1NlbGVjdGVkW2hvdGVsLmlkXSA9IHtpZDogaG90ZWwuaWQsIG5hbWU6IGhvdGVsLm5hbWV9IDogZGVsZXRlIHRoaXMub2JIb3RlbHNTZWxlY3RlZFtob3RlbC5pZF07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLmhvdGVscyA9IF8ubWFwKHRoaXMub2JIb3RlbHNTZWxlY3RlZCwgJ2lkJykuam9pbigpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ob3RlbHNOYW1lID0gXy5tYXAodGhpcy5vYkhvdGVsc1NlbGVjdGVkLCAnbmFtZScpO1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgLy/Qn9C40YLQsNC90LjQtSDQtNC70Y8g0LfQsNC/0YDQvtGB0LBcclxuICAgICAgICAgICAgbWVhbHNTZWxlY3Q6IGZ1bmN0aW9uIChtZWFsID0gXCJcIikge1xyXG4gICAgICAgICAgICAgICAgaWYgKG1lYWwgIT09IFwiXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAhXy5pbmNsdWRlcyh0aGlzLm9iTWVhbHNTZWxlY3RlZCwgbWVhbC5pZCkgPyB0aGlzLm9iTWVhbHNTZWxlY3RlZC5wdXNoKG1lYWwuaWQpIDogXy5wdWxsKHRoaXMub2JNZWFsc1NlbGVjdGVkLCBtZWFsLmlkKTtcclxuICAgICAgICAgICAgICAgICAgICAhXy5pbmNsdWRlcyh0aGlzLm9iTWVhbHNOYW1lLCBtZWFsLm5hbWUpID8gdGhpcy5vYk1lYWxzTmFtZS5wdXNoKG1lYWwubmFtZSkgOiBfLnB1bGwodGhpcy5vYk1lYWxzTmFtZSwgbWVhbC5uYW1lKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMubWVhbCA9IHRoaXMub2JNZWFsc1NlbGVjdGVkLmpvaW4oKTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIC8v0KLRg9GA0L7Qv9C10YDQsNGC0L7RgNGLINC00LvRjyDQt9Cw0L/RgNC+0YHQsFxyXG4gICAgICAgICAgICBvcGVyYXRvclNlbGVjdDogZnVuY3Rpb24gKG9wZXJhdG9yKSB7XHJcbiAgICAgICAgICAgICAgICAhXy5pbmNsdWRlcyh0aGlzLm9iT3BlcmF0b3JzU2VsZWN0ZWQsIG9wZXJhdG9yLmlkKSA/IHRoaXMub2JPcGVyYXRvcnNTZWxlY3RlZC5wdXNoKG9wZXJhdG9yLmlkKSA6IF8ucHVsbCh0aGlzLm9iT3BlcmF0b3JzU2VsZWN0ZWQsIG9wZXJhdG9yLmlkKTtcclxuICAgICAgICAgICAgICAgICFfLmluY2x1ZGVzKHRoaXMub2JPcGVyYXRvcnNOYW1lLCBvcGVyYXRvci5uYW1lKSA/IHRoaXMub2JPcGVyYXRvcnNOYW1lLnB1c2gob3BlcmF0b3IubmFtZSkgOiBfLnB1bGwodGhpcy5vYk9wZXJhdG9yc05hbWUsIG9wZXJhdG9yLm5hbWUpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vcGVyYXRvcnMgPSB0aGlzLm9iT3BlcmF0b3JzU2VsZWN0ZWQuam9pbigpO1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgLyrQpNC+0YDQvNC40YDQvtCy0L3QuNC1INGB0YLRgNC+0LrQuCDRgSDRgtGD0YDQuNGB0YLQsNC80LgqL1xyXG4gICAgICAgICAgICBzZXRUb3VyaXN0c1N0cmluZzogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgICAgIHNlbGYudG91cmlzdFN0cmluZyA9IHNlbGYuYWR1bHRzICsgXCIg0LLQt9GALiBcIjtcclxuICAgICAgICAgICAgICAgIGlmIChzZWxmLmNoaWxkID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGYudG91cmlzdFN0cmluZyArPSBzZWxmLmNoaWxkICsgXCIg0YDQtdCxLihcIjtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMTsgaSA8PSBzZWxmLmNoaWxkOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi50b3VyaXN0U3RyaW5nICs9IHNlbGZbJ2NoaWxkYWdlJyArIGldO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaSA8IHNlbGYuY2hpbGQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYudG91cmlzdFN0cmluZyArPSAnINC4ICdcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBzZWxmLnRvdXJpc3RTdHJpbmcgKz0gXCIpXCI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBmYXN0T3JkZXI6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZmFzdEZvcm1TZW5kZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGxldCBzZXNzaWR2YWwgPSAkKFwiI3Nlc3NpZFwiKS52YWwoKSxcclxuICAgICAgICAgICAgICAgICAgICB0b3VyaWQgPSAkKFwiaW5wdXRbbmFtZT1mYXN0VG91cklkXVwiKS52YWwoKSxcclxuICAgICAgICAgICAgICAgICAgICBjcmVkaXQgPSAkKFwiaW5wdXRbbmFtZT1jcmVkaXRdXCIpLnZhbCgpO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuZmFzdEZvcm1QaG9uZUhhc0Vycm9yID0gdGhpcy5mYXN0Rm9ybVBob25lLmxlbmd0aCA8PSAwO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mYXN0Rm9ybU5hbWVIYXNFcnJvciA9IHRoaXMuZmFzdEZvcm1OYW1lLmxlbmd0aCA8PSAwO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5mYXN0Rm9ybVBob25lSGFzRXJyb3IgJiYgIXRoaXMuZmFzdEZvcm1OYW1lSGFzRXJyb3IgJiYgdG91cmlkLmxlbmd0aCA+IDApIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kaHR0cC5nZXQoJy9sb2NhbC9tb2R1bGVzL25ld3RyYXZlbC5zZWFyY2gvbGliL2FqYXgucGhwJywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFwiZmFzdG9yZGVyXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b3VyaWQ6IHRvdXJpZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBob25lOiB0aGlzLmZhc3RGb3JtUGhvbmUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiB0aGlzLmZhc3RGb3JtTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlc3NpZDogc2Vzc2lkdmFsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3JlZGl0OiBjcmVkaXRcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuYm9keS5FUlJPUiA9PT0gXCJcIiAmJiByZXNwb25zZS5ib2R5LlNUQVRVUykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeWFDb3VudGVyMjQzOTU5MTEucmVhY2hHb2FsKCdGQVNUX09SREVSX0RFVEFJTF9UT1VSJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZhc3RGb3JtU2VuZGVkID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjZmFzdE9yZGVyTW9kYWwnKS5tb2RhbCgnaGlkZScpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgMTAwMClcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIC8v0JrQvtC80LLQtdGA0YLQuNGA0YPQtdC8INGB0LvQvtCy0LAg0LIg0LzQsNGB0YHQuNCyXHJcbiAgICAgICAgICAgIGFycmF5Q29udmVydDogZnVuY3Rpb24gKHF1ZXJ5KSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgYXJRdWVyeTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoXy5pc1N0cmluZyhxdWVyeSkpIHtcclxuICAgICAgICAgICAgICAgICAgICBhclF1ZXJ5ID0gW3F1ZXJ5XTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXJRdWVyeSA9IHF1ZXJ5O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGFyUXVlcnk7XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBjbG9zZURyb3Bkb3duOiBmdW5jdGlvbiAodGFyZ2V0KSB7XHJcbiAgICAgICAgICAgICAgICAkKCcjJyt0YXJnZXQpLmRyb3Bkb3duKCd0b2dnbGUnKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICB3YXRjaDoge1xyXG4gICAgICAgICAgICBkZXBhcnR1cmU6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0Q291bnRyaWVzKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFJlZ2lvbnMoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0SG90ZWxzKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldEZseWRhdGVzKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgQlguc2V0Q29va2llKCdORVdUUkFWRUxfVVNFUl9DSVRZJywgdGhpcy5kZXBhcnR1cmUsIHtwYXRoOiAnLyd9KTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgY291bnRyeTogZnVuY3Rpb24gKG5ld0NvdW50cnkpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0UmVnaW9ucygpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRIb3RlbHMoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0Rmx5ZGF0ZXMoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMub2JSZWdpb25zU2VsZWN0ZWQgPSB7fTtcclxuICAgICAgICAgICAgICAgIHRoaXMucmVnaW9uc05hbWUgPSBcIlwiO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZWdpb25zID0gXCJcIjtcclxuICAgICAgICAgICAgICAgIHRoaXMub2JTdWJSZWdpb25zU2VsZWN0ZWQgPSB7fTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc3VicmVnaW9ucyA9IFwiXCI7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN1YlJlZ2lvbnNOYW1lID0gXCJcIjtcclxuICAgICAgICAgICAgICAgIHRoaXMub2JIb3RlbHNTZWxlY3RlZCA9IHt9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ob3RlbHNOYW1lID0gXCJcIjtcclxuICAgICAgICAgICAgICAgIHRoaXMuaG90ZWxzID0gXCJcIjtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgcmVnaW9uczogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRIb3RlbHMoKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgc3RhcnM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0SG90ZWxzKCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHJhdGluZzogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRIb3RlbHMoKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgYWR1bHRzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFRvdXJpc3RzU3RyaW5nKCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGNoaWxkOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFRvdXJpc3RzU3RyaW5nKCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGNoaWxkYWdlMTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRUb3VyaXN0c1N0cmluZygpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBjaGlsZGFnZTI6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0VG91cmlzdHNTdHJpbmcoKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgY2hpbGRhZ2UzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFRvdXJpc3RzU3RyaW5nKCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgY29tcHV0ZWQ6IHtcclxuICAgICAgICAgICAgLy8gZ3JvdXBlZERlcGFydHVyZXM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgLy8gICAgIHJldHVybiBfLmdyb3VwQnkodGhpcy5vYkRlcGFydHVyZXMsIGZ1bmN0aW9uIChzKSB7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgcmV0dXJuIHMubmFtZS5jaGFyQXQoKTtcclxuICAgICAgICAgICAgLy8gICAgIH0pO1xyXG4gICAgICAgICAgICAvLyB9LFxyXG4gICAgICAgICAgICBncm91cGVkQ291bnRyaWVzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gXy5ncm91cEJ5KHRoaXMub2JDb3VudHJpZXMsIGZ1bmN0aW9uIChzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHMubmFtZS5jaGFyQXQoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBmaWx0ZXJlZEhvdGVsczogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgbGV0IHNlbGYgPSB0aGlzLFxyXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcmVkID0gW107XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHNlbGYub2JIb3RlbHMgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJlZCA9IHNlbGYub2JIb3RlbHMuZmlsdGVyKGZ1bmN0aW9uIChob3RlbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gaG90ZWwubmFtZS50b0xvd2VyQ2FzZSgpLmluZGV4T2Yoc2VsZi5ob3RlbFNlYXJjaFF1ZXJ5LnRvTG93ZXJDYXNlKCkpICE9PSAtMVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiBmaWx0ZXJlZC5zbGljZSgwLCAzMDApO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBmaWx0ZXJlZFJlZ2lvbnM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgICAgICAgICAgIHJldHVybiBzZWxmLm9iUmVnaW9uc0Rpc3BsYXkuZmlsdGVyKGZ1bmN0aW9uIChyZWdpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVnaW9uLm5hbWUudG9Mb3dlckNhc2UoKS5pbmRleE9mKHNlbGYucmVnaW9uU2VhcmNoUXVlcnkudG9Mb3dlckNhc2UoKSkgIT09IC0xXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBmaWx0ZXJlZENvdW50cmllczogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHNlbGYub2JDb3VudHJpZXMuZmlsdGVyKGZ1bmN0aW9uIChjb3VudHJ5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGNvdW50cnkubmFtZS50b0xvd2VyQ2FzZSgpLmluZGV4T2Yoc2VsZi5jb3VudHJ5U2VhcmNoUXVlcnkudG9Mb3dlckNhc2UoKSkgIT09IC0xXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZmlsdGVyczoge1xyXG4gICAgICAgICAgICBkaXNwbGF5QWdlOiBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIGlmICghdmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJcIjtcclxuICAgICAgICAgICAgICAgIH1lbHNlIGlmKHBhcnNlSW50KHZhbHVlKSA9PT0gMSl7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwiPCAyINC70LXRglwiO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IHRpdGxlcyA9IFsn0LPQvtC0JywgJ9Cz0L7QtNCwJywgJ9C70LXRgiddO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlID0gcGFyc2VJbnQodmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB2YWx1ZSArIFwiIFwiICsgdGl0bGVzWyh2YWx1ZSAlIDEwID09IDEgJiYgdmFsdWUgJSAxMDAgIT0gMTEgPyAwIDogdmFsdWUgJSAxMCA+PSAyICYmIHZhbHVlICUgMTAgPD0gNCAmJiAodmFsdWUgJSAxMDAgPCAxMCB8fCB2YWx1ZSAlIDEwMCA+PSAyMCkgPyAxIDogMildO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgZGlzcGxheUFnZUludDogZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAocGFyc2VJbnQodmFsdWUpID49IDIgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHZhbHVlO1xyXG4gICAgICAgICAgICAgICAgfWVsc2UgaWYocGFyc2VJbnQodmFsdWUpIDw9IDEpe1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBcIjFcIjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGRpc3BsYXlOaWdodHM6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCF2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgdGl0bGVzID0gWyfQvdC+0YfRjCcsICfQvdC+0YfQuCcsICfQvdC+0YfQtdC5J107XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSBwYXJzZUludCh2YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHZhbHVlICsgXCIgXCIgKyB0aXRsZXNbKHZhbHVlICUgMTAgPT0gMSAmJiB2YWx1ZSAlIDEwMCAhPSAxMSA/IDAgOiB2YWx1ZSAlIDEwID49IDIgJiYgdmFsdWUgJSAxMCA8PSA0ICYmICh2YWx1ZSAlIDEwMCA8IDEwIHx8IHZhbHVlICUgMTAwID49IDIwKSA/IDEgOiAyKV07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBzaG9ydE5hbWU6IGZ1bmN0aW9uICh2YWx1ZSwgdHlwZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHZhbHVlLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHRpdGxlcyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodHlwZSA9PSAncmVnaW9uJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGVzID0gWyfQutGD0YDQvtGA0YInLCAn0LrRg9GA0L7RgNGC0LAnLCAn0LrRg9GA0L7RgNGC0L7QsiddO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGUgPT0gJ2hvdGVsJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGVzID0gWyfQvtGC0LXQu9GMJywgJ9C+0YLQtdC70Y8nLCAn0L7RgtC10LvQtdC5J107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZSA9PSAnc3VicmVnaW9uJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGVzID0gWyfQv9C+0YHQtdC70L7QuicsICfQv9C+0YHQtdC70LrQsCcsICfQv9C+0YHQtdC70LrQvtCyJ107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlID0gdmFsdWUubGVuZ3RoO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWUgKyBcIiBcIiArIHRpdGxlc1sodmFsdWUgJSAxMCA9PSAxICYmIHZhbHVlICUgMTAwICE9IDExID8gMCA6IHZhbHVlICUgMTAgPj0gMiAmJiB2YWx1ZSAlIDEwIDw9IDQgJiYgKHZhbHVlICUgMTAwIDwgMTAgfHwgdmFsdWUgJSAxMDAgPj0gMjApID8gMSA6IDIpXTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh2YWx1ZS5sZW5ndGggPT0gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWUudG9TdHJpbmcoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICByYXRpbmdEaXNwbGF5ZWQ6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgbGV0IHJhdGluZyA9ICfQm9GO0LHQvtC5JztcclxuICAgICAgICAgICAgICAgIHN3aXRjaCAodmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIDU6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJhdGluZyA9ICfQvtGCICcgKyA0LjU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgNDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmF0aW5nID0gJ9C+0YIgJyArIDQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmF0aW5nID0gJ9C+0YIgJyArIDMuNTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAyOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByYXRpbmcgPSAn0L7RgiAnICsgMztcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAwOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByYXRpbmcgPSAn0JvRjtCx0L7QuSc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJhdGluZztcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGZvcm1hdFByaWNlOiBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB2YWx1ZS50b1N0cmluZygpLnJlcGxhY2UoL1xcQig/PShcXGR7M30pKyg/IVxcZCkpL2csIFwiIFwiKSArICcg0KAnO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGNvbXBvbmVudHM6IHtcclxuICAgICAgICAgICAgQ29tUmVzdWx0cyxcclxuICAgICAgICAgICAgQ29tUHJvZ3Jlc3NCYXIsXHJcbiAgICAgICAgICAgIENvbURlcGFydHVyZSxcclxuICAgICAgICAgICAgSG90ZWxEYXRlUGlja2VyXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH0pO1xyXG5cclxuICAgICQoJy5zZWxlY3QtZHJvcGRvd24ubm8tY2xvc2UnKS5jbGljayhmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgIH0pO1xyXG5cclxufSk7XHJcblxyXG5cclxuZnVuY3Rpb24gZ2V0QWxsVXJsUGFyYW1zKHVybCkge1xyXG5cclxuICAgIC8vINC40LfQstC70LXQutCw0LXQvCDRgdGC0YDQvtC60YMg0LjQtyBVUkwg0LjQu9C4INC+0LHRitC10LrRgtCwIHdpbmRvd1xyXG4gICAgbGV0IHF1ZXJ5U3RyaW5nID0gdXJsID8gdXJsLnNwbGl0KCc/JylbMV0gOiB3aW5kb3cubG9jYXRpb24uc2VhcmNoLnNsaWNlKDEpO1xyXG5cclxuICAgIC8vINC+0LHRitC10LrRgiDQtNC70Y8g0YXRgNCw0L3QtdC90LjRjyDQv9Cw0YDQsNC80LXRgtGA0L7QslxyXG4gICAgbGV0IG9iaiA9IHt9O1xyXG5cclxuICAgIC8vINC10YHQu9C4INC10YHRgtGMINGB0YLRgNC+0LrQsCDQt9Cw0L/RgNC+0YHQsFxyXG4gICAgaWYgKHF1ZXJ5U3RyaW5nKSB7XHJcblxyXG4gICAgICAgIC8vINC00LDQvdC90YvQtSDQv9C+0YHQu9C1INC30L3QsNC60LAgIyDQsdGD0LTRg9GCINC+0L/Rg9GJ0LXQvdGLXHJcbiAgICAgICAgcXVlcnlTdHJpbmcgPSBxdWVyeVN0cmluZy5zcGxpdCgnIycpWzBdO1xyXG5cclxuICAgICAgICAvLyDRgNCw0LfQtNC10LvRj9C10Lwg0L/QsNGA0LDQvNC10YLRgNGLXHJcbiAgICAgICAgbGV0IGFyciA9IHF1ZXJ5U3RyaW5nLnNwbGl0KCcmJyk7XHJcblxyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgYXJyLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIC8vINGA0LDQt9C00LXQu9GP0LXQvCDQv9Cw0YDQsNC80LXRgtGAINC90LAg0LrQu9GO0YcgPT4g0LfQvdCw0YfQtdC90LjQtVxyXG4gICAgICAgICAgICBsZXQgYSA9IGFycltpXS5zcGxpdCgnPScpO1xyXG5cclxuICAgICAgICAgICAgLy8g0L7QsdGA0LDQsdC+0YLQutCwINC00LDQvdC90YvRhSDQstC40LTQsDogbGlzdFtdPXRoaW5nMSZsaXN0W109dGhpbmcyXHJcbiAgICAgICAgICAgIGxldCBwYXJhbU51bSA9IHVuZGVmaW5lZDtcclxuICAgICAgICAgICAgbGV0IHBhcmFtTmFtZSA9IGFbMF0ucmVwbGFjZSgvXFxbXFxkKlxcXS8sIGZ1bmN0aW9uICh2KSB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbU51bSA9IHYuc2xpY2UoMSwgLTEpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vINC/0LXRgNC10LTQsNGH0LAg0LfQvdCw0YfQtdC90LjRjyDQv9Cw0YDQsNC80LXRgtGA0LAgKCd0cnVlJyDQtdGB0LvQuCDQt9C90LDRh9C10L3QuNC1INC90LUg0LfQsNC00LDQvdC+KVxyXG4gICAgICAgICAgICBsZXQgcGFyYW1WYWx1ZSA9IHR5cGVvZihhWzFdKSA9PT0gJ3VuZGVmaW5lZCcgPyB0cnVlIDogYVsxXTtcclxuXHJcbiAgICAgICAgICAgIC8vINC/0YDQtdC+0LHRgNCw0LfQvtCy0LDQvdC40LUg0YDQtdCz0LjRgdGC0YDQsFxyXG4gICAgICAgICAgICBwYXJhbU5hbWUgPSBwYXJhbU5hbWUudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICAgICAgcGFyYW1WYWx1ZSA9IHBhcmFtVmFsdWUudG9Mb3dlckNhc2UoKTtcclxuXHJcbiAgICAgICAgICAgIC8vINC10YHQu9C4INC60LvRjtGHINC/0LDRgNCw0LzQtdGC0YDQsCDRg9C20LUg0LfQsNC00LDQvVxyXG4gICAgICAgICAgICBpZiAob2JqW3BhcmFtTmFtZV0pIHtcclxuICAgICAgICAgICAgICAgIC8vINC/0YDQtdC+0LHRgNCw0LfRg9C10Lwg0YLQtdC60YPRidC10LUg0LfQvdCw0YfQtdC90LjQtSDQsiDQvNCw0YHRgdC40LJcclxuICAgICAgICAgICAgICAgIGlmICh0eXBlb2Ygb2JqW3BhcmFtTmFtZV0gPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JqW3BhcmFtTmFtZV0gPSBbb2JqW3BhcmFtTmFtZV1dO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8g0LXRgdC70Lgg0L3QtSDQt9Cw0LTQsNC9INC40L3QtNC10LrRgS4uLlxyXG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBwYXJhbU51bSA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyDQv9C+0LzQtdGJ0LDQtdC8INC30L3QsNGH0LXQvdC40LUg0LIg0LrQvtC90LXRhiDQvNCw0YHRgdC40LLQsFxyXG4gICAgICAgICAgICAgICAgICAgIG9ialtwYXJhbU5hbWVdLnB1c2gocGFyYW1WYWx1ZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAvLyDQtdGB0LvQuCDQuNC90LTQtdC60YEg0LfQsNC00LDQvS4uLlxyXG4gICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8g0YDQsNC30LzQtdGJ0LDQtdC8INGN0LvQtdC80LXQvdGCINC/0L4g0LfQsNC00LDQvdC90L7QvNGDINC40L3QtNC10LrRgdGDXHJcbiAgICAgICAgICAgICAgICAgICAgb2JqW3BhcmFtTmFtZV1bcGFyYW1OdW1dID0gcGFyYW1WYWx1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyDQtdGB0LvQuCDQv9Cw0YDQsNC80LXRgtGAINC90LUg0LfQsNC00LDQvSwg0LTQtdC70LDQtdC8INGN0YLQviDQstGA0YPRh9C90YPRjlxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIG9ialtwYXJhbU5hbWVdID0gcGFyYW1WYWx1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gb2JqO1xyXG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9zZWFyY2gtZm9ybS5qcyIsInZhciBnO1xyXG5cclxuLy8gVGhpcyB3b3JrcyBpbiBub24tc3RyaWN0IG1vZGVcclxuZyA9IChmdW5jdGlvbigpIHtcclxuXHRyZXR1cm4gdGhpcztcclxufSkoKTtcclxuXHJcbnRyeSB7XHJcblx0Ly8gVGhpcyB3b3JrcyBpZiBldmFsIGlzIGFsbG93ZWQgKHNlZSBDU1ApXHJcblx0ZyA9IGcgfHwgRnVuY3Rpb24oXCJyZXR1cm4gdGhpc1wiKSgpIHx8ICgxLGV2YWwpKFwidGhpc1wiKTtcclxufSBjYXRjaChlKSB7XHJcblx0Ly8gVGhpcyB3b3JrcyBpZiB0aGUgd2luZG93IHJlZmVyZW5jZSBpcyBhdmFpbGFibGVcclxuXHRpZih0eXBlb2Ygd2luZG93ID09PSBcIm9iamVjdFwiKVxyXG5cdFx0ZyA9IHdpbmRvdztcclxufVxyXG5cclxuLy8gZyBjYW4gc3RpbGwgYmUgdW5kZWZpbmVkLCBidXQgbm90aGluZyB0byBkbyBhYm91dCBpdC4uLlxyXG4vLyBXZSByZXR1cm4gdW5kZWZpbmVkLCBpbnN0ZWFkIG9mIG5vdGhpbmcgaGVyZSwgc28gaXQnc1xyXG4vLyBlYXNpZXIgdG8gaGFuZGxlIHRoaXMgY2FzZS4gaWYoIWdsb2JhbCkgeyAuLi59XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IGc7XHJcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vICh3ZWJwYWNrKS9idWlsZGluL2dsb2JhbC5qc1xuLy8gbW9kdWxlIGlkID0gNVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vY3NzLWxvYWRlci9pbmRleC5qcyEuL3YtY2FsZW5kYXIubWluLmNzc1wiKTtcblxuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5cbnZhciB0cmFuc2Zvcm07XG52YXIgaW5zZXJ0SW50bztcblxuXG5cbnZhciBvcHRpb25zID0ge1wiaG1yXCI6dHJ1ZX1cblxub3B0aW9ucy50cmFuc2Zvcm0gPSB0cmFuc2Zvcm1cbm9wdGlvbnMuaW5zZXJ0SW50byA9IHVuZGVmaW5lZDtcblxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXMuanNcIikoY29udGVudCwgb3B0aW9ucyk7XG5cbmlmKGNvbnRlbnQubG9jYWxzKSBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzO1xuXG5pZihtb2R1bGUuaG90KSB7XG5cdG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi9jc3MtbG9hZGVyL2luZGV4LmpzIS4vdi1jYWxlbmRhci5taW4uY3NzXCIsIGZ1bmN0aW9uKCkge1xuXHRcdHZhciBuZXdDb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vY3NzLWxvYWRlci9pbmRleC5qcyEuL3YtY2FsZW5kYXIubWluLmNzc1wiKTtcblxuXHRcdGlmKHR5cGVvZiBuZXdDb250ZW50ID09PSAnc3RyaW5nJykgbmV3Q29udGVudCA9IFtbbW9kdWxlLmlkLCBuZXdDb250ZW50LCAnJ11dO1xuXG5cdFx0dmFyIGxvY2FscyA9IChmdW5jdGlvbihhLCBiKSB7XG5cdFx0XHR2YXIga2V5LCBpZHggPSAwO1xuXG5cdFx0XHRmb3Ioa2V5IGluIGEpIHtcblx0XHRcdFx0aWYoIWIgfHwgYVtrZXldICE9PSBiW2tleV0pIHJldHVybiBmYWxzZTtcblx0XHRcdFx0aWR4Kys7XG5cdFx0XHR9XG5cblx0XHRcdGZvcihrZXkgaW4gYikgaWR4LS07XG5cblx0XHRcdHJldHVybiBpZHggPT09IDA7XG5cdFx0fShjb250ZW50LmxvY2FscywgbmV3Q29udGVudC5sb2NhbHMpKTtcblxuXHRcdGlmKCFsb2NhbHMpIHRocm93IG5ldyBFcnJvcignQWJvcnRpbmcgQ1NTIEhNUiBkdWUgdG8gY2hhbmdlZCBjc3MtbW9kdWxlcyBsb2NhbHMuJyk7XG5cblx0XHR1cGRhdGUobmV3Q29udGVudCk7XG5cdH0pO1xuXG5cdG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgdXBkYXRlKCk7IH0pO1xufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3YtY2FsZW5kYXIvbGliL3YtY2FsZW5kYXIubWluLmNzc1xuLy8gbW9kdWxlIGlkID0gNlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikoZmFsc2UpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiQC13ZWJraXQta2V5ZnJhbWVzIHNjYWxlRW50ZXItZGF0YS12LWJjNTUwMjRjezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCguNykgc2NhbGVZKC43KTt0cmFuc2Zvcm06c2NhbGVYKC43KSBzY2FsZVkoLjcpO29wYWNpdHk6LjN9OTAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjEpIHNjYWxlWSgxLjEpO3RyYW5zZm9ybTpzY2FsZVgoMS4xKSBzY2FsZVkoMS4xKX05NSV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKC45NSkgc2NhbGVZKC45NSk7dHJhbnNmb3JtOnNjYWxlWCguOTUpIHNjYWxlWSguOTUpfXRvey13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSk7dHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSk7b3BhY2l0eToxfX1Aa2V5ZnJhbWVzIHNjYWxlRW50ZXItZGF0YS12LWJjNTUwMjRjezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCguNykgc2NhbGVZKC43KTt0cmFuc2Zvcm06c2NhbGVYKC43KSBzY2FsZVkoLjcpO29wYWNpdHk6LjN9OTAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjEpIHNjYWxlWSgxLjEpO3RyYW5zZm9ybTpzY2FsZVgoMS4xKSBzY2FsZVkoMS4xKX05NSV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKC45NSkgc2NhbGVZKC45NSk7dHJhbnNmb3JtOnNjYWxlWCguOTUpIHNjYWxlWSguOTUpfXRvey13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSk7dHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSk7b3BhY2l0eToxfX1ALXdlYmtpdC1rZXlmcmFtZXMgc2NhbGVMZWF2ZS1kYXRhLXYtYmM1NTAyNGN7MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEpIHNjYWxlWSgxKTt0cmFuc2Zvcm06c2NhbGVYKDEpIHNjYWxlWSgxKX02MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMTgpIHNjYWxlWSgxLjE4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMTgpIHNjYWxlWSgxLjE4KTtvcGFjaXR5Oi4yfXRvey13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjE1KSBzY2FsZVkoMS4xOCk7dHJhbnNmb3JtOnNjYWxlWCgxLjE1KSBzY2FsZVkoMS4xOCk7b3BhY2l0eTowfX1Aa2V5ZnJhbWVzIHNjYWxlTGVhdmUtZGF0YS12LWJjNTUwMjRjezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSk7dHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSl9NjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjE4KSBzY2FsZVkoMS4xOCk7dHJhbnNmb3JtOnNjYWxlWCgxLjE4KSBzY2FsZVkoMS4xOCk7b3BhY2l0eTouMn10b3std2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xNSkgc2NhbGVZKDEuMTgpO3RyYW5zZm9ybTpzY2FsZVgoMS4xNSkgc2NhbGVZKDEuMTgpO29wYWNpdHk6MH19QC13ZWJraXQta2V5ZnJhbWVzIHNsaWRlUmlnaHRTY2FsZUVudGVyLWRhdGEtdi1iYzU1MDI0Y3swJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMCk7dHJhbnNmb3JtOnNjYWxlWCgwKX02MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMDgpO3RyYW5zZm9ybTpzY2FsZVgoMS4wOCl9fUBrZXlmcmFtZXMgc2xpZGVSaWdodFNjYWxlRW50ZXItZGF0YS12LWJjNTUwMjRjezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgwKTt0cmFuc2Zvcm06c2NhbGVYKDApfTYwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4wOCk7dHJhbnNmb3JtOnNjYWxlWCgxLjA4KX19QC13ZWJraXQta2V5ZnJhbWVzIHNsaWRlUmlnaHRUcmFuc2xhdGVFbnRlci1kYXRhLXYtYmM1NTAyNGN7MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtNnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtNnB4KX02MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgycHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKDJweCl9fUBrZXlmcmFtZXMgc2xpZGVSaWdodFRyYW5zbGF0ZUVudGVyLWRhdGEtdi1iYzU1MDI0Y3swJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC02cHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC02cHgpfTYwJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKDJweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoMnB4KX19QC13ZWJraXQta2V5ZnJhbWVzIHNsaWRlTGVmdFNjYWxlRW50ZXItZGF0YS12LWJjNTUwMjRjezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgwKTt0cmFuc2Zvcm06c2NhbGVYKDApfTYwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4wOCk7dHJhbnNmb3JtOnNjYWxlWCgxLjA4KX19QGtleWZyYW1lcyBzbGlkZUxlZnRTY2FsZUVudGVyLWRhdGEtdi1iYzU1MDI0Y3swJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMCk7dHJhbnNmb3JtOnNjYWxlWCgwKX02MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMDgpO3RyYW5zZm9ybTpzY2FsZVgoMS4wOCl9fUAtd2Via2l0LWtleWZyYW1lcyBzbGlkZUxlZnRUcmFuc2xhdGVFbnRlci1kYXRhLXYtYmM1NTAyNGN7MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCg2cHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKDZweCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTJweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTJweCl9fUBrZXlmcmFtZXMgc2xpZGVMZWZ0VHJhbnNsYXRlRW50ZXItZGF0YS12LWJjNTUwMjRjezAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoNnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCg2cHgpfTYwJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC0ycHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC0ycHgpfX0uYy1wYW5lLWNvbnRhaW5lcltkYXRhLXYtYmM1NTAyNGNdey1tcy1mbGV4LW5lZ2F0aXZlOjE7ZmxleC1zaHJpbms6MTtkaXNwbGF5Oi13ZWJraXQtaW5saW5lLWJveDtkaXNwbGF5Oi1tcy1pbmxpbmUtZmxleGJveDtkaXNwbGF5OmlubGluZS1mbGV4O2ZvbnQtZmFtaWx5OkJsaW5rTWFjU3lzdGVtRm9udCwtYXBwbGUtc3lzdGVtLFNlZ29lIFVJLFJvYm90byxPeHlnZW4sVWJ1bnR1LENhbnRhcmVsbCxGaXJhIFNhbnMsRHJvaWQgU2FucyxIZWx2ZXRpY2EgTmV1ZSxIZWx2ZXRpY2EsQXJpYWwsc2Fucy1zZXJpZjtmb250LXdlaWdodDo0MDA7bGluZS1oZWlnaHQ6MS41O2NvbG9yOiMzOTNkNDY7LXdlYmtpdC1mb250LXNtb290aGluZzphbnRpYWxpYXNlZDstbW96LW9zeC1mb250LXNtb290aGluZzpncmF5c2NhbGU7Ym94LXNpemluZzpib3JkZXItYm94fS5jLXBhbmUtY29udGFpbmVyLmlzLWV4cGFuZGVkW2RhdGEtdi1iYzU1MDI0Y117d2lkdGg6MTAwJX0uYy1wYW5lLWNvbnRhaW5lci5pcy12ZXJ0aWNhbFtkYXRhLXYtYmM1NTAyNGNdey13ZWJraXQtYm94LW9yaWVudDp2ZXJ0aWNhbDstd2Via2l0LWJveC1kaXJlY3Rpb246bm9ybWFsOy1tcy1mbGV4LWRpcmVjdGlvbjpjb2x1bW47ZmxleC1kaXJlY3Rpb246Y29sdW1ufS5jLXBhbmUtY29udGFpbmVyW2RhdGEtdi1iYzU1MDI0Y10gKntib3gtc2l6aW5nOmluaGVyaXR9LmMtcGFuZS1jb250YWluZXJbZGF0YS12LWJjNTUwMjRjXSA6Zm9jdXN7b3V0bGluZTpub25lfS5jLXBhbmUtZGl2aWRlcltkYXRhLXYtYmM1NTAyNGNde3dpZHRoOjFweDtib3JkZXI6MXB4IGluc2V0O2JvcmRlci1jb2xvcjojZmFmYWZhfUAtd2Via2l0LWtleWZyYW1lcyBzY2FsZUVudGVyLWRhdGEtdi0yMDgzY2I3MnswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoLjcpIHNjYWxlWSguNyk7dHJhbnNmb3JtOnNjYWxlWCguNykgc2NhbGVZKC43KTtvcGFjaXR5Oi4zfTkwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xKSBzY2FsZVkoMS4xKTt0cmFuc2Zvcm06c2NhbGVYKDEuMSkgc2NhbGVZKDEuMSl9OTUley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCguOTUpIHNjYWxlWSguOTUpO3RyYW5zZm9ybTpzY2FsZVgoLjk1KSBzY2FsZVkoLjk1KX10b3std2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO3RyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO29wYWNpdHk6MX19QC13ZWJraXQta2V5ZnJhbWVzIHNjYWxlTGVhdmUtZGF0YS12LTIwODNjYjcyezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSk7dHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSl9NjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjE4KSBzY2FsZVkoMS4xOCk7dHJhbnNmb3JtOnNjYWxlWCgxLjE4KSBzY2FsZVkoMS4xOCk7b3BhY2l0eTouMn10b3std2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xNSkgc2NhbGVZKDEuMTgpO3RyYW5zZm9ybTpzY2FsZVgoMS4xNSkgc2NhbGVZKDEuMTgpO29wYWNpdHk6MH19QC13ZWJraXQta2V5ZnJhbWVzIHNsaWRlUmlnaHRTY2FsZUVudGVyLWRhdGEtdi0yMDgzY2I3MnswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMCk7dHJhbnNmb3JtOnNjYWxlWCgwKX02MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMDgpO3RyYW5zZm9ybTpzY2FsZVgoMS4wOCl9fUAtd2Via2l0LWtleWZyYW1lcyBzbGlkZVJpZ2h0VHJhbnNsYXRlRW50ZXItZGF0YS12LTIwODNjYjcyezAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTZweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTZweCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoMnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgycHgpfX1ALXdlYmtpdC1rZXlmcmFtZXMgc2xpZGVMZWZ0U2NhbGVFbnRlci1kYXRhLXYtMjA4M2NiNzJ7MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDApO3RyYW5zZm9ybTpzY2FsZVgoMCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjA4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMDgpfX1ALXdlYmtpdC1rZXlmcmFtZXMgc2xpZGVMZWZ0VHJhbnNsYXRlRW50ZXItZGF0YS12LTIwODNjYjcyezAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoNnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCg2cHgpfTYwJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC0ycHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC0ycHgpfX1Aa2V5ZnJhbWVzIHNjYWxlRW50ZXItZGF0YS12LTIwODNjYjcyezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCguNykgc2NhbGVZKC43KTt0cmFuc2Zvcm06c2NhbGVYKC43KSBzY2FsZVkoLjcpO29wYWNpdHk6LjN9OTAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjEpIHNjYWxlWSgxLjEpO3RyYW5zZm9ybTpzY2FsZVgoMS4xKSBzY2FsZVkoMS4xKX05NSV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKC45NSkgc2NhbGVZKC45NSk7dHJhbnNmb3JtOnNjYWxlWCguOTUpIHNjYWxlWSguOTUpfXRvey13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSk7dHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSk7b3BhY2l0eToxfX1Aa2V5ZnJhbWVzIHNjYWxlTGVhdmUtZGF0YS12LTIwODNjYjcyezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSk7dHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSl9NjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjE4KSBzY2FsZVkoMS4xOCk7dHJhbnNmb3JtOnNjYWxlWCgxLjE4KSBzY2FsZVkoMS4xOCk7b3BhY2l0eTouMn10b3std2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xNSkgc2NhbGVZKDEuMTgpO3RyYW5zZm9ybTpzY2FsZVgoMS4xNSkgc2NhbGVZKDEuMTgpO29wYWNpdHk6MH19QGtleWZyYW1lcyBzbGlkZVJpZ2h0U2NhbGVFbnRlci1kYXRhLXYtMjA4M2NiNzJ7MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDApO3RyYW5zZm9ybTpzY2FsZVgoMCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjA4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMDgpfX1Aa2V5ZnJhbWVzIHNsaWRlUmlnaHRUcmFuc2xhdGVFbnRlci1kYXRhLXYtMjA4M2NiNzJ7MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtNnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtNnB4KX02MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgycHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKDJweCl9fUBrZXlmcmFtZXMgc2xpZGVMZWZ0U2NhbGVFbnRlci1kYXRhLXYtMjA4M2NiNzJ7MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDApO3RyYW5zZm9ybTpzY2FsZVgoMCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjA4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMDgpfX1Aa2V5ZnJhbWVzIHNsaWRlTGVmdFRyYW5zbGF0ZUVudGVyLWRhdGEtdi0yMDgzY2I3MnswJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKDZweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoNnB4KX02MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtMnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtMnB4KX19LmMtcGFuZVtkYXRhLXYtMjA4M2NiNzJdey13ZWJraXQtYm94LWZsZXg6MTstbXMtZmxleC1wb3NpdGl2ZToxO2ZsZXgtZ3JvdzoxOy1tcy1mbGV4LW5lZ2F0aXZlOjE7ZmxleC1zaHJpbms6MTtkaXNwbGF5Oi13ZWJraXQtYm94O2Rpc3BsYXk6LW1zLWZsZXhib3g7ZGlzcGxheTpmbGV4Oy13ZWJraXQtYm94LW9yaWVudDp2ZXJ0aWNhbDstd2Via2l0LWJveC1kaXJlY3Rpb246bm9ybWFsOy1tcy1mbGV4LWRpcmVjdGlvbjpjb2x1bW47ZmxleC1kaXJlY3Rpb246Y29sdW1uOy13ZWJraXQtYm94LXBhY2s6Y2VudGVyOy1tcy1mbGV4LXBhY2s6Y2VudGVyO2p1c3RpZnktY29udGVudDpjZW50ZXI7LXdlYmtpdC1ib3gtYWxpZ246c3RyZXRjaDstbXMtZmxleC1hbGlnbjpzdHJldGNoO2FsaWduLWl0ZW1zOnN0cmV0Y2h9LmMtaG9yaXpvbnRhbC1kaXZpZGVyW2RhdGEtdi0yMDgzY2I3Ml17LW1zLWZsZXgtaXRlbS1hbGlnbjpjZW50ZXI7YWxpZ24tc2VsZjpjZW50ZXJ9LmMtaGVhZGVyW2RhdGEtdi0yMDgzY2I3Ml17ZGlzcGxheTotd2Via2l0LWJveDtkaXNwbGF5Oi1tcy1mbGV4Ym94O2Rpc3BsYXk6ZmxleDstd2Via2l0LWJveC1hbGlnbjpzdHJldGNoOy1tcy1mbGV4LWFsaWduOnN0cmV0Y2g7YWxpZ24taXRlbXM6c3RyZXRjaDstd2Via2l0LXVzZXItc2VsZWN0Om5vbmU7LW1vei11c2VyLXNlbGVjdDpub25lOy1tcy11c2VyLXNlbGVjdDpub25lO3VzZXItc2VsZWN0Om5vbmU7cGFkZGluZzoxMHB4fS5jLWhlYWRlciAuYy1hcnJvdy1sYXlvdXRbZGF0YS12LTIwODNjYjcyXXttaW4td2lkdGg6MjZweH0uYy1oZWFkZXIgLmMtYXJyb3ctbGF5b3V0IC5jLWFycm93W2RhdGEtdi0yMDgzY2I3Ml0sLmMtaGVhZGVyIC5jLWFycm93LWxheW91dFtkYXRhLXYtMjA4M2NiNzJde2Rpc3BsYXk6LXdlYmtpdC1ib3g7ZGlzcGxheTotbXMtZmxleGJveDtkaXNwbGF5OmZsZXg7LXdlYmtpdC1ib3gtcGFjazpjZW50ZXI7LW1zLWZsZXgtcGFjazpjZW50ZXI7anVzdGlmeS1jb250ZW50OmNlbnRlcjstd2Via2l0LWJveC1hbGlnbjpjZW50ZXI7LW1zLWZsZXgtYWxpZ246Y2VudGVyO2FsaWduLWl0ZW1zOmNlbnRlcjttYXJnaW46MDtwYWRkaW5nOjB9LmMtaGVhZGVyIC5jLWFycm93LWxheW91dCAuYy1hcnJvd1tkYXRhLXYtMjA4M2NiNzJde2ZvbnQtc2l6ZToxLjZyZW07dHJhbnNpdGlvbjpmaWxsLW9wYWNpdHkgLjNzIGVhc2UtaW4tb3V0O2N1cnNvcjpwb2ludGVyOy13ZWJraXQtdXNlci1zZWxlY3Q6bm9uZTstbW96LXVzZXItc2VsZWN0Om5vbmU7LW1zLXVzZXItc2VsZWN0Om5vbmU7dXNlci1zZWxlY3Q6bm9uZX0uYy1oZWFkZXIgLmMtYXJyb3ctbGF5b3V0IC5jLWFycm93W2RhdGEtdi0yMDgzY2I3Ml06aG92ZXJ7ZmlsbC1vcGFjaXR5Oi41fS5jLWhlYWRlciAuYy10aXRsZS1sYXlvdXRbZGF0YS12LTIwODNjYjcyXXtkaXNwbGF5Oi13ZWJraXQtaW5saW5lLWJveDtkaXNwbGF5Oi1tcy1pbmxpbmUtZmxleGJveDtkaXNwbGF5OmlubGluZS1mbGV4Oy13ZWJraXQtYm94LXBhY2s6Y2VudGVyOy1tcy1mbGV4LXBhY2s6Y2VudGVyO2p1c3RpZnktY29udGVudDpjZW50ZXI7LXdlYmtpdC1ib3gtYWxpZ246Y2VudGVyOy1tcy1mbGV4LWFsaWduOmNlbnRlcjthbGlnbi1pdGVtczpjZW50ZXI7LXdlYmtpdC1ib3gtZmxleDoxOy1tcy1mbGV4LXBvc2l0aXZlOjE7ZmxleC1ncm93OjF9LmMtaGVhZGVyIC5jLXRpdGxlLWxheW91dCAuYy10aXRsZS1wb3BvdmVyIC5jLXRpdGxlLWFuY2hvcltkYXRhLXYtMjA4M2NiNzJdLC5jLWhlYWRlciAuYy10aXRsZS1sYXlvdXQgLmMtdGl0bGUtcG9wb3ZlcltkYXRhLXYtMjA4M2NiNzJde2Rpc3BsYXk6LXdlYmtpdC1ib3g7ZGlzcGxheTotbXMtZmxleGJveDtkaXNwbGF5OmZsZXg7LXdlYmtpdC1ib3gtcGFjazppbmhlcml0Oy1tcy1mbGV4LXBhY2s6aW5oZXJpdDtqdXN0aWZ5LWNvbnRlbnQ6aW5oZXJpdH0uYy1oZWFkZXIgLmMtdGl0bGUtbGF5b3V0IC5jLXRpdGxlLXBvcG92ZXIgLmMtdGl0bGUtYW5jaG9yIC5jLXRpdGxlW2RhdGEtdi0yMDgzY2I3Ml17Zm9udC13ZWlnaHQ6NDAwO2ZvbnQtc2l6ZToxLjE1cmVtO2N1cnNvcjpwb2ludGVyOy13ZWJraXQtdXNlci1zZWxlY3Q6bm9uZTstbW96LXVzZXItc2VsZWN0Om5vbmU7LW1zLXVzZXItc2VsZWN0Om5vbmU7dXNlci1zZWxlY3Q6bm9uZTt3aGl0ZS1zcGFjZTpub3dyYXB9LmMtaGVhZGVyIC5jLXRpdGxlLWxheW91dC5hbGlnbi1sZWZ0W2RhdGEtdi0yMDgzY2I3Ml17LXdlYmtpdC1ib3gtb3JkaW5hbC1ncm91cDowOy1tcy1mbGV4LW9yZGVyOi0xO29yZGVyOi0xOy13ZWJraXQtYm94LXBhY2s6c3RhcnQ7LW1zLWZsZXgtcGFjazpzdGFydDtqdXN0aWZ5LWNvbnRlbnQ6ZmxleC1zdGFydH0uYy1oZWFkZXIgLmMtdGl0bGUtbGF5b3V0LmFsaWduLXJpZ2h0W2RhdGEtdi0yMDgzY2I3Ml17LXdlYmtpdC1ib3gtb3JkaW5hbC1ncm91cDoyOy1tcy1mbGV4LW9yZGVyOjE7b3JkZXI6MTstd2Via2l0LWJveC1wYWNrOmVuZDstbXMtZmxleC1wYWNrOmVuZDtqdXN0aWZ5LWNvbnRlbnQ6ZmxleC1lbmR9LmMtaGVhZGVyIC5jLWFycm93LmMtZGlzYWJsZWRbZGF0YS12LTIwODNjYjcyXXtjdXJzb3I6bm90LWFsbG93ZWQ7cG9pbnRlci1ldmVudHM6bm9uZTtvcGFjaXR5Oi4yfS5jLXdlZWtkYXlzW2RhdGEtdi0yMDgzY2I3Ml17ZGlzcGxheTotd2Via2l0LWJveDtkaXNwbGF5Oi1tcy1mbGV4Ym94O2Rpc3BsYXk6ZmxleDtwYWRkaW5nOjAgNXB4O2NvbG9yOiM5NDk5YTg7Zm9udC1zaXplOi45cmVtO2ZvbnQtd2VpZ2h0OjUwMH0uYy13ZWVrZGF5W2RhdGEtdi0yMDgzY2I3Ml17ZGlzcGxheTotd2Via2l0LWJveDtkaXNwbGF5Oi1tcy1mbGV4Ym94O2Rpc3BsYXk6ZmxleDstd2Via2l0LWJveC1wYWNrOmNlbnRlcjstbXMtZmxleC1wYWNrOmNlbnRlcjtqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyOy13ZWJraXQtYm94LWFsaWduOmNlbnRlcjstbXMtZmxleC1hbGlnbjpjZW50ZXI7YWxpZ24taXRlbXM6Y2VudGVyO21hcmdpbjowO3BhZGRpbmc6MDstd2Via2l0LWJveC1mbGV4OjE7LW1zLWZsZXg6MTtmbGV4OjE7Y3Vyc29yOmRlZmF1bHR9LmMtd2Vla3NbZGF0YS12LTIwODNjYjcyXXstd2Via2l0LWJveC1mbGV4OjE7LW1zLWZsZXgtcG9zaXRpdmU6MTtmbGV4LWdyb3c6MTtwYWRkaW5nOjVweCA1cHggN3B4fS5jLXdlZWtzLXJvd3Mtd3JhcHBlcltkYXRhLXYtMjA4M2NiNzJde3Bvc2l0aW9uOnJlbGF0aXZlfS5jLXdlZWtzLXJvd3NbZGF0YS12LTIwODNjYjcyXXtkaXNwbGF5Oi13ZWJraXQtYm94O2Rpc3BsYXk6LW1zLWZsZXhib3g7ZGlzcGxheTpmbGV4Oy13ZWJraXQtYm94LW9yaWVudDp2ZXJ0aWNhbDstd2Via2l0LWJveC1kaXJlY3Rpb246bm9ybWFsOy1tcy1mbGV4LWRpcmVjdGlvbjpjb2x1bW47ZmxleC1kaXJlY3Rpb246Y29sdW1uO3dpZHRoOjEwMCV9LnRpdGxlLWZhZGUtZW50ZXItYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml0sLnRpdGxlLWZhZGUtbGVhdmUtYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml0sLnRpdGxlLXNsaWRlLWRvd24tZW50ZXItYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml0sLnRpdGxlLXNsaWRlLWRvd24tbGVhdmUtYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml0sLnRpdGxlLXNsaWRlLWxlZnQtZW50ZXItYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml0sLnRpdGxlLXNsaWRlLWxlZnQtbGVhdmUtYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml0sLnRpdGxlLXNsaWRlLXJpZ2h0LWVudGVyLWFjdGl2ZVtkYXRhLXYtMjA4M2NiNzJdLC50aXRsZS1zbGlkZS1yaWdodC1sZWF2ZS1hY3RpdmVbZGF0YS12LTIwODNjYjcyXSwudGl0bGUtc2xpZGUtdXAtZW50ZXItYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml0sLnRpdGxlLXNsaWRlLXVwLWxlYXZlLWFjdGl2ZVtkYXRhLXYtMjA4M2NiNzJde3RyYW5zaXRpb246YWxsIC4yNXMgZWFzZS1pbi1vdXR9LnRpdGxlLWZhZGUtbGVhdmUtYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml0sLnRpdGxlLW5vbmUtbGVhdmUtYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml0sLnRpdGxlLXNsaWRlLWRvd24tbGVhdmUtYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml0sLnRpdGxlLXNsaWRlLWxlZnQtbGVhdmUtYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml0sLnRpdGxlLXNsaWRlLXJpZ2h0LWxlYXZlLWFjdGl2ZVtkYXRhLXYtMjA4M2NiNzJdLC50aXRsZS1zbGlkZS11cC1sZWF2ZS1hY3RpdmVbZGF0YS12LTIwODNjYjcyXXtwb3NpdGlvbjphYnNvbHV0ZX0udGl0bGUtbm9uZS1lbnRlci1hY3RpdmVbZGF0YS12LTIwODNjYjcyXSwudGl0bGUtbm9uZS1sZWF2ZS1hY3RpdmVbZGF0YS12LTIwODNjYjcyXXt0cmFuc2l0aW9uLWR1cmF0aW9uOjBzfS50aXRsZS1zbGlkZS1sZWZ0LWVudGVyW2RhdGEtdi0yMDgzY2I3Ml0sLnRpdGxlLXNsaWRlLXJpZ2h0LWxlYXZlLXRvW2RhdGEtdi0yMDgzY2I3Ml17b3BhY2l0eTowOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoMjVweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoMjVweCl9LnRpdGxlLXNsaWRlLWxlZnQtbGVhdmUtdG9bZGF0YS12LTIwODNjYjcyXSwudGl0bGUtc2xpZGUtcmlnaHQtZW50ZXJbZGF0YS12LTIwODNjYjcyXXtvcGFjaXR5OjA7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtMjVweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTI1cHgpfS50aXRsZS1zbGlkZS1kb3duLWxlYXZlLXRvW2RhdGEtdi0yMDgzY2I3Ml0sLnRpdGxlLXNsaWRlLXVwLWVudGVyW2RhdGEtdi0yMDgzY2I3Ml17b3BhY2l0eTowOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoMjBweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoMjBweCl9LnRpdGxlLXNsaWRlLWRvd24tZW50ZXJbZGF0YS12LTIwODNjYjcyXSwudGl0bGUtc2xpZGUtdXAtbGVhdmUtdG9bZGF0YS12LTIwODNjYjcyXXtvcGFjaXR5OjA7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgtMjBweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTIwcHgpfS53ZWVrcy1mYWRlLWVudGVyLWFjdGl2ZVtkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1mYWRlLWxlYXZlLWFjdGl2ZVtkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1zbGlkZS1kb3duLWVudGVyLWFjdGl2ZVtkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1zbGlkZS1kb3duLWxlYXZlLWFjdGl2ZVtkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1zbGlkZS1sZWZ0LWVudGVyLWFjdGl2ZVtkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1zbGlkZS1sZWZ0LWxlYXZlLWFjdGl2ZVtkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1zbGlkZS1yaWdodC1lbnRlci1hY3RpdmVbZGF0YS12LTIwODNjYjcyXSwud2Vla3Mtc2xpZGUtcmlnaHQtbGVhdmUtYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml0sLndlZWtzLXNsaWRlLXVwLWVudGVyLWFjdGl2ZVtkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1zbGlkZS11cC1sZWF2ZS1hY3RpdmVbZGF0YS12LTIwODNjYjcyXXt0cmFuc2l0aW9uOmFsbCAuMjVzIGVhc2UtaW4tb3V0fS53ZWVrcy1mYWRlLWxlYXZlLWFjdGl2ZVtkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1ub25lLWxlYXZlLWFjdGl2ZVtkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1zbGlkZS1kb3duLWxlYXZlLWFjdGl2ZVtkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1zbGlkZS1sZWZ0LWxlYXZlLWFjdGl2ZVtkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1zbGlkZS1yaWdodC1sZWF2ZS1hY3RpdmVbZGF0YS12LTIwODNjYjcyXSwud2Vla3Mtc2xpZGUtdXAtbGVhdmUtYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml17cG9zaXRpb246YWJzb2x1dGV9LndlZWtzLW5vbmUtZW50ZXItYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml0sLndlZWtzLW5vbmUtbGVhdmUtYWN0aXZlW2RhdGEtdi0yMDgzY2I3Ml17dHJhbnNpdGlvbi1kdXJhdGlvbjowc30ud2Vla3Mtc2xpZGUtbGVmdC1lbnRlcltkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1zbGlkZS1yaWdodC1sZWF2ZS10b1tkYXRhLXYtMjA4M2NiNzJde29wYWNpdHk6MDstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKDIwcHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKDIwcHgpfS53ZWVrcy1zbGlkZS1sZWZ0LWxlYXZlLXRvW2RhdGEtdi0yMDgzY2I3Ml0sLndlZWtzLXNsaWRlLXJpZ2h0LWVudGVyW2RhdGEtdi0yMDgzY2I3Ml17b3BhY2l0eTowOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTIwcHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC0yMHB4KX0ud2Vla3Mtc2xpZGUtZG93bi1sZWF2ZS10b1tkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1zbGlkZS11cC1lbnRlcltkYXRhLXYtMjA4M2NiNzJde29wYWNpdHk6MDstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKDIwcHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKDIwcHgpfS53ZWVrcy1zbGlkZS1kb3duLWVudGVyW2RhdGEtdi0yMDgzY2I3Ml0sLndlZWtzLXNsaWRlLXVwLWxlYXZlLXRvW2RhdGEtdi0yMDgzY2I3Ml17b3BhY2l0eTowOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTIwcHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKC0yMHB4KX0udGl0bGUtZmFkZS1lbnRlcltkYXRhLXYtMjA4M2NiNzJdLC50aXRsZS1mYWRlLWxlYXZlLXRvW2RhdGEtdi0yMDgzY2I3Ml0sLnRpdGxlLW5vbmUtZW50ZXJbZGF0YS12LTIwODNjYjcyXSwudGl0bGUtbm9uZS1sZWF2ZS10b1tkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1mYWRlLWVudGVyW2RhdGEtdi0yMDgzY2I3Ml0sLndlZWtzLWZhZGUtbGVhdmUtdG9bZGF0YS12LTIwODNjYjcyXSwud2Vla3Mtbm9uZS1lbnRlcltkYXRhLXYtMjA4M2NiNzJdLC53ZWVrcy1ub25lLWxlYXZlLXRvW2RhdGEtdi0yMDgzY2I3Ml17b3BhY2l0eTowfUAtd2Via2l0LWtleWZyYW1lcyBzY2FsZUVudGVyLWRhdGEtdi0xYWQyNDM2ZnswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoLjcpIHNjYWxlWSguNyk7dHJhbnNmb3JtOnNjYWxlWCguNykgc2NhbGVZKC43KTtvcGFjaXR5Oi4zfTkwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xKSBzY2FsZVkoMS4xKTt0cmFuc2Zvcm06c2NhbGVYKDEuMSkgc2NhbGVZKDEuMSl9OTUley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCguOTUpIHNjYWxlWSguOTUpO3RyYW5zZm9ybTpzY2FsZVgoLjk1KSBzY2FsZVkoLjk1KX10b3std2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO3RyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO29wYWNpdHk6MX19QGtleWZyYW1lcyBzY2FsZUVudGVyLWRhdGEtdi0xYWQyNDM2ZnswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoLjcpIHNjYWxlWSguNyk7dHJhbnNmb3JtOnNjYWxlWCguNykgc2NhbGVZKC43KTtvcGFjaXR5Oi4zfTkwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xKSBzY2FsZVkoMS4xKTt0cmFuc2Zvcm06c2NhbGVYKDEuMSkgc2NhbGVZKDEuMSl9OTUley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCguOTUpIHNjYWxlWSguOTUpO3RyYW5zZm9ybTpzY2FsZVgoLjk1KSBzY2FsZVkoLjk1KX10b3std2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO3RyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO29wYWNpdHk6MX19QC13ZWJraXQta2V5ZnJhbWVzIHNjYWxlTGVhdmUtZGF0YS12LTFhZDI0MzZmezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSk7dHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSl9NjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjE4KSBzY2FsZVkoMS4xOCk7dHJhbnNmb3JtOnNjYWxlWCgxLjE4KSBzY2FsZVkoMS4xOCk7b3BhY2l0eTouMn10b3std2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xNSkgc2NhbGVZKDEuMTgpO3RyYW5zZm9ybTpzY2FsZVgoMS4xNSkgc2NhbGVZKDEuMTgpO29wYWNpdHk6MH19QGtleWZyYW1lcyBzY2FsZUxlYXZlLWRhdGEtdi0xYWQyNDM2ZnswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO3RyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpfTYwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xOCkgc2NhbGVZKDEuMTgpO3RyYW5zZm9ybTpzY2FsZVgoMS4xOCkgc2NhbGVZKDEuMTgpO29wYWNpdHk6LjJ9dG97LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMTUpIHNjYWxlWSgxLjE4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMTUpIHNjYWxlWSgxLjE4KTtvcGFjaXR5OjB9fUAtd2Via2l0LWtleWZyYW1lcyBzbGlkZVJpZ2h0U2NhbGVFbnRlci1kYXRhLXYtMWFkMjQzNmZ7MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDApO3RyYW5zZm9ybTpzY2FsZVgoMCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjA4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMDgpfX1Aa2V5ZnJhbWVzIHNsaWRlUmlnaHRTY2FsZUVudGVyLWRhdGEtdi0xYWQyNDM2ZnswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMCk7dHJhbnNmb3JtOnNjYWxlWCgwKX02MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMDgpO3RyYW5zZm9ybTpzY2FsZVgoMS4wOCl9fUAtd2Via2l0LWtleWZyYW1lcyBzbGlkZVJpZ2h0VHJhbnNsYXRlRW50ZXItZGF0YS12LTFhZDI0MzZmezAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTZweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTZweCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoMnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgycHgpfX1Aa2V5ZnJhbWVzIHNsaWRlUmlnaHRUcmFuc2xhdGVFbnRlci1kYXRhLXYtMWFkMjQzNmZ7MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtNnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtNnB4KX02MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgycHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKDJweCl9fUAtd2Via2l0LWtleWZyYW1lcyBzbGlkZUxlZnRTY2FsZUVudGVyLWRhdGEtdi0xYWQyNDM2ZnswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMCk7dHJhbnNmb3JtOnNjYWxlWCgwKX02MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMDgpO3RyYW5zZm9ybTpzY2FsZVgoMS4wOCl9fUBrZXlmcmFtZXMgc2xpZGVMZWZ0U2NhbGVFbnRlci1kYXRhLXYtMWFkMjQzNmZ7MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDApO3RyYW5zZm9ybTpzY2FsZVgoMCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjA4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMDgpfX1ALXdlYmtpdC1rZXlmcmFtZXMgc2xpZGVMZWZ0VHJhbnNsYXRlRW50ZXItZGF0YS12LTFhZDI0MzZmezAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoNnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCg2cHgpfTYwJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC0ycHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC0ycHgpfX1Aa2V5ZnJhbWVzIHNsaWRlTGVmdFRyYW5zbGF0ZUVudGVyLWRhdGEtdi0xYWQyNDM2ZnswJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKDZweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoNnB4KX02MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtMnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtMnB4KX19LnBvcG92ZXItY29udGFpbmVyW2RhdGEtdi0xYWQyNDM2Zl17cG9zaXRpb246cmVsYXRpdmU7b3V0bGluZTpub25lfS5wb3BvdmVyLWNvbnRhaW5lci5leHBhbmRlZFtkYXRhLXYtMWFkMjQzNmZde2Rpc3BsYXk6YmxvY2t9LnBvcG92ZXItb3JpZ2luW2RhdGEtdi0xYWQyNDM2Zl17cG9zaXRpb246YWJzb2x1dGU7LXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOnRvcCBjZW50ZXI7dHJhbnNmb3JtLW9yaWdpbjp0b3AgY2VudGVyO3otaW5kZXg6MTA7cG9pbnRlci1ldmVudHM6bm9uZX0ucG9wb3Zlci1vcmlnaW4uZGlyZWN0aW9uLXRvcFtkYXRhLXYtMWFkMjQzNmZde2JvdHRvbToxMDAlfS5wb3BvdmVyLW9yaWdpbi5kaXJlY3Rpb24tYm90dG9tW2RhdGEtdi0xYWQyNDM2Zl17dG9wOjEwMCV9LnBvcG92ZXItb3JpZ2luLmRpcmVjdGlvbi1sZWZ0W2RhdGEtdi0xYWQyNDM2Zl17dG9wOjA7cmlnaHQ6MTAwJX0ucG9wb3Zlci1vcmlnaW4uZGlyZWN0aW9uLXJpZ2h0W2RhdGEtdi0xYWQyNDM2Zl17dG9wOjA7bGVmdDoxMDAlfS5wb3BvdmVyLW9yaWdpbi5kaXJlY3Rpb24tYm90dG9tLmFsaWduLWxlZnRbZGF0YS12LTFhZDI0MzZmXSwucG9wb3Zlci1vcmlnaW4uZGlyZWN0aW9uLXRvcC5hbGlnbi1sZWZ0W2RhdGEtdi0xYWQyNDM2Zl17bGVmdDowfS5wb3BvdmVyLW9yaWdpbi5kaXJlY3Rpb24tYm90dG9tLmFsaWduLWNlbnRlcltkYXRhLXYtMWFkMjQzNmZdLC5wb3BvdmVyLW9yaWdpbi5kaXJlY3Rpb24tdG9wLmFsaWduLWNlbnRlcltkYXRhLXYtMWFkMjQzNmZde2xlZnQ6NTAlOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTUwJSk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTUwJSl9LnBvcG92ZXItb3JpZ2luLmRpcmVjdGlvbi1ib3R0b20uYWxpZ24tcmlnaHRbZGF0YS12LTFhZDI0MzZmXSwucG9wb3Zlci1vcmlnaW4uZGlyZWN0aW9uLXRvcC5hbGlnbi1yaWdodFtkYXRhLXYtMWFkMjQzNmZde3JpZ2h0OjB9LnBvcG92ZXItb3JpZ2luLmRpcmVjdGlvbi1sZWZ0LmFsaWduLXRvcFtkYXRhLXYtMWFkMjQzNmZdLC5wb3BvdmVyLW9yaWdpbi5kaXJlY3Rpb24tcmlnaHQuYWxpZ24tdG9wW2RhdGEtdi0xYWQyNDM2Zl17dG9wOjB9LnBvcG92ZXItb3JpZ2luLmRpcmVjdGlvbi1sZWZ0LmFsaWduLW1pZGRsZVtkYXRhLXYtMWFkMjQzNmZdLC5wb3BvdmVyLW9yaWdpbi5kaXJlY3Rpb24tcmlnaHQuYWxpZ24tbWlkZGxlW2RhdGEtdi0xYWQyNDM2Zl17dG9wOjUwJTstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKC01MCUpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKC01MCUpfS5wb3BvdmVyLW9yaWdpbi5kaXJlY3Rpb24tbGVmdC5hbGlnbi1ib3R0b21bZGF0YS12LTFhZDI0MzZmXSwucG9wb3Zlci1vcmlnaW4uZGlyZWN0aW9uLXJpZ2h0LmFsaWduLWJvdHRvbVtkYXRhLXYtMWFkMjQzNmZde3RvcDphdXRvO2JvdHRvbTowfS5wb3BvdmVyLW9yaWdpbiAucG9wb3Zlci1jb250ZW50LXdyYXBwZXJbZGF0YS12LTFhZDI0MzZmXXtwb3NpdGlvbjpyZWxhdGl2ZTtvdXRsaW5lOm5vbmV9LnBvcG92ZXItb3JpZ2luIC5wb3BvdmVyLWNvbnRlbnQtd3JhcHBlci5pbnRlcmFjdGl2ZVtkYXRhLXYtMWFkMjQzNmZde3BvaW50ZXItZXZlbnRzOmFsbH0ucG9wb3Zlci1vcmlnaW4gLnBvcG92ZXItY29udGVudC13cmFwcGVyIC5wb3BvdmVyLWNvbnRlbnRbZGF0YS12LTFhZDI0MzZmXXtwb3NpdGlvbjpyZWxhdGl2ZTtiYWNrZ3JvdW5kLWNvbG9yOiNmYWZhZmE7Ym9yZGVyOjFweCBzb2xpZCByZ2JhKDM0LDM2LDM4LC4xNSk7Ym9yZGVyLXJhZGl1czo1cHg7Ym94LXNoYWRvdzowIDFweCAycHggMCByZ2JhKDM0LDM2LDM4LC4xNSk7cGFkZGluZzo0cHh9LnBvcG92ZXItb3JpZ2luIC5wb3BvdmVyLWNvbnRlbnQtd3JhcHBlciAucG9wb3Zlci1jb250ZW50W2RhdGEtdi0xYWQyNDM2Zl06YWZ0ZXJ7ZGlzcGxheTpibG9jaztwb3NpdGlvbjphYnNvbHV0ZTtiYWNrZ3JvdW5kOmluaGVyaXQ7Ym9yZGVyOmluaGVyaXQ7Ym9yZGVyLXdpZHRoOjFweCAxcHggMCAwO3dpZHRoOjEycHg7aGVpZ2h0OjEycHg7Y29udGVudDpcXFwiXFxcIn0ucG9wb3Zlci1vcmlnaW4gLnBvcG92ZXItY29udGVudC13cmFwcGVyIC5wb3BvdmVyLWNvbnRlbnQuZGlyZWN0aW9uLWJvdHRvbVtkYXRhLXYtMWFkMjQzNmZdOmFmdGVye3RvcDowO2JvcmRlci13aWR0aDoxcHggMXB4IDAgMH0ucG9wb3Zlci1vcmlnaW4gLnBvcG92ZXItY29udGVudC13cmFwcGVyIC5wb3BvdmVyLWNvbnRlbnQuZGlyZWN0aW9uLXRvcFtkYXRhLXYtMWFkMjQzNmZdOmFmdGVye3RvcDoxMDAlO2JvcmRlci13aWR0aDowIDAgMXB4IDFweH0ucG9wb3Zlci1vcmlnaW4gLnBvcG92ZXItY29udGVudC13cmFwcGVyIC5wb3BvdmVyLWNvbnRlbnQuZGlyZWN0aW9uLWxlZnRbZGF0YS12LTFhZDI0MzZmXTphZnRlcntsZWZ0OjEwMCU7Ym9yZGVyLXdpZHRoOjAgMXB4IDFweCAwfS5wb3BvdmVyLW9yaWdpbiAucG9wb3Zlci1jb250ZW50LXdyYXBwZXIgLnBvcG92ZXItY29udGVudC5kaXJlY3Rpb24tcmlnaHRbZGF0YS12LTFhZDI0MzZmXTphZnRlcntsZWZ0OjA7Ym9yZGVyLXdpZHRoOjFweCAwIDAgMXB4fS5wb3BvdmVyLW9yaWdpbiAucG9wb3Zlci1jb250ZW50LXdyYXBwZXIgLnBvcG92ZXItY29udGVudC5hbGlnbi1sZWZ0W2RhdGEtdi0xYWQyNDM2Zl06YWZ0ZXJ7bGVmdDoyMHB4Oy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTUwJSkgdHJhbnNsYXRlWCgtNTAlKSByb3RhdGUoLTQ1ZGVnKTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgtNTAlKSB0cmFuc2xhdGVYKC01MCUpIHJvdGF0ZSgtNDVkZWcpfS5wb3BvdmVyLW9yaWdpbiAucG9wb3Zlci1jb250ZW50LXdyYXBwZXIgLnBvcG92ZXItY29udGVudC5hbGlnbi1yaWdodFtkYXRhLXYtMWFkMjQzNmZdOmFmdGVye3JpZ2h0OjIwcHg7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgtNTAlKSB0cmFuc2xhdGVYKDUwJSkgcm90YXRlKC00NWRlZyk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTUwJSkgdHJhbnNsYXRlWCg1MCUpIHJvdGF0ZSgtNDVkZWcpfS5wb3BvdmVyLW9yaWdpbiAucG9wb3Zlci1jb250ZW50LXdyYXBwZXIgLnBvcG92ZXItY29udGVudC5hbGlnbi1jZW50ZXJbZGF0YS12LTFhZDI0MzZmXTphZnRlcntsZWZ0OjUwJTstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKC01MCUpIHRyYW5zbGF0ZVgoLTUwJSkgcm90YXRlKC00NWRlZyk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTUwJSkgdHJhbnNsYXRlWCgtNTAlKSByb3RhdGUoLTQ1ZGVnKX0ucG9wb3Zlci1vcmlnaW4gLnBvcG92ZXItY29udGVudC13cmFwcGVyIC5wb3BvdmVyLWNvbnRlbnQuYWxpZ24tdG9wW2RhdGEtdi0xYWQyNDM2Zl06YWZ0ZXJ7dG9wOjE4cHg7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgtNTAlKSB0cmFuc2xhdGVYKC01MCUpIHJvdGF0ZSgtNDVkZWcpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKC01MCUpIHRyYW5zbGF0ZVgoLTUwJSkgcm90YXRlKC00NWRlZyl9LnBvcG92ZXItb3JpZ2luIC5wb3BvdmVyLWNvbnRlbnQtd3JhcHBlciAucG9wb3Zlci1jb250ZW50LmFsaWduLW1pZGRsZVtkYXRhLXYtMWFkMjQzNmZdOmFmdGVye3RvcDo1MCU7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgtNTAlKSB0cmFuc2xhdGVYKC01MCUpIHJvdGF0ZSgtNDVkZWcpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKC01MCUpIHRyYW5zbGF0ZVgoLTUwJSkgcm90YXRlKC00NWRlZyl9LnBvcG92ZXItb3JpZ2luIC5wb3BvdmVyLWNvbnRlbnQtd3JhcHBlciAucG9wb3Zlci1jb250ZW50LmFsaWduLWJvdHRvbVtkYXRhLXYtMWFkMjQzNmZdOmFmdGVye2JvdHRvbToxOHB4Oy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoNTAlKSB0cmFuc2xhdGVYKC01MCUpIHJvdGF0ZSgtNDVkZWcpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKDUwJSkgdHJhbnNsYXRlWCgtNTAlKSByb3RhdGUoLTQ1ZGVnKX0uZmFkZS1lbnRlci1hY3RpdmVbZGF0YS12LTFhZDI0MzZmXSwuZmFkZS1sZWF2ZS1hY3RpdmVbZGF0YS12LTFhZDI0MzZmXSwuc2xpZGUtZmFkZS1lbnRlci1hY3RpdmVbZGF0YS12LTFhZDI0MzZmXSwuc2xpZGUtZmFkZS1sZWF2ZS1hY3RpdmVbZGF0YS12LTFhZDI0MzZmXXt0cmFuc2l0aW9uOmFsbCAuMTRzIGVhc2UtaW4tb3V0fS5mYWRlLWVudGVyW2RhdGEtdi0xYWQyNDM2Zl0sLmZhZGUtbGVhdmUtdG9bZGF0YS12LTFhZDI0MzZmXSwuc2xpZGUtZmFkZS1lbnRlcltkYXRhLXYtMWFkMjQzNmZdLC5zbGlkZS1mYWRlLWxlYXZlLXRvW2RhdGEtdi0xYWQyNDM2Zl17b3BhY2l0eTowfS5zbGlkZS1mYWRlLWVudGVyLmRpcmVjdGlvbi1ib3R0b21bZGF0YS12LTFhZDI0MzZmXSwuc2xpZGUtZmFkZS1sZWF2ZS10by5kaXJlY3Rpb24tYm90dG9tW2RhdGEtdi0xYWQyNDM2Zl17LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgtMTVweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTE1cHgpfS5zbGlkZS1mYWRlLWVudGVyLmRpcmVjdGlvbi10b3BbZGF0YS12LTFhZDI0MzZmXSwuc2xpZGUtZmFkZS1sZWF2ZS10by5kaXJlY3Rpb24tdG9wW2RhdGEtdi0xYWQyNDM2Zl17LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgxNXB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgxNXB4KX0uc2xpZGUtZmFkZS1lbnRlci5kaXJlY3Rpb24tbGVmdFtkYXRhLXYtMWFkMjQzNmZdLC5zbGlkZS1mYWRlLWxlYXZlLXRvLmRpcmVjdGlvbi1sZWZ0W2RhdGEtdi0xYWQyNDM2Zl17LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgxNXB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgxNXB4KX0uc2xpZGUtZmFkZS1lbnRlci5kaXJlY3Rpb24tcmlnaHRbZGF0YS12LTFhZDI0MzZmXSwuc2xpZGUtZmFkZS1sZWF2ZS10by5kaXJlY3Rpb24tcmlnaHRbZGF0YS12LTFhZDI0MzZmXXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC0xNXB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtMTVweCl9LmMtd2Vla1tkYXRhLXYtMjg4OTY1NDJdey13ZWJraXQtYm94LWZsZXg6MTstbXMtZmxleC1wb3NpdGl2ZToxO2ZsZXgtZ3JvdzoxO2Rpc3BsYXk6LXdlYmtpdC1ib3g7ZGlzcGxheTotbXMtZmxleGJveDtkaXNwbGF5OmZsZXh9QC13ZWJraXQta2V5ZnJhbWVzIHNjYWxlRW50ZXItZGF0YS12LTNkYjgwZjgwezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCguNykgc2NhbGVZKC43KTt0cmFuc2Zvcm06c2NhbGVYKC43KSBzY2FsZVkoLjcpO29wYWNpdHk6LjN9OTAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjEpIHNjYWxlWSgxLjEpO3RyYW5zZm9ybTpzY2FsZVgoMS4xKSBzY2FsZVkoMS4xKX05NSV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKC45NSkgc2NhbGVZKC45NSk7dHJhbnNmb3JtOnNjYWxlWCguOTUpIHNjYWxlWSguOTUpfXRvey13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSk7dHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSk7b3BhY2l0eToxfX1ALXdlYmtpdC1rZXlmcmFtZXMgc2NhbGVMZWF2ZS1kYXRhLXYtM2RiODBmODB7MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEpIHNjYWxlWSgxKTt0cmFuc2Zvcm06c2NhbGVYKDEpIHNjYWxlWSgxKX02MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMTgpIHNjYWxlWSgxLjE4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMTgpIHNjYWxlWSgxLjE4KTtvcGFjaXR5Oi4yfXRvey13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjE1KSBzY2FsZVkoMS4xOCk7dHJhbnNmb3JtOnNjYWxlWCgxLjE1KSBzY2FsZVkoMS4xOCk7b3BhY2l0eTowfX1ALXdlYmtpdC1rZXlmcmFtZXMgc2xpZGVSaWdodFNjYWxlRW50ZXItZGF0YS12LTNkYjgwZjgwezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgwKTt0cmFuc2Zvcm06c2NhbGVYKDApfTYwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4wOCk7dHJhbnNmb3JtOnNjYWxlWCgxLjA4KX19QC13ZWJraXQta2V5ZnJhbWVzIHNsaWRlUmlnaHRUcmFuc2xhdGVFbnRlci1kYXRhLXYtM2RiODBmODB7MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtNnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtNnB4KX02MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgycHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKDJweCl9fUAtd2Via2l0LWtleWZyYW1lcyBzbGlkZUxlZnRTY2FsZUVudGVyLWRhdGEtdi0zZGI4MGY4MHswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMCk7dHJhbnNmb3JtOnNjYWxlWCgwKX02MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMDgpO3RyYW5zZm9ybTpzY2FsZVgoMS4wOCl9fUAtd2Via2l0LWtleWZyYW1lcyBzbGlkZUxlZnRUcmFuc2xhdGVFbnRlci1kYXRhLXYtM2RiODBmODB7MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCg2cHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKDZweCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTJweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTJweCl9fUBrZXlmcmFtZXMgc2NhbGVFbnRlci1kYXRhLXYtM2RiODBmODB7MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKC43KSBzY2FsZVkoLjcpO3RyYW5zZm9ybTpzY2FsZVgoLjcpIHNjYWxlWSguNyk7b3BhY2l0eTouM305MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMSkgc2NhbGVZKDEuMSk7dHJhbnNmb3JtOnNjYWxlWCgxLjEpIHNjYWxlWSgxLjEpfTk1JXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoLjk1KSBzY2FsZVkoLjk1KTt0cmFuc2Zvcm06c2NhbGVYKC45NSkgc2NhbGVZKC45NSl9dG97LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEpIHNjYWxlWSgxKTt0cmFuc2Zvcm06c2NhbGVYKDEpIHNjYWxlWSgxKTtvcGFjaXR5OjF9fUBrZXlmcmFtZXMgc2NhbGVMZWF2ZS1kYXRhLXYtM2RiODBmODB7MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEpIHNjYWxlWSgxKTt0cmFuc2Zvcm06c2NhbGVYKDEpIHNjYWxlWSgxKX02MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMTgpIHNjYWxlWSgxLjE4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMTgpIHNjYWxlWSgxLjE4KTtvcGFjaXR5Oi4yfXRvey13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjE1KSBzY2FsZVkoMS4xOCk7dHJhbnNmb3JtOnNjYWxlWCgxLjE1KSBzY2FsZVkoMS4xOCk7b3BhY2l0eTowfX1Aa2V5ZnJhbWVzIHNsaWRlUmlnaHRTY2FsZUVudGVyLWRhdGEtdi0zZGI4MGY4MHswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMCk7dHJhbnNmb3JtOnNjYWxlWCgwKX02MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMDgpO3RyYW5zZm9ybTpzY2FsZVgoMS4wOCl9fUBrZXlmcmFtZXMgc2xpZGVSaWdodFRyYW5zbGF0ZUVudGVyLWRhdGEtdi0zZGI4MGY4MHswJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC02cHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC02cHgpfTYwJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKDJweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoMnB4KX19QGtleWZyYW1lcyBzbGlkZUxlZnRTY2FsZUVudGVyLWRhdGEtdi0zZGI4MGY4MHswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMCk7dHJhbnNmb3JtOnNjYWxlWCgwKX02MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMDgpO3RyYW5zZm9ybTpzY2FsZVgoMS4wOCl9fUBrZXlmcmFtZXMgc2xpZGVMZWZ0VHJhbnNsYXRlRW50ZXItZGF0YS12LTNkYjgwZjgwezAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoNnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCg2cHgpfTYwJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC0ycHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC0ycHgpfX0uYy1kYXktcG9wb3ZlcltkYXRhLXYtM2RiODBmODBdey13ZWJraXQtYm94LWZsZXg6MTstbXMtZmxleDoxO2ZsZXg6MX0uYy1kYXlbZGF0YS12LTNkYjgwZjgwXXtwb3NpdGlvbjpyZWxhdGl2ZTttaW4taGVpZ2h0OjI4cHg7ei1pbmRleDoxfS5jLWRheS1sYXllcltkYXRhLXYtM2RiODBmODBde3Bvc2l0aW9uOmFic29sdXRlO2xlZnQ6MDtyaWdodDowO3RvcDowO2JvdHRvbTowO3BvaW50ZXItZXZlbnRzOm5vbmV9LmMtZGF5LWJveC1jZW50ZXItY2VudGVyW2RhdGEtdi0zZGI4MGY4MF17LXdlYmtpdC1ib3gtcGFjazpjZW50ZXI7LW1zLWZsZXgtcGFjazpjZW50ZXI7anVzdGlmeS1jb250ZW50OmNlbnRlcjstd2Via2l0LXRyYW5zZm9ybS1vcmlnaW46NTAlIDUwJTt0cmFuc2Zvcm0tb3JpZ2luOjUwJSA1MCV9LmMtZGF5LWJveC1jZW50ZXItY2VudGVyW2RhdGEtdi0zZGI4MGY4MF0sLmMtZGF5LWJveC1sZWZ0LWNlbnRlcltkYXRhLXYtM2RiODBmODBde2Rpc3BsYXk6LXdlYmtpdC1ib3g7ZGlzcGxheTotbXMtZmxleGJveDtkaXNwbGF5OmZsZXg7LXdlYmtpdC1ib3gtYWxpZ246Y2VudGVyOy1tcy1mbGV4LWFsaWduOmNlbnRlcjthbGlnbi1pdGVtczpjZW50ZXI7bWFyZ2luOjA7cGFkZGluZzowO2hlaWdodDoxMDAlfS5jLWRheS1ib3gtbGVmdC1jZW50ZXJbZGF0YS12LTNkYjgwZjgwXXstd2Via2l0LWJveC1wYWNrOnN0YXJ0Oy1tcy1mbGV4LXBhY2s6c3RhcnQ7anVzdGlmeS1jb250ZW50OmZsZXgtc3RhcnQ7LXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOjAgNTAlO3RyYW5zZm9ybS1vcmlnaW46MCA1MCV9LmMtZGF5LWJveC1yaWdodC1jZW50ZXJbZGF0YS12LTNkYjgwZjgwXXtkaXNwbGF5Oi13ZWJraXQtYm94O2Rpc3BsYXk6LW1zLWZsZXhib3g7ZGlzcGxheTpmbGV4Oy13ZWJraXQtYm94LXBhY2s6ZW5kOy1tcy1mbGV4LXBhY2s6ZW5kO2p1c3RpZnktY29udGVudDpmbGV4LWVuZDstd2Via2l0LWJveC1hbGlnbjpjZW50ZXI7LW1zLWZsZXgtYWxpZ246Y2VudGVyO2FsaWduLWl0ZW1zOmNlbnRlcjttYXJnaW46MDtwYWRkaW5nOjA7aGVpZ2h0OjEwMCU7LXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOjEwMCUgNTAlO3RyYW5zZm9ybS1vcmlnaW46MTAwJSA1MCV9LmMtZGF5LWJveC1jZW50ZXItYm90dG9tW2RhdGEtdi0zZGI4MGY4MF17LXdlYmtpdC1ib3gtYWxpZ246ZW5kOy1tcy1mbGV4LWFsaWduOmVuZDthbGlnbi1pdGVtczpmbGV4LWVuZDttYXJnaW46MDtwYWRkaW5nOjB9LmMtZGF5LWJveC1jZW50ZXItYm90dG9tW2RhdGEtdi0zZGI4MGY4MF0sLmMtZGF5LWNvbnRlbnQtd3JhcHBlcltkYXRhLXYtM2RiODBmODBde2Rpc3BsYXk6LXdlYmtpdC1ib3g7ZGlzcGxheTotbXMtZmxleGJveDtkaXNwbGF5OmZsZXg7LXdlYmtpdC1ib3gtcGFjazpjZW50ZXI7LW1zLWZsZXgtcGFjazpjZW50ZXI7anVzdGlmeS1jb250ZW50OmNlbnRlcn0uYy1kYXktY29udGVudC13cmFwcGVyW2RhdGEtdi0zZGI4MGY4MF17LXdlYmtpdC1ib3gtYWxpZ246Y2VudGVyOy1tcy1mbGV4LWFsaWduOmNlbnRlcjthbGlnbi1pdGVtczpjZW50ZXI7cG9pbnRlci1ldmVudHM6YWxsOy13ZWJraXQtdXNlci1zZWxlY3Q6bm9uZTstbW96LXVzZXItc2VsZWN0Om5vbmU7LW1zLXVzZXItc2VsZWN0Om5vbmU7dXNlci1zZWxlY3Q6bm9uZTtjdXJzb3I6ZGVmYXVsdH0uYy1kYXktY29udGVudFtkYXRhLXYtM2RiODBmODBde2Rpc3BsYXk6LXdlYmtpdC1ib3g7ZGlzcGxheTotbXMtZmxleGJveDtkaXNwbGF5OmZsZXg7LXdlYmtpdC1ib3gtcGFjazpjZW50ZXI7LW1zLWZsZXgtcGFjazpjZW50ZXI7anVzdGlmeS1jb250ZW50OmNlbnRlcjstd2Via2l0LWJveC1hbGlnbjpjZW50ZXI7LW1zLWZsZXgtYWxpZ246Y2VudGVyO2FsaWduLWl0ZW1zOmNlbnRlcjt3aWR0aDoxLjhyZW07aGVpZ2h0OjEuOHJlbTtmb250LXNpemU6LjlyZW07Zm9udC13ZWlnaHQ6NDAwO2xpbmUtaGVpZ2h0OjE7Ym9yZGVyLXJhZGl1czo1MCU7dHJhbnNpdGlvbjphbGwgLjE4cyBlYXNlLWluLW91dDttYXJnaW46LjFyZW0gLjA4cmVtfS5jLWRheS1iYWNrZ3JvdW5kc1tkYXRhLXYtM2RiODBmODBde292ZXJmbG93OmhpZGRlbjtwb2ludGVyLWV2ZW50czpub25lO3otaW5kZXg6LTE7LXdlYmtpdC1iYWNrZmFjZS12aXNpYmlsaXR5OmhpZGRlbjtiYWNrZmFjZS12aXNpYmlsaXR5OmhpZGRlbn0uYy1kYXktYmFja2dyb3VuZFtkYXRhLXYtM2RiODBmODBde3RyYW5zaXRpb246aGVpZ2h0IC4xM3MgZWFzZS1pbi1vdXQsYmFja2dyb3VuZC1jb2xvciAuMTNzIGVhc2UtaW4tb3V0fS5zaGlmdC1sZWZ0W2RhdGEtdi0zZGI4MGY4MF17bWFyZ2luLWxlZnQ6LTFweH0uc2hpZnQtcmlnaHRbZGF0YS12LTNkYjgwZjgwXXttYXJnaW4tcmlnaHQ6LTFweH0uc2hpZnQtbGVmdC1yaWdodFtkYXRhLXYtM2RiODBmODBde21hcmdpbjowIC0xcHh9LmMtZGF5LWRvdHNbZGF0YS12LTNkYjgwZjgwXXtkaXNwbGF5Oi13ZWJraXQtYm94O2Rpc3BsYXk6LW1zLWZsZXhib3g7ZGlzcGxheTpmbGV4Oy13ZWJraXQtYm94LXBhY2s6Y2VudGVyOy1tcy1mbGV4LXBhY2s6Y2VudGVyO2p1c3RpZnktY29udGVudDpjZW50ZXI7LXdlYmtpdC1ib3gtYWxpZ246Y2VudGVyOy1tcy1mbGV4LWFsaWduOmNlbnRlcjthbGlnbi1pdGVtczpjZW50ZXI7bWFyZ2luOjA7cGFkZGluZzowfS5jLWRheS1kb3RbZGF0YS12LTNkYjgwZjgwXXt3aWR0aDo1cHg7aGVpZ2h0OjVweDtib3JkZXItcmFkaXVzOjUwJTtiYWNrZ3JvdW5kLWNvbG9yOiM2NmIzY2M7dHJhbnNpdGlvbjphbGwgLjE4cyBlYXNlLWluLW91dH0uYy1kYXktZG90W2RhdGEtdi0zZGI4MGY4MF06bm90KDpsYXN0LWNoaWxkKXttYXJnaW4tcmlnaHQ6M3B4fS5jLWRheS1iYXJzW2RhdGEtdi0zZGI4MGY4MF17ZGlzcGxheTotd2Via2l0LWJveDtkaXNwbGF5Oi1tcy1mbGV4Ym94O2Rpc3BsYXk6ZmxleDstd2Via2l0LWJveC1wYWNrOnN0YXJ0Oy1tcy1mbGV4LXBhY2s6c3RhcnQ7anVzdGlmeS1jb250ZW50OmZsZXgtc3RhcnQ7LXdlYmtpdC1ib3gtYWxpZ246Y2VudGVyOy1tcy1mbGV4LWFsaWduOmNlbnRlcjthbGlnbi1pdGVtczpjZW50ZXI7bWFyZ2luOjA7cGFkZGluZzowO3dpZHRoOjc1JX0uYy1kYXktYmFyW2RhdGEtdi0zZGI4MGY4MF17LXdlYmtpdC1ib3gtZmxleDoxOy1tcy1mbGV4LXBvc2l0aXZlOjE7ZmxleC1ncm93OjE7aGVpZ2h0OjNweDtiYWNrZ3JvdW5kLWNvbG9yOiM2NmIzY2M7dHJhbnNpdGlvbjphbGwgLjE4cyBlYXNlLWluLW91dH0uYy1kYXktcG9wb3Zlci1jb250ZW50W2RhdGEtdi0zZGI4MGY4MF17Zm9udC1zaXplOi44cmVtO2ZvbnQtd2VpZ2h0OjQwMH0uYmFja2dyb3VuZC1lbnRlci1hY3RpdmUuYy1kYXktZmFkZS1lbnRlcltkYXRhLXYtM2RiODBmODBde3RyYW5zaXRpb246b3BhY2l0eSAuMnMgZWFzZS1pbi1vdXR9LmJhY2tncm91bmQtZW50ZXItYWN0aXZlLmMtZGF5LXNsaWRlLXJpZ2h0LXNjYWxlLWVudGVyW2RhdGEtdi0zZGI4MGY4MF17LXdlYmtpdC1hbmltYXRpb246c2xpZGVSaWdodFNjYWxlRW50ZXItZGF0YS12LTNkYjgwZjgwIC4xNnMgZWFzZS1pbi1vdXQ7YW5pbWF0aW9uOnNsaWRlUmlnaHRTY2FsZUVudGVyLWRhdGEtdi0zZGI4MGY4MCAuMTZzIGVhc2UtaW4tb3V0fS5iYWNrZ3JvdW5kLWVudGVyLWFjdGl2ZS5jLWRheS1zbGlkZS1yaWdodC10cmFuc2xhdGUtZW50ZXJbZGF0YS12LTNkYjgwZjgwXXstd2Via2l0LWFuaW1hdGlvbjpzbGlkZVJpZ2h0VHJhbnNsYXRlRW50ZXItZGF0YS12LTNkYjgwZjgwIC4xNnMgZWFzZS1pbi1vdXQ7YW5pbWF0aW9uOnNsaWRlUmlnaHRUcmFuc2xhdGVFbnRlci1kYXRhLXYtM2RiODBmODAgLjE2cyBlYXNlLWluLW91dH0uYmFja2dyb3VuZC1lbnRlci1hY3RpdmUuYy1kYXktc2xpZGUtbGVmdC1zY2FsZS1lbnRlcltkYXRhLXYtM2RiODBmODBdey13ZWJraXQtYW5pbWF0aW9uOnNsaWRlTGVmdFNjYWxlRW50ZXItZGF0YS12LTNkYjgwZjgwIC4xNnMgZWFzZS1pbi1vdXQ7YW5pbWF0aW9uOnNsaWRlTGVmdFNjYWxlRW50ZXItZGF0YS12LTNkYjgwZjgwIC4xNnMgZWFzZS1pbi1vdXR9LmJhY2tncm91bmQtZW50ZXItYWN0aXZlLmMtZGF5LXNsaWRlLWxlZnQtdHJhbnNsYXRlLWVudGVyW2RhdGEtdi0zZGI4MGY4MF17LXdlYmtpdC1hbmltYXRpb246c2xpZGVMZWZ0VHJhbnNsYXRlRW50ZXItZGF0YS12LTNkYjgwZjgwIC4xNnMgZWFzZS1pbi1vdXQ7YW5pbWF0aW9uOnNsaWRlTGVmdFRyYW5zbGF0ZUVudGVyLWRhdGEtdi0zZGI4MGY4MCAuMTZzIGVhc2UtaW4tb3V0fS5iYWNrZ3JvdW5kLWVudGVyLWFjdGl2ZS5jLWRheS1zY2FsZS1lbnRlcltkYXRhLXYtM2RiODBmODBdey13ZWJraXQtYW5pbWF0aW9uOnNjYWxlRW50ZXItZGF0YS12LTNkYjgwZjgwIC4xNnMgZWFzZS1pbi1vdXQ7YW5pbWF0aW9uOnNjYWxlRW50ZXItZGF0YS12LTNkYjgwZjgwIC4xNnMgZWFzZS1pbi1vdXR9LmJhY2tncm91bmQtbGVhdmUtYWN0aXZlLmMtZGF5LWZhZGUtbGVhdmVbZGF0YS12LTNkYjgwZjgwXXt0cmFuc2l0aW9uOm9wYWNpdHkgLjJzIGVhc2UtaW4tb3V0fS5iYWNrZ3JvdW5kLWxlYXZlLWFjdGl2ZS5jLWRheS1zY2FsZS1sZWF2ZVtkYXRhLXYtM2RiODBmODBdey13ZWJraXQtYW5pbWF0aW9uOnNjYWxlTGVhdmUtZGF0YS12LTNkYjgwZjgwIC4ycyBlYXNlLWluLW91dDthbmltYXRpb246c2NhbGVMZWF2ZS1kYXRhLXYtM2RiODBmODAgLjJzIGVhc2UtaW4tb3V0fS5iYWNrZ3JvdW5kLWVudGVyLmMtZGF5LWZhZGUtZW50ZXJbZGF0YS12LTNkYjgwZjgwXSwuYmFja2dyb3VuZC1sZWF2ZS10by5jLWRheS1mYWRlLWxlYXZlW2RhdGEtdi0zZGI4MGY4MF17b3BhY2l0eTowfUAtd2Via2l0LWtleWZyYW1lcyBzY2FsZUVudGVyLWRhdGEtdi01NGIxZjkzYnswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoLjcpIHNjYWxlWSguNyk7dHJhbnNmb3JtOnNjYWxlWCguNykgc2NhbGVZKC43KTtvcGFjaXR5Oi4zfTkwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xKSBzY2FsZVkoMS4xKTt0cmFuc2Zvcm06c2NhbGVYKDEuMSkgc2NhbGVZKDEuMSl9OTUley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCguOTUpIHNjYWxlWSguOTUpO3RyYW5zZm9ybTpzY2FsZVgoLjk1KSBzY2FsZVkoLjk1KX10b3std2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO3RyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO29wYWNpdHk6MX19QGtleWZyYW1lcyBzY2FsZUVudGVyLWRhdGEtdi01NGIxZjkzYnswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoLjcpIHNjYWxlWSguNyk7dHJhbnNmb3JtOnNjYWxlWCguNykgc2NhbGVZKC43KTtvcGFjaXR5Oi4zfTkwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xKSBzY2FsZVkoMS4xKTt0cmFuc2Zvcm06c2NhbGVYKDEuMSkgc2NhbGVZKDEuMSl9OTUley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCguOTUpIHNjYWxlWSguOTUpO3RyYW5zZm9ybTpzY2FsZVgoLjk1KSBzY2FsZVkoLjk1KX10b3std2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO3RyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO29wYWNpdHk6MX19QC13ZWJraXQta2V5ZnJhbWVzIHNjYWxlTGVhdmUtZGF0YS12LTU0YjFmOTNiezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSk7dHJhbnNmb3JtOnNjYWxlWCgxKSBzY2FsZVkoMSl9NjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjE4KSBzY2FsZVkoMS4xOCk7dHJhbnNmb3JtOnNjYWxlWCgxLjE4KSBzY2FsZVkoMS4xOCk7b3BhY2l0eTouMn10b3std2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xNSkgc2NhbGVZKDEuMTgpO3RyYW5zZm9ybTpzY2FsZVgoMS4xNSkgc2NhbGVZKDEuMTgpO29wYWNpdHk6MH19QGtleWZyYW1lcyBzY2FsZUxlYXZlLWRhdGEtdi01NGIxZjkzYnswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO3RyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpfTYwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xOCkgc2NhbGVZKDEuMTgpO3RyYW5zZm9ybTpzY2FsZVgoMS4xOCkgc2NhbGVZKDEuMTgpO29wYWNpdHk6LjJ9dG97LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMTUpIHNjYWxlWSgxLjE4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMTUpIHNjYWxlWSgxLjE4KTtvcGFjaXR5OjB9fUAtd2Via2l0LWtleWZyYW1lcyBzbGlkZVJpZ2h0U2NhbGVFbnRlci1kYXRhLXYtNTRiMWY5M2J7MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDApO3RyYW5zZm9ybTpzY2FsZVgoMCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjA4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMDgpfX1Aa2V5ZnJhbWVzIHNsaWRlUmlnaHRTY2FsZUVudGVyLWRhdGEtdi01NGIxZjkzYnswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMCk7dHJhbnNmb3JtOnNjYWxlWCgwKX02MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMDgpO3RyYW5zZm9ybTpzY2FsZVgoMS4wOCl9fUAtd2Via2l0LWtleWZyYW1lcyBzbGlkZVJpZ2h0VHJhbnNsYXRlRW50ZXItZGF0YS12LTU0YjFmOTNiezAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTZweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTZweCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoMnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgycHgpfX1Aa2V5ZnJhbWVzIHNsaWRlUmlnaHRUcmFuc2xhdGVFbnRlci1kYXRhLXYtNTRiMWY5M2J7MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtNnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtNnB4KX02MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgycHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKDJweCl9fUAtd2Via2l0LWtleWZyYW1lcyBzbGlkZUxlZnRTY2FsZUVudGVyLWRhdGEtdi01NGIxZjkzYnswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMCk7dHJhbnNmb3JtOnNjYWxlWCgwKX02MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMDgpO3RyYW5zZm9ybTpzY2FsZVgoMS4wOCl9fUBrZXlmcmFtZXMgc2xpZGVMZWZ0U2NhbGVFbnRlci1kYXRhLXYtNTRiMWY5M2J7MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDApO3RyYW5zZm9ybTpzY2FsZVgoMCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjA4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMDgpfX1ALXdlYmtpdC1rZXlmcmFtZXMgc2xpZGVMZWZ0VHJhbnNsYXRlRW50ZXItZGF0YS12LTU0YjFmOTNiezAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoNnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCg2cHgpfTYwJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC0ycHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC0ycHgpfX1Aa2V5ZnJhbWVzIHNsaWRlTGVmdFRyYW5zbGF0ZUVudGVyLWRhdGEtdi01NGIxZjkzYnswJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKDZweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoNnB4KX02MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtMnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtMnB4KX19LmMtZGF5LXBvcG92ZXItcm93W2RhdGEtdi01NGIxZjkzYl17ZGlzcGxheTotd2Via2l0LWJveDtkaXNwbGF5Oi1tcy1mbGV4Ym94O2Rpc3BsYXk6ZmxleDstd2Via2l0LWJveC1hbGlnbjpjZW50ZXI7LW1zLWZsZXgtYWxpZ246Y2VudGVyO2FsaWduLWl0ZW1zOmNlbnRlcjtwYWRkaW5nOjJweCA1cHg7dHJhbnNpdGlvbjphbGwgLjE4cyBlYXNlLWluLW91dH0uYy1kYXktcG9wb3Zlci1yb3cuc2VsZWN0YWJsZVtkYXRhLXYtNTRiMWY5M2Jde2N1cnNvcjpwb2ludGVyfS5jLWRheS1wb3BvdmVyLXJvdy5zZWxlY3RhYmxlW2RhdGEtdi01NGIxZjkzYl06aG92ZXJ7YmFja2dyb3VuZC1jb2xvcjpyZ2JhKDAsMCwwLC4xKX0uYy1kYXktcG9wb3Zlci1yb3dbZGF0YS12LTU0YjFmOTNiXTpub3QoOmZpcnN0LWNoaWxkKXttYXJnaW4tdG9wOjNweH0uYy1kYXktcG9wb3Zlci1yb3cgLmMtZGF5LXBvcG92ZXItaW5kaWNhdG9yW2RhdGEtdi01NGIxZjkzYl17ZGlzcGxheTotd2Via2l0LWJveDtkaXNwbGF5Oi1tcy1mbGV4Ym94O2Rpc3BsYXk6ZmxleDstd2Via2l0LWJveC1wYWNrOmNlbnRlcjstbXMtZmxleC1wYWNrOmNlbnRlcjtqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyOy13ZWJraXQtYm94LWFsaWduOmNlbnRlcjstbXMtZmxleC1hbGlnbjpjZW50ZXI7YWxpZ24taXRlbXM6Y2VudGVyOy13ZWJraXQtYm94LWZsZXg6MDstbXMtZmxleC1wb3NpdGl2ZTowO2ZsZXgtZ3JvdzowO3dpZHRoOjE1cHg7bWFyZ2luLXJpZ2h0OjNweH0uYy1kYXktcG9wb3Zlci1yb3cgLmMtZGF5LXBvcG92ZXItaW5kaWNhdG9yIHNwYW5bZGF0YS12LTU0YjFmOTNiXXt0cmFuc2l0aW9uOmFsbCAuMThzIGVhc2UtaW4tb3V0fS5jLWRheS1wb3BvdmVyLXJvdyAuYy1kYXktcG9wb3Zlci1jb250ZW50W2RhdGEtdi01NGIxZjkzYl17ZGlzcGxheTotd2Via2l0LWJveDtkaXNwbGF5Oi1tcy1mbGV4Ym94O2Rpc3BsYXk6ZmxleDstd2Via2l0LWJveC1hbGlnbjpjZW50ZXI7LW1zLWZsZXgtYWxpZ246Y2VudGVyO2FsaWduLWl0ZW1zOmNlbnRlcjstbXMtZmxleC13cmFwOm5vbmU7ZmxleC13cmFwOm5vbmU7LXdlYmtpdC1ib3gtZmxleDoxOy1tcy1mbGV4LXBvc2l0aXZlOjE7ZmxleC1ncm93OjE7dHJhbnNpdGlvbjphbGwgLjE4cyBlYXNlLWluLW91dH1ALXdlYmtpdC1rZXlmcmFtZXMgc2NhbGVFbnRlci1kYXRhLXYtODE5NDhlZmV7MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKC43KSBzY2FsZVkoLjcpO3RyYW5zZm9ybTpzY2FsZVgoLjcpIHNjYWxlWSguNyk7b3BhY2l0eTouM305MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMSkgc2NhbGVZKDEuMSk7dHJhbnNmb3JtOnNjYWxlWCgxLjEpIHNjYWxlWSgxLjEpfTk1JXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoLjk1KSBzY2FsZVkoLjk1KTt0cmFuc2Zvcm06c2NhbGVYKC45NSkgc2NhbGVZKC45NSl9dG97LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEpIHNjYWxlWSgxKTt0cmFuc2Zvcm06c2NhbGVYKDEpIHNjYWxlWSgxKTtvcGFjaXR5OjF9fUAtd2Via2l0LWtleWZyYW1lcyBzY2FsZUxlYXZlLWRhdGEtdi04MTk0OGVmZXswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO3RyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpfTYwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xOCkgc2NhbGVZKDEuMTgpO3RyYW5zZm9ybTpzY2FsZVgoMS4xOCkgc2NhbGVZKDEuMTgpO29wYWNpdHk6LjJ9dG97LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMTUpIHNjYWxlWSgxLjE4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMTUpIHNjYWxlWSgxLjE4KTtvcGFjaXR5OjB9fUAtd2Via2l0LWtleWZyYW1lcyBzbGlkZVJpZ2h0U2NhbGVFbnRlci1kYXRhLXYtODE5NDhlZmV7MCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDApO3RyYW5zZm9ybTpzY2FsZVgoMCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgxLjA4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMDgpfX1ALXdlYmtpdC1rZXlmcmFtZXMgc2xpZGVSaWdodFRyYW5zbGF0ZUVudGVyLWRhdGEtdi04MTk0OGVmZXswJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC02cHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC02cHgpfTYwJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKDJweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoMnB4KX19QC13ZWJraXQta2V5ZnJhbWVzIHNsaWRlTGVmdFNjYWxlRW50ZXItZGF0YS12LTgxOTQ4ZWZlezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgwKTt0cmFuc2Zvcm06c2NhbGVYKDApfTYwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4wOCk7dHJhbnNmb3JtOnNjYWxlWCgxLjA4KX19QC13ZWJraXQta2V5ZnJhbWVzIHNsaWRlTGVmdFRyYW5zbGF0ZUVudGVyLWRhdGEtdi04MTk0OGVmZXswJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKDZweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoNnB4KX02MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtMnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtMnB4KX19QGtleWZyYW1lcyBzY2FsZUVudGVyLWRhdGEtdi04MTk0OGVmZXswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoLjcpIHNjYWxlWSguNyk7dHJhbnNmb3JtOnNjYWxlWCguNykgc2NhbGVZKC43KTtvcGFjaXR5Oi4zfTkwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xKSBzY2FsZVkoMS4xKTt0cmFuc2Zvcm06c2NhbGVYKDEuMSkgc2NhbGVZKDEuMSl9OTUley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCguOTUpIHNjYWxlWSguOTUpO3RyYW5zZm9ybTpzY2FsZVgoLjk1KSBzY2FsZVkoLjk1KX10b3std2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO3RyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO29wYWNpdHk6MX19QGtleWZyYW1lcyBzY2FsZUxlYXZlLWRhdGEtdi04MTk0OGVmZXswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpO3RyYW5zZm9ybTpzY2FsZVgoMSkgc2NhbGVZKDEpfTYwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4xOCkgc2NhbGVZKDEuMTgpO3RyYW5zZm9ybTpzY2FsZVgoMS4xOCkgc2NhbGVZKDEuMTgpO29wYWNpdHk6LjJ9dG97LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKDEuMTUpIHNjYWxlWSgxLjE4KTt0cmFuc2Zvcm06c2NhbGVYKDEuMTUpIHNjYWxlWSgxLjE4KTtvcGFjaXR5OjB9fUBrZXlmcmFtZXMgc2xpZGVSaWdodFNjYWxlRW50ZXItZGF0YS12LTgxOTQ4ZWZlezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgwKTt0cmFuc2Zvcm06c2NhbGVYKDApfTYwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4wOCk7dHJhbnNmb3JtOnNjYWxlWCgxLjA4KX19QGtleWZyYW1lcyBzbGlkZVJpZ2h0VHJhbnNsYXRlRW50ZXItZGF0YS12LTgxOTQ4ZWZlezAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTZweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTZweCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoMnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgycHgpfX1Aa2V5ZnJhbWVzIHNsaWRlTGVmdFNjYWxlRW50ZXItZGF0YS12LTgxOTQ4ZWZlezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgwKTt0cmFuc2Zvcm06c2NhbGVYKDApfTYwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoMS4wOCk7dHJhbnNmb3JtOnNjYWxlWCgxLjA4KX19QGtleWZyYW1lcyBzbGlkZUxlZnRUcmFuc2xhdGVFbnRlci1kYXRhLXYtODE5NDhlZmV7MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCg2cHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKDZweCl9NjAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTJweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTJweCl9fS5jLW5hdltkYXRhLXYtODE5NDhlZmVde3RyYW5zaXRpb246aGVpZ2h0IDVzIGVhc2UtaW4tb3V0O2NvbG9yOiMzMzN9LmMtaGVhZGVyW2RhdGEtdi04MTk0OGVmZV17ZGlzcGxheTotd2Via2l0LWJveDtkaXNwbGF5Oi1tcy1mbGV4Ym94O2Rpc3BsYXk6ZmxleDstd2Via2l0LWJveC1wYWNrOmp1c3RpZnk7LW1zLWZsZXgtcGFjazpqdXN0aWZ5O2p1c3RpZnktY29udGVudDpzcGFjZS1iZXR3ZWVuOy13ZWJraXQtYm94LWFsaWduOmNlbnRlcjstbXMtZmxleC1hbGlnbjpjZW50ZXI7YWxpZ24taXRlbXM6Y2VudGVyO2JvcmRlci1ib3R0b206MXB4IHNvbGlkICNkYWRhZGE7cGFkZGluZzozcHggMH0uYy1hcnJvdy1sYXlvdXRbZGF0YS12LTgxOTQ4ZWZlXXttaW4td2lkdGg6MjZweH0uYy1hcnJvdy1sYXlvdXRbZGF0YS12LTgxOTQ4ZWZlXSwuYy1hcnJvd1tkYXRhLXYtODE5NDhlZmVde2Rpc3BsYXk6LXdlYmtpdC1ib3g7ZGlzcGxheTotbXMtZmxleGJveDtkaXNwbGF5OmZsZXg7LXdlYmtpdC1ib3gtcGFjazpjZW50ZXI7LW1zLWZsZXgtcGFjazpjZW50ZXI7anVzdGlmeS1jb250ZW50OmNlbnRlcjstd2Via2l0LWJveC1hbGlnbjpjZW50ZXI7LW1zLWZsZXgtYWxpZ246Y2VudGVyO2FsaWduLWl0ZW1zOmNlbnRlcjttYXJnaW46MDtwYWRkaW5nOjB9LmMtYXJyb3dbZGF0YS12LTgxOTQ4ZWZlXXtmb250LXNpemU6MS42cmVtO3RyYW5zaXRpb246ZmlsbC1vcGFjaXR5IC4zcyBlYXNlLWluLW91dDtjdXJzb3I6cG9pbnRlcjstd2Via2l0LXVzZXItc2VsZWN0Om5vbmU7LW1vei11c2VyLXNlbGVjdDpub25lOy1tcy11c2VyLXNlbGVjdDpub25lO3VzZXItc2VsZWN0Om5vbmV9LmMtYXJyb3dbZGF0YS12LTgxOTQ4ZWZlXTpob3ZlcntmaWxsLW9wYWNpdHk6LjV9LmMtdGl0bGVbZGF0YS12LTgxOTQ4ZWZlXXtmb250LXdlaWdodDo1MDA7dHJhbnNpdGlvbjphbGwgLjI1cyBlYXNlLWluLW91dH0uYy10YWJsZS1jZWxsW2RhdGEtdi04MTk0OGVmZV0sLmMtdGl0bGVbZGF0YS12LTgxOTQ4ZWZlXXtmb250LXNpemU6LjlyZW07Y3Vyc29yOnBvaW50ZXI7LXdlYmtpdC11c2VyLXNlbGVjdDpub25lOy1tb3otdXNlci1zZWxlY3Q6bm9uZTstbXMtdXNlci1zZWxlY3Q6bm9uZTt1c2VyLXNlbGVjdDpub25lfS5jLXRhYmxlLWNlbGxbZGF0YS12LTgxOTQ4ZWZlXXtkaXNwbGF5Oi13ZWJraXQtYm94O2Rpc3BsYXk6LW1zLWZsZXhib3g7ZGlzcGxheTpmbGV4Oy13ZWJraXQtYm94LW9yaWVudDp2ZXJ0aWNhbDstd2Via2l0LWJveC1kaXJlY3Rpb246bm9ybWFsOy1tcy1mbGV4LWRpcmVjdGlvbjpjb2x1bW47ZmxleC1kaXJlY3Rpb246Y29sdW1uOy13ZWJraXQtYm94LXBhY2s6Y2VudGVyOy1tcy1mbGV4LXBhY2s6Y2VudGVyO2p1c3RpZnktY29udGVudDpjZW50ZXI7LXdlYmtpdC1ib3gtYWxpZ246Y2VudGVyOy1tcy1mbGV4LWFsaWduOmNlbnRlcjthbGlnbi1pdGVtczpjZW50ZXI7aGVpZ2h0OjEwMCU7cG9zaXRpb246cmVsYXRpdmU7Zm9udC13ZWlnaHQ6NDAwO2JhY2tncm91bmQtY29sb3I6I2ZmZjt0cmFuc2l0aW9uOmFsbCAuMXMgZWFzZS1pbi1vdXR9LmMtdGFibGUtY2VsbFtkYXRhLXYtODE5NDhlZmVdOmhvdmVye2JhY2tncm91bmQtY29sb3I6I2YwZjBmMH0uYy1kaXNhYmxlZFtkYXRhLXYtODE5NDhlZmVde29wYWNpdHk6LjI7Y3Vyc29yOm5vdC1hbGxvd2VkO3BvaW50ZXItZXZlbnRzOm5vbmV9LmMtZGlzYWJsZWRbZGF0YS12LTgxOTQ4ZWZlXTpob3ZlcntiYWNrZ3JvdW5kLWNvbG9yOnRyYW5zcGFyZW50fS5jLWFjdGl2ZVtkYXRhLXYtODE5NDhlZmVde2JhY2tncm91bmQtY29sb3I6I2YwZjBmMDtmb250LXdlaWdodDo2MDB9LmMtaW5kaWNhdG9yc1tkYXRhLXYtODE5NDhlZmVde3Bvc2l0aW9uOmFic29sdXRlO2Rpc3BsYXk6LXdlYmtpdC1ib3g7ZGlzcGxheTotbXMtZmxleGJveDtkaXNwbGF5OmZsZXg7LXdlYmtpdC1ib3gtcGFjazpjZW50ZXI7LW1zLWZsZXgtcGFjazpjZW50ZXI7anVzdGlmeS1jb250ZW50OmNlbnRlcjstd2Via2l0LWJveC1hbGlnbjpjZW50ZXI7LW1zLWZsZXgtYWxpZ246Y2VudGVyO2FsaWduLWl0ZW1zOmNlbnRlcjtib3R0b206NXB4O3dpZHRoOjEwMCU7dHJhbnNpdGlvbjphbGwgLjFzIGVhc2UtaW4tb3V0fS5jLWluZGljYXRvcnMgLmMtaW5kaWNhdG9yW2RhdGEtdi04MTk0OGVmZV17d2lkdGg6NXB4O2hlaWdodDo1cHg7Ym9yZGVyLXJhZGl1czo1MCV9LmMtaW5kaWNhdG9ycyAuYy1pbmRpY2F0b3JbZGF0YS12LTgxOTQ4ZWZlXTpub3QoOmZpcnN0LWNoaWxkKXttYXJnaW4tbGVmdDozcHh9LmMtdGFibGVbZGF0YS12LTgxOTQ4ZWZlXXt0YWJsZS1sYXlvdXQ6Zml4ZWQ7d2lkdGg6MTAwJTtib3JkZXItY29sbGFwc2U6Y29sbGFwc2V9LmMtdGFibGUgdHIgdGRbZGF0YS12LTgxOTQ4ZWZlXXtib3JkZXI6MXB4IHNvbGlkICNkYWRhZGE7d2lkdGg6NjBweDtoZWlnaHQ6MzRweH0uYy10YWJsZSB0ciB0ZFtkYXRhLXYtODE5NDhlZmVdOmZpcnN0LWNoaWxke2JvcmRlci1sZWZ0OjB9LmMtdGFibGUgdHIgdGRbZGF0YS12LTgxOTQ4ZWZlXTpsYXN0LWNoaWxke2JvcmRlci1yaWdodDowfS5jLXRhYmxlIHRyOmZpcnN0LWNoaWxkIHRkW2RhdGEtdi04MTk0OGVmZV17Ym9yZGVyLXRvcDowfS5jLXRhYmxlIHRyOmxhc3QtY2hpbGQgdGRbZGF0YS12LTgxOTQ4ZWZlXXtib3JkZXItYm90dG9tOjB9LmluZGljYXRvcnMtZW50ZXItYWN0aXZlW2RhdGEtdi04MTk0OGVmZV0sLmluZGljYXRvcnMtbGVhdmUtYWN0aXZlW2RhdGEtdi04MTk0OGVmZV17dHJhbnNpdGlvbjphbGwgLjFzIGVhc2UtaW4tb3V0fS5pbmRpY2F0b3JzLWVudGVyW2RhdGEtdi04MTk0OGVmZV0sLmluZGljYXRvcnMtbGVhdmUtdG9bZGF0YS12LTgxOTQ4ZWZlXXtvcGFjaXR5OjB9LnN2Zy1pY29uW2RhdGEtdi0xMmU5MWFiNF17ZGlzcGxheTppbmxpbmUtYmxvY2s7c3Ryb2tlOmN1cnJlbnRDb2xvcjtzdHJva2Utd2lkdGg6MH0uc3ZnLWljb24gcGF0aFtkYXRhLXYtMTJlOTFhYjRde2ZpbGw6Y3VycmVudENvbG9yfS5kYXRlLWxhYmVsW2RhdGEtdi02YzMzMWU2Ml17dGV4dC1hbGlnbjpjZW50ZXJ9LmRheXMtbmlnaHRzW2RhdGEtdi02YzMzMWU2Ml17LXdlYmtpdC1ib3gtcGFjazpjZW50ZXI7LW1zLWZsZXgtcGFjazpjZW50ZXI7anVzdGlmeS1jb250ZW50OmNlbnRlcjttYXJnaW4tdG9wOjNweH0uZGF5cy1uaWdodHMgLmRheXNbZGF0YS12LTZjMzMxZTYyXSwuZGF5cy1uaWdodHMgLm5pZ2h0c1tkYXRhLXYtNmMzMzFlNjJdLC5kYXlzLW5pZ2h0c1tkYXRhLXYtNmMzMzFlNjJde2Rpc3BsYXk6LXdlYmtpdC1ib3g7ZGlzcGxheTotbXMtZmxleGJveDtkaXNwbGF5OmZsZXg7LXdlYmtpdC1ib3gtYWxpZ246Y2VudGVyOy1tcy1mbGV4LWFsaWduOmNlbnRlcjthbGlnbi1pdGVtczpjZW50ZXJ9LmRheXMtbmlnaHRzIC5kYXlzW2RhdGEtdi02YzMzMWU2Ml0sLmRheXMtbmlnaHRzIC5uaWdodHNbZGF0YS12LTZjMzMxZTYyXXtmb250LXdlaWdodDo3MDB9LmRheXMtbmlnaHRzIC5kYXlzW2RhdGEtdi02YzMzMWU2Ml06bm90KDpmaXJzdC1jaGlsZCksLmRheXMtbmlnaHRzIC5uaWdodHNbZGF0YS12LTZjMzMxZTYyXTpub3QoOmZpcnN0LWNoaWxkKXttYXJnaW4tbGVmdDoxM3B4fS5kYXlzLW5pZ2h0cyAudmMtbW9vbi1vW2RhdGEtdi02YzMzMWU2Ml0sLmRheXMtbmlnaHRzIC52Yy1zdW4tb1tkYXRhLXYtNmMzMzFlNjJde21hcmdpbi1yaWdodDo1cHg7d2lkdGg6MTZweDtoZWlnaHQ6MTZweH0uZGF5cy1uaWdodHMgLnZjLXN1bi1vW2RhdGEtdi02YzMzMWU2Ml17Y29sb3I6I2ZmYjM2Nn0uZGF5cy1uaWdodHMgLnZjLW1vb24tb1tkYXRhLXYtNmMzMzFlNjJde2NvbG9yOiM0ZDRkNjR9XCIsIFwiXCJdKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlciEuL25vZGVfbW9kdWxlcy92LWNhbGVuZGFyL2xpYi92LWNhbGVuZGFyLm1pbi5jc3Ncbi8vIG1vZHVsZSBpZCA9IDdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiLypcblx0TUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcblx0QXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxuKi9cbi8vIGNzcyBiYXNlIGNvZGUsIGluamVjdGVkIGJ5IHRoZSBjc3MtbG9hZGVyXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKHVzZVNvdXJjZU1hcCkge1xuXHR2YXIgbGlzdCA9IFtdO1xuXG5cdC8vIHJldHVybiB0aGUgbGlzdCBvZiBtb2R1bGVzIGFzIGNzcyBzdHJpbmdcblx0bGlzdC50b1N0cmluZyA9IGZ1bmN0aW9uIHRvU3RyaW5nKCkge1xuXHRcdHJldHVybiB0aGlzLm1hcChmdW5jdGlvbiAoaXRlbSkge1xuXHRcdFx0dmFyIGNvbnRlbnQgPSBjc3NXaXRoTWFwcGluZ1RvU3RyaW5nKGl0ZW0sIHVzZVNvdXJjZU1hcCk7XG5cdFx0XHRpZihpdGVtWzJdKSB7XG5cdFx0XHRcdHJldHVybiBcIkBtZWRpYSBcIiArIGl0ZW1bMl0gKyBcIntcIiArIGNvbnRlbnQgKyBcIn1cIjtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHJldHVybiBjb250ZW50O1xuXHRcdFx0fVxuXHRcdH0pLmpvaW4oXCJcIik7XG5cdH07XG5cblx0Ly8gaW1wb3J0IGEgbGlzdCBvZiBtb2R1bGVzIGludG8gdGhlIGxpc3Rcblx0bGlzdC5pID0gZnVuY3Rpb24obW9kdWxlcywgbWVkaWFRdWVyeSkge1xuXHRcdGlmKHR5cGVvZiBtb2R1bGVzID09PSBcInN0cmluZ1wiKVxuXHRcdFx0bW9kdWxlcyA9IFtbbnVsbCwgbW9kdWxlcywgXCJcIl1dO1xuXHRcdHZhciBhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzID0ge307XG5cdFx0Zm9yKHZhciBpID0gMDsgaSA8IHRoaXMubGVuZ3RoOyBpKyspIHtcblx0XHRcdHZhciBpZCA9IHRoaXNbaV1bMF07XG5cdFx0XHRpZih0eXBlb2YgaWQgPT09IFwibnVtYmVyXCIpXG5cdFx0XHRcdGFscmVhZHlJbXBvcnRlZE1vZHVsZXNbaWRdID0gdHJ1ZTtcblx0XHR9XG5cdFx0Zm9yKGkgPSAwOyBpIDwgbW9kdWxlcy5sZW5ndGg7IGkrKykge1xuXHRcdFx0dmFyIGl0ZW0gPSBtb2R1bGVzW2ldO1xuXHRcdFx0Ly8gc2tpcCBhbHJlYWR5IGltcG9ydGVkIG1vZHVsZVxuXHRcdFx0Ly8gdGhpcyBpbXBsZW1lbnRhdGlvbiBpcyBub3QgMTAwJSBwZXJmZWN0IGZvciB3ZWlyZCBtZWRpYSBxdWVyeSBjb21iaW5hdGlvbnNcblx0XHRcdC8vICB3aGVuIGEgbW9kdWxlIGlzIGltcG9ydGVkIG11bHRpcGxlIHRpbWVzIHdpdGggZGlmZmVyZW50IG1lZGlhIHF1ZXJpZXMuXG5cdFx0XHQvLyAgSSBob3BlIHRoaXMgd2lsbCBuZXZlciBvY2N1ciAoSGV5IHRoaXMgd2F5IHdlIGhhdmUgc21hbGxlciBidW5kbGVzKVxuXHRcdFx0aWYodHlwZW9mIGl0ZW1bMF0gIT09IFwibnVtYmVyXCIgfHwgIWFscmVhZHlJbXBvcnRlZE1vZHVsZXNbaXRlbVswXV0pIHtcblx0XHRcdFx0aWYobWVkaWFRdWVyeSAmJiAhaXRlbVsyXSkge1xuXHRcdFx0XHRcdGl0ZW1bMl0gPSBtZWRpYVF1ZXJ5O1xuXHRcdFx0XHR9IGVsc2UgaWYobWVkaWFRdWVyeSkge1xuXHRcdFx0XHRcdGl0ZW1bMl0gPSBcIihcIiArIGl0ZW1bMl0gKyBcIikgYW5kIChcIiArIG1lZGlhUXVlcnkgKyBcIilcIjtcblx0XHRcdFx0fVxuXHRcdFx0XHRsaXN0LnB1c2goaXRlbSk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9O1xuXHRyZXR1cm4gbGlzdDtcbn07XG5cbmZ1bmN0aW9uIGNzc1dpdGhNYXBwaW5nVG9TdHJpbmcoaXRlbSwgdXNlU291cmNlTWFwKSB7XG5cdHZhciBjb250ZW50ID0gaXRlbVsxXSB8fCAnJztcblx0dmFyIGNzc01hcHBpbmcgPSBpdGVtWzNdO1xuXHRpZiAoIWNzc01hcHBpbmcpIHtcblx0XHRyZXR1cm4gY29udGVudDtcblx0fVxuXG5cdGlmICh1c2VTb3VyY2VNYXAgJiYgdHlwZW9mIGJ0b2EgPT09ICdmdW5jdGlvbicpIHtcblx0XHR2YXIgc291cmNlTWFwcGluZyA9IHRvQ29tbWVudChjc3NNYXBwaW5nKTtcblx0XHR2YXIgc291cmNlVVJMcyA9IGNzc01hcHBpbmcuc291cmNlcy5tYXAoZnVuY3Rpb24gKHNvdXJjZSkge1xuXHRcdFx0cmV0dXJuICcvKiMgc291cmNlVVJMPScgKyBjc3NNYXBwaW5nLnNvdXJjZVJvb3QgKyBzb3VyY2UgKyAnICovJ1xuXHRcdH0pO1xuXG5cdFx0cmV0dXJuIFtjb250ZW50XS5jb25jYXQoc291cmNlVVJMcykuY29uY2F0KFtzb3VyY2VNYXBwaW5nXSkuam9pbignXFxuJyk7XG5cdH1cblxuXHRyZXR1cm4gW2NvbnRlbnRdLmpvaW4oJ1xcbicpO1xufVxuXG4vLyBBZGFwdGVkIGZyb20gY29udmVydC1zb3VyY2UtbWFwIChNSVQpXG5mdW5jdGlvbiB0b0NvbW1lbnQoc291cmNlTWFwKSB7XG5cdC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby11bmRlZlxuXHR2YXIgYmFzZTY0ID0gYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc291cmNlTWFwKSkpKTtcblx0dmFyIGRhdGEgPSAnc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247Y2hhcnNldD11dGYtODtiYXNlNjQsJyArIGJhc2U2NDtcblxuXHRyZXR1cm4gJy8qIyAnICsgZGF0YSArICcgKi8nO1xufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcbi8vIG1vZHVsZSBpZCA9IDhcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiLypcblx0TUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcblx0QXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxuKi9cblxudmFyIHN0eWxlc0luRG9tID0ge307XG5cbnZhclx0bWVtb2l6ZSA9IGZ1bmN0aW9uIChmbikge1xuXHR2YXIgbWVtbztcblxuXHRyZXR1cm4gZnVuY3Rpb24gKCkge1xuXHRcdGlmICh0eXBlb2YgbWVtbyA9PT0gXCJ1bmRlZmluZWRcIikgbWVtbyA9IGZuLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG5cdFx0cmV0dXJuIG1lbW87XG5cdH07XG59O1xuXG52YXIgaXNPbGRJRSA9IG1lbW9pemUoZnVuY3Rpb24gKCkge1xuXHQvLyBUZXN0IGZvciBJRSA8PSA5IGFzIHByb3Bvc2VkIGJ5IEJyb3dzZXJoYWNrc1xuXHQvLyBAc2VlIGh0dHA6Ly9icm93c2VyaGFja3MuY29tLyNoYWNrLWU3MWQ4NjkyZjY1MzM0MTczZmVlNzE1YzIyMmNiODA1XG5cdC8vIFRlc3RzIGZvciBleGlzdGVuY2Ugb2Ygc3RhbmRhcmQgZ2xvYmFscyBpcyB0byBhbGxvdyBzdHlsZS1sb2FkZXJcblx0Ly8gdG8gb3BlcmF0ZSBjb3JyZWN0bHkgaW50byBub24tc3RhbmRhcmQgZW52aXJvbm1lbnRzXG5cdC8vIEBzZWUgaHR0cHM6Ly9naXRodWIuY29tL3dlYnBhY2stY29udHJpYi9zdHlsZS1sb2FkZXIvaXNzdWVzLzE3N1xuXHRyZXR1cm4gd2luZG93ICYmIGRvY3VtZW50ICYmIGRvY3VtZW50LmFsbCAmJiAhd2luZG93LmF0b2I7XG59KTtcblxudmFyIGdldFRhcmdldCA9IGZ1bmN0aW9uICh0YXJnZXQpIHtcbiAgcmV0dXJuIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KTtcbn07XG5cbnZhciBnZXRFbGVtZW50ID0gKGZ1bmN0aW9uIChmbikge1xuXHR2YXIgbWVtbyA9IHt9O1xuXG5cdHJldHVybiBmdW5jdGlvbih0YXJnZXQpIHtcbiAgICAgICAgICAgICAgICAvLyBJZiBwYXNzaW5nIGZ1bmN0aW9uIGluIG9wdGlvbnMsIHRoZW4gdXNlIGl0IGZvciByZXNvbHZlIFwiaGVhZFwiIGVsZW1lbnQuXG4gICAgICAgICAgICAgICAgLy8gVXNlZnVsIGZvciBTaGFkb3cgUm9vdCBzdHlsZSBpLmVcbiAgICAgICAgICAgICAgICAvLyB7XG4gICAgICAgICAgICAgICAgLy8gICBpbnNlcnRJbnRvOiBmdW5jdGlvbiAoKSB7IHJldHVybiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2Zvb1wiKS5zaGFkb3dSb290IH1cbiAgICAgICAgICAgICAgICAvLyB9XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiB0YXJnZXQgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0YXJnZXQoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBtZW1vW3RhcmdldF0gPT09IFwidW5kZWZpbmVkXCIpIHtcblx0XHRcdHZhciBzdHlsZVRhcmdldCA9IGdldFRhcmdldC5jYWxsKHRoaXMsIHRhcmdldCk7XG5cdFx0XHQvLyBTcGVjaWFsIGNhc2UgdG8gcmV0dXJuIGhlYWQgb2YgaWZyYW1lIGluc3RlYWQgb2YgaWZyYW1lIGl0c2VsZlxuXHRcdFx0aWYgKHdpbmRvdy5IVE1MSUZyYW1lRWxlbWVudCAmJiBzdHlsZVRhcmdldCBpbnN0YW5jZW9mIHdpbmRvdy5IVE1MSUZyYW1lRWxlbWVudCkge1xuXHRcdFx0XHR0cnkge1xuXHRcdFx0XHRcdC8vIFRoaXMgd2lsbCB0aHJvdyBhbiBleGNlcHRpb24gaWYgYWNjZXNzIHRvIGlmcmFtZSBpcyBibG9ja2VkXG5cdFx0XHRcdFx0Ly8gZHVlIHRvIGNyb3NzLW9yaWdpbiByZXN0cmljdGlvbnNcblx0XHRcdFx0XHRzdHlsZVRhcmdldCA9IHN0eWxlVGFyZ2V0LmNvbnRlbnREb2N1bWVudC5oZWFkO1xuXHRcdFx0XHR9IGNhdGNoKGUpIHtcblx0XHRcdFx0XHRzdHlsZVRhcmdldCA9IG51bGw7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHRcdG1lbW9bdGFyZ2V0XSA9IHN0eWxlVGFyZ2V0O1xuXHRcdH1cblx0XHRyZXR1cm4gbWVtb1t0YXJnZXRdXG5cdH07XG59KSgpO1xuXG52YXIgc2luZ2xldG9uID0gbnVsbDtcbnZhclx0c2luZ2xldG9uQ291bnRlciA9IDA7XG52YXJcdHN0eWxlc0luc2VydGVkQXRUb3AgPSBbXTtcblxudmFyXHRmaXhVcmxzID0gcmVxdWlyZShcIi4vdXJsc1wiKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihsaXN0LCBvcHRpb25zKSB7XG5cdGlmICh0eXBlb2YgREVCVUcgIT09IFwidW5kZWZpbmVkXCIgJiYgREVCVUcpIHtcblx0XHRpZiAodHlwZW9mIGRvY3VtZW50ICE9PSBcIm9iamVjdFwiKSB0aHJvdyBuZXcgRXJyb3IoXCJUaGUgc3R5bGUtbG9hZGVyIGNhbm5vdCBiZSB1c2VkIGluIGEgbm9uLWJyb3dzZXIgZW52aXJvbm1lbnRcIik7XG5cdH1cblxuXHRvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcblxuXHRvcHRpb25zLmF0dHJzID0gdHlwZW9mIG9wdGlvbnMuYXR0cnMgPT09IFwib2JqZWN0XCIgPyBvcHRpb25zLmF0dHJzIDoge307XG5cblx0Ly8gRm9yY2Ugc2luZ2xlLXRhZyBzb2x1dGlvbiBvbiBJRTYtOSwgd2hpY2ggaGFzIGEgaGFyZCBsaW1pdCBvbiB0aGUgIyBvZiA8c3R5bGU+XG5cdC8vIHRhZ3MgaXQgd2lsbCBhbGxvdyBvbiBhIHBhZ2Vcblx0aWYgKCFvcHRpb25zLnNpbmdsZXRvbiAmJiB0eXBlb2Ygb3B0aW9ucy5zaW5nbGV0b24gIT09IFwiYm9vbGVhblwiKSBvcHRpb25zLnNpbmdsZXRvbiA9IGlzT2xkSUUoKTtcblxuXHQvLyBCeSBkZWZhdWx0LCBhZGQgPHN0eWxlPiB0YWdzIHRvIHRoZSA8aGVhZD4gZWxlbWVudFxuICAgICAgICBpZiAoIW9wdGlvbnMuaW5zZXJ0SW50bykgb3B0aW9ucy5pbnNlcnRJbnRvID0gXCJoZWFkXCI7XG5cblx0Ly8gQnkgZGVmYXVsdCwgYWRkIDxzdHlsZT4gdGFncyB0byB0aGUgYm90dG9tIG9mIHRoZSB0YXJnZXRcblx0aWYgKCFvcHRpb25zLmluc2VydEF0KSBvcHRpb25zLmluc2VydEF0ID0gXCJib3R0b21cIjtcblxuXHR2YXIgc3R5bGVzID0gbGlzdFRvU3R5bGVzKGxpc3QsIG9wdGlvbnMpO1xuXG5cdGFkZFN0eWxlc1RvRG9tKHN0eWxlcywgb3B0aW9ucyk7XG5cblx0cmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZSAobmV3TGlzdCkge1xuXHRcdHZhciBtYXlSZW1vdmUgPSBbXTtcblxuXHRcdGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVzLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHR2YXIgaXRlbSA9IHN0eWxlc1tpXTtcblx0XHRcdHZhciBkb21TdHlsZSA9IHN0eWxlc0luRG9tW2l0ZW0uaWRdO1xuXG5cdFx0XHRkb21TdHlsZS5yZWZzLS07XG5cdFx0XHRtYXlSZW1vdmUucHVzaChkb21TdHlsZSk7XG5cdFx0fVxuXG5cdFx0aWYobmV3TGlzdCkge1xuXHRcdFx0dmFyIG5ld1N0eWxlcyA9IGxpc3RUb1N0eWxlcyhuZXdMaXN0LCBvcHRpb25zKTtcblx0XHRcdGFkZFN0eWxlc1RvRG9tKG5ld1N0eWxlcywgb3B0aW9ucyk7XG5cdFx0fVxuXG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBtYXlSZW1vdmUubGVuZ3RoOyBpKyspIHtcblx0XHRcdHZhciBkb21TdHlsZSA9IG1heVJlbW92ZVtpXTtcblxuXHRcdFx0aWYoZG9tU3R5bGUucmVmcyA9PT0gMCkge1xuXHRcdFx0XHRmb3IgKHZhciBqID0gMDsgaiA8IGRvbVN0eWxlLnBhcnRzLmxlbmd0aDsgaisrKSBkb21TdHlsZS5wYXJ0c1tqXSgpO1xuXG5cdFx0XHRcdGRlbGV0ZSBzdHlsZXNJbkRvbVtkb21TdHlsZS5pZF07XG5cdFx0XHR9XG5cdFx0fVxuXHR9O1xufTtcblxuZnVuY3Rpb24gYWRkU3R5bGVzVG9Eb20gKHN0eWxlcywgb3B0aW9ucykge1xuXHRmb3IgKHZhciBpID0gMDsgaSA8IHN0eWxlcy5sZW5ndGg7IGkrKykge1xuXHRcdHZhciBpdGVtID0gc3R5bGVzW2ldO1xuXHRcdHZhciBkb21TdHlsZSA9IHN0eWxlc0luRG9tW2l0ZW0uaWRdO1xuXG5cdFx0aWYoZG9tU3R5bGUpIHtcblx0XHRcdGRvbVN0eWxlLnJlZnMrKztcblxuXHRcdFx0Zm9yKHZhciBqID0gMDsgaiA8IGRvbVN0eWxlLnBhcnRzLmxlbmd0aDsgaisrKSB7XG5cdFx0XHRcdGRvbVN0eWxlLnBhcnRzW2pdKGl0ZW0ucGFydHNbal0pO1xuXHRcdFx0fVxuXG5cdFx0XHRmb3IoOyBqIDwgaXRlbS5wYXJ0cy5sZW5ndGg7IGorKykge1xuXHRcdFx0XHRkb21TdHlsZS5wYXJ0cy5wdXNoKGFkZFN0eWxlKGl0ZW0ucGFydHNbal0sIG9wdGlvbnMpKTtcblx0XHRcdH1cblx0XHR9IGVsc2Uge1xuXHRcdFx0dmFyIHBhcnRzID0gW107XG5cblx0XHRcdGZvcih2YXIgaiA9IDA7IGogPCBpdGVtLnBhcnRzLmxlbmd0aDsgaisrKSB7XG5cdFx0XHRcdHBhcnRzLnB1c2goYWRkU3R5bGUoaXRlbS5wYXJ0c1tqXSwgb3B0aW9ucykpO1xuXHRcdFx0fVxuXG5cdFx0XHRzdHlsZXNJbkRvbVtpdGVtLmlkXSA9IHtpZDogaXRlbS5pZCwgcmVmczogMSwgcGFydHM6IHBhcnRzfTtcblx0XHR9XG5cdH1cbn1cblxuZnVuY3Rpb24gbGlzdFRvU3R5bGVzIChsaXN0LCBvcHRpb25zKSB7XG5cdHZhciBzdHlsZXMgPSBbXTtcblx0dmFyIG5ld1N0eWxlcyA9IHt9O1xuXG5cdGZvciAodmFyIGkgPSAwOyBpIDwgbGlzdC5sZW5ndGg7IGkrKykge1xuXHRcdHZhciBpdGVtID0gbGlzdFtpXTtcblx0XHR2YXIgaWQgPSBvcHRpb25zLmJhc2UgPyBpdGVtWzBdICsgb3B0aW9ucy5iYXNlIDogaXRlbVswXTtcblx0XHR2YXIgY3NzID0gaXRlbVsxXTtcblx0XHR2YXIgbWVkaWEgPSBpdGVtWzJdO1xuXHRcdHZhciBzb3VyY2VNYXAgPSBpdGVtWzNdO1xuXHRcdHZhciBwYXJ0ID0ge2NzczogY3NzLCBtZWRpYTogbWVkaWEsIHNvdXJjZU1hcDogc291cmNlTWFwfTtcblxuXHRcdGlmKCFuZXdTdHlsZXNbaWRdKSBzdHlsZXMucHVzaChuZXdTdHlsZXNbaWRdID0ge2lkOiBpZCwgcGFydHM6IFtwYXJ0XX0pO1xuXHRcdGVsc2UgbmV3U3R5bGVzW2lkXS5wYXJ0cy5wdXNoKHBhcnQpO1xuXHR9XG5cblx0cmV0dXJuIHN0eWxlcztcbn1cblxuZnVuY3Rpb24gaW5zZXJ0U3R5bGVFbGVtZW50IChvcHRpb25zLCBzdHlsZSkge1xuXHR2YXIgdGFyZ2V0ID0gZ2V0RWxlbWVudChvcHRpb25zLmluc2VydEludG8pXG5cblx0aWYgKCF0YXJnZXQpIHtcblx0XHR0aHJvdyBuZXcgRXJyb3IoXCJDb3VsZG4ndCBmaW5kIGEgc3R5bGUgdGFyZ2V0LiBUaGlzIHByb2JhYmx5IG1lYW5zIHRoYXQgdGhlIHZhbHVlIGZvciB0aGUgJ2luc2VydEludG8nIHBhcmFtZXRlciBpcyBpbnZhbGlkLlwiKTtcblx0fVxuXG5cdHZhciBsYXN0U3R5bGVFbGVtZW50SW5zZXJ0ZWRBdFRvcCA9IHN0eWxlc0luc2VydGVkQXRUb3Bbc3R5bGVzSW5zZXJ0ZWRBdFRvcC5sZW5ndGggLSAxXTtcblxuXHRpZiAob3B0aW9ucy5pbnNlcnRBdCA9PT0gXCJ0b3BcIikge1xuXHRcdGlmICghbGFzdFN0eWxlRWxlbWVudEluc2VydGVkQXRUb3ApIHtcblx0XHRcdHRhcmdldC5pbnNlcnRCZWZvcmUoc3R5bGUsIHRhcmdldC5maXJzdENoaWxkKTtcblx0XHR9IGVsc2UgaWYgKGxhc3RTdHlsZUVsZW1lbnRJbnNlcnRlZEF0VG9wLm5leHRTaWJsaW5nKSB7XG5cdFx0XHR0YXJnZXQuaW5zZXJ0QmVmb3JlKHN0eWxlLCBsYXN0U3R5bGVFbGVtZW50SW5zZXJ0ZWRBdFRvcC5uZXh0U2libGluZyk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHRhcmdldC5hcHBlbmRDaGlsZChzdHlsZSk7XG5cdFx0fVxuXHRcdHN0eWxlc0luc2VydGVkQXRUb3AucHVzaChzdHlsZSk7XG5cdH0gZWxzZSBpZiAob3B0aW9ucy5pbnNlcnRBdCA9PT0gXCJib3R0b21cIikge1xuXHRcdHRhcmdldC5hcHBlbmRDaGlsZChzdHlsZSk7XG5cdH0gZWxzZSBpZiAodHlwZW9mIG9wdGlvbnMuaW5zZXJ0QXQgPT09IFwib2JqZWN0XCIgJiYgb3B0aW9ucy5pbnNlcnRBdC5iZWZvcmUpIHtcblx0XHR2YXIgbmV4dFNpYmxpbmcgPSBnZXRFbGVtZW50KG9wdGlvbnMuaW5zZXJ0SW50byArIFwiIFwiICsgb3B0aW9ucy5pbnNlcnRBdC5iZWZvcmUpO1xuXHRcdHRhcmdldC5pbnNlcnRCZWZvcmUoc3R5bGUsIG5leHRTaWJsaW5nKTtcblx0fSBlbHNlIHtcblx0XHR0aHJvdyBuZXcgRXJyb3IoXCJbU3R5bGUgTG9hZGVyXVxcblxcbiBJbnZhbGlkIHZhbHVlIGZvciBwYXJhbWV0ZXIgJ2luc2VydEF0JyAoJ29wdGlvbnMuaW5zZXJ0QXQnKSBmb3VuZC5cXG4gTXVzdCBiZSAndG9wJywgJ2JvdHRvbScsIG9yIE9iamVjdC5cXG4gKGh0dHBzOi8vZ2l0aHViLmNvbS93ZWJwYWNrLWNvbnRyaWIvc3R5bGUtbG9hZGVyI2luc2VydGF0KVxcblwiKTtcblx0fVxufVxuXG5mdW5jdGlvbiByZW1vdmVTdHlsZUVsZW1lbnQgKHN0eWxlKSB7XG5cdGlmIChzdHlsZS5wYXJlbnROb2RlID09PSBudWxsKSByZXR1cm4gZmFsc2U7XG5cdHN0eWxlLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc3R5bGUpO1xuXG5cdHZhciBpZHggPSBzdHlsZXNJbnNlcnRlZEF0VG9wLmluZGV4T2Yoc3R5bGUpO1xuXHRpZihpZHggPj0gMCkge1xuXHRcdHN0eWxlc0luc2VydGVkQXRUb3Auc3BsaWNlKGlkeCwgMSk7XG5cdH1cbn1cblxuZnVuY3Rpb24gY3JlYXRlU3R5bGVFbGVtZW50IChvcHRpb25zKSB7XG5cdHZhciBzdHlsZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJzdHlsZVwiKTtcblxuXHRpZihvcHRpb25zLmF0dHJzLnR5cGUgPT09IHVuZGVmaW5lZCkge1xuXHRcdG9wdGlvbnMuYXR0cnMudHlwZSA9IFwidGV4dC9jc3NcIjtcblx0fVxuXG5cdGFkZEF0dHJzKHN0eWxlLCBvcHRpb25zLmF0dHJzKTtcblx0aW5zZXJ0U3R5bGVFbGVtZW50KG9wdGlvbnMsIHN0eWxlKTtcblxuXHRyZXR1cm4gc3R5bGU7XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZUxpbmtFbGVtZW50IChvcHRpb25zKSB7XG5cdHZhciBsaW5rID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxpbmtcIik7XG5cblx0aWYob3B0aW9ucy5hdHRycy50eXBlID09PSB1bmRlZmluZWQpIHtcblx0XHRvcHRpb25zLmF0dHJzLnR5cGUgPSBcInRleHQvY3NzXCI7XG5cdH1cblx0b3B0aW9ucy5hdHRycy5yZWwgPSBcInN0eWxlc2hlZXRcIjtcblxuXHRhZGRBdHRycyhsaW5rLCBvcHRpb25zLmF0dHJzKTtcblx0aW5zZXJ0U3R5bGVFbGVtZW50KG9wdGlvbnMsIGxpbmspO1xuXG5cdHJldHVybiBsaW5rO1xufVxuXG5mdW5jdGlvbiBhZGRBdHRycyAoZWwsIGF0dHJzKSB7XG5cdE9iamVjdC5rZXlzKGF0dHJzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcblx0XHRlbC5zZXRBdHRyaWJ1dGUoa2V5LCBhdHRyc1trZXldKTtcblx0fSk7XG59XG5cbmZ1bmN0aW9uIGFkZFN0eWxlIChvYmosIG9wdGlvbnMpIHtcblx0dmFyIHN0eWxlLCB1cGRhdGUsIHJlbW92ZSwgcmVzdWx0O1xuXG5cdC8vIElmIGEgdHJhbnNmb3JtIGZ1bmN0aW9uIHdhcyBkZWZpbmVkLCBydW4gaXQgb24gdGhlIGNzc1xuXHRpZiAob3B0aW9ucy50cmFuc2Zvcm0gJiYgb2JqLmNzcykge1xuXHQgICAgcmVzdWx0ID0gb3B0aW9ucy50cmFuc2Zvcm0ob2JqLmNzcyk7XG5cblx0ICAgIGlmIChyZXN1bHQpIHtcblx0ICAgIFx0Ly8gSWYgdHJhbnNmb3JtIHJldHVybnMgYSB2YWx1ZSwgdXNlIHRoYXQgaW5zdGVhZCBvZiB0aGUgb3JpZ2luYWwgY3NzLlxuXHQgICAgXHQvLyBUaGlzIGFsbG93cyBydW5uaW5nIHJ1bnRpbWUgdHJhbnNmb3JtYXRpb25zIG9uIHRoZSBjc3MuXG5cdCAgICBcdG9iai5jc3MgPSByZXN1bHQ7XG5cdCAgICB9IGVsc2Uge1xuXHQgICAgXHQvLyBJZiB0aGUgdHJhbnNmb3JtIGZ1bmN0aW9uIHJldHVybnMgYSBmYWxzeSB2YWx1ZSwgZG9uJ3QgYWRkIHRoaXMgY3NzLlxuXHQgICAgXHQvLyBUaGlzIGFsbG93cyBjb25kaXRpb25hbCBsb2FkaW5nIG9mIGNzc1xuXHQgICAgXHRyZXR1cm4gZnVuY3Rpb24oKSB7XG5cdCAgICBcdFx0Ly8gbm9vcFxuXHQgICAgXHR9O1xuXHQgICAgfVxuXHR9XG5cblx0aWYgKG9wdGlvbnMuc2luZ2xldG9uKSB7XG5cdFx0dmFyIHN0eWxlSW5kZXggPSBzaW5nbGV0b25Db3VudGVyKys7XG5cblx0XHRzdHlsZSA9IHNpbmdsZXRvbiB8fCAoc2luZ2xldG9uID0gY3JlYXRlU3R5bGVFbGVtZW50KG9wdGlvbnMpKTtcblxuXHRcdHVwZGF0ZSA9IGFwcGx5VG9TaW5nbGV0b25UYWcuYmluZChudWxsLCBzdHlsZSwgc3R5bGVJbmRleCwgZmFsc2UpO1xuXHRcdHJlbW92ZSA9IGFwcGx5VG9TaW5nbGV0b25UYWcuYmluZChudWxsLCBzdHlsZSwgc3R5bGVJbmRleCwgdHJ1ZSk7XG5cblx0fSBlbHNlIGlmIChcblx0XHRvYmouc291cmNlTWFwICYmXG5cdFx0dHlwZW9mIFVSTCA9PT0gXCJmdW5jdGlvblwiICYmXG5cdFx0dHlwZW9mIFVSTC5jcmVhdGVPYmplY3RVUkwgPT09IFwiZnVuY3Rpb25cIiAmJlxuXHRcdHR5cGVvZiBVUkwucmV2b2tlT2JqZWN0VVJMID09PSBcImZ1bmN0aW9uXCIgJiZcblx0XHR0eXBlb2YgQmxvYiA9PT0gXCJmdW5jdGlvblwiICYmXG5cdFx0dHlwZW9mIGJ0b2EgPT09IFwiZnVuY3Rpb25cIlxuXHQpIHtcblx0XHRzdHlsZSA9IGNyZWF0ZUxpbmtFbGVtZW50KG9wdGlvbnMpO1xuXHRcdHVwZGF0ZSA9IHVwZGF0ZUxpbmsuYmluZChudWxsLCBzdHlsZSwgb3B0aW9ucyk7XG5cdFx0cmVtb3ZlID0gZnVuY3Rpb24gKCkge1xuXHRcdFx0cmVtb3ZlU3R5bGVFbGVtZW50KHN0eWxlKTtcblxuXHRcdFx0aWYoc3R5bGUuaHJlZikgVVJMLnJldm9rZU9iamVjdFVSTChzdHlsZS5ocmVmKTtcblx0XHR9O1xuXHR9IGVsc2Uge1xuXHRcdHN0eWxlID0gY3JlYXRlU3R5bGVFbGVtZW50KG9wdGlvbnMpO1xuXHRcdHVwZGF0ZSA9IGFwcGx5VG9UYWcuYmluZChudWxsLCBzdHlsZSk7XG5cdFx0cmVtb3ZlID0gZnVuY3Rpb24gKCkge1xuXHRcdFx0cmVtb3ZlU3R5bGVFbGVtZW50KHN0eWxlKTtcblx0XHR9O1xuXHR9XG5cblx0dXBkYXRlKG9iaik7XG5cblx0cmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZVN0eWxlIChuZXdPYmopIHtcblx0XHRpZiAobmV3T2JqKSB7XG5cdFx0XHRpZiAoXG5cdFx0XHRcdG5ld09iai5jc3MgPT09IG9iai5jc3MgJiZcblx0XHRcdFx0bmV3T2JqLm1lZGlhID09PSBvYmoubWVkaWEgJiZcblx0XHRcdFx0bmV3T2JqLnNvdXJjZU1hcCA9PT0gb2JqLnNvdXJjZU1hcFxuXHRcdFx0KSB7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblxuXHRcdFx0dXBkYXRlKG9iaiA9IG5ld09iaik7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHJlbW92ZSgpO1xuXHRcdH1cblx0fTtcbn1cblxudmFyIHJlcGxhY2VUZXh0ID0gKGZ1bmN0aW9uICgpIHtcblx0dmFyIHRleHRTdG9yZSA9IFtdO1xuXG5cdHJldHVybiBmdW5jdGlvbiAoaW5kZXgsIHJlcGxhY2VtZW50KSB7XG5cdFx0dGV4dFN0b3JlW2luZGV4XSA9IHJlcGxhY2VtZW50O1xuXG5cdFx0cmV0dXJuIHRleHRTdG9yZS5maWx0ZXIoQm9vbGVhbikuam9pbignXFxuJyk7XG5cdH07XG59KSgpO1xuXG5mdW5jdGlvbiBhcHBseVRvU2luZ2xldG9uVGFnIChzdHlsZSwgaW5kZXgsIHJlbW92ZSwgb2JqKSB7XG5cdHZhciBjc3MgPSByZW1vdmUgPyBcIlwiIDogb2JqLmNzcztcblxuXHRpZiAoc3R5bGUuc3R5bGVTaGVldCkge1xuXHRcdHN0eWxlLnN0eWxlU2hlZXQuY3NzVGV4dCA9IHJlcGxhY2VUZXh0KGluZGV4LCBjc3MpO1xuXHR9IGVsc2Uge1xuXHRcdHZhciBjc3NOb2RlID0gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY3NzKTtcblx0XHR2YXIgY2hpbGROb2RlcyA9IHN0eWxlLmNoaWxkTm9kZXM7XG5cblx0XHRpZiAoY2hpbGROb2Rlc1tpbmRleF0pIHN0eWxlLnJlbW92ZUNoaWxkKGNoaWxkTm9kZXNbaW5kZXhdKTtcblxuXHRcdGlmIChjaGlsZE5vZGVzLmxlbmd0aCkge1xuXHRcdFx0c3R5bGUuaW5zZXJ0QmVmb3JlKGNzc05vZGUsIGNoaWxkTm9kZXNbaW5kZXhdKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0c3R5bGUuYXBwZW5kQ2hpbGQoY3NzTm9kZSk7XG5cdFx0fVxuXHR9XG59XG5cbmZ1bmN0aW9uIGFwcGx5VG9UYWcgKHN0eWxlLCBvYmopIHtcblx0dmFyIGNzcyA9IG9iai5jc3M7XG5cdHZhciBtZWRpYSA9IG9iai5tZWRpYTtcblxuXHRpZihtZWRpYSkge1xuXHRcdHN0eWxlLnNldEF0dHJpYnV0ZShcIm1lZGlhXCIsIG1lZGlhKVxuXHR9XG5cblx0aWYoc3R5bGUuc3R5bGVTaGVldCkge1xuXHRcdHN0eWxlLnN0eWxlU2hlZXQuY3NzVGV4dCA9IGNzcztcblx0fSBlbHNlIHtcblx0XHR3aGlsZShzdHlsZS5maXJzdENoaWxkKSB7XG5cdFx0XHRzdHlsZS5yZW1vdmVDaGlsZChzdHlsZS5maXJzdENoaWxkKTtcblx0XHR9XG5cblx0XHRzdHlsZS5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpKTtcblx0fVxufVxuXG5mdW5jdGlvbiB1cGRhdGVMaW5rIChsaW5rLCBvcHRpb25zLCBvYmopIHtcblx0dmFyIGNzcyA9IG9iai5jc3M7XG5cdHZhciBzb3VyY2VNYXAgPSBvYmouc291cmNlTWFwO1xuXG5cdC8qXG5cdFx0SWYgY29udmVydFRvQWJzb2x1dGVVcmxzIGlzbid0IGRlZmluZWQsIGJ1dCBzb3VyY2VtYXBzIGFyZSBlbmFibGVkXG5cdFx0YW5kIHRoZXJlIGlzIG5vIHB1YmxpY1BhdGggZGVmaW5lZCB0aGVuIGxldHMgdHVybiBjb252ZXJ0VG9BYnNvbHV0ZVVybHNcblx0XHRvbiBieSBkZWZhdWx0LiAgT3RoZXJ3aXNlIGRlZmF1bHQgdG8gdGhlIGNvbnZlcnRUb0Fic29sdXRlVXJscyBvcHRpb25cblx0XHRkaXJlY3RseVxuXHQqL1xuXHR2YXIgYXV0b0ZpeFVybHMgPSBvcHRpb25zLmNvbnZlcnRUb0Fic29sdXRlVXJscyA9PT0gdW5kZWZpbmVkICYmIHNvdXJjZU1hcDtcblxuXHRpZiAob3B0aW9ucy5jb252ZXJ0VG9BYnNvbHV0ZVVybHMgfHwgYXV0b0ZpeFVybHMpIHtcblx0XHRjc3MgPSBmaXhVcmxzKGNzcyk7XG5cdH1cblxuXHRpZiAoc291cmNlTWFwKSB7XG5cdFx0Ly8gaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL2EvMjY2MDM4NzVcblx0XHRjc3MgKz0gXCJcXG4vKiMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247YmFzZTY0LFwiICsgYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc291cmNlTWFwKSkpKSArIFwiICovXCI7XG5cdH1cblxuXHR2YXIgYmxvYiA9IG5ldyBCbG9iKFtjc3NdLCB7IHR5cGU6IFwidGV4dC9jc3NcIiB9KTtcblxuXHR2YXIgb2xkU3JjID0gbGluay5ocmVmO1xuXG5cdGxpbmsuaHJlZiA9IFVSTC5jcmVhdGVPYmplY3RVUkwoYmxvYik7XG5cblx0aWYob2xkU3JjKSBVUkwucmV2b2tlT2JqZWN0VVJMKG9sZFNyYyk7XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlcy5qc1xuLy8gbW9kdWxlIGlkID0gOVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJcbi8qKlxuICogV2hlbiBzb3VyY2UgbWFwcyBhcmUgZW5hYmxlZCwgYHN0eWxlLWxvYWRlcmAgdXNlcyBhIGxpbmsgZWxlbWVudCB3aXRoIGEgZGF0YS11cmkgdG9cbiAqIGVtYmVkIHRoZSBjc3Mgb24gdGhlIHBhZ2UuIFRoaXMgYnJlYWtzIGFsbCByZWxhdGl2ZSB1cmxzIGJlY2F1c2Ugbm93IHRoZXkgYXJlIHJlbGF0aXZlIHRvIGFcbiAqIGJ1bmRsZSBpbnN0ZWFkIG9mIHRoZSBjdXJyZW50IHBhZ2UuXG4gKlxuICogT25lIHNvbHV0aW9uIGlzIHRvIG9ubHkgdXNlIGZ1bGwgdXJscywgYnV0IHRoYXQgbWF5IGJlIGltcG9zc2libGUuXG4gKlxuICogSW5zdGVhZCwgdGhpcyBmdW5jdGlvbiBcImZpeGVzXCIgdGhlIHJlbGF0aXZlIHVybHMgdG8gYmUgYWJzb2x1dGUgYWNjb3JkaW5nIHRvIHRoZSBjdXJyZW50IHBhZ2UgbG9jYXRpb24uXG4gKlxuICogQSBydWRpbWVudGFyeSB0ZXN0IHN1aXRlIGlzIGxvY2F0ZWQgYXQgYHRlc3QvZml4VXJscy5qc2AgYW5kIGNhbiBiZSBydW4gdmlhIHRoZSBgbnBtIHRlc3RgIGNvbW1hbmQuXG4gKlxuICovXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGNzcykge1xuICAvLyBnZXQgY3VycmVudCBsb2NhdGlvblxuICB2YXIgbG9jYXRpb24gPSB0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiICYmIHdpbmRvdy5sb2NhdGlvbjtcblxuICBpZiAoIWxvY2F0aW9uKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKFwiZml4VXJscyByZXF1aXJlcyB3aW5kb3cubG9jYXRpb25cIik7XG4gIH1cblxuXHQvLyBibGFuayBvciBudWxsP1xuXHRpZiAoIWNzcyB8fCB0eXBlb2YgY3NzICE9PSBcInN0cmluZ1wiKSB7XG5cdCAgcmV0dXJuIGNzcztcbiAgfVxuXG4gIHZhciBiYXNlVXJsID0gbG9jYXRpb24ucHJvdG9jb2wgKyBcIi8vXCIgKyBsb2NhdGlvbi5ob3N0O1xuICB2YXIgY3VycmVudERpciA9IGJhc2VVcmwgKyBsb2NhdGlvbi5wYXRobmFtZS5yZXBsYWNlKC9cXC9bXlxcL10qJC8sIFwiL1wiKTtcblxuXHQvLyBjb252ZXJ0IGVhY2ggdXJsKC4uLilcblx0Lypcblx0VGhpcyByZWd1bGFyIGV4cHJlc3Npb24gaXMganVzdCBhIHdheSB0byByZWN1cnNpdmVseSBtYXRjaCBicmFja2V0cyB3aXRoaW5cblx0YSBzdHJpbmcuXG5cblx0IC91cmxcXHMqXFwoICA9IE1hdGNoIG9uIHRoZSB3b3JkIFwidXJsXCIgd2l0aCBhbnkgd2hpdGVzcGFjZSBhZnRlciBpdCBhbmQgdGhlbiBhIHBhcmVuc1xuXHQgICAoICA9IFN0YXJ0IGEgY2FwdHVyaW5nIGdyb3VwXG5cdCAgICAgKD86ICA9IFN0YXJ0IGEgbm9uLWNhcHR1cmluZyBncm91cFxuXHQgICAgICAgICBbXikoXSAgPSBNYXRjaCBhbnl0aGluZyB0aGF0IGlzbid0IGEgcGFyZW50aGVzZXNcblx0ICAgICAgICAgfCAgPSBPUlxuXHQgICAgICAgICBcXCggID0gTWF0Y2ggYSBzdGFydCBwYXJlbnRoZXNlc1xuXHQgICAgICAgICAgICAgKD86ICA9IFN0YXJ0IGFub3RoZXIgbm9uLWNhcHR1cmluZyBncm91cHNcblx0ICAgICAgICAgICAgICAgICBbXikoXSsgID0gTWF0Y2ggYW55dGhpbmcgdGhhdCBpc24ndCBhIHBhcmVudGhlc2VzXG5cdCAgICAgICAgICAgICAgICAgfCAgPSBPUlxuXHQgICAgICAgICAgICAgICAgIFxcKCAgPSBNYXRjaCBhIHN0YXJ0IHBhcmVudGhlc2VzXG5cdCAgICAgICAgICAgICAgICAgICAgIFteKShdKiAgPSBNYXRjaCBhbnl0aGluZyB0aGF0IGlzbid0IGEgcGFyZW50aGVzZXNcblx0ICAgICAgICAgICAgICAgICBcXCkgID0gTWF0Y2ggYSBlbmQgcGFyZW50aGVzZXNcblx0ICAgICAgICAgICAgICkgID0gRW5kIEdyb3VwXG4gICAgICAgICAgICAgICpcXCkgPSBNYXRjaCBhbnl0aGluZyBhbmQgdGhlbiBhIGNsb3NlIHBhcmVuc1xuICAgICAgICAgICkgID0gQ2xvc2Ugbm9uLWNhcHR1cmluZyBncm91cFxuICAgICAgICAgICogID0gTWF0Y2ggYW55dGhpbmdcbiAgICAgICApICA9IENsb3NlIGNhcHR1cmluZyBncm91cFxuXHQgXFwpICA9IE1hdGNoIGEgY2xvc2UgcGFyZW5zXG5cblx0IC9naSAgPSBHZXQgYWxsIG1hdGNoZXMsIG5vdCB0aGUgZmlyc3QuICBCZSBjYXNlIGluc2Vuc2l0aXZlLlxuXHQgKi9cblx0dmFyIGZpeGVkQ3NzID0gY3NzLnJlcGxhY2UoL3VybFxccypcXCgoKD86W14pKF18XFwoKD86W14pKF0rfFxcKFteKShdKlxcKSkqXFwpKSopXFwpL2dpLCBmdW5jdGlvbihmdWxsTWF0Y2gsIG9yaWdVcmwpIHtcblx0XHQvLyBzdHJpcCBxdW90ZXMgKGlmIHRoZXkgZXhpc3QpXG5cdFx0dmFyIHVucXVvdGVkT3JpZ1VybCA9IG9yaWdVcmxcblx0XHRcdC50cmltKClcblx0XHRcdC5yZXBsYWNlKC9eXCIoLiopXCIkLywgZnVuY3Rpb24obywgJDEpeyByZXR1cm4gJDE7IH0pXG5cdFx0XHQucmVwbGFjZSgvXicoLiopJyQvLCBmdW5jdGlvbihvLCAkMSl7IHJldHVybiAkMTsgfSk7XG5cblx0XHQvLyBhbHJlYWR5IGEgZnVsbCB1cmw/IG5vIGNoYW5nZVxuXHRcdGlmICgvXigjfGRhdGE6fGh0dHA6XFwvXFwvfGh0dHBzOlxcL1xcL3xmaWxlOlxcL1xcL1xcL3xcXHMqJCkvaS50ZXN0KHVucXVvdGVkT3JpZ1VybCkpIHtcblx0XHQgIHJldHVybiBmdWxsTWF0Y2g7XG5cdFx0fVxuXG5cdFx0Ly8gY29udmVydCB0aGUgdXJsIHRvIGEgZnVsbCB1cmxcblx0XHR2YXIgbmV3VXJsO1xuXG5cdFx0aWYgKHVucXVvdGVkT3JpZ1VybC5pbmRleE9mKFwiLy9cIikgPT09IDApIHtcblx0XHQgIFx0Ly9UT0RPOiBzaG91bGQgd2UgYWRkIHByb3RvY29sP1xuXHRcdFx0bmV3VXJsID0gdW5xdW90ZWRPcmlnVXJsO1xuXHRcdH0gZWxzZSBpZiAodW5xdW90ZWRPcmlnVXJsLmluZGV4T2YoXCIvXCIpID09PSAwKSB7XG5cdFx0XHQvLyBwYXRoIHNob3VsZCBiZSByZWxhdGl2ZSB0byB0aGUgYmFzZSB1cmxcblx0XHRcdG5ld1VybCA9IGJhc2VVcmwgKyB1bnF1b3RlZE9yaWdVcmw7IC8vIGFscmVhZHkgc3RhcnRzIHdpdGggJy8nXG5cdFx0fSBlbHNlIHtcblx0XHRcdC8vIHBhdGggc2hvdWxkIGJlIHJlbGF0aXZlIHRvIGN1cnJlbnQgZGlyZWN0b3J5XG5cdFx0XHRuZXdVcmwgPSBjdXJyZW50RGlyICsgdW5xdW90ZWRPcmlnVXJsLnJlcGxhY2UoL15cXC5cXC8vLCBcIlwiKTsgLy8gU3RyaXAgbGVhZGluZyAnLi8nXG5cdFx0fVxuXG5cdFx0Ly8gc2VuZCBiYWNrIHRoZSBmaXhlZCB1cmwoLi4uKVxuXHRcdHJldHVybiBcInVybChcIiArIEpTT04uc3RyaW5naWZ5KG5ld1VybCkgKyBcIilcIjtcblx0fSk7XG5cblx0Ly8gc2VuZCBiYWNrIHRoZSBmaXhlZCBjc3Ncblx0cmV0dXJuIGZpeGVkQ3NzO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3N0eWxlLWxvYWRlci9saWIvdXJscy5qc1xuLy8gbW9kdWxlIGlkID0gMTBcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwidmFyIGRpc3Bvc2VkID0gZmFsc2VcbnZhciBub3JtYWxpemVDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKVxuLyogc2NyaXB0ICovXG5leHBvcnQgKiBmcm9tIFwiISFiYWJlbC1sb2FkZXIhLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Db21EZXBhcnR1cmUudnVlXCJcbmltcG9ydCBfX3Z1ZV9zY3JpcHRfXyBmcm9tIFwiISFiYWJlbC1sb2FkZXIhLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Db21EZXBhcnR1cmUudnVlXCJcbi8qIHRlbXBsYXRlICovXG5pbXBvcnQgX192dWVfdGVtcGxhdGVfXyBmcm9tIFwiISEuLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1lYjMzNWNjZVxcXCIsXFxcImhhc1Njb3BlZFxcXCI6ZmFsc2UsXFxcImJ1YmxlXFxcIjp7XFxcInRyYW5zZm9ybXNcXFwiOnt9fX0hLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0NvbURlcGFydHVyZS52dWVcIlxuLyogdGVtcGxhdGUgZnVuY3Rpb25hbCAqL1xudmFyIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyA9IGZhbHNlXG4vKiBzdHlsZXMgKi9cbnZhciBfX3Z1ZV9zdHlsZXNfXyA9IG51bGxcbi8qIHNjb3BlSWQgKi9cbnZhciBfX3Z1ZV9zY29wZUlkX18gPSBudWxsXG4vKiBtb2R1bGVJZGVudGlmaWVyIChzZXJ2ZXIgb25seSkgKi9cbnZhciBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fID0gbnVsbFxudmFyIENvbXBvbmVudCA9IG5vcm1hbGl6ZUNvbXBvbmVudChcbiAgX192dWVfc2NyaXB0X18sXG4gIF9fdnVlX3RlbXBsYXRlX18sXG4gIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyxcbiAgX192dWVfc3R5bGVzX18sXG4gIF9fdnVlX3Njb3BlSWRfXyxcbiAgX192dWVfbW9kdWxlX2lkZW50aWZpZXJfX1xuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmNcXFxcQ29tRGVwYXJ0dXJlLnZ1ZVwiXG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi1lYjMzNWNjZVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LWViMzM1Y2NlXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG4gIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbiAoZGF0YSkge1xuICAgIGRpc3Bvc2VkID0gdHJ1ZVxuICB9KVxufSkoKX1cblxuZXhwb3J0IGRlZmF1bHQgQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL0NvbURlcGFydHVyZS52dWVcbi8vIG1vZHVsZSBpZCA9IDExXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJyb3dcIiB9LCBbXG4gICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbGctMyBjb2wtc20tNlwiIH0sIFtcbiAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiaDVcIiB9LCBbXG4gICAgICAgIF92bS5fdihcItCf0L7QuNGB0Log0YLRg9GA0L7QsiDQuNC3XFxuXFx0XFx0XFx0XCIpLFxuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImRyb3Bkb3duIGQtaW5saW5lLWJsb2NrXCIgfSwgW1xuICAgICAgICAgIF9jKFxuICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcInRleHQtdXBwZXJjYXNlIGRpc3BsYXktNyBkZXBhcnR1cmVfd3JhcFwiLFxuICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgIGhyZWY6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsXG4gICAgICAgICAgICAgICAgaWQ6IFwiZGVwYXJ0dXJlVmFsdWVcIixcbiAgICAgICAgICAgICAgICBcImRhdGEtdG9nZ2xlXCI6IFwiZHJvcGRvd25cIixcbiAgICAgICAgICAgICAgICBcImFyaWEtaGFzcG9wdXBcIjogXCJ0cnVlXCIsXG4gICAgICAgICAgICAgICAgXCJhcmlhLWV4cGFuZGVkXCI6IFwiZmFsc2VcIlxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgW192bS5fdihfdm0uX3MoX3ZtLmNpdHlGcm9tTmFtZSkpXVxuICAgICAgICAgICksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcbiAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImRyb3Bkb3duLW1lbnUgc2VsZWN0LWRyb3Bkb3duIGJnLWxpZ2h0XCIsXG4gICAgICAgICAgICAgIGF0dHJzOiB7IFwiYXJpYS1sYWJlbGxlZGJ5XCI6IFwiZGVwYXJ0dXJlVmFsdWVcIiB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgW1xuICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgIHsgc3RhdGljQ2xhc3M6IFwic2Nyb2xsYWJsZS1tZW51XCIgfSxcbiAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgXCJzcGFuXCIsXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJkcm9wZG93bi1pdGVtXCIsXG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3M6IHsgXCJiZy1wcmltYXJ5IHRleHQtd2hpdGVcIjogX3ZtLmRlcGFydHVyZSA9PSA5OSB9LFxuICAgICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5zZXREZXBhcnR1cmUoX3ZtLm5vbl9mbGlnaHQpXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KFwi0JHQtdC3INC/0LXRgNC10LvQtdGC0LBcIildXG4gICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgIF92bS5fbChfdm0uZ3JvdXBlZERlcGFydHVyZXMsIGZ1bmN0aW9uKGl0ZW0sIGxldHRlcikge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gX2MoXG4gICAgICAgICAgICAgICAgICAgICAgXCJzcGFuXCIsXG4gICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJoNlwiLCB7IHN0YXRpY0NsYXNzOiBcImRyb3Bkb3duLWhlYWRlclwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KF92bS5fcyhsZXR0ZXIpKVxuICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9sKGl0ZW0sIGZ1bmN0aW9uKGRlcGFydHVyZUl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGRlcGFydHVyZUl0ZW0uaWQgIT0gOTlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA/IF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInNwYW5cIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImRyb3Bkb3duLWl0ZW1cIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJiZy1wcmltYXJ5IHRleHQtd2hpdGVcIjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVwYXJ0dXJlSXRlbS5pZCA9PSBfdm0uZGVwYXJ0dXJlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uc2V0RGVwYXJ0dXJlKGRlcGFydHVyZUl0ZW0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9zKGRlcGFydHVyZUl0ZW0ubmFtZSkgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBfdm0uX2UoKVxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgIDJcbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgIDJcbiAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgXVxuICAgICAgICAgIClcbiAgICAgICAgXSlcbiAgICAgIF0pXG4gICAgXSksXG4gICAgX3ZtLl92KFwiIFwiKSxcbiAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1sZy02IGNvbC1zbS02IHB0LTJcIiB9LCBbXG4gICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImZvcm0tY2hlY2tcIiB9LCBbXG4gICAgICAgIF9jKFwiaW5wdXRcIiwge1xuICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgICAgICAgdmFsdWU6IF92bS5yZWd1bGFyLFxuICAgICAgICAgICAgICBleHByZXNzaW9uOiBcInJlZ3VsYXJcIlxuICAgICAgICAgICAgfVxuICAgICAgICAgIF0sXG4gICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jaGVjay1pbnB1dFwiLFxuICAgICAgICAgIGF0dHJzOiB7IHR5cGU6IFwiY2hlY2tib3hcIiwgdmFsdWU6IFwiXCIsIGlkOiBcImhpZGVSZWd1bGFyXCIgfSxcbiAgICAgICAgICBkb21Qcm9wczoge1xuICAgICAgICAgICAgY2hlY2tlZDogQXJyYXkuaXNBcnJheShfdm0ucmVndWxhcilcbiAgICAgICAgICAgICAgPyBfdm0uX2koX3ZtLnJlZ3VsYXIsIFwiXCIpID4gLTFcbiAgICAgICAgICAgICAgOiBfdm0ucmVndWxhclxuICAgICAgICAgIH0sXG4gICAgICAgICAgb246IHtcbiAgICAgICAgICAgIGNoYW5nZTogW1xuICAgICAgICAgICAgICBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICB2YXIgJCRhID0gX3ZtLnJlZ3VsYXIsXG4gICAgICAgICAgICAgICAgICAkJGVsID0gJGV2ZW50LnRhcmdldCxcbiAgICAgICAgICAgICAgICAgICQkYyA9ICQkZWwuY2hlY2tlZCA/IHRydWUgOiBmYWxzZVxuICAgICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KCQkYSkpIHtcbiAgICAgICAgICAgICAgICAgIHZhciAkJHYgPSBcIlwiLFxuICAgICAgICAgICAgICAgICAgICAkJGkgPSBfdm0uX2koJCRhLCAkJHYpXG4gICAgICAgICAgICAgICAgICBpZiAoJCRlbC5jaGVja2VkKSB7XG4gICAgICAgICAgICAgICAgICAgICQkaSA8IDAgJiYgKF92bS5yZWd1bGFyID0gJCRhLmNvbmNhdChbJCR2XSkpXG4gICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAkJGkgPiAtMSAmJlxuICAgICAgICAgICAgICAgICAgICAgIChfdm0ucmVndWxhciA9ICQkYVxuICAgICAgICAgICAgICAgICAgICAgICAgLnNsaWNlKDAsICQkaSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC5jb25jYXQoJCRhLnNsaWNlKCQkaSArIDEpKSlcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgX3ZtLnJlZ3VsYXIgPSAkJGNcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgIF92bS5zZXRSZWd1bGFyKClcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH1cbiAgICAgICAgfSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFxuICAgICAgICAgIFwibGFiZWxcIixcbiAgICAgICAgICB7XG4gICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNoZWNrLWxhYmVsIHBsLTFcIixcbiAgICAgICAgICAgIGF0dHJzOiB7IGZvcjogXCJoaWRlUmVndWxhclwiIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHRcXHTQodC60YDRi9GC0Ywg0YDQtdCz0YPQu9GP0YDQvdGL0LUg0YDQtdC50YHRi1xcblxcdFxcdFxcdFwiKV1cbiAgICAgICAgKVxuICAgICAgXSlcbiAgICBdKVxuICBdKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtdXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbnZhciBlc0V4cG9ydHMgPSB7IHJlbmRlcjogcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnM6IHN0YXRpY1JlbmRlckZucyB9XG5leHBvcnQgZGVmYXVsdCBlc0V4cG9ydHNcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikgICAgICAucmVyZW5kZXIoXCJkYXRhLXYtZWIzMzVjY2VcIiwgZXNFeHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi1lYjMzNWNjZVwiLFwiaGFzU2NvcGVkXCI6ZmFsc2UsXCJidWJsZVwiOntcInRyYW5zZm9ybXNcIjp7fX19IS4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3NyYy9Db21EZXBhcnR1cmUudnVlXG4vLyBtb2R1bGUgaWQgPSAxMlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJ2YXIgZGlzcG9zZWQgPSBmYWxzZVxudmFyIG5vcm1hbGl6ZUNvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpXG4vKiBzY3JpcHQgKi9cbmV4cG9ydCAqIGZyb20gXCIhIWJhYmVsLWxvYWRlciEuLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0NvbVJlc3VsdHMudnVlXCJcbmltcG9ydCBfX3Z1ZV9zY3JpcHRfXyBmcm9tIFwiISFiYWJlbC1sb2FkZXIhLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Db21SZXN1bHRzLnZ1ZVwiXG4vKiB0ZW1wbGF0ZSAqL1xuaW1wb3J0IF9fdnVlX3RlbXBsYXRlX18gZnJvbSBcIiEhLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNTU0M2QyZGJcXFwiLFxcXCJoYXNTY29wZWRcXFwiOmZhbHNlLFxcXCJidWJsZVxcXCI6e1xcXCJ0cmFuc2Zvcm1zXFxcIjp7fX19IS4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Db21SZXN1bHRzLnZ1ZVwiXG4vKiB0ZW1wbGF0ZSBmdW5jdGlvbmFsICovXG52YXIgX192dWVfdGVtcGxhdGVfZnVuY3Rpb25hbF9fID0gZmFsc2Vcbi8qIHN0eWxlcyAqL1xudmFyIF9fdnVlX3N0eWxlc19fID0gbnVsbFxuLyogc2NvcGVJZCAqL1xudmFyIF9fdnVlX3Njb3BlSWRfXyA9IG51bGxcbi8qIG1vZHVsZUlkZW50aWZpZXIgKHNlcnZlciBvbmx5KSAqL1xudmFyIF9fdnVlX21vZHVsZV9pZGVudGlmaWVyX18gPSBudWxsXG52YXIgQ29tcG9uZW50ID0gbm9ybWFsaXplQ29tcG9uZW50KFxuICBfX3Z1ZV9zY3JpcHRfXyxcbiAgX192dWVfdGVtcGxhdGVfXyxcbiAgX192dWVfdGVtcGxhdGVfZnVuY3Rpb25hbF9fLFxuICBfX3Z1ZV9zdHlsZXNfXyxcbiAgX192dWVfc2NvcGVJZF9fLFxuICBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInNyY1xcXFxDb21SZXN1bHRzLnZ1ZVwiXG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi01NTQzZDJkYlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTU1NDNkMmRiXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG4gIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbiAoZGF0YSkge1xuICAgIGRpc3Bvc2VkID0gdHJ1ZVxuICB9KVxufSkoKX1cblxuZXhwb3J0IGRlZmF1bHQgQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL0NvbVJlc3VsdHMudnVlXG4vLyBtb2R1bGUgaWQgPSAxM1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwidWxcIiwgeyBzdGF0aWNDbGFzczogXCJsaXN0LXVuc3R5bGVkXCIgfSwgW1xuICAgIF92bS5yZXN1bHRzRGF0YS5sZW5ndGggPiAwXG4gICAgICA/IF9jKFxuICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgX3ZtLl9sKF92bS5ncm91cHBlZFJlc3VsdHMsIGZ1bmN0aW9uKGhvdGVsLCBpbmRleCkge1xuICAgICAgICAgICAgcmV0dXJuIF9jKFxuICAgICAgICAgICAgICBcImxpXCIsXG4gICAgICAgICAgICAgIHsgc3RhdGljQ2xhc3M6IFwiYW5pbWF0ZWQgZmFkZUluUmlnaHQgbWItMiBwdC0yIHBiLTJcIiB9LFxuICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgaG90ZWwudHlwZSA9PT0gXCJyZXN1bHRcIlxuICAgICAgICAgICAgICAgICAgPyBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNhcmQgYmctbGlnaHRcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJjYXJkLWJvZHkgYmctd2hpdGUgcm93IG5vLWd1dHRlcnNcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljU3R5bGU6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImJveC1zaGFkb3dcIjogXCIwcHggMXB4IDNweCAxcHggI2JmYmZiZlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiei1pbmRleFwiOiBcIjFcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX20oMCwgdHJ1ZSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiY29sLWF1dG8gY29sLW1kLTYgY29sLXhsLWF1dG8gdGV4dC1jZW50ZXIgbWItMyBtYi1tZC0wIGFsaWduLXNlbGYtY2VudGVyIHByLTNcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpbWdcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBzcmM6IGhvdGVsLnBpY3R1cmVsaW5rLCBhbHQ6IFwiXCIgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiY29sLTEyIGNvbC1tZC02IGNvbC14bC0zICBhbGlnbi1zZWxmLWNlbnRlciBwci0zXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sIHBsLTBcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaDRcIiwgeyBzdGF0aWNDbGFzczogXCJkaXNwbGF5LTggbWItMFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiYVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldDogXCJfYmxhbmtcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBocmVmOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIvaG90ZWwvXCIgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaG90ZWwuaG90ZWxjb2RlICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiLz9cIiArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uc2VhcmNoVXJsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KF92bS5fcyhob3RlbC5ob3RlbG5hbWUpKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBzdGF0aWNDbGFzczogXCJjb2wgcGwtMFwiIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fbChwYXJzZUludChob3RlbC5ob3RlbHN0YXJzKSwgZnVuY3Rpb24obikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfYyhcImlcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtc3RhciB0ZXh0LXdhcm5pbmdcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhcnNlSW50KGhvdGVsLmhvdGVscmF0aW5nKSA+IDBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPyBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbCBwbC0wXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwic3BhblwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3M6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidGV4dC1zdWNjZXNzXCI6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhvdGVsLmhvdGVscmF0aW5nID49IDQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRleHQtd2FybmluZ1wiOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBob3RlbC5ob3RlbHJhdGluZyA+PSAzICYmXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhvdGVsLmhvdGVscmF0aW5nIDwgNCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidGV4dC1kYW5nZXJcIjogaG90ZWwuaG90ZWxyYXRpbmcgPCAzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImZhXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImZhLXRodW1icy11cFwiOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhvdGVsLmhvdGVscmF0aW5nID49IDQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZmEtdGh1bWJzLW8tdXBcIjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBob3RlbC5ob3RlbHJhdGluZyA+PSAzICYmXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaG90ZWwuaG90ZWxyYXRpbmcgPCA0LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImZhLXRodW1icy1kb3duXCI6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaG90ZWwuaG90ZWxyYXRpbmcgPCAzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcIiArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fcyhob3RlbC5ob3RlbHJhdGluZylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIvNVxcblxcdFxcdFxcdFxcdFxcdFxcdFwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogX3ZtLl9lKCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wgcGwtMFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJzbWFsbFwiLCB7IHN0YXRpY0NsYXNzOiBcInRleHQtbXV0ZWRcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9zKGhvdGVsLnJlZ2lvbm5hbWUpICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIsIFwiICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9zKGhvdGVsLmNvdW50cnluYW1lKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImNvbC0xMiBjb2wtbWQtNiBjb2wteGwtMyBhbGlnbi1zZWxmLXN0YXJ0IGFsaWduLXNlbGYtbWQtZW5kIGFsaWduLXNlbGYteGwtY2VudGVyIHByLTNcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkbFwiLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZHRcIiwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwi0JLRi9C70LXRgtGLIFwiICsgX3ZtLl9zKGhvdGVsLm1pbkZseWRhdGUpICsgXCLCoFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBob3RlbC5taW5GbHlkYXRlICE9PSBob3RlbC5tYXhGbHlkYXRlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA/IF9jKFwic3BhblwiLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIOKAlMKgXCIgKyBfdm0uX3MoaG90ZWwubWF4Rmx5ZGF0ZSkpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IF92bS5fZSgpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImRkXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBob3RlbC5taW5OaWdodCA9PSBob3RlbC5tYXhOaWdodFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPyBfYyhcInNwYW5cIiwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwi0L3QsCBcIiArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fcyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX2YoXCJkaXNwbGF5TmlnaHRzXCIpKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaG90ZWwubWluTmlnaHRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IF92bS5fZSgpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaG90ZWwubWluTmlnaHQgIT0gaG90ZWwubWF4TmlnaHRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gX2MoXCJzcGFuXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCLQvdCwIFwiICsgX3ZtLl9zKGhvdGVsLm1pbk5pZ2h0KSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogX3ZtLl9lKCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBob3RlbC5taW5OaWdodCAhPT0gaG90ZWwubWF4TmlnaHRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gX2MoXCJzcGFuXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIsKg4oCUwqAgXCIgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9mKFwiZGlzcGxheU5pZ2h0c1wiKShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhvdGVsLm1heE5pZ2h0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBfdm0uX2UoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiYnJcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInNwYW5cIiwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3MoaG90ZWwudG91cnMudG91clswXS5hZHVsdHMpICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIiDQstC30YAuIFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFyc2VJbnQoaG90ZWwudG91cnMudG91clswXS5jaGlsZCkgPiAwXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gX2MoXCJzcGFuXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIrIFwiICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBob3RlbC50b3Vycy50b3VyWzBdLmNoaWxkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIg0YDQtdCxLlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBfdm0uX2UoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkLW5vbmUgZC14bC1ibG9jayBjb2wteGwtMiBhbGlnbi1zZWxmLXN0YXJ0IGFsaWduLXNlbGYtbWQtZW5kIGFsaWduLXNlbGYteGwtY2VudGVyIHByLTMgaG90ZWwtbWluaS1kZXNjcmlwdGlvblwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcImJvdHRvbVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRhdGEtdG9nZ2xlXCI6IFwicG9wb3ZlclwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogXCLQntC/0LjRgdCw0L3QuNC1XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGF0YS1jb250ZW50XCI6IGhvdGVsLmhvdGVsZGVzY3JpcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJcXG5cXHRcXHRcXHRcXHRcXHRcXHRcIiArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9zKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9mKFwic2hvcnRUZXh0XCIpKGhvdGVsLmhvdGVsZGVzY3JpcHRpb24pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJcXG5cXHRcXHRcXHRcXHRcXHRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJjb2wtMTIgY29sLW1kLTYgY29sLXhsLWF1dG8gdGV4dC1yaWdodCBhbGlnbi1zZWxmLXN0YXJ0IG1sLW1kLWF1dG9cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9tKDEsIHRydWUpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwidGV4dC1tdXRlZCBoNSBtYi0wXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInNtYWxsXCIsIFtfdm0uX3YoXCLQsdC10Lcg0YHQutC40LTQutC4XCIpXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwic1wiLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9zKF92bS5fZihcImdldE9sZFByaWNlXCIpKGhvdGVsLnByaWNlKSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInNwYW5cIiwgeyBzdGF0aWNDbGFzczogXCJoNVwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fZihcImdldENyZWRpdFByaWNlXCIpKGhvdGVsLnByaWNlKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwic21hbGxcIiwgW192bS5fdihcItCyINC60YDQtdC00LjRgiA2INC80LXRgS5cIildKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LXByaW1hcnkgaDVcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0ub3BlblRvdXJzTGlzdChob3RlbC5ob3RlbGNvZGUpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImFcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgYXR0cnM6IHsgaHJlZjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIiB9IH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwic3BhblwiLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9zKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX2YoXCJmb3JtYXRQcmljZVwiKShob3RlbC5wcmljZSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIg0LfQsCDRgtGD0YAgXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwibXQtMlwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibGQtZXh0LWxlZnQgYnRuIGJ0bi1pbmZvIGJ0bi1ibG9ja1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaHJlZjogXCJqYXZhc2NyaXB0OnZvaWQoMCk7XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBcImhvdGVsQnRuLVwiICsgaG90ZWwuaG90ZWxjb2RlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5vcGVuSG90ZWxEZXNjcihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhvdGVsLmhvdGVsY29kZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXNcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwic3BhblwiLCBbX3ZtLl92KFwi0J7QsSDQvtGC0LXQu9C1XCIpXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJsZCBsZC1yaW5nIGxkLXNwaW5cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwibXQtMlwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1vcmFuZ2UgYnRuLWJsb2NrXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBocmVmOiBcImphdmFzY3JpcHQ6dm9pZCgwKTtcIiB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5vcGVuVG91cnNMaXN0KGhvdGVsLmhvdGVsY29kZSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwi0JLRgdC1INCy0LDRgNC40LDQvdGC0YsgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2hldnJvbi1kb3duXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICBfdm0ub2JIb3RlbHNEZXNjcltob3RlbC5ob3RlbGNvZGVdICE9PSB1bmRlZmluZWRcbiAgICAgICAgICAgICAgICAgICAgICAgID8gX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXJlY3RpdmVzOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLm9iSG90ZWxzRGVzY3JbaG90ZWwuaG90ZWxjb2RlXSAhPT1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVuZGVmaW5lZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJvYkhvdGVsc0Rlc2NyW2hvdGVsLmhvdGVsY29kZV0gIT09IHVuZGVmaW5lZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJwLTMgaG90ZWwtZGVzY3JpcHRpb25cIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY1N0eWxlOiB7IGRpc3BsYXk6IFwibm9uZVwiIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBpZDogXCJkZXNjci1cIiArIGhvdGVsLmhvdGVsY29kZSB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXJzZUludChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLm9iSG90ZWxzRGVzY3JbaG90ZWwuaG90ZWxjb2RlXS5pbWFnZXNjb3VudFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSA+IDBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPyBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwicm93IGhvdGVsLXBob3RvcyBkLWZsZXgganVzdGlmeS1jb250ZW50LWJldHdlZW5cIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogXCJob3RlbFBob3Rvc19cIiArIGhvdGVsLmhvdGVsY29kZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9sKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0ub2JIb3RlbHNEZXNjcltob3RlbC5ob3RlbGNvZGVdLmltYWdlc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5pbWFnZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24oaW1hZ2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gX2MoXCJkaXZcIiwge30sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImFcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBocmVmOiBpbWFnZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGF0YS1mYW5jeWJveFwiOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImdhbGxlcnkxXCIgKyBob3RlbC5ob3RlbGNvZGVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImltZ1wiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJpbWctdGh1bWJuYWlsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBzcmM6IGltYWdlLCBhbHQ6IFwiXCIgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IF92bS5fZSgpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicm93XCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC0xMlwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwicFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXJlY3RpdmVzOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5vYkhvdGVsc0Rlc2NyW2hvdGVsLmhvdGVsY29kZV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZGVzY3JpcHRpb24sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIm9iSG90ZWxzRGVzY3JbaG90ZWwuaG90ZWxjb2RlXS5kZXNjcmlwdGlvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9zKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLm9iSG90ZWxzRGVzY3JbaG90ZWwuaG90ZWxjb2RlXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZGVzY3JpcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInBcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlyZWN0aXZlczogW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiAhX3ZtLm9iSG90ZWxzRGVzY3JbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhvdGVsLmhvdGVsY29kZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXS5kZXNjcmlwdGlvbixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiIW9iSG90ZWxzRGVzY3JbaG90ZWwuaG90ZWxjb2RlXS5kZXNjcmlwdGlvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCLQmNC30LLQuNC90LjRgtC1LCDQvtC/0LjRgdCw0L3QuNC1INC+0YLQtdC70Y8g0L3QtdC00L7RgdGC0YPQv9C90L5cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidWxcIiwgeyBzdGF0aWNDbGFzczogXCJsaXN0LXVuc3R5bGVkXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibGlcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5vYkhvdGVsc0Rlc2NyW2hvdGVsLmhvdGVsY29kZV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5pbnJvb20sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJvYkhvdGVsc0Rlc2NyW2hvdGVsLmhvdGVsY29kZV0uaW5yb29tXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInN0cm9uZ1wiLCBbX3ZtLl92KFwi0JIg0LrQvtC80L3QsNGC0LU6XCIpXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIgXCIgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLm9iSG90ZWxzRGVzY3JbaG90ZWwuaG90ZWxjb2RlXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmlucm9vbVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImxpXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXJlY3RpdmVzOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0ub2JIb3RlbHNEZXNjcltob3RlbC5ob3RlbGNvZGVdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAudGVycml0b3J5LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwib2JIb3RlbHNEZXNjcltob3RlbC5ob3RlbGNvZGVdLnRlcnJpdG9yeVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJzdHJvbmdcIiwgW192bS5fdihcItCi0LXRgNGA0LjRgtC+0YDQuNGPOlwiKV0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiIFwiICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9zKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5vYkhvdGVsc0Rlc2NyW2hvdGVsLmhvdGVsY29kZV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50ZXJyaXRvcnlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJsaVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlyZWN0aXZlczogW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLm9iSG90ZWxzRGVzY3JbaG90ZWwuaG90ZWxjb2RlXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmJlYWNoLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwib2JIb3RlbHNEZXNjcltob3RlbC5ob3RlbGNvZGVdLmJlYWNoXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInN0cm9uZ1wiLCBbX3ZtLl92KFwi0J/Qu9GP0LY6XCIpXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIgXCIgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLm9iSG90ZWxzRGVzY3JbaG90ZWwuaG90ZWxjb2RlXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmJlYWNoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibGlcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5vYkhvdGVsc0Rlc2NyW2hvdGVsLmhvdGVsY29kZV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5wbGFjZW1lbnQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJvYkhvdGVsc0Rlc2NyW2hvdGVsLmhvdGVsY29kZV0ucGxhY2VtZW50XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInN0cm9uZ1wiLCBbX3ZtLl92KFwi0KDQsNGB0L/QvtC70L7QttC10L3QuNC1OlwiKV0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiIFwiICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9zKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5vYkhvdGVsc0Rlc2NyW2hvdGVsLmhvdGVsY29kZV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5wbGFjZW1lbnRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJyb3cgZC1mbGV4IHJvdyBiZy1pbmZvIHB0LTIgcGItMiB0ZXh0LXdoaXRlIGFsaWduLWl0ZW1zLWNlbnRlciBtdC0zXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJjb2wtMTIgY29sLWxnLTkgZGlzcGxheS04IG1iLTIgbWItbGctMFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCLQldGB0YLRjCDQstC+0L/RgNC+0YHRiyDQv9C+INC+0YLQtdC70Y4/INCX0LDQtNCw0Lkg0LjRhSDQvdCw0YjQtdC80YMg0Y3QutGB0L/QtdGA0YLRg1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBzdGF0aWNDbGFzczogXCJjb2wtMTIgY29sLWxnLTNcIiB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImFcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJidG4gYnRuLXByaW1hcnkgYnRuLWJsb2NrXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBocmVmOiBcImphdmFzY3JpcHQ6dm9pZCgwKVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRhdGEtaG90ZWwtaWRcIjogaG90ZWwuaG90ZWxjb2RlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRhdGEtdG9nZ2xlXCI6IFwibW9kYWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkYXRhLXRhcmdldFwiOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiI2V4cGVydFF1ZXN0aW9uTW9kYWxcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW192bS5fdihcItCX0LDQtNCw0YLRjCDQstC+0L/RgNC+0YFcIildXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJiclwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY1N0eWxlOiB7IGhlaWdodDogXCIyMDBweFwiIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGlkOiBcIm1hcGludGFiX1wiICsgaG90ZWwuaG90ZWxjb2RlIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICA6IF92bS5fZSgpLFxuICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJwLTMgdG91cnMtbGlzdCBkLW5vbmUgYmctZGFya2VyXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGlkOiBcInRvdXJzLVwiICsgaG90ZWwuaG90ZWxjb2RlIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBzdGF0aWNDbGFzczogXCJkLWJsb2NrXCIgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX2woaG90ZWwudG91cnMudG91ciwgZnVuY3Rpb24odG91cikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJyb3cgZC1mbGV4IGFsaWduLWl0ZW1zLWNlbnRlciBiZy1kYXJrZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgc3RhdGljQ2xhc3M6IFwiY29sLTEyIHRleHQtbXV0ZWQgbWItM1wiIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoX3ZtLl9zKHRvdXIudG91cm5hbWUpKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgc3RhdGljQ2xhc3M6IFwiY29sLTYgY29sLW1kLTJcIiB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJwXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwic3Ryb25nXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KF92bS5fcyh0b3VyLmZseWRhdGUpKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwicFwiLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoX3ZtLl9zKHRvdXIubmlnaHRzKSArIFwiINC90L7Rh9C10LlcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcImNvbC02IGNvbC1tZC0zXCIgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwicFwiLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInN0cm9uZ1wiLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihfdm0uX3ModG91ci5tZWFsKSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwicFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyB0aXRsZTogaG90ZWwubWVhbHJ1c3NpYW4gfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoX3ZtLl9zKHRvdXIubWVhbHJ1c3NpYW4pKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgc3RhdGljQ2xhc3M6IFwiY29sLTYgY29sLW1kLTJcIiB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJwXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwic3Ryb25nXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KF92bS5fcyh0b3VyLnJvb20pKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJwXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGF0dHJzOiB7IHRpdGxlOiBob3RlbC5wbGFjZW1lbnQgfSB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW192bS5fdihfdm0uX3ModG91ci5wbGFjZW1lbnQpKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgc3RhdGljQ2xhc3M6IFwiY29sLTYgY29sLW1kLTJcIiB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJwXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwic3Ryb25nXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KF92bS5fcyh0b3VyLm9wZXJhdG9ybmFtZSkpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJwXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaW1nXCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzcmM6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImh0dHBzOi8vdG91cnZpc29yLnJ1L3BpY3Mvb3BlcmF0b3JzL3NlYXJjaGxvZ28vXCIgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG91ci5vcGVyYXRvcmNvZGUgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIuZ2lmXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWx0OiBcIlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBzdGF0aWNDbGFzczogXCJjb2wtMTIgY29sLW1kLTNcIiB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRleHQtc3VjY2VzcyB0ZXh0LWxlZnQgdGV4dC1tZC1yaWdodCBtYi0wXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdNC30LAg0YLRg9GAXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJzcGFuXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwicHJpY2UgaDNcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5nZXREZXRhaWwodG91ci50b3VyaWQpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl9zKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fZihcImZvcm1hdFByaWNlXCIpKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG91ci5wcmljZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidGV4dC1tdXRlZCB0ZXh0LWxlZnQgdGV4dC1tZC1yaWdodFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHTQsdC10Lcg0YHQutC40LTQutC4OiBcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInNcIiwgeyBzdGF0aWNDbGFzczogXCJoNVwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fZihcImdldE9sZFByaWNlXCIpKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvdXIucHJpY2VcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRleHQtZGFuZ2VyIHRleHQtbGVmdCB0ZXh0LW1kLXJpZ2h0IHByaWNlXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcIm1vZGFsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkYXRhLWZhc3QtdG91ci1pZFwiOiB0b3VyLnRvdXJpZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRhdGEtaW5mb1wiOiBcImNyZWRpdFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGF0YS10YXJnZXRcIjogXCIjZmFzdE9yZGVyTW9kYWxcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHTQsiDQutGA0LXQtNC40YIgNiDQvNC10YEuOiBcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInNwYW5cIiwgeyBzdGF0aWNDbGFzczogXCJoNVwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fZihcImdldENyZWRpdFByaWNlXCIpKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvdXIucHJpY2VcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fbSgyLCB0cnVlKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAhX3ZtLmhpZGVSZWd1bGFyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA/IF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidGV4dC1pbmZvIHNtYWxsIHRleHQtbGVmdCB0ZXh0LW1kLXJpZ2h0IG1iLTJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwi0YbQtdC90LAg0LzQvtC20LXRgiDQsdGL0YLRjCDQvdC1INC+0LrQvtC90YfQsNGC0LXQu9GM0L3QvtC5LCDQvdC10L7QsdGF0L7QtNC40LzQviDQvtCx0YDQsNGC0LjRgtGM0YHRjyDQsiDQvtGE0LjRgVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IF92bS5fZSgpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImFcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJidG4gYnRuLXByaW1hcnkgYnRuLWJsb2NrIGJ0bi1zbSBsZC1leHQtbGVmdFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzczogXCJkZXRhaWwtYnRuX1wiICsgdG91ci50b3VyaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGhyZWY6IFwiI1wiIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5nZXREZXRhaWwodG91ci50b3VyaWQpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHTQotGD0YAg0L/QvtC00YDQvtCx0L3QvlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJsZCBsZC1yaW5nIGxkLXNwaW5cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiYVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImJ0biBidG4tb3V0bGluZS1vcmFuZ2UgYnRuLXNtIGJ0bi1ibG9ja1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhyZWY6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcIm1vZGFsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkYXRhLWZhc3QtdG91ci1pZFwiOiB0b3VyLnRvdXJpZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImRhdGEtdGFyZ2V0XCI6IFwiI2Zhc3RPcmRlck1vZGFsXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoXCLQl9Cw0LrQsNC30LDRgtGMINCyIDEg0LrQu9C40LpcIildXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fbSgzLCB0cnVlKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlciB0ZXh0LWluZm9cIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0ub3BlblRvdXJzTGlzdChob3RlbC5ob3RlbGNvZGUpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbX3ZtLl9tKDQsIHRydWUpXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgMlxuICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgOiBfdm0uX2UoKSxcbiAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgIGhvdGVsLnR5cGUgPT09IFwiYmFubmVyXCJcbiAgICAgICAgICAgICAgICAgID8gX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjYXJkIGJnLWRhcmtcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KF92bS5fcyhob3RlbC5iYW5uZXIubmFtZSkpXG4gICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICA6IF92bS5fZSgpXG4gICAgICAgICAgICAgIF1cbiAgICAgICAgICAgIClcbiAgICAgICAgICB9KVxuICAgICAgICApXG4gICAgICA6IF9jKFwiZGl2XCIsIFtcbiAgICAgICAgICAhX3ZtLnRvdXJzTm90Rm91bmRcbiAgICAgICAgICAgID8gX2MoXG4gICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgXCJjb2wtMTIgZGlzcGxheS02IG10LTUgbWItNSB0ZXh0LWNlbnRlciB0ZXh0LXN1Y2Nlc3NcIlxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICAnXFxuXFx0XFx0XFx00J3QsNC20LzQuNGC0LUg0LrQvdC+0L/QutGDIFwi0JjRgdC60LDRgtGMXCIsINC00LvRjyDQvdCw0YfQsNC70LAg0YDQsNCx0L7RgtGLINC/0L7QuNGB0LrQsFxcblxcdFxcdCdcbiAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgIDogX3ZtLl9lKCksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfdm0udG91cnNOb3RGb3VuZFxuICAgICAgICAgICAgPyBfYyhcbiAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICBcImNvbC0xMiBkaXNwbGF5LTcgbXQtNSBtYi01IHRleHQtY2VudGVyIHRleHQtZGFuZ2VyXCJcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgICAgJ1xcblxcdFxcdFxcdNCj0L/RgSEg0JjQt9Cy0LjQvdC40YLQtSwg0L3QviDQv9C+INC30LDQtNCw0L3QvdGL0Lwg0L/QsNGA0LDQvNC10YLRgNCw0Lwg0YLRg9GA0L7QsiDQvdC1INC90LDQudC00LXQvdC+LiDQn9C+0L/RgNC+0LHRg9C50YLQtSDQuNC30LzQtdC90LjRgtGMINC/0LDRgNC80LXRgtGA0Ysg0L/QvtC40YHQutCwINC4INGB0L3QvtCy0LAg0L3QsNC20LDRgtGMINC60L3QvtC/0LrRgyBcItCd0LDQudGC0LhcIiAnXG4gICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgX2MoXCJiclwiKSxcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgICAgXCJcXG5cXHRcXHRcXHTQm9C40LHQviDQvtGC0YHRgtCw0LLRjNGC0LUg0LfQsNGP0LLQutGDINC90LAg0L/QvtC00LHQvtGAINGC0YPRgNCwINC00LvRjyDQvdCw0YjQuNGFINC80LXQvdC10LTQttC10YDQvtCyLlxcblxcdFxcdFwiXG4gICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICApXG4gICAgICAgICAgICA6IF92bS5fZSgpXG4gICAgICAgIF0pXG4gIF0pXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW1xuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcbiAgICAgIFwiZGl2XCIsXG4gICAgICB7XG4gICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgIFwiY29sLTEyIHRleHQtc3VjY2VzcyBzbWFsbCB0ZXh0LW1kLXJpZ2h0IGZvbnQtaXRhbGljIGQtbm9uZSBkLXhsLWJsb2NrXCJcbiAgICAgIH0sXG4gICAgICBbXG4gICAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcImZhIGZhLWluZm9cIiB9KSxcbiAgICAgICAgX3ZtLl92KFwiINCy0YHQtSDRhtC10L3RiyDRgSDRgtC+0L/Qu9C40LLQvdGL0Lwg0YHQsdC+0YDQvtC8XCIpXG4gICAgICBdXG4gICAgKVxuICB9LFxuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcbiAgICAgIFwiZGl2XCIsXG4gICAgICB7IHN0YXRpY0NsYXNzOiBcInRleHQtc3VjY2VzcyBzbWFsbCBmb250LWl0YWxpYyBkLWJsb2NrIGQteGwtbm9uZVwiIH0sXG4gICAgICBbXG4gICAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcImZhIGZhLWluZm9cIiB9KSxcbiAgICAgICAgX3ZtLl92KFwiINCy0YHQtSDRhtC10L3RiyDRgSDRgtC+0L/Qu9C40LLQvdGL0Lwg0YHQsdC+0YDQvtC8XCIpXG4gICAgICBdXG4gICAgKVxuICB9LFxuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcInBcIiwgW1xuICAgICAgX2MoXCJzbWFsbFwiLCBbX3ZtLl92KFwiKNGB0YLQvtC40LzQvtGB0YLRjCDQsdC10Lcg0L/QtdGA0LLQvtC90LDRh9Cw0LvRjNC90L7Qs9C+INCy0LfQvdC+0YHQsClcIildKVxuICAgIF0pXG4gIH0sXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLTEyXCIgfSwgW19jKFwiaHJcIildKVxuICB9LFxuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcImphdmFzY3JpcHQ6dm9pZCgwKVwiIH0gfSwgW1xuICAgICAgX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2hldnJvbi11cFwiIH0pLFxuICAgICAgX3ZtLl92KFwiINGB0LLQtdGA0L3Rg9GC0YxcIilcbiAgICBdKVxuICB9XG5dXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbnZhciBlc0V4cG9ydHMgPSB7IHJlbmRlcjogcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnM6IHN0YXRpY1JlbmRlckZucyB9XG5leHBvcnQgZGVmYXVsdCBlc0V4cG9ydHNcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikgICAgICAucmVyZW5kZXIoXCJkYXRhLXYtNTU0M2QyZGJcIiwgZXNFeHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi01NTQzZDJkYlwiLFwiaGFzU2NvcGVkXCI6ZmFsc2UsXCJidWJsZVwiOntcInRyYW5zZm9ybXNcIjp7fX19IS4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3NyYy9Db21SZXN1bHRzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMTRcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwidmFyIGRpc3Bvc2VkID0gZmFsc2VcbnZhciBub3JtYWxpemVDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKVxuLyogc2NyaXB0ICovXG5leHBvcnQgKiBmcm9tIFwiISFiYWJlbC1sb2FkZXIhLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Db21Qcm9ncmVzc0Jhci52dWVcIlxuaW1wb3J0IF9fdnVlX3NjcmlwdF9fIGZyb20gXCIhIWJhYmVsLWxvYWRlciEuLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0NvbVByb2dyZXNzQmFyLnZ1ZVwiXG4vKiB0ZW1wbGF0ZSAqL1xuaW1wb3J0IF9fdnVlX3RlbXBsYXRlX18gZnJvbSBcIiEhLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtZmFlYTcxZWFcXFwiLFxcXCJoYXNTY29wZWRcXFwiOmZhbHNlLFxcXCJidWJsZVxcXCI6e1xcXCJ0cmFuc2Zvcm1zXFxcIjp7fX19IS4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Db21Qcm9ncmVzc0Jhci52dWVcIlxuLyogdGVtcGxhdGUgZnVuY3Rpb25hbCAqL1xudmFyIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyA9IGZhbHNlXG4vKiBzdHlsZXMgKi9cbnZhciBfX3Z1ZV9zdHlsZXNfXyA9IG51bGxcbi8qIHNjb3BlSWQgKi9cbnZhciBfX3Z1ZV9zY29wZUlkX18gPSBudWxsXG4vKiBtb2R1bGVJZGVudGlmaWVyIChzZXJ2ZXIgb25seSkgKi9cbnZhciBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fID0gbnVsbFxudmFyIENvbXBvbmVudCA9IG5vcm1hbGl6ZUNvbXBvbmVudChcbiAgX192dWVfc2NyaXB0X18sXG4gIF9fdnVlX3RlbXBsYXRlX18sXG4gIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyxcbiAgX192dWVfc3R5bGVzX18sXG4gIF9fdnVlX3Njb3BlSWRfXyxcbiAgX192dWVfbW9kdWxlX2lkZW50aWZpZXJfX1xuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmNcXFxcQ29tUHJvZ3Jlc3NCYXIudnVlXCJcblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LWZhZWE3MWVhXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtZmFlYTcxZWFcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbiAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgZGlzcG9zZWQgPSB0cnVlXG4gIH0pXG59KSgpfVxuXG5leHBvcnQgZGVmYXVsdCBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvQ29tUHJvZ3Jlc3NCYXIudnVlXG4vLyBtb2R1bGUgaWQgPSAxNVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF92bS5zZWFyY2hJbkFjdGlvblxuICAgID8gX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJwcm9ncmVzcyBtdC0zIG1iLTNcIiB9LCBbXG4gICAgICAgIF9jKFxuICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAge1xuICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgIFwicHJvZ3Jlc3MtYmFyIHByb2dyZXNzLWJhci1zdHJpcGVkIHByb2dyZXNzLWJhci1hbmltYXRlZCBiZy1zdWNjZXNzIGQtZmxleCBqdXN0aWZ5LWNvbnRlbnQtYmV0d2VlblwiLFxuICAgICAgICAgICAgc3R5bGU6IHsgd2lkdGg6IF92bS5zZWFyY2hQcm9ncmVzcyArIFwiJVwiIH0sXG4gICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICByb2xlOiBcInByb2dyZXNzYmFyXCIsXG4gICAgICAgICAgICAgIFwiYXJpYS12YWx1ZW5vd1wiOiBfdm0uc2VhcmNoUHJvZ3Jlc3MsXG4gICAgICAgICAgICAgIFwiYXJpYS12YWx1ZW1pblwiOiBcIjBcIixcbiAgICAgICAgICAgICAgXCJhcmlhLXZhbHVlbWF4XCI6IFwiMTAwXCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIFtcbiAgICAgICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcInByb2dyZXNzLXR5cGUgcGwtMlwiIH0sIFtcbiAgICAgICAgICAgICAgX3ZtLl92KFwi0J/QvtC40YHQui4uLiDQndCw0LnQtNC10L3QviDRgtGD0YDQvtCyOiBcIiArIF92bS5fcyhfdm0uYWxsUm93c0NvdW50KSlcbiAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcInByb2dyZXNzLWNvbXBsZXRlZCBwci0yXCIgfSwgW1xuICAgICAgICAgICAgICBfdm0uX3YoX3ZtLl9zKF92bS5zZWFyY2hQcm9ncmVzcykgKyBcIiVcIilcbiAgICAgICAgICAgIF0pXG4gICAgICAgICAgXVxuICAgICAgICApXG4gICAgICBdKVxuICAgIDogX3ZtLl9lKClcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG52YXIgZXNFeHBvcnRzID0geyByZW5kZXI6IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnMgfVxuZXhwb3J0IGRlZmF1bHQgZXNFeHBvcnRzXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpICAgICAgLnJlcmVuZGVyKFwiZGF0YS12LWZhZWE3MWVhXCIsIGVzRXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtZmFlYTcxZWFcIixcImhhc1Njb3BlZFwiOmZhbHNlLFwiYnVibGVcIjp7XCJ0cmFuc2Zvcm1zXCI6e319fSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9zcmMvQ29tUHJvZ3Jlc3NCYXIudnVlXG4vLyBtb2R1bGUgaWQgPSAxNlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIhZnVuY3Rpb24odCxlKXtcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cyYmXCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZT9tb2R1bGUuZXhwb3J0cz1lKCk6XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShbXSxlKTpcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cz9leHBvcnRzLkhvdGVsRGF0ZVBpY2tlcj1lKCk6dC5Ib3RlbERhdGVQaWNrZXI9ZSgpfShcInVuZGVmaW5lZFwiIT10eXBlb2Ygc2VsZj9zZWxmOnRoaXMsZnVuY3Rpb24oKXtyZXR1cm4gZnVuY3Rpb24odCl7ZnVuY3Rpb24gZShpKXtpZihuW2ldKXJldHVybiBuW2ldLmV4cG9ydHM7dmFyIG89bltpXT17aTppLGw6ITEsZXhwb3J0czp7fX07cmV0dXJuIHRbaV0uY2FsbChvLmV4cG9ydHMsbyxvLmV4cG9ydHMsZSksby5sPSEwLG8uZXhwb3J0c312YXIgbj17fTtyZXR1cm4gZS5tPXQsZS5jPW4sZS5kPWZ1bmN0aW9uKHQsbixpKXtlLm8odCxuKXx8T2JqZWN0LmRlZmluZVByb3BlcnR5KHQsbix7Y29uZmlndXJhYmxlOiExLGVudW1lcmFibGU6ITAsZ2V0Oml9KX0sZS5uPWZ1bmN0aW9uKHQpe3ZhciBuPXQmJnQuX19lc01vZHVsZT9mdW5jdGlvbigpe3JldHVybiB0LmRlZmF1bHR9OmZ1bmN0aW9uKCl7cmV0dXJuIHR9O3JldHVybiBlLmQobixcImFcIixuKSxufSxlLm89ZnVuY3Rpb24odCxlKXtyZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHQsZSl9LGUucD1cIi9cIixlKGUucz0xNil9KFtmdW5jdGlvbih0LGUpe3ZhciBuPXQuZXhwb3J0cz1cInVuZGVmaW5lZFwiIT10eXBlb2Ygd2luZG93JiZ3aW5kb3cuTWF0aD09TWF0aD93aW5kb3c6XCJ1bmRlZmluZWRcIiE9dHlwZW9mIHNlbGYmJnNlbGYuTWF0aD09TWF0aD9zZWxmOkZ1bmN0aW9uKFwicmV0dXJuIHRoaXNcIikoKTtcIm51bWJlclwiPT10eXBlb2YgX19nJiYoX19nPW4pfSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm5cIm9iamVjdFwiPT10eXBlb2YgdD9udWxsIT09dDpcImZ1bmN0aW9uXCI9PXR5cGVvZiB0fX0sZnVuY3Rpb24odCxlLG4pe3QuZXhwb3J0cz0hbigzKShmdW5jdGlvbigpe3JldHVybiA3IT1PYmplY3QuZGVmaW5lUHJvcGVydHkoe30sXCJhXCIse2dldDpmdW5jdGlvbigpe3JldHVybiA3fX0pLmF9KX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7dHJ5e3JldHVybiEhdCgpfWNhdGNoKHQpe3JldHVybiEwfX19LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPVwiZGF0YTppbWFnZS9zdmcreG1sO2Jhc2U2NCxQSE4yWnlCNGJXeHVjejBpYUhSMGNEb3ZMM2QzZHk1M015NXZjbWN2TWpBd01DOXpkbWNpSUhkcFpIUm9QU0k0SWlCb1pXbG5hSFE5SWpFMElpQjJhV1YzUW05NFBTSXdJREFnT0NBeE5DSStDaUFnSUNBOGNHRjBhQ0JtYVd4c1BTSnViMjVsSWlCbWFXeHNMWEoxYkdVOUltVjJaVzV2WkdRaUlITjBjbTlyWlQwaUl6QXdRMEU1UkNJZ2MzUnliMnRsTFhkcFpIUm9QU0l4TGpVaUlHUTlJazB4TGpNMk9DQXhURFl1TmpZZ055NHdPVElnTVNBeE15NDFNVElpTHo0S1BDOXpkbWMrQ2c9PVwifSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz1mdW5jdGlvbih0LGUsbixpLG8sYSl7dmFyIHIscz10PXR8fHt9LGM9dHlwZW9mIHQuZGVmYXVsdDtcIm9iamVjdFwiIT09YyYmXCJmdW5jdGlvblwiIT09Y3x8KHI9dCxzPXQuZGVmYXVsdCk7dmFyIHU9XCJmdW5jdGlvblwiPT10eXBlb2Ygcz9zLm9wdGlvbnM6cztlJiYodS5yZW5kZXI9ZS5yZW5kZXIsdS5zdGF0aWNSZW5kZXJGbnM9ZS5zdGF0aWNSZW5kZXJGbnMsdS5fY29tcGlsZWQ9ITApLG4mJih1LmZ1bmN0aW9uYWw9ITApLG8mJih1Ll9zY29wZUlkPW8pO3ZhciBkO2lmKGE/KGQ9ZnVuY3Rpb24odCl7dD10fHx0aGlzLiR2bm9kZSYmdGhpcy4kdm5vZGUuc3NyQ29udGV4dHx8dGhpcy5wYXJlbnQmJnRoaXMucGFyZW50LiR2bm9kZSYmdGhpcy5wYXJlbnQuJHZub2RlLnNzckNvbnRleHQsdHx8XCJ1bmRlZmluZWRcIj09dHlwZW9mIF9fVlVFX1NTUl9DT05URVhUX198fCh0PV9fVlVFX1NTUl9DT05URVhUX18pLGkmJmkuY2FsbCh0aGlzLHQpLHQmJnQuX3JlZ2lzdGVyZWRDb21wb25lbnRzJiZ0Ll9yZWdpc3RlcmVkQ29tcG9uZW50cy5hZGQoYSl9LHUuX3NzclJlZ2lzdGVyPWQpOmkmJihkPWkpLGQpe3ZhciBoPXUuZnVuY3Rpb25hbCxsPWg/dS5yZW5kZXI6dS5iZWZvcmVDcmVhdGU7aD8odS5faW5qZWN0U3R5bGVzPWQsdS5yZW5kZXI9ZnVuY3Rpb24odCxlKXtyZXR1cm4gZC5jYWxsKGUpLGwodCxlKX0pOnUuYmVmb3JlQ3JlYXRlPWw/W10uY29uY2F0KGwsZCk6W2RdfXJldHVybntlc01vZHVsZTpyLGV4cG9ydHM6cyxvcHRpb25zOnV9fX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIGkodCl7cmV0dXJuIHQmJnQuX19lc01vZHVsZT90OntkZWZhdWx0OnR9fU9iamVjdC5kZWZpbmVQcm9wZXJ0eShlLFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pO3ZhciBvPW4oNyksYT1pKG8pLHI9big1Mikscz1uKDEzKSxjPWkocyksdT1uKDUzKSxkPWkodSksaD1uKDE1KSxsPWkoaCkscD17bmlnaHQ6XCJOaWdodFwiLG5pZ2h0czpcIk5pZ2h0c1wiLFwiZGF5LW5hbWVzXCI6W1wiU3VuXCIsXCJNb25cIixcIlR1ZVwiLFwiV2VkXCIsXCJUaHVyXCIsXCJGcmlcIixcIlNhdFwiXSxcImNoZWNrLWluXCI6XCJDaGVjay1pblwiLFwiY2hlY2stb3V0XCI6XCJDaGVjay1vdXRcIixcIm1vbnRoLW5hbWVzXCI6W1wiSmFudWFyeVwiLFwiRmVicnVhcnlcIixcIk1hcmNoXCIsXCJBcHJpbFwiLFwiTWF5XCIsXCJKdW5lXCIsXCJKdWx5XCIsXCJBdWd1c3RcIixcIlNlcHRlbWJlclwiLFwiT2N0b2JlclwiLFwiTm92ZW1iZXJcIixcIkRlY2VtYmVyXCJdfTtlLmRlZmF1bHQ9e25hbWU6XCJIb3RlbERhdGVQaWNrZXJcIixkaXJlY3RpdmVzOntcIm9uLWNsaWNrLW91dHNpZGVcIjpyLmRpcmVjdGl2ZX0sY29tcG9uZW50czp7RGF5OmQuZGVmYXVsdH0scHJvcHM6e3ZhbHVlOnt0eXBlOlN0cmluZ30sc3RhcnRpbmdEYXRlVmFsdWU6e2RlZmF1bHQ6bnVsbCx0eXBlOkRhdGV9LGVuZGluZ0RhdGVWYWx1ZTp7ZGVmYXVsdDpudWxsLHR5cGU6RGF0ZX0sZm9ybWF0OntkZWZhdWx0OlwiWVlZWS1NTS1ERFwiLHR5cGU6U3RyaW5nfSxzdGFydERhdGU6e2RlZmF1bHQ6ZnVuY3Rpb24oKXtyZXR1cm4gbmV3IERhdGV9LHR5cGU6W0RhdGUsU3RyaW5nXX0sZW5kRGF0ZTp7ZGVmYXVsdDoxLzAsdHlwZTpbRGF0ZSxTdHJpbmcsTnVtYmVyXX0sZmlyc3REYXlPZldlZWs6e2RlZmF1bHQ6MCx0eXBlOk51bWJlcn0sbWluTmlnaHRzOntkZWZhdWx0OjEsdHlwZTpOdW1iZXJ9LG1heE5pZ2h0czp7ZGVmYXVsdDpudWxsLHR5cGU6TnVtYmVyfSxkaXNhYmxlZERhdGVzOntkZWZhdWx0OmZ1bmN0aW9uKCl7cmV0dXJuW119LHR5cGU6QXJyYXl9LGRpc2FibGVkRGF5c09mV2Vlazp7ZGVmYXVsdDpmdW5jdGlvbigpe3JldHVybltdfSx0eXBlOkFycmF5fSxhbGxvd2VkUmFuZ2VzOntkZWZhdWx0OmZ1bmN0aW9uKCl7cmV0dXJuW119LHR5cGU6QXJyYXl9LGhvdmVyaW5nVG9vbHRpcDp7ZGVmYXVsdDohMCx0eXBlOltCb29sZWFuLEZ1bmN0aW9uXX0sdG9vbHRpcE1lc3NhZ2U6e2RlZmF1bHQ6bnVsbCx0eXBlOlN0cmluZ30saTE4bjp7ZGVmYXVsdDpmdW5jdGlvbigpe3JldHVybiBwfSx0eXBlOk9iamVjdH0sZW5hYmxlQ2hlY2tvdXQ6e2RlZmF1bHQ6ITEsdHlwZTpCb29sZWFufSxzaW5nbGVEYXlTZWxlY3Rpb246e2RlZmF1bHQ6ITEsdHlwZTpCb29sZWFufSxzaG93WWVhcjp7ZGVmYXVsdDohMSx0eXBlOkJvb2xlYW59fSxkYXRhOmZ1bmN0aW9uKCl7cmV0dXJue2hvdmVyaW5nRGF0ZTpudWxsLGNoZWNrSW46dGhpcy5zdGFydGluZ0RhdGVWYWx1ZSxjaGVja091dDp0aGlzLmVuZGluZ0RhdGVWYWx1ZSxjdXJyZW50RGF0ZTpuZXcgRGF0ZSxtb250aHM6W10sYWN0aXZlTW9udGhJbmRleDowLG5leHREaXNhYmxlZERhdGU6bnVsbCxzaG93OiEwLGlzT3BlbjohMSx4RG93bjpudWxsLHlEb3duOm51bGwseFVwOm51bGwseVVwOm51bGwsc29ydGVkRGlzYWJsZWREYXRlczpudWxsLHNjcmVlblNpemU6dGhpcy5oYW5kbGVXaW5kb3dSZXNpemUoKX19LHdhdGNoOntpc09wZW46ZnVuY3Rpb24odCl7aWYoXCJkZXNrdG9wXCIhPT10aGlzLnNjcmVlblNpemUpe3ZhciBlPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJib2R5XCIpLmNsYXNzTGlzdDt0P2UuYWRkKFwiLW92ZXJmbG93LWhpZGRlblwiKTplLnJlbW92ZShcIi1vdmVyZmxvdy1oaWRkZW5cIil9fSxjaGVja0luOmZ1bmN0aW9uKHQpe3RoaXMuJGVtaXQoXCJjaGVjay1pbi1jaGFuZ2VkXCIsdCl9LGNoZWNrT3V0OmZ1bmN0aW9uKHQpe251bGwhPT10aGlzLmNoZWNrT3V0JiZudWxsIT09dGhpcy5jaGVja091dCYmKHRoaXMuaG92ZXJpbmdEYXRlPW51bGwsdGhpcy5uZXh0RGlzYWJsZWREYXRlPW51bGwsdGhpcy5zaG93PSEwLHRoaXMucGFyc2VEaXNhYmxlZERhdGVzKCksdGhpcy5yZVJlbmRlcigpLHRoaXMuaXNPcGVuPSExKSx0aGlzLiRlbWl0KFwiY2hlY2stb3V0LWNoYW5nZWRcIix0KX19LG1ldGhvZHM6KDAsYS5kZWZhdWx0KSh7fSxsLmRlZmF1bHQse2hhbmRsZVdpbmRvd1Jlc2l6ZTpmdW5jdGlvbigpe3JldHVybiB3aW5kb3cuaW5uZXJXaWR0aDw0ODA/dGhpcy5zY3JlZW5TaXplPVwic21hcnRwaG9uZVwiOndpbmRvdy5pbm5lcldpZHRoPj00ODAmJndpbmRvdy5pbm5lcldpZHRoPDc2OD90aGlzLnNjcmVlblNpemU9XCJ0YWJsZXRcIjp3aW5kb3cuaW5uZXJXaWR0aD49NzY4JiYodGhpcy5zY3JlZW5TaXplPVwiZGVza3RvcFwiKSx0aGlzLnNjcmVlblNpemV9LG9uRWxlbWVudEhlaWdodENoYW5nZTpmdW5jdGlvbih0LGUpe3ZhciBuPXQuY2xpZW50SGVpZ2h0LGk9bjshZnVuY3Rpb24gbygpe2k9dC5jbGllbnRIZWlnaHQsbiE9PWkmJmUoKSxuPWksdC5vbkVsZW1lbnRIZWlnaHRDaGFuZ2VUaW1lciYmY2xlYXJUaW1lb3V0KHQub25FbGVtZW50SGVpZ2h0Q2hhbmdlVGltZXIpLHQub25FbGVtZW50SGVpZ2h0Q2hhbmdlVGltZXI9c2V0VGltZW91dChvLDFlMyl9KCl9LGVtaXRIZWlnaENoYW5nZUV2ZW50OmZ1bmN0aW9uKCl7dGhpcy4kZW1pdChcImhlaWdodENoYW5nZWRcIil9LHJlUmVuZGVyOmZ1bmN0aW9uKCl7dmFyIHQ9dGhpczt0aGlzLnNob3c9ITEsdGhpcy4kbmV4dFRpY2soZnVuY3Rpb24oKXt0LnNob3c9ITB9KX0sY2xlYXJTZWxlY3Rpb246ZnVuY3Rpb24oKXt0aGlzLmhvdmVyaW5nRGF0ZT1udWxsLHRoaXMuY2hlY2tJbj1udWxsLHRoaXMuY2hlY2tPdXQ9bnVsbCx0aGlzLm5leHREaXNhYmxlZERhdGU9bnVsbCx0aGlzLnNob3c9ITAsdGhpcy5wYXJzZURpc2FibGVkRGF0ZXMoKSx0aGlzLnJlUmVuZGVyKCl9LGhpZGVEYXRlcGlja2VyOmZ1bmN0aW9uKCl7dGhpcy5pc09wZW49ITF9LHNob3dEYXRlcGlja2VyOmZ1bmN0aW9uKCl7dGhpcy5pc09wZW49ITB9LHRvZ2dsZURhdGVwaWNrZXI6ZnVuY3Rpb24oKXt0aGlzLmlzT3Blbj0hdGhpcy5pc09wZW59LGhhbmRsZURheUNsaWNrOmZ1bmN0aW9uKHQpe251bGw9PXRoaXMuY2hlY2tJbiYmMD09dGhpcy5zaW5nbGVEYXlTZWxlY3Rpb24/dGhpcy5jaGVja0luPXQuZGF0ZToxPT10aGlzLnNpbmdsZURheVNlbGVjdGlvbj8odGhpcy5jaGVja0luPXQuZGF0ZSx0aGlzLmNoZWNrT3V0PXQuZGF0ZSk6bnVsbCE9PXRoaXMuY2hlY2tJbiYmbnVsbD09dGhpcy5jaGVja091dD90aGlzLmNoZWNrT3V0PXQuZGF0ZToodGhpcy5jaGVja091dD1udWxsLHRoaXMuY2hlY2tJbj10LmRhdGUpLHRoaXMubmV4dERpc2FibGVkRGF0ZT10Lm5leHREaXNhYmxlZERhdGV9LHJlbmRlclByZXZpb3VzTW9udGg6ZnVuY3Rpb24oKXt0aGlzLmFjdGl2ZU1vbnRoSW5kZXg+PTEmJnRoaXMuYWN0aXZlTW9udGhJbmRleC0tfSxyZW5kZXJOZXh0TW9udGg6ZnVuY3Rpb24oKXtpZih0aGlzLmFjdGl2ZU1vbnRoSW5kZXg8dGhpcy5tb250aHMubGVuZ3RoLTIpcmV0dXJuIHZvaWQgdGhpcy5hY3RpdmVNb250aEluZGV4Kys7dmFyIHQ9dm9pZCAwO3Q9XCJkZXNrdG9wXCIhPT10aGlzLnNjcmVlblNpemU/dGhpcy5tb250aHNbdGhpcy5tb250aHMubGVuZ3RoLTFdLmRheXMuZmlsdGVyKGZ1bmN0aW9uKHQpe3JldHVybiEwPT09dC5iZWxvbmdzVG9UaGlzTW9udGh9KTp0aGlzLm1vbnRoc1t0aGlzLmFjdGl2ZU1vbnRoSW5kZXgrMV0uZGF5cy5maWx0ZXIoZnVuY3Rpb24odCl7cmV0dXJuITA9PT10LmJlbG9uZ3NUb1RoaXNNb250aH0pLHRoaXMuZW5kRGF0ZSE9PTEvMCYmYy5kZWZhdWx0LmZvcm1hdCh0WzBdLmRhdGUsXCJZWVlZTU1cIik9PWMuZGVmYXVsdC5mb3JtYXQobmV3IERhdGUodGhpcy5lbmREYXRlKSxcIllZWVlNTVwiKXx8KHRoaXMuY3JlYXRlTW9udGgodGhpcy5nZXROZXh0TW9udGgodFswXS5kYXRlKSksdGhpcy5hY3RpdmVNb250aEluZGV4KyspfSxzZXRDaGVja0luOmZ1bmN0aW9uKHQpe3RoaXMuY2hlY2tJbj10fSxzZXRDaGVja091dDpmdW5jdGlvbih0KXt0aGlzLmNoZWNrT3V0PXR9LGdldERheTpmdW5jdGlvbih0KXtyZXR1cm4gYy5kZWZhdWx0LmZvcm1hdCh0LFwiRFwiKX0sZ2V0TW9udGg6ZnVuY3Rpb24odCl7cmV0dXJuIHRoaXMuaTE4bltcIm1vbnRoLW5hbWVzXCJdW2MuZGVmYXVsdC5mb3JtYXQodCxcIk1cIiktMV0rKHRoaXMuc2hvd1llYXI/Yy5kZWZhdWx0LmZvcm1hdCh0LFwiIFlZWVlcIik6XCJcIil9LGZvcm1hdERhdGU6ZnVuY3Rpb24odCl7cmV0dXJuIGMuZGVmYXVsdC5mb3JtYXQodCx0aGlzLmZvcm1hdCl9LGNyZWF0ZU1vbnRoOmZ1bmN0aW9uKHQpe2Zvcih2YXIgZT10aGlzLmdldEZpcnN0RGF5KHQsdGhpcy5maXJzdERheU9mV2Vlayksbj17ZGF5czpbXX0saT0wO2k8NDI7aSsrKW4uZGF5cy5wdXNoKHtkYXRlOnRoaXMuYWRkRGF5cyhlLGkpLGJlbG9uZ3NUb1RoaXNNb250aDp0aGlzLmFkZERheXMoZSxpKS5nZXRNb250aCgpPT09dC5nZXRNb250aCgpLGlzSW5SYW5nZTohMX0pO3RoaXMubW9udGhzLnB1c2gobil9LHBhcnNlRGlzYWJsZWREYXRlczpmdW5jdGlvbigpe2Zvcih2YXIgdD1bXSxlPTA7ZTx0aGlzLmRpc2FibGVkRGF0ZXMubGVuZ3RoO2UrKyl0W2VdPW5ldyBEYXRlKHRoaXMuZGlzYWJsZWREYXRlc1tlXSk7dC5zb3J0KGZ1bmN0aW9uKHQsZSl7cmV0dXJuIHQtZX0pLHRoaXMuc29ydGVkRGlzYWJsZWREYXRlcz10fX0pLGJlZm9yZU1vdW50OmZ1bmN0aW9uKCl7Yy5kZWZhdWx0LmkxOG49e2RheU5hbWVzOnRoaXMuaTE4bltcImRheS1uYW1lc1wiXSxkYXlOYW1lc1Nob3J0OnRoaXMuc2hvcnRlblN0cmluZyh0aGlzLmkxOG5bXCJkYXktbmFtZXNcIl0sMyksbW9udGhOYW1lczp0aGlzLmkxOG5bXCJtb250aC1uYW1lc1wiXSxtb250aE5hbWVzU2hvcnQ6dGhpcy5zaG9ydGVuU3RyaW5nKHRoaXMuaTE4bltcIm1vbnRoLW5hbWVzXCJdLDMpLGFtUG06W1wiYW1cIixcInBtXCJdLERvRm46ZnVuY3Rpb24odCl7cmV0dXJuIHQrW1widGhcIixcInN0XCIsXCJuZFwiLFwicmRcIl1bdCUxMD4zPzA6KHQtdCUxMCE9MTApKnQlMTBdfX0sdGhpcy5jcmVhdGVNb250aChuZXcgRGF0ZSh0aGlzLnN0YXJ0RGF0ZSkpLHRoaXMuY3JlYXRlTW9udGgodGhpcy5nZXROZXh0TW9udGgobmV3IERhdGUodGhpcy5zdGFydERhdGUpKSksdGhpcy5wYXJzZURpc2FibGVkRGF0ZXMoKX0sbW91bnRlZDpmdW5jdGlvbigpe3ZhciB0PXRoaXM7ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcInRvdWNoc3RhcnRcIix0aGlzLmhhbmRsZVRvdWNoU3RhcnQsITEpLGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJ0b3VjaG1vdmVcIix0aGlzLmhhbmRsZVRvdWNoTW92ZSwhMSksd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJyZXNpemVcIix0aGlzLmhhbmRsZVdpbmRvd1Jlc2l6ZSksdGhpcy5vbkVsZW1lbnRIZWlnaHRDaGFuZ2UoZG9jdW1lbnQuYm9keSxmdW5jdGlvbigpe3QuZW1pdEhlaWdoQ2hhbmdlRXZlbnQoKX0pfSxkZXN0cm95ZWQ6ZnVuY3Rpb24oKXt3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInRvdWNoc3RhcnRcIix0aGlzLmhhbmRsZVRvdWNoU3RhcnQpLHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwidG91Y2htb3ZlXCIsdGhpcy5oYW5kbGVUb3VjaE1vdmUpLHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwicmVzaXplXCIsdGhpcy5oYW5kbGVXaW5kb3dSZXNpemUpfX19LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjtlLl9fZXNNb2R1bGU9ITA7dmFyIGk9bigyNCksbz1mdW5jdGlvbih0KXtyZXR1cm4gdCYmdC5fX2VzTW9kdWxlP3Q6e2RlZmF1bHQ6dH19KGkpO2UuZGVmYXVsdD1vLmRlZmF1bHR8fGZ1bmN0aW9uKHQpe2Zvcih2YXIgZT0xO2U8YXJndW1lbnRzLmxlbmd0aDtlKyspe3ZhciBuPWFyZ3VtZW50c1tlXTtmb3IodmFyIGkgaW4gbilPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwobixpKSYmKHRbaV09bltpXSl9cmV0dXJuIHR9fSxmdW5jdGlvbih0LGUpe3ZhciBuPXQuZXhwb3J0cz17dmVyc2lvbjpcIjIuNS4xXCJ9O1wibnVtYmVyXCI9PXR5cGVvZiBfX2UmJihfX2U9bil9LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDEwKSxvPW4oMTEpO3QuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm4gaShvKHQpKX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDQxKTt0LmV4cG9ydHM9T2JqZWN0KFwielwiKS5wcm9wZXJ0eUlzRW51bWVyYWJsZSgwKT9PYmplY3Q6ZnVuY3Rpb24odCl7cmV0dXJuXCJTdHJpbmdcIj09aSh0KT90LnNwbGl0KFwiXCIpOk9iamVjdCh0KX19LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe2lmKHZvaWQgMD09dCl0aHJvdyBUeXBlRXJyb3IoXCJDYW4ndCBjYWxsIG1ldGhvZCBvbiAgXCIrdCk7cmV0dXJuIHR9fSxmdW5jdGlvbih0LGUpe3ZhciBuPU1hdGguY2VpbCxpPU1hdGguZmxvb3I7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVybiBpc05hTih0PSt0KT8wOih0PjA/aTpuKSh0KX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaTshZnVuY3Rpb24obyl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gYSh0LGUpe2Zvcih2YXIgbj1bXSxpPTAsbz10Lmxlbmd0aDtpPG87aSsrKW4ucHVzaCh0W2ldLnN1YnN0cigwLGUpKTtyZXR1cm4gbn1mdW5jdGlvbiByKHQpe3JldHVybiBmdW5jdGlvbihlLG4saSl7dmFyIG89aVt0XS5pbmRleE9mKG4uY2hhckF0KDApLnRvVXBwZXJDYXNlKCkrbi5zdWJzdHIoMSkudG9Mb3dlckNhc2UoKSk7fm8mJihlLm1vbnRoPW8pfX1mdW5jdGlvbiBzKHQsZSl7Zm9yKHQ9U3RyaW5nKHQpLGU9ZXx8Mjt0Lmxlbmd0aDxlOyl0PVwiMFwiK3Q7cmV0dXJuIHR9dmFyIGM9e30sdT0vZHsxLDR9fE17MSw0fXxZWSg/OllZKT98U3sxLDN9fERvfFpafChbSGhNc0RtXSlcXDE/fFthQV18XCJbXlwiXSpcInwnW14nXSonL2csZD0vXFxkXFxkPy8saD0vXFxkezN9LyxsPS9cXGR7NH0vLHA9L1swLTldKlsnYS16XFx1MDBBMC1cXHUwNUZGXFx1MDcwMC1cXHVEN0ZGXFx1RjkwMC1cXHVGRENGXFx1RkRGMC1cXHVGRkVGXSt8W1xcdTA2MDAtXFx1MDZGRlxcL10rKFxccyo/W1xcdTA2MDAtXFx1MDZGRl0rKXsxLDJ9L2ksZj0vXFxbKFteXSo/KVxcXS9nbSxtPWZ1bmN0aW9uKCl7fSxnPVtcIlN1bmRheVwiLFwiTW9uZGF5XCIsXCJUdWVzZGF5XCIsXCJXZWRuZXNkYXlcIixcIlRodXJzZGF5XCIsXCJGcmlkYXlcIixcIlNhdHVyZGF5XCJdLHk9W1wiSmFudWFyeVwiLFwiRmVicnVhcnlcIixcIk1hcmNoXCIsXCJBcHJpbFwiLFwiTWF5XCIsXCJKdW5lXCIsXCJKdWx5XCIsXCJBdWd1c3RcIixcIlNlcHRlbWJlclwiLFwiT2N0b2JlclwiLFwiTm92ZW1iZXJcIixcIkRlY2VtYmVyXCJdLEQ9YSh5LDMpLGs9YShnLDMpO2MuaTE4bj17ZGF5TmFtZXNTaG9ydDprLGRheU5hbWVzOmcsbW9udGhOYW1lc1Nob3J0OkQsbW9udGhOYW1lczp5LGFtUG06W1wiYW1cIixcInBtXCJdLERvRm46ZnVuY3Rpb24odCl7cmV0dXJuIHQrW1widGhcIixcInN0XCIsXCJuZFwiLFwicmRcIl1bdCUxMD4zPzA6KHQtdCUxMCE9MTApKnQlMTBdfX07dmFyIHg9e0Q6ZnVuY3Rpb24odCl7cmV0dXJuIHQuZ2V0RGF0ZSgpfSxERDpmdW5jdGlvbih0KXtyZXR1cm4gcyh0LmdldERhdGUoKSl9LERvOmZ1bmN0aW9uKHQsZSl7cmV0dXJuIGUuRG9Gbih0LmdldERhdGUoKSl9LGQ6ZnVuY3Rpb24odCl7cmV0dXJuIHQuZ2V0RGF5KCl9LGRkOmZ1bmN0aW9uKHQpe3JldHVybiBzKHQuZ2V0RGF5KCkpfSxkZGQ6ZnVuY3Rpb24odCxlKXtyZXR1cm4gZS5kYXlOYW1lc1Nob3J0W3QuZ2V0RGF5KCldfSxkZGRkOmZ1bmN0aW9uKHQsZSl7cmV0dXJuIGUuZGF5TmFtZXNbdC5nZXREYXkoKV19LE06ZnVuY3Rpb24odCl7cmV0dXJuIHQuZ2V0TW9udGgoKSsxfSxNTTpmdW5jdGlvbih0KXtyZXR1cm4gcyh0LmdldE1vbnRoKCkrMSl9LE1NTTpmdW5jdGlvbih0LGUpe3JldHVybiBlLm1vbnRoTmFtZXNTaG9ydFt0LmdldE1vbnRoKCldfSxNTU1NOmZ1bmN0aW9uKHQsZSl7cmV0dXJuIGUubW9udGhOYW1lc1t0LmdldE1vbnRoKCldfSxZWTpmdW5jdGlvbih0KXtyZXR1cm4gU3RyaW5nKHQuZ2V0RnVsbFllYXIoKSkuc3Vic3RyKDIpfSxZWVlZOmZ1bmN0aW9uKHQpe3JldHVybiB0LmdldEZ1bGxZZWFyKCl9LGg6ZnVuY3Rpb24odCl7cmV0dXJuIHQuZ2V0SG91cnMoKSUxMnx8MTJ9LGhoOmZ1bmN0aW9uKHQpe3JldHVybiBzKHQuZ2V0SG91cnMoKSUxMnx8MTIpfSxIOmZ1bmN0aW9uKHQpe3JldHVybiB0LmdldEhvdXJzKCl9LEhIOmZ1bmN0aW9uKHQpe3JldHVybiBzKHQuZ2V0SG91cnMoKSl9LG06ZnVuY3Rpb24odCl7cmV0dXJuIHQuZ2V0TWludXRlcygpfSxtbTpmdW5jdGlvbih0KXtyZXR1cm4gcyh0LmdldE1pbnV0ZXMoKSl9LHM6ZnVuY3Rpb24odCl7cmV0dXJuIHQuZ2V0U2Vjb25kcygpfSxzczpmdW5jdGlvbih0KXtyZXR1cm4gcyh0LmdldFNlY29uZHMoKSl9LFM6ZnVuY3Rpb24odCl7cmV0dXJuIE1hdGgucm91bmQodC5nZXRNaWxsaXNlY29uZHMoKS8xMDApfSxTUzpmdW5jdGlvbih0KXtyZXR1cm4gcyhNYXRoLnJvdW5kKHQuZ2V0TWlsbGlzZWNvbmRzKCkvMTApLDIpfSxTU1M6ZnVuY3Rpb24odCl7cmV0dXJuIHModC5nZXRNaWxsaXNlY29uZHMoKSwzKX0sYTpmdW5jdGlvbih0LGUpe3JldHVybiB0LmdldEhvdXJzKCk8MTI/ZS5hbVBtWzBdOmUuYW1QbVsxXX0sQTpmdW5jdGlvbih0LGUpe3JldHVybiB0LmdldEhvdXJzKCk8MTI/ZS5hbVBtWzBdLnRvVXBwZXJDYXNlKCk6ZS5hbVBtWzFdLnRvVXBwZXJDYXNlKCl9LFpaOmZ1bmN0aW9uKHQpe3ZhciBlPXQuZ2V0VGltZXpvbmVPZmZzZXQoKTtyZXR1cm4oZT4wP1wiLVwiOlwiK1wiKStzKDEwMCpNYXRoLmZsb29yKE1hdGguYWJzKGUpLzYwKStNYXRoLmFicyhlKSU2MCw0KX19LGI9e0Q6W2QsZnVuY3Rpb24odCxlKXt0LmRheT1lfV0sRG86W25ldyBSZWdFeHAoZC5zb3VyY2UrcC5zb3VyY2UpLGZ1bmN0aW9uKHQsZSl7dC5kYXk9cGFyc2VJbnQoZSwxMCl9XSxNOltkLGZ1bmN0aW9uKHQsZSl7dC5tb250aD1lLTF9XSxZWTpbZCxmdW5jdGlvbih0LGUpe3ZhciBuPW5ldyBEYXRlLGk9KyhcIlwiK24uZ2V0RnVsbFllYXIoKSkuc3Vic3RyKDAsMik7dC55ZWFyPVwiXCIrKGU+Njg/aS0xOmkpK2V9XSxoOltkLGZ1bmN0aW9uKHQsZSl7dC5ob3VyPWV9XSxtOltkLGZ1bmN0aW9uKHQsZSl7dC5taW51dGU9ZX1dLHM6W2QsZnVuY3Rpb24odCxlKXt0LnNlY29uZD1lfV0sWVlZWTpbbCxmdW5jdGlvbih0LGUpe3QueWVhcj1lfV0sUzpbL1xcZC8sZnVuY3Rpb24odCxlKXt0Lm1pbGxpc2Vjb25kPTEwMCplfV0sU1M6Wy9cXGR7Mn0vLGZ1bmN0aW9uKHQsZSl7dC5taWxsaXNlY29uZD0xMCplfV0sU1NTOltoLGZ1bmN0aW9uKHQsZSl7dC5taWxsaXNlY29uZD1lfV0sZDpbZCxtXSxkZGQ6W3AsbV0sTU1NOltwLHIoXCJtb250aE5hbWVzU2hvcnRcIildLE1NTU06W3AscihcIm1vbnRoTmFtZXNcIildLGE6W3AsZnVuY3Rpb24odCxlLG4pe3ZhciBpPWUudG9Mb3dlckNhc2UoKTtpPT09bi5hbVBtWzBdP3QuaXNQbT0hMTppPT09bi5hbVBtWzFdJiYodC5pc1BtPSEwKX1dLFpaOlsvKFtcXCtcXC1dXFxkXFxkOj9cXGRcXGR8WikvLGZ1bmN0aW9uKHQsZSl7XCJaXCI9PT1lJiYoZT1cIiswMDowMFwiKTt2YXIgbixpPShlK1wiXCIpLm1hdGNoKC8oW1xcK1xcLV18XFxkXFxkKS9naSk7aSYmKG49NjAqaVsxXStwYXJzZUludChpWzJdLDEwKSx0LnRpbWV6b25lT2Zmc2V0PVwiK1wiPT09aVswXT9uOi1uKX1dfTtiLmRkPWIuZCxiLmRkZGQ9Yi5kZGQsYi5ERD1iLkQsYi5tbT1iLm0sYi5oaD1iLkg9Yi5ISD1iLmgsYi5NTT1iLk0sYi5zcz1iLnMsYi5BPWIuYSxjLm1hc2tzPXtkZWZhdWx0OlwiZGRkIE1NTSBERCBZWVlZIEhIOm1tOnNzXCIsc2hvcnREYXRlOlwiTS9EL1lZXCIsbWVkaXVtRGF0ZTpcIk1NTSBELCBZWVlZXCIsbG9uZ0RhdGU6XCJNTU1NIEQsIFlZWVlcIixmdWxsRGF0ZTpcImRkZGQsIE1NTU0gRCwgWVlZWVwiLHNob3J0VGltZTpcIkhIOm1tXCIsbWVkaXVtVGltZTpcIkhIOm1tOnNzXCIsbG9uZ1RpbWU6XCJISDptbTpzcy5TU1NcIn0sYy5mb3JtYXQ9ZnVuY3Rpb24odCxlLG4pe3ZhciBpPW58fGMuaTE4bjtpZihcIm51bWJlclwiPT10eXBlb2YgdCYmKHQ9bmV3IERhdGUodCkpLFwiW29iamVjdCBEYXRlXVwiIT09T2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKHQpfHxpc05hTih0LmdldFRpbWUoKSkpdGhyb3cgbmV3IEVycm9yKFwiSW52YWxpZCBEYXRlIGluIGZlY2hhLmZvcm1hdFwiKTtlPWMubWFza3NbZV18fGV8fGMubWFza3MuZGVmYXVsdDt2YXIgbz1bXTtyZXR1cm4gZT1lLnJlcGxhY2UoZixmdW5jdGlvbih0LGUpe3JldHVybiBvLnB1c2goZSksXCI/P1wifSksZT1lLnJlcGxhY2UodSxmdW5jdGlvbihlKXtyZXR1cm4gZSBpbiB4P3hbZV0odCxpKTplLnNsaWNlKDEsZS5sZW5ndGgtMSl9KSxlLnJlcGxhY2UoL1xcP1xcPy9nLGZ1bmN0aW9uKCl7cmV0dXJuIG8uc2hpZnQoKX0pfSxjLnBhcnNlPWZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1ufHxjLmkxOG47aWYoXCJzdHJpbmdcIiE9dHlwZW9mIGUpdGhyb3cgbmV3IEVycm9yKFwiSW52YWxpZCBmb3JtYXQgaW4gZmVjaGEucGFyc2VcIik7aWYoZT1jLm1hc2tzW2VdfHxlLHQubGVuZ3RoPjFlMylyZXR1cm4hMTt2YXIgbz0hMCxhPXt9O2lmKGUucmVwbGFjZSh1LGZ1bmN0aW9uKGUpe2lmKGJbZV0pe3ZhciBuPWJbZV0scj10LnNlYXJjaChuWzBdKTt+cj90LnJlcGxhY2UoblswXSxmdW5jdGlvbihlKXtyZXR1cm4gblsxXShhLGUsaSksdD10LnN1YnN0cihyK2UubGVuZ3RoKSxlfSk6bz0hMX1yZXR1cm4gYltlXT9cIlwiOmUuc2xpY2UoMSxlLmxlbmd0aC0xKX0pLCFvKXJldHVybiExO3ZhciByPW5ldyBEYXRlOyEwPT09YS5pc1BtJiZudWxsIT1hLmhvdXImJjEyIT0rYS5ob3VyP2EuaG91cj0rYS5ob3VyKzEyOiExPT09YS5pc1BtJiYxMj09K2EuaG91ciYmKGEuaG91cj0wKTt2YXIgcztyZXR1cm4gbnVsbCE9YS50aW1lem9uZU9mZnNldD8oYS5taW51dGU9KyhhLm1pbnV0ZXx8MCktK2EudGltZXpvbmVPZmZzZXQscz1uZXcgRGF0ZShEYXRlLlVUQyhhLnllYXJ8fHIuZ2V0RnVsbFllYXIoKSxhLm1vbnRofHwwLGEuZGF5fHwxLGEuaG91cnx8MCxhLm1pbnV0ZXx8MCxhLnNlY29uZHx8MCxhLm1pbGxpc2Vjb25kfHwwKSkpOnM9bmV3IERhdGUoYS55ZWFyfHxyLmdldEZ1bGxZZWFyKCksYS5tb250aHx8MCxhLmRheXx8MSxhLmhvdXJ8fDAsYS5taW51dGV8fDAsYS5zZWNvbmR8fDAsYS5taWxsaXNlY29uZHx8MCksc30sdm9pZCAwIT09dCYmdC5leHBvcnRzP3QuZXhwb3J0cz1jOnZvaWQgMCE9PShpPWZ1bmN0aW9uKCl7cmV0dXJuIGN9LmNhbGwoZSxuLGUsdCkpJiYodC5leHBvcnRzPWkpfSgpfSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gaSh0KXtyZXR1cm4gdCYmdC5fX2VzTW9kdWxlP3Q6e2RlZmF1bHQ6dH19T2JqZWN0LmRlZmluZVByb3BlcnR5KGUsXCJfX2VzTW9kdWxlXCIse3ZhbHVlOiEwfSk7dmFyIG89big3KSxhPWkobykscj1uKDEzKSxzPWkociksYz1uKDE1KSx1PWkoYyk7ZS5kZWZhdWx0PXtuYW1lOlwiRGF5XCIscHJvcHM6e3NvcnRlZERpc2FibGVkRGF0ZXM6e3R5cGU6QXJyYXl9LG9wdGlvbnM6e3R5cGU6T2JqZWN0fSxjaGVja0luOnt0eXBlOkRhdGV9LGNoZWNrT3V0Ont0eXBlOkRhdGV9LGhvdmVyaW5nRGF0ZTp7dHlwZTpEYXRlfSxtb3Vuc2VPdmVyRnVuY3Rpb246e3R5cGU6RnVuY3Rpb259LGJlbG9uZ3NUb1RoaXNNb250aDp7dHlwZTpCb29sZWFufSxhY3RpdmVNb250aEluZGV4Ont0eXBlOk51bWJlcn0sZGF0ZTp7dHlwZTpEYXRlfSxkYXlOdW1iZXI6e3R5cGU6U3RyaW5nfSxuZXh0RGlzYWJsZWREYXRlOnt0eXBlOltEYXRlLE51bWJlcixTdHJpbmddfSxob3ZlcmluZ1Rvb2x0aXA6e2RlZmF1bHQ6ITAsdHlwZTpCb29sZWFufSx0b29sdGlwTWVzc2FnZTp7ZGVmYXVsdDpudWxsLHR5cGU6U3RyaW5nfX0sZGF0YTpmdW5jdGlvbigpe3JldHVybntpc0hpZ2hsaWdodGVkOiExLGlzRGlzYWJsZWQ6ITEsYWxsb3dlZENoZWNrb3V0RGF5czpbXX19LGNvbXB1dGVkOntuaWdodHNDb3VudDpmdW5jdGlvbigpe3JldHVybiB0aGlzLmNvdW50RGF5cyh0aGlzLmNoZWNrSW4sdGhpcy5ob3ZlcmluZ0RhdGUpfSx0b29sdGlwTWVzc2FnZURpc3BsYXk6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy50b29sdGlwTWVzc2FnZT90aGlzLnRvb2x0aXBNZXNzYWdlOnRoaXMubmlnaHRzQ291bnQrXCIgXCIrKDEhPT10aGlzLm5pZ2h0c0NvdW50P3RoaXMub3B0aW9ucy5pMThuLm5pZ2h0czp0aGlzLm9wdGlvbnMuaTE4bi5uaWdodCl9LHNob3dUb29sdGlwOmZ1bmN0aW9uKCl7cmV0dXJuIXRoaXMuaXNEaXNhYmxlZCYmdGhpcy5kYXRlPT10aGlzLmhvdmVyaW5nRGF0ZSYmbnVsbCE9PXRoaXMuY2hlY2tJbiYmbnVsbD09dGhpcy5jaGVja091dH0sZGF5Q2xhc3M6ZnVuY3Rpb24oKXt2YXIgdD10aGlzO3JldHVybiB0aGlzLmJlbG9uZ3NUb1RoaXNNb250aD8hdGhpcy5pc0Rpc2FibGVkJiYxPT10aGlzLmNvbXBhcmVEYXkodGhpcy5kYXRlLHRoaXMuY2hlY2tJbikmJnRoaXMub3B0aW9ucy5taW5OaWdodHM+MCYmLTE9PXRoaXMuY29tcGFyZURheSh0aGlzLmRhdGUsdGhpcy5hZGREYXlzKHRoaXMuY2hlY2tJbix0aGlzLm9wdGlvbnMubWluTmlnaHRzKSk/XCJkYXRlcGlja2VyX19tb250aC1kYXktLXNlbGVjdGVkIGRhdGVwaWNrZXJfX21vbnRoLWRheS0tb3V0LW9mLXJhbmdlXCI6MD09PXRoaXMub3B0aW9ucy5hbGxvd2VkUmFuZ2VzLmxlbmd0aHx8dGhpcy5pc0Rpc2FibGVkfHxudWxsPT09dGhpcy5jaGVja0lufHxudWxsIT10aGlzLmNoZWNrT3V0P251bGwhPT10aGlzLmNoZWNrSW4mJnMuZGVmYXVsdC5mb3JtYXQodGhpcy5jaGVja0luLFwiWVlZWU1NRERcIik9PXMuZGVmYXVsdC5mb3JtYXQodGhpcy5kYXRlLFwiWVlZWU1NRERcIik/MD09dGhpcy5vcHRpb25zLm1pbk5pZ2h0cz9cImRhdGVwaWNrZXJfX21vbnRoLWRheS0tZmlyc3QtZGF5LXNlbGVjdGVkXCI6XCJkYXRlcGlja2VyX19tb250aC1kYXktLWRpc2FibGVkIGRhdGVwaWNrZXJfX21vbnRoLWRheS0tZmlyc3QtZGF5LXNlbGVjdGVkXCI6bnVsbCE9PXRoaXMuY2hlY2tPdXQmJnMuZGVmYXVsdC5mb3JtYXQodGhpcy5jaGVja091dCxcIllZWVlNTUREXCIpPT1zLmRlZmF1bHQuZm9ybWF0KHRoaXMuZGF0ZSxcIllZWVlNTUREXCIpP1wiZGF0ZXBpY2tlcl9fbW9udGgtZGF5LS1kaXNhYmxlZCBkYXRlcGlja2VyX19tb250aC1kYXktLWxhc3QtZGF5LXNlbGVjdGVkXCI6dGhpcy5pc0hpZ2hsaWdodGVkJiYhdGhpcy5pc0Rpc2FibGVkP1wiIGRhdGVwaWNrZXJfX21vbnRoLWRheS0tc2VsZWN0ZWRcIjp0aGlzLmlzRGlzYWJsZWQ/XCJkYXRlcGlja2VyX19tb250aC1kYXktLWRpc2FibGVkXCI6dm9pZCAwOnRoaXMuYWxsb3dlZENoZWNrb3V0RGF5cy5zb21lKGZ1bmN0aW9uKGUpe3JldHVybiAwPT10LmNvbXBhcmVEYXkoZSx0LmRhdGUpJiYhdC5pc0hpZ2hsaWdodGVkfSk/XCJkYXRlcGlja2VyX19tb250aC1kYXktLWFsbG93ZWQtY2hlY2tvdXRcIjp0aGlzLmFsbG93ZWRDaGVja291dERheXMuc29tZShmdW5jdGlvbihlKXtyZXR1cm4gMD09dC5jb21wYXJlRGF5KGUsdC5kYXRlKSYmdC5pc0hpZ2hsaWdodGVkfSk/XCJkYXRlcGlja2VyX19tb250aC1kYXktLXNlbGVjdGVkIGRhdGVwaWNrZXJfX21vbnRoLWRheS0tYWxsb3dlZC1jaGVja291dFwiOiF0aGlzLmFsbG93ZWRDaGVja291dERheXMuc29tZShmdW5jdGlvbihlKXtyZXR1cm4gMD09dC5jb21wYXJlRGF5KGUsdC5kYXRlKX0pJiZ0aGlzLmlzSGlnaGxpZ2h0ZWQ/XCJkYXRlcGlja2VyX19tb250aC1kYXktLW91dC1vZi1yYW5nZSBkYXRlcGlja2VyX19tb250aC1kYXktLXNlbGVjdGVkXCI6XCJkYXRlcGlja2VyX19tb250aC1kYXkgZGF0ZXBpY2tlcl9fbW9udGgtZGF5LS1vdXQtb2YtcmFuZ2VcIjp0aGlzLmJlbG9uZ3NUb1RoaXNNb250aD9cImRhdGVwaWNrZXJfX21vbnRoLWRheS0tdmFsaWRcIjpcImRhdGVwaWNrZXJfX21vbnRoLWRheS0taGlkZGVuXCJ9fSx3YXRjaDp7aG92ZXJpbmdEYXRlOmZ1bmN0aW9uKHQpe251bGwhPT10aGlzLmNoZWNrSW4mJm51bGw9PXRoaXMuY2hlY2tPdXQmJjA9PXRoaXMuaXNEaXNhYmxlZCYmKHRoaXMuaXNEYXRlTGVzc09yRXF1YWxzKHRoaXMuY2hlY2tJbix0aGlzLmRhdGUpJiZ0aGlzLmlzRGF0ZUxlc3NPckVxdWFscyh0aGlzLmRhdGUsdGhpcy5ob3ZlcmluZ0RhdGUpP3RoaXMuaXNIaWdobGlnaHRlZD0hMDp0aGlzLmlzSGlnaGxpZ2h0ZWQ9ITEpLG51bGwhPT10aGlzLmNoZWNrSW4mJm51bGw9PXRoaXMuY2hlY2tPdXQmJnRoaXMuYWxsb3dlZENoZWNrb3V0RGF5cy5sZW5ndGh9LGFjdGl2ZU1vbnRoSW5kZXg6ZnVuY3Rpb24odCl7aWYodGhpcy5jaGVja0lmRGlzYWJsZWQoKSx0aGlzLmNoZWNrSWZIaWdobGlnaHRlZCgpLG51bGwhPT10aGlzLmNoZWNrSW4mJm51bGwhPT10aGlzLmNoZWNrT3V0KXRoaXMuaXNEYXRlTGVzc09yRXF1YWxzKHRoaXMuY2hlY2tJbix0aGlzLmRhdGUpJiZ0aGlzLmlzRGF0ZUxlc3NPckVxdWFscyh0aGlzLmRhdGUsdGhpcy5jaGVja091dCk/dGhpcy5pc0hpZ2hsaWdodGVkPSEwOnRoaXMuaXNIaWdobGlnaHRlZD0hMTtlbHNle2lmKG51bGw9PT10aGlzLmNoZWNrSW58fG51bGwhPXRoaXMuY2hlY2tPdXQpcmV0dXJuO3RoaXMuZGlzYWJsZU5leHREYXlzKCl9fSxuZXh0RGlzYWJsZWREYXRlOmZ1bmN0aW9uKCl7dGhpcy5kaXNhYmxlTmV4dERheXMoKX0sY2hlY2tJbjpmdW5jdGlvbih0KXt0aGlzLmNyZWF0ZUFsbG93ZWRDaGVja291dERheXModCl9fSxtZXRob2RzOigwLGEuZGVmYXVsdCkoe30sdS5kZWZhdWx0LHtjb21wYXJlRGF5OmZ1bmN0aW9uKHQsZSl7dmFyIG49cy5kZWZhdWx0LmZvcm1hdChuZXcgRGF0ZSh0KSxcIllZWVlNTUREXCIpLGk9cy5kZWZhdWx0LmZvcm1hdChuZXcgRGF0ZShlKSxcIllZWVlNTUREXCIpO3JldHVybiBuPmk/MTpuPT1pPzA6bjxpPy0xOnZvaWQgMH0sZGF5Q2xpY2tlZDpmdW5jdGlvbih0KXtpZighdGhpcy5pc0Rpc2FibGVkKXswIT09dGhpcy5vcHRpb25zLmFsbG93ZWRSYW5nZXMubGVuZ3RoJiZ0aGlzLmNyZWF0ZUFsbG93ZWRDaGVja291dERheXModCk7dmFyIGU9KHRoaXMub3B0aW9ucy5tYXhOaWdodHM/dGhpcy5hZGREYXlzKHRoaXMuZGF0ZSx0aGlzLm9wdGlvbnMubWF4TmlnaHRzKTpudWxsKXx8dGhpcy5hbGxvd2VkQ2hlY2tvdXREYXlzW3RoaXMuYWxsb3dlZENoZWNrb3V0RGF5cy5sZW5ndGgtMV18fHRoaXMuZ2V0TmV4dERhdGUodGhpcy5zb3J0ZWREaXNhYmxlZERhdGVzLHRoaXMuZGF0ZSl8fHRoaXMubmV4dERhdGVCeURheU9mV2Vla0FycmF5KHRoaXMub3B0aW9ucy5kaXNhYmxlZERheXNPZldlZWssdGhpcy5kYXRlKXx8MS8wO3RoaXMub3B0aW9ucy5lbmFibGVDaGVja291dCYmKGU9MS8wKSx0aGlzLiRlbWl0KFwiZGF5Q2xpY2tlZFwiLHtkYXRlOnQsbmV4dERpc2FibGVkRGF0ZTplfSl9fSxjb21wYXJlRW5kRGF5OmZ1bmN0aW9uKCl7aWYodGhpcy5vcHRpb25zLmVuZERhdGUhPT0xLzApcmV0dXJuIDE9PXRoaXMuY29tcGFyZURheSh0aGlzLmRhdGUsdGhpcy5vcHRpb25zLmVuZERhdGUpfSxjaGVja0lmRGlzYWJsZWQ6ZnVuY3Rpb24oKXt2YXIgdD10aGlzO3RoaXMuaXNEaXNhYmxlZD0odGhpcy5zb3J0ZWREaXNhYmxlZERhdGVzP3RoaXMuc29ydGVkRGlzYWJsZWREYXRlcy5zb21lKGZ1bmN0aW9uKGUpe3JldHVybiAwPT10LmNvbXBhcmVEYXkoZSx0LmRhdGUpfSk6bnVsbCl8fC0xPT10aGlzLmNvbXBhcmVEYXkodGhpcy5kYXRlLHRoaXMub3B0aW9ucy5zdGFydERhdGUpfHx0aGlzLmNvbXBhcmVFbmREYXkoKXx8dGhpcy5vcHRpb25zLmRpc2FibGVkRGF5c09mV2Vlay5zb21lKGZ1bmN0aW9uKGUpe3JldHVybiBlPT1zLmRlZmF1bHQuZm9ybWF0KHQuZGF0ZSxcImRkZGRcIil9KSx0aGlzLm9wdGlvbnMuZW5hYmxlQ2hlY2tvdXQmJjE9PXRoaXMuY29tcGFyZURheSh0aGlzLmRhdGUsdGhpcy5jaGVja0luKSYmLTE9PXRoaXMuY29tcGFyZURheSh0aGlzLmRhdGUsdGhpcy5jaGVja091dCkmJih0aGlzLmlzRGlzYWJsZWQ9ITEpfSxjaGVja0lmSGlnaGxpZ2h0ZWQ6ZnVuY3Rpb24oKXtudWxsIT09dGhpcy5jaGVja0luJiZudWxsIT09dGhpcy5jaGVja091dCYmMD09dGhpcy5pc0Rpc2FibGVkJiYodGhpcy5pc0RhdGVMZXNzT3JFcXVhbHModGhpcy5jaGVja0luLHRoaXMuZGF0ZSkmJnRoaXMuaXNEYXRlTGVzc09yRXF1YWxzKHRoaXMuZGF0ZSx0aGlzLmNoZWNrT3V0KT90aGlzLmlzSGlnaGxpZ2h0ZWQ9ITA6dGhpcy5pc0hpZ2hsaWdodGVkPSExKX0sY3JlYXRlQWxsb3dlZENoZWNrb3V0RGF5czpmdW5jdGlvbih0KXt2YXIgZT10aGlzO3RoaXMuYWxsb3dlZENoZWNrb3V0RGF5cz1bXSx0aGlzLm9wdGlvbnMuYWxsb3dlZFJhbmdlcy5mb3JFYWNoKGZ1bmN0aW9uKG4pe3JldHVybiBlLmFsbG93ZWRDaGVja291dERheXMucHVzaChlLmFkZERheXModCxuKSl9KSx0aGlzLmFsbG93ZWRDaGVja291dERheXMuc29ydChmdW5jdGlvbih0LGUpe3JldHVybiB0LWV9KX0sZGlzYWJsZU5leHREYXlzOmZ1bmN0aW9uKCl7dGhpcy5pc0RhdGVMZXNzT3JFcXVhbHModGhpcy5kYXRlLHRoaXMubmV4dERpc2FibGVkRGF0ZSl8fHRoaXMubmV4dERpc2FibGVkRGF0ZT09PTEvMD90aGlzLmlzRGF0ZUxlc3NPckVxdWFscyh0aGlzLmRhdGUsdGhpcy5jaGVja0luKSYmKHRoaXMuaXNEaXNhYmxlZD0hMCk6dGhpcy5pc0Rpc2FibGVkPSEwLDA9PXRoaXMuY29tcGFyZURheSh0aGlzLmRhdGUsdGhpcy5jaGVja0luKSYmMD09dGhpcy5vcHRpb25zLm1pbk5pZ2h0cyYmKHRoaXMuaXNEaXNhYmxlZD0hMSksdGhpcy5pc0RhdGVMZXNzT3JFcXVhbHModGhpcy5jaGVja0luLHRoaXMuZGF0ZSkmJnRoaXMub3B0aW9ucy5lbmFibGVDaGVja291dCYmKHRoaXMuaXNEaXNhYmxlZD0hMSl9fSksYmVmb3JlTW91bnQ6ZnVuY3Rpb24oKXt0aGlzLmNoZWNrSWZEaXNhYmxlZCgpLHRoaXMuY2hlY2tJZkhpZ2hsaWdodGVkKCl9fX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO09iamVjdC5kZWZpbmVQcm9wZXJ0eShlLFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pLGUuZGVmYXVsdD17Z2V0TmV4dERhdGU6ZnVuY3Rpb24odCxlKXt2YXIgbj1uZXcgRGF0ZShlKSxpPTEvMDtyZXR1cm4gdC5mb3JFYWNoKGZ1bmN0aW9uKHQpe3ZhciBlPW5ldyBEYXRlKHQpO2U+PW4mJmU8aSYmKGk9dCl9KSxpPT09MS8wP251bGw6aX0sbmV4dERhdGVCeURheU9mV2VlazpmdW5jdGlvbih0LGUpe2U9bmV3IERhdGUoZSksdD10LnRvTG93ZXJDYXNlKCk7Zm9yKHZhciBuPVtcInN1bmRheVwiLFwibW9uZGF5XCIsXCJ0dWVzZGF5XCIsXCJ3ZWRuZXNkYXlcIixcInRodXJzZGF5XCIsXCJmcmlkYXlcIixcInNhdHVyZGF5XCJdLGk9ZS5nZXREYXkoKSxvPTc7by0tOylpZih0PT09bltvXSl7dD1vPD1pP28rNzpvO2JyZWFrfXZhciBhPXQtaTtyZXR1cm4gZS5zZXREYXRlKGUuZ2V0RGF0ZSgpK2EpfSxuZXh0RGF0ZUJ5RGF5T2ZXZWVrQXJyYXk6ZnVuY3Rpb24odCxlKXtmb3IodmFyIG49W10saT0wO2k8dC5sZW5ndGg7aSsrKW4ucHVzaChuZXcgRGF0ZSh0aGlzLm5leHREYXRlQnlEYXlPZldlZWsodFtpXSxlKSkpO3JldHVybiB0aGlzLmdldE5leHREYXRlKG4sZSl9LGlzRGF0ZUxlc3NPckVxdWFsczpmdW5jdGlvbih0LGUpe3JldHVybiBuZXcgRGF0ZSh0KTw9bmV3IERhdGUoZSl9LGNvdW50RGF5czpmdW5jdGlvbih0LGUpe3ZhciBuPW5ldyBEYXRlKHQpLGk9bmV3IERhdGUoZSk7cmV0dXJuIE1hdGgucm91bmQoTWF0aC5hYnMoKG4uZ2V0VGltZSgpLWkuZ2V0VGltZSgpKS84NjRlNSkpfSxhZGREYXlzOmZ1bmN0aW9uKHQsZSl7dmFyIG49bmV3IERhdGUodCk7cmV0dXJuIG4uc2V0RGF0ZShuLmdldERhdGUoKStlKSxufSxnZXRGaXJzdERheTpmdW5jdGlvbih0LGUpe3ZhciBuPXRoaXMuZ2V0Rmlyc3REYXlPZk1vbnRoKHQpLGk9MDtyZXR1cm4gZT4wJiYoaT0wPT09bi5nZXREYXkoKT8tNytlOmUpLG5ldyBEYXRlKG4uc2V0RGF0ZShuLmdldERhdGUoKS0obi5nZXREYXkoKS1pKSkpfSxnZXRGaXJzdERheU9mTW9udGg6ZnVuY3Rpb24odCl7cmV0dXJuIG5ldyBEYXRlKHQuZ2V0RnVsbFllYXIoKSx0LmdldE1vbnRoKCksMSl9LGdldE5leHRNb250aDpmdW5jdGlvbih0KXtyZXR1cm4gMTE9PXQuZ2V0TW9udGgoKT9uZXcgRGF0ZSh0LmdldEZ1bGxZZWFyKCkrMSwwLDEpOm5ldyBEYXRlKHQuZ2V0RnVsbFllYXIoKSx0LmdldE1vbnRoKCkrMSwxKX0sc3dpcGVBZnRlclNjcm9sbDpmdW5jdGlvbih0KXtpZihcImRlc2t0b3BcIiE9PXRoaXMuc2NyZWVuU2l6ZSYmdGhpcy5pc09wZW4pe3ZhciBlPWRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwic3dpcGVyV3JhcHBlclwiKTtpZihlLnNjcm9sbEhlaWdodD5lLmNsaWVudEhlaWdodClpZihlLnNjcm9sbFRvcD09PWUuc2Nyb2xsSGVpZ2h0LWUub2Zmc2V0SGVpZ2h0KXRoaXMucmVuZGVyTmV4dE1vbnRoKCk7ZWxzZXtpZigwIT09ZS5zY3JvbGxUb3ApcmV0dXJuO3RoaXMucmVuZGVyUHJldmlvdXNNb250aCgpfWVsc2VcInVwXCI9PXQ/dGhpcy5yZW5kZXJOZXh0TW9udGgoKTpcImRvd25cIj09dCYmdGhpcy5yZW5kZXJQcmV2aW91c01vbnRoKCl9fSxoYW5kbGVUb3VjaFN0YXJ0OmZ1bmN0aW9uKHQpe3RoaXMueERvd249dC50b3VjaGVzWzBdLmNsaWVudFgsdGhpcy55RG93bj10LnRvdWNoZXNbMF0uY2xpZW50WX0saGFuZGxlVG91Y2hNb3ZlOmZ1bmN0aW9uKHQpe2lmKHRoaXMueERvd24mJnRoaXMueURvd24pe3RoaXMueFVwPXQudG91Y2hlc1swXS5jbGllbnRYLHRoaXMueVVwPXQudG91Y2hlc1swXS5jbGllbnRZO3ZhciBlPXRoaXMueERvd24tdGhpcy54VXAsbj10aGlzLnlEb3duLXRoaXMueVVwO01hdGguYWJzKGUpPk1hdGguYWJzKG4pfHwobj4wP3RoaXMuc3dpcGVBZnRlclNjcm9sbChcInVwXCIpOnRoaXMuc3dpcGVBZnRlclNjcm9sbChcImRvd25cIikpLHRoaXMueERvd249bnVsbCx0aGlzLnlEb3duPW51bGx9fSxzaG9ydGVuU3RyaW5nOmZ1bmN0aW9uKHQsZSl7Zm9yKHZhciBuPVtdLGk9MCxvPXQubGVuZ3RoO2k8bztpKyspbi5wdXNoKHRbaV0uc3Vic3RyKDAsZSkpO3JldHVybiBufX19LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBpKHQpe2N8fG4oMTcpfU9iamVjdC5kZWZpbmVQcm9wZXJ0eShlLFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pO3ZhciBvPW4oNiksYT1uLm4obyk7Zm9yKHZhciByIGluIG8pXCJkZWZhdWx0XCIhPT1yJiZmdW5jdGlvbih0KXtuLmQoZSx0LGZ1bmN0aW9uKCl7cmV0dXJuIG9bdF19KX0ocik7dmFyIHM9big1NSksYz0hMSx1PW4oNSksZD1pLGg9dShhLmEscy5hLCExLGQsbnVsbCxudWxsKTtoLm9wdGlvbnMuX19maWxlPVwic3JjXFxcXGNvbXBvbmVudHNcXFxcRGF0ZVBpY2tlci52dWVcIixlLmRlZmF1bHQ9aC5leHBvcnRzfSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigxOCk7XCJzdHJpbmdcIj09dHlwZW9mIGkmJihpPVtbdC5pLGksXCJcIl1dKSxpLmxvY2FscyYmKHQuZXhwb3J0cz1pLmxvY2Fscyk7bigyMikoXCJhZGUwMGY5OFwiLGksITEse30pfSxmdW5jdGlvbih0LGUsbil7ZT10LmV4cG9ydHM9bigxOSkodm9pZCAwKSxlLnB1c2goW3QuaSxcIlxcbi5zcXVhcmV7d2lkdGg6MTQuMjg1NzElO2Zsb2F0OmxlZnRcXG59XFxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDo3NjhweCl7XFxuLnNxdWFyZXtjdXJzb3I6cG9pbnRlclxcbn1cXG59XFxuKiw6YWZ0ZXIsOmJlZm9yZXstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDtib3gtc2l6aW5nOmJvcmRlci1ib3hcXG59XFxuLmRhdGVwaWNrZXJ7LXdlYmtpdC10cmFuc2l0aW9uOmFsbCAuMnMgZWFzZS1pbi1vdXQ7LW8tdHJhbnNpdGlvbjphbGwgLjJzIGVhc2UtaW4tb3V0O3RyYW5zaXRpb246YWxsIC4ycyBlYXNlLWluLW91dDtiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7Y29sb3I6IzQyNGI1Mztmb250LXNpemU6MTZweDtsaW5lLWhlaWdodDoxNHB4O292ZXJmbG93OmhpZGRlbjtsZWZ0OjA7dG9wOjQ4cHg7cG9zaXRpb246YWJzb2x1dGU7ei1pbmRleDoyXFxufVxcbi5kYXRlcGlja2VyIGJ1dHRvbi5uZXh0LS1tb2JpbGV7YmFja2dyb3VuZDpub25lO2JvcmRlcjoxcHggc29saWQgI2Q3ZDllMjtmbG9hdDpub25lO2hlaWdodDo1MHB4O3dpZHRoOjEwMCU7cG9zaXRpb246cmVsYXRpdmU7YmFja2dyb3VuZC1wb3NpdGlvbjo1MCU7LXdlYmtpdC1hcHBlYXJhbmNlOm5vbmU7LW1vei1hcHBlYXJhbmNlOm5vbmU7YXBwZWFyYW5jZTpub25lO292ZXJmbG93OmhpZGRlbjtwb3NpdGlvbjpmaXhlZDtib3R0b206MDtsZWZ0OjA7b3V0bGluZTpub25lOy13ZWJraXQtYm94LXNoYWRvdzowIDVweCAzMHB4IDEwcHggcmdiYSgwLDAsMCwuMDgpO2JveC1zaGFkb3c6MCA1cHggMzBweCAxMHB4IHJnYmEoMCwwLDAsLjA4KTtiYWNrZ3JvdW5kOiNmZmZcXG59XFxuLmRhdGVwaWNrZXIgYnV0dG9uLm5leHQtLW1vYmlsZTphZnRlcntiYWNrZ3JvdW5kOnRyYW5zcGFyZW50IHVybChcIituKDQpKycpIG5vLXJlcGVhdCA1MCUvOHB4Oy13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSg5MGRlZyk7LW1zLXRyYW5zZm9ybTpyb3RhdGUoOTBkZWcpO3RyYW5zZm9ybTpyb3RhdGUoOTBkZWcpO2NvbnRlbnQ6XCJcIjtwb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDoyMDAlO2hlaWdodDoyMDAlO3RvcDotNTAlO2xlZnQ6LTUwJVxcbn1cXG4uZGF0ZXBpY2tlci0tY2xvc2Vkey13ZWJraXQtYm94LXNoYWRvdzowIDE1cHggMzBweCAxMHB4IHRyYW5zcGFyZW50O2JveC1zaGFkb3c6MCAxNXB4IDMwcHggMTBweCB0cmFuc3BhcmVudDttYXgtaGVpZ2h0OjBcXG59XFxuLmRhdGVwaWNrZXItLW9wZW57LXdlYmtpdC1ib3gtc2hhZG93OjAgMTVweCAzMHB4IDEwcHggcmdiYSgwLDAsMCwuMDgpO2JveC1zaGFkb3c6MCAxNXB4IDMwcHggMTBweCByZ2JhKDAsMCwwLC4wOCk7bWF4LWhlaWdodDo5MDBweFxcbn1cXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOjc2N3B4KXtcXG4uZGF0ZXBpY2tlci0tb3Blbnstd2Via2l0LWJveC1zaGFkb3c6bm9uZTtib3gtc2hhZG93Om5vbmU7aGVpZ2h0OjEwMCU7bGVmdDowO3JpZ2h0OjA7Ym90dG9tOjA7LXdlYmtpdC1vdmVyZmxvdy1zY3JvbGxpbmc6dG91Y2ghaW1wb3J0YW50O3Bvc2l0aW9uOmZpeGVkO3RvcDowO3dpZHRoOjEwMCVcXG59XFxufVxcbi5kYXRlcGlja2VyX193cmFwcGVye3Bvc2l0aW9uOnJlbGF0aXZlO2Rpc3BsYXk6aW5saW5lLWJsb2NrO3dpZHRoOjEwMCU7aGVpZ2h0OjQ4cHg7YmFja2dyb3VuZDojZmZmIHVybCgnK24oMjApK1wiKSBuby1yZXBlYXQgMTdweC8xNnB4XFxufVxcbi5kYXRlcGlja2VyX19pbnB1dHtiYWNrZ3JvdW5kOnRyYW5zcGFyZW50O2hlaWdodDo0OHB4O2NvbG9yOiMzNTM0M2Q7Zm9udC1zaXplOjEycHg7b3V0bGluZTpub25lO3BhZGRpbmc6NHB4IDMwcHggMnB4O3dpZHRoOjEwMCU7d29yZC1zcGFjaW5nOjVweDtib3JkZXI6MFxcbn1cXG4uZGF0ZXBpY2tlcl9faW5wdXQ6Zm9jdXN7b3V0bGluZTpub25lXFxufVxcbi5kYXRlcGlja2VyX19pbnB1dDotbW96LXBsYWNlaG9sZGVyLC5kYXRlcGlja2VyX19pbnB1dDotbXMtaW5wdXQtcGxhY2Vob2xkZXIsLmRhdGVwaWNrZXJfX2lucHV0OjotbW96LXBsYWNlaG9sZGVyLC5kYXRlcGlja2VyX19pbnB1dDo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlcntjb2xvcjojMzUzNDNkXFxufVxcbi5kYXRlcGlja2VyX19kdW1teS13cmFwcGVye2JvcmRlcjoxcHggc29saWQgI2Q3ZDllMjtjdXJzb3I6cG9pbnRlcjtkaXNwbGF5OmJsb2NrO2Zsb2F0OmxlZnQ7d2lkdGg6MTAwJTtoZWlnaHQ6MTAwJVxcbn1cXG4uZGF0ZXBpY2tlcl9fZHVtbXktd3JhcHBlci0tbm8tYm9yZGVyLmRhdGVwaWNrZXJfX2R1bW15LXdyYXBwZXJ7bWFyZ2luLXRvcDoxNXB4O2JvcmRlcjowXFxufVxcbi5kYXRlcGlja2VyX19kdW1teS13cmFwcGVyLS1pcy1hY3RpdmV7Ym9yZGVyOjFweCBzb2xpZCAjMDBjYTlkXFxufVxcbi5kYXRlcGlja2VyX19kdW1teS1pbnB1dHtjb2xvcjojMzUzNDNkO3BhZGRpbmctdG9wOjA7Zm9udC1zaXplOjE0cHg7ZmxvYXQ6bGVmdDtoZWlnaHQ6NDhweDtsaW5lLWhlaWdodDozLjE7dGV4dC1hbGlnbjpsZWZ0O3RleHQtaW5kZW50OjVweDt3aWR0aDotd2Via2l0LWNhbGMoNTAlICsgNHB4KTt3aWR0aDpjYWxjKDUwJSArIDRweClcXG59XFxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDo0NzlweCl7XFxuLmRhdGVwaWNrZXJfX2R1bW15LWlucHV0e3RleHQtaW5kZW50OjA7dGV4dC1hbGlnbjpjZW50ZXJcXG59XFxufVxcbi5kYXRlcGlja2VyX19kdW1teS1pbnB1dDpmaXJzdC1jaGlsZHtiYWNrZ3JvdW5kOnRyYW5zcGFyZW50IHVybChcIituKDIxKStcIikgbm8tcmVwZWF0IDEwMCUvOHB4O3dpZHRoOi13ZWJraXQtY2FsYyg1MCUgLSA0cHgpO3dpZHRoOmNhbGMoNTAlIC0gNHB4KTt0ZXh0LWluZGVudDoyMHB4XFxufVxcbi5kYXRlcGlja2VyX19kdW1teS1pbnB1dC0taXMtYWN0aXZle2NvbG9yOiMwMGNhOWRcXG59XFxuLmRhdGVwaWNrZXJfX2R1bW15LWlucHV0LS1pcy1hY3RpdmU6Oi13ZWJraXQtaW5wdXQtcGxhY2Vob2xkZXJ7Y29sb3I6IzAwY2E5ZFxcbn1cXG4uZGF0ZXBpY2tlcl9fZHVtbXktaW5wdXQtLWlzLWFjdGl2ZTo6cGxhY2Vob2xkZXJ7Y29sb3I6IzAwY2E5ZFxcbn1cXG4uZGF0ZXBpY2tlcl9fZHVtbXktaW5wdXQtLWlzLWFjdGl2ZTo6LW1vei1wbGFjZWhvbGRlcntjb2xvcjojMDBjYTlkXFxufVxcbi5kYXRlcGlja2VyX19kdW1teS1pbnB1dC0taXMtYWN0aXZlOi1tcy1pbnB1dC1wbGFjZWhvbGRlcntjb2xvcjojMDBjYTlkXFxufVxcbi5kYXRlcGlja2VyX19kdW1teS1pbnB1dC0taXMtYWN0aXZlOi1tb3otcGxhY2Vob2xkZXJ7Y29sb3I6IzAwY2E5ZFxcbn1cXG4uZGF0ZXBpY2tlcl9fZHVtbXktaW5wdXQtLXNpbmdsZS1kYXRlOmZpcnN0LWNoaWxke3dpZHRoOjEwMCU7YmFja2dyb3VuZDpub25lO3RleHQtYWxpZ246bGVmdFxcbn1cXG4uZGF0ZXBpY2tlcl9fbW9udGgtZGF5e3Zpc2liaWxpdHk6dmlzaWJsZTt3aWxsLWNoYW5nZTphdXRvO3RleHQtYWxpZ246Y2VudGVyO21hcmdpbjowO2JvcmRlcjowO2hlaWdodDo0MHB4O3BhZGRpbmctdG9wOjE1cHhcXG59XFxuLmRhdGVwaWNrZXJfX21vbnRoLWRheS0taW52YWxpZC1yYW5nZXtiYWNrZ3JvdW5kLWNvbG9yOnJnYmEoMCwyMDIsMTU3LC4zKTtjb2xvcjojZjNmNWY4O2N1cnNvcjpub3QtYWxsb3dlZDtwb3NpdGlvbjpyZWxhdGl2ZVxcbn1cXG4uZGF0ZXBpY2tlcl9fbW9udGgtZGF5LS1pbnZhbGlke2NvbG9yOiNmM2Y1Zjg7Y3Vyc29yOm5vdC1hbGxvd2VkXFxufVxcbi5kYXRlcGlja2VyX19tb250aC1kYXktLWFsbG93ZWQtY2hlY2tvdXQ6aG92ZXIsLmRhdGVwaWNrZXJfX21vbnRoLWRheS0tdmFsaWQ6aG92ZXJ7YmFja2dyb3VuZC1jb2xvcjojZmZmO2NvbG9yOiMwMGNhOWQ7ei1pbmRleDoxO3Bvc2l0aW9uOnJlbGF0aXZlOy13ZWJraXQtYm94LXNoYWRvdzowIDAgMTBweCAzcHggcmdiYSg2Niw3NSw4MywuNCk7Ym94LXNoYWRvdzowIDAgMTBweCAzcHggcmdiYSg2Niw3NSw4MywuNClcXG59XFxuLmRhdGVwaWNrZXJfX21vbnRoLWRheS0tZGlzYWJsZWR7b3BhY2l0eTouMjU7Y3Vyc29yOm5vdC1hbGxvd2VkO3BvaW50ZXItZXZlbnRzOm5vbmU7cG9zaXRpb246cmVsYXRpdmVcXG59XFxuLmRhdGVwaWNrZXJfX21vbnRoLWRheS0tc2VsZWN0ZWR7YmFja2dyb3VuZC1jb2xvcjpyZ2JhKDAsMjAyLDE1NywuNSk7Y29sb3I6I2ZmZlxcbn1cXG4uZGF0ZXBpY2tlcl9fbW9udGgtZGF5LS1zZWxlY3RlZDpob3ZlcntiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7Y29sb3I6IzAwY2E5ZDt6LWluZGV4OjE7cG9zaXRpb246cmVsYXRpdmU7LXdlYmtpdC1ib3gtc2hhZG93OjAgMCAxMHB4IDNweCByZ2JhKDY2LDc1LDgzLC40KTtib3gtc2hhZG93OjAgMCAxMHB4IDNweCByZ2JhKDY2LDc1LDgzLC40KVxcbn1cXG4uZGF0ZXBpY2tlcl9fbW9udGgtZGF5LS10b2RheXtiYWNrZ3JvdW5kLWNvbG9yOiNkN2Q5ZTI7Y29sb3I6Izk5OVxcbn1cXG4uZGF0ZXBpY2tlcl9fbW9udGgtZGF5LS1maXJzdC1kYXktc2VsZWN0ZWQsLmRhdGVwaWNrZXJfX21vbnRoLWRheS0tbGFzdC1kYXktc2VsZWN0ZWR7YmFja2dyb3VuZDojMDBjYTlkO2NvbG9yOiNmZmZcXG59XFxuLmRhdGVwaWNrZXJfX21vbnRoLWRheS0tYWxsb3dlZC1jaGVja291dHtjb2xvcjojOTk5XFxufVxcbi5kYXRlcGlja2VyX19tb250aC1kYXktLW91dC1vZi1yYW5nZXtjb2xvcjojZjNmNWY4O2N1cnNvcjpub3QtYWxsb3dlZDtwb3NpdGlvbjpyZWxhdGl2ZTtwb2ludGVyLWV2ZW50czpub25lXFxufVxcbi5kYXRlcGlja2VyX19tb250aC1kYXktLXZhbGlke2N1cnNvcjpwb2ludGVyO2NvbG9yOiM5OTlcXG59XFxuLmRhdGVwaWNrZXJfX21vbnRoLWRheS0taGlkZGVue29wYWNpdHk6LjI1O3BvaW50ZXItZXZlbnRzOm5vbmU7Y29sb3I6I2ZmZlxcbn1cXG4uZGF0ZXBpY2tlcl9fbW9udGgtYnV0dG9ue2JhY2tncm91bmQ6dHJhbnNwYXJlbnQgdXJsKFwiK24oNCkrJykgbm8tcmVwZWF0IDEwMCUvOHB4O2N1cnNvcjpwb2ludGVyO2Rpc3BsYXk6aW5saW5lLWJsb2NrO2hlaWdodDo2MHB4O3dpZHRoOjYwcHhcXG59XFxuLmRhdGVwaWNrZXJfX21vbnRoLWJ1dHRvbi0tcHJldnstd2Via2l0LXRyYW5zZm9ybTpyb3RhdGVZKDE4MGRlZyk7dHJhbnNmb3JtOnJvdGF0ZVkoMTgwZGVnKVxcbn1cXG4uZGF0ZXBpY2tlcl9fbW9udGgtYnV0dG9uLS1uZXh0e2Zsb2F0OnJpZ2h0XFxufVxcbi5kYXRlcGlja2VyX19tb250aC1idXR0b24tLWxvY2tlZHtvcGFjaXR5Oi4yO2N1cnNvcjpub3QtYWxsb3dlZFxcbn1cXG4uZGF0ZXBpY2tlcl9faW5uZXJ7cGFkZGluZzoyMHB4O2Zsb2F0OmxlZnRcXG59XFxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDo3NjdweCl7XFxuLmRhdGVwaWNrZXJfX2lubmVye3BhZGRpbmc6MFxcbn1cXG59XFxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDo3NjhweCl7XFxuLmRhdGVwaWNrZXJfX21vbnRoc3t3aWR0aDo2NTBweFxcbn1cXG59XFxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDo3NjdweCl7XFxuLmRhdGVwaWNrZXJfX21vbnRoc3ttYXJnaW4tdG9wOjkycHg7aGVpZ2h0Oi13ZWJraXQtY2FsYygxMDAlIC0gOTJweCk7aGVpZ2h0OmNhbGMoMTAwJSAtIDkycHgpO3Bvc2l0aW9uOmFic29sdXRlO2xlZnQ6MDt0b3A6MDtvdmVyZmxvdzpzY3JvbGw7cmlnaHQ6MDtib3R0b206MDtkaXNwbGF5Oi13ZWJraXQtYm94O2Rpc3BsYXk6LXdlYmtpdC1mbGV4O2Rpc3BsYXk6LW1zLWZsZXhib3g7ZGlzcGxheTpmbGV4Oy13ZWJraXQtYm94LW9yaWVudDp2ZXJ0aWNhbDstd2Via2l0LWJveC1kaXJlY3Rpb246bm9ybWFsOy13ZWJraXQtZmxleC1kaXJlY3Rpb246Y29sdW1uOy1tcy1mbGV4LWRpcmVjdGlvbjpjb2x1bW47ZmxleC1kaXJlY3Rpb246Y29sdW1uOy13ZWJraXQtYm94LXBhY2s6c3RhcnQ7LXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6ZmxleC1zdGFydDstbXMtZmxleC1wYWNrOnN0YXJ0O2p1c3RpZnktY29udGVudDpmbGV4LXN0YXJ0XFxufVxcbn1cXG4uZGF0ZXBpY2tlcl9fbW9udGhzOmJlZm9yZXtiYWNrZ3JvdW5kOiNkN2Q5ZTI7Ym90dG9tOjA7Y29udGVudDpcIlwiO2Rpc3BsYXk6YmxvY2s7bGVmdDo1MCU7cG9zaXRpb246YWJzb2x1dGU7dG9wOjA7d2lkdGg6MXB4XFxufVxcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6NzY3cHgpe1xcbi5kYXRlcGlja2VyX19tb250aHM6YmVmb3Jle2Rpc3BsYXk6bm9uZVxcbn1cXG59XFxuLmRhdGVwaWNrZXJfX21vbnRoe2ZvbnQtc2l6ZToxMnB4O2Zsb2F0OmxlZnQ7d2lkdGg6NTAlO3BhZGRpbmctcmlnaHQ6MTBweFxcbn1cXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOjc2N3B4KXtcXG4uZGF0ZXBpY2tlcl9fbW9udGh7d2lkdGg6MTAwJTtwYWRkaW5nLXJpZ2h0OjA7cGFkZGluZy10b3A6NjBweFxcbn1cXG4uZGF0ZXBpY2tlcl9fbW9udGg6bGFzdC1vZi10eXBle21hcmdpbi1ib3R0b206NjVweFxcbn1cXG59XFxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDo3NjhweCl7XFxuLmRhdGVwaWNrZXJfX21vbnRoOmxhc3Qtb2YtdHlwZXtwYWRkaW5nLXJpZ2h0OjA7cGFkZGluZy1sZWZ0OjEwcHhcXG59XFxufVxcbi5kYXRlcGlja2VyX19tb250aC1jYXB0aW9ue2hlaWdodDoyLjVlbTt2ZXJ0aWNhbC1hbGlnbjptaWRkbGVcXG59XFxuLmRhdGVwaWNrZXJfX21vbnRoLW5hbWV7Zm9udC1zaXplOjE2cHg7Zm9udC13ZWlnaHQ6NTAwO21hcmdpbi10b3A6LTQwcHg7cGFkZGluZy1ib3R0b206MTdweDtwb2ludGVyLWV2ZW50czpub25lO3RleHQtYWxpZ246Y2VudGVyXFxufVxcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6NzY3cHgpe1xcbi5kYXRlcGlja2VyX19tb250aC1uYW1le21hcmdpbi10b3A6LTI1cHg7bWFyZ2luLWJvdHRvbTowO3Bvc2l0aW9uOmFic29sdXRlO3dpZHRoOjEwMCVcXG59XFxufVxcbi5kYXRlcGlja2VyX193ZWVrLWRheXN7aGVpZ2h0OjJlbTt2ZXJ0aWNhbC1hbGlnbjptaWRkbGVcXG59XFxuLmRhdGVwaWNrZXJfX3dlZWstcm93e2JvcmRlci1ib3R0b206NXB4IHNvbGlkICNmZmY7aGVpZ2h0OjM4cHhcXG59XFxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDo3NjdweCl7XFxuLmRhdGVwaWNrZXJfX3dlZWstcm93ey13ZWJraXQtYm94LXNoYWRvdzowIDEzcHggMThweCAtOHB4IHJnYmEoMCwwLDAsLjA3KTtib3gtc2hhZG93OjAgMTNweCAxOHB4IC04cHggcmdiYSgwLDAsMCwuMDcpO2hlaWdodDoyNXB4O2xlZnQ6MDt0b3A6NjVweDtwb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDoxMDAlXFxufVxcbn1cXG4uZGF0ZXBpY2tlcl9fd2Vlay1uYW1le3dpZHRoOjE0LjI4NTcxJTtmbG9hdDpsZWZ0O2ZvbnQtc2l6ZToxMnB4O2ZvbnQtd2VpZ2h0OjQwMDtjb2xvcjojOTk5O3RleHQtYWxpZ246Y2VudGVyXFxufVxcbi5kYXRlcGlja2VyX19jbG9zZS1idXR0b257Zm9udC1zaXplOjIxcHg7bWFyZ2luLXRvcDowO3otaW5kZXg6NDtwb3NpdGlvbjpmaXhlZDtsZWZ0OjdweDt0b3A6NXB4XFxufVxcbi5kYXRlcGlja2VyX19jbGVhci1idXR0b24sLmRhdGVwaWNrZXJfX2Nsb3NlLWJ1dHRvbnthcHBlYXJlbmNlOm5vbmU7YmFja2dyb3VuZDp0cmFuc3BhcmVudDtib3JkZXI6MDtjb2xvcjojMDBjYTlkO2N1cnNvcjpwb2ludGVyO2ZvbnQtd2VpZ2h0OjcwMDtvdXRsaW5lOjA7LXdlYmtpdC10cmFuc2Zvcm06cm90YXRlKDQ1ZGVnKTstbXMtdHJhbnNmb3JtOnJvdGF0ZSg0NWRlZyk7dHJhbnNmb3JtOnJvdGF0ZSg0NWRlZylcXG59XFxuLmRhdGVwaWNrZXJfX2NsZWFyLWJ1dHRvbntmb250LXNpemU6MjVweDtoZWlnaHQ6NDBweDttYXJnaW46NHB4IC0ycHggMCAwO3BhZGRpbmc6MDtwb3NpdGlvbjphYnNvbHV0ZTtyaWdodDowO3RvcDowO3dpZHRoOjQwcHhcXG59XFxuLmRhdGVwaWNrZXJfX3Rvb2x0aXB7YmFja2dyb3VuZC1jb2xvcjojMmQzMDQ3Oy13ZWJraXQtYm9yZGVyLXJhZGl1czoycHg7Ym9yZGVyLXJhZGl1czoycHg7Y29sb3I6I2ZmZjtmb250LXNpemU6MTFweDttYXJnaW4tbGVmdDo1cHg7bWFyZ2luLXRvcDotMjJweDtwYWRkaW5nOjVweCAxMHB4O3Bvc2l0aW9uOmFic29sdXRlO3otaW5kZXg6M1xcbn1cXG4uZGF0ZXBpY2tlcl9fdG9vbHRpcDphZnRlcntib3JkZXItbGVmdDo0cHggc29saWQgdHJhbnNwYXJlbnQ7Ym9yZGVyLXJpZ2h0OjRweCBzb2xpZCB0cmFuc3BhcmVudDtib3JkZXItdG9wOjRweCBzb2xpZCAjMmQzMDQ3O2JvdHRvbTotNHB4O2NvbnRlbnQ6XCJcIjtsZWZ0OjUwJTttYXJnaW4tbGVmdDotNHB4O3Bvc2l0aW9uOmFic29sdXRlXFxufVxcbi4tb3ZlcmZsb3ctaGlkZGVue292ZXJmbG93OmhpZGRlblxcbn1cXG4uLWlzLWhpZGRlbntkaXNwbGF5Om5vbmVcXG59XFxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDo3NjdweCl7XFxuLi1oaWRlLXVwLXRvLXRhYmxldHtkaXNwbGF5Om5vbmVcXG59XFxufVxcbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6NzY4cHgpe1xcbi4taGlkZS1vbi1kZXNrdG9we2Rpc3BsYXk6bm9uZVxcbn1cXG59JyxcIlwiXSl9LGZ1bmN0aW9uKHQsZSl7ZnVuY3Rpb24gbih0LGUpe3ZhciBuPXRbMV18fFwiXCIsbz10WzNdO2lmKCFvKXJldHVybiBuO2lmKGUmJlwiZnVuY3Rpb25cIj09dHlwZW9mIGJ0b2Epe3ZhciBhPWkobyk7cmV0dXJuW25dLmNvbmNhdChvLnNvdXJjZXMubWFwKGZ1bmN0aW9uKHQpe3JldHVyblwiLyojIHNvdXJjZVVSTD1cIitvLnNvdXJjZVJvb3QrdCtcIiAqL1wifSkpLmNvbmNhdChbYV0pLmpvaW4oXCJcXG5cIil9cmV0dXJuW25dLmpvaW4oXCJcXG5cIil9ZnVuY3Rpb24gaSh0KXtyZXR1cm5cIi8qIyBzb3VyY2VNYXBwaW5nVVJMPWRhdGE6YXBwbGljYXRpb24vanNvbjtjaGFyc2V0PXV0Zi04O2Jhc2U2NCxcIitidG9hKHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeSh0KSkpKStcIiAqL1wifXQuZXhwb3J0cz1mdW5jdGlvbih0KXt2YXIgZT1bXTtyZXR1cm4gZS50b1N0cmluZz1mdW5jdGlvbigpe3JldHVybiB0aGlzLm1hcChmdW5jdGlvbihlKXt2YXIgaT1uKGUsdCk7cmV0dXJuIGVbMl0/XCJAbWVkaWEgXCIrZVsyXStcIntcIitpK1wifVwiOml9KS5qb2luKFwiXCIpfSxlLmk9ZnVuY3Rpb24odCxuKXtcInN0cmluZ1wiPT10eXBlb2YgdCYmKHQ9W1tudWxsLHQsXCJcIl1dKTtmb3IodmFyIGk9e30sbz0wO288dGhpcy5sZW5ndGg7bysrKXt2YXIgYT10aGlzW29dWzBdO1wibnVtYmVyXCI9PXR5cGVvZiBhJiYoaVthXT0hMCl9Zm9yKG89MDtvPHQubGVuZ3RoO28rKyl7dmFyIHI9dFtvXTtcIm51bWJlclwiPT10eXBlb2YgclswXSYmaVtyWzBdXXx8KG4mJiFyWzJdP3JbMl09bjpuJiYoclsyXT1cIihcIityWzJdK1wiKSBhbmQgKFwiK24rXCIpXCIpLGUucHVzaChyKSl9fSxlfX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9XCJkYXRhOmltYWdlL3N2Zyt4bWw7YmFzZTY0LFBITjJaeUI0Yld4dWN6MGlhSFIwY0RvdkwzZDNkeTUzTXk1dmNtY3ZNakF3TUM5emRtY2lJSGh0Ykc1ek9uaHNhVzVyUFNKb2RIUndPaTh2ZDNkM0xuY3pMbTl5Wnk4eE9UazVMM2hzYVc1cklpQjNhV1IwYUQwaU1UWWlJR2hsYVdkb2REMGlNVGdpSUhacFpYZENiM2c5SWpBZ01DQXhOaUF4T0NJK0NpQWdJQ0E4WkdWbWN6NEtJQ0FnSUNBZ0lDQThjR0YwYUNCcFpEMGlZU0lnWkQwaVRUQWdNVGN1TXpFeGFERTFMamMxTkZZdU1URTFTREI2SWk4K0NpQWdJQ0E4TDJSbFpuTStDaUFnSUNBOFp5Qm1hV3hzUFNKdWIyNWxJaUJtYVd4c0xYSjFiR1U5SW1WMlpXNXZaR1FpUGdvZ0lDQWdJQ0FnSUR4bklIUnlZVzV6Wm05eWJUMGlkSEpoYm5Oc1lYUmxLREFnTGpFME9Da2lQZ29nSUNBZ0lDQWdJQ0FnSUNBOGJXRnpheUJwWkQwaVlpSWdabWxzYkQwaUkyWm1aaUkrQ2lBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0E4ZFhObElIaHNhVzVyT21oeVpXWTlJaU5oSWk4K0NpQWdJQ0FnSUNBZ0lDQWdJRHd2YldGemF6NEtJQ0FnSUNBZ0lDQWdJQ0FnUEhCaGRHZ2dabWxzYkQwaUl6QXdRMEU1UkNJZ1pEMGlUVEUwTGpRMk5TQXhOaTR5TVRWSU1TNHlPV0V1TVRrMkxqRTVOaUF3SURBZ01TMHVNVGswTFM0eE9UTjJMVGd1Tmpsb01UTXVOVFl5ZGpndU5qbGhMakU1Tmk0eE9UWWdNQ0F3SURFdExqRTVNeTR4T1RONlRURXVNamtnTWk0NU5UaG9NUzR3TVROMkxqVTJNMk13SUM0NE1UY3VOalkwSURFdU5EZ3lJREV1TkRneElERXVORGd5TGpneE9DQXdJREV1TkRneUxTNDJOalVnTVM0ME9ESXRNUzQwT0RKMkxTNDFOak5vTlM0d05EWjJMalUyTTJNd0lDNDRNVGN1TmpZMUlERXVORGd5SURFdU5EZ3lJREV1TkRneWN6RXVORGd5TFM0Mk5qVWdNUzQwT0RJdE1TNDBPREoyTFM0MU5qTm9NUzR4T0RsakxqRXdOU0F3SUM0eE9UTXVNRGc1TGpFNU15NHhPVE4yTXk0eE5ESklNUzR3T1RaV015NHhOVEZqTUMwdU1UQTBMakE0T1MwdU1Ua3pMakU1TkMwdU1Ua3plazB6TGpNMElERXVOVGsyWVM0ME5EUXVORFEwSURBZ01DQXhJQzQ0T0RjZ01IWXhMamt5TldFdU5EUTBMalEwTkNBd0lEQWdNUzB1T0RnM0lEQldNUzQxT1RaNmJUZ3VNREVnTUdFdU5EUTBMalEwTkNBd0lEQWdNU0F1T0RnMklEQjJNUzQ1TWpWaExqUTBOQzQwTkRRZ01DQXdJREV0TGpnNE55QXdWakV1TlRrMmVtMHpMakV4TkM0eU5qWm9MVEV1TVRsMkxTNHlOalpqTUMwdU9ERTNMUzQyTmpRdE1TNDBPREl0TVM0ME9ERXRNUzQwT0RJdExqZ3hOeUF3TFRFdU5EZ3lMalkyTlMweExqUTRNaUF4TGpRNE1uWXVNalkyU0RVdU1qWTJkaTB1TWpZMlF6VXVNalkyTGpjNElEUXVOakF5TGpFeE5DQXpMamM0TkM0eE1UUmpMUzQ0TVRjZ01DMHhMalE0TVM0Mk5qVXRNUzQwT0RFZ01TNDBPREoyTGpJMk5rZ3hMakk1UXk0MU56a2dNUzQ0TmpJZ01DQXlMalEwSURBZ015NHhOVEoyTVRJdU9EZGpNQ0F1TnpFeExqVTNPU0F4TGpJNUlERXVNamtnTVM0eU9XZ3hNeTR4TnpWaE1TNHlPU0F4TGpJNUlEQWdNQ0F3SURFdU1qa3RNUzR5T1ZZekxqRTFNbUV4TGpJNUlERXVNamtnTUNBd0lEQXRNUzR5T1MweExqSTVlaUlnYldGemF6MGlkWEpzS0NOaUtTSXZQZ29nSUNBZ0lDQWdJRHd2Wno0S0lDQWdJQ0FnSUNBOGNHRjBhQ0JtYVd4c1BTSWpNREJEUVRsRUlpQmtQU0pOTXk0eE9UUWdNVEF1T1RrNGFERXVNelUzVmprdU5qUXlTRE11TVRrMGVrMDFMamcyTkNBeE1DNDVPVGhvTVM0ek5UZFdPUzQyTkRKSU5TNDROalI2VFRndU5UTTBJREV3TGprNU9FZzVMamc1VmprdU5qUXlTRGd1TlRNMGVrMHhNUzR5TURRZ01UQXVPVGs0YURFdU16VTJWamt1TmpReWFDMHhMak0xTm5wTk15NHhPVFFnTVRNdU9UUm9NUzR6TlRkMkxURXVNelUzU0RNdU1UazBlazAxTGpnMk5DQXhNeTQ1TkdneExqTTFOM1l0TVM0ek5UZElOUzQ0TmpSNlRUZ3VOVE0wSURFekxqazBTRGt1T0RsMkxURXVNelUzU0RndU5UTTBlazB4TVM0eU1EUWdNVE11T1RSb01TNHpOVFoyTFRFdU16VTNhQzB4TGpNMU5ub2lMejRLSUNBZ0lEd3ZaejRLUEM5emRtYytDZz09XCJ9LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPVwiZGF0YTppbWFnZS9zdmcreG1sO2Jhc2U2NCxQSE4yWnlCNGJXeHVjejBpYUhSMGNEb3ZMM2QzZHk1M015NXZjbWN2TWpBd01DOXpkbWNpSUhkcFpIUm9QU0k0SWlCb1pXbG5hSFE5SWpFNElpQjJhV1YzUW05NFBTSXdJREFnT0NBeE9DSStDaUFnSUNBOGNHRjBhQ0JtYVd4c1BTSWpPVFU1T1VGQklpQm1hV3hzTFhKMWJHVTlJbTV2Ym5wbGNtOGlJR1E5SWswdU1URTVMamN4T0d3M0xqRTFPQ0EzTGpRd055MHVNRE16TFM0MU5URXROaTQzTXpjZ09DNDRPRGxoTGpReU5TNDBNalVnTUNBd0lEQWdMakE0TGpVNU15NDBNaTQwTWlBd0lEQWdNQ0F1TlRrdExqQTRiRFl1TnpNM0xUZ3VPRGc1WVM0ME1qVXVOREkxSURBZ01DQXdMUzR3TXpNdExqVTFNVXd1TnpJekxqRXlPRUV1TkRJdU5ESWdNQ0F3SURBZ0xqRXlPQzR4TW1FdU5ESTFMalF5TlNBd0lEQWdNQzB1TURBNUxqVTVPSG9pTHo0S1BDOXpkbWMrQ2c9PVwifSxmdW5jdGlvbih0LGUsbil7ZnVuY3Rpb24gaSh0KXtmb3IodmFyIGU9MDtlPHQubGVuZ3RoO2UrKyl7dmFyIG49dFtlXSxpPWRbbi5pZF07aWYoaSl7aS5yZWZzKys7Zm9yKHZhciBvPTA7bzxpLnBhcnRzLmxlbmd0aDtvKyspaS5wYXJ0c1tvXShuLnBhcnRzW29dKTtmb3IoO288bi5wYXJ0cy5sZW5ndGg7bysrKWkucGFydHMucHVzaChhKG4ucGFydHNbb10pKTtpLnBhcnRzLmxlbmd0aD5uLnBhcnRzLmxlbmd0aCYmKGkucGFydHMubGVuZ3RoPW4ucGFydHMubGVuZ3RoKX1lbHNle2Zvcih2YXIgcj1bXSxvPTA7bzxuLnBhcnRzLmxlbmd0aDtvKyspci5wdXNoKGEobi5wYXJ0c1tvXSkpO2Rbbi5pZF09e2lkOm4uaWQscmVmczoxLHBhcnRzOnJ9fX19ZnVuY3Rpb24gbygpe3ZhciB0PWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJzdHlsZVwiKTtyZXR1cm4gdC50eXBlPVwidGV4dC9jc3NcIixoLmFwcGVuZENoaWxkKHQpLHR9ZnVuY3Rpb24gYSh0KXt2YXIgZSxuLGk9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInN0eWxlW1wiK3krJ349XCInK3QuaWQrJ1wiXScpO2lmKGkpe2lmKGYpcmV0dXJuIG07aS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGkpfWlmKEQpe3ZhciBhPXArKztpPWx8fChsPW8oKSksZT1yLmJpbmQobnVsbCxpLGEsITEpLG49ci5iaW5kKG51bGwsaSxhLCEwKX1lbHNlIGk9bygpLGU9cy5iaW5kKG51bGwsaSksbj1mdW5jdGlvbigpe2kucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChpKX07cmV0dXJuIGUodCksZnVuY3Rpb24oaSl7aWYoaSl7aWYoaS5jc3M9PT10LmNzcyYmaS5tZWRpYT09PXQubWVkaWEmJmkuc291cmNlTWFwPT09dC5zb3VyY2VNYXApcmV0dXJuO2UodD1pKX1lbHNlIG4oKX19ZnVuY3Rpb24gcih0LGUsbixpKXt2YXIgbz1uP1wiXCI6aS5jc3M7aWYodC5zdHlsZVNoZWV0KXQuc3R5bGVTaGVldC5jc3NUZXh0PWsoZSxvKTtlbHNle3ZhciBhPWRvY3VtZW50LmNyZWF0ZVRleHROb2RlKG8pLHI9dC5jaGlsZE5vZGVzO3JbZV0mJnQucmVtb3ZlQ2hpbGQocltlXSksci5sZW5ndGg/dC5pbnNlcnRCZWZvcmUoYSxyW2VdKTp0LmFwcGVuZENoaWxkKGEpfX1mdW5jdGlvbiBzKHQsZSl7dmFyIG49ZS5jc3MsaT1lLm1lZGlhLG89ZS5zb3VyY2VNYXA7aWYoaSYmdC5zZXRBdHRyaWJ1dGUoXCJtZWRpYVwiLGkpLGcuc3NySWQmJnQuc2V0QXR0cmlidXRlKHksZS5pZCksbyYmKG4rPVwiXFxuLyojIHNvdXJjZVVSTD1cIitvLnNvdXJjZXNbMF0rXCIgKi9cIixuKz1cIlxcbi8qIyBzb3VyY2VNYXBwaW5nVVJMPWRhdGE6YXBwbGljYXRpb24vanNvbjtiYXNlNjQsXCIrYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkobykpKSkrXCIgKi9cIiksdC5zdHlsZVNoZWV0KXQuc3R5bGVTaGVldC5jc3NUZXh0PW47ZWxzZXtmb3IoO3QuZmlyc3RDaGlsZDspdC5yZW1vdmVDaGlsZCh0LmZpcnN0Q2hpbGQpO3QuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUobikpfX12YXIgYz1cInVuZGVmaW5lZFwiIT10eXBlb2YgZG9jdW1lbnQ7aWYoXCJ1bmRlZmluZWRcIiE9dHlwZW9mIERFQlVHJiZERUJVRyYmIWMpdGhyb3cgbmV3IEVycm9yKFwidnVlLXN0eWxlLWxvYWRlciBjYW5ub3QgYmUgdXNlZCBpbiBhIG5vbi1icm93c2VyIGVudmlyb25tZW50LiBVc2UgeyB0YXJnZXQ6ICdub2RlJyB9IGluIHlvdXIgV2VicGFjayBjb25maWcgdG8gaW5kaWNhdGUgYSBzZXJ2ZXItcmVuZGVyaW5nIGVudmlyb25tZW50LlwiKTt2YXIgdT1uKDIzKSxkPXt9LGg9YyYmKGRvY3VtZW50LmhlYWR8fGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiaGVhZFwiKVswXSksbD1udWxsLHA9MCxmPSExLG09ZnVuY3Rpb24oKXt9LGc9bnVsbCx5PVwiZGF0YS12dWUtc3NyLWlkXCIsRD1cInVuZGVmaW5lZFwiIT10eXBlb2YgbmF2aWdhdG9yJiYvbXNpZSBbNi05XVxcYi8udGVzdChuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkpO3QuZXhwb3J0cz1mdW5jdGlvbih0LGUsbixvKXtmPW4sZz1vfHx7fTt2YXIgYT11KHQsZSk7cmV0dXJuIGkoYSksZnVuY3Rpb24oZSl7Zm9yKHZhciBuPVtdLG89MDtvPGEubGVuZ3RoO28rKyl7dmFyIHI9YVtvXSxzPWRbci5pZF07cy5yZWZzLS0sbi5wdXNoKHMpfWU/KGE9dSh0LGUpLGkoYSkpOmE9W107Zm9yKHZhciBvPTA7bzxuLmxlbmd0aDtvKyspe3ZhciBzPW5bb107aWYoMD09PXMucmVmcyl7Zm9yKHZhciBjPTA7YzxzLnBhcnRzLmxlbmd0aDtjKyspcy5wYXJ0c1tjXSgpO2RlbGV0ZSBkW3MuaWRdfX19fTt2YXIgaz1mdW5jdGlvbigpe3ZhciB0PVtdO3JldHVybiBmdW5jdGlvbihlLG4pe3JldHVybiB0W2VdPW4sdC5maWx0ZXIoQm9vbGVhbikuam9pbihcIlxcblwiKX19KCl9LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7Zm9yKHZhciBuPVtdLGk9e30sbz0wO288ZS5sZW5ndGg7bysrKXt2YXIgYT1lW29dLHI9YVswXSxzPWFbMV0sYz1hWzJdLHU9YVszXSxkPXtpZDp0K1wiOlwiK28sY3NzOnMsbWVkaWE6Yyxzb3VyY2VNYXA6dX07aVtyXT9pW3JdLnBhcnRzLnB1c2goZCk6bi5wdXNoKGlbcl09e2lkOnIscGFydHM6W2RdfSl9cmV0dXJuIG59fSxmdW5jdGlvbih0LGUsbil7dC5leHBvcnRzPXtkZWZhdWx0Om4oMjUpLF9fZXNNb2R1bGU6ITB9fSxmdW5jdGlvbih0LGUsbil7bigyNiksdC5leHBvcnRzPW4oOCkuT2JqZWN0LmFzc2lnbn0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMjcpO2koaS5TK2kuRixcIk9iamVjdFwiLHthc3NpZ246bigzNyl9KX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMCksbz1uKDgpLGE9bigyOCkscj1uKDMwKSxzPWZ1bmN0aW9uKHQsZSxuKXt2YXIgYyx1LGQsaD10JnMuRixsPXQmcy5HLHA9dCZzLlMsZj10JnMuUCxtPXQmcy5CLGc9dCZzLlcseT1sP286b1tlXXx8KG9bZV09e30pLEQ9eS5wcm90b3R5cGUsaz1sP2k6cD9pW2VdOihpW2VdfHx7fSkucHJvdG90eXBlO2wmJihuPWUpO2ZvcihjIGluIG4pKHU9IWgmJmsmJnZvaWQgMCE9PWtbY10pJiZjIGluIHl8fChkPXU/a1tjXTpuW2NdLHlbY109bCYmXCJmdW5jdGlvblwiIT10eXBlb2Yga1tjXT9uW2NdOm0mJnU/YShkLGkpOmcmJmtbY109PWQ/ZnVuY3Rpb24odCl7dmFyIGU9ZnVuY3Rpb24oZSxuLGkpe2lmKHRoaXMgaW5zdGFuY2VvZiB0KXtzd2l0Y2goYXJndW1lbnRzLmxlbmd0aCl7Y2FzZSAwOnJldHVybiBuZXcgdDtjYXNlIDE6cmV0dXJuIG5ldyB0KGUpO2Nhc2UgMjpyZXR1cm4gbmV3IHQoZSxuKX1yZXR1cm4gbmV3IHQoZSxuLGkpfXJldHVybiB0LmFwcGx5KHRoaXMsYXJndW1lbnRzKX07cmV0dXJuIGUucHJvdG90eXBlPXQucHJvdG90eXBlLGV9KGQpOmYmJlwiZnVuY3Rpb25cIj09dHlwZW9mIGQ/YShGdW5jdGlvbi5jYWxsLGQpOmQsZiYmKCh5LnZpcnR1YWx8fCh5LnZpcnR1YWw9e30pKVtjXT1kLHQmcy5SJiZEJiYhRFtjXSYmcihELGMsZCkpKX07cy5GPTEscy5HPTIscy5TPTQscy5QPTgscy5CPTE2LHMuVz0zMixzLlU9NjQscy5SPTEyOCx0LmV4cG9ydHM9c30sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMjkpO3QuZXhwb3J0cz1mdW5jdGlvbih0LGUsbil7aWYoaSh0KSx2b2lkIDA9PT1lKXJldHVybiB0O3N3aXRjaChuKXtjYXNlIDE6cmV0dXJuIGZ1bmN0aW9uKG4pe3JldHVybiB0LmNhbGwoZSxuKX07Y2FzZSAyOnJldHVybiBmdW5jdGlvbihuLGkpe3JldHVybiB0LmNhbGwoZSxuLGkpfTtjYXNlIDM6cmV0dXJuIGZ1bmN0aW9uKG4saSxvKXtyZXR1cm4gdC5jYWxsKGUsbixpLG8pfX1yZXR1cm4gZnVuY3Rpb24oKXtyZXR1cm4gdC5hcHBseShlLGFyZ3VtZW50cyl9fX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7aWYoXCJmdW5jdGlvblwiIT10eXBlb2YgdCl0aHJvdyBUeXBlRXJyb3IodCtcIiBpcyBub3QgYSBmdW5jdGlvbiFcIik7cmV0dXJuIHR9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigzMSksbz1uKDM2KTt0LmV4cG9ydHM9bigyKT9mdW5jdGlvbih0LGUsbil7cmV0dXJuIGkuZih0LGUsbygxLG4pKX06ZnVuY3Rpb24odCxlLG4pe3JldHVybiB0W2VdPW4sdH19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDMyKSxvPW4oMzMpLGE9bigzNSkscj1PYmplY3QuZGVmaW5lUHJvcGVydHk7ZS5mPW4oMik/T2JqZWN0LmRlZmluZVByb3BlcnR5OmZ1bmN0aW9uKHQsZSxuKXtpZihpKHQpLGU9YShlLCEwKSxpKG4pLG8pdHJ5e3JldHVybiByKHQsZSxuKX1jYXRjaCh0KXt9aWYoXCJnZXRcImluIG58fFwic2V0XCJpbiBuKXRocm93IFR5cGVFcnJvcihcIkFjY2Vzc29ycyBub3Qgc3VwcG9ydGVkIVwiKTtyZXR1cm5cInZhbHVlXCJpbiBuJiYodFtlXT1uLnZhbHVlKSx0fX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMSk7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe2lmKCFpKHQpKXRocm93IFR5cGVFcnJvcih0K1wiIGlzIG5vdCBhbiBvYmplY3QhXCIpO3JldHVybiB0fX0sZnVuY3Rpb24odCxlLG4pe3QuZXhwb3J0cz0hbigyKSYmIW4oMykoZnVuY3Rpb24oKXtyZXR1cm4gNyE9T2JqZWN0LmRlZmluZVByb3BlcnR5KG4oMzQpKFwiZGl2XCIpLFwiYVwiLHtnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gN319KS5hfSl9LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDEpLG89bigwKS5kb2N1bWVudCxhPWkobykmJmkoby5jcmVhdGVFbGVtZW50KTt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cmV0dXJuIGE/by5jcmVhdGVFbGVtZW50KHQpOnt9fX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMSk7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7aWYoIWkodCkpcmV0dXJuIHQ7dmFyIG4sbztpZihlJiZcImZ1bmN0aW9uXCI9PXR5cGVvZihuPXQudG9TdHJpbmcpJiYhaShvPW4uY2FsbCh0KSkpcmV0dXJuIG87aWYoXCJmdW5jdGlvblwiPT10eXBlb2Yobj10LnZhbHVlT2YpJiYhaShvPW4uY2FsbCh0KSkpcmV0dXJuIG87aWYoIWUmJlwiZnVuY3Rpb25cIj09dHlwZW9mKG49dC50b1N0cmluZykmJiFpKG89bi5jYWxsKHQpKSlyZXR1cm4gbzt0aHJvdyBUeXBlRXJyb3IoXCJDYW4ndCBjb252ZXJ0IG9iamVjdCB0byBwcmltaXRpdmUgdmFsdWVcIil9fSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz1mdW5jdGlvbih0LGUpe3JldHVybntlbnVtZXJhYmxlOiEoMSZ0KSxjb25maWd1cmFibGU6ISgyJnQpLHdyaXRhYmxlOiEoNCZ0KSx2YWx1ZTplfX19LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1uKDM4KSxvPW4oNDkpLGE9big1MCkscj1uKDUxKSxzPW4oMTApLGM9T2JqZWN0LmFzc2lnbjt0LmV4cG9ydHM9IWN8fG4oMykoZnVuY3Rpb24oKXt2YXIgdD17fSxlPXt9LG49U3ltYm9sKCksaT1cImFiY2RlZmdoaWprbG1ub3BxcnN0XCI7cmV0dXJuIHRbbl09NyxpLnNwbGl0KFwiXCIpLmZvckVhY2goZnVuY3Rpb24odCl7ZVt0XT10fSksNyE9Yyh7fSx0KVtuXXx8T2JqZWN0LmtleXMoYyh7fSxlKSkuam9pbihcIlwiKSE9aX0pP2Z1bmN0aW9uKHQsZSl7Zm9yKHZhciBuPXIodCksYz1hcmd1bWVudHMubGVuZ3RoLHU9MSxkPW8uZixoPWEuZjtjPnU7KWZvcih2YXIgbCxwPXMoYXJndW1lbnRzW3UrK10pLGY9ZD9pKHApLmNvbmNhdChkKHApKTppKHApLG09Zi5sZW5ndGgsZz0wO20+ZzspaC5jYWxsKHAsbD1mW2crK10pJiYobltsXT1wW2xdKTtyZXR1cm4gbn06Y30sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMzkpLG89big0OCk7dC5leHBvcnRzPU9iamVjdC5rZXlzfHxmdW5jdGlvbih0KXtyZXR1cm4gaSh0LG8pfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNDApLG89big5KSxhPW4oNDIpKCExKSxyPW4oNDUpKFwiSUVfUFJPVE9cIik7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7dmFyIG4scz1vKHQpLGM9MCx1PVtdO2ZvcihuIGluIHMpbiE9ciYmaShzLG4pJiZ1LnB1c2gobik7Zm9yKDtlLmxlbmd0aD5jOylpKHMsbj1lW2MrK10pJiYofmEodSxuKXx8dS5wdXNoKG4pKTtyZXR1cm4gdX19LGZ1bmN0aW9uKHQsZSl7dmFyIG49e30uaGFzT3duUHJvcGVydHk7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7cmV0dXJuIG4uY2FsbCh0LGUpfX0sZnVuY3Rpb24odCxlKXt2YXIgbj17fS50b1N0cmluZzt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cmV0dXJuIG4uY2FsbCh0KS5zbGljZSg4LC0xKX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDkpLG89big0MyksYT1uKDQ0KTt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cmV0dXJuIGZ1bmN0aW9uKGUsbixyKXt2YXIgcyxjPWkoZSksdT1vKGMubGVuZ3RoKSxkPWEocix1KTtpZih0JiZuIT1uKXtmb3IoO3U+ZDspaWYoKHM9Y1tkKytdKSE9cylyZXR1cm4hMH1lbHNlIGZvcig7dT5kO2QrKylpZigodHx8ZCBpbiBjKSYmY1tkXT09PW4pcmV0dXJuIHR8fGR8fDA7cmV0dXJuIXQmJi0xfX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDEyKSxvPU1hdGgubWluO3QuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm4gdD4wP28oaSh0KSw5MDA3MTk5MjU0NzQwOTkxKTowfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMTIpLG89TWF0aC5tYXgsYT1NYXRoLm1pbjt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXtyZXR1cm4gdD1pKHQpLHQ8MD9vKHQrZSwwKTphKHQsZSl9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9big0NikoXCJrZXlzXCIpLG89big0Nyk7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVybiBpW3RdfHwoaVt0XT1vKHQpKX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDApLG89aVtcIl9fY29yZS1qc19zaGFyZWRfX1wiXXx8KGlbXCJfX2NvcmUtanNfc2hhcmVkX19cIl09e30pO3QuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm4gb1t0XXx8KG9bdF09e30pfX0sZnVuY3Rpb24odCxlKXt2YXIgbj0wLGk9TWF0aC5yYW5kb20oKTt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cmV0dXJuXCJTeW1ib2woXCIuY29uY2F0KHZvaWQgMD09PXQ/XCJcIjp0LFwiKV9cIiwoKytuK2kpLnRvU3RyaW5nKDM2KSl9fSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz1cImNvbnN0cnVjdG9yLGhhc093blByb3BlcnR5LGlzUHJvdG90eXBlT2YscHJvcGVydHlJc0VudW1lcmFibGUsdG9Mb2NhbGVTdHJpbmcsdG9TdHJpbmcsdmFsdWVPZlwiLnNwbGl0KFwiLFwiKX0sZnVuY3Rpb24odCxlKXtlLmY9T2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9sc30sZnVuY3Rpb24odCxlKXtlLmY9e30ucHJvcGVydHlJc0VudW1lcmFibGV9LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDExKTt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cmV0dXJuIE9iamVjdChpKHQpKX19LGZ1bmN0aW9uKHQsZSxuKXshZnVuY3Rpb24odCxuKXtuKGUpfSgwLGZ1bmN0aW9uKHQpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIGUodCxlLG4pe3JldHVybiB0LmFkZEV2ZW50TGlzdGVuZXIoZSxuLCExKSx7ZGVzdHJveTpmdW5jdGlvbigpe3JldHVybiB0LnJlbW92ZUV2ZW50TGlzdGVuZXIoZSxuLCExKX19fWZ1bmN0aW9uIG4odCxuKXt2YXIgaT0hMSxvPWUodCxcIm1vdXNlZW50ZXJcIixmdW5jdGlvbigpe2k9ITB9KSxhPWUodCxcIm1vdXNlbGVhdmVcIixmdW5jdGlvbigpe2k9ITF9KTtyZXR1cm57ZWw6dCxjaGVjazpmdW5jdGlvbih0KXtpfHxuKHQpfSxkZXN0cm95OmZ1bmN0aW9uKCl7by5kZXN0cm95KCksYS5kZXN0cm95KCl9fX1mdW5jdGlvbiBpKHQsZSl7cmV0dXJue2VsOnQsY2hlY2s6ZnVuY3Rpb24obil7dC5jb250YWlucyhuLnRhcmdldCl8fGUobil9LGRlc3Ryb3k6ZnVuY3Rpb24oKXt9fX1mdW5jdGlvbiBvKHQsbyl7dmFyIGE9by52YWx1ZSx1PW8ubW9kaWZpZXJzO3IodCksY3x8KGM9ZShkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQsXCJjbGlja1wiLGZ1bmN0aW9uKHQpe3MuZm9yRWFjaChmdW5jdGlvbihlKXtyZXR1cm4gZS5jaGVjayh0KX0pfSkpLHNldFRpbWVvdXQoZnVuY3Rpb24oKXtzLnB1c2godS5zdGF0aWM/aSh0LGEpOm4odCxhKSl9LDApfWZ1bmN0aW9uIGEodCxlKXtlLnZhbHVlIT09ZS5vbGRWYWx1ZSYmbyh0LGUpfWZ1bmN0aW9uIHIodCl7Zm9yKHZhciBlPXMubGVuZ3RoLTE7ZT49MDspc1tlXS5lbD09PXQmJihzW2VdLmRlc3Ryb3koKSxzLnNwbGljZShlLDEpKSxlLT0xOzA9PT1zLmxlbmd0aCYmYyYmKGMuZGVzdHJveSgpLGM9bnVsbCl9dmFyIHM9W10sYz12b2lkIDAsdT17YmluZDpvLHVuYmluZDpyLHVwZGF0ZTphfSxkPXtkaXJlY3RpdmVzOntcIm9uLWNsaWNrLW91dHNpZGVcIjp1fX07dC5kaXJlY3RpdmU9dSx0Lm1peGluPWQsT2JqZWN0LmRlZmluZVByb3BlcnR5KHQsXCJfX2VzTW9kdWxlXCIse3ZhbHVlOiEwfSl9KX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO09iamVjdC5kZWZpbmVQcm9wZXJ0eShlLFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pO3ZhciBpPW4oMTQpLG89bi5uKGkpO2Zvcih2YXIgYSBpbiBpKVwiZGVmYXVsdFwiIT09YSYmZnVuY3Rpb24odCl7bi5kKGUsdCxmdW5jdGlvbigpe3JldHVybiBpW3RdfSl9KGEpO3ZhciByPW4oNTQpLHM9big1KSxjPXMoby5hLHIuYSwhMSxudWxsLG51bGwsbnVsbCk7Yy5vcHRpb25zLl9fZmlsZT1cInNyY1xcXFxjb21wb25lbnRzXFxcXERheS52dWVcIixlLmRlZmF1bHQ9Yy5leHBvcnRzfSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7dmFyIGk9ZnVuY3Rpb24oKXt2YXIgdD10aGlzLGU9dC4kY3JlYXRlRWxlbWVudCxuPXQuX3NlbGYuX2N8fGU7cmV0dXJuIG4oXCJkaXZcIixbbihcInNwYW5cIiksdC5zaG93VG9vbHRpcCYmdGhpcy5vcHRpb25zLmhvdmVyaW5nVG9vbHRpcD9uKFwiZGl2XCIse3N0YXRpY0NsYXNzOlwiZGF0ZXBpY2tlcl9fdG9vbHRpcFwiLGRvbVByb3BzOntpbm5lckhUTUw6dC5fcyh0LnRvb2x0aXBNZXNzYWdlRGlzcGxheSl9fSk6dC5fZSgpLG4oXCJkaXZcIix7c3RhdGljQ2xhc3M6XCJkYXRlcGlja2VyX19tb250aC1kYXlcIixjbGFzczp0LmRheUNsYXNzLGRvbVByb3BzOnt0ZXh0Q29udGVudDp0Ll9zKFwiXCIrdC5kYXlOdW1iZXIpfSxvbjp7Y2xpY2s6ZnVuY3Rpb24oZSl7dC5kYXlDbGlja2VkKHQuZGF0ZSl9fX0pXSl9LG89W107aS5fd2l0aFN0cmlwcGVkPSEwO3ZhciBhPXtyZW5kZXI6aSxzdGF0aWNSZW5kZXJGbnM6b307ZS5hPWF9LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1mdW5jdGlvbigpe3ZhciB0PXRoaXMsZT10LiRjcmVhdGVFbGVtZW50LG49dC5fc2VsZi5fY3x8ZTtyZXR1cm4gdC5zaG93P24oXCJkaXZcIix7ZGlyZWN0aXZlczpbe25hbWU6XCJvbi1jbGljay1vdXRzaWRlXCIscmF3TmFtZTpcInYtb24tY2xpY2stb3V0c2lkZVwiLHZhbHVlOnQuaGlkZURhdGVwaWNrZXIsZXhwcmVzc2lvbjpcImhpZGVEYXRlcGlja2VyXCJ9XSxzdGF0aWNDbGFzczpcImRhdGVwaWNrZXJfX3dyYXBwZXJcIn0sW3QuaXNPcGVuP24oXCJkaXZcIix7c3RhdGljQ2xhc3M6XCJkYXRlcGlja2VyX19jbG9zZS1idXR0b24gLWhpZGUtb24tZGVza3RvcFwiLG9uOntjbGljazp0LmhpZGVEYXRlcGlja2VyfX0sW3QuX3YoXCLvvItcIildKTp0Ll9lKCksbihcImRpdlwiLHtzdGF0aWNDbGFzczpcImRhdGVwaWNrZXJfX2R1bW15LXdyYXBwZXJcIixjbGFzczp0LmlzT3Blbj9cImRhdGVwaWNrZXJfX2R1bW15LXdyYXBwZXItLWlzLWFjdGl2ZVwiOlwiXCIsb246e2NsaWNrOmZ1bmN0aW9uKGUpe3QuaXNPcGVuPSF0LmlzT3Blbn19fSxbbihcImJ1dHRvblwiLHtzdGF0aWNDbGFzczpcImRhdGVwaWNrZXJfX2R1bW15LWlucHV0IGRhdGVwaWNrZXJfX2lucHV0XCIsY2xhc3M6KHQuaXNPcGVuJiZudWxsPT10LmNoZWNrSW4/XCJkYXRlcGlja2VyX19kdW1teS1pbnB1dC0taXMtYWN0aXZlXCI6XCJcIikrXCIgXCIrKHQuc2luZ2xlRGF5U2VsZWN0aW9uP1wiZGF0ZXBpY2tlcl9fZHVtbXktaW5wdXQtLXNpbmdsZS1kYXRlXCI6XCJcIiksYXR0cnM6e1wiZGF0YS1xYVwiOlwiZGF0ZXBpY2tlcklucHV0XCIsdHlwZTpcImJ1dHRvblwifSxkb21Qcm9wczp7dGV4dENvbnRlbnQ6dC5fcyhcIlwiKyh0LmNoZWNrSW4/dC5mb3JtYXREYXRlKHQuY2hlY2tJbik6dC5pMThuW1wiY2hlY2staW5cIl0pKX19KSx0LnNpbmdsZURheVNlbGVjdGlvbj90Ll9lKCk6bihcImJ1dHRvblwiLHtzdGF0aWNDbGFzczpcImRhdGVwaWNrZXJfX2R1bW15LWlucHV0IGRhdGVwaWNrZXJfX2lucHV0XCIsY2xhc3M6dC5pc09wZW4mJm51bGw9PXQuY2hlY2tPdXQmJm51bGwhPT10LmNoZWNrSW4/XCJkYXRlcGlja2VyX19kdW1teS1pbnB1dC0taXMtYWN0aXZlXCI6XCJcIixhdHRyczp7dHlwZTpcImJ1dHRvblwifSxkb21Qcm9wczp7dGV4dENvbnRlbnQ6dC5fcyhcIlwiKyh0LmNoZWNrT3V0P3QuZm9ybWF0RGF0ZSh0LmNoZWNrT3V0KTp0LmkxOG5bXCJjaGVjay1vdXRcIl0pKX19KV0pLG4oXCJidXR0b25cIix7c3RhdGljQ2xhc3M6XCJkYXRlcGlja2VyX19jbGVhci1idXR0b25cIixhdHRyczp7dHlwZTpcImJ1dHRvblwifSxvbjp7Y2xpY2s6dC5jbGVhclNlbGVjdGlvbn19LFt0Ll92KFwi77yLXCIpXSksbihcImRpdlwiLHtzdGF0aWNDbGFzczpcImRhdGVwaWNrZXJcIixjbGFzczp0LmlzT3Blbj9cImRhdGVwaWNrZXItLW9wZW5cIjpcImRhdGVwaWNrZXItLWNsb3NlZFwifSxbbihcImRpdlwiLHtzdGF0aWNDbGFzczpcIi1oaWRlLW9uLWRlc2t0b3BcIn0sW3QuaXNPcGVuP24oXCJkaXZcIix7c3RhdGljQ2xhc3M6XCJkYXRlcGlja2VyX19kdW1teS13cmFwcGVyIGRhdGVwaWNrZXJfX2R1bW15LXdyYXBwZXItLW5vLWJvcmRlclwiLGNsYXNzOnQuaXNPcGVuP1wiZGF0ZXBpY2tlcl9fZHVtbXktd3JhcHBlci0taXMtYWN0aXZlXCI6XCJcIixvbjp7Y2xpY2s6ZnVuY3Rpb24oZSl7dC5pc09wZW49IXQuaXNPcGVufX19LFtuKFwiYnV0dG9uXCIse3N0YXRpY0NsYXNzOlwiZGF0ZXBpY2tlcl9fZHVtbXktaW5wdXQgZGF0ZXBpY2tlcl9faW5wdXRcIixjbGFzczp0LmlzT3BlbiYmbnVsbD09dC5jaGVja0luP1wiZGF0ZXBpY2tlcl9fZHVtbXktaW5wdXQtLWlzLWFjdGl2ZVwiOlwiXCIsYXR0cnM6e3R5cGU6XCJidXR0b25cIn0sZG9tUHJvcHM6e3RleHRDb250ZW50OnQuX3MoXCJcIisodC5jaGVja0luP3QuZm9ybWF0RGF0ZSh0LmNoZWNrSW4pOnQuaTE4bltcImNoZWNrLWluXCJdKSl9fSksbihcImJ1dHRvblwiLHtzdGF0aWNDbGFzczpcImRhdGVwaWNrZXJfX2R1bW15LWlucHV0IGRhdGVwaWNrZXJfX2lucHV0XCIsY2xhc3M6dC5pc09wZW4mJm51bGw9PXQuY2hlY2tPdXQmJm51bGwhPT10LmNoZWNrSW4/XCJkYXRlcGlja2VyX19kdW1teS1pbnB1dC0taXMtYWN0aXZlXCI6XCJcIixhdHRyczp7dHlwZTpcImJ1dHRvblwifSxkb21Qcm9wczp7dGV4dENvbnRlbnQ6dC5fcyhcIlwiKyh0LmNoZWNrT3V0P3QuZm9ybWF0RGF0ZSh0LmNoZWNrT3V0KTp0LmkxOG5bXCJjaGVjay1vdXRcIl0pKX19KV0pOnQuX2UoKV0pLG4oXCJkaXZcIix7c3RhdGljQ2xhc3M6XCJkYXRlcGlja2VyX19pbm5lclwifSxbbihcImRpdlwiLHtzdGF0aWNDbGFzczpcImRhdGVwaWNrZXJfX2hlYWRlclwifSxbbihcInNwYW5cIix7c3RhdGljQ2xhc3M6XCJkYXRlcGlja2VyX19tb250aC1idXR0b24gZGF0ZXBpY2tlcl9fbW9udGgtYnV0dG9uLS1wcmV2IC1oaWRlLXVwLXRvLXRhYmxldFwiLG9uOntjbGljazp0LnJlbmRlclByZXZpb3VzTW9udGh9fSksbihcInNwYW5cIix7c3RhdGljQ2xhc3M6XCJkYXRlcGlja2VyX19tb250aC1idXR0b24gZGF0ZXBpY2tlcl9fbW9udGgtYnV0dG9uLS1uZXh0IC1oaWRlLXVwLXRvLXRhYmxldFwiLG9uOntjbGljazp0LnJlbmRlck5leHRNb250aH19KV0pLFwiZGVza3RvcFwiPT10LnNjcmVlblNpemU/bihcImRpdlwiLHtzdGF0aWNDbGFzczpcImRhdGVwaWNrZXJfX21vbnRoc1wifSx0Ll9sKFswLDFdLGZ1bmN0aW9uKGUpe3JldHVybiBuKFwiZGl2XCIse2tleTplLHN0YXRpY0NsYXNzOlwiZGF0ZXBpY2tlcl9fbW9udGhcIn0sW24oXCJoMVwiLHtzdGF0aWNDbGFzczpcImRhdGVwaWNrZXJfX21vbnRoLW5hbWVcIixkb21Qcm9wczp7dGV4dENvbnRlbnQ6dC5fcyh0LmdldE1vbnRoKHQubW9udGhzW3QuYWN0aXZlTW9udGhJbmRleCtlXS5kYXlzWzE1XS5kYXRlKSl9fSksbihcImRpdlwiLHtzdGF0aWNDbGFzczpcImRhdGVwaWNrZXJfX3dlZWstcm93IC1oaWRlLXVwLXRvLXRhYmxldFwifSx0Ll9sKHQuaTE4bltcImRheS1uYW1lc1wiXSxmdW5jdGlvbihlKXtyZXR1cm4gbihcImRpdlwiLHtzdGF0aWNDbGFzczpcImRhdGVwaWNrZXJfX3dlZWstbmFtZVwiLGRvbVByb3BzOnt0ZXh0Q29udGVudDp0Ll9zKGUpfX0pfSkpLHQuX2wodC5tb250aHNbdC5hY3RpdmVNb250aEluZGV4K2VdLmRheXMsZnVuY3Rpb24oZSl7cmV0dXJuIG4oXCJkaXZcIix7c3RhdGljQ2xhc3M6XCJzcXVhcmVcIixvbjp7bW91c2VvdmVyOmZ1bmN0aW9uKG4pe3QuaG92ZXJpbmdEYXRlPWUuZGF0ZX19fSxbbihcIkRheVwiLHthdHRyczp7b3B0aW9uczp0LiRwcm9wcyxkYXRlOmUuZGF0ZSxzb3J0ZWREaXNhYmxlZERhdGVzOnQuc29ydGVkRGlzYWJsZWREYXRlcyxuZXh0RGlzYWJsZWREYXRlOnQubmV4dERpc2FibGVkRGF0ZSxhY3RpdmVNb250aEluZGV4OnQuYWN0aXZlTW9udGhJbmRleCxob3ZlcmluZ0RhdGU6dC5ob3ZlcmluZ0RhdGUsdG9vbHRpcE1lc3NhZ2U6dC50b29sdGlwTWVzc2FnZSxkYXlOdW1iZXI6dC5nZXREYXkoZS5kYXRlKSxiZWxvbmdzVG9UaGlzTW9udGg6ZS5iZWxvbmdzVG9UaGlzTW9udGgsY2hlY2tJbjp0LmNoZWNrSW4sY2hlY2tPdXQ6dC5jaGVja091dH0sb246e2RheUNsaWNrZWQ6ZnVuY3Rpb24oZSl7dC5oYW5kbGVEYXlDbGljayhlKX19fSldLDEpfSldLDIpfSkpOnQuX2UoKSxcImRlc2t0b3BcIiE9PXQuc2NyZWVuU2l6ZSYmdC5pc09wZW4/bihcImRpdlwiLFtuKFwiZGl2XCIse3N0YXRpY0NsYXNzOlwiZGF0ZXBpY2tlcl9fd2Vlay1yb3dcIn0sdC5fbCh0aGlzLmkxOG5bXCJkYXktbmFtZXNcIl0sZnVuY3Rpb24oZSl7cmV0dXJuIG4oXCJkaXZcIix7c3RhdGljQ2xhc3M6XCJkYXRlcGlja2VyX193ZWVrLW5hbWVcIixkb21Qcm9wczp7dGV4dENvbnRlbnQ6dC5fcyhlKX19KX0pKSxuKFwiZGl2XCIse3N0YXRpY0NsYXNzOlwiZGF0ZXBpY2tlcl9fbW9udGhzXCIsYXR0cnM6e2lkOlwic3dpcGVyV3JhcHBlclwifX0sW3QuX2wodC5tb250aHMsZnVuY3Rpb24oZSxpKXtyZXR1cm4gbihcImRpdlwiLHtrZXk6aSxzdGF0aWNDbGFzczpcImRhdGVwaWNrZXJfX21vbnRoXCJ9LFtuKFwiaDFcIix7c3RhdGljQ2xhc3M6XCJkYXRlcGlja2VyX19tb250aC1uYW1lXCIsZG9tUHJvcHM6e3RleHRDb250ZW50OnQuX3ModC5nZXRNb250aCh0Lm1vbnRoc1tpXS5kYXlzWzE1XS5kYXRlKSl9fSksbihcImRpdlwiLHtzdGF0aWNDbGFzczpcImRhdGVwaWNrZXJfX3dlZWstcm93IC1oaWRlLXVwLXRvLXRhYmxldFwifSx0Ll9sKHQuaTE4bltcImRheS1uYW1lc1wiXSxmdW5jdGlvbihlKXtyZXR1cm4gbihcImRpdlwiLHtzdGF0aWNDbGFzczpcImRhdGVwaWNrZXJfX3dlZWstbmFtZVwiLGRvbVByb3BzOnt0ZXh0Q29udGVudDp0Ll9zKGUpfX0pfSkpLHQuX2wodC5tb250aHNbaV0uZGF5cyxmdW5jdGlvbihlLGkpe3JldHVybiBuKFwiZGl2XCIse2tleTppLHN0YXRpY0NsYXNzOlwic3F1YXJlXCIsb246e21vdXNlb3ZlcjpmdW5jdGlvbihuKXt0LmhvdmVyaW5nRGF0ZT1lLmRhdGV9fX0sW24oXCJEYXlcIix7YXR0cnM6e29wdGlvbnM6dC4kcHJvcHMsZGF0ZTplLmRhdGUsc29ydGVkRGlzYWJsZWREYXRlczp0LnNvcnRlZERpc2FibGVkRGF0ZXMsbmV4dERpc2FibGVkRGF0ZTp0Lm5leHREaXNhYmxlZERhdGUsYWN0aXZlTW9udGhJbmRleDp0LmFjdGl2ZU1vbnRoSW5kZXgsaG92ZXJpbmdEYXRlOnQuaG92ZXJpbmdEYXRlLHRvb2x0aXBNZXNzYWdlOnQudG9vbHRpcE1lc3NhZ2UsZGF5TnVtYmVyOnQuZ2V0RGF5KGUuZGF0ZSksYmVsb25nc1RvVGhpc01vbnRoOmUuYmVsb25nc1RvVGhpc01vbnRoLGNoZWNrSW46dC5jaGVja0luLGNoZWNrT3V0OnQuY2hlY2tPdXR9LG9uOntkYXlDbGlja2VkOmZ1bmN0aW9uKGUpe3QuaGFuZGxlRGF5Q2xpY2soZSl9fX0pXSwxKX0pXSwyKX0pLG4oXCJidXR0b25cIix7c3RhdGljQ2xhc3M6XCJuZXh0LS1tb2JpbGVcIixhdHRyczp7dHlwZTpcImJ1dHRvblwifSxvbjp7Y2xpY2s6dC5yZW5kZXJOZXh0TW9udGh9fSldLDIpXSk6dC5fZSgpXSldKV0pOnQuX2UoKX0sbz1bXTtpLl93aXRoU3RyaXBwZWQ9ITA7dmFyIGE9e3JlbmRlcjppLHN0YXRpY1JlbmRlckZuczpvfTtlLmE9YX1dKX0pO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9dnVlLWhvdGVsLWRhdGVwaWNrZXIubWluLmpzLm1hcFxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Z1ZS1ob3RlbC1kYXRlcGlja2VyL2Rpc3QvdnVlLWhvdGVsLWRhdGVwaWNrZXIubWluLmpzXG4vLyBtb2R1bGUgaWQgPSAxN1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiXSwibWFwcGluZ3MiOiI7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQzdEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3RFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFIQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBakNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3dKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQUZBO0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFGQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUZBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFKQTtBQXpCQTtBQXFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUEE7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBcktBO0FBdUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF0Q0E7QUF3Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFyQ0E7QUFDQTtBQWxPQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0tBO0FBQ0E7QUFDQTtBQUZBOzs7Ozs7O0FDWEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7OztBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7Ozs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQ0E7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBckdBO0FBQ0E7QUF1R0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXRCQTtBQUNBO0FBd0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFEQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBNWZBO0FBQ0E7QUE4ZkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTlDQTtBQUNBO0FBZ0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFsQ0E7QUFDQTtBQW9DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFmQTtBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTlFQTtBQUNBO0FBZ0ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBMTFCQTtBQUNBO0FBaTJCQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWhCQTtBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQ3A3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDcEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FDNUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUNQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQzNFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDM1hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDeEZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ3pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQ2xLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUN6Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDbitCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUN6Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUN6Q0E7QUFDQTs7O0EiLCJzb3VyY2VSb290IjoiIn0=