var detail =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 18);
/******/ })
/************************************************************************/
/******/ ({

/***/ 18:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


window.detailapp = new Vue({
    el: '#detail',
    data: {
        /*Текущие свойства*/
        tourid: curParams.tourid,

        obTourInfo: {
            operatorPic: ""
        },
        obResources: [],
        obFlights: [],
        obHotelsDescr: {},

        isShowFastOrder: false,
        fastFormPhone: "",
        fastFormName: "",
        fastFormNameHasError: false,
        fastFormPhoneHasError: false,
        fastFormSended: false,

        credit: false //Тур в рассрочку
    },

    created: function created() {
        this.init();
    },

    methods: {
        init: function init() {
            var self = this;

            self.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "actualize", tourid: self.tourid } }).then(function (response) {
                if (response.body.data.tour !== undefined) {
                    self.obTourInfo = response.body.data.tour;

                    self.obTourInfo.operatorPic = 'https://tourvisor.ru/pics/operators/searchlogo/' + self.obTourInfo.operatorcode + '.gif';

                    if (self.obTourInfo.detailavailable === "1") {
                        self.detailActualize();
                    }

                    setTimeout(function () {
                        self.loadHotel(self.obTourInfo.hotelcode);
                    }, 1000);
                }
            });
        },

        detailActualize: function detailActualize() {
            var _this = this;

            this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "actdetail", tourid: this.tourid } }).then(function (response) {
                if (!response.body.iserror) {

                    //console.log(response.body);
                    _this.obFlights = response.body.flights;
                } else {
                    // this.obResources = this.obResourcesPrev;
                    console.log('=============== Ошибка ' + response.body.errormessage + ' ================');
                }
            });
        },

        fastOrder: function fastOrder() {
            var _this2 = this;

            this.fastFormSended = false;
            var sessidval = $("#sessid").val();
            this.fastFormPhoneHasError = this.fastFormPhone.length <= 0;
            this.fastFormNameHasError = this.fastFormName.length <= 0;

            if (!this.fastFormPhoneHasError && !this.fastFormNameHasError) {

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {
                    params: {
                        type: "fastorder",
                        tourid: this.tourid,
                        phone: this.fastFormPhone,
                        name: this.fastFormName,
                        credit: this.credit ? "Y" : "N",
                        sessid: sessidval
                    }
                }).then(function (response) {
                    if (response.body.ERROR === "" && response.body.STATUS) {
                        yaCounter24395911.reachGoal('FAST_ORDER_DETAIL_TOUR');
                        _this2.fastFormSended = true;
                    }
                });
            }
        },

        order: function order() {
            var discount = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
            var $event = arguments[1];

            var orderBtn = $('.order-btn'),
                discounOrderBtn = $('.discount-order-btn');

            var params = {
                tourid: this.tourid,
                buy: "Y"
            };

            if (discount) {
                params.usediscount = 'Y';
                discounOrderBtn.addClass('running');
            } else {
                orderBtn.addClass('running');
            }

            this.$http.get('/local/tools/newtravel.search.buytour.php', { params: params }).then(function (response) {
                if (response.body === 'success') {}
            });

            this.getOrderPage();
        },

        getOrderPage: function getOrderPage() {
            setTimeout(function () {
                var el = document.getElementById('order-link');
                el.click();
                $('.order-btn').removeClass('running');
                $('.discount-order-btn').removeClass('running');
            }, 1000);
        },

        loadHotel: function loadHotel(hotelcode) {
            this.$http.get('/local/modules/newtravel.search/lib/ajax.php', { params: { type: "fullhotel", hotelcode: hotelcode } }).then(function (response) {
                if (response.body.data.hotel !== undefined) {
                    if (this.debug) {
                        console.log('======= Информация по отелю ========');
                    }
                    this.obHotelsDescr = response.body.data.hotel;
                } else {
                    if (this.debug) {
                        console.log("Ошибка fullhotel");
                    }
                }
            });
        },

        getTourInfo: function getTourInfo() {
            var self = this;

            var html = "<ul style='list-style-type: none'>";

            html += "<li>Название тура: " + self.obTourInfo.tourname + "</li>";
            html += "<li>Туроператор: " + self.obTourInfo.operatorname + "</li>";
            html += "<li>Дата вылета: " + self.obTourInfo.flydate + "(" + this.$options.filters.displayNights(self.obTourInfo.nights) + ") </li>";
            html += "<li>Город вылета: " + self.obTourInfo.departurename + "</li>";
            html += "<li>Страна: " + self.obTourInfo.countryname + "</li>";
            html += "<li>Курорт: " + self.obTourInfo.hotelregionname + "</li>";
            html += "<li>Отель: " + self.obTourInfo.hotelname + " " + self.obTourInfo.hotelstars + "*</li>";
            html += "<li>Взрослых: " + self.obTourInfo.adults + "</li>";
            html += "<li>Детей: " + self.obTourInfo.child + "</li>";
            html += "<li>Номер: " + self.obTourInfo.room + "</li>";
            html += "<li>Питание: " + self.obTourInfo.meal + "</li>";
            html += "<li>Размещение: " + self.obTourInfo.placement + "</li>";
            html += "<li>Стоимость: " + this.$options.filters.formatPrice(self.obTourInfo.price) + "</li>";
            html += "<li>Стоимость со скидкой за самостоятельность: : " + this.$options.filters.getDiscountPrice(self.obTourInfo.price) + "</li>";

            html += "</ul>";

            return html;
        }
    },

    filters: {

        displayNights: function displayNights(value) {
            if (!value) {
                return "";
            } else {
                var titles = ['ночь', 'ночи', 'ночей'];
                value = parseInt(value);
                return value + " " + titles[value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2];
            }
        },

        shortPeoples: function shortPeoples(value, type) {
            if (value) {
                var titles = [];
                if (type == 'adults') {
                    titles = ['взрослый', 'взрослых', 'взрослых'];
                } else if (type == 'kids') {
                    titles = ['ребенок', 'ребенка', 'детей'];
                }
                return value + " " + titles[value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2];
            }
        },

        propertyStatusText: function propertyStatusText(value) {
            if (value === 'Stop') {
                return 'нет билетов';
            } else if (value === 'Available') {
                return 'есть';
            } else if (value === 'Request') {
                return 'по запросу';
            } else {
                return 'нет данных';
            }
        },

        formatPrice: function formatPrice(value) {
            if (value !== undefined) {
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
            }
        },

        //Цена со скидкой
        getDiscountPrice: function getDiscountPrice(value) {
            value = parseInt(value) - parseInt(value * 0.01);
            return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
        },

        //Старая цена
        getOldPrice: function getOldPrice(value) {
            value = parseInt(value) + parseInt(value * 0.11);
            return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
        },

        getCreditPrice: function getCreditPrice(value) {
            value = (parseInt(value) + parseInt(value * 0.0717)) / 6;
            return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
        }

    }

});

$(document).ready(function () {
    var myModal = new jBox('Modal', {
        onClose: function onClose() {
            $('.jBox-Modal').remove();
            $('.jBox-overlay').remove();
        },
        onOpen: function onOpen() {
            console.log("sdfsdf");
        }
    });
    new jBox('Modal', {
        width: "320",
        responsiveWidth: true,
        responsiveHeight: true,
        attach: '.fastOrderModalBtn',
        content: $('#fastOrderModalJbox'),
        closeButton: true,
        overlay: true,
        zIndex: 10001
    });
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGV0YWlsLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3dlYnBhY2svYm9vdHN0cmFwIDAzMTExN2JmZDM0MjViNDMyNTA2Iiwid2VicGFjazovLy9zcmMvZGV0YWlsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDE4KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCAwMzExMTdiZmQzNDI1YjQzMjUwNiIsIid1c2Ugc3RyaWN0JztcclxuXHJcbndpbmRvdy5kZXRhaWxhcHAgPSBuZXcgVnVlKHtcclxuICAgIGVsOiAnI2RldGFpbCcsXHJcbiAgICBkYXRhOiB7XHJcbiAgICAgICAgLyrQotC10LrRg9GJ0LjQtSDRgdCy0L7QudGB0YLQstCwKi9cclxuICAgICAgICB0b3VyaWQ6IGN1clBhcmFtcy50b3VyaWQsXHJcblxyXG5cclxuICAgICAgICBvYlRvdXJJbmZvOiB7XHJcbiAgICAgICAgICAgIG9wZXJhdG9yUGljOiBcIlwiXHJcbiAgICAgICAgfSxcclxuICAgICAgICBvYlJlc291cmNlczogW10sXHJcbiAgICAgICAgb2JGbGlnaHRzOiBbXSxcclxuICAgICAgICBvYkhvdGVsc0Rlc2NyOiB7fSxcclxuXHJcblxyXG4gICAgICAgIGlzU2hvd0Zhc3RPcmRlcjogZmFsc2UsXHJcbiAgICAgICAgZmFzdEZvcm1QaG9uZTogXCJcIixcclxuICAgICAgICBmYXN0Rm9ybU5hbWU6IFwiXCIsXHJcbiAgICAgICAgZmFzdEZvcm1OYW1lSGFzRXJyb3I6IGZhbHNlLFxyXG4gICAgICAgIGZhc3RGb3JtUGhvbmVIYXNFcnJvcjogZmFsc2UsXHJcbiAgICAgICAgZmFzdEZvcm1TZW5kZWQ6IGZhbHNlLFxyXG5cclxuICAgICAgICBjcmVkaXQ6IGZhbHNlLCAvL9Ci0YPRgCDQsiDRgNCw0YHRgdGA0L7Rh9C60YNcclxuICAgIH0sXHJcblxyXG4gICAgY3JlYXRlZDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMuaW5pdCgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgaW5pdDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBsZXQgc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICBzZWxmLiRodHRwLmdldCgnL2xvY2FsL21vZHVsZXMvbmV3dHJhdmVsLnNlYXJjaC9saWIvYWpheC5waHAnLCB7cGFyYW1zOiB7dHlwZTogXCJhY3R1YWxpemVcIiwgdG91cmlkOiBzZWxmLnRvdXJpZH19KS50aGVuKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5ib2R5LmRhdGEudG91ciAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5vYlRvdXJJbmZvID0gcmVzcG9uc2UuYm9keS5kYXRhLnRvdXI7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHNlbGYub2JUb3VySW5mby5vcGVyYXRvclBpYyA9ICdodHRwczovL3RvdXJ2aXNvci5ydS9waWNzL29wZXJhdG9ycy9zZWFyY2hsb2dvLycgKyBzZWxmLm9iVG91ckluZm8ub3BlcmF0b3Jjb2RlICsgJy5naWYnO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZi5vYlRvdXJJbmZvLmRldGFpbGF2YWlsYWJsZSA9PT0gXCIxXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi5kZXRhaWxBY3R1YWxpemUoKVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYubG9hZEhvdGVsKHNlbGYub2JUb3VySW5mby5ob3RlbGNvZGUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIDEwMDApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgfSxcclxuXHJcblxyXG4gICAgICAgIGRldGFpbEFjdHVhbGl6ZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB0aGlzLiRodHRwLmdldCgnL2xvY2FsL21vZHVsZXMvbmV3dHJhdmVsLnNlYXJjaC9saWIvYWpheC5waHAnLCB7cGFyYW1zOiB7dHlwZTogXCJhY3RkZXRhaWxcIiwgdG91cmlkOiB0aGlzLnRvdXJpZH19KS50aGVuKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICghcmVzcG9uc2UuYm9keS5pc2Vycm9yKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2cocmVzcG9uc2UuYm9keSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vYkZsaWdodHMgPSByZXNwb25zZS5ib2R5LmZsaWdodHM7XHJcblxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyB0aGlzLm9iUmVzb3VyY2VzID0gdGhpcy5vYlJlc291cmNlc1ByZXY7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PSDQntGI0LjQsdC60LAgJyArIHJlc3BvbnNlLmJvZHkuZXJyb3JtZXNzYWdlICsgJyA9PT09PT09PT09PT09PT09Jyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcblxyXG5cclxuICAgICAgICBmYXN0T3JkZXI6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdGhpcy5mYXN0Rm9ybVNlbmRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICBsZXQgc2Vzc2lkdmFsID0gJChcIiNzZXNzaWRcIikudmFsKCk7XHJcbiAgICAgICAgICAgIHRoaXMuZmFzdEZvcm1QaG9uZUhhc0Vycm9yID0gdGhpcy5mYXN0Rm9ybVBob25lLmxlbmd0aCA8PSAwO1xyXG4gICAgICAgICAgICB0aGlzLmZhc3RGb3JtTmFtZUhhc0Vycm9yID0gdGhpcy5mYXN0Rm9ybU5hbWUubGVuZ3RoIDw9IDA7XHJcblxyXG4gICAgICAgICAgICBpZiAoIXRoaXMuZmFzdEZvcm1QaG9uZUhhc0Vycm9yICYmICF0aGlzLmZhc3RGb3JtTmFtZUhhc0Vycm9yKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy4kaHR0cC5nZXQoJy9sb2NhbC9tb2R1bGVzL25ld3RyYXZlbC5zZWFyY2gvbGliL2FqYXgucGhwJywge1xyXG4gICAgICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBcImZhc3RvcmRlclwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b3VyaWQ6IHRoaXMudG91cmlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwaG9uZTogdGhpcy5mYXN0Rm9ybVBob25lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiB0aGlzLmZhc3RGb3JtTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3JlZGl0OiB0aGlzLmNyZWRpdCA/IFwiWVwiIDogXCJOXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlc3NpZDogc2Vzc2lkdmFsXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSkudGhlbihyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmJvZHkuRVJST1IgPT09IFwiXCIgJiYgcmVzcG9uc2UuYm9keS5TVEFUVVMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgeWFDb3VudGVyMjQzOTU5MTEucmVhY2hHb2FsKCdGQVNUX09SREVSX0RFVEFJTF9UT1VSJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZmFzdEZvcm1TZW5kZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgb3JkZXI6IGZ1bmN0aW9uIChkaXNjb3VudCA9IGZhbHNlLCAkZXZlbnQpIHtcclxuICAgICAgICAgICAgbGV0IG9yZGVyQnRuID0gJCgnLm9yZGVyLWJ0bicpLFxyXG4gICAgICAgICAgICAgICAgZGlzY291bk9yZGVyQnRuID0gJCgnLmRpc2NvdW50LW9yZGVyLWJ0bicpO1xyXG5cclxuICAgICAgICAgICAgbGV0IHBhcmFtcyA9IHtcclxuICAgICAgICAgICAgICAgIHRvdXJpZDogdGhpcy50b3VyaWQsXHJcbiAgICAgICAgICAgICAgICBidXk6IFwiWVwiLFxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgaWYgKGRpc2NvdW50KSB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbXMudXNlZGlzY291bnQgPSAnWSc7XHJcbiAgICAgICAgICAgICAgICBkaXNjb3VuT3JkZXJCdG4uYWRkQ2xhc3MoJ3J1bm5pbmcnKVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgb3JkZXJCdG4uYWRkQ2xhc3MoJ3J1bm5pbmcnKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy4kaHR0cC5nZXQoJy9sb2NhbC90b29scy9uZXd0cmF2ZWwuc2VhcmNoLmJ1eXRvdXIucGhwJywge3BhcmFtc30pXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmJvZHkgPT09ICdzdWNjZXNzJykge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0T3JkZXJQYWdlKCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZ2V0T3JkZXJQYWdlOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgbGV0IGVsID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ29yZGVyLWxpbmsnKTtcclxuICAgICAgICAgICAgICAgIGVsLmNsaWNrKCk7XHJcbiAgICAgICAgICAgICAgICAkKCcub3JkZXItYnRuJykucmVtb3ZlQ2xhc3MoJ3J1bm5pbmcnKTtcclxuICAgICAgICAgICAgICAgICQoJy5kaXNjb3VudC1vcmRlci1idG4nKS5yZW1vdmVDbGFzcygncnVubmluZycpO1xyXG4gICAgICAgICAgICB9LCAxMDAwKTtcclxuICAgICAgICB9LFxyXG5cclxuXHJcbiAgICAgICAgbG9hZEhvdGVsOiBmdW5jdGlvbiAoaG90ZWxjb2RlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuJGh0dHAuZ2V0KCcvbG9jYWwvbW9kdWxlcy9uZXd0cmF2ZWwuc2VhcmNoL2xpYi9hamF4LnBocCcsIHtwYXJhbXM6IHt0eXBlOiBcImZ1bGxob3RlbFwiLCBob3RlbGNvZGU6IGhvdGVsY29kZX19KS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmJvZHkuZGF0YS5ob3RlbCAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuZGVidWcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT0g0JjQvdGE0L7RgNC80LDRhtC40Y8g0L/QviDQvtGC0LXQu9GOID09PT09PT09Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub2JIb3RlbHNEZXNjciA9IHJlc3BvbnNlLmJvZHkuZGF0YS5ob3RlbDtcclxuXHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlYnVnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwi0J7RiNC40LHQutCwIGZ1bGxob3RlbFwiKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGdldFRvdXJJbmZvOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGxldCBzZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgIGxldCBodG1sID0gXCI8dWwgc3R5bGU9J2xpc3Qtc3R5bGUtdHlwZTogbm9uZSc+XCI7XHJcblxyXG4gICAgICAgICAgICBodG1sICs9IFwiPGxpPtCd0LDQt9Cy0LDQvdC40LUg0YLRg9GA0LA6IFwiICsgc2VsZi5vYlRvdXJJbmZvLnRvdXJuYW1lICsgXCI8L2xpPlwiO1xyXG4gICAgICAgICAgICBodG1sICs9IFwiPGxpPtCi0YPRgNC+0L/QtdGA0LDRgtC+0YA6IFwiICsgc2VsZi5vYlRvdXJJbmZvLm9wZXJhdG9ybmFtZSArIFwiPC9saT5cIjtcclxuICAgICAgICAgICAgaHRtbCArPSBcIjxsaT7QlNCw0YLQsCDQstGL0LvQtdGC0LA6IFwiICsgc2VsZi5vYlRvdXJJbmZvLmZseWRhdGUgKyBcIihcIiArIHRoaXMuJG9wdGlvbnMuZmlsdGVycy5kaXNwbGF5TmlnaHRzKHNlbGYub2JUb3VySW5mby5uaWdodHMpICsgXCIpIDwvbGk+XCI7XHJcbiAgICAgICAgICAgIGh0bWwgKz0gXCI8bGk+0JPQvtGA0L7QtCDQstGL0LvQtdGC0LA6IFwiICsgc2VsZi5vYlRvdXJJbmZvLmRlcGFydHVyZW5hbWUgKyBcIjwvbGk+XCI7XHJcbiAgICAgICAgICAgIGh0bWwgKz0gXCI8bGk+0KHRgtGA0LDQvdCwOiBcIiArIHNlbGYub2JUb3VySW5mby5jb3VudHJ5bmFtZSArIFwiPC9saT5cIjtcclxuICAgICAgICAgICAgaHRtbCArPSBcIjxsaT7QmtGD0YDQvtGA0YI6IFwiICsgc2VsZi5vYlRvdXJJbmZvLmhvdGVscmVnaW9ubmFtZSArIFwiPC9saT5cIjtcclxuICAgICAgICAgICAgaHRtbCArPSBcIjxsaT7QntGC0LXQu9GMOiBcIiArIHNlbGYub2JUb3VySW5mby5ob3RlbG5hbWUgKyBcIiBcIiArIHNlbGYub2JUb3VySW5mby5ob3RlbHN0YXJzICsgXCIqPC9saT5cIjtcclxuICAgICAgICAgICAgaHRtbCArPSBcIjxsaT7QktC30YDQvtGB0LvRi9GFOiBcIiArIHNlbGYub2JUb3VySW5mby5hZHVsdHMgKyBcIjwvbGk+XCI7XHJcbiAgICAgICAgICAgIGh0bWwgKz0gXCI8bGk+0JTQtdGC0LXQuTogXCIgKyBzZWxmLm9iVG91ckluZm8uY2hpbGQgKyBcIjwvbGk+XCI7XHJcbiAgICAgICAgICAgIGh0bWwgKz0gXCI8bGk+0J3QvtC80LXRgDogXCIgKyBzZWxmLm9iVG91ckluZm8ucm9vbSArIFwiPC9saT5cIjtcclxuICAgICAgICAgICAgaHRtbCArPSBcIjxsaT7Qn9C40YLQsNC90LjQtTogXCIgKyBzZWxmLm9iVG91ckluZm8ubWVhbCArIFwiPC9saT5cIjtcclxuICAgICAgICAgICAgaHRtbCArPSBcIjxsaT7QoNCw0LfQvNC10YnQtdC90LjQtTogXCIgKyBzZWxmLm9iVG91ckluZm8ucGxhY2VtZW50ICsgXCI8L2xpPlwiO1xyXG4gICAgICAgICAgICBodG1sICs9IFwiPGxpPtCh0YLQvtC40LzQvtGB0YLRjDogXCIgKyB0aGlzLiRvcHRpb25zLmZpbHRlcnMuZm9ybWF0UHJpY2Uoc2VsZi5vYlRvdXJJbmZvLnByaWNlKSArIFwiPC9saT5cIjtcclxuICAgICAgICAgICAgaHRtbCArPSBcIjxsaT7QodGC0L7QuNC80L7RgdGC0Ywg0YHQviDRgdC60LjQtNC60L7QuSDQt9CwINGB0LDQvNC+0YHRgtC+0Y/RgtC10LvRjNC90L7RgdGC0Yw6IDogXCIgKyB0aGlzLiRvcHRpb25zLmZpbHRlcnMuZ2V0RGlzY291bnRQcmljZShzZWxmLm9iVG91ckluZm8ucHJpY2UpICsgXCI8L2xpPlwiO1xyXG5cclxuICAgICAgICAgICAgaHRtbCArPSBcIjwvdWw+XCI7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gaHRtbDtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGZpbHRlcnM6IHtcclxuXHJcbiAgICAgICAgZGlzcGxheU5pZ2h0czogZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgICAgIGlmICghdmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBcIlwiO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgbGV0IHRpdGxlcyA9IFsn0L3QvtGH0YwnLCAn0L3QvtGH0LgnLCAn0L3QvtGH0LXQuSddO1xyXG4gICAgICAgICAgICAgICAgdmFsdWUgPSBwYXJzZUludCh2YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWUgKyBcIiBcIiArIHRpdGxlc1sodmFsdWUgJSAxMCA9PSAxICYmIHZhbHVlICUgMTAwICE9IDExID8gMCA6IHZhbHVlICUgMTAgPj0gMiAmJiB2YWx1ZSAlIDEwIDw9IDQgJiYgKHZhbHVlICUgMTAwIDwgMTAgfHwgdmFsdWUgJSAxMDAgPj0gMjApID8gMSA6IDIpXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHNob3J0UGVvcGxlczogZnVuY3Rpb24gKHZhbHVlLCB0eXBlKSB7XHJcbiAgICAgICAgICAgIGlmICh2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgbGV0IHRpdGxlcyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgaWYgKHR5cGUgPT0gJ2FkdWx0cycpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZXMgPSBbJ9Cy0LfRgNC+0YHQu9GL0LknLCAn0LLQt9GA0L7RgdC70YvRhScsICfQstC30YDQvtGB0LvRi9GFJ107XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGUgPT0gJ2tpZHMnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGVzID0gWyfRgNC10LHQtdC90L7QuicsICfRgNC10LHQtdC90LrQsCcsICfQtNC10YLQtdC5J107XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWUgKyBcIiBcIiArIHRpdGxlc1sodmFsdWUgJSAxMCA9PSAxICYmIHZhbHVlICUgMTAwICE9IDExID8gMCA6IHZhbHVlICUgMTAgPj0gMiAmJiB2YWx1ZSAlIDEwIDw9IDQgJiYgKHZhbHVlICUgMTAwIDwgMTAgfHwgdmFsdWUgJSAxMDAgPj0gMjApID8gMSA6IDIpXTtcclxuXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBwcm9wZXJ0eVN0YXR1c1RleHQ6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICBpZiAodmFsdWUgPT09ICdTdG9wJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICfQvdC10YIg0LHQuNC70LXRgtC+0LInO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHZhbHVlID09PSAnQXZhaWxhYmxlJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICfQtdGB0YLRjCc7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodmFsdWUgPT09ICdSZXF1ZXN0Jykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICfQv9C+INC30LDQv9GA0L7RgdGDJztcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAn0L3QtdGCINC00LDQvdC90YvRhSc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG5cclxuXHJcbiAgICAgICAgZm9ybWF0UHJpY2U6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICBpZiAodmFsdWUgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHZhbHVlLnRvU3RyaW5nKCkucmVwbGFjZSgvXFxCKD89KFxcZHszfSkrKD8hXFxkKSkvZywgXCIgXCIpICsgJyDQoCc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICAvL9Cm0LXQvdCwINGB0L4g0YHQutC40LTQutC+0LlcclxuICAgICAgICBnZXREaXNjb3VudFByaWNlOiBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgICAgdmFsdWUgPSBwYXJzZUludCh2YWx1ZSkgLSBwYXJzZUludCh2YWx1ZSAqIDAuMDEpO1xyXG4gICAgICAgICAgICByZXR1cm4gTWF0aC5yb3VuZCh2YWx1ZSkudG9TdHJpbmcoKS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIiBcIikgKyAnINCgJztcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICAvL9Ch0YLQsNGA0LDRjyDRhtC10L3QsFxyXG4gICAgICAgIGdldE9sZFByaWNlOiBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgICAgdmFsdWUgPSBwYXJzZUludCh2YWx1ZSkgKyBwYXJzZUludCh2YWx1ZSAqIDAuMTEpO1xyXG4gICAgICAgICAgICByZXR1cm4gTWF0aC5yb3VuZCh2YWx1ZSkudG9TdHJpbmcoKS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIiBcIikgKyAnINCgJztcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBnZXRDcmVkaXRQcmljZTogZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgICAgIHZhbHVlID0gKHBhcnNlSW50KHZhbHVlKSArIHBhcnNlSW50KHZhbHVlICogMC4wNzE3KSkgLyA2O1xyXG4gICAgICAgICAgICByZXR1cm4gTWF0aC5yb3VuZCh2YWx1ZSkudG9TdHJpbmcoKS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIiBcIikgKyAnINCgJztcclxuICAgICAgICB9LFxyXG5cclxuICAgIH1cclxuXHJcbn0pO1xyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xyXG4gICAgbGV0IG15TW9kYWwgPSBuZXcgakJveCgnTW9kYWwnLCB7XHJcbiAgICAgICAgb25DbG9zZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAkKCcuakJveC1Nb2RhbCcpLnJlbW92ZSgpO1xyXG4gICAgICAgICAgICAkKCcuakJveC1vdmVybGF5JykucmVtb3ZlKCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBvbk9wZW46IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJzZGZzZGZcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICBuZXcgakJveCgnTW9kYWwnLCB7XHJcbiAgICAgICAgd2lkdGg6IFwiMzIwXCIsXHJcbiAgICAgICAgcmVzcG9uc2l2ZVdpZHRoOiB0cnVlLFxyXG4gICAgICAgIHJlc3BvbnNpdmVIZWlnaHQ6IHRydWUsXHJcbiAgICAgICAgYXR0YWNoOiAnLmZhc3RPcmRlck1vZGFsQnRuJyxcclxuICAgICAgICBjb250ZW50OiAkKCcjZmFzdE9yZGVyTW9kYWxKYm94JyksXHJcbiAgICAgICAgY2xvc2VCdXR0b246IHRydWUsXHJcbiAgICAgICAgb3ZlcmxheTogdHJ1ZSxcclxuICAgICAgICB6SW5kZXg6IDEwMDAxXHJcbiAgICB9KTtcclxufSk7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9kZXRhaWwuanMiXSwibWFwcGluZ3MiOiI7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FDN0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFwQkE7QUFDQTtBQXNCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBREE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTNJQTtBQUNBO0FBNklBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE1REE7QUFDQTtBQTVLQTtBQUNBO0FBMk9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFVQTs7OztBIiwic291cmNlUm9vdCI6IiJ9