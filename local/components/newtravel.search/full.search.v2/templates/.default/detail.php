<?php
define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getQueryList()->toArray();

$cur_params = CUtil::PhpToJSObject($request);

$version = "2.6";

//ajax
if ( ! isset($_SERVER['HTTP_X_REQUESTED_WITH']) || empty($_SERVER['HTTP_X_REQUESTED_WITH'])
     || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest'
) {
	return;
}
?>
	<link href="/local/templates/promo_pages/assets/css/style.min.css?ver=<?=$version?>" rel="stylesheet" type="text/css">
	<link href="/local/components/newtravel.search/full.search.v2/templates/.default/css/detail.css?ver=<?=$version?>" rel="stylesheet" type="text/css">

	<script>curParams = <?=$cur_params?>;</script>

	<script src="/local/components/newtravel.search/full.search.v2/templates/.default/dist/detail.js?ver=<?=$version?>"></script>

	<div id="detail" class="tour-wrapper">
		<div class="row">

			<!--Информация по туру-->
			<div class="col-12 col-md-8">
				<div class="text-muted">
					{{obTourInfo.tourname}}
				</div>

				<h1 class="display-6 text-info">{{obTourInfo.departurename}} - {{obTourInfo.countryname}}, {{obTourInfo.hotelregionname}} </h1>

				<!--mobile show-->
				<div class="d-block d-md-none">
					<div class="text-center ">
						<img :src="obTourInfo.hotelpicturemedium" class="img-thumbnail img-fluid">
					</div>
					<div class="d-flex align-items-center justify-content-between mt-3 mb-3">
						<span>Цена <b>без скидки: </b></span>
						<span class="text-muted display-7"><s>{{obTourInfo.price | getOldPrice}}</s></span>
					</div>

					<div class="d-flex align-items-center justify-content-between mt-3 mb-3">
						<span>Цена <b>за тур: </b></span>
						<span class="text-success display-6">{{obTourInfo.price | formatPrice}}</span>
					</div>
					<div class="d-flex align-items-center justify-content-between mt-3">
						<span>Цена <b>в кредит: </b></span>
						<span class="text-danger display-6">{{obTourInfo.price | getCreditPrice}}</span>
					</div>
					<small class="d-block text-muted mb-3">(стоимость без первоначального взноса)</small>

					<a class="ld-ext-left btn btn-primary btn-lg btn-block order-btn" href="javascript:void(0)" @click="order">
						<span>Забронировать</span>
						<div class="ld ld-ring ld-spin"></div>
					</a>

					<a class="btn btn-outline-primary btn-lg btn-block fastOrderModalBtn" href="javascript:void(0)" @click="credit = false">Заказать в 1 клик</a>
					<a class="btn btn-outline-danger btn-lg btn-block fastOrderModalBtn" href="javascript:void(0)" @click="credit = true">Заказать в рассрочку</a>

				</div>


				<ul class="list-inline no-gutters">
					<li class="list-inline-item col-12 col-sm-auto mb-2 mt-2">
						<span class="badge badge-pill badge-light-green detail-tour-badge"><i class="fa fa-plane bg-success text-white"></i> Билеты туда</span>
					</li>
					<li class="list-inline-item col-12 col-sm-auto mb-2 mt-2">
						<span class="badge badge-pill badge-light-green detail-tour-badge"><i class="fa fa-plane bg-success text-white"></i> Билеты обратно</span>
					</li>
					<li class="list-inline-item col-12 col-sm-auto mb-2 mt-2">
						<span class="badge badge-pill badge-light-green detail-tour-badge"><i class="fa fa-hotel bg-success text-white"></i> Места в отеле</span>
					</li>
				</ul>

				<div class="tour-parameters">
					<dl class="row">
						<dt class="col-5">Туроператор:</dt>
						<dd class="col-7"><img :src="obTourInfo.operatorPic" alt=""> {{obTourInfo.operatorname}}</dd>

						<dt class="col-5">Дата:</dt>
						<dd class="col-7">{{obTourInfo.flydate}} ({{obTourInfo.nights | displayNights}})</dd>

						<dt class="col-5">Отель:</dt>
						<dd class="col-7">
						<span>
							{{obTourInfo.hotelname}}
							<span class="hotel-rating">{{obTourInfo.hotelstars}} *</span>
						</span>
						</dd>

						<dt class="col-5">Туристы:</dt>
						<dd class="col-7">{{obTourInfo.adults | shortPeoples('adults')}} <span v-if="obTourInfo.child > 0">, {{obTourInfo.child | shortPeoples('kids')}}</span></dd>

						<dt class="col-5">Номер:</dt>
						<dd class="col-7">{{obTourInfo.room}}</dd>

						<dt class="col-5">Размещение:</dt>
						<dd class="col-7">{{obTourInfo.placement}}</dd>

						<dt class="col-5">Питание:</dt>
						<dd class="col-7">{{obTourInfo.meal}}</dd>
					</dl>

					<div class="row mb-3 d-flex align-items-center text-center text-md-left book-yourself-wrap">
						<div class="col-12 col-md-9 book-yourself-text">
							Забронируй сейчас online <strong>самостоятельно</strong>, по цене
						</div>
						<div class="col-12 col-md-3 text-center">
							<a class="btn-link" data-toggle="modal" data-target="#torgModal" :data-tour-info="getTourInfo()" href="#"><span style="font-weight: bold; color: #582000">Поторгуемся?</span></a>
							<a class="ld-ext-left btn btn-orange discount-order-btn" href="javascript:void(0)" @click="order(true, $event)">
								<span>{{obTourInfo.price | getDiscountPrice}}</span>
								<div class="ld ld-ring ld-spin"></div>
							</a>
						</div>
					</div>
				</div>

				<div class="display-7 text-info">Что входит в стоимость тура?</div>
				<div class="tour-parameters">
					<dl class="row">
						<dt class="col-sm-3">Включено:</dt>
						<dd class="col-sm-9">Авиаперелет, Проживание в отеле, Питание, Медицинская страховка, Трансфер</dd>
					</dl>
				</div>

				<div class="display-7 text-info">Перелёт:</div>

				<div class="tour-parameters" v-if="obFlights.length > 0">
					<dl class="tour-parameters__item">
						<dt class="tour-parameters__name">Туда:</dt>
						<dd class="tour-parameters__value">
							<p v-for="flight in obFlights" v-if="flight.forward.length > 0">
								{{flight.dateforward}} - {{flight.forward[0].departure.port.name}}({{flight.forward[0].departure.port.id}}), {{flight.forward[0].departure.time}}
								<i class="fa fa-arrow-circle-right"></i>
								{{flight.forward[0].arrival.port.name}}({{flight.forward[0].arrival.port.id}}), {{flight.forward[0].arrival.time}}
							</p>
						</dd>
					</dl>
					<dl class="tour-parameters__item">
						<dt class="tour-parameters__name">Обратно:</dt>
						<dd class="tour-parameters__value">
							<p v-for="flight in obFlights" v-if="flight.backward.length > 0">
								{{flight.datebackward}} - {{flight.backward[0].departure.port.name}}({{flight.backward[0].departure.port.id}}), {{flight.backward[0].departure.time}}
								<i class="fa fa-arrow-circle-right"></i>
								{{flight.backward[0].arrival.port.name}}({{flight.backward[0].arrival.port.id}}), {{flight.backward[0].arrival.time}}
							</p>

						</dd>
					</dl>
				</div>
				<div v-else>
					<br>
					<div class="alert alert-warning">
						<strong>Внимание! Не удалось получить данные о перелете</strong> <br>
						Рейс не определился, цена может быть не актуальной. <br>
						Просим связаться с менеджером и уточнить стоимость, перед заказом данного тура
					</div>
				</div>

			</div>


			<!--desktop show-->
			<div class="col-12 col-md-4 d-none d-md-block">
				<div>
					<a class="tour-preview__link"><img :src="obTourInfo.hotelpicturemedium" class="img-thumbnail img-fluid"></a>
				</div>
				<div class="d-flex align-items-center justify-content-between mt-3">
					<span>Цена <b>без скидки: </b></span>
					<span class="text-muted display-7"><s>{{obTourInfo.price | getOldPrice}}</s></span>
				</div>
				<div class="d-flex align-items-center justify-content-between mt-3">
					<span>Цена <b>за тур: </b></span>
					<span class="text-success display-6">{{obTourInfo.price | formatPrice}}</span>
				</div>
				<div class="d-flex align-items-center justify-content-between mt-3">
					<span><b>В кредит: </b> на 6 мес.</span>
					<span class="text-danger display-6">{{obTourInfo.price | getCreditPrice}}</span><br>
				</div>
				<small class="text-muted">(стоимость без первоначального взноса)</small>

				<div class="mt-3">
					<a class="ld-ext-left btn btn-primary btn-lg btn-block order-btn" href="javascript:void(0)" @click="order(false, $event)">
						<span>Забронировать</span>
						<div class="ld ld-ring ld-spin"></div>
					</a>
					<a class="btn btn-outline-primary btn-lg btn-block fastOrderModalBtn" href="javascript:void(0)" @click="credit = false">Заказать в 1 клик</a>
					<a class="btn btn-outline-danger btn-lg btn-block fastOrderModalBtn" href="javascript:void(0)" @click="credit = true">Заказать в рассрочку</a>

				</div>

				<a class="d-none" href="/personal/order/make/" id="order-link" target="_blank">форма заказа</a>
			</div>

			<!--mobile show-->
			<div class="col-12 pb-4 d-block d-md-none">
				<a class="ld-ext-left btn btn-primary btn-lg btn-block order-btn" href="javascript:void(0)" @click="order">
					<span>Забронировать</span>
					<div class="ld ld-ring ld-spin"></div>
				</a>
				<a class="btn btn-outline-primary btn-lg btn-block fastOrderModalBtn">Заказать в 1 клик</a>
			</div>
		</div>

		<div id="fastOrderModalJbox" style="display: none;">
			<div class="row">
				<div class="col-md-12">
					<h3 class="text-primary display-8">Все очень просто! Закажите тур<span v-if="credit"> в рассрочку</span>, оставив только имя и номер телефона!</h3>
				</div>
			</div>

			<div class="row" :class="{'d-none' : !fastFormSended}">
				<div class="col-12">
					<div class="alert alert-success" role="alert">Ваш заказ успешно отправлен. В скором времени специалисты свяжутся с Вами.</div>
				</div>
			</div>

			<div class="row" :class="{'d-none' : fastFormSended}">
				<div class="col-12 mb-3">
					<div class="from-group">
						<input type="text" class="form-control name_input" :class="{'is-invalid' : fastFormNameHasError}" v-model="fastFormName" name="form_text_82" placeholder="Ваше имя" required/>
					</div>
				</div>
				<div class="col-12 mb-3">
					<div class="from-group">
						<input type="text" class="form-control phone phone_input" :class="{'is-invalid' : fastFormPhoneHasError}" v-model="fastFormPhone" name="form_text_71" placeholder="Номер телефона" required/>
					</div>
				</div>
				<div class="col-12">
					<div class="from-group">
						<?=bitrix_sessid_post()?>
						<button class="btn btn-success btn-block" @click="fastOrder">Заказать</button>
					</div>
				</div>
			</div>
		</div>
	</div>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");