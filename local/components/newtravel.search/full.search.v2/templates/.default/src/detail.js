'use strict';

window.detailapp = new Vue({
    el: '#detail',
    data: {
        /*Текущие свойства*/
        tourid: curParams.tourid,


        obTourInfo: {
            operatorPic: ""
        },
        obResources: [],
        obFlights: [],
        obHotelsDescr: {},


        isShowFastOrder: false,
        fastFormPhone: "",
        fastFormName: "",
        fastFormNameHasError: false,
        fastFormPhoneHasError: false,
        fastFormSended: false,

        credit: false, //Тур в рассрочку
    },

    created: function () {
        this.init();
    },

    methods: {
        init: function () {
            let self = this;

            self.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "actualize", tourid: self.tourid}}).then(response => {
                if (response.body.data.tour !== undefined) {
                    self.obTourInfo = response.body.data.tour;

                    self.obTourInfo.operatorPic = 'https://tourvisor.ru/pics/operators/searchlogo/' + self.obTourInfo.operatorcode + '.gif';

                    if (self.obTourInfo.detailavailable === "1") {
                        self.detailActualize()
                    }

                    setTimeout(function () {
                        self.loadHotel(self.obTourInfo.hotelcode);
                    }, 1000);
                }
            });

        },


        detailActualize: function () {
            this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "actdetail", tourid: this.tourid}}).then(response => {
                if (!response.body.iserror) {

                    //console.log(response.body);
                    this.obFlights = response.body.flights;

                } else {
                    // this.obResources = this.obResourcesPrev;
                    console.log('=============== Ошибка ' + response.body.errormessage + ' ================');
                }
            });
        },


        fastOrder: function () {
            this.fastFormSended = false;
            let sessidval = $("#sessid").val();
            this.fastFormPhoneHasError = this.fastFormPhone.length <= 0;
            this.fastFormNameHasError = this.fastFormName.length <= 0;

            if (!this.fastFormPhoneHasError && !this.fastFormNameHasError) {

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {
                    params: {
                        type: "fastorder",
                        tourid: this.tourid,
                        phone: this.fastFormPhone,
                        name: this.fastFormName,
                        credit: this.credit ? "Y" : "N",
                        sessid: sessidval
                    }
                }).then(response => {
                    if (response.body.ERROR === "" && response.body.STATUS) {
                        yaCounter24395911.reachGoal('FAST_ORDER_DETAIL_TOUR');
                        this.fastFormSended = true;
                    }
                });
            }
        },

        order: function (discount = false, $event) {
            let orderBtn = $('.order-btn'),
                discounOrderBtn = $('.discount-order-btn');

            let params = {
                tourid: this.tourid,
                buy: "Y",
            };

            if (discount) {
                params.usediscount = 'Y';
                discounOrderBtn.addClass('running')
            } else {
                orderBtn.addClass('running');
            }

            this.$http.get('/local/tools/newtravel.search.buytour.php', {params})
                .then(response => {
                    if (response.body === 'success') {

                    }
                });

            this.getOrderPage();
        },

        getOrderPage: function () {
            setTimeout(function () {
                let el = document.getElementById('order-link');
                el.click();
                $('.order-btn').removeClass('running');
                $('.discount-order-btn').removeClass('running');
            }, 1000);
        },


        loadHotel: function (hotelcode) {
            this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "fullhotel", hotelcode: hotelcode}}).then(function (response) {
                if (response.body.data.hotel !== undefined) {
                    if (this.debug) {
                        console.log('======= Информация по отелю ========');
                    }
                    this.obHotelsDescr = response.body.data.hotel;

                } else {
                    if (this.debug) {
                        console.log("Ошибка fullhotel");
                    }
                }
            });
        },

        getTourInfo: function () {
            let self = this;

            let html = "<ul style='list-style-type: none'>";

            html += "<li>Название тура: " + self.obTourInfo.tourname + "</li>";
            html += "<li>Туроператор: " + self.obTourInfo.operatorname + "</li>";
            html += "<li>Дата вылета: " + self.obTourInfo.flydate + "(" + this.$options.filters.displayNights(self.obTourInfo.nights) + ") </li>";
            html += "<li>Город вылета: " + self.obTourInfo.departurename + "</li>";
            html += "<li>Страна: " + self.obTourInfo.countryname + "</li>";
            html += "<li>Курорт: " + self.obTourInfo.hotelregionname + "</li>";
            html += "<li>Отель: " + self.obTourInfo.hotelname + " " + self.obTourInfo.hotelstars + "*</li>";
            html += "<li>Взрослых: " + self.obTourInfo.adults + "</li>";
            html += "<li>Детей: " + self.obTourInfo.child + "</li>";
            html += "<li>Номер: " + self.obTourInfo.room + "</li>";
            html += "<li>Питание: " + self.obTourInfo.meal + "</li>";
            html += "<li>Размещение: " + self.obTourInfo.placement + "</li>";
            html += "<li>Стоимость: " + this.$options.filters.formatPrice(self.obTourInfo.price) + "</li>";
            html += "<li>Стоимость со скидкой за самостоятельность: : " + this.$options.filters.getDiscountPrice(self.obTourInfo.price) + "</li>";

            html += "</ul>";

            return html;
        }
    },

    filters: {

        displayNights: function (value) {
            if (!value) {
                return "";
            } else {
                let titles = ['ночь', 'ночи', 'ночей'];
                value = parseInt(value);
                return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
            }
        },

        shortPeoples: function (value, type) {
            if (value) {
                let titles = [];
                if (type == 'adults') {
                    titles = ['взрослый', 'взрослых', 'взрослых'];
                } else if (type == 'kids') {
                    titles = ['ребенок', 'ребенка', 'детей'];
                }
                return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];

            }
        },

        propertyStatusText: function (value) {
            if (value === 'Stop') {
                return 'нет билетов';
            } else if (value === 'Available') {
                return 'есть';
            } else if (value === 'Request') {
                return 'по запросу';
            } else {
                return 'нет данных';
            }
        },


        formatPrice: function (value) {
            if (value !== undefined) {
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
            }
        },

        //Цена со скидкой
        getDiscountPrice: function (value) {
            value = parseInt(value) - parseInt(value * 0.01);
            return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
        },

        //Старая цена
        getOldPrice: function (value) {
            value = parseInt(value) + parseInt(value * 0.11);
            return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
        },

        getCreditPrice: function (value) {
            value = (parseInt(value) + parseInt(value * 0.0717)) / 6;
            return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
        },

    }

});

$(document).ready(function () {
    let myModal = new jBox('Modal', {
        onClose: function () {
            $('.jBox-Modal').remove();
            $('.jBox-overlay').remove();
        },
        onOpen: function () {
            console.log("sdfsdf");
        }
    });
    new jBox('Modal', {
        width: "320",
        responsiveWidth: true,
        responsiveHeight: true,
        attach: '.fastOrderModalBtn',
        content: $('#fastOrderModalJbox'),
        closeButton: true,
        overlay: true,
        zIndex: 10001
    });
});