'use strict';

// import Vue from 'vue'
// import VueResource from 'vue-resource'
// Vue.use(VueResource);
import 'v-calendar/lib/v-calendar.min.css';

import ComDeparture from './ComDeparture.vue'
import ComResults from './ComResults.vue'
import ComProgressBar from './ComProgressBar.vue'
import HotelDatePicker from 'vue-hotel-datepicker'


$(document).ready(function () {

    window.searchApp = new Vue({
        el: '#search',
        data: {
            /*Текущие свойства*/
            departure: "4", //Текущий город
            country: "4", //Текущая страна
            regions: "", //Выбранные регионы
            subregions: "", //Выбранные субрегионы
            meal: "", //Выбранные типы питания
            stars: "", //Выбранные категории отеля
            hotels: "", //Выбранные отели
            adults: 2, //Количество взрослых
            child: 0, //Количество детей
            childage1: "0", //Возраст ребенка 1
            childage2: "0", //Возраст ребенка 2
            childage3: "0", //Возраст ребенка 3
            nightsfrom: 6, //Количество ночей от
            nightsto: 14, //Количество ночей до
            pricefrom: "", //Цена от
            priceto: "", //Цена до
            datefrom: moment().add(1, 'days').format('DD.MM.YYYY'), //Дата вылета от
            dateto: moment().add(14, 'days').format('DD.MM.YYYY'), //Дата вылета до
            operators: "", //Список операторов
            requestid: "", //ID запроса
            pageNumber: 1, //Номер страницы
            rating: "", //Рейтинг отеля
            hideRegular: true, //Рейтинг отеля

            /*Объекты*/
            obDepartures: [],
            obCountries: [],
            obRegions: [],
            obRegionsDisplay: [],
            obSubRegions: [],
            obStars: [],
            obOperators: [],
            obFlydates: [],
            obHotels: [],
            obMeals: [],
            obSearchResults: [],

            /*Временные объекты*/
            obMealsSelected: [],
            obRegionsSelected: {},
            obSubRegionsSelected: {},
            obHotelsSelected: {},
            obHotelsDescr: {},
            obOperatorsSelected: [],
            obMealsName: [],
            obOperatorsName: [],

            /*Наименования*/
            countryName: "Турция",
            regionsName: "",
            subRegionsName: "",
            hotelsName: "",
            touristString: "2 взр.",

            /*служебные*/
            pop_countries: [4,1,15,5,6,20,14,47],
            toursNotFound: false,

            ruRU: {
                night: 'ночь',
                nights: 'ночей',
                'day-names': ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
                'check-in': 'С',
                'check-out': 'По',
                'month-names': ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            },

            hotelSearchQuery: "",
            regionSearchQuery: "",
            countrySearchQuery: "",

            dateStart: null,
            dateEnd: null,

            /*поиск*/
            searchString: "", //поисковая строка страны
            allOperatorsProcessed: true, //Все ТО обработали запрос
            allRowsCount: 0, //Общее количество результатов
            firstRowsIsDisplayed: false, //Первая партия результатов загружена
            minPrice: 0, //Минимальная найденная цена
            searchProgress: 0, //Прогрес поиска
            searchInAction: false, //Флаг того, что поиск стартовал
            nextPageRequestInAction: false, //Флаг того, что идет запрос на следующую страницу туров
            toursIsOver: false, //Флаг того, что больше туров нет, показаны все страницы

            /*FastOrder*/
            isShowFastOrder: false,
            fastFormPhone: "",
            fastFormName: "",
            fastFormNameHasError: false,
            fastFormPhoneHasError: false,
            fastFormSended: false,


            debug: false,
            queryParams: [],
            searchUrl: "",
            mobile: false,
            showAdditionalParams: false,

        },

        created: function () {
            let _this = this;

            this.init();
            this.queryParams = getAllUrlParams(window.location.toString());

            //Автостарт поиска
            if (_this.queryParams.start === 'y') {
                setTimeout(function () {
                    _this.startSearch();
                }, 2000)
            }

            //Заполняем параметры из request
            if (this.queryParams.datefrom !== undefined) {
                this.datefrom = this.queryParams.datefrom;
            }
            if (this.queryParams.dateto !== undefined) {
                this.dateto = this.queryParams.dateto;
            }
            if (this.queryParams.nightsfrom !== undefined) {
                this.nightsfrom = this.queryParams.nightsfrom;
            }
            if (this.queryParams.nightsto !== undefined) {
                this.nightsto = this.queryParams.nightsto;
            }
            if (this.queryParams.pricefrom !== undefined) {
                this.pricefrom = this.queryParams.pricefrom;
            }
            if (this.queryParams.priceto !== undefined) {
                this.priceto = this.queryParams.priceto;
            }
            if (this.queryParams.rating !== undefined) {
                this.rating = this.queryParams.rating;
            }

            if (this.queryParams.adults !== undefined) {
                this.adults = this.queryParams.adults;
            }
            if (this.queryParams.child !== undefined) {
                this.child = this.queryParams.child;
                if (this.queryParams.childage1 !== undefined) {
                    this.childage1 = this.queryParams.childage1;
                }
                if (this.queryParams.childage2 !== undefined) {
                    this.childage2 = this.queryParams.childage2;
                }
                if (this.queryParams.childage3 !== undefined) {
                    this.childage3 = this.queryParams.childage3;
                }
            }
            if (this.queryParams.hideregular !== undefined) {
                this.hideRegular = this.queryParams.hideregular === 1;
            }

            this.dateStart = moment(this.datefrom, 'DD.MM.YYYY')._d;
            this.dateEnd = moment(this.dateto, 'DD.MM.YYYY')._d;

            // dunno why v-on="resize: func" not working
            if (document.body.clientWidth <= 700) {
                _this.$set(_this, 'mobile', true);
            } else {
                _this.$set(_this, 'mobile', false);
            }
            global.window.addEventListener('resize', function () {
                if (document.body.clientWidth <= 700) {
                    _this.$set(_this, 'mobile', true);
                } else {
                    _this.$set(_this, 'mobile', false);
                }
            });
        },

        methods: {
            init: function () {
                let _this = this;

                _this.getDepartures();
                _this.getCountries();
                _this.getRegions();
                _this.getOperators();
                _this.getStars();
                _this.getFlydates();
                _this.getHotels();
                _this.getMeals();
            },

            getDepartures: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "departure"}}).then(response => {
                        this.obDepartures = response.body.lists.departures.departure;
                        this.obDepartures = _.sortBy(this.obDepartures, ['name']);

                        //Заполняем параметры из request
                        if (this.queryParams.departure !== undefined) {
                            this.departure = this.queryParams.departure;
                            this.cityFromName = _.find(this.obDepartures, ['id', this.queryParams.departure.toString()]) ? _.find(this.obDepartures, ['id', this.queryParams.departure.toString()]).namefrom : "Уфы";
                        } else if (BX.getCookie('NEWTRAVEL_USER_CITY') !== undefined) {
                            this.departure = BX.getCookie('NEWTRAVEL_USER_CITY');
                            this.cityFromName = _.find(this.obDepartures, ['id', BX.getCookie('NEWTRAVEL_USER_CITY').toString()]) ? _.find(this.obDepartures, ['id', BX.getCookie('NEWTRAVEL_USER_CITY').toString()]).namefrom : "Уфы";
                        }

                        //Подтвреждение города
                        // $('.departure_wrap').confirmCity({
                        //     departureModal: $('.cityFrom'),
                        //     departureId: this.departure
                        // });
                    }
                );
            },

            getCountries: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "country", cndep: this.departure}}).then(response => {
                    if (response.body.lists.countries.country !== null) {
                        this.obCountries = response.body.lists.countries.country;
                        this.obCountries = _.sortBy(this.obCountries, ['name']);

                        //Заполняем параметры из request
                        if (this.queryParams.country !== undefined) {
                            this.country = this.queryParams.country;
                            this.countryName = _.find(this.obCountries, ['id', this.queryParams.country.toString()]) ? _.find(this.obCountries, ['id', this.queryParams.country.toString()]).name : "Турция";
                        }
                    }

                });
            },

            getRegions: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "region", regcountry: this.country}}).then(response => {
                    let _this = this;

                    _this.obRegions = response.body.lists.regions.region;

                    //Заполняем параметры из request
                    if (_this.queryParams.regions !== undefined) {
                        let query = this.arrayConvert(_this.queryParams.regions);
                        _this.regions = query.join();
                        _.forEach(query, function (value, key) {
                            if (!_.find(_this.obRegions, ['id', value])) {
                                return;
                            }
                            _this.obRegionsSelected[value] = _.find(_this.obRegions, ['id', value]) ? _.find(_this.obRegions, ['id', value]) : "";
                        });
                        _this.regionsSelect();
                    }

                    _this.getSubRegions();

                });
            },

            getSubRegions: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "subregion", regcountry: this.country}}).then(response => {
                    let _this = this;

                    setTimeout(function () {
                        _this.obRegionsDisplay = _this.obRegions;

                        _this.obSubRegions = response.body.lists.subregions.subregion;

                        _.forEach(_this.obSubRegions, function (value, key) {
                            //Ищем индекс родительского региона в массиве obRegions
                            let finded = _.findIndex(_this.obRegions, function (o) {
                                return o.id === value.parentregion;
                            });

                            //Если у данного родительского региона нет субрегионов, тогда добавляем ему объект subregions
                            if (_.isNil(_this.obRegions[parseInt(finded)].subregions)) {
                                _this.obRegions[parseInt(finded)].subregions = {};
                            }

                            //Заполняем объект subregions
                            _this.$set(_this.obRegions[parseInt(finded)].subregions, key, value);

                            _this.obRegionsDisplay = _this.obRegions;
                        });

                        //Заполняем параметры из request
                        if (_this.queryParams.subregions !== undefined) {
                            let query = this.arrayConvert(_this.queryParams.subregions);
                            _this.subregions = query.join();
                            _.forEach(query, function (value, key) {
                                if (!_.find(_this.obSubRegions, ['id', value])) {
                                    return;
                                }
                                _this.obSubRegionsSelected[value] = _.find(_this.obSubRegions, ['id', value]) ? _.find(_this.obSubRegions, ['id', value]) : "";
                            });
                            _this.subRegionsSelect();
                        }
                    }, 500);

                });
            },

            getStars: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "stars"}}).then(response => {
                    let _this = this;
                    this.obStars = response.body.lists.stars.star;

                    //Заполняем параметры из request
                    if (_this.queryParams.stars !== undefined) {
                        _this.stars = _this.queryParams.stars
                    }

                });
            },

            getOperators: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "operator"}}).then(response => {
                    let _this = this;
                    this.obOperators = response.body.lists.operators.operator;

                    //Заполняем параметры из request
                    if (_this.queryParams.operators !== undefined) {
                        let query = this.arrayConvert(_this.queryParams.operators);
                        _this.operators = query.join();
                        _.forEach(query, function (value, key) {
                            _this.operatorSelect(_.find(_this.obOperators, ['id', value]) ? _.find(_this.obOperators, ['id', value]) : "");
                        });
                    }
                });
            },

            getFlydates: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "flydate", flydeparture: this.departure, flycountry: this.country}}).then(response => {
                    if (response.body.lists.flydates !== null) {
                        this.obFlydates = response.body.lists.flydates.flydate;
                        this.datepickerAttrs[0].dates = this.obFlydates;

                    } else {
                        this.datepickerAttrs[0].dates = [];
                    }
                });
            },

            checkInChange: function (date) {
                console.log(date);
                this.datefrom = moment(date).format('DD.MM.YYYY');
            },
            checkOutChange: function (date) {
                this.dateto = moment(date).format('DD.MM.YYYY')
            },

            getHotels: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "hotel", hotcountry: this.country, hotregion: this.regions, hotstars: this.stars, hotrating: this.rating}}).then(response => {
                    let _this = this;
                    _this.obHotels = response.body.lists.hotels.hotel;

                    //Заполняем параметры из request
                    if (_this.queryParams.hotels !== undefined) {
                        let query = this.arrayConvert(_this.queryParams.hotels);

                        _this.hotels = query.join();
                        _.forEach(query, function (value, key) {
                            if (!_.find(_this.obHotels, ['id', value])) {
                                return;
                            }
                            _this.obHotelsSelected[value] = _.find(_this.obHotels, ['id', value]) ? _.find(_this.obHotels, ['id', value]) : "";
                        });
                        _this.hotelsSelect();
                    }
                });
            },

            getMeals: function () {
                let _this = this;
                _this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "meal"}}).then(response => {
                    _this.obMeals = response.body.lists.meals.meal;

                    //Заполняем параметры из request
                    if (_this.queryParams.meal !== undefined) {
                        let query = this.arrayConvert(_this.queryParams.regions);
                        _this.meal = query.join();
                        _.forEach(query, function (value, key) {
                            _this.mealsSelect(_.find(_this.obMeals, ['id', value]) ? _.find(_this.obMeals, ['id', value]) : "");
                        });
                    }
                });
            },


            /*ПОИСКОВЫЕ МЕТОДЫ*/

            //Начало поиска
            startSearch: function () {
                let $this = this;
                if (this.debug) {
                    console.log('============= Начинаем поиск =============');
                }

                this.searchInAction = true;
                this.obSearchResults = [];
                this.requestid = '';
                this.pageNumber = 1;
                this.firstRowsIsDisplayed = false;
                this.allRowsCount = 0;
                this.searchProgress = 0;
                this.queryParams = [];
                this.toursNotFound = false;

                let params = {
                    type: "search",
                    departure: this.departure,
                    country: this.country,
                    regions: this.regions,
                    subregions: this.subregions,
                    meal: this.meal,
                    stars: this.stars,
                    hotels: this.hotels,
                    adults: this.adults,
                    child: this.child,
                    childage1: this.childage1,
                    childage2: this.childage2,
                    childage3: this.childage3,
                    nightsfrom: this.nightsfrom,
                    nightsto: this.nightsto,
                    pricefrom: this.pricefrom,
                    priceto: this.priceto,
                    datefrom: this.datefrom,
                    dateto: this.dateto,
                    operators: this.operators,
                    rating: this.rating,
                    hideregular: this.hideRegular ? 1 : 0,
                };

                $("html, body").animate({scrollTop: $('#results').offset().top - 100}, 1000);

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: params}).then(response => {
                    if (response.body.result.requestid !== undefined) {
                        this.requestid = response.body.result.requestid;
                        this.searchUrl = response.body.url;
                        history.pushState(this.requestid, "Поиск туров", "?start=Y&" + response.body.url);

                        setTimeout(function () {
                            $this.getStatus();
                        }, 3000);
                    } else {
                        if (this.debug) {
                            console.log("Ошибка startSearch");
                        }
                    }
                });
            },

            //Запрос состояние поиска
            getStatus: function () {
                if (this.debug) {
                    console.log('=========== Проверяем статус ==========');
                }
                let $this = this;
                let RowsCount = 0; //Общее количество результатов для цикла
                $this.getReaueIteration = 1;

                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "status", requestid: this.requestid, operatorstatus: 1}}).then(response => {

                    if (response.body.data.status !== undefined) { //Запрос прошел без ошибок

                        $this.searchProgress = response.body.data.status.progress; //Процент выполнения

                        RowsCount = response.body.data.status.hotelsfound;

                        if ($this.allRowsCount !== RowsCount && !$this.firstRowsIsDisplayed) { //Если количество найденных результатов за цикл не равна уже найденным результатам и первая партия еще не отображена, тогда выводим первую партию результатов
                            $this.firstRowsIsDisplayed = true;
                            if (this.debug) {
                                console.log("==Выводим первые результаты на экран==");
                            }
                            $this.getResult();
                        }

                        if (parseInt($this.searchProgress) !== 100) { //Повторяем проверку статуса
                            setTimeout(function () {
                                if (this.debug) {
                                    console.log("Не все операторы отработали, повторяем запрос статусов");
                                }
                                $this.getStatus();
                            }, 2000);
                        } else {
                            if (this.debug) {
                                console.log("==Все обработно! Урра!!!==");
                                console.log("==Выводим все результаты на экран==");
                            }
                            $this.searchProgress = 100;
                            $this.searchInAction = false;
                            $this.getResult();
                        }

                        $this.allRowsCount = response.body.data.status.hotelsfound; //Общее количество результатов
                    } else {
                        if (this.debug) {
                            console.log("Ошибка getStatus");
                        }
                    }
                });
            },

            //Получение результатов поиска
            getResult: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "result", requestid: this.requestid, page: this.pageNumber}}).then(response => {
                    if (response.body.data.status.state === 'finished' && parseInt(response.body.data.status.toursfound) === 0) {
                        this.toursNotFound = true;
                    }

                    if (response.body.data.result !== undefined) {
                        if (this.debug) {
                            console.log('======= Получите результаты ========');
                        }

                        if (this.pageNumber === 1) {
                            this.obSearchResults = response.body.data.result.hotel;
                        } else {
                            this.obSearchResults = _.concat(this.obSearchResults, response.body.data.result.hotel);
                            this.nextPageRequestInAction = false;
                        }

                        setTimeout(function () {
                            $('[data-toggle="popover"]').popover({
                                trigger: 'hover | click',
                                container: $('body')
                            });
                        }, 500)

                    } else {
                        if (this.pageNumber > 1) {
                            this.nextPageRequestInAction = false;
                            this.toursIsOver = true;
                        }
                        if (this.debug) {
                            console.log("Ошибка getResult");
                        }
                    }
                });
            },

            loadMoreTours: function () {
                this.nextPageRequestInAction = true;
                this.pageNumber = this.pageNumber + 1;
                this.getResult();
            },

            /*Доп. методы*/
            /**
             * @return {string}
             */
            FlagClass: function (index) {
                return "values__image flag-ui_narrowtpl_flags_30x20_" + index;
            },


            /**
             * Фильтр популярных стран
             * @return {boolean}
             */
            IsPopularCountry: function (country) {
                return _.includes(this.pop_countries, parseInt(country));
            },

            regionsSelect: function (region = "") {
                let self = this;

                if (region !== "") {
                    !_.find(self.obRegionsSelected, ['id', region.id]) ? self.obRegionsSelected[region.id.toString()] = {id: region.id, name: region.name} : delete self.obRegionsSelected[region.id.toString()];

                    //Очищаем все субрегионы при выборе родительского региона
                    let finded = _.findIndex(self.obRegionsDisplay, function (o) {
                        return o.id === region.id;
                    });
                    if (!_.isNil(self.obRegionsDisplay[finded].subregions)) {
                        _.forEach(self.obRegionsDisplay[finded].subregions, function (value, key) {
                            delete self.obSubRegionsSelected[value.id]
                        });
                        self.subRegionsSelect();
                    }
                }
                self.regions = _.map(self.obRegionsSelected, 'id').join();
                self.regionsName = _.map(self.obRegionsSelected, 'name');
            },

            subRegionsSelect: function (subregion = "") {

                //Очищаем родительский регион если выбран субрегион
                if (this.obRegionsSelected[subregion.parentregion] !== undefined) {
                    delete this.obRegionsSelected[subregion.parentregion];
                    this.regionsSelect();
                }

                if (subregion !== "") {
                    !_.find(this.obSubRegionsSelected, ['id', subregion.id]) ? this.obSubRegionsSelected[subregion.id] = {id: subregion.id, name: subregion.name} : delete this.obSubRegionsSelected[subregion.id];
                }
                this.subregions = _.map(this.obSubRegionsSelected, 'id').join();
                this.subRegionsName = _.map(this.obSubRegionsSelected, 'name');
            },

            hotelsSelect: function (hotel = "") {
                if (hotel !== "") {
                    !_.find(this.obHotelsSelected, ['id', hotel.id]) ? this.obHotelsSelected[hotel.id] = {id: hotel.id, name: hotel.name} : delete this.obHotelsSelected[hotel.id];
                }
                this.hotels = _.map(this.obHotelsSelected, 'id').join();
                this.hotelsName = _.map(this.obHotelsSelected, 'name');
            },

            //Питание для запроса
            mealsSelect: function (meal = "") {
                if (meal !== "") {
                    !_.includes(this.obMealsSelected, meal.id) ? this.obMealsSelected.push(meal.id) : _.pull(this.obMealsSelected, meal.id);
                    !_.includes(this.obMealsName, meal.name) ? this.obMealsName.push(meal.name) : _.pull(this.obMealsName, meal.name);
                }
                this.meal = this.obMealsSelected.join();
            },

            //Туроператоры для запроса
            operatorSelect: function (operator) {
                !_.includes(this.obOperatorsSelected, operator.id) ? this.obOperatorsSelected.push(operator.id) : _.pull(this.obOperatorsSelected, operator.id);
                !_.includes(this.obOperatorsName, operator.name) ? this.obOperatorsName.push(operator.name) : _.pull(this.obOperatorsName, operator.name);
                this.operators = this.obOperatorsSelected.join();
            },

            /*Формировние строки с туристами*/
            setTouristsString: function () {
                let self = this;

                self.touristString = self.adults + " взр. ";
                if (self.child > 0) {
                    self.touristString += self.child + " реб.(";
                    for (let i = 1; i <= self.child; i++) {
                        self.touristString += self['childage' + i];
                        if (i < self.child) {
                            self.touristString += ' и '
                        }
                    }
                    self.touristString += ")";
                }
            },

            fastOrder: function () {
                this.fastFormSended = false;
                let sessidval = $("#sessid").val(),
                    tourid = $("input[name=fastTourId]").val(),
                    credit = $("input[name=credit]").val();

                this.fastFormPhoneHasError = this.fastFormPhone.length <= 0;
                this.fastFormNameHasError = this.fastFormName.length <= 0;

                if (!this.fastFormPhoneHasError && !this.fastFormNameHasError && tourid.length > 0) {

                    this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {
                        params: {
                            type: "fastorder",
                            tourid: tourid,
                            phone: this.fastFormPhone,
                            name: this.fastFormName,
                            sessid: sessidval,
                            credit: credit
                        }
                    }).then(response => {
                        if (response.body.ERROR === "" && response.body.STATUS) {
                            yaCounter24395911.reachGoal('FAST_ORDER_DETAIL_TOUR');
                            this.fastFormSended = true;

                            setTimeout(function () {
                                $('#fastOrderModal').modal('hide');
                            }, 1000)

                        }
                    });
                }
            },

            //Комвертируем слова в массив
            arrayConvert: function (query) {
                let arQuery;

                if (_.isString(query)) {
                    arQuery = [query];
                } else {
                    arQuery = query;
                }
                return arQuery;
            },

            closeDropdown: function (target) {
                $('#'+target).dropdown('toggle');
            }

        },

        watch: {
            departure: function () {
                this.getCountries();
                this.getRegions();
                this.getHotels();
                this.getFlydates();

                BX.setCookie('NEWTRAVEL_USER_CITY', this.departure, {path: '/'});
            },
            country: function (newCountry) {
                this.getRegions();
                this.getHotels();
                this.getFlydates();
                this.obRegionsSelected = {};
                this.regionsName = "";
                this.regions = "";
                this.obSubRegionsSelected = {};
                this.subregions = "";
                this.subRegionsName = "";
                this.obHotelsSelected = {};
                this.hotelsName = "";
                this.hotels = "";
            },
            regions: function () {
                this.getHotels();
            },
            stars: function () {
                this.getHotels();
            },
            rating: function () {
                this.getHotels();
            },
            adults: function () {
                this.setTouristsString();
            },
            child: function () {
                this.setTouristsString();
            },
            childage1: function () {
                this.setTouristsString();
            },
            childage2: function () {
                this.setTouristsString();
            },
            childage3: function () {
                this.setTouristsString();
            },
        },

        computed: {
            // groupedDepartures: function () {
            //     return _.groupBy(this.obDepartures, function (s) {
            //         return s.name.charAt();
            //     });
            // },
            groupedCountries: function () {
                return _.groupBy(this.obCountries, function (s) {
                    return s.name.charAt();
                });
            },
            filteredHotels: function () {
                let self = this,
                    filtered = [];

                if (self.obHotels !== null) {
                    filtered = self.obHotels.filter(function (hotel) {
                        return hotel.name.toLowerCase().indexOf(self.hotelSearchQuery.toLowerCase()) !== -1
                    });
                }

                return filtered.slice(0, 300);
            },
            filteredRegions: function () {
                let self = this;
                return self.obRegionsDisplay.filter(function (region) {
                    return region.name.toLowerCase().indexOf(self.regionSearchQuery.toLowerCase()) !== -1
                })
            },
            filteredCountries: function () {
                let self = this;
                return self.obCountries.filter(function (country) {
                    return country.name.toLowerCase().indexOf(self.countrySearchQuery.toLowerCase()) !== -1
                })
            }
        },

        filters: {
            displayAge: function (value) {
                if (!value) {
                    return "";
                }else if(parseInt(value) === 1){
                    return "< 2 лет";
                }
                else {
                    let titles = ['год', 'года', 'лет'];
                    value = parseInt(value);
                    return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                }
            },

            displayAgeInt: function (value) {
                if (parseInt(value) >= 2 ) {
                    return value;
                }else if(parseInt(value) <= 1){
                    return "1";
                }
            },

            displayNights: function (value) {
                if (!value) {
                    return "";
                } else {
                    let titles = ['ночь', 'ночи', 'ночей'];
                    value = parseInt(value);
                    return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                }
            },

            shortName: function (value, type) {
                if (value) {
                    if (value.length > 1) {
                        let titles = [];
                        if (type == 'region') {
                            titles = ['курорт', 'курорта', 'курортов'];
                        } else if (type == 'hotel') {
                            titles = ['отель', 'отеля', 'отелей'];
                        } else if (type == 'subregion') {
                            titles = ['поселок', 'поселка', 'поселков'];
                        }

                        value = value.length;
                        return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];

                    } else if (value.length == 1) {
                        return value.toString();
                    }
                }
            },

            ratingDisplayed: function (value) {
                let rating = 'Любой';
                switch (value) {
                    case 5:
                        rating = 'от ' + 4.5;
                        break;
                    case 4:
                        rating = 'от ' + 4;
                        break;
                    case 3:
                        rating = 'от ' + 3.5;
                        break;
                    case 2:
                        rating = 'от ' + 3;
                        break;
                    case 0:
                        rating = 'Любой';
                        break;
                }
                return rating;
            },

            formatPrice: function (value) {
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
            }

        },

        components: {
            ComResults,
            ComProgressBar,
            ComDeparture,
            HotelDatePicker
        }

    });

    $('.select-dropdown.no-close').click(function (event) {
        event.stopPropagation();
    });

});


function getAllUrlParams(url) {

    // извлекаем строку из URL или объекта window
    let queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // объект для хранения параметров
    let obj = {};

    // если есть строка запроса
    if (queryString) {

        // данные после знака # будут опущены
        queryString = queryString.split('#')[0];

        // разделяем параметры
        let arr = queryString.split('&');

        for (let i = 0; i < arr.length; i++) {
            // разделяем параметр на ключ => значение
            let a = arr[i].split('=');

            // обработка данных вида: list[]=thing1&list[]=thing2
            let paramNum = undefined;
            let paramName = a[0].replace(/\[\d*\]/, function (v) {
                paramNum = v.slice(1, -1);
                return '';
            });

            // передача значения параметра ('true' если значение не задано)
            let paramValue = typeof(a[1]) === 'undefined' ? true : a[1];

            // преобразование регистра
            paramName = paramName.toLowerCase();
            paramValue = paramValue.toLowerCase();

            // если ключ параметра уже задан
            if (obj[paramName]) {
                // преобразуем текущее значение в массив
                if (typeof obj[paramName] === 'string') {
                    obj[paramName] = [obj[paramName]];
                }
                // если не задан индекс...
                if (typeof paramNum === 'undefined') {
                    // помещаем значение в конец массива
                    obj[paramName].push(paramValue);
                }
                // если индекс задан...
                else {
                    // размещаем элемент по заданному индексу
                    obj[paramName][paramNum] = paramValue;
                }
            }
            // если параметр не задан, делаем это вручную
            else {
                obj[paramName] = paramValue;
            }
        }
    }

    return obj;
}