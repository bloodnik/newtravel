<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"CITY" => Array(
			"PARENT" => "BASE",
			"NAME" => "ID Города вылета",
			"TYPE" => "STRING",
			"DEFAULT" => "4",
		),
		"COUNTRY" => Array(
			"PARENT" => "BASE",
			"NAME" => "ID Страны назначения",
			"TYPE" => "STRING",
			"DEFAULT" => "4",
		),
		"NIGHTSFROM" => Array(
			"PARENT" => "BASE",
			"NAME" => "Ночей от",
			"TYPE" => "STRING",
			"DEFAULT" => 6,
		),
		"NIGHTSTO" => Array(
			"PARENT" => "BASE",
			"NAME" => "Ночей до",
			"TYPE" => "STRING",
			"DEFAULT" => 10,
		),
		"ADULTS" => Array(
			"PARENT" => "BASE",
			"NAME" => "Взрослых",
			"TYPE" => "STRING",
			"DEFAULT" => 2,
		),
		"STARS" => Array(
			"PARENT" => "BASE",
			"NAME" => "Категория отеля(звездность)",
			"TYPE" => "STRING",
			"DEFAULT" => 3,
		),
		"DATE_TO" => Array(
			"PARENT" => "BASE",
			"NAME" => "Дата до(+сколько дней)",
			"TYPE" => "STRING",
			"DEFAULT" => 10,
		),
		"USE_GEO_DEFINE" => Array(
			"PARENT" => "BASE",
			"NAME" => "Подставлять город по IP",
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		"USE_COOKIE_DEPARTURE" => Array(
			"PARENT" => "BASE",
			"NAME" => "Использовать город из Куки",
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),

		"CACHE_TIME"  =>  array("DEFAULT"=>36000000)
	),
);