<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Newtravel\Search\TourvisorHlLists;
use Newtravel\Search\DefineGeo;
use Bitrix\Main\Loader;
use Bitrix\Main\Application;

class CFullSearch extends CBitrixComponent {


	private function prepareParams() {
		global $APPLICATION;
		$request = Application::getInstance()->getContext()->getRequest();

		//Проверка дат
		$datefrom = $request->get('datefrom');
		$datefrom = isset($datefrom) && strtotime($request->get('datefrom')) < strtotime(date("d.m.Y", strtotime("+1 day"))) ? date("d.m.Y", strtotime("+1 day")) : $datefrom;
		$dateto   = $request->get('dateto');
		$dateto   = isset($dateto) && strtotime($request->get('dateto')) < strtotime(date("d.m.Y", strtotime("+1 day"))) ? date("d.m.Y", strtotime($datefrom . " +" . $this->arParams['DATE_TO'] . " day")) : $dateto;

		//Если существует кука с городом, подставляем ее
		if ($this->arParams['USE_COOKIE_DEPARTURE'] == "Y") {
			$USER_CITY = $APPLICATION->get_cookie('USER_CITY');
			if (isset($USER_CITY) && ! empty($USER_CITY)) {
				$this->arParams['CITY'] = $USER_CITY;
			}
		}

		$request_data               = array();
		$request_data['DEPARTURE']  = $request->get('departure') ?: $this->arParams['CITY'];
		$request_data['COUNTRY']    = $request->get('country') ?: $this->arParams['COUNTRY'];
		$request_data['DATEFROM']   = $datefrom ? $datefrom : date("d.m.Y", strtotime("+1 day"));
		$request_data['DATETO']     = $dateto ? $dateto : date("d.m.Y", strtotime($request_data['DATEFROM'] . " +" . $this->arParams['DATE_TO'] . " day"));
		$request_data['DATERANGE']  = $request_data['DATEFROM'] . " - " . $request_data['DATETO'];
		$request_data['NIGHTSFROM'] = $request->get('nightsfrom') ?: $this->arParams['NIGHTSFROM'];
		$request_data['NIGHTSTO']   = $request->get('nightsto') ?: $this->arParams['NIGHTSTO'];
		$request_data['STARS']      = $request->get('stars') ?: $this->arParams['STARS'];
		$request_data['RATING']     = $request->get('rating') ?: "";
		$request_data['MEAL']       = $request->get('meal') ?: "";
		$request_data['ADULTS']     = $request->get('adults') ?: $this->arParams['ADULTS'];
		$request_data['CHILD']      = $request->get('child') ?: "0";
		$request_data['CHILDAGE1']  = $request->get('childage1') ?: "0";
		$request_data['CHILDAGE2']  = $request->get('childage2') ?: "0";
		$request_data['CHILDAGE3']  = $request->get('childage3') ?: "0";
		$request_data['PRICEFROM']  = $request->get('pricefrom') ?: "0";
		$request_data['PRICETO']    = $request->get('priceto') ?: "1000000";;
		$request_data['PRICERANGE']    = $request_data['PRICEFROM'] . "," . $request_data['PRICETO'];
		$request_data['AR_HOTELTYPES'] = $request->get('hoteltypes');
		$request_data['HOTACTIVE']     = in_array("active", $request_data['AR_HOTELTYPES']) ? "1" : "";
		$request_data['HOTRELAX']      = in_array("relax", $request_data['AR_HOTELTYPES']) ? "1" : "";
		$request_data['HOTFAMILY']     = in_array("family", $request_data['AR_HOTELTYPES']) ? "1" : "";
		$request_data['HOTHEALTH']     = in_array("health", $request_data['AR_HOTELTYPES']) ? "1" : "";
		$request_data['HOTCITY']       = in_array("city", $request_data['AR_HOTELTYPES']) ? "1" : "";
		$request_data['HOTBEACH']      = in_array("beach", $request_data['AR_HOTELTYPES']) ? "1" : "";
		$request_data['HOTDELUXE']     = in_array("deluxe", $request_data['AR_HOTELTYPES']) ? "1" : "";
		$request_data['HOTELTYPES']    = implode(",", $request->get('hoteltypes')) ?: "";
		$request_data['REGIONS']       = implode(",", $request->get('regions'));;
		$request_data['SUBREGIONS']    = implode(",", $request->get('subregions'));
		$request_data['OPERATORS']     = implode(",", $request->get('operators'));
		$request_data['HOTELS']        = implode(",", $request->get('hotels'));
		$request_data['AR_HOTELS']     = $request->get('hotels') ? $request->get('hotels') : array();
		$request_data['AR_REGIONS']    = $request->get('regions');
		$request_data['AR_SUBREGIONS'] = $request->get('subregions');
		$request_data['AR_OREPATORS']  = $request->get('operators');
		$request_data['ONPAGE']        = $this->arParams['COUNT'] ?: "25";


		$request_data['START'] = $request->get('start') ?: "N";

		$this->arResult['REQUEST_PARAMS'] = $request_data;

		unset($request_data);
	}


	//Запускаем компонент
	public function executeComponent() {
		global $APPLICATION;
		Loader::includeModule("newtravel.search");
		Loader::includeModule("iblock");

		//Подготовим параматеры для подстановок
		$this->prepareParams();
		$DEPARTURE_IN_URL = $this->request->get('departure') ? true : false; //Присутстувет ли параметр departure в url

		$cache_id = $this->arResult['REQUEST_PARAMS']['DEPARTURE'] . "_" . $this->arResult['REQUEST_PARAMS']['COUNTRY'] . "_" . $DEPARTURE_IN_URL;
		$cache_id .= "_" . $this->arResult['REQUEST_PARAMS']['DATERANGE'];
		$cache_id .= "_" . $this->arResult['REQUEST_PARAMS']['REGIONS'];
		$cache_id .= "_" . $this->arResult['REQUEST_PARAMS']['SUBREGIONS'];
		if ($this->arParams['USE_GEO_DEFINE'] == "Y") {
			DefineGeo::setRegionIso();
			$cache_id .= "_" . $_SESSION['REGION_ISO'];
		}

		$USER_CITY = $APPLICATION->get_cookie('USER_CITY');
		$cache_id .= $USER_CITY;

		if ($this->startResultCache($this->arParams['CACHE_TIME'], $cache_id)) {
			//Получаем города вылета
			$this->arResult['DEPARTURE_LIST'] = TourvisorHlLists::getList('departures');

			//Определяем текущий город. Если не определен get параметр departure
			//Иначе выбираем город из параметра departure
			if (isset($USER_CITY) && ! empty($USER_CITY) && ! $DEPARTURE_IN_URL && $this->arParams['USE_COOKIE_DEPARTURE'] == "Y") { //Определена кука с ID города и не определен параметр departure
				foreach ($this->arResult['DEPARTURE_LIST'] as $arDeparture) {
					if ($arDeparture["UF_XML_ID"] == $USER_CITY) {
						$this->arResult['CURRENT_DEPARTURE'] = $arDeparture;
						break;
					}
				}
			} else if ($this->arParams['USE_GEO_DEFINE'] == "Y" && ! $DEPARTURE_IN_URL) { //тогда выбираем город определенный по IP
				Loader::includeModule('newtravel.search');
				DefineGeo::setRegionIso();

				$this->arResult['CURRENT_DEPARTURE']           = DefineGeo::getCity();
				$this->arResult['REQUEST_PARAMS']['DEPARTURE'] = $this->arResult['CURRENT_DEPARTURE']["UF_XML_ID"];
			} else {
				if (isset($this->arResult['REQUEST_PARAMS']['DEPARTURE']) && ! empty($this->arResult['REQUEST_PARAMS']['DEPARTURE'])) {
					foreach ($this->arResult['DEPARTURE_LIST'] as $arDeparture) {
						if ($arDeparture["UF_XML_ID"] == $this->arResult['REQUEST_PARAMS']['DEPARTURE']) {
							$this->arResult['CURRENT_DEPARTURE'] = $arDeparture;
							break;
						}
					}
				}
			}
			unset($arDeparture);

			//Получаем страны
			$this->arResult['COUNTRY_LIST'] = TourvisorHlLists::getList('countries');

			//Определяем текущую страну
			if (isset($this->arResult['REQUEST_PARAMS']['COUNTRY']) && ! empty($this->arResult['REQUEST_PARAMS']['COUNTRY'])) {
				foreach ($this->arResult['COUNTRY_LIST'] as $arCountry) {
					if ($arCountry["UF_XML_ID"] == $this->arResult['REQUEST_PARAMS']['COUNTRY']) {
						$this->arResult['CURRENT_COUNTRY'] = $arCountry;
						break;
					}
				}
			}
			unset($arCountry);

			//=======Получаем популярные страны========/
			$arSelect = Array("ID", "NAME", "CODE", "PREVIEW_PICTURE");
			$arFilter = Array("IBLOCK_CODE" => "popular_countries", "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$res      = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array(), $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields                                                 = $ob->GetFields();
				$this->arResult["POPULAR_COUNTRIES"][ $arFields["CODE"] ] = array("NAME" => $arFields["NAME"], "PICTURE" => $arFields["PREVIEW_PICTURE"]);
			}
			unset($arFields);


			//Получаем категории питания
			$this->arResult['MEAL_LIST'] = TourvisorHlLists::getList('meals');

			//Получаем регионы
			$this->arResult['REGION_LIST'] = TourvisorHlLists::getList('regions', array('*'), array("UF_COUNTRY_ID" => $this->arResult['CURRENT_COUNTRY']['UF_XML_ID']));
			//Если есть выбранные регионы в запросе, установливаем новый ключ SELECTED
			$request_reqions = explode(",", $this->arResult['REQUEST_PARAMS']['REGIONS']);
			if (isset($request_reqions) && is_array($request_reqions)) {
				foreach ($this->arResult['REGION_LIST'] as $key => $arRegion) {
					if (in_array($arRegion['UF_XML_ID'], $request_reqions)) {
						$this->arResult['REGION_LIST'][ $key ]['SELECTED'] = true;
					}
				}
			}
			unset($arRegion);

			//Получаем туроператоров
			$this->arResult['OPERATOR_LIST'] = TourvisorHlLists::getList('operators', array('*'));
			//Если есть выбранные операторы в запросе, установливаем новый ключ SELECTED
			$request_operators = explode(",", $this->arResult['REQUEST_PARAMS']['OPERATORS']);
			if (isset($request_operators) && is_array($request_operators)) {
				foreach ($this->arResult['OPERATOR_LIST'] as $key => $arOperator) {
					if (in_array($arOperator['UF_XML_ID'], $request_operators)) {
						$this->arResult['OPERATOR_LIST'][ $key ]['SELECTED'] = true;
					}
				}
			}
			unset($arOperator);

			$this->includeComponentTemplate();
		}
	}
}

?>