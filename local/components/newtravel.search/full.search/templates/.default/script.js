(function (window) {

    if (!!window.JSNewtravelSearch) {
        return;
    }

    window.JSNewtravelSearch = function (arParams) {

        this.params = arParams;
        this.params.currentPage = 1;
        this.params.searchIsFinish = false;
        this.toursfound = 0;
        this.params.nfrom = 0;
        this.params.nto = 0;
        this.params.hotelList = []; //Маcсив загруженных отелей
        this.params.openedtours = []; //уже раскрытые туры
        this.params.checkedHotels = arParams.AR_HOTELS; //Масив выбранных отелей
        this.params.urlParams = ""; //ссылка пользователя на поиск
        this.params.searchUrl = "/local/tools/newtravel.search.ajax.php"; //ajax ссылка на получение данных
        this.params.resultUrl = "/local/tools/newtravel.search.ajaxresult.php"; //ajax ссылка на получение данных
        this.helpformmessage = ""; //Генерируемое сообщение для формы подбора тура
        this.userurl = ""; //Ссылка пользователеля для формы поддбора тура
        this.visual = {
            departure: $("#departure"),
            country: $("#country"),
            countryList: $("#countryList"),
            regions: $("#regions"),
            hoteltypes: $("#hoteltypes input"),
            hotels: $("#hotels"),

            stars: $("#stars"),
            rating: $("#rating"),
            priceMin: $("#price-min"),
            priceMax: $("#price-max"),
            priceRange: $("#priceRange"),
            nightsCell: $("#nights-select table tr>td"),
            nightsText: $("#nights-select>strong"),

            requestId: $("#requestid"),
            daterange: $('input[name="daterange"]')
        };
        this.Init();
    };

    //Инициализация
    window.JSNewtravelSearch.prototype.Init = function () {
        var obThis = this;
        var visual = this.visual;
        var params = this.params;

        //mCustomScrollBar
        $(".customScroll").mCustomScrollbar();

        //Подтвреждение города
        $('.departure_wrap').confirmCity();


        //============================================Выбор цены===================================//
        var priceSlider = $('.slider');
        priceSlider.slider({});
        priceSlider.on("slide", function (slideEvt) {
            params.PRICEFROM = slideEvt.value[0];
            params.PRICETO = slideEvt.value[1];
            params.PRICERANGE = params.PRICEFROM + "," + params.PRICETO;

            visual.priceMin.text((slideEvt.value[0]).formatMoney(0, ' ', ' ') + " ₽");
            visual.priceMax.text((slideEvt.value[1]).formatMoney(0, ' ', ' ') + " ₽");
            visual.priceRange.val(slideEvt.value);
        });

        //==============================================Выбор диапазона дат==========================================//
        $('input[name="daterange"]').dateRangePicker({
            format: 'DD.MM.YYYY',
            startOfWeek: 'monday',
            singleMonth: 'auto',
            showShortcuts: false,
            autoClose: true,
            selectForward: true,
            stickyMonths: true,
            separator: '-',
            minDays: 1,
            maxDays: 14
        }).bind('datepicker-change', function (event, obj) {
            params.DATEFROM = moment(obj.date1).format("DD.MM.YYYY");
            params.DATETO = moment(obj.date2).format("DD.MM.YYYY");
            params.DATERANGE = params.DATEFROM + " - " + params.DATETO;
        });

        //==================================================Выбор ночей===========================================//
        $('#nightsValue').nightsSelect({
            defaultFrom: params.NIGHTSFROM,
            defaultTo: params.NIGHTSTO
        }).bind('nightSelect-change', function (event, obj) {
            params.NIGHTSFROM = obj.from;
            params.NIGHTSTO = obj.to;
        });

        obThis.getCountries();

        //Звездный рейтинг
        this.visual.stars.rating("update", this.params.STARS);


        //===============================Выбор туристов===============================================//
        $('.tourists-total').touristSelect({
            adults: params.ADULTS,
            child: params.CHILD,
            childage1: params.CHILDAGE1,
            childage2: params.CHILDAGE2,
            childage3: params.CHILDAGE3
        }).bind('touristsSelect-change', function (event, obj) {
            params.ADULTS = obj.adults;
            params.CHILD = obj.child;
            params.CHILDAGE1 = obj.childage1;
            params.CHILDAGE2 = obj.childage2;
            params.CHILDAGE3 = obj.childage3;
        });


        //===================Событие при смене курортов========================================//
        visual.regions.on("change", function (event) {
            //Если отмечаем родителя, тогда отмечаются все дочерние чекбоксы и наоборот
            var elem = event.target.parentElement;
            var checkbox = $(elem).find('input:first');
            if (checkbox.prop('checked')) {
                $.each($(elem).find('input'), function (i, el) {
                    $(el).prop('checked', true);
                });
            } else {
                $.each($(elem).find('input'), function (i, el) {
                    $(el).prop('checked', false);
                });
            }

            //Заполняем в параметры курорты и субкуороты
            var checkedRegions = $("#regions input:checked");
            var regions = "";
            var subregions = "";
            $.each(checkedRegions, function (i, region) {
                if ($(region).hasClass("subregion_item")) {
                    subregions += region.value + ",";
                } else {
                    regions += region.value + ",";
                }
            });
            params.REGIONS = regions.substring(0, regions.length - 1);
            params.SUBREGIONS = subregions.substring(0, subregions.length - 1);

            if (!checkbox.hasClass('subregion_item')) {
                obThis.getHotels();
            }
        });

        //================События при смене типов отелей========================================//
        $("#hotactive").on('change', function () {
            $("#hotall").prop("checked", false);
            params.HOTACTIVE = $(this).prop("checked") ? "1" : "";
            obThis.getHotels();
        });
        $("#hotrelax").on('change', function () {
            $("#hotall").prop("checked", false);
            params.HOTRELAX = $(this).prop("checked") ? "1" : "";
            obThis.getHotels();
        });
        $("#hotfamily").on('change', function () {
            $("#hotall").prop("checked", false);
            params.HOTFAMILY = $(this).prop("checked") ? "1" : "";
            obThis.getHotels();
        });
        $("#hothealth").on('change', function () {
            $("#hotall").prop("checked", false);
            params.HOTHEALTH = $(this).prop("checked") ? "1" : "";
            obThis.getHotels();
        });
        $("#hotcity").on('change', function () {
            $("#hotall").prop("checked", false);
            params.HOTCITY = $(this).prop("checked") ? "1" : "";
            obThis.getHotels();
        });
        $("#hotbeach").on('change', function () {
            $("#hotall").prop("checked", false);
            params.HOTBEACH = $(this).prop("checked") ? "1" : "";
            obThis.getHotels();
        });
        $("#hotdeluxe").on('change', function () {
            $("#hotall").prop("checked", false);
            params.HOTDELUXE = $(this).prop("checked") ? "1" : "";
            obThis.getHotels();
        });
        //================События при смене типов отелей========================================//
        $("#hoteltypes input").on('change', function () {
            params.AR_HOTELTYPES = [];

            //Если выбираем тип отеля, снимаем флафок с "Любой"
            if ($(this).attr("id") !== "hotall") {
                $("#hotall").prop("checked", false);
            }

            var checkedTypes = $("#hoteltypes input:checked");
            var types = "";
            $.each(checkedTypes, function (i, type) {
                if ($(type).data("value") !== undefined) {
                    types += $(type).data("value") + ",";
                }
            });
            params.HOTELTYPES = types.substring(0, types.length - 1);
        });


        //==============================================Событие при смене операторов====================================//
        $("#operators").on('change', function () {
            var checkedOperators = $("#operators input:checked");
            var operators = "";
            $.each(checkedOperators, function (i, operator) {
                operators += operator.value + ",";
            });
            params.OPERATORS = operators.substring(0, operators.length - 1);
        });

        //==============================================Событие при смене звезд====================================//
        visual.stars.on("rating.change", function (event, value, caption) {
            params.STARS = value;
            obThis.getHotels();
        });
        //===============================================Событие при смене рейтинга================================//
        visual.rating.on("change", function (event) {
            params.RATING = $(this).val();
            obThis.getHotels();
        });
        //==============================================Событие при смене типа питания====================================//
        $("#meal").on('change', function () {
            params.MEAL = $(this).val();
        });

        this.getSubRegions();

        if (params.START == "Y") { //Стартуем поиск
            setTimeout(this.searchTours(), 1500);
        }
    };

    //Событие при смене города отправления из модального окна
    window.JSNewtravelSearch.prototype.selectDeparture = function (id, name) {
        this.params.DEPARTURE = id;
        $("#departureValue").text(name);
        $("#departureModal").modal('hide');
        $.cookie('NEWTRAVEL_USER_CITY', id, { path: '/' }); //записываем в куки

        this.getCountries();
        this.getFlydates();
    };

    //Событие при смене страны из модального окна
    window.JSNewtravelSearch.prototype.selectCountry = function (id, name) {
        this.params.COUNTRY = id.toString();
        $("#countryValue").text(name);
        $("#countryModal").modal('hide');

        this.params.checkedHotels = []; //обнуляем выбранные отели
        this.params.AR_HOTELS = []; //обнуляем
        this.params.AR_REGIONS = []; //обнуляем
        this.params.AR_SUBREGIONS = []; //обнуляем
        this.params.HOTELS = ""; //обнуляем
        this.params.REGIONS = ""; //обнуляем
        this.params.SUBREGIONS = ""; //обнуляем
        this.getRegions();
    };

    //Получаем список стран. Если выбран город вылета то задействован параметр cndep
    window.JSNewtravelSearch.prototype.getCountries = function () {
        var visual = this.visual;
        $.getJSON(this.params.searchUrl, {type: "country", cndep: this.params.DEPARTURE}, function (json) {
            visual.countryList.empty();
            $.each(json.lists.countries.country, function (i, country) {
                visual.countryList.append($('<li><a href="javascript:void(0)" onclick="searchObject.selectCountry(' + country.id + ', \'' + country.name + '\')">' + country.name + '</a> </li>'));
            });
        });
        /* ==========================================================================
         Разбивка списка на колонки
         ========================================================================== */
        setTimeout(function () {
            $('.multiColumn').autocolumnlist({columns: 4});
        }, 1000);
        this.getFlydates();
    };

    //Получаем список курортов. Если выбрана страна то задействован параметр regcountry
    window.JSNewtravelSearch.prototype.getRegions = function () {
        $('.regions-spinner.spinner').show();
        $('.hotel-spinner.spinner').show();
        var obThis = this;
        var params = this.params;
        var visual = this.visual;
        $.getJSON(params.searchUrl, {type: "region", regcountry: params.COUNTRY}, function (json) {
            visual.regions.empty();
            $.each(json.lists.regions.region, function (i, region) {
                var checked = "";
                if ($.inArray(region.id, params.AR_REGIONS) >= 0) checked = "checked";
                visual.regions.append('<li id="region_' + region.id + '"><input type="checkbox" class="list-item" onclick="$(\'#allregions\').prop(\'checked\', false)" name="regions[]" ' + checked + ' value="' + region.id + '">' + region.name + '</li>')
            });

            var getFlydateMethod = function () {
                obThis.getFlydates(); //Запрашиваем даты вылета
            };
            var getSubRegMethod = function () {
                obThis.getSubRegions(); //Запрашиваем субрегионы
            };
            setTimeout(getFlydateMethod, 500);
            setTimeout(getSubRegMethod, 1000)

        });
    };

    //Получаем список субкурортов. Если выбрана страна то задействован параметр regcountry, если выбран курорт, тогда задействуем параметр parentregion
    window.JSNewtravelSearch.prototype.getSubRegions = function () {
        var obThis = this;
        var params = this.params;
        $.getJSON(params.searchUrl, {type: "subregion", regcountry: params.COUNTRY}, function (json) {
            if (json.lists !== undefined && json.lists.subregions.subregion !== null) {
                $.each(json.lists.subregions.subregion, function (i, subregion) {
                    var checked = "";
                    if ($.inArray(subregion.id, params.AR_REGIONS) >= 0) checked = "checked";
                    if ($.inArray(subregion.id, params.AR_SUBREGIONS) >= 0) checked = "checked";
                    $('#region_' + subregion.parentregion).append('<span><input type="checkbox" class="list-item subregion_item" onclick="$(\'#allregions\').prop(\'checked\', false)" name="subregions[]" ' + checked + ' value="' + subregion.id + '" data-parent="' + subregion.parentregion + '">' + subregion.name + '</span>')
                });
                //События при смене субкурортов. Выставляем статус indeterminate для региона, если выбан хотябы один субкурорт
                $('.subregion_item').on('change', function () {
                    var parentRegion = 'region_' + $(this).data('parent');
                    var countCheked = $('#' + parentRegion + '>span>input:checked').length;
                    obThis.subRegionFilter(); //фильтруем
                    if (countCheked > 0) {
                        $('#' + parentRegion + '>input').prop("indeterminate", true);
                    } else {
                        $('#' + parentRegion + '>input').prop("indeterminate", false);
                    }

                });
            }
            $('.regions-spinner.spinner').hide();
            obThis.getHotels();
        });
    };

    //Получаем спиоск дат вылета
    window.JSNewtravelSearch.prototype.getFlydates = function () {
        var obThis = this;
        var params = this.params;
        var visual = this.visual;
        $.getJSON(params.searchUrl, {type: "flydate", flydeparture: params.DEPARTURE, flycountry: params.COUNTRY}, function (json) {
            if (json.lists !== undefined && json.lists.flydates !== null) {
                $('input[name="daterange"]').data('dateRangePicker').destroy();
                $('input[name="daterange"]').dateRangePicker({
                    format: 'DD.MM.YYYY',
                    startOfWeek: 'monday',
                    singleMonth: 'auto',
                    showShortcuts: false,
                    autoClose: true,
                    selectForward: true,
                    stickyMonths: true,
                    separator: '-',
                    minDays: 1,
                    maxDays: 14,
                    beforeShowDay: function (date) {
                        if ($.inArray(moment(date).format("DD.MM.YYYY"), $.makeArray(json.lists.flydates.flydate)) >= 0) {
                            return [true, "in-flydate", ""];
                        } else {
                            return [true, "", ""];
                        }
                    }
                }).bind('datepicker-change', function (event, obj) {
                    params.DATEFROM = moment(obj.date1).format("DD.MM.YYYY");
                    params.DATETO = moment(obj.date2).format("DD.MM.YYYY");
                    params.DATERANGE = params.DATEFROM + " - " + params.DATETO;
                });
            }
        });
    };

    //Получаем список отелей
    window.JSNewtravelSearch.prototype.getHotels = function () {
        var params = this.params;
        var visual = this.visual;

        params.hotelList.length = 0;
        params.hotelList = new Array();

        $.getJSON(params.searchUrl, {
            type: "hotel",
            hotcountry: params.COUNTRY,
            hotregion: params.REGIONS,
            hotstars: params.STARS,
            hotrating: params.RATING,
            hotactive: params.HOTACTIVE,
            hotrelax: params.HOTRELAX,
            hotfamily: params.HOTFAMILY,
            hothealth: params.HOTHEALTH,
            hotcity: params.HOTCITY,
            hotbeach: params.HOTBEACH,
            hotdeluxe: params.HOTDELUXE
        }, function (json) {
            if (json.lists.hotels.hotel !== null) {
                visual.hotels.empty();
                $.each(json.lists.hotels.hotel, function (i, hotel) {
                    //заполняем массив отелей для фильтрации
                    var hotelInfo = new Array();
                    hotelInfo[0] = hotel.id;
                    hotelInfo[1] = hotel.name;
                    hotelInfo[3] = hotel.subregion;
                    params.hotelList[i] = hotelInfo;

                    var checked = "";
                    //if ($.inArray(hotel.id, selectedHotels) >= 0) checked = "checked";
                    if ($.inArray(hotel.id, params.checkedHotels) >= 0) checked = "checked";
                    visual.hotels.append('<li><input type="checkbox" class="list-item" onclick="searchObject.hotelSelect(this)" name="hotels[]" ' + checked + ' value="' + hotel.id + '">' + hotel.name + ' (' + hotel.stars + '*)</li>')
                });
                $('.hotel-spinner.spinner').hide();
            }
        });
    };

    /*
     * =============================================================
     * ПОИСК ТУРА
     * =============================================================
     * */
    window.JSNewtravelSearch.prototype.searchTours = function () {
        //Устанавливаем начальные значения видимых объектов
        $(".progress-section").slideDown(200);

        $(".progress-bar").css("width", "0%");
        $(".searchBtn").attr("disabled", true);
        $("#progress-percent").text("0%");
        $("#tours-found").text("0 туров найдено");
        $("#progress-status").text("идет поиск");

        $("#formok").css('display', 'none'); //Скрываем статус о успешной отправки заявки на подбор

        $("#request").css('display', 'none');
        $('body').scrollTo('#request');
        $('.defaultCountdown').countdown('destroy');
        $(".tours-more-btn").hide();
        $("#tour-help-form-link, #tour-help-form-conatainer").hide();

        this.params.currentPage = 1;

        this.makeRequest();
    };

    //Делаем запрос на получение requestId
    window.JSNewtravelSearch.prototype.makeRequest = function () {

        var obThis = this;
        var params = this.params;

        var request_params = {
            type: "search",
            departure: params.DEPARTURE,
            country: params.COUNTRY,
            datefrom: params.DATEFROM,
            dateto: params.DATETO,
            nightsfrom: params.NIGHTSFROM,
            nightsto: params.NIGHTSTO,
            adults: params.ADULTS,
            child: params.CHILD,
            childage1: params.CHILDAGE1,
            childage2: params.CHILDAGE2,
            childage3: params.CHILDAGE3,
            stars: params.STARS,
            meal: params.MEAL,
            rating: params.RATING,
            pricefrom: params.PRICEFROM,
            priceto: params.PRICETO,
            regions: params.REGIONS,
            subregions: params.SUBREGIONS,
            hotels: params.HOTELS,
            hoteltypes: params.HOTELTYPES,
            operators: params.OPERATORS
        };
        //Формируем строку для формы подбора тура, если туров не будет найдено
        params.helpformmessage = "Из города: " + $.trim($("#departureValue").text()) + "\n";
        params.helpformmessage += "Куда: " + $.trim($("#countryValue").text()) + "\n";
        params.helpformmessage += "Кто едет: " + params.ADULTS + " взрослых, " + params.CHILD + " детей" + "\n";
        params.helpformmessage += "Дата вылета: с " + params.DATEFROM + " по " + params.DATETO + "\n";
        params.helpformmessage += "Продолжительность: " + params.NIGHTSFROM + " - " + params.NIGHTSTO + " ночей \n";
        params.helpformmessage += "Класс отеля: " + params.STARS + "*" + "\n";

        params.currentPage = 1;
        params.searchIsFinish = false;

        $.getJSON(params.searchUrl, request_params, function (json) {
            //Меняем url браузера
            obThis.urlParams = json.url;
            history.pushState(json.result.requestid, "Поиск туров", "?start=Y&" + obThis.urlParams);
            params.userurl = window.location.href; //Параметр со ссылкой по которой искал пользователь(подставляется в скрытое поле в фомре помощи подбора, елси ничего не найдено)
            $("#user-url").val(params.userurl);

            params.REQUEST_ID = json.result.requestid;

            obThis.checkTourStatus();
        });
        $("#help-form-message").val(params.helpformmessage); //Подставляем сообщение в форму подбора


    };
    //Проверяем статус поиска туров, елси finished, тогда выводим результаты
    window.JSNewtravelSearch.prototype.checkTourStatus = function () {
        var obThis = this;
        var params = this.params;

        $.getJSON(params.searchUrl, {type: "status", requestid: params.REQUEST_ID}, function (json) {
            if (json.data.status.state != "finished" && json.data.status.timepassed < "45") {
                $(".progress-bar").css("width", json.data.status.progress + "%");
                setTimeout(function () {
                    obThis.checkTourStatus();
                }, 3000);
                $("#progress-percent").text(json.data.status.progress + "%");

                if ((json.data.status.timepassed >= 2 && json.data.status.timepassed <= 20) && json.data.status.toursfound > 0) {
                    obThis.getToursData();
                    $("#tours-found").text(json.data.status.toursfound + " туров найдено");
                }
            } else {
                params.searchIsFinish = true;
                params.toursfound = json.data.status.toursfound; //Количество найденных туров

                $(".searchBtn").attr("disabled", false);
                $(".progress-bar").css("width", json.data.status.progress + "%");
                $("#progress-percent").text(json.data.status.progress + "%");
                $("#tours-found").text(json.data.status.toursfound + " туров найдено");
                /*=========================================================================
                 countdown
                 ========================================================================== */
                var newYear = new Date();
                newYear = new Date(newYear.getFullYear() + 1, 1 - 1, 1);
                $('.defaultCountdown').countdown({
                    since: '',
                    compact: true
                    //layout: "<span>{hn}</span>:<span>{mn}</span>:<span>{sn}</span>"
                });

                obThis.getToursData();
            }
        });
    };

    //Получаем данные по турам
    window.JSNewtravelSearch.prototype.getToursData = function () {
        var params = this.params;
        var obThis = this;

        $.get(params.resultUrl, {requestid: params.REQUEST_ID, page: params.currentPage, isfinish: params.searchIsFinish}, function (data) {
            if (params.searchIsFinish) {
                $("#progress-percent").text("100%");
                $("#progress-status").text("поиск завершен");

                if (params.currentPage == "1") {
                    $(".progress-section").slideUp(200);
                    $("#request").css('display', 'block').html(data);
                    obThis.checkOpenedTours();
                    setTimeout(function () {
                        obThis.createHotelLink();
                    }, 1000);
                } else {
                    $("#request").append(data);
                    setTimeout(function () {
                        obThis.createHotelLink();
                    }, 1000);
                }
                params.currentPage = params.currentPage + 1;
                $('#requestid').html(requestid);

                if (params.toursfound > 0) {
                    $(".tours-more-btn").show();
                    $(".tours-more-btn a").text("Показать еще...");
                    $("#tour-help-form-link").show();
                }

                return false;
            } else {
                $("#request").css('display', 'block').html(data);
                obThis.checkOpenedTours();
                setTimeout(function () {
                    obThis.createHotelLink();
                }, 1000);
            }
        })
    };

    //Подробная информация по туру
    window.JSNewtravelSearch.prototype.actualizeTour = function (tourid) {
        $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "actualize", tourid: tourid}, function (json) {
        });
    };

    window.JSNewtravelSearch.prototype.buyTour = function (tourid) {
        var params = this.params;
        $("body").css("cursor", "wait");
        $.get(params.resultUrl, {requestid: params.REQUEST_ID, tourid: tourid}, function (data) {
            $('#request').html(data);
            $("body").css("cursor", "default");
        })
    };

    /*
     * =============================================================
     * СЕРВИСНЫЕ ФУНКЦИИ
     * =============================================================
     * */
    //Очищаем чекбоксы
    window.JSNewtravelSearch.prototype.clearCheckboxes = function (element, listId) {
        if ($(element).prop("checked")) {
            var clearEl = $("#" + listId).find(".list-item");
            $(clearEl).prop("checked", false);
        }
        if (listId == "hotels") {
            this.params.checkedHotels = [];
            this.params.AR_HOTELS = [];
            this.params.HOTELS = "";
        }
        if (listId == "regions") {
            this.params.AR_REGIONS = [];
            this.params.AR_SUBREGIONS = [];
            this.params.REGIONS = "";
            this.params.SUBREGIONS = "";
        }
        if (listId == "operators") {
            this.params.AR_OPERATORS = [];
            this.params.OPERATORS = "";
        }
        if (listId == "operators") {
            this.params.HOTELTYPES = "";
        }

    };
    /*===========Работа с результатами=============*/
    //Запоминаем открытые туры в отеле
    window.JSNewtravelSearch.prototype.setOpenedTours = function (className) {
        var params = this.params;
        setTimeout(function () {
            var display = $(className).css("display");
            if (display == "block") {
                params.openedtours.push(className);
            } else {
                params.openedtours.splice(className, 1);
            }
        }, 500);
    };
    //Востанавливием открытые туры в отеле
    window.JSNewtravelSearch.prototype.checkOpenedTours = function () {
        $(this.params.openedtours).each(function (key, item) {
            $(item).css("display", "block")
        })
    };

    //фильтрация по субрегионам
    window.JSNewtravelSearch.prototype.subRegionFilter = function () {
        var params = this.params;
        var visual = this.visual;

        var subRegIds = new Array();
        $.each($('.subregion_item:checked'), function (key, elem) {
            subRegIds[key] = $(elem).val();
        });

        visual.hotels.empty();
        var hotelListClone = params.hotelList;
        $.each(hotelListClone, function (index, hotel) {
            var checked = "";
            if ($.inArray(hotel[3], subRegIds) >= 0) {
                visual.hotels.append($('<li><input type="checkbox" onclick="searchObject.hotelSelect(this)" class="list-item" name="hotels[]" ' + checked + ' value="' + hotel[0] + '">' + hotel[1] + '</li>'));
            }
        });
        if (subRegIds.length == 0) {
            $.each(hotelListClone, function (index, hotel) {
                var checked = "";
                visual.hotels.append($('<li><input type="checkbox" onclick="searchObject.hotelSelect(this)" class="list-item" name="hotels[]" ' + checked + ' value="' + hotel[0] + '">' + hotel[1] + '</li>'));
            });
        }
    };
    //фильтрация отелей
    window.JSNewtravelSearch.prototype.filterHotel = function (self) {
        var params = this.params;
        var visual = this.visual;

        var filterString = $("#hotelsName").val();
        if (filterString.length >= 3) {
            $("#showSelectedHotels").prop("checked", false);
            visual.hotels.empty();
            var hotelListClone = params.hotelList;
            var filteredList = hotelListClone.filter(searchObject.filter);
            $.each(filteredList, function (index, hotel) {
                var checked = "";
                if ($.inArray(hotel[0], params.checkedHotels) >= 0) checked = "checked";
                visual.hotels.append($('<li><input type="checkbox" onclick="searchObject.hotelSelect(this)" class="list-item" name="hotels[]" ' + checked + ' value="' + hotel[0] + '">' + hotel[1] + '</li>'));
            });
        } else {
            visual.hotels.empty();
            $.each(params.hotelList, function (index, hotel) {
                var checked = "";
                if ($.inArray(hotel[0], params.checkedHotels) >= 0) checked = "checked";
                visual.hotels.append($('<li><input type="checkbox" onclick="searchObject.hotelSelect(this)" class="list-item" name="hotels[]" ' + checked + ' value="' + hotel[0] + '">' + hotel[1] + '</li>'));

            })
        }
    };
    window.JSNewtravelSearch.prototype.filter = function (item) {
        var filterString = $("#hotelsName").val();
        var hotelName = item[1];
        hotelName = hotelName.toLowerCase();
        filterString = filterString.toLowerCase();
        pos = hotelName.indexOf(filterString);
        if (-1 < pos) {
            return true;
        } else {
            return false;
        }
    };
    //Запоминаем выбранные отели
    window.JSNewtravelSearch.prototype.hotelSelect = function (hotel) {
        var params = this.params;
        var checked = $(hotel).prop("checked");
        var id = $(hotel).val();
        if (checked) {
            params.checkedHotels.push(id);
        } else {
            params.checkedHotels.splice(params.checkedHotels.indexOf(id), 1);
        }
        $("#allhotel").prop("checked", false);

        //Генерируем строку из массива выбранных отелей
        params.HOTELS = "";
        $.each(params.checkedHotels, function (i, hotel) {
            params.HOTELS += hotel + ",";
        });
        params.HOTELS = params.HOTELS.substring(0, params.HOTELS.length - 1);
    };
    //Показываем только выборанные отели
    window.JSNewtravelSearch.prototype.showOnlySelectedHotels = function (elem) {
        var visual = this.visual;
        if ($(elem).prop("checked")) {
            visual.hotels.find(".list-item").each(function (index, item) {
                if (!$(item).prop("checked")) {
                    $(this).parent().css("display", "none");
                }
            });
        } else {
            visual.hotels.find(".list-item").each(function (index, item) {
                $(this).parent().css("display", "block");
            });
        }
    };

    window.JSNewtravelSearch.prototype.createHotelLink = function () {
        var params = this.params;
        var obThis = this;

        $(".hotel-link").each(function () {
            var href = $(this).attr("href");
            var newHref = href + "&" + obThis.urlParams;
            $(this).attr("href", newHref);
        });
    };

    //===============================================Подгрузка следующей страницы отелей =============================//
    window.JSNewtravelSearch.prototype.moreTours = function () {
        $(".tours-more-btn a").html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
        this.getToursData();
    };

    //===============================================Для компонента результатов =============================//
    //Разворачивание описания отеля
    window.JSNewtravelSearch.prototype.openHotelDesc = function (hotelcode) {
        var params = this.params;

        $(".tours_" + hotelcode).slideUp();
        $(".map_" + hotelcode).slideUp();

        $.getJSON(params.searchUrl, {type: "fullhotel", hotelcode: hotelcode}, function (json) {
            $(".hotel_" + hotelcode).find(".description").html(json.data.hotel.description);
            $(".hotel_" + hotelcode).find(".images").empty();

            $(json.data.hotel.images.image).each(function (i, item) {
                $(".hotel_" + hotelcode).find(".images").append("<a href='" + item + "' class='fancybox'><img onmouseover='searchObject.viewFullImg(this," + hotelcode + ")' src='" + item + "' style='height:40px; width:50px; margin-bottom:5px;margin-right:5px'></a>");
            });

            setTimeout(function () {
                mapintab = new GMaps({
                    el: '#mapintab_' + hotelcode,
                    height: 322,
                    width: 100,
                    scrollwheel: false,
                    lat: json.data.hotel.coord1,
                    lng: json.data.hotel.coord2
                });

                mapintab.addMarker({
                    lat: json.data.hotel.coord1,
                    lng: json.data.hotel.coord2,
                    clickable: false,
                    icon: "/local/templates/newtravel/images/mappin.png"
                });
            }, 500);
            $(".hotel_" + hotelcode).slideToggle();
        });
    };

    window.JSNewtravelSearch.prototype.viewFullImg = function (img, hotelcode) {
        $("#fullImg_" + hotelcode).css("display", "block");
        $("#fullImg_" + hotelcode + ">img").attr("src", $(img).attr("src"));
    };

    window.JSNewtravelSearch.prototype.showTourHelpForm = function () {
        $("#tour-help-form-link").slideUp(200);
        $("#tour-help-form-conatainer").slideDown(200);
    };

})(window);


