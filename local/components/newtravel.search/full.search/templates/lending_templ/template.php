<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

$this->addExternalCss(SITE_TEMPLATE_PATH . "/js/star-rating/css/star-rating.min.css");
$this->addExternalCss(SITE_TEMPLATE_PATH . "/js/jquery.countdown/jquery.countdown.css");
$this->addExternalCss(SITE_TEMPLATE_PATH . "/js/slider/css/slider.css");
$this->addExternalCss(SITE_TEMPLATE_PATH . "/js/daterangepicker2/daterangepicker.min.css");


$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/star-rating/js/star-rating.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jquery.countdown/jquery.plugin.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jquery.countdown/jquery.countdown.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jquery.countdown/jquery.countdown-ru.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/slider/js/bootstrap-slider.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jQuery.AutoColumnList/jquery.autocolumnlist.min.js");
$this->addExternalJs("https://maps.googleapis.com/maps/api/js?key=AIzaSyAKYVzO5rPM1Gqq8crmt9Bku0uvKhr7t4g&sensor=true");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/plugins/gmaps.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/daterangepicker2/jquery.daterangepicker.min.js");

//Выбор ночей
$this->addExternalCss("/local/modules/newtravel.search/js/jquery.nights.select/jquery.nights.select.min.css");
$this->addExternalJs("/local/modules/newtravel.search/js/jquery.nights.select/jquery.nights.select.min.js");
//Выбор туристов
$this->addExternalCss("/local/modules/newtravel.search/js/jquery.tourists.select/jquery.tourists.select.min.css");
$this->addExternalJs("/local/modules/newtravel.search/js/jquery.tourists.select/jquery.tourists.select.min.js");

//Подтверждение города
$this->addExternalCss("/local/modules/newtravel.search/js/jquery.confirm.city/jquery.confirm.city.css");
$this->addExternalJs("/local/modules/newtravel.search/js/jquery.confirm.city/jquery.confirm.city.js");
?>


<!-- tour-section -->
<section class="gray-section" style="padding-top: 15px">
	<div class="container">
		<div class="tour-form-top row">
			<div class="row">
				<form class="form-inline" action="/tours" name="searchTour" id="searchTour" method="GET">
					<input type="hidden" name="type" value="search">

					<div id="departure_frame">
						<div class="col-md-2 col-sm-12 input-line">
							<div class="input-group departure_wrap" data-container="body">
								<input type="hidden" name="departure" id="departure" value="<?=$arResult['CURRENT_DEPARTURE']['UF_XML_ID']?>"/>
								<label>Город вылета: </label>
								<div class="form-control pseudo-input" id="departureValue" onclick="$('#departureModal').modal('show');">
									<?=$arResult['CURRENT_DEPARTURE']['UF_NAME']?>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-2 col-sm-12 input-line">
						<div class="input-group">
							<input type="hidden" name="country" id="country" value="<?=$arResult['CURRENT_COUNTRY']['UF_XML_ID']?>"/>
							<label>Страна: </label>

							<div class="form-control pseudo-input" id="countryValue" onclick="$('#countryModal').modal('show');">
								<?=$arResult['CURRENT_COUNTRY']['UF_NAME']?>
							</div>
						</div>
					</div>
					<div class="col-md-2 col-sm-12 input-line">
						<div class="input-group">
							<label>Дата вылета:</label>
							<input type="text" class="form-control" name="daterange" id="daterange" value="<?=$arResult['REQUEST_PARAMS']['DATERANGE']?>" style="height: 36px;"/>
						</div>
					</div>
					<div class="col-md-2 col-xs-12 input-line">
						<div class="input-group">
							<label>Ночей: </label>
							<div class="form-control pseudo-input" id="nightsValue">от <?=$arResult['REQUEST_PARAMS']['NIGHTSFROM']?> до <?=$arResult['REQUEST_PARAMS']['NIGHTSTO']?></div>
						</div>
					</div>
					<div class="col-md-2 col-sm-12 input-line">
						<div class="toursit-select input-group">
							<label>Туристы: </label>
							<div class="form-control tourists-total">
								<?=$arResult['REQUEST_PARAMS']['ADULTS']?> взр. <? if ($arResult['REQUEST_PARAMS']['CHILD'] > 0): ?><?=$arResult['REQUEST_PARAMS']['CHILD']?> реб.<? endif; ?>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-2 input-line">
						<div class="form-group" style="  margin-top: 30px; width: 100%">
							<!--noindex-->
							<button type="button" onclick="searchObject.searchTours()" class="btn btn-blue pull-right" style="width: 100%">Поехали</button>
							<!--/noindex-->
						</div>
					</div>
				</form>
			</div>
			<div class="row">
				<a href="javascript:void(0)" class="show-filter-link" onclick="searchObject.showFullFilter();">скрыть/показать дополнительные параметры</a>
			</div>
		</div>

		<div class="row tour-body">

			<div class="col-xs-12 text-center">
				<a href="javascript:void(0)" class="hidden-lg hidden-md" onclick="$('.tour-form-sidebar').slideToggle(200)">
					Показать дополнительные параметры поиска
				</a>
			</div>

			<div class="tour-form-sidebar col-xs-12 col-md-3" style="display: none;">
				<form class="form-horizontal">

					<!--Категории отеля-->
					<div class="form-group">
						<div class="col-xs-12">
							<label>Категория отеля: </label>
							<input id="stars" type="number" name="stars" class="rating" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-caption="false" data-show-clear="false">
						</div>
					</div>

					<!--Категория питания-->
					<div class="form-group">
						<div class="col-xs-12">
							<label>Категория питания: </label>
						</div>
						<div class="col-xs-12">
							<div class="row">
								<select class="form-control" name="meal" id="meal">
									<option value="1">Любое</option>
									<? foreach ($arResult['MEAL_LIST'] as $arMeal): ?>
										<option <? if ($arResult['REQUEST_PARAMS']['MEAL'] == $arMeal['UF_XML_ID']): ?>selected<? endif; ?> value="<?=$arMeal['UF_XML_ID']?>">
											<?=$arMeal['UF_NAME'] . "(" . $arMeal['UF_CODE'] . ")"?>
										</option>
									<? endforeach; ?>
								</select>
							</div>
						</div>
					</div>

					<!--Рейтинг отеля-->
					<div class="form-group">
						<div class="col-xs-12">
							<label>Рейтинг отеля: </label>
						</div>
						<div class="col-xs-12">
							<div class="row">
								<select class="form-control" name="rating" id="rating">
									<option value="0">Любой</option>
									<option <? if ($arResult['REQUEST_PARAMS']['RATING'] == 2): ?>selected<? endif; ?> value="2">>3</option>
									<option <? if ($arResult['REQUEST_PARAMS']['RATING'] == 3): ?>selected<? endif; ?> value="3">>3.5</option>
									<option <? if ($arResult['REQUEST_PARAMS']['RATING'] == 4): ?>selected<? endif; ?> value="4">>4</option>
									<option <? if ($arResult['REQUEST_PARAMS']['RATING'] == 5): ?>selected<? endif; ?> value="5">>4.5</option>
								</select>
							</div>
						</div>
					</div>

					<!--Тип отеля-->
					<div class="form-group">
						<div class="col-xs-12">
							<label>Тип отеля: </label>
						</div>
						<div class="whiteBg" id="hoteltypes">
							<div class="col-xs-12">
								<label class="checkbox-inline" for="hotall">
									<input type="checkbox" value="" name="all" onclick="searchObject.clearCheckboxes(this, 'hoteltypes')" id="hotall">
									Любой
								</label>
								<label class="checkbox-inline" for="hotactive">
									<input type="checkbox" value="1" class="list-item" name="hotactive"
									       <? if (in_array("active", $arResult['REQUEST_PARAMS']['AR_HOTELTYPES'])): ?>checked<? endif; ?>
									       id="hotactive"
									       data-value="active">
									Активный
								</label>
							</div>
							<div class="col-xs-12">
								<label class="checkbox-inline" for="hotdeluxe">
									<input type="checkbox" value="1" class="list-item" name="hotdeluxe"
									       <? if (in_array("deluxe", $arResult['REQUEST_PARAMS']['AR_HOTELTYPES'])): ?>checked<? endif; ?>
									       id="hotdeluxe"
									       data-value="deluxe">
									Deluxe
								</label>
								<label class="checkbox-inline" for="hotrelax">
									<input type="checkbox" value="1" class="list-item" name="hotrelax"
									       <? if (in_array("relax", $arResult['REQUEST_PARAMS']['AR_HOTELTYPES'])): ?>checked<? endif; ?>
									       id="hotrelax"
									       data-value="relax">
									Спокойный
								</label>
							</div>
							<div class="col-xs-12">
								<label class="checkbox-inline" for="hotfamily">
									<input type="checkbox" value="1" class="list-item" name="hotfamily"
									       <? if (in_array("family", $arResult['REQUEST_PARAMS']['AR_HOTELTYPES'])): ?>checked<? endif; ?>
									       id="hotfamily"
									       data-value="family">
									Семейный
								</label>
								<label class="checkbox-inline" for="hotcity">
									<input type="checkbox" value="1" class="list-item" name="hotcity"
									       <? if (in_array("city", $arResult['REQUEST_PARAMS']['AR_HOTELTYPES'])): ?>checked<? endif; ?>
									       id="hotcity"
									       data-value="city">
									Городской
								</label>
							</div>
							<div class="col-xs-12">
								<label class="checkbox-inline" for="hothealth">
									<input type="checkbox" value="1" class="list-item" name="hothealth"
									       <? if (in_array("health", $arResult['REQUEST_PARAMS']['AR_HOTELTYPES'])): ?>checked<? endif; ?>
									       id="hothealth"
									       data-value="health">
									Здоровье
								</label>
								<label class="checkbox-inline" for="hotbeach">
									<input type="checkbox" value="1" class="list-item" name="hotbeach"
									       <? if (in_array("beach", $arResult['REQUEST_PARAMS']['AR_HOTELTYPES'])): ?>checked<? endif; ?>
									       id="hotbeach"
									       data-value="beach">
									Пляжный
								</label>
							</div>
						</div>
					</div>

					<!--Стоимость-->
					<div class="form-group">
						<div class="col-xs-12 price-group">
							<label>Стоимость: </label>
						</div>
						<div class="col-xs-12">
							<input type="text" class="slider" value="" data-slider-min="0" data-slider-max="1000000" data-slider-step="1000"
							       data-slider-value="[<?=$arResult['REQUEST_PARAMS']['PRICEFROM']?>,<?=$arResult['REQUEST_PARAMS']['PRICETO']?>]"
							       data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide">
							<input type="hidden" class="form-control" id="priceRange" aria-describedby="sizing-addon2" value="<?=$arResult['REQUEST_PARAMS']['PRICERANGE']?>">
							<div class="row">
								<div class="col-xs-5 price-box" id="price-min"><?=CurrencyFormat($arResult['REQUEST_PARAMS']['PRICEFROM'], 'RUB')?></div>
								<div class="col-xs-5 col-xs-offset-2 price-box text-right" id="price-max"><?=CurrencyFormat($arResult['REQUEST_PARAMS']['PRICETO'], 'RUB')?></div>
							</div>
						</div>
					</div>

					<!--Курорты-->
					<div class="form-group">
						<div class="col-xs-12">
							<label>Курорты: </label>
						</div>
						<div class="whiteBg customScroll">
							<div class="regions-spinner spinner text-center">
								<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
								<br>
								<span class="sr-only">Загрузка...</span>
							</div>
							<ul id="regions">
								<? foreach ($arResult['REGION_LIST'] as $arRegion): ?>
									<li id="region_<?=$arRegion['UF_XML_ID']?>">
										<input type="checkbox" class="list-item" <? if ($arRegion['SELECTED']): ?>checked<? endif; ?>
										       onclick="$('#allregions').prop('checked', false)" name="regions[]" value="<?=$arRegion['UF_XML_ID']?>">
										<?=$arRegion['UF_NAME']?>
									</li>
								<? endforeach; ?>
							</ul>
						</div>
						<div class="col-xs-12 col-md-5">
							<label for="allregions"><input type="checkbox" id="allregions" name="regions[]" onclick="searchObject.clearCheckboxes(this, 'regions')" value="0" style="margin-right:
							10px">Любой</label>
						</div>
					</div>

					<!--Отлели-->
					<div class="form-group">
						<div class="col-xs-12">
							<label>Отели: </label>
						</div>
						<input type="text" style="background: #fff" class="form-control" placeholder="Поиск...Например 'ROSE HOTEL' " aria-describedby="sizing-addon2" name="hotelsName" value=""
						       id="hotelsName" onkeyup="searchObject.filterHotel(this)">

						<div class="whiteBg customScroll">
							<div class="hotel-spinner spinner text-center">
								<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
								<br>
								<span class="sr-only">Загрузка...</span>
							</div>
							<ul id="hotels">
								<!--Здесь список отелей-->
							</ul>
						</div>
						<div class="col-xs-12 col-md-5">
							<label for="allhotel">
								<input type="checkbox" id="allhotel" name="hotels[]" onclick="searchObject.clearCheckboxes(this, 'hotels')" value="0" style="margin-right: 10px">
								Любой
							</label>
						</div>
						<div class="col-xs-12 col-md-7">
							<label for="showSelectedHotels">
								<input type="checkbox" id="showSelectedHotels" name="hotels[]" onclick="searchObject.showOnlySelectedHotels(this)" value="0" style="margin-right: 10px">
								Выбраные отели
							</label>
						</div>
					</div>

					<!--Туроператор-->
					<div class="form-group">
						<div class="col-xs-12">
							<label>Туроператор: </label>
						</div>
						<div class="whiteBg customScroll">
							<ul id="operators">
								<? foreach ($arResult['OPERATOR_LIST'] as $arOperator): ?>
									<li>
										<input type="checkbox" class="list-item"
										       <? if ($arOperator['SELECTED']): ?>checked<? endif; ?>
										       onclick="$('#alloperators').prop('checked', false)" name="operators[]" value="<?=$arOperator['UF_XML_ID']?>"/>
										<?=$arOperator['UF_NAME']?>
									</li>
								<? endforeach; ?>
							</ul>
						</div>
						<div class="col-xs-12 col-md-5">
							<label for="alloperators"><input type="checkbox" id="alloperators" name="regions[]" onclick="searchObject.clearCheckboxes(this, 'operators')" value="0"
							                                 style="margin-right: 10px">Любой</label>
						</div>
					</div>

					<!--Кнопка поиск-->
					<div class="form-group">
						<!--noindex-->
						<button type="button" class="btn btn-blue pull-right" onclick="searchObject.searchTours()" style="width: 100%">Поехали</button>
						<!--/noindex-->
					</div>
				</form>

				<div class="row hidden-xs">

					<div class="col-xs-12"><label>Актуальность туров: </label></div>
					<div class="timer">
						<div class="defaultCountdown">

						</div>
					</div>

					<div class="col-xs-12"><label>В стоимость каждого тура входит: </label></div>

					<div class="whiteBg">
						<ul class="additional-list">
							<li><i class="fa fa-plane fa-2x"></i> <span>Авиаперелет туда-обратно</span></li>
							<li><i class="fa fa-arrow-circle-o-right fa-2x"></i> <span>Трансфер аэропорт-отель-аэропорт</span></li>
							<li><i class="fa fa-home fa-2x"></i> <span>Проживание в отеле</span></li>
							<li><i class="fa fa-list fa-2x"></i> <span>Питание в отеле на выбор</span></li>
							<li><i class="fa fa-plus fa-2x"></i> <span>Медицинская страховка</span></li>
						</ul>
					</div>
				</div>
			</div>


			<!--Результаты-->
			<div class="col-xs-12 col-md-9 tour-form-content">

				<div class="col-xs-12 progress-section" style="display: none">
					<!--Результаты поиска туров-->
					<div class="col-md-4 col-sm-9">
						<div class="progress progress-striped active" style="display: block">
							<div class="progress-bar" style="width: 0%; color:#000"></div>
						</div>
					</div>
					<div class="col-md-2 col-sm-2" id="progress-percent">0%</div>
					<div class="col-md-3 col-xs-6" id="progress-status">идет поиск</div>
					<div class="col-md-3 col-xs-6" id="tours-found">0 туров найдено</div>
				</div>

				<div id="currentPage" style="display: none">1</div>
				<div id="requestid" style="display: none">0</div>

				<div id="request" style="display: none;"></div>

				<!--Форма подбора тура-->
				<div class="text-center" id="tour-help-form-link" style="background: #00aeef; padding: 20px; color: #fff; overflow: hidden; display: none">
					Нужна помощь в подборе тура? <a href="javascript:void(0)" onclick="searchObject.showTourHelpForm()" class="btn btn-white">Оставить заявку</a>
				</div>
				<div class="col-xs-12 tours-section" id="tour-help-form-conatainer" style="display: none">
					<? $APPLICATION->IncludeComponent(
						"bitrix:form.result.new",
						"tour_help",
						Array(
							"AJAX_MODE"              => "Y",
							"SEF_MODE"               => "N",
							"WEB_FORM_ID"            => "6",
							"LIST_URL"               => "/tours",
							"EDIT_URL"               => "/tours",
							"SUCCESS_URL"            => "",
							"CHAIN_ITEM_TEXT"        => "",
							"CHAIN_ITEM_LINK"        => "",
							"IGNORE_CUSTOM_TEMPLATE" => "N",
							"USE_EXTENDED_ERRORS"    => "N",
							"CACHE_TYPE"             => "A",
							"CACHE_TIME"             => "3600",
							"VARIABLE_ALIASES"       => Array(
								"WEB_FORM_ID" => "WEB_FORM_ID",
								"RESULT_ID"   => "RESULT_ID"
							),
						),
						false
					); ?>

				</div>
				<br>
				<!--/Форма подбора тура-->

				<div class="col-md-12 tours-more-btn" style="display: none;">
					<div class="row text-center">
						<a href="javascript:void(0)" onclick="searchObject.moreTours()" class="btn btn-blue">Показать еще...</a>
					</div>
				</div>

				<? if (isset($_REQUEST["formresult"]) && $_REQUEST["formresult"] == "addok"): ?>
					<!--Результаты поиска туров-->
					<div class="tour-list-wrap" id="formok">
						<div class="tour-item">
							<div class="col-md-12 yes">
								Заявка на подбор успешно отправлена
							</div>
						</div>
					</div>
				<? endif; ?>
				<div style="clear:both;"></div>
			</div>
		</div>
	</div>
</section>

<!--Модальное окно выбора города отправления-->
<div class="modal fade" id="departureModal" tabindex="-1" role="dialog" aria-labelledby="departureModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="departureModalLabel">Выберите город вылета</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-2 popularDepartures hidden-xs hidden-sm">
						<ul class="nt-list">
							<li><a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(1, 'Москва')">Москва</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(3, 'Екатеринбург')">Екатеринбург</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(6, 'Челябинск')">Челябинск</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(28, 'Оренбург')">Оренбург</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(7, 'Самара')">Самара</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(4, 'Уфа')">Уфа</a></li>
						</ul>
					</div>
					<div class="col-md-10 allDepartures">
						<div class="row">
							<div class="list customScroll">
								<ul class="nt-list multiColumn" id="departureList">
									<li class="divider"></li>
									<li><a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(99, 'Без перелета')">Без перелета</a></li>
									<li class="divider">А</li>
									<? $first_letter = 'А'; ?>
									<? foreach ($arResult['DEPARTURE_LIST'] as $arDeparture): ?>
										<? if ($first_letter !== substr($arDeparture['UF_NAME'], 0, 1)): //Если новая буква, тогда выводим разделитель?>
											<li class="divider"><?=substr($arDeparture['UF_NAME'], 0, 1)?></li>
										<? endif; ?>
										<? $first_letter = substr($arDeparture['UF_NAME'], 0, 1); ?>
										<? if ($arDeparture['UF_XML_ID'] == 99) {
											continue;
										} //Убираем из списка, т.к. уже выведен до цикла?>
										<li>
											<a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectDeparture(<?=$arDeparture['UF_XML_ID']?>, '<?=$arDeparture['UF_NAME']?>')">
												<?=$arDeparture['UF_NAME']?>
											</a>
										</li>
									<? endforeach; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<!--Модальное окно выбора страны-->
<div class="modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="countryModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="countryModalLabel">Выберите страну</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-3 popularCountry hidden-xs">
						<h5>Популярные страны</h5>
						<ul class="nt-list">
							<? foreach ($arResult["POPULAR_COUNTRIES"] as $key => $popCountry): ?>
								<li>
									<img src="<?=CFile::GetPath($popCountry["PICTURE"])?>" alt="<?=$popCountry["NAME"]?>"/>
									<a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectCountry(<?=$key?>, '<?=$popCountry["NAME"]?>')"><?=$popCountry["NAME"]?></a>
								</li>
							<? endforeach; ?>
						</ul>
					</div>
					<div class="col-sm-9 allCountry">
						<h5>Выберите страну:</h5>
						<div class="row">
							<div class="list customScroll">
								<ul class="nt-list multiColumn" id="countryList">
									<? foreach ($arResult['COUNTRY_LIST'] as $arCountry): ?>
										<li>
											<a href="javascript:void(0)" rel="nofollow" onclick="searchObject.selectCountry(<?=$arCountry['UF_XML_ID']?>, '<?=$arCountry['UF_NAME']?>')">
												<?=$arCountry['UF_NAME']?>
											</a>
										</li>
									<? endforeach; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<!-- Detail Tour Modal -->
<div class="modal fade" id="tourActulizeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div style="height: 300px">
				<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
				<span class="sr-only">Loading...</span>
			</div>
		</div>
	</div>
</div>

<script>
	var helpformmessage, userurl;
	var searchObject = new JSNewtravelSearch(<? echo CUtil::PhpToJSObject($arResult['REQUEST_PARAMS'], false, true); ?>);
</script>