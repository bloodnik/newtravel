<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var $this CBitrixComponentTemplate */
$this->addExternalCss(SITE_TEMPLATE_PATH . "/js/daterangepicker2/daterangepicker.min.css");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/daterangepicker2/jquery.daterangepicker.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jQuery.AutoColumnList/jquery.autocolumnlist.min.js");

//Выбор ночей
$this->addExternalCss("/local/modules/newtravel.search/js/jquery.nights.select/jquery.nights.select.min.css");
$this->addExternalJs("/local/modules/newtravel.search/js/jquery.nights.select/jquery.nights.select.min.js");
//Выбор туристов
$this->addExternalCss("/local/modules/newtravel.search/js/jquery.tourists.select/jquery.tourists.select.min.css");
$this->addExternalJs("/local/modules/newtravel.search/js/jquery.tourists.select/jquery.tourists.select.min.js");

//Подтверждение города
$this->addExternalCss("/local/modules/newtravel.search/js/jquery.confirm.city/jquery.confirm.city.css");
$this->addExternalJs("/local/modules/newtravel.search/js/jquery.confirm.city/jquery.confirm.city.js");

?>
<!--noindex -->
<div class="simple-search-form">
	<form action="/tours">
		<div id="departure_frame">
			<? $frame = $this->createFrame('departure_frame')->begin() ?>
			<div class="form-group departure_wrap" data-container="body">
				<input type="hidden" name="departure" id="departure" value="<?=$arResult['CURRENT_DEPARTURE']['UF_XML_ID']?>"/>
				<label>Смотреть туры из города:</label>
				<span id="departureValue" data-toggle="modal" data-target="#departureModal">
					<?=$arResult['CURRENT_DEPARTURE']['UF_NAME']?>
				</span>
			</div>
			<? $frame->beginStub(); ?>
			<div class="form-group">
				<input type="hidden" name="departure" id="departure" value=""/>
				<label>Смотреть туры из города:</label>
				<span id="departureValue" data-toggle="modal" data-target="#departureModal">
					Загрузка...
				</span>
			</div>
			<? $frame->end(); ?>
		</div>

		<div class="form-group">
			<div class="row">
				<input type="hidden" name="country" id="country" value="<?=$arResult['CURRENT_COUNTRY']['UF_XML_ID']?>"/>
				<div class="col-md-5">
					<label>Куда поедем?: </label>
				</div>
				<div class="col-md-7">
					<div class="form-control pseudo-input" id="countryValue" data-toggle="modal" data-target="#countryModal">
						<?=$arResult['CURRENT_COUNTRY']['UF_NAME']?>
					</div>
				</div>
			</div>
		</div>

		<div id="daterange_frame">
			<? $frame = $this->createFrame('daterange_frame')->begin() ?>
			<div class="form-group">
				<div class="row">
					<div class="col-md-5">
						<label>Даты вылета: </label>
					</div>
					<div class="col-md-7">
						<input type="text" class="form-control" id="daterange" name="daterange" value="<?=$arResult['CONFIG']['DATE_RANGE']?>"/>
						<input type="hidden" class="form-control" id="datefrom" name="datefrom" value="<?=$arResult['CONFIG']['DATE_FROM']?>"/>
						<input type="hidden" class="form-control" id="dateto" name="dateto" value="<?=$arResult['CONFIG']['DATE_TO']?>"/>
					</div>
				</div>
			</div>
			<? $frame->beginStub(); ?>
			<div class="form-group">
				<div class="row">
					<div class="col-md-5">
						<label>Даты вылета: </label>
					</div>
					<div class="col-md-7">
						<input type="text" class="form-control" id="daterange" name="daterange" value=""/>
						<input type="hidden" class="form-control" id="datefrom" name="datefrom" value=""/>
						<input type="hidden" class="form-control" id="dateto" name="dateto" value=""/>
					</div>
				</div>
			</div>
			<? $frame->end(); ?>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-md-5">
					<label>Ночей: </label>
				</div>
				<div class="col-md-7">
					<div class="form-control pseudo-input" id="nightsValue">от <?=$arParams["NIGHTSFROM"]?> до <?=$arParams["NIGHTSTO"]?></div>
				</div>
			</div>
		</div>

		<div class="toursit-select form-group">
			<div class="row">
				<div class="col-md-5">
					<label>Туристы: </label>
				</div>
				<div class="col-md-7">
					<div class="form-control tourists-total"><?=$arResult['CONFIG']['ADULTS']?> взр.</div>
				</div>
			</div>
		</div>

		<div class="form-group">
			<input type="hidden" name="start" id="start" value="Y"/>
			<button type="submit" class="btn btn-blue" style="width: 100%">Поехали, Жми!</button>
		</div>
	</form>
	<div class="row">
		<div class="col-md-12 text-center"><a href="/tours" class="small">подробная форма поиска</a></div>
	</div>
</div>



<!--Модальное окно выбора города отправления-->
<div class="modal fade" id="departureModal" tabindex="-1" role="dialog" aria-labelledby="departureModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="departureModalLabel">Выберите город вылета</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3 hidden-xs hidden-sm popularDepartures">
						<ul class="nt-list">
							<li><a href="javascript:void(0)" rel="nofollow" onclick="selectDeparture(1, 'Москва')">Москва</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="selectDeparture(3, 'Екатеринбург')">Екатеринбург</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="selectDeparture(6, 'Челябинск')">Челябинск</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="selectDeparture(28, 'Оренбург')">Оренбург</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="selectDeparture(7, 'Самара')">Самара</a></li>
							<li><a href="javascript:void(0)" rel="nofollow" onclick="selectDeparture(4, 'Уфа')">Уфа</a></li>
						</ul>
					</div>
					<div class="col-md-9 allDepartures">
						<div class="list customScroll">
							<ul class="nt-list multiColumn" id="departureList">
								<li class="divider"></li>
								<li><a href="javascript:void(0)" rel="nofollow" onclick="selectDeparture(99, 'Без перелета')">Без перелета</a></li>
								<li class="divider">А</li>
								<? $first_letter = 'А'; ?>
								<? foreach ($arResult['DEPARTURE_LIST'] as $arDeparture): ?>
									<? if ($first_letter !== substr($arDeparture['UF_NAME'], 0, 2)): //Если новая буква, тогда выводим разделитель?>
										<li class="divider"><?=substr($arDeparture['UF_NAME'], 0, 2)?></li>
									<? endif; ?>
									<? $first_letter = substr($arDeparture['UF_NAME'], 0, 2); ?>
									<? if ($arDeparture['UF_XML_ID'] == 99) {
										continue;
									} //Убираем из списка, т.к. уже выведен до цикла?>
									<li>
										<a href="javascript:void(0)" rel="nofollow" onclick="selectDeparture(<?=$arDeparture['UF_XML_ID']?>, '<?=$arDeparture['UF_NAME']?>')">
											<?=$arDeparture['UF_NAME']?>
										</a>
									</li>
								<? endforeach; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<!--Модальное окно выбора страны-->
<div class="modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="countryModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="countryModalLabel">Выберите страну</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3 hidden-xs popularCountry">
						<h5>Популярные страны</h5>
						<ul class="nt-list">
							<? foreach ($arResult["POPULAR_COUNTRIES"] as $key => $popCountry): ?>
								<li>
									<img src="<?=CFile::GetPath($popCountry["PICTURE"])?>" alt="<?=$popCountry["NAME"]?>"/>
									<a href="javascript:void(0)" rel="nofollow" onclick="selectCountry(<?=$key?>, '<?=$popCountry["NAME"]?>')"><?=$popCountry["NAME"]?></a>
								</li>
							<? endforeach; ?>
						</ul>
					</div>
					<div class="col-md-9 allCountry">
						<h5>Выберите страну:</h5>
						<div class="list customScroll">
							<ul class="nt-list multiColumn" id="countryList">
								<? foreach ($arResult['COUNTRY_LIST'] as $arCountry): ?>
									<li>
										<a href="javascript:void(0)" rel="nofollow" onclick="selectCountry(<?=$arCountry['UF_XML_ID']?>, '<?=$arCountry['UF_NAME']?>')">
											<?=$arCountry['UF_NAME']?>
										</a>
									</li>
								<? endforeach; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<!--/noindex-->

<script>
	$('.departure_wrap').confirmCity();

	$('#nightsValue').nightsSelect({
		defaultFrom: <?=$arParams["NIGHTSFROM"]?>,
		defaultTo: <?=$arParams["NIGHTSTO"]?>
	});
	$('.tourists-total').touristSelect({
		adults: <?=$arParams["ADULTS"]?>
	});

	var helpformmessage = "";
	var userurl = "";

</script>