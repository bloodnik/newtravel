$(document).ready(function () {

    //С задержкой обновляем список стран под город вылета
    setTimeout(function () {
        getCountries($("#departure").val());
    }, 500);


    /* ==========================================================================
     Разбивка списка на колонки
     ========================================================================== */
    $('.multiColumn').autocolumnlist({columns: 4});
    /*=========================================================================
     mCustomScrollBar
     ========================================================================== */
    $(".customScroll").mCustomScrollbar();

    /**
     * Выбор диапазона дат
     */
    $('input[name="daterange"]').dateRangePicker({
        format: 'DD.MM.YYYY',
        startOfWeek: 'monday',
        singleMonth: 'auto',
        showShortcuts: false,
        autoClose: true,
        selectForward: true,
        stickyMonths: true,
        separator: '-',
        minDays: 1,
        maxDays: 14
    }).bind('datepicker-change', function (event, obj) {
        $("#datefrom").val(moment(obj.date1).format("DD.MM.YYYY"));
        $("#dateto").val(moment(obj.date2).format("DD.MM.YYYY"));
        $("#daterange").val($("#datefrom").val() + "-" + $("#dateto").val());

        console.log("sdfsdf");
    });
});


/*==============Основные Функции ============================================================================*/

//Событие при смене города отправления
function selectDeparture(id, name) {
    $("#departure").val(id);
    $("#departureValue").text(name);
    $("#departureModal").modal('hide');
    $.cookie('NEWTRAVEL_USER_CITY', id, {path: '/'}); //записываем в куки
    getCountries(id);

    getFlydates(id, $("#country").val());
}

//Событие при смене страны
function selectCountry(id, name) {
    $("#country").val(id);
    $("#countryValue").text(name);
    $("#countryModal").modal('hide');

    getFlydates($("#departure").val(), id);
}

//Получаем список стран. Если выбран город вылета то задействован параметр cndep
function getCountries(cndep) {
    $.getJSON('/local/tools/newtravel.search.ajax.php', {type: "country", cndep: cndep}, function (json) {
        $('#countryList').empty();

        $.each(json.lists.countries.country, function (i, country) {
            $('#countryList').append($('<li><a href="javascript:void(0)" onclick="selectCountry(' + country.id + ', \'' + country.name + '\')">' + country.name + '</a> </li>'));
        });
    });
    /* ==========================================================================
     Разбивка списка на колонки
     ========================================================================== */
    setTimeout(function () {
        $('.multiColumn').autocolumnlist({columns: 4});
    }, 600);
    getFlydates(cndep, $("#country").val());
}

//Получаем спиоск дат вылета
function getFlydates(flydeparture, flycountry) {
    $.getJSON('/local/tools/newtravel.search.ajax.php', {type: "flydate", flydeparture: flydeparture, flycountry: flycountry}, function (json) {

        $('#daterange').data('dateRangePicker').destroy();

        $('#daterange').dateRangePicker({
            format: 'DD.MM.YYYY',
            startOfWeek: 'monday',
            singleMonth: 'auto',
            showShortcuts: false,
            autoClose: true,
            selectForward: true,
            stickyMonths: true,
            separator: '-',
            minDays: 1,
            maxDays: 14,
            beforeShowDay: function (date) {
                if (json.lists !== undefined && json.lists.flydates !== null) {
                    if ($.inArray(moment(date).format("DD.MM.YYYY"), $.makeArray(json.lists.flydates.flydate)) >= 0) {
                        return [true, "in-flydate", ""];
                    } else {
                        return [true, "", ""];
                    }
                } else {
                    return [true, "", ""];
                }
            }
        }).bind('datepicker-change', function (event, obj) {
            $("#datefrom").val(moment(obj.date1).format("DD.MM.YYYY"));
            $("#dateto").val(moment(obj.date2).format("DD.MM.YYYY"));
            $("#daterange").val($("#datefrom").val() + "-" + $("#dateto").val());
        });
    });
}