<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Newtravel\Search\TourvisorHlLists;
use Newtravel\Search\DefineGeo;
use Bitrix\Main\Loader;

class CSimpleSearch extends CBitrixComponent {

	//Объявлеяем переменные

	//Объявлеяем функции

	//Запускаем компонент
	public function executeComponent() {
		global $APPLICATION;
		Loader::includeModule("newtravel.search");

		$this->arResult['CONFIG']['DATE_FROM']  = date("d.m.Y", strtotime("1 DAY"));
		$this->arResult['CONFIG']['DATE_TO']    = date("d.m.Y", strtotime("+" . $this->arParams["DATE_TO"] . " DAY"));
		$this->arResult['CONFIG']['DATE_RANGE'] = $this->arResult['CONFIG']['DATE_FROM'] . "-" . $this->arResult['CONFIG']['DATE_TO'];
		$this->arResult['CONFIG']['ADULTS']     = isset($this->arParams["ADULTS"]) ? $this->arParams["ADULTS"] : 2;

		$cache_id = $this->arResult['CONFIG']['DATE_RANGE'];
		if ($this->arParams['USE_GEO_DEFINE'] == "Y") {
			DefineGeo::setRegionIso();
			$cache_id .= $_SESSION['REGION_ISO'];
		}

		$USER_CITY = $APPLICATION->get_cookie('USER_CITY');
		$cache_id .= $USER_CITY;

		if ($this->startResultCache($this->arParams['CACHE_TIME'], $cache_id)) {

			//Получаем города вылета
			$this->arResult['DEPARTURE_LIST'] = TourvisorHlLists::getList('departures');

			if (isset($USER_CITY) && !empty($USER_CITY)) { //Определена ли кука с ID города
				foreach ($this->arResult['DEPARTURE_LIST'] as $arDeparture) {
					if ($arDeparture["UF_XML_ID"] == $USER_CITY) {
						$this->arResult['CURRENT_DEPARTURE'] = $arDeparture;
						break;
					}
				}
			} else if ($this->arParams['USE_GEO_DEFINE'] == "Y") {//Определяем текущий город
				Loader::includeModule('newtravel.search');
				$this->arResult['CURRENT_DEPARTURE'] = DefineGeo::getCity();
			} else {
				if (isset($this->arParams['CITY']) && ! empty($this->arParams['CITY'])) {
					foreach ($this->arResult['DEPARTURE_LIST'] as $arDeparture) {
						if ($arDeparture["UF_XML_ID"] == $this->arParams['CITY']) {
							$this->arResult['CURRENT_DEPARTURE'] = $arDeparture;
							break;
						}
					}
				}
			}
			
			//Получаем страны
			$this->arResult['COUNTRY_LIST'] = TourvisorHlLists::getList('countries');

			//Определяем текущую страну
			if (isset($this->arParams['COUNTRY']) && ! empty($this->arParams['COUNTRY'])) {
				foreach ($this->arResult['COUNTRY_LIST'] as $arCountry) {
					if ($arCountry["UF_XML_ID"] == $this->arParams['COUNTRY']) {
						$this->arResult['CURRENT_COUNTRY'] = $arCountry;
						break;
					}
				}
			}


			//=======Получаем популярные страны========/
			$arSelect = Array("ID", "NAME", "CODE", "PREVIEW_PICTURE");
			$arFilter = Array("IBLOCK_CODE" => "popular_countries", "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$res      = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array(), $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields                                                 = $ob->GetFields();
				$this->arResult["POPULAR_COUNTRIES"][ $arFields["CODE"] ] = array("NAME" => $arFields["NAME"], "PICTURE" => $arFields["PREVIEW_PICTURE"]);
			}

			$this->includeComponentTemplate();
		}
	}
}

;
?>