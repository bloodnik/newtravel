<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var $this CBitrixComponentTemplate */
$this->setFrameMode(true);

$this->addExternalCss(SITE_TEMPLATE_PATH . "/js/bootstrap-multiselect/css/bootstrap-multiselect.css");
$this->addExternalCss(SITE_TEMPLATE_PATH . "/js/footable/footable.core.min.css");
$this->addExternalCss(SITE_TEMPLATE_PATH . "/js/daterangepicker2/daterangepicker.min.css");

$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/daterangepicker2/jquery.daterangepicker.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/bootstrap-multiselect/js/bootstrap-multiselect.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jQuery.AutoColumnList/jquery.autocolumnlist.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/footable/footable.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/plugins/jquery.isotope.js");
$this->addExternalJs("https://maps.googleapis.com/maps/api/js?key=AIzaSyAKYVzO5rPM1Gqq8crmt9Bku0uvKhr7t4g&sensor=true");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/plugins/gmaps.min.js");

//Выбор ночей
$this->addExternalCss("/local/modules/newtravel.search/js/jquery.nights.select/jquery.nights.select.min.css");
$this->addExternalJs("/local/modules/newtravel.search/js/jquery.nights.select/jquery.nights.select.min.js");
//Выбор туристов
$this->addExternalCss("/local/modules/newtravel.search/js/jquery.tourists.select/jquery.tourists.select.min.css");
$this->addExternalJs("/local/modules/newtravel.search/js/jquery.tourists.select/jquery.tourists.select.min.js");
//Подтверждение города
$this->addExternalCss("/local/modules/newtravel.search/js/jquery.confirm.city/jquery.confirm.city.css");
$this->addExternalJs("/local/modules/newtravel.search/js/jquery.confirm.city/jquery.confirm.city.js");

$arProps = $arResult["hotel"]['PROPS'];

?>
<section class="white-section top-section">
	<div class="tabs-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-7 col-sm-12">
					<div class="hotel-title">
						<span class="hotel-stars">
							<?
							switch ($arProps['STARS']['VALUE']) {
								case "1":
									echo '<i class="icon icon-star-filled"></i>';
									break;
								case "2":
									echo '<i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i>';
									break;
								case "3":
									echo '<i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i>';
									break;
								case "4":
									echo '<i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i>';
									break;
								case "5":
									echo '<i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i><i class="icon icon-star-filled"></i>';
									break;
								default:
									echo "";
									break;
							}
							?>
						</span>
						<span class="links">
						    <?=$arProps["COUNTRY"]['VALUE']?>, <?=$arProps["REGION"]['VALUE']?>
						</span>
					</div>
				</div>

				<div class="col-lg-4 col-md-7 col-sm-12">
					<div class="hotel-info">
						<span class="rating text-success">
							Рейтинг: <span class="badge badge-info"><?=$arProps["RATING"]['VALUE']?> </span>/<span class="badge badge-success">5</span>
						</span>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs">
						<? if (count($arProps['MORE_PHOTO']['VALUE']) > 0): ?>
							<li class="active"><a href="#tab-photo" rel="nofollow" data-toggle="tab"><i class="icon icon-picture hidden-xs"></i>Фотографии</a></li>
						<? endif; ?>
						<? if ($arProps['LAT']['VALUE'] && $arProps['LONG']['VALUE']): ?>
							<li><a href="#tab-map" rel="nofollow" id="gmap" data-toggle="tab"><i class="icon icon-location hidden-xs"></i>Карта</a></li>
						<? endif; ?>
						<li><a href="javascript:void(0)" rel="nofollow" id="go_to_search_form"><i class="icon icon-flight hidden-xs"></i>Туры в отель</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div id="map-photos" class="tab-content">
			<? if (count($arProps['MORE_PHOTO']['VALUE']) > 0): ?>
				<div class="tab-pane fade in active" id="tab-photo">
					<div class="hotel-photos" style="position: relative">
						<ul class="carousel">
							<? foreach ($arProps['MORE_PHOTO']['SRC'] as $image): ?>
								<li>
									<a class="carousel-item" href="<?=$image?>"><img src="<?=$image?>" height="220" alt="<?=$arResult["hotel"]["NAME"]?>"></a>
								</li>
							<? endforeach; ?>
						</ul>
						<a class="caroufredsel-prev" href="#" style="display: block;"><span> <i class="fa fa-chevron-left"></i> </span></a>
						<a class="caroufredsel-next" href="#" style="display: block;"><span> <i class="fa fa-chevron-right"></i> </span></a>
					</div>
				</div>
			<? endif; ?>
			<? if ($arProps['LAT']['VALUE'] && $arProps['LONG']['VALUE']): ?>
				<div class="tab-pane" id="tab-map">
					<div id="mapintab" style="height: 350px;"></div>
					<script>
						$(document).on('shown.bs.tab', 'a[href="#tab-map"]', function (e) {
							mapintab = new GMaps({
								el: '#mapintab',
								//height: 322,
								scrollwheel: false,
								lat: <?=isset($arProps['LAT']['VALUE']) ? $arProps['LAT']['VALUE'] : "0"?>,
								lng: <?=isset($arProps['LONG']['VALUE']) ? $arProps['LONG']['VALUE'] : "0"?>
							});

							mapintab.addMarker({
								lat: <?=isset($arProps['LAT']['VALUE']) ? $arProps['LAT']['VALUE'] : "0"?>,
								lng: <?=isset($arProps['LONG']['VALUE']) ? $arProps['LONG']['VALUE'] : "0"?>,
								clickable: false,
								icon: "<?=SITE_TEMPLATE_PATH?>/images/mappin.png"
							});
						});
					</script>
				</div>
			<? endif; ?>
		</div>
	</div>
</section>

<div id="hotel_search_frame">
	<? $frame = $this->createFrame('hotel_search_frame')->begin("Загрузка") ?>
	<!--Поиск тура-->
	<? $APPLICATION->IncludeComponent(
		"newtravel.search:hotel.search",
		".default",
		array(
			"ADULTS"               => "2",
			"CACHE_TIME"           => "36000000",
			"CACHE_TYPE"           => "A",
			"CITY"                 => "4",
			"COUNTRY"              => $arProps["COUNTRYCODE"]['VALUE'],
			"COMPOSITE_FRAME_MODE" => "Y",
			"COMPOSITE_FRAME_TYPE" => "DYNAMIC_WITH_STUB_LOADING",
			"DATE_TO"              => "60",
			"NIGHTSFROM"           => "6",
			"NIGHTSTO"             => "14",
			"HOTEL"                => $arResult["HOTELCODE"],
			"USE_GEO_DEFINE"       => "Y",
			"COMPONENT_TEMPLATE"   => ".default",
			"STARS"                => "3"
		),
		false
	); ?>
	<? $frame->end(); ?>
</div>

<section class="white-section">
	<div class="container" id="tour-search-anchor">
		<div class="hotel-details">
			<div class="row">
				<div class="col-md-4">
					<div class="hotel-spec-item i-contacts">
						<h3>Контакты</h3>
						<p><span>Телефон:</span> <?=$arProps["PHOTE"]['VALUE']?></p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="hotel-spec-item i-info">
						<h3>Кратко об отеле</h3>
						<? if (isset($arProps["BUILD"]['VALUE'])): ?>
							<p><span>Год постройки:</span> <?=$arProps["BUILD"]['~VALUE']?></p>
						<? endif; ?>
						<? if (isset($arProps["REPAIR"]['VALUE'])): ?>
							<p><span>Последний ремонт:</span> <?=$arProps["REPAIR"]['~VALUE']?></p>
						<? endif; ?>
						<? if (isset($arProps["PLACEMENT"]['VALUE'])): ?>
							<p><span>Расположение:</span><?=$arProps["PLACEMENT"]['~VALUE']?></p>
						<? endif; ?>
					</div>
				</div>
				<div class="col-md-4 beach">
					<div class="hotel-spec-item i-beach">
						<h3>Пляж</h3>
						<?=$arProps["BEACH"]['~VALUE']?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- =========================================
White Section ( hotel detailed + responses )
========================================== -->
<section class="white-section">
	<div class="tabs-box">

		<div class="container">
			<div class="row">

				<div class="col-md-12">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-services" rel="nofollow" data-toggle="tab"><i class="icon icon-cogs hidden-xs"></i>Услуги отеля</a></li>
						<li><a href="#tab-info" rel="nofollow" data-toggle="tab"><i class="icon icon-info hidden-xs"></i>Подробное описание</a></li>
					</ul>
				</div><!-- /col-md-12 -->

			</div><!-- /row -->
		</div><!-- /container -->

		<div class="tab-content">
			<div class="tab-pane fade in active" id="tab-services">
				<div class="container">
					<? foreach ($arResult["SERVICES"] as $arRow): ?>
						<div class="row thumbnail">
							<? foreach ($arRow as $arItem): ?>
								<div class="col-sm-12 col-md-4 hotel-spec-item post-thumbnail">
									<h3><?=$arItem['NAME']?></h3>
									<i class="fa fa-<?=$arItem['ICON']?>"></i>
									<? foreach ($arItem['ITEMS'] as $arService): ?>
										<div><?=$arService?></div>
									<? endforeach; ?>
								</div>
							<? endforeach; ?>
						</div>
					<? endforeach; ?>
				</div>
			</div><!-- /tab-pane -->

			<div class="tab-pane fade in" id="tab-info">
				<div class="container">
					<? if (! empty($arResult['hotel']["PREVIEW_TEXT"])): ?>
						<p><?=$arResult['hotel']["~PREVIEW_TEXT"]?></p>
					<? else: ?>
						<p>Описание отеля недоступно</p>
					<? endif ?>
				</div>
			</div><!-- /tab-pane -->

		</div><!-- /tab-content -->

	</div><!-- /tabs-box -->
</section>