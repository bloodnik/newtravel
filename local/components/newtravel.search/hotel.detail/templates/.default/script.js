(function (window) {

    if (!!window.JSNewtravelSearch) {
        return;
    }

    window.JSNewtravelSearch = function (arParams) {

        this.params = arParams;
        this.params.searchIsFinish = false;
        this.params.nfrom = 0;
        this.params.nto = 0;
        this.params.urlParams = ""; //ссылка пользователя на поиск
        this.params.searchUrl = "/local/tools/newtravel.search.ajax.php"; //ajax ссылка на получение данных
        this.params.resultUrl = "/local/tools/newtravel.search.ajaxresult.php"; //ajax ссылка на получение данных
        this.helpformmessage = ""; //Генерируемое сообщение для формы подбора тура
        this.userurl = ""; //Ссылка пользователеля для формы поддбора тура
        
        this.visual = {
            departure: $("#departure"),
            stars: $("#stars"),
            rating: $("#rating"),
            priceMin: $("#price-min"),
            priceMax: $("#price-max"),
            priceRange: $("#priceRange"),
            nightsCell: $("#nights-select table tr>td"),
            nightsText: $("#nights-select>strong"),

            requestId: $("#requestid"),
            daterange: $('input[name="daterange"]')
        };
        this.Init();
    };

    //Инициализация
    window.JSNewtravelSearch.prototype.Init = function () {
        var obThis = this;
        var visual = this.visual;
        var params = this.params;

        $('.multiply').multiselect({
            enableFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 200,
            buttonWidth: '195px',
            buttonClass: 'pseudo-input',
            allSelectedText: 'Выбраны все туроперторы',
            selectAllText: 'Выбрать все!',
            filterPlaceholder: 'Поиск...',
            buttonText: function (options, select) {
                if (options.length === 0) {
                    return 'Выберите туроператора...';
                }
                else if (options.length > 3) {
                    return 'Выбрано ' + options.length;
                }
                else {
                    var labels = [];
                    options.each(function () {
                        if ($(this).attr('label') !== undefined) {
                            labels.push($(this).attr('label'));
                        }
                        else {
                            labels.push($(this).html());
                        }
                    });
                    return labels.join(', ') + '';
                }
            },
            onChange: function (option) {
                var checkedOperators = $("#operators option:selected");
                var operators = "";
                $.each(checkedOperators, function (i, operator) {
                    operators += operator.value + ",";
                });
                params.OPERATORS = operators.substring(0, operators.length - 1);
            },
            onSelectAll: function () {
                var checkedOperators = $("#operators option:selected");
                var operators = "";
                $.each(checkedOperators, function (i, operator) {
                    operators += operator.value + ",";
                });
                params.OPERATORS = operators.substring(0, operators.length - 1);
            },
            onDeselectAll: function () {
                var checkedOperators = $("#operators option:selected");
                var operators = "";
                $.each(checkedOperators, function (i, operator) {
                    operators += operator.value + ",";
                });
                params.OPERATORS = operators.substring(0, operators.length - 1);
            }
        });

        //Подтвреждение города
        $('.departure_wrap').confirmCity();

        setTimeout(function () {
            $('.multiColumn').autocolumnlist({columns: 4});

            //mCustomScrollBar
            $(".customScroll").mCustomScrollbar();

            //==============================================Выбор диапазона дат==========================================//
            $('input[name="daterange"]').dateRangePicker({
                format: 'DD.MM.YYYY',
                startOfWeek: 'monday',
                singleMonth: 'auto',
                showShortcuts: false,
                stickyMonths: true,
                autoClose: true,
                selectForward: true,
                separator: '-',
                minDays: 1,
                maxDays: 14
            }).bind('datepicker-change', function (event, obj) {
                params.DATEFROM = moment(obj.date1).format("DD.MM.YYYY");
                params.DATETO = moment(obj.date2).format("DD.MM.YYYY");
                params.DATERANGE = params.DATEFROM + " - " + params.DATETO;
            }).bind('datepicker-open', function () {
                $(".daterange_input").removeClass('has-warning').tooltip('hide');
            });
        }, 1500);

        //==================================================Выбор ночей===========================================//
        $('#nightsValue').nightsSelect({
            defaultFrom: params.NIGHTSFROM,
            defaultTo: params.NIGHTSTO
        }).bind('nightSelect-change', function (event, obj) {
            params.NIGHTSFROM = obj.from;
            params.NIGHTSTO = obj.to;
        });
        //===============================Выбор туристов===============================================//
        $('.tourists-total').touristSelect({
            adults: params.ADULTS,
            child: params.CHILD,
            childage1: params.CHILDAGE1,
            childage2: params.CHILDAGE2,
            childage3: params.CHILDAGE3
        }).bind('touristsSelect-change', function (event, obj) {
            params.ADULTS = obj.adults;
            params.CHILD = obj.child;
            params.CHILDAGE1 = obj.childage1;
            params.CHILDAGE2 = obj.childage2;
            params.CHILDAGE3 = obj.childage3;
        });

        //==============================================Событие при смене типа питания====================================//
        $("#meal").on('change', function () {
            params.MEAL = $(this).val();
        });

        if (params.START == "Y") { //Стартуем поиск
            setTimeout(this.searchTours(), 1500);
        }

    };

    //Событие при смене города отправления из модального окна
    window.JSNewtravelSearch.prototype.selectDeparture = function (id, name) {
        this.params.DEPARTURE = id;
        $("#departureValue").text(name);
        $("#departureModal").modal('hide');
        $.cookie('NEWTRAVEL_USER_CITY', id, { path: '/' }); //записываем в куки
        this.getFlydates();
    };

    //Получаем спиоск дат вылета
    window.JSNewtravelSearch.prototype.getFlydates = function () {
        var params = this.params;
        var visual = this.visual;
        $.getJSON(params.searchUrl, {type: "flydate", flydeparture: params.DEPARTURE, flycountry: params.COUNTRY}, function (json) {
            if (json.lists.flydates !== null) {
                $('input[name="daterange"]').data('dateRangePicker').destroy();
                $('input[name="daterange"]').dateRangePicker({
                    format: 'DD.MM.YYYY',
                    startOfWeek: 'monday',
                    singleMonth: 'auto',
                    showShortcuts: false,
                    autoClose: true,
                    selectForward: true,
                    separator: '-',
                    minDays: 1,
                    maxDays: 14,
                    beforeShowDay: function (date) {
                        if ($.inArray(moment(date).format("DD.MM.YYYY"), $.makeArray(json.lists.flydates.flydate)) >= 0) {
                            return [true, "in-flydate", ""];
                        } else {
                            return [true, "", ""];
                        }
                    }
                }).bind('datepicker-change', function (event, obj) {
                    params.DATEFROM = moment(obj.date1).format("DD.MM.YYYY");
                    params.DATETO = moment(obj.date2).format("DD.MM.YYYY");
                    params.DATERANGE = params.DATEFROM + " - " + params.DATETO;
                }).bind('datepicker-open', function () {
                    $(".daterange_input").removeClass('has-warning').tooltip('hide');
                });
            }
        });
    };

    /*
     * =============================================================
     * ПОИСК ТУРА
     * =============================================================
     * */
    window.JSNewtravelSearch.prototype.searchTours = function () {
        //Устанавливаем начальные значения видимых объектов
        $(".progress-section").slideDown(200);
        $(".progress-bar").css("width", "0%");
        $("#progress-percent").text("0%");
        $("#tours-found").text("0 туров найдено");
        $("#progress-status").text("идет поиск");

        $("#formok").css('display', 'none'); //Скрываем статус о успешной отправки заявки на подбор
        $('.show-all').addClass('hidden');

        $("#request").css('display', 'none');
        $('body').scrollTo('#request');

        this.makeRequest();
    };

    //Делаем запрос на получение requestId
    window.JSNewtravelSearch.prototype.makeRequest = function () {
        var obThis = this;
        var params = this.params;

        var request_params = {
            type: "search",
            departure: params.DEPARTURE,
            country: params.COUNTRY,
            datefrom: params.DATEFROM,
            dateto: params.DATETO,
            nightsfrom: params.NIGHTSFROM,
            nightsto: params.NIGHTSTO,
            adults: params.ADULTS,
            child: params.CHILD,
            childage1: params.CHILDAGE1,
            childage2: params.CHILDAGE2,
            childage3: params.CHILDAGE3,
            meal: params.MEAL,
            pricefrom: params.PRICEFROM,
            priceto: params.PRICETO,
            hotels: params.HOTELS,
            operators: params.OPERATORS,
            hotelpage: true
        };

        //Формируем строку для формы подбора тура, если туров не будет найдено
        params.helpformmessage = "Из города: " + $.trim($("#departureValue").text()) + "\n";
        params.helpformmessage += "Куда: " + $.trim($(".hotel-title h1").text()) + "\n";
        params.helpformmessage += "Кто едет: " + params.ADULTS + " взрослых, " + params.CHILD + " детей" + "\n";
        params.helpformmessage += "Дата вылета: с " + params.DATEFROM + " по " + params.DATETO + "\n";
        params.helpformmessage += "Продолжительность: " + params.NIGHTSFROM + " - " + params.NIGHTSTO + " ночей \n";

        $.getJSON(params.searchUrl, request_params, function (json) {
            //Меняем url браузера
            obThis.urlParams = json.url;
            history.pushState(json.result.requestid, "Поиск туров", "?start=Y&" + obThis.urlParams);
            params.userurl = window.location.href; //Параметр со ссылкой по которой искал пользователь(подставляется в скрытое поле в фомре помощи подбора, елси ничего не найдено)

            params.REQUEST_ID = json.result.requestid;

            obThis.checkTourStatus();
        });

        $("#help-form-message").val(params.helpformmessage); //Подставляем сообщение в форму подбора
    };
    //Проверяем статус поиска туров, елси finished, тогда выводим результаты
    window.JSNewtravelSearch.prototype.checkTourStatus = function () {
        var obThis = this;
        var params = this.params;

        $.getJSON(params.searchUrl, {type: "status", requestid: params.REQUEST_ID}, function (json) {
            if (json.data.status.state != "finished") {
                $(".progress-bar").css("width", json.data.status.progress + "%");
                setTimeout(function () {
                    obThis.checkTourStatus();
                }, 3000);
                $("#progress-percent").text(json.data.status.progress + "%");

                if (json.data.status.timepassed >= 7 && json.data.status.timepassed <= 9) {
                    obThis.getToursData();
                    $("#tours-found").text(json.data.status.toursfound + " туров найдено");
                }
            } else {
                params.searchIsFinish = true;
                $(".progress-bar").css("width", json.data.status.progress + "%");
                $("#progress-percent").text(json.data.status.progress + "%");
                $("#tours-found").text(json.data.status.toursfound + " туров найдено");
                obThis.getToursData();
            }
        });
    };

    //Получаем данные по турам
    window.JSNewtravelSearch.prototype.getToursData = function () {
        var params = this.params;
        var obThis = this;

        $.get(params.resultUrl, {requestid: params.REQUEST_ID, page: params.currentPage, isfinish: params.searchIsFinish, template: "hotel"}, function (data) {
            if (params.searchIsFinish) {
                $("#progress-percent").text("100%");
                $("#progress-status").text("поиск завершен");
                $(".progress-section").slideUp(200);

                $("#request").css('display', 'block').html(data);
                $('#requestid').html(requestid);
                $('.show-all').removeClass('hidden');

                return false;
            } else {
                $("#request").css('display', 'block').html(data);
            }
        })
    };

    //Подробная информация по туру
    window.JSNewtravelSearch.prototype.actualizeTour = function (tourid) {
        $.getJSON('/bitrix/tools/tourvisor.travel_ajax.php', {type: "actualize", tourid: tourid}, function (json) {
        });
    };

    window.JSNewtravelSearch.prototype.buyTour = function (tourid) {
        var params = this.params;
        $("body").css("cursor", "wait");
        $.get(params.resultUrl, {requestid: params.REQUEST_ID, tourid: tourid, template: "hotel"}, function (data) {
            $('#request').html(data);
            $("body").css("cursor", "default");
        })
    };
})(window);


/********************************************** ОБЩИЕ ФУНКЦИИ *******************************************************/
$(document).ready(function () {

    $("#go_to_search_form").on('click', function () {
        $.scrollTo('#hotel_search_frame', 200, {offset: -100});
        setTimeout(function () {
            $(".daterange_input").addClass('has-warning').tooltip('show');
        }, 500);
        setTimeout(function () {
            $(".adults_input").addClass('has-warning').tooltip('show');
        }, 1000);
    });

    var imgshinevar, map, mapintab, photosintab, $activetab;

    /* ==========================================================================
     Bootstrap Tabs (Map + Photos)
     ========================================================================== */
    function createHotelsCarousel(id) {
        var element = $(id);
        var result;

        element.imagesLoaded(function (instance) {

            result = element.carouFredSel({
                circular: true,
                prev: ".caroufredsel-prev",
                next: ".caroufredsel-next",
                height: 320,
                width: "100%",
                items: {
                    visible: 'odd+2'
                },
                swipe: {
                    onTouch: true,
                    onMouse: true,
                    options: {
                        excludedElements: "button, input, select, textarea, .noSwipe"
                    },
                    onBefore: function () {
                        //hover effect fix
                        element.trigger('mouseleave');
                        element.find('.carousel-item').trigger('mouseup');
                    }
                },
                scroll: {
                    items: 1
                    //timeoutDuration: 1250
                },
                mousewheel: false,
                auto: {
                    play: true
                }
            });

            element.find('.carousel-item').fancybox({
                cyclic: true,
                opacity: true,
                overlayOpacity: 0.9,
                zoomSpeedIn: 1000,
                scrolling: 'no',
                onStart: function () {
                    element.trigger("pause");
                },
                onClosed: function () {
                    element.trigger("play");
                },
                onComplete: function () {
                    $('#fancybox-wrap').css('margin-top', '-50px');
                    var fb = $("#fancybox-content");
                    $(fb).css({'opacity': '1', 'height': '520px'});
                }
            });

        }).animate({'opacity': 1}, 1000);//images loaded

        return result;
    }

    createHotelsCarousel('.hotel-photos .carousel');

    $(document).on('shown.bs.tab', 'a[href="#tab-photo"]', function (e) {

        if (photosintab == undefined) {

            createHotelsCarousel('.hotel-photos .carousel');

        } else {
            $('.hotel-photos .carousel').trigger('updateSizes');
        }

    });

    $(document).on('shown.bs.tab', 'a[href="#tab-photo"]', function (e) {
        $('.carousel-photo').trigger('updateSizes');
    });


    /* fullwidth carousel swipe link fix
     -------------------------------------------------------------------------- */
    var $mousePosStart = 0;
    var $mousePosEnd = 0;
    $('.carousel-wrap[data-full-width="true"] .portfolio-items .col .work-item .work-info a').mousedown(function (e) {
        $mousePosStart = e.clientX;
    });

    $('.carousel-wrap[data-full-width="true"] .portfolio-items .col .work-item .work-info a').mouseup(function (e) {
        $mousePosEnd = e.clientX;
    });

    $('.carousel-wrap[data-full-width="true"] .portfolio-items .col .work-item .work-info a').click(function (e) {
        if (Math.abs($mousePosStart - $mousePosEnd) > 10)  return false;
    });


    /* ==========================================================================
     ToolTip
     ========================================================================== */
    $("li[data-rel=tooltip]").tooltip();
    $("a[data-rel=modal-tooltip]").tooltip();
    $("a[data-rel=tooltip]").tooltip({container: 'body'});
    $("i[data-rel=tooltip]").tooltip({container: 'body'});
    $("span[data-rel=tooltip]").tooltip({container: 'body'});

});
