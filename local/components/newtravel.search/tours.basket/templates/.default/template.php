<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

global $USER;

$isManager = in_array(8, explode(",", $USER->GetGroups()));
?>
<style>
	#tourActulizeModal .modal-header {
		padding: 5px 15px;
	}

	#tourActulizeModal .modal-title {
		padding-bottom: 0px;
		font-size: 24px;
	}

	.tourdetail .short-section-title {
		margin: 0 0 10px 0;
	}

	.tourdetail h3 {
		padding: 0 0 5px;
	}

	.tourdetail .project-OverView ul > li {
		font-size: 14px;
	}

	.tourdetail .list-label {
		font-size: 14px;
		padding: 0;
		display: inline-block;
		width: 150px;
	}

	.tourdetail .price {
		font-size: 24px !important;
		color: #00aeef !important;
		font-weight: normal !important;
		display: inline-block;
		padding: 0;
		margin: 5px 0
	}

	#checkoutBtn {
		width: 150px;
		margin-left: 10px;
	}

	.tourdetail .turoperator-data .col-md-4 {
		text-align: center;
	}

	.tourdetail .turoperator-data .item {
		margin-top: 15px;
		padding: 5px 10px;
		background: rgba(0, 174, 239, 0.3);
		display: block;
		overflow: hidden;
		font-size: 20px;
		position: relative;
		-webkit-box-shadow: 1px 2px 5px #ccc;
		-moz-box-shadow: 1px 2px 5px #ccc;
		box-shadow: 1px 2px 5px #ccc;
	}

	.tourdetail .turoperator-data .item:hover {
		background: rgba(0, 174, 239, 0.39);
	}

	.tourdetail .turoperator-data .item label {
		display: block;
	}

	.tourdetail .turoperator-data .item .price {
		font-size: 28px;
		color: #00AEEF;
		font-weight: bold;
		padding: 0;
	}

	.tourdetail .turoperator-data .sub-item {
		margin: 0 15px 5px 15px;
		background: #f0f0f0;
		text-align: center;
		padding: 5px 10px;
		overflow: hidden;
	}

	.tourdetail .turoperator-data .sub-item label {
		margin: 0;
	}

	.tourdetail .turoperator-data .sub-item .small > span {
		color: #a2a2a2;
	}

	.tourdetail .turoperator-data .sub-item .small > span {
		color: #a2a2a2;
	}

	.tour-wrap {
		background: #fff;
		margin-bottom: 15px;
	}

	@media only screen and (max-width: 479px) {
		#tourActulizeModal .modal-title {
			font-size: 18px;
		}

		#checkoutBtn {
			width: 174px;
			margin-left: 0;
		}

		.tourdetail .turoperator-data .col-md-4 {
			margin-bottom: 10px;
		}
	}
</style>
<div id="tourActulizeModal">
	<? if ( ! empty($arResult['TOUR'])): ?>
		<? if (strlen($arResult['COMMENT']) > 0): ?>
			<div class="panel panel-default">
				<div class="panel-heading">Комментарий эксперта</div>
				<div class="panel-body">
					<?=$arResult['COMMENT']?>
				</div>
			</div>
		<? endif; ?>

		<? if ($arResult['FROM_BASKET_LINK']): ?>
			<div id="col-md-12">
				<a href="javascript:void(0)" id="getBasketLink" data-basket-id="<?=$arResult['BASKET_ID']?>" class="pull-right">получить ссылку на корзину</a>
			</div>
		<? elseif (($USER->IsAdmin() || $isManager) && $arResult['FROM_COOKIE']): ?>
			<div id="col-md-12">
				<a href="javascript:void(0)" id="createNewManagerBasket" data-basket-id="<?=$arResult['BASKET_ID']?>" class="pull-right">создать новую корзину и получить ссылку на нее</a>

			</div>
		<? endif; ?>

		<div class="clearfix"></div>
		<? foreach ($arResult['TOUR'] as $key => $arTour): ?>
			<? if (empty($arTour['DATA'])): ?>
				<div class="row" id="tour_<?=$arTour['TOUR_ID']?>">
					<div class="col-md-12">
						<div class="alert alert-warning" role="alert">
							<? if ($arResult['CAN_DELETE']): ?>
								<button type="button" class="close" onclick="removeFromBasket('<?=$arTour['TOUR_ID']?>')" data-dismiss="alert" aria-label="Close" title="Убрать из корзины"><span aria-hidden="true">&times;</span></button>
								Данный тур больше не доступен
							<? endif ?>
						</div>
					</div>
				</div>
				<? continue; ?>
			<? endif; ?>
			<div class="tour-wrap" id="tour_<?=$arTour['TOUR_ID']?>">
				<div class="modal-header"></button>
					<? if ($arResult['CAN_DELETE']): ?>
						<button type="button" class="close" onclick="removeFromBasket('<?=$arTour['TOUR_ID']?>')" title="Убрать из корзины"><span aria-hidden="true">&times;</span></button>
					<? endif; ?>
					<h4 class="modal-title" id="myModalLabel">Тур <?=$arTour['DATA']['tourname']?></h4>
				</div>
				<div class="modal-body tourdetail">
					<div class="row">
						<div class="col-md-3 col-sm-3">
							<img src="<?=$arTour['DATA']['hotelpicturemedium']?>" alt="" width="100%"/>
						</div>

						<div class="col-md-9 col-sm-9">
							<div class="row">
								<div class="col-md-8">
									<div class="project-OverView">
										<h3>
											<?=$arTour['DATA']['hotelname'] . " " . $arTour['DATA']['hotelstars']?>*
											<p>
												<small><?=$arTour['DATA']['countryname']?> / <?=$arTour['DATA']['hotelregionname']?></small>
											</p>
										</h3>
									</div>
								</div>
								<div class="col-md-4 col-sm-12">
									<div class="buttons_wrap pull-right">
										<a href="/personal/order/make/" target="_blank" type="button" data-tour-id="<?=$arTour['TOUR_ID']?>" class="btn btn-blue checkoutBtn">Забронировать</a>
									</div>
								</div>
								<div class="col-md-12"><h3 class="title">Информация о туре</h3></div>
								<div class="col-md-4 col-sm-6">
									<ul class="list-unstyled">
										<li>
											<div class="list-label">Вылет:</div>
											<strong><?=$arTour['DATA']['departurename']?> - <?=trim($arTour['DATA']['flydate'])?></strong>
										</li>
										<li>
											<div class="list-label">Ночей:</div>
											<strong><?=$arTour['DATA']['nights']?></strong></li>
										<li>
											<div class="list-label">Питание:</div>
											<strong><?=$arTour['DATA']['meal']?></strong></li>
										<li>
											<div class="list-label">Размещение:</div>
											<strong><?=$arTour['DATA']['placement']?></strong></li>
									</ul>
								</div>
								<div class="col-md-4 col-sm-6">
									<ul class="list-unstyled">
										<li>
											<div class="list-label">Комната:</div>
											<strong><?=$arTour['DATA']['room']?></strong></li>
										<li>
											<div class="list-label">Туроператор:</div>
											<img src="https://tourvisor.ru/pics/operators/searchlogo/<?=$arTour['DATA']['operatorcode']?>.gif" alt="<?=$arTour['DATA']['operatorname']?>">
										</li>
										<li>
											<div class="list-label">Цена за тур:</div>
											<span class="price"><?=trim(CurrencyFormat($arTour['DATA']['price'], 'RUB'))?></span>
										</li>
										<li>
											<small>
												* в т.ч. топливный сбор: <?=trim(CurrencyFormat($arTour['DATA']['fuelcharge'], 'RUB'))?>
											</small>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12">
							<? if ( ! empty($arTour['FLIGHTS']['flights'])): ?>
								<hr>
								<h3 class="title">Информация рейсах
									<small><a href="#" class="show-flights" data-tour-key="<?=$key?>">показать/скрыть</a></small>
								</h3>
								<div class=" section-inner" style="display: none;" id="flight_<?=$key?>">
									<div class="row">
										<div class="col-xs-12 turoperator-data">
											<div class="col-md-4 col-sm-4 hidden-xs">
												Туроператор
											</div>
											<div class="col-md-4 col-sm-4 hidden-xs">
												Авиакомпания
											</div>
											<div class="col-md-4 col-sm-4 hidden-xs">
												Цена за тур
											</div>
											<? foreach ($arTour['FLIGHTS']["flights"] as $keyFl => $arFlight): ?>
												<div class="item">
													<label for="turoperator<?=$keyFl?>" onclick="changeFlight('<?=$keyFl?>_<?=$key?>');">
														<div class="col-md-4 col-sm-4 col-xs-12">
															<img src="https://tourvisor.ru/pics/operators/searchlogo/<?=$arTour['DATA']["operatorcode"]?>.gif" alt="<?=$arTour['DATA']["operatorname"]?>">
														</div>
														<div class="col-md-4 col-sm-4 col-xs-12">
															<?=$arFlight["forward"][0]["company"]['name']?>
															<? if ($arFlight["forward"][0]["company"]['thumb']): ?>
																<div class="img-thumbnail"><img src="<?=$arFlight["forward"][0]["company"]['thumb']?>" height="20" alt=""></div>
															<? endif; ?>
														</div>
														<div class="col-md-4 col-sm-4 col-xs-12 price">
															<?=CurrencyFormat($arFlight["price"]["value"], "RUB")?>
														</div>
													</label>
												</div>
												<div class="sub-item sub-item<?=$keyFl?>_<?=$key?>" <? if ($keyFl >= 1): ?>style="display: none;" <? endif ?>>
													<div class="col-md-6 col-sm-6 small">
														<label for="forward<?=$keyFl?>">
															<strong style="color:#00AEEF">Туда</strong> - <?=$arFlight["dateforward"]?>
														</label>
														<br/>
														<strong><?=$arFlight["forward"][0]["departure"]["time"]?></strong> <?=$arFlight["forward"][0]["departure"]["port"]['name']?> -
														<strong><?=$arFlight["forward"][0]["arrival"]["time"]?></strong> <?=$arFlight["forward"][0]["arrival"]["port"]['name']?><br/>
														<span><?=$arFlight["forward"][0]["plane"]?> (<?=$arFlight["forward"][0]["number"]?>)</span>
														<br/>
													</div>

													<div class="col-md-6 col-sm-6 small">
														<label for="backward<?=$keyFl?>">
															<strong style="color:#00AEEF">Обратно</strong> - <?=$arFlight["datebackward"]?>
														</label>
														<br/>
														<strong><?=$arFlight["backward"][0]["departure"]["time"]?></strong> <?=$arFlight["backward"][0]["departure"]["port"]['name']?> -
														<strong><?=$arFlight["backward"][0]["arrival"]["time"]?></strong> <?=$arFlight["backward"][0]["arrival"]["port"]['name']?><br/>
														<span><?=$arFlight["backward"][0]["plane"]?> (<?=$arFlight["backward"][0]["number"]?>)</span>
													</div>
												</div>
											<? endforeach; ?>
											<? if (count($arResult["FLIGHTS"]["flights"]) > 1): ?>
												<div class="col-md-12 text-right">
													<small>* выбрать вариант рейса можно на странице оформления заказа</small>
												</div>
											<? endif; ?>
										</div>
									</div>
								</div>
								<script>
                                    function changeFlight(key) {
                                        $(".sub-item").slideUp(500);
                                        $(".sub-item" + key).slideDown(500);
                                    }
								</script>
							<? endif; ?>
						</div>
					</div>
				</div>
			</div>
		<? endforeach; ?>

		<? if ($USER->IsAdmin() || $isManager && $arResult['FROM_BASKET_LINK']): ?>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<textarea class="form-control" id="comment_field" rows="3" style="resize: none" placeholder="Комментарий менеджера"><?=$arResult['COMMENT']?></textarea>
					</div>
					<div class="form-group">
						<a href="#" class="btn btn-blue add-comment" data-basket-id="<?=$arResult['BASKET_ID']?>">Добавить комментарий к корзине</a>
					</div>
				</div>
			</div>
		<? endif; ?>

	<? else: ?>
		<div class="tour-wrap">
			<div class="alert alert-warning">
				Ваша корзина туров пустая.
			</div>
		</div>
	<? endif; ?>
</div>

<script>


    $(document).ready(function () {

        $('.show-flights').on('click', function (e) {
            e.preventDefault();
            var target = e.target,
                targetKey = $(target).data('tour-key');
            $('#flight_' + targetKey).slideToggle();
        });


		<? if ($USER->IsAdmin() || $isManager): ?>
        $('#getBasketLink').on('click', function (e) {
            let target = $(e.target),
                basket_id = target.data('basket-id');

            prompt("Скопируйте ссылку на корзину туров", "https://умныетуристы.рф/tour-basket/?bid=" + btoa(basket_id + '#newtravel'))
        });

        $('#createNewManagerBasket').on('click', function (e) {
            let target = $(e.target),
                basket_id = target.data('basket-id');

            $.get('/local/tools/newtravel.search.ajax.php', {type: "addManagerBasket"}, function (data) {
                console.log(data);
                if (data > 0) {
                    console.log('добавлено');
                    prompt("Скопируйте ссылку на корзину туров", "https://умныетуристы.рф/tour-basket/?bid=" + btoa(data + '#newtravel'))
                } else {
                    console.log('ошибка');
                }
            });
        });

        $('.add-comment').on('click', function (e) {
            e.preventDefault();
            let target = $(e.target),
                basket_id = target.data('basket-id'),
                comment_field = $('#comment_field').val();

            target.text('Добавляем...');
            $.get('/local/tools/newtravel.search.ajax.php', {type: "addBasketComment", basket_id: basket_id, comment: comment_field}, function (data) {
                console.log(data);
                if (data === 'success') {
                    target.text('Добавлено');
                } else {
                    target.text('Ошибка добавления');
                }
            });
        });
		<?endif;?>
    });

    function removeFromBasket(tourid) {
        var tourWrap = $('#tour_' + tourid);
        $.get('/local/tools/newtravel.search.ajax.php', {type: "removeFromToursBasket", tourid: tourid}, function (data) {
            if (data === 'success') {
                tourWrap.remove();
                if (window.tbs !== undefined) {
                    window.tbs.getCount();
                }
            }
        });
    }

    //Нажимаем на "Оформить заказ"
    $(".checkoutBtn").on("click", function () {
        var tour_id = $(this).data('tour-id');
        $(this).html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
        $.get('/local/tools/newtravel.search.buytour.php', {buy: "Y", tourid: tour_id}, function (data) {
            console.log(data);
            if (data == "success") {
                $(".checkoutBtn").text("Забронировать");
            }
        });
    });
</script>