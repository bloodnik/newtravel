<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Entity\Query;
use Newtravel\Search\ToursBasket;
use Newtravel\Search\Tourvisor;
use Bitrix\Main\Loader;
use Bitrix\Main\Application;

class CToursBasket extends CBitrixComponent {


	//Запускаем компонент
	public function executeComponent() {
		global $USER;

		define('SALT', "#newtravel");
		$isManager = in_array(8, explode(",", $USER->GetGroups()));

		// Подключение модуля
		Loader::includeModule("newtravel.search");
		Loader::includeModule('sale');
		Loader::includeModule("highloadblock");

		$this->arResult["TOUR"] = [];
		$this->arResult['FROM_COOKIE'] = false;
		$this->arResult['BASKET_ID'] = 0;
		$this->arResult['COMMENT'] = "";
		$this->arResult['FROM_BASKET_LINK'] = false;

		$request  = Application::getInstance()->getContext()->getRequest();
		$bid = $request->get('bid');
		$basket_id = str_replace(SALT, '', base64_decode($bid));

		//Если жестко задан ID корзины. нужен для того чтобы скидывать подборки клиентам
		if (isset($basket_id) && strlen($basket_id) > 0) {
			$arTours = ToursBasket::getBasketById($basket_id);
			$arToursId = json_decode(base64_decode($arTours['UF_TOURS']), true);
			$this->setToursArray($arToursId);
			$this->arResult['BASKET_USER'] = $arTours['UF_USER'];
			$this->arResult['BASKET_ID'] = $arTours['ID'];
			$this->arResult['COMMENT'] = $arTours['UF_COMMENT'];
			$this->arResult['FROM_BASKET_LINK'] = true;
		}else{
			//Если пользователь не авторизован, то берем его корзину из cookie
			if ( ! $USER->IsAuthorized() || $isManager) {
				$tourBasket = $request->getCookie('TOURS_BASKET');
				if(isset($tourBasket) && strlen($tourBasket) > 0){
					$this->setToursArray(json_decode(base64_decode($tourBasket), true));
					$this->arResult['FROM_COOKIE'] = true;
				}
			} else { //Загружаем его корзину из БД - HL инфоблока
				$arTours = ToursBasket::getBasketByUserId($USER->GetID());
				$arToursId = json_decode(base64_decode($arTours['UF_TOURS']), true);
				$this->setToursArray($arToursId);
				$this->arResult['BASKET_USER'] = $arTours['UF_USER'];
				$this->arResult['BASKET_ID'] = $arTours['ID'];
				$this->arResult['COMMENT'] = $arTours['UF_COMMENT'];
			}
		}

		$this->arResult['CAN_DELETE'] = ($USER->IsAuthorized() && $USER->GetId() === $this->arResult['BASKET_USER']) || $this->arResult['FROM_COOKIE']? true : false;

		$this->includeComponentTemplate();
	}

	/**
	 * @param array $arToursId - Массив ID туров
	 */
	private function setToursArray($arToursId) {
		$tv = new Tourvisor();

		foreach ($arToursId as $key => $tourId) {
			$this->arResult["TOUR"][ $key ]['TOUR_ID'] = $tourId;
			$arTour                                    = $tv->getData("actualize", array("tourid" => $tourId));
			$this->arResult["TOUR"][ $key ]['DATA']    = $arTour["data"]['tour'];
			$arActTour                                 = $tv->getData("actdetail", array("tourid" => $tourId));
			$this->arResult["TOUR"][ $key ]['FLIGHTS'] = $arActTour;
		}
	}

}

?>