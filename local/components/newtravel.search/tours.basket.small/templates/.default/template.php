<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>

<style>
	#tbs {
		position: fixed;
		right: 0;
		top: 50%;
		background: #999999;
		padding: 10px 5px;
		text-align: center;
		font-size: 16px;
		color: #ffffff;
		z-index: 999;

		-webkit-transition: background 0.3s ease;
		-moz-transition: background 0.3s ease;
		-ms-transition: background 0.3s ease;
		-o-transition: background 0.3s ease;
		transition: background 0.3s ease;
	}

	#tbs:hover {
		background: #828282;
	}

	#tbs a {
		color: #ffffff;
	}

	[v-cloak] {
		display: none;
	}

	@media only screen and (max-width: 1199px) and (min-width: 320px) {
		#tbs {
			padding: 5px;
			font-size: 12px;
		}
	}


</style>

<div id="tbs">
	<a href="/tour-basket/" target="_blank" v-cloak>
		<i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i><br>
		В корзине <br>
		{{tours_count | pluralText}}
	</a>
</div>


<script>
    window.tbs = new Vue({
        el: '#tbs',
        data: {
            tours_count: 0
        },
        created: function () {
            this.getCount();
        },
        methods: {
            getCount: function () {
                this.$http.get('/local/modules/newtravel.search/lib/ajax.php', {params: {type: "basketCount"}}).then(function (response) {
                        this.tours_count = parseInt(response.body);
                    }
                );
            },
        },
        filters: {
            pluralText: function (value) {
                var titles = ['тур', 'тура', 'туров'];
                value = parseInt(value);
                return value + " " + titles[(value % 10 === 1 && value % 100 !== 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
            }
        }
    })
</script>