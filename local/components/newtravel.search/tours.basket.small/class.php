<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Entity\Query;
use Newtravel\Search\ToursBasket;
use Newtravel\Search\Tourvisor;
use Bitrix\Main\Loader;
use Bitrix\Main\Application;

class CToursBasketSmall extends CBitrixComponent {


	//Запускаем компонент
	public function executeComponent() {
		// Подключение модуля
		Loader::includeModule("newtravel.search");

		$this->arResult["COUNT"]= ToursBasket::getBasketCount();
		$this->includeComponentTemplate();
	}

}

?>