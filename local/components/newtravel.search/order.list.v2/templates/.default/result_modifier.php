<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

$arResult['ORDER']['PROPS_MESSAGE'] = array(
	'CONTACT_PERSON' => array(
		'DESCRIPTION' => 'Как мы можем к Вам обращаться?',
		'ERROR' => 'Введите пожалуйста Ваше имя'
	),
	'EMAIL' => array(
		'DESCRIPTION' => 'Email необходим для регистрации на сайте и сохранения заказа',
		'ERROR' => 'Нужно ввести корректный Email'
	),
	'PHONE' => array(
		'DESCRIPTION' => 'По телефону нашим менедежеры свяжутся с Вами для подтверждения заказа',
		'ERROR' => 'Вы не ввели номер телефона'
	)
);

