<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CTourOrder $component */
global $USER;
?>

<div class="preload" style="display: none;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="loader">
					<div class="loader-inner">
						<div class="box-1"></div>
						<div class="box-2"></div>
						<div class="box-3"></div>
						<div class="box-4"></div>
					</div>
					<span class="text">ждем...</span>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
    var orderData = <?=CUtil::PhpToJSObject($arResult)?>;
</script>

<div class="container">
	<div id="order-list" v-cloak>
		<div class="card mb-5" v-for="order in sortedOrders" v-if="countOrders">
			<h4 class="card-header">Заказ №{{order.ACCOUNT_NUMBER}} от {{order.DATE}}</h4>
			<div class="card-body bg-light">
				<h4 class="card-title">Статус заказа: <span class="text-success">{{data.STATUS[order.STATUS_ID].NAME}}</span></h4>
				<div class="alert alert-warning" v-if="data.STATUS[order.STATUS_ID].DESCRIPTION">{{data.STATUS[order.STATUS_ID].DESCRIPTION}}</div>
				<div class="alert alert-info" v-if="order.COMMENTS">
					<strong>Комментарий менеджера:</strong> <br>
					{{order.COMMENTS}}
				</div>

				<div class="card-text mb-3" v-if="order.HAS_BASKET_ITEMS">
					<div v-if="order.PROPS.TOUR_PROPS.DOCUMENTS.VALUE">
						<div class="h5">Документы на тур:</div>
						<div class="row">
							<div class="col">
								<ul class="list-unstyled">
									<li v-for="doc in order.PROPS.TOUR_PROPS.DOCUMENTS.VALUE"><a :href="doc.SRC" target="_blank">{{doc.ORIGINAL_NAME}}</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="h5">Состав заказа:</div>
					<div class="row">
						<div class="col-9">Тур: {{order.BASKET.NAME}}</div>
						<div class="col-3 display-6">{{order.BASKET.FINAL_PRICE | formatPrice}}</div>
					</div>
					<div class="h5" @click="toggleProps(order.ID)">Свойства заказа:
						<small class="text-muted">развернуть/свернуть</small>
					</div>
					<div :id="'props_' + order.ID" style="display: none;">
						<div class="row" v-for="prop in order.BASKET.PROPS">
							<div class="col-6">{{prop.NAME}}</div>
							<div class="col-6" style="font-weight: bold;">{{prop.VALUE}}</div>
						</div>
					</div>
					<div v-if="!isComplete(order.STATUS_ID)">
						<div class="h5">Оплаты:</div>
						<div class="row" v-if="order.HAS_BASKET_ITEMS">
							<div class="col-12" v-if="order.CALCULATED">
								<div v-if="! order.PAID">
									<div class="row">
										<div class="col-12 col-md-6" v-for="(payment, index) in order.PAYMENTS">
											<div v-if="payment.ACTION_FILE && payment.NEW_WINDOW && !payment.IS_CASH && !payment.IS_PAID">
												<div class="card p-3 mb-3">
													<div class="row d-flex align-items-center">
														<div class="col-6 display-7">
															{{payment.NAME}}
														</div>
														<div class="col-6 text-right">
															<div class="display-7" style="font-weight: 400;"><a class="btn btn-success" :href="payment.PAY_URL">Оплатить {{payment.SUM |
																	formatPrice}}</a></div>
														</div>
													</div>
												</div>
											</div>
											<div v-else>
												<div class="card p-3 mb-3">
													<div class="row d-flex align-items-center">
														<div class="col-6 display-7">
															{{payment.NAME}}
															<span class="text-success" v-if="payment.IS_PAID">оплачено</span>
														</div>
														<div class="col-6 text-right">
															<div class="display-7" style="font-weight: 400;">{{payment.SUM | formatPrice}}</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-12">
											<div class="alert alert-info" v-html="order.PAYMENTS[0].DESCRIPTION"></div>
											<div v-if="order.PAYMENTS[0].ACTION_FILE && !order.PAYMENTS[0].NEW_WINDOW" v-html="order.PAYMENTS[0].BUFFERED_OUTPUT"></div>
										</div>
									</div>
								</div>
								<div v-else>
									<span class="display-6 text-success">Заказ оплачен полностью</span>
								</div>
							</div>
							<div class="col-12" v-else>
								<button class="btn btn-warning" @click="recalculateOrder(order.ID)">Пересчитать стоимость заказа</button>
							</div>
						</div>
					</div>
				</div>
				<a class="btn btn-link" href="/personal/order" v-if="data.IS_DETAIL_PAGE">Все заказы</a>
			</div>
		</div>
		<div v-if="!countOrders">
			<div class="alert alert-warning">
				Заказы не найдены
			</div>
		</div>
		<?=$arResult['NAV']?>
	</div>


