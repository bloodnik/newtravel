'use strict';

$(document).ready(function () {

    Vue.use(VueTheMask);

    window.orderList = new Vue({
        el: '#order-list',
        data: {
            data: orderData,
        },
        created: function () {
        },

        mounted: function () {
        },
        methods: {

            toggleProps: function (id) {
                $('#props_' + id).slideToggle();
            },

            changePaySystem: function (paysystem_id) {
                this.data.SELECTED_PAYSYSTEM = paysystem_id;
                this.sendForm();
            },

            //Разделяем оплату
            doSplitPayment: function () {
                this.sendForm({'split_payment': this.data.SPLIT_PAYMENT_IS_ACTIVE === 'Y' ? "N" : "Y"});
            },

            //Пересчитываем стоимость заказа
            recalculateOrder: function (id) {
                console.log(id);
                this.sendForm({'action': "recalculate", order_id: id, 'is_ajax': 'Y'});
            },

            isComplete(orderStatus) {
              return orderStatus === 'F'
            },

            sendForm: function (data = {}) {
                var self = this;
                setTimeout(function () {
                    $('.preload').show();
                    console.log(data);
                    $.post(self.POST_FORM_URI, data, function (response) {
                        self.data = _.isObject(response) ? response : JSON.parse(response);
                        $('.preload').hide();
                    });
                }, 100);
            },

        },
        computed: {
            sortedOrders : function () {
                return _.orderBy(this.data.ITEMS, ['ID'], ['desc']);
            },
            countOrders: function () {
                return _.size(this.data.ITEMS);
            }
        },
        filters:
            {
                formatPrice: function (value) {
                    return parseInt(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
                }
                ,

                getOldPrice: function (value) {
                    value = parseInt(value) + parseInt(value * 0.11);
                    return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
                }
                ,

                getCreditPrice: function (value) {
                    value = (parseInt(value) + parseInt(value * 0.0717)) / 6;
                    return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' Р';
                }
                ,

                displayNights: function (value) {
                    if (!value) {
                        return "";
                    } else {
                        var titles = ['ночь', 'ночи', 'ночей'];
                        value = parseInt(value);
                        return value + " " + titles[(value % 10 == 1 && value % 100 != 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                    }
                }
                ,


                numberToText: function (value) {
                    switch (value) {
                        case 1:
                            return 'Первый';
                            break;
                        case 2:
                            return 'Второй';
                            break;
                        case 3:
                            return 'Третий';
                            break;
                        case 4:
                            return 'Четвертый';
                            break;
                        case 5:
                            return 'Пятый';
                            break;
                        case 6:
                            return 'Шестой';
                            break;
                        case 7:
                            return 'Седьмой';
                            break;
                    }

                }
                ,

                pluralText: function (value) {
                    var titles = ['тур', 'тура', 'туров'];
                    value = parseInt(value);
                    return value + " " + titles[(value % 10 === 1 && value % 100 !== 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
                }
            }
    })
})
;