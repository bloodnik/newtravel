<?

use Bitrix\Main\Loader;
use Bitrix\Main\Type\Date;
use Bitrix\Sale\DiscountCouponsManager;
use Bitrix\Sale\Internals\BasketTable;
use Bitrix\Main\Localization\Loc;

if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

class CTourOrderList extends CBitrixComponent {

	/**
	 * Наценка за онлайн оплату
	 */
	const ONLINE_PAY_COMISSION = 0.018;

	/**
	 * @var \Bitrix\Sale\Order
	 */
	public $order;

	/**
	 * @var array
	 */
	protected $errors = [];

	/**
	 * @var array
	 */
	protected $arResponse = [];

	/**
	 * Идентификатор тура(товара) в корзине
	 * @var int
	 */
	protected $basketTourProductId;


	/**
	 * Формирование массива свойств заказа
	 * Выделяем в массив свойства для отображения и свойства, которые дложны быть скрыты (будут заполняться автоматический через vuejs)
	 * @var \Bitrix\Sale\Order $order
	 * @return array
	 */
	protected function getAvailableOrderProps($order) {
		$props       = array("SHOW" => array(), "HIDDEN" => array());
		$hiddenProps = array('TOURIST_1', "TOURIST_2", "TOURIST_3", "TOURIST_4", "TOURIST_5", "CHILD_1", "CHILD_2", "CHILD_3", "FLIGHT_DATA", "OPERATOR_LINK", "RECALCULATE_DATE", "");
		$shownProps  = array('CONTACT_PERSON', "EMAIL", "PHONE");
		$tourProps  = array('DOCUMENTS');

		/** @var \Bitrix\Sale\PropertyValue $prop */
		foreach ($order->getPropertyCollection() as $prop):
			if (in_array($prop->getField('CODE'), $hiddenProps)) {
				$props['HIDDEN'][ $prop->getField('CODE') ] = array(
					"ID"       => $prop->getPropertyId(),
					"CODE"     => $prop->getField('CODE'),
					"NAME"     => $prop->getName(),
					"VALUE"    => $prop->getValue(),
					"REQUIRED" => $prop->isRequired() ? true : false
				);
			}
			if (in_array($prop->getField('CODE'), $shownProps)) {
				$props['SHOW'][ $prop->getField('CODE') ] = array(
					"ID"        => $prop->getPropertyId(),
					"CODE"      => $prop->getField('CODE'),
					"NAME"      => $prop->getName(),
					"VALUE"     => $prop->getValue(),
					"HAS_ERROR" => false,
					"REQUIRED"  => $prop->isRequired() ? true : false
				);
			}
			if (in_array($prop->getField('CODE'), $tourProps)) {
				$props['TOUR_PROPS'][ $prop->getField('CODE') ] = array(
					"ID"        => $prop->getPropertyId(),
					"CODE"      => $prop->getField('CODE'),
					"NAME"      => $prop->getName(),
					"VALUE"     => $prop->getValue(),
					"HAS_ERROR" => false,
					"REQUIRED"  => $prop->isRequired() ? true : false
				);
			}
		endforeach;

		return $props;
	}


	function __construct($component = null) {
		parent::__construct($component);

		if ( ! Loader::includeModule('sale')) {
			$this->errors[] = 'No sale module';
		};

		if ( ! Loader::includeModule('catalog')) {
			$this->errors[] = 'No catalog module';
		};
	}

	function onPrepareComponentParams($arParams) {

		if (isset($arParams['IS_AJAX']) && ($arParams['IS_AJAX'] == 'Y' || $arParams['IS_AJAX'] == 'N')) {
			$arParams['IS_AJAX'] = $arParams['IS_AJAX'] == 'Y';
		} else {
			if (isset($this->request['is_ajax']) && ($this->request['is_ajax'] == 'Y' || $this->request['is_ajax'] == 'N')) {
				$arParams['IS_AJAX'] = $this->request['is_ajax'] == 'Y';
			} else {
				$arParams['IS_AJAX'] = false;
			}
		}

		return $arParams;
	}

	/**
	 * Получение существующего заказа
	 *
	 * @param $order_id
	 */
	protected function getCreatedOrder($order_id) {
		$order = \Bitrix\Sale\Order::load($order_id);

		/**
		 * Флаг необходимости пересчета. Нужен для совместимости со старыми заказами без цены в UE
		 */
		$needRecalculate = false;

		$this->arResult['ITEMS'][ $order_id ]['PAID']  = $order->isPaid(); //Флаг оплаченного заказа
		$this->arResult['ITEMS'][ $order_id ]['PROPS'] = $this->getAvailableOrderProps($order); //Массив свойств заказа для вывода в шаблоне

		/** @var \Bitrix\Sale\Basket $basket */
		if ( ! ($basket = \Bitrix\Sale\Basket::loadItemsForOrder($order))) {
			throw new \Bitrix\Main\ObjectNotFoundException('Entity "Basket" not found');
		}

		$this->arResult['ITEMS'][ $order_id ]['HAS_BASKET_ITEMS'] = $basket->count() ? true : false;

		if ($basket->count()) {
			/** @var \Bitrix\Sale\BasketItem $orderableItem */
			foreach ($basket->getOrderableItems() as $orderableItem) {
				$this->arResult['ITEMS'][ $order_id ]['BASKET']['ID']          = $orderableItem->getId();
				$this->arResult['ITEMS'][ $order_id ]['BASKET']['NAME']        = $orderableItem->getField('NAME');
				$this->arResult['ITEMS'][ $order_id ]['BASKET']['PRODUCT_ID']  = $orderableItem->getProductId();
				$this->arResult['ITEMS'][ $order_id ]['BASKET']['PRICE']       = $orderableItem->getPrice();
				$this->arResult['ITEMS'][ $order_id ]['BASKET']['FINAL_PRICE'] = $orderableItem->getFinalPrice();
				$this->arResult['ITEMS'][ $order_id ]['BASKET']['BASE_PRICE']  = $orderableItem->getBasePrice();
				$this->arResult['ITEMS'][ $order_id ]['BASKET']['QUANTITY']    = $orderableItem->getQuantity();
				$this->arResult['ITEMS'][ $order_id ]['BASKET']['CAN_BUY']     = $orderableItem->canBuy();

				//Получаем цену в валюте и название валюты из свойств товара в корзине заказа
				$basketPropRes = \Bitrix\Sale\Internals\BasketPropertyTable::getList(array(
					'filter' => array(
						"BASKET_ID" => $orderableItem->getId(),
					),
				));

				$excludeProps = array('TOUR_ID', 'USE_DISCOUNT', 'UE', 'UE_PRICE');
				while ($property = $basketPropRes->fetch()) {

					if($property['CODE'] === 'UE_PRICE') {
						$needRecalculate = true;
					}

					if (in_array($property['CODE'], $excludeProps)) {
						continue;
					}

					$this->arResult['ITEMS'][ $order_id ]['BASKET']['PROPS'][ $property['CODE'] ] = array('NAME' => $property['NAME'], 'VALUE' => $property['VALUE']);
				}
			}
		}

		$paymentCollection     = $order->getPaymentCollection();
		$arPaySystemServiceAll = [];

		//Список созданных оплат
		/* @var \Bitrix\Sale\Payment $payment */
		foreach ($paymentCollection as $key => $payment) {
			$arPaySystemServices = \Bitrix\Sale\PaySystem\Manager::getListWithRestrictions($payment);

			$arPaySystemServiceAll                                                   += $arPaySystemServices;
			$this->arResult['ITEMS'][ $order_id ]['PAYMENTS'][ $key ]['ID']          = $payment->getPaymentSystemId();
			$this->arResult['ITEMS'][ $order_id ]['PAYMENTS'][ $key ]['NAME']        = $payment->getField('PAY_SYSTEM_NAME');
			$this->arResult['ITEMS'][ $order_id ]['PAYMENTS'][ $key ]['SUM']         = $payment->getSum();
			$this->arResult['ITEMS'][ $order_id ]['PAYMENTS'][ $key ]['DESCRIPTION'] = $arPaySystemServices[ $payment->getPaymentSystemId() ]['DESCRIPTION'];
			$this->arResult['ITEMS'][ $order_id ]['PAYMENTS'][ $key ]['NEW_WINDOW']  = $arPaySystemServices[ $payment->getPaymentSystemId() ]['NEW_WINDOW'] === 'Y' ? true : false;
			$this->arResult['ITEMS'][ $order_id ]['PAYMENTS'][ $key ]['ACTION_FILE'] = $arPaySystemServices[ $payment->getPaymentSystemId() ]['ACTION_FILE'];
			$this->arResult['ITEMS'][ $order_id ]['PAYMENTS'][ $key ]['IS_CASH']     = $arPaySystemServices[ $payment->getPaymentSystemId() ]['IS_CASH'] === 'Y' ? true : false;
			$this->arResult['ITEMS'][ $order_id ]['PAYMENTS'][ $key ]['IS_PAID']     = $payment->isPaid();
			$this->arResult['ITEMS'][ $order_id ]['PAYMENTS'][ $key ]['PAY_URL']     = htmlspecialcharsbx($this->arParams["PATH_TO_PAYMENT"]) . 'payment?ORDER_ID=' . urlencode(urlencode($order->getField('ACCOUNT_NUMBER'))) . '&PAYMENT_ID=' .
			                                                                           $payment->getField('ACCOUNT_NUMBER');

			$paySystemService = \Bitrix\Sale\PaySystem\Manager::getObjectById($payment->getPaymentSystemId());
			if ($arPaySystemServices[ $payment->getPaymentSystemId() ]['NEW_WINDOW'] === 'N' || $arPaySystemServices[ $payment->getPaymentSystemId() ]['ID'] == \Bitrix\Sale\PaySystem\Manager::getInnerPaySystemId()) {
				/** @var \Bitrix\Sale\PaySystem\ServiceResult $initResult */
				$initResult = $paySystemService->initiatePay($payment, null, \Bitrix\Sale\PaySystem\BaseServiceHandler::STRING);
				if ($initResult->isSuccess()) {
					$this->arResult['ITEMS'][ $order_id ]['PAYMENTS'][ $key ]['BUFFERED_OUTPUT'] = $initResult->getTemplate();
				} else {
					$this->errors[] = $initResult->getErrorMessages();
				}
			}
		}

		/**
		 * Получение массива списка доступных платежных систем, идет деление на 2 типа, кредит(рассрочка) и остальные(нал-безнал)
		 * arResult['ORDER']['PAY_SYSTEMS'] = array('CREDIT', 'OTHER')
		 */
		$creditPaySystemsId = array(17, 23, 4);
		$otherPaySystemsId  = array(1, 6, 18, 25);
		$paySystemsList     = array();

		foreach ($arPaySystemServiceAll as $key => $paySystem) {

			if (in_array($key, $creditPaySystemsId)) {
				$paySystemsList['CREDIT'][ $key ] = $paySystem;

				if (intval($order->getId()) > 0) {
					$paySystemsList['CREDIT'][ $key ]['PAY_URL'] = htmlspecialcharsbx($this->arParams["PATH_TO_PAYMENT"]) . '?ORDER_ID=' . urlencode(urlencode($order->getField('ACCOUNT_NUMBER'))) . '&PAYMENT_ID=' . $paySystem['ID'];
				}

				if (intval($paySystem['LOGOTIP']) > 0) {
					$paySystemsList['CREDIT'][ $key ]['LOGO_PATH'] = CFile::GetPath($paySystem['LOGOTIP']);
				}
			}

			if (in_array($key, $otherPaySystemsId)) {
				$paySystemsList['OTHER'][ $key ] = $paySystem;

				if (intval($order->getId()) > 0) {
					$paySystemsList['OTHER'][ $key ]['PAY_URL'] = htmlspecialcharsbx($this->arParams["PATH_TO_PAYMENT"]) . '?ORDER_ID=' . urlencode(urlencode($order->getField('ACCOUNT_NUMBER'))) . '&PAYMENT_ID=' . $paySystem['ID'];
				}

				if (intval($paySystem['LOGOTIP']) > 0) {
					$paySystemsList['OTHER'][ $key ]['LOGO_PATH'] = CFile::GetPath($paySystem['LOGOTIP']);
				}
			}
		}

		$this->arResult['ITEMS'][ $order_id ]['PAY_SYSTEMS'] = $paySystemsList;


		//Условия для пересчета заказа
		$orderDate   = new DateTime($order->getDateInsert()->format('d.m.Y')); //Дата формирования заказа
		$obInterval  = $orderDate->diff(new DateTime());
		$dayInterval = $obInterval->days;


		//Проверяем свойство RECALCULATE_DATE
		if (!empty($order->getPropertyCollection()->getItemByOrderPropertyId(59)->getValue())) {
			$recalculateDate = new DateTime($order->getPropertyCollection()->getItemByOrderPropertyId(59)->getValue());
			$obInterval      = $recalculateDate->diff(new DateTime());
			$dayInterval     = $obInterval->days;
		}
		$this->arResult['ITEMS'][ $order_id ]['TEST'] = $order->getPropertyCollection()->getItemByOrderPropertyId(59)->getValue();

		//Флаг того, что заказ пересчитан на теукщий день, если дата отличается от даты создания заказа
		if ($dayInterval > 0 && $needRecalculate) {
			$this->arResult['ITEMS'][ $order_id ]['CALCULATED'] = false;
		} else {
			$this->arResult['ITEMS'][ $order_id ]['CALCULATED'] = true;
		}

	}

	//Запускаем компонент
	public function executeComponent() {
		global $APPLICATION, $USER, $NavNum;

		$Num        = intval($NavNum) + 1;
		$pageNumber = $this->request[ 'PAGEN_' . $Num ];


		if ($this->arParams['IS_AJAX']) {
			$APPLICATION->RestartBuffer();
		}

		$this->arResult['POST_FORM_URI'] = POST_FORM_ACTION_URI; //Ссылка POST_FORM_ACTION_URI

		if (isset($this->request['action'])) {
			if (is_callable([$this, $this->request['action'] . "Action"])) {
				try {
					call_user_func([$this, $this->request['action'] . "Action"]);
				} catch (\Exception $e) {
					$this->errors[] = $e->getMessage();
				}
			}
		}

		//Получаем статусы заказов
		$resStatus = \Bitrix\Sale\StatusLangTable::getList(array(
			'filter' => array('LID' => 'ru')
		));
		while ($row = $resStatus->fetch()) {
			$this->arResult['STATUS'][ $row['STATUS_ID'] ] = array('NAME' => $row['NAME'], 'DESCRIPTION' => $row['DESCRIPTION']);
		}


		$limit = array(
			'nPageSize'          => 5, // Укажем количество выводимых элементов
			"bDescPageNumbering" => false, // Обратная навигация или прямая
			'iNumPage'           => $pageNumber ? $pageNumber : 1,
			'bShowAll'           => false // тут как вам нравится если надо можно показывать ссылку все
		);
		$navigation = \CDBResult::GetNavParams($limit);

		$arFilter = array('USER_ID' => $USER->GetID());
		if ($this->request->get('ORDER_ID')) {
			$arFilter['ID'] =  $this->request->get('ORDER_ID');
			$this->arResult['IS_DETAIL_PAGE'] = true;
		}else {
			$this->arResult['IS_DETAIL_PAGE'] = false;
		}

		$res = \Bitrix\Sale\Internals\OrderTable::getList(array(
			'filter' => $arFilter,
			'order'  => array('ID' => 'DESC', 'DATE_INSERT' => 'DESC'),
			'limit'  => $limit['nPageSize'],
			'offset' => ($limit['iNumPage']-1) * $limit['nPageSize']
		));

		//Количество заказов у пользователя
		$resCount = \Bitrix\Sale\Internals\OrderTable::getList(array(
			'filter' => $arFilter,
			'select'  => array('COUNT'),
			'order' => ['COUNT' => 'DESC'],
			'runtime' => [
				//в качестве ключа используется поле, которое мы указали в select
				'COUNT' => [
					//тут указывается тип данных, для поля.
					//я встречал только примеры с integer и reference
					'data_type' => 'integer',

					//тут указывается функция, вроде count, max и т.п.
					//важно следить за кавычками и указывать правильные имена таблицы
					//(они обычно не совпадают с названиями HL блока)
					'expression' => ['count(`ID`)']
				]
			]
		));
		$ordersCount = $resCount->fetch();
		unset($resCount);

		while ($order = $res->fetch()) {
			$this->arResult['ITEMS'][ $order['ID'] ]         = $order;
			$this->arResult['ITEMS'][ $order['ID'] ]['DATE'] = $order['DATE_INSERT']->toString();
			$this->getCreatedOrder($order['ID']); //Получение объекта заказа по его ИД
		}


		//Постраничная навигация
		$result = new \CDBResult();
		$result->NavStart($navigation,false,true);
		$result->NavRecordCount = $ordersCount['COUNT'];
		$result->NavPageSize = $limit['nPageSize'];
		$result->bShowAll = $limit['bShowAll'];
		$result->NavPageCount = ceil($result->NavRecordCount/$result->NavPageSize);
		$result->NavPageNomer = $limit['iNumPage'];
		$NAV_STRING = $result->GetPageNavStringEx($navComponentObject,'Название постранички', 'newtravel', $limit['bShowAll']);

		$this->arResult['NAV'] = $NAV_STRING;

		$this->arResult['REQUEST'] = $this->request->toArray();

		$this->arResult['ERRORS'] = $this->errors;

		/*AJAX OR NOT AJAX*/
		if ($this->arParams['IS_AJAX']) {
			ob_start();
			$this->includeComponentTemplate();
			$this->arResponse = $this->arResult;
			ob_end_clean();

			header('Content-Type: application/json');
			echo json_encode($this->arResponse);
			$APPLICATION->FinalActions();
			die();
		} else {
			$this->includeComponentTemplate();
		}
	}


	//================ACTIONS==============//

	/**
	 * ***********************Пересчет заказа в зависимости от курса**************************
	 */
	protected function recalculateAction() {
		if (isset($this->request['order_id']) && intval($this->request['order_id']) > 0) {
			//Снова получаем заказ и сохраняем его
			//Не знаю зачем так, но без этого сумма заказа не обновляется
			$order = Bitrix\Sale\Order::load($this->request['order_id']);

			$current_rate  = 0;
			$ue_currency   = '';
			$ue_price      = 0;
			$newOrderPrice = 0;

			/** @var \Bitrix\Sale\Basket $basket */
			if ( ! ($basket = \Bitrix\Sale\Basket::loadItemsForOrder($order))) {
				throw new \Bitrix\Main\ObjectNotFoundException('Entity "Basket" not found');
			}

			$this->arResult['ITEMS'][ $this->request['order_id'] ]['HAS_BASKET_ITEMS'] = $basket->count() ? true : false;

			if ($basket->count()) {
				/** @var \Bitrix\Sale\BasketItem $item */
				foreach ($basket->getOrderableItems() as $item) {
					//Получаем цену в валюте и название валюты из свойств товара в корзине заказа
					$basketPropRes = \Bitrix\Sale\Internals\BasketPropertyTable::getList(array(
						'filter' => array(
							"BASKET_ID" => $item->getId(),
						),
					));

					$isTourProduct = false;
					while ($property = $basketPropRes->fetch()) {
						//Цена в УЕ
						if ($property['CODE'] === 'UE_PRICE') {
							$orderPaymentSystem = $order->getPaymentSystemId();
							$ue_price = $orderPaymentSystem[0] == 25 ? floatval($property['VALUE']) + (intval($property['VALUE']) * self::ONLINE_PAY_COMISSION) : intval($property['VALUE']);
						}

						//Валюта УЕ
						if ($property['CODE'] === 'UE') {
							$ue_currency = $property['VALUE'];
						}

						//Это тур
						if ($property['CODE'] === 'TOUR_ID') {
							$isTourProduct = true;
						}
					}

					if ($isTourProduct) {
						//Получаем курс валюты на текущий день
						$currencyRes = \Bitrix\Currency\CurrencyRateTable::getList(array(
							'filter' => array(
								"CURRENCY"  => $ue_currency,
								'DATE_RATE' => new \Bitrix\Main\Type\DateTime()
							),
						));

						/** @var \Bitrix\Currency\CurrencyRateTable $currency */
						$current_rate = 0;
						while ($currency = $currencyRes->fetch()) {
							$current_rate = floatval($currency['RATE']); //Курс
						}

						//Считаем новую стоимсоть заказа в зависимотсти от курса валюты + 2,5%
						//И устанавливаем новую полную цену для товара в корзине
						$newOrderPrice = $ue_price * (floatval($current_rate) + (floatval($current_rate) * 0.025));
						if ($newOrderPrice > 0) {
							$item->setPrice($newOrderPrice, true);
							$item->save();
						}
					}
				}
			}


			if ($newOrderPrice > 0) {
				//Счтиаем скидки
				$discount = $order->getDiscount();
				\Bitrix\Sale\DiscountCouponsManager::clearApply(true);
				\Bitrix\Sale\DiscountCouponsManager::useSavedCouponsForApply(true);
				$discount->setOrderRefresh(true);
				$discount->setApplyResult(array());

				$res = $basket->refreshData(array('PRICE', 'COUPONS'));

				if ( ! $res->isSuccess()) {
					$this->errors[] = $res->getErrors();
				}

				$res = $discount->calculate();
				if ( ! $res->isSuccess()) {
					$this->errors[] = $res->getErrors();
				}

				//Меняем сумму оплаты для выбранного типа оплат
				if ( ! $order->isCanceled() && ! $order->isPaid()) {
					/** @var \Bitrix\Sale\PaymentCollection $paymentCollection */
					$paymentCollection = $order->getPaymentCollection();
					/** @var \Bitrix\Sale\Payment $payment */
					foreach ($paymentCollection as $key => $payment) {
						//Если платежа 2, тогда делим новую сумму пополам
						if ($paymentCollection->count() == 2) {
							$newPaymentSumm = $newOrderPrice/2;
						}else {
							$newPaymentSumm = $newOrderPrice;
						}
						if (!$payment->isPaid()) {
							$payment->setFieldNoDemand('SUM', $newPaymentSumm);
						}
					}
				}

				//Сохраняем заказ
				if (empty($this->errors)) {
					//Обновляем занчение свойство заказа RECALCULATE_DATE, текущей датой
					foreach ($order->getPropertyCollection() as $prop) {
						/** @var \Bitrix\Sale\PropertyValue $prop */
						if ($prop->getField('CODE') === 'RECALCULATE_DATE') {
							$prop->setValue(new Date());
						}
					}

					$res = $order->save();
					if ( ! $res->isSuccess()) {
						$this->errors[] = $res->getErrors();
					}
				}

				$order->refreshData();

				$discount = $order->getDiscount();
				\Bitrix\Sale\DiscountCouponsManager::clearApply(true);
				\Bitrix\Sale\DiscountCouponsManager::useSavedCouponsForApply(true);
				$discount->setOrderRefresh(true);
				$discount->setApplyResult(array());
				/** @var \Bitrix\Sale\Basket $basket */
				if (!($basket = $order->getBasket()))
					throw new \Bitrix\Main\ObjectNotFoundException('Entity "Basket" not found');

				$res = $basket->refreshData(array('PRICE', 'COUPONS'));

				if(!$res->isSuccess())
					$this->errors[] = $res->getErrors();

				$order->save();





//				//Снова получаем список оплат с новыми суммами
//				$paymentCollection = $order->getPaymentCollection();
//				//Список созданных оплат
//				/* @var \Bitrix\Sale\Payment $payment */
//				foreach ($paymentCollection as $key => $payment) {
//					$arPaySystemServices = \Bitrix\Sale\PaySystem\Manager::getListWithRestrictions($payment);
//
//					$this->arResult['ORDER']['PAYMENTS'][ $key ]['ID']          = $payment->getPaymentSystemId();
//					$this->arResult['ORDER']['PAYMENTS'][ $key ]['NAME']        = $payment->getField('PAY_SYSTEM_NAME');
//					$this->arResult['ORDER']['PAYMENTS'][ $key ]['SUM']         = $payment->getSum();
//					$this->arResult['ORDER']['PAYMENTS'][ $key ]['DESCRIPTION'] = $arPaySystemServices[ $payment->getPaymentSystemId() ]['DESCRIPTION'];
//					$this->arResult['ORDER']['PAYMENTS'][ $key ]['NEW_WINDOW']  = $arPaySystemServices[ $payment->getPaymentSystemId() ]['NEW_WINDOW'] === 'Y' ? true : false;
//					$this->arResult['ORDER']['PAYMENTS'][ $key ]['ACTION_FILE'] = $arPaySystemServices[ $payment->getPaymentSystemId() ]['ACTION_FILE'];
//					$this->arResult['ORDER']['PAYMENTS'][ $key ]['IS_CASH']     = $arPaySystemServices[ $payment->getPaymentSystemId() ]['IS_CASH'] === 'Y' ? true : false;
//					$this->arResult['ORDER']['PAYMENTS'][ $key ]['IS_PAID']     = $payment->isPaid();
//					$this->arResult['ORDER']['PAYMENTS'][ $key ]['PAY_URL']     = htmlspecialcharsbx($this->arParams["PATH_TO_PAYMENT"]) . '?ORDER_ID=' . urlencode(urlencode($order->getField('ACCOUNT_NUMBER'))) . '&PAYMENT_ID=' . $payment->getField('ACCOUNT_NUMBER');
//
//					$paySystemService = \Bitrix\Sale\PaySystem\Manager::getObjectById($payment->getPaymentSystemId());
//					if ($arPaySystemServices[ $payment->getPaymentSystemId() ]['NEW_WINDOW'] === 'N' || $arPaySystemServices[ $payment->getPaymentSystemId() ]['ID'] == \Bitrix\Sale\PaySystem\Manager::getInnerPaySystemId()) {
//						/** @var \Bitrix\Sale\PaySystem\ServiceResult $initResult */
//						$initResult = $paySystemService->initiatePay($payment, null, \Bitrix\Sale\PaySystem\BaseServiceHandler::STRING);
//						if ($initResult->isSuccess()) {
//							$this->arResult['ORDER']['PAYMENTS'][ $key ]['BUFFERED_OUTPUT'] = $initResult->getTemplate();
//						} else {
//							$this->errors[] = $initResult->getErrorMessages();
//						}
//					}
//				}
			}
		}
	}


}

?>