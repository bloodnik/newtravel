<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Bitrix\Main\Loader;
use Newtravel\Search\Tourvisor;

Loader::includeModule("newtravel.search");

$TOUR_ID = $arResult["BASKET_ITEMS"][0]["PROPS"][13]["VALUE"];

if ( ! empty($TOUR_ID)) {

	$tv                                  = new Tourvisor();
	$arTour                              = $tv->getData("actualize", array("tourid" => $TOUR_ID));
	$arResult["BASKET_ITEMS"][0]["TOUR"] = $arTour["data"]["tour"];

	$arFlights                              = $tv->getData("actdetail", array("tourid" => $TOUR_ID));
	$arResult["BASKET_ITEMS"][0]["FLIGHTS"] = $arFlights["flights"];
}

$AdultsCount = $arResult["BASKET_ITEMS"][0]["PROPS"][1]["VALUE"]; //Количество взрослых
$ChildsCount = $arResult["BASKET_ITEMS"][0]["PROPS"][11]["VALUE"]; //Количество детей

$arResult["TOURIST_COUNT"] = $AdultsCount + $ChildsCount; //Общее количество туристов
$arResult["TOUR_ID"]       = $TOUR_ID; //ID тура
$arResult["USE_DISCOUNT"]  = $arResult["BASKET_ITEMS"][0]['PROPS'][14]['VALUE'] === "Y" ? true : false; //Доп скидка за самостоятельность