<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"ALLOW_NEW_PROFILE" => Array(
		"NAME"=>GetMessage("T_ALLOW_NEW_PROFILE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT"=>"Y",
		"PARENT" => "BASE",
	),
	"SHOW_PAYMENT_SERVICES_NAMES" => Array(
		"NAME" => GetMessage("T_PAYMENT_SERVICES_NAMES"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" =>"Y",
		"PARENT" => "BASE",
	),
	"SHOW_STORES_IMAGES" => Array(
		"NAME" => GetMessage("T_SHOW_STORES_IMAGES"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" =>"N",
		"PARENT" => "BASE",
	),
    "TOURIST_1" => Array(
        "NAME"=>"ID свойства TOURIST_1",
        "TYPE" => "STRING",
        "DEFAULT"=>"",
        "PARENT" => "BASE",
    ),
    "TOURIST_2" => Array(
        "NAME"=>"ID свойства TOURIST_2",
        "TYPE" => "STRING",
        "DEFAULT"=>"",
        "PARENT" => "BASE",
    ),
    "TOURIST_3" => Array(
        "NAME"=>"ID свойства TOURIST_3",
        "TYPE" => "STRING",
        "DEFAULT"=>"",
        "PARENT" => "BASE",
    ),
    "TOURIST_4" => Array(
        "NAME"=>"ID свойства TOURIST_4",
        "TYPE" => "STRING",
        "DEFAULT"=>"",
        "PARENT" => "BASE",
    ),
    "TOURIST_5" => Array(
        "NAME"=>"ID свойства TOURIST_5",
        "TYPE" => "STRING",
        "DEFAULT"=>"",
        "PARENT" => "BASE",
    ),
    "INFANT" => Array(
        "NAME"=>"ID свойства INFANT",
        "TYPE" => "STRING",
        "DEFAULT"=>"",
        "PARENT" => "BASE",
    ),
    "FLIGHT_DATA" => Array(
        "NAME"=>"ID свойства FLIGHT_DATA",
        "TYPE" => "STRING",
        "DEFAULT"=>"",
        "PARENT" => "BASE",
    ),
    "OFFICE_ID" => Array(
        "NAME"=>"ID свойства OFFICE",
        "TYPE" => "STRING",
        "DEFAULT"=>"",
        "PARENT" => "BASE",
    ),
    "PERSON_NAME_ID" => Array(
        "NAME"=>"ID свойства Имя плательщика",
        "TYPE" => "STRING",
        "DEFAULT"=>"",
        "PARENT" => "BASE",
    ),
    "PERSON_LAST_NAME_ID" => Array(
        "NAME"=>"ID свойства Фамилия плательщика",
        "TYPE" => "STRING",
        "DEFAULT"=>"",
        "PARENT" => "BASE",
    ),
   "PERSON_FATHER_NAME_ID" => Array(
        "NAME"=>"ID свойства Отчество плательщика",
        "TYPE" => "STRING",
        "DEFAULT"=>"",
        "PARENT" => "BASE",
    ),
   "PERSON_FULL_NAME_ID" => Array(
        "NAME"=>"ID свойства ФИО плательщика",
        "TYPE" => "STRING",
        "DEFAULT"=>"",
        "PARENT" => "BASE",
    ),
	"OPERATOR_LINK_ID" => Array(
		"NAME"=>"ID свойства ссылка на оператора",
		"TYPE" => "STRING",
		"DEFAULT"=>"",
		"PARENT" => "BASE",
	),
	"USE_COUPONS" => Array(
		"NAME" => "Использовть купоны",
		"TYPE" => "CHECKBOX",
		"DEFAULT" =>"N",
		"PARENT" => "BASE",
	),
);
?>
