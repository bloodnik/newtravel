<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>

<?
if ($arDeliv = CSaleDelivery::GetByID($arResult["ORDER"]["DELIVERY_ID"])) {
	//PR($arDeliv);
}


if ($arPaySys = CSalePaySystem::GetByID($arResult["ORDER"]["PAY_SYSTEM_ID"], $arResult["ORDER"]["PERSON_TYPE_ID"])) {
	//PR($arPaySys);
}
?>

<section class="gray-section">
	<div class="container">
		<div class="row">
			<h1 class="title"><?=GetMessage("SOA_TEMPL_ORDER_COMPLETE")?></h1>
		</div>
	</div>
	<div class="container section-inner">
		<? if ( ! empty($arResult["ORDER"])): ?>
			<div class="row">
				<div class="col-md-12">
					<h3>Для завершения бронирования необходимо провести оплату</h3>
				</div>
			</div>
			<div class="row">
				<? if ( ! empty($arResult["PAY_SYSTEM"])) : ?>
					<div class="col-md-12">
						<div class="pay_name"><?=GetMessage("SOA_TEMPL_PAY")?></div>
						<div class="col-md-3">
							<div class="thumbnail">
								<img src="<?=$arResult["PAY_SYSTEM"]["LOGOTIP"]['SRC']?>" alt="<?=$arResult["PAY_SYSTEM"]["NAME"]?>" class="img-responsive">
							</div>
						</div>
						<div class="col-md-9 payment-descr">
							<div class="paysystem_name"><?=$arResult["PAY_SYSTEM"]["NAME"]?></div>
							<div class="paysystem-descr">
								<?=$arPaySys['DESCRIPTION']?>
							</div>
							<br>
							<? if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0): ?>
								<?
								if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y") {
									?>
									<script language="JavaScript">
                                        window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
									</script>
								<?=GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))))?>
									<?
									if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE'])) {
										?><br/>
										<?=GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"])) . "&pdf=1&DOWNLOAD=Y"))?>
										<?
									}
								} else {
									if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]) > 0) {
										include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
									}
								}
								?>
							<? endif; ?>
						</div>
					</div>
				<? else: ?>
					<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br/><br/>
					<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
					<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
				<? endif; ?>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<?=GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"]))?>
					<br/><br/>
					<?=GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"]))?>
				</div>
			</div>
		<? endif; ?>
	</div>
</section>
