<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<section class="gray-section">
	<? if ($arResult["USE_DISCOUNT"]): ?>
		<div class="alert alert-warning">
			<strong>Внимание!</strong>
			<p>Уважаемый Турист! Вы выбрали вариант бронирования on-line самостоятельно. Чтобы сохранить скидку, необходимо заполнить все поля по туристам и оплатить тур.</p>
		</div>

	<? endif; ?>
	<h1 class="title">Оплата</h1>
	<div class="section-inner">
		<div class="row">
			<div class="col-xs-12 pay-data">
				<div class="oferta-wrap col-md-12">
					<div>
						Изучите наш <a href="/faq/modal_contract.php" data-toggle="modal" data-target=".ofertaModal">договор публичной оферты</a>, поставьте галочку, согласившись с условиями и перейдите к выбору способа оплаты.
						<div class="confirm-oferta">
							<label for="ofertaconfirm">
								<input type="checkbox" name="ofertaconfirm" id="ofertaconfirm" style="margin-right: 10px" checked/>
								Я ознакомлен с условиями договора оферты на туристическое обслуживаение и согласен с <a href="/about/agreement/" target="_blank">условиями</a> обработки персональных
								данных
							</label>
						</div>
						<div class="alert alert-danger" id="ofertaerror" style="display: none">Подтверите согласие с офертой!</div>
					</div>
				</div>
				<ul class="payment-list">
					<? //include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/delivery.php"); ?>

					<h5 class="title">Выберите способ оплаты:</h5>
					<script type="text/javascript">
                        function changePaySystem(param) {
                            if (BX("account_only") && BX("account_only").value == 'Y') // PAY_CURRENT_ACCOUNT checkbox should act as radio
                            {
                                if (param == 'account') {
                                    if (BX("PAY_CURRENT_ACCOUNT")) {
                                        BX("PAY_CURRENT_ACCOUNT").checked = true;
                                        BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
                                        BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');

                                        // deselect all other
                                        var el = document.getElementsByName("PAY_SYSTEM_ID");
                                        for (var i = 0; i < el.length; i++)
                                            el[i].checked = false;
                                    }
                                }
                                else {
                                    BX("PAY_CURRENT_ACCOUNT").checked = false;
                                    BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
                                    BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
                                }
                            }
                            else if (BX("account_only") && BX("account_only").value == 'N') {
                                if (param == 'account') {
                                    if (BX("PAY_CURRENT_ACCOUNT")) {
                                        BX("PAY_CURRENT_ACCOUNT").checked = !BX("PAY_CURRENT_ACCOUNT").checked;

                                        if (BX("PAY_CURRENT_ACCOUNT").checked) {
                                            BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
                                            BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
                                        }
                                        else {
                                            BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
                                            BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
                                        }
                                    }
                                }
                            }

                            submitForm();
                        }
					</script>

					<?
					if ($arResult["PAY_FROM_ACCOUNT"] == "Y") {
						$accountOnly = ($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y") ? "Y" : "N";
						?>
						<input type="hidden" id="account_only" value="<?=$accountOnly?>"/>
						<div class="bx_block w100 vertical">
							<div class="bx_element">
								<input type="hidden" name="PAY_CURRENT_ACCOUNT" value="N">
								<label for="PAY_CURRENT_ACCOUNT" id="PAY_CURRENT_ACCOUNT_LABEL" onclick="changePaySystem('account');"
								       class="<? if ($arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y")
									       echo "selected" ?>">
									<input type="checkbox" name="PAY_CURRENT_ACCOUNT" id="PAY_CURRENT_ACCOUNT" value="Y"<? if ($arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y") {
										echo " checked=\"checked\"";
									} ?>>
									<div class="bx_logotype">
										<span style="background-image:url(<?=$templateFolder?>/images/logo-default-ps.gif);"></span>
									</div>
									<div class="bx_description">
										<strong><?=GetMessage("SOA_TEMPL_PAY_ACCOUNT")?></strong>
										<p>
										<div><?=GetMessage("SOA_TEMPL_PAY_ACCOUNT1") . " <b>" . $arResult["CURRENT_BUDGET_FORMATED"]?></b></div>
										<? if ($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y"): ?>
											<div><?=GetMessage("SOA_TEMPL_PAY_ACCOUNT3")?></div>
										<? else: ?>
											<div><?=GetMessage("SOA_TEMPL_PAY_ACCOUNT2")?></div>
										<? endif; ?>
										</p>
									</div>
								</label>
								<div class="clear"></div>
							</div>
						</div>
						<?
					}
					uasort($arResult["PAY_SYSTEM"], "cmpBySort"); // resort arrays according to SORT value

					foreach ($arResult["PAY_SYSTEM"] as $arPaySystem) {
						if (strlen(trim(str_replace("<br />", "", $arPaySystem["DESCRIPTION"]))) > 0 || intval($arPaySystem["PRICE"]) > 0) {
							if (count($arResult["PAY_SYSTEM"]) == 1) {
								?>
								<div class="bx_block w100 vertical">
									<div class="bx_element">
										<input type="hidden" name="PAY_SYSTEM_ID" value="<?=$arPaySystem["ID"]?>">
										<input type="radio"
										       id="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"
										       name="PAY_SYSTEM_ID"
										       value="<?=$arPaySystem["ID"]?>"
											<? if ($arPaySystem["CHECKED"] == "Y" && ! ($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y")) {
												echo " checked=\"checked\"";
											} ?>
											   onclick="changePaySystem();"
										/>
										<label for="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>" onclick="BX('ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>').checked=true;changePaySystem();">
											<?
											if (count($arPaySystem["PSA_LOGOTIP"]) > 0):
												$imgUrl = $arPaySystem["PSA_LOGOTIP"]["SRC"];
											else:
												$imgUrl = $templateFolder . "/images/logo-default-ps.gif";
											endif;
											?>
											<div class="bx_logotype">
												<span style="background-image:url(<?=$imgUrl?>);"></span>
											</div>
											<div class="bx_description">
												<? if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"): ?>
													<strong><?=$arPaySystem["PSA_NAME"];?></strong>
												<? endif; ?>
												<p>
													<?
													if (intval($arPaySystem["PRICE"]) > 0) {
														echo str_replace("#PAYSYSTEM_PRICE#", SaleFormatCurrency(roundEx($arPaySystem["PRICE"], SALE_VALUE_PRECISION), $arResult["BASE_LANG_CURRENCY"]), GetMessage("SOA_TEMPL_PAYSYSTEM_PRICE"));
													} else {
														echo $arPaySystem["DESCRIPTION"];
													}
													?>
												</p>
											</div>
										</label>
										<div class="clear"></div>
									</div>
								</div>
								<?
							} else // more than one
							{
								//Если город вылета не Уфа, не выводим способ оплаты наличными
								if (isset($_COOKIE['NEWTRAVEL_USER_CITY']) && ! empty($_COOKIE['NEWTRAVEL_USER_CITY']) && $_COOKIE['NEWTRAVEL_USER_CITY'] != 4 && $arPaySystem["ID"] == 1) {
									continue;
								}
								?>
								<li>
									<?
									if (count($arPaySystem["PSA_LOGOTIP"]) > 0):
										$imgUrl = $arPaySystem["PSA_LOGOTIP"]["SRC"];
									else:
										$imgUrl = $templateFolder . "/images/logo-default-ps.gif";
									endif;
									?>
									<div class="col-md-2">
										<input type="radio" id="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>" name="PAY_SYSTEM_ID" value="<?=$arPaySystem["ID"]?>"
											<? if ($arPaySystem["CHECKED"] == "Y" && ! ($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y")) {
												echo " checked=\"checked\"";
											} ?>
											   onclick="changePaySystem();"/>
										<img src="<?=$imgUrl?>" alt="<?=$arPaySystem["PSA_NAME"];?>" class="hidden-xs">
									</div>
									<div class="col-md-10">
										<label for="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"
										       onclick="BX('ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>').checked=true;changePaySystem();">
											<strong>
												<? if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"): ?>
													<strong><?=$arPaySystem["PSA_NAME"];?></strong>
												<? endif; ?>
											</strong><br>
											<?
											if (intval($arPaySystem["PRICE"]) > 0) {
												echo str_replace("#PAYSYSTEM_PRICE#", SaleFormatCurrency(roundEx($arPaySystem["PRICE"], SALE_VALUE_PRECISION), $arResult["BASE_LANG_CURRENCY"]), GetMessage("SOA_TEMPL_PAYSYSTEM_PRICE"));
											} else {
												echo $arPaySystem["DESCRIPTION"];
											}
											?>
										</label>
									</div>
								</li>
								<?
							}
						}

						if (strlen(trim(str_replace("<br />", "", $arPaySystem["DESCRIPTION"]))) == 0 && intval($arPaySystem["PRICE"]) == 0) {
							if (count($arResult["PAY_SYSTEM"]) == 1) {
								?>
								<div class="bx_block horizontal">
									<div class="bx_element">
										<input type="hidden" name="PAY_SYSTEM_ID" value="<?=$arPaySystem["ID"]?>">
										<input type="radio"
										       id="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"
										       name="PAY_SYSTEM_ID"
										       value="<?=$arPaySystem["ID"]?>"
											<? if ($arPaySystem["CHECKED"] == "Y" && ! ($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y")) {
												echo " checked=\"checked\"";
											} ?>
											   onclick="changePaySystem();"
										/>
										<label for="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>" onclick="BX('ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>').checked=true;changePaySystem();">
											<?
											if (count($arPaySystem["PSA_LOGOTIP"]) > 0):
												$imgUrl = $arPaySystem["PSA_LOGOTIP"]["SRC"];
											else:
												$imgUrl = $templateFolder . "/images/logo-default-ps.gif";
											endif;
											?>
											<div class="bx_logotype">
												<span style='background-image:url(<?=$imgUrl?>);'></span>
											</div>
											<? if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"): ?>
												<div class="bx_description">
													<div class="clear"></div>
													<strong><?=$arPaySystem["PSA_NAME"];?></strong>
												</div>
											<? endif; ?>
									</div>
								</div>
								<?
							} else // more than one
							{
								?>
								<div class="bx_block horizontal">
									<div class="bx_element">

										<input type="radio"
										       id="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"
										       name="PAY_SYSTEM_ID"
										       value="<?=$arPaySystem["ID"]?>"
											<? if ($arPaySystem["CHECKED"] == "Y" && ! ($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y")) {
												echo " checked=\"checked\"";
											} ?>
											   onclick="changePaySystem();"/>

										<label for="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>" onclick="BX('ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>').checked=true;changePaySystem();">
											<?
											if (count($arPaySystem["PSA_LOGOTIP"]) > 0):
												$imgUrl = $arPaySystem["PSA_LOGOTIP"]["SRC"];
											else:
												$imgUrl = $templateFolder . "/images/logo-default-ps.gif";
											endif;
											?>
											<div class="bx_logotype">
												<span style='background-image:url(<?=$imgUrl?>);'></span>
											</div>
											<? if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"): ?>
												<div class="bx_description">
													<div class="clear"></div>
													<strong>
														<? if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"): ?>
															<?=$arPaySystem["PSA_NAME"];?>
															<?
														else:?>
															<?="&nbsp;"?>
														<? endif; ?>
													</strong>
												</div>
											<? endif; ?>

										</label>
									</div>
								</div>
								<?
							}
						}
					}
					?>
					<li class="comment-line">
						<strong>Вы можете добавить комментарий к своему заказу:</strong><br>
						<textarea name="ORDER_DESCRIPTION" id="ORDER_DESCRIPTION" cols="" rows="5" class="form-control" placeholder="Ваш комментарий..."></textarea>
						<input type="hidden" name="" value="">
					</li>

					<? if ($arParams['USE_COUPONS'] == "Y"): ?>
						<li class="comment-line">
							<br>
							<strong>Если у Вас есть код купона, введите его здесь:</strong><br>
							<div class="row">
								<div class="col-md-10">
									<input name="ORDER_COUPON" id="ORDER_COUPON" class="form-control" placeholder="введите код купона" value="<?=$arResult["ORDER_COUPON"]?>"/>
								</div>
								<div class="col-md-2">
									<input type="button" onclick="submitForm(); return false;" class="btn btn-blue coupon_activate" value="Применить"/>
								</div>
								<div class="col-md-12">
									<? if ($arResult["COUPON_MESSAGE"]): ?>
										<?=$arResult["COUPON_MESSAGE"]?>
									<? endif; ?>
								</div>
							</div>
						</li>
					<? endif; ?>
				</ul>
			</div>
		</div>
	</div>
</section>

<div class="modal fade ofertaModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content col-md-12">
		</div>
	</div>
</div>