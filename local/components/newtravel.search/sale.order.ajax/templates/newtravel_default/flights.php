<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<? if ( ! empty($arTour["FLIGHTS"])): ?>
	<section class="gray-section">
		<h1 class="title">Выбор рейса и туроператора</h1>
		<div class=" section-inner">
			<div class="row">
				<div class="col-xs-12 turoperator-data">
					<div class="col-md-4 col-sm-4 hidden-xs">
						Туроператор
					</div>
					<div class="col-md-4 col-sm-4 hidden-xs">
						Авиакомпания
					</div>
					<div class="col-md-4 col-sm-4 hidden-xs">
						Цена за номер
					</div>
					<? $prevAirCompany = ""; ?>
					<? foreach ($arTour["FLIGHTS"] as $key => $arFlight): ?>
						<?
						$checked = "";
						?>
						<? if (isset($_POST["turoperator"]) && $_POST["turoperator"] == $key): ?>
							<? $checked = "checked"; ?>
							<?
							$selectedFlight = "Авиакомпания: " . $arFlight["forward"][0]["company"]['name'] . "\r\n";
							$selectedFlight .= "Рейс туда: " . $arFlight["dateforward"] . "\r\n";
							$selectedFlight .= $arFlight["forward"][0]["departure"]["time"] . " " . $arFlight["forward"][0]["port"]["name"]
							                   . "-" . $arFlight["forward"][0]["arrival"]["time"] . " " . $arFlight["forward"][0]["arrival"]["port"]["name"] . "\r\n";
							$selectedFlight .= "Самолет: " . $arFlight["forward"][0]["plane"] . " (" . $arFlight["forward"][0]["number"] . ") \r\n";
							$selectedFlight .= "Рейс обратно: " . $arFlight["datebackward"] . "\r\n";
							$selectedFlight .= $arFlight["backward"][0]["departure"]["time"] . " " . $arFlight["backward"][0]["port"]["name"]
							                   . "-" . $arFlight["backward"][0]["arrival"]["time"] . " " . $arFlight["backward"][0]["arrival"]["port"]["name"] . "\r\n";
							$selectedFlight .= "Самолет: " . $arFlight["backward"][0]["plane"] . " (" . $arFlight["backward"][0]["number"] . ")"; ?>
						<? elseif (empty($_POST) && $key == 0): ?>
							<? $checked = "checked"; ?>
							<?
							$selectedFlight = "Авиакомпания: " . $arFlight["forward"][0]["company"]['name'] . "\r\n";
							$selectedFlight .= "Рейс туда: " . $arFlight["dateforward"] . "\r\n";
							$selectedFlight .= $arFlight["forward"][0]["departure"]["time"] . " " . $arFlight["forward"][0]["departure"]["port"]["name"]
							                   . " - " . $arFlight["forward"][0]["arrival"]["time"] . " " . $arFlight["forward"][0]["arrival"]["port"]["name"] . "\r\n";
							$selectedFlight .= "Самолет: " . $arFlight["forward"][0]["plane"] . " (" . $arFlight["forward"][0]["number"] . ") \r\n";
							$selectedFlight .= "Рейс обратно: " . $arFlight["datebackward"] . "\r\n";
							$selectedFlight .= $arFlight["backward"][0]["departure"]["time"] . " " . $arFlight["backward"][0]["departure"]["port"]["name"]
							                   . " - " . $arFlight["backward"][0]["arrival"]["time"] . " " . $arFlight["backward"][0]["arrival"]["port"]["name"] . "\r\n";
							$selectedFlight .= "Самолет: " . $arFlight["backward"][0]["plane"] . " (" . $arFlight["backward"][0]["number"] . ")";
							?>
						<? endif; ?>

						<div class="item">
							<label for="turoperator<?=$key?>">
								<div class="col-md-4 col-sm-4 col-xs-12">
									<input type="radio" id="turoperator<?=$key?>" name="turoperator" onclick="BX('turoperator<?=$key?>').checked=true;changeFlight();"
									       value="<?=$key?>" <?=$checked?> data-sub-item="sub-item<?=$key?>"/>
									<input type="hidden" name="turoperatorPrice<?=$key?>" value="<?=$arFlight["price"]["value"]?>"/>
									<input type="hidden" name="turoperatorOilTax<?=$key?>" value="<?=$arFlight["fuelcharge"]["value"]?>"/>
									<img src="https://tourvisor.ru/pics/operators/searchlogo/<?=$arTour["TOUR"]["operatorcode"]?>.gif" alt="<?=$arTour["TOURS"]["operatorname"]?>">
								</div>
								<div class="col-md-4 col-sm-4 col-xs-12">
									<?=$arFlight["forward"][0]["company"]['name']?>
									<? if ($arFlight["forward"][0]["company"]['thumb']): ?>
										<div class="img-thumbnail"><img src="<?=$arFlight["forward"][0]["company"]['thumb']?>" height="20" alt=""></div>
									<? endif; ?>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-12 price">
									<?=CurrencyFormat($arFlight["price"]["value"], "RUB")?>
								</div>
							</label>
						</div>
						<div class="sub-item" id="sub-item<?=$key?>" <? if ($checked == ""): ?>style="display: none" <? endif ?>>
							<div class="col-md-6  col-sm-6 col-xs-12 small">
								<label for="forward<?=$key?>">
									<input type="radio" id="forward<?=$key?>" <?=$checked?> name="forward<?=$key?>" value="forward<?=$key?>"/>
									<strong style="color:#00AEEF">Рейс туда</strong> - <?=$arFlight["dateforward"]?>
								</label>
								<br/>
								<strong><?=$arFlight["forward"][0]["departure"]["time"]?></strong> <?=$arFlight["forward"][0]["departure"]["port"]['name']?> -
								<strong><?=$arFlight["forward"][0]["arrival"]["time"]?></strong> <?=$arFlight["forward"][0]["arrival"]["port"]['name']?><br/>
								<span><?=$arFlight["forward"][0]["plane"]?> (<?=$arFlight["forward"][0]["number"]?>)</span>
								<br/>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12 small">
								<label for="backward<?=$key?>">
									<input type="radio" id="backward<?=$key?>" <?=$checked?> name="backward<?=$key?>" value="backward<?=$key?>"/>
									<strong style="color:#00AEEF">Рейс обратно</strong> - <?=$arFlight["datebackward"]?>
								</label>
								<br/>
								<strong><?=$arFlight["backward"][0]["departure"]["time"]?></strong> <?=$arFlight["backward"][0]["departure"]["port"]['name']?> -
								<strong><?=$arFlight["backward"][0]["arrival"]["time"]?></strong> <?=$arFlight["backward"][0]["arrival"]["port"]['name']?><br/>
								<span><?=$arFlight["backward"][0]["plane"]?> (<?=$arFlight["backward"][0]["number"]?>)</span>
							</div>
						</div>
					<? endforeach; ?>
					<input type="hidden" name="ORDER_PROP_<?=$arParams["FLIGHT_DATA"]?>" id="ORDER_PROP_<?=$arParams["FLIGHT_DATA"]?>" value="<?=$selectedFlight?>"/>
				</div>
			</div>
		</div>
	</section>

	<script type="text/javascript">
		/*=======================================================================
		 Выбор туроператора и рейса
		 ========================================================================= */
		function changeFlight() {
			$(".sub-item").slideUp(500);
			$(".sub-item input").prop("checked", false);
			var subItem = $(".turoperator-data .item input:checked").attr("data-sub-item");
			$("#" + subItem).slideDown(500);

			$("#" + subItem).find("input").prop("checked", true);

			submitForm();
		}
	</script>

<? endif; ?>