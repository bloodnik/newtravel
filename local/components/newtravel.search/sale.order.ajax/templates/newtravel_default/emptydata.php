<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<section class="gray-section">
    <div class="container">
        <h1 class="title">Нет ифонрмации о туре</h1>
    </div>
    <div class="container section-inner">
        <div class="col-xs-12">
            Информация о туре устарела или недоступна. Выберите другой тур
        </div>
    </div>
</section>
