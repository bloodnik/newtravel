<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<? if ( ! empty($arTour['TOUR'])): ?>
	<section class="gray-section">
		<h1 class="title">Данные тура</h1>
		<div class="section-inner">
			<? if ($arResult["USE_DISCOUNT"]): ?>
				<a href="#" class="tag">
					Тур со скидкой
				</a>
			<? endif; ?>
			<div class="col-xs-12 tour-data">
				<div class="tour-data-body">
					<div class="row">
						<div class="col-md-3 col-sm-3">
							<img src="<?=$arTour["TOUR"]["hotelpicturemedium"]?>" alt="<?=$arTour["TOUR"]["hotelname"]?>" class="hotelimage">
						</div>
						<div class="col-md-9 col-sm-9 text-left">
							<h3><?=$arTour["TOUR"]["hotelname"]?> <?=$arTour["TOUR"]["hotelstars"]?>* </h3>
							<span class="small"><?=$arTour["TOUR"]["countryname"]?>, <?=$arTour["TOUR"]["hotelregionname"]?></span>
							<ul class="list-inline" style="margin-bottom: 2px;">
								<li>Места в отеле: <span class="yes">Есть <i class="fa fa-check"></i></span></li>
								<li>Места на рейс: <span class="yes">Есть <i class="fa fa-check"></i></span></li>
							</ul>
							<a href="/hotel/<?=$arTour["TOUR"]["hotelcode"]?>/" class="btn btn-white" target="_blank">описание отеля</a>
							<noindex>
								<a href='javascript:void(0)' rel="nofollow"
								   class="btn btn-green btn-lg hidden-lg hidden-md hidden-sm" id="fast_order" data-toggle="modal" data-target=".fastOrderModal"
								   style="width:100%; margin-top: 5px;">
									Купить в 1 клик
								</a>
							</noindex>
						</div>
					</div>
					<div class="clearfix"></div>
					<hr>
					<div class="row">
						<div class="col-md-12"><h3 class="title">Информация о туре</h3></div>
						<div class="col-md-6 col-sm-6">
							<ul class="list-unstyled">
								<li>
									<div class="list-label">Вылет:</div>
									<strong><?=$arTour['TOUR']['departurename']?> - <?=trim($arTour['TOUR']['flydate'])?></strong>
								</li>
								<li>
									<div class="list-label">Ночей:</div>
									<strong><?=$arTour['TOUR']['nights']?></strong></li>
								<li>
									<div class="list-label">Питание:</div>
									<strong><?=$arTour['TOUR']['meal']?></strong></li>
								<li>
									<div class="list-label">Размещение:</div>
									<strong><?=$arTour['TOUR']['placement']?></strong></li>
							</ul>
						</div>
						<div class="col-md-6 col-sm-6">
							<ul class="list-unstyled">
								<li>
									<div class="list-label">Комната:</div>
									<strong><?=$arTour['TOUR']['room']?></strong></li>
								<li>
									<div class="list-label">Туроператор:</div>
									<img src="https://tourvisor.ru/pics/operators/searchlogo/<?=$arTour['TOUR']['operatorcode']?>.gif" alt="<?=$arTour['TOUR']['operatorname']?>">
								</li>
								<li>
									<div class="list-label">Цена за тур:</div>
									<span class="price original"><?=$arResult['PRICE_WITHOUT_DISCOUNT']?></span>
								</li>
								<? if ($arResult["USE_DISCOUNT"]): ?>
									<li>
										<div class="list-label">Цена за тур со скидкой:</div>
										<span class="price discount"><?=$arResult["ORDER_TOTAL_PRICE_FORMATED"]?></span>
									</li>
								<? endif; ?>
								<li>
									<small>
										* в т.ч. топливный сбор: <?=trim(CurrencyFormat($arTour['TOUR']['fuelcharge'], 'RUB'))?>
									</small>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="tour-data-footer">
					<div class="row">
						<div class="col-md-12">
							<span class="badge badge-info badge-more" onclick="$('.tour-more-info').slideToggle(200)">В стоимость тура включено <i class="fa fa-plus-circle"></i></span>
						</div>
					</div>
					<div class="row tour-more-info" style="display: none">
						<div class="col-md-6">
							<ul class="list">
								<li>- перелет туда-обратно</li>
								<li>- трансфер аэропорт-отель-аэропорт*</li>
								<li>- проживание в отеле</li>
								<li>- медицинская страховка на время поездки</li>
							</ul>
							<small>* В некоторых турах по России трансфер в туре не предоставляется, уточняйте информацию у менеджеров компании.</small>
						</div>
						<div class="col-md-6">
							<p>
								Для приобретения данного тура, требуется внести данные ниже, выбрать способ оплаты, и согласиться с условиями договора публичной оферты, если у Вас возикли трудности
								или
								вопросы, закажите обратный звонок и мы Вас проконсультируем в течении минуты.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" name="ORDER_PROP_<?=$arParams["OPERATOR_LINK_ID"]?>" id="ORDER_PROP_<?=$arParams["OPERATOR_LINK_ID"]?>" value="<?=$arResult["BASKET_ITEMS"][0]["TOUR"]["operatorlink"]?>"/>
	</section>
<? else: ?>
	<br>
	<h3 class="text-danger">ВНИМАНИЕ! Данный тур больше не актуален</h3>
	<p>Пожалуйтста <a href="/tours/">начните новый поиск</a> и выберите подходящий тур </p>
<? endif; ?>

