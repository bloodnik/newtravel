<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props_format.php");
?>
<? if ( ! $USER->IsAuthorized()): ?>
	<div class="col-xs-12 text-center">
		<div class="row">
			<div class="alert alert-info">
				Если у Вас уже есть учетная запись на сайте, пожалуйста войдите на сайт &nbsp;
				<a href="javascript:void(0)" class="btn btn-blue" data-toggle="modal" data-target="#auth_modal">Войти</a>
			</div>
		</div>
	</div>
<? endif; ?>

<!-- Профиль плательщика -->
<section class="gray-section" id="user-data">
	<h1 class="title">Данные заказчика</h1>
	<div class=" section-inner">
		<?
		if ( ! empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y") {
		foreach ($arResult["ERROR"] as $v) ?>
			<div class="alert alert-danger text-center"><? echo ShowError($v); ?></div>

			<script type="text/javascript">
				top.BX.scrollToNode(top.BX('user-data'));
			</script>
			<?
		}
		?>
		<div class="row">
			<div class="col-xs-12 sale-user-data">
				<?
				$bHideProps = true;

				if (is_array($arResult["ORDER_PROP"]["USER_PROFILES"]) && ! empty($arResult["ORDER_PROP"]["USER_PROFILES"])):
					if ($arParams["ALLOW_NEW_PROFILE"] == "Y"):
						?>
						<div class="bx_block r1x3">
							<?=GetMessage("SOA_TEMPL_PROP_CHOOSE")?>
						</div>
						<div class="bx_block r3x1">
							<select name="PROFILE_ID" id="ID_PROFILE_ID" onChange="SetContact(this.value)">
								<option value="0"><?=GetMessage("SOA_TEMPL_PROP_NEW_PROFILE")?></option>
								<?
								foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles) {
									?>
									<option value="<?=$arUserProfiles["ID"]?>"<? if ($arUserProfiles["CHECKED"] == "Y") {
										echo " selected";
									} ?>><?=$arUserProfiles["NAME"]?></option>
									<?
								}
								?>
							</select>
							<div style="clear: both;"></div>
						</div>
						<?
					else:
						?>
						<div class="bx_block r1x3" style="display: none">
							<?=GetMessage("SOA_TEMPL_EXISTING_PROFILE")?>
						</div>
						<div class="bx_block r3x1" style="display: none">
							<?
							if (count($arResult["ORDER_PROP"]["USER_PROFILES"]) == 1) {
								foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles) {
									echo "<strong>" . $arUserProfiles["NAME"] . "</strong>";
									?>
									<input type="hidden" name="PROFILE_ID" id="ID_PROFILE_ID" value="<?=$arUserProfiles["ID"]?>"/>
									<?
								}
							} else {
								?>
								<select name="PROFILE_ID" id="ID_PROFILE_ID" onChange="SetContact(this.value)">
									<?
									foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles) {
										?>
										<option value="<?=$arUserProfiles["ID"]?>"<? if ($arUserProfiles["CHECKED"] == "Y") {
											echo " selected";
										} ?>><?=$arUserProfiles["NAME"]?></option>
										<?
									}
									?>
								</select>
								<?
							}
							?>
							<div style="clear: both;"></div>
						</div>
						<?
					endif;
				else:
					$bHideProps = false;
				endif;
				?>

				<div id="sale_order_props">
					<?
					PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_N"], $arParams["TEMPLATE_LOCATION"]);
					PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"]);
					?>
					<input type="hidden" placeholder="" id="ORDER_PROP_<?=$arParams["PERSON_FULL_NAME_ID"]?>" name="ORDER_PROP_<?=$arParams["PERSON_FULL_NAME_ID"]?>" value="">
				</div>


			</div>
		</div>
	</div>
</section>

<script type="text/javascript">

	/**
	 * Устанавливаем значение скрытого свойства ФИО плательщика из полей Имя+Отчество+Фамилия
	 */
	function setPersonName() {
		var fullNameString = $("#ORDER_PROP_<?=$arParams["PERSON_NAME_ID"]?>").val() + " " + $("#ORDER_PROP_<?=$arParams["PERSON_LAST_NAME_ID"]?>").val();
		$("#ORDER_PROP_<?=$arParams["PERSON_FULL_NAME_ID"]?>").val(fullNameString);
	}


	function fGetBuyerProps(el) {
		var show = '<?=GetMessageJS('SOA_TEMPL_BUYER_SHOW')?>';
		var hide = '<?=GetMessageJS('SOA_TEMPL_BUYER_HIDE')?>';
		var status = BX('sale_order_props').style.display;
		var startVal = 0;
		var startHeight = 0;
		var endVal = 0;
		var endHeight = 0;
		var pFormCont = BX('sale_order_props');
		pFormCont.style.display = "block";
		pFormCont.style.overflow = "hidden";
		pFormCont.style.height = 0;
		var display = "";

		if (status == 'none') {
			el.text = '<?=GetMessageJS('SOA_TEMPL_BUYER_HIDE');?>';

			startVal = 0;
			startHeight = 0;
			endVal = 100;
			endHeight = pFormCont.scrollHeight;
			display = 'block';
			BX('showProps').value = "Y";
			el.innerHTML = hide;
		}
		else {
			el.text = '<?=GetMessageJS('SOA_TEMPL_BUYER_SHOW');?>';

			startVal = 100;
			startHeight = pFormCont.scrollHeight;
			endVal = 0;
			endHeight = 0;
			display = 'none';
			BX('showProps').value = "N";
			pFormCont.style.height = startHeight + 'px';
			el.innerHTML = show;
		}

		(new BX.easing({
			duration: 700,
			start: {opacity: startVal, height: startHeight},
			finish: {opacity: endVal, height: endHeight},
			transition: BX.easing.makeEaseOut(BX.easing.transitions.quart),
			step: function (state) {
				pFormCont.style.height = state.height + "px";
				pFormCont.style.opacity = state.opacity / 100;
			},
			complete: function () {
				BX('sale_order_props').style.display = display;
				BX('sale_order_props').style.height = '';
			}
		})).animate();
	}
</script>

<div style="display:none;">
	<?
	$APPLICATION->IncludeComponent(
		"bitrix:sale.ajax.locations",
		$arParams["TEMPLATE_LOCATION"],
		array(
			"AJAX_CALL"          => "N",
			"COUNTRY_INPUT_NAME" => "COUNTRY_tmp",
			"REGION_INPUT_NAME"  => "REGION_tmp",
			"CITY_INPUT_NAME"    => "tmp",
			"CITY_OUT_LOCATION"  => "Y",
			"LOCATION_VALUE"     => "",
			"ONCITYCHANGE"       => "submitForm()",
		),
		null,
		array('HIDE_ICONS' => 'Y')
	);
	?>
</div>