<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
if ($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y") {
	if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
		if (strlen($arResult["REDIRECT_URL"]) > 0) {
			$APPLICATION->RestartBuffer();
			?>
			<script type="text/javascript">
                window.top.location.href = '<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
			</script>
			<?
			die();
		}
	}
}

$this->addExternalCss($templateFolder . "/style_cart.css");
$this->addExternalCss($templateFolder . "/style.css");
$this->addExternalCss(SITE_TEMPLATE_PATH . "/js/jquery.countdown/jquery.countdown.css");


$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/daterangepicker/daterangepicker.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jquery.countdown/jquery.plugin.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jquery.countdown/jquery.countdown.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jquery.countdown/jquery.countdown-ru.js");


CJSCore::Init(array('fx', 'window', 'ajax'));

$this->setFrameMode(true);
?>
	<a name="order_form"></a>

	<div id="order_form_div" class="order-checkout col-md-9 col-xs-12">
		<NOSCRIPT>
			<div class="errortext"><?=GetMessage("SOA_NO_JS")?></div>
		</NOSCRIPT>

		<?
		if ( ! function_exists("getColumnName")) {
			function getColumnName($arHeader) {
				return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_" . $arHeader["id"]);
			}
		}

		if ( ! function_exists("cmpBySort")) {
			function cmpBySort($array1, $array2) {
				if ( ! isset($array1["SORT"]) || ! isset($array2["SORT"])) {
					return -1;
				}

				if ($array1["SORT"] > $array2["SORT"]) {
					return 1;
				}

				if ($array1["SORT"] < $array2["SORT"]) {
					return -1;
				}

				if ($array1["SORT"] == $array2["SORT"]) {
					return 0;
				}
			}
		}
		?>

		<div class="bx_order_make order-page">
			<?
			if ( ! $USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N") {
				if ( ! empty($arResult["ERROR"])) {
					foreach ($arResult["ERROR"] as $v) {
						echo ShowError($v);
					}
				} elseif ( ! empty($arResult["OK_MESSAGE"])) {
					foreach ($arResult["OK_MESSAGE"] as $v) {
						echo ShowNote($v);
					}
				}

				include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/auth.php");
			} else {
				if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
					if (strlen($arResult["REDIRECT_URL"]) == 0) {
						include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/confirm.php");
					}
				} else {
					?>
					<script type="text/javascript">
                        function numberWithCommas(x) {
                            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                        }

                        function submitForm(val) {
                            if($('.warning') !== null){
                                $('.warning').remove();
                            }

                            //Если не согласились с офертой то не оформляем заказ
                            if (val == "Y" && !$("#ofertaconfirm").prop("checked")) {
                                val = "N";
                                $("#ofertaerror").slideDown(200);

                                $("body").scrollTo(".expand-tourists");

                            }else if($('#ORDER_PROP_24').val().search(/^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))/) === -1){ //Проверка на номер телефона
                                val = "N";
                                $("body").scrollTo("#user-data");
                                $('#ORDER_PROP_24').after('<small class="text-danger warning">Введите корректный номер телефона (89279999999, +79279999999)</small>')
                            } else {
                                if (val != 'Y')
                                    BX('confirmorder').value = 'N';

                                setTouristsData(<?=$arResult["TOURIST_COUNT"]?>);
                                setPersonName();

                                var orderForm = BX('ORDER_FORM');
                                BX.showWait();
                                BX.ajax.submit(orderForm, ajaxResult);

                                return true;
                            }

                        }

                        function ajaxResult(res) {
                            try {
                                var json = JSON.parse(res);

                                BX.closeWait();

                                if (json.error) {
                                    return;
                                }
                                else if (json.redirect) {
                                    window.top.location.href = json.redirect;
                                }
                            }
                            catch (e) {
                                BX('order_form_content').innerHTML = res;
                            }

                            //Запускаем скрипты после аякса
                            $('#startBtn').click();

                            //Устанавливаем цены для блоков лушчей цены и рейса
                            var newPrice = $(".summary #price").text();
                            var newDiscount = $(".summary #discount_price").text();
							<? if ($arResult["USE_DISCOUNT"]): ?>
                            $(".aside #aside_price, .tour-data-body .discount").text(newPrice);
                            $(".tour-data-body .original").text(numberWithCommas(parseInt(newPrice.replace(' ', '')) + parseInt(newDiscount.replace(' ', ''))) + " Р");
							<?else:?>
                            $(".aside #aside_price, .tour-data-body .price").text(newPrice);
							<? endif; ?>
                            if (parseInt(newDiscount) > 0) {
                                $(".aside #aside_discount_wrap").removeClass('hidden');
                                $(".aside #aside_discount").text(newDiscount);
                            } else {
                                $(".aside #aside_discount_wrap").addClass('hidden');
                                $(".aside #aside_discount").text("0");

                            }

                            BX.closeWait();
                        }

                        function SetContact(profileId) {
                            BX("profile_change").value = "Y";
                            submitForm();
                        }
					</script>
				<? if ($_POST["is_ajax_post"] != "Y")
				{
				?>
					<form action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data">
						<?=bitrix_sessid_post()?>
						<div id="order_form_content">

							<?
							}
							else {
								$APPLICATION->RestartBuffer();
							} ?>

							<?
							$arTour = $arResult["BASKET_ITEMS"][0];

							//------------------Подключаем раздел информации об отеле----------------------//
							include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/tourdata.php");
							//------------------Подключаем раздел о туроператоре----------------------//
							include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/flights.php");
							include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/person_type.php");
							include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props.php");
							//------------------Подключаем раздел c туристами----------------------//
							include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/tourists.php");

							include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/paysystem.php");
							include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/related_props.php");
							//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");
							include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/newsummary.php");
							//else:
							//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/emptydata.php");
							//endif;

							if (strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0) {
								echo $arResult["PREPAY_ADIT_FIELDS"];
							}
							?>

							<? if ($_POST["is_ajax_post"] != "Y")
							{
							?>
						</div>

						<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
						<input type="hidden" name="profile_change" id="profile_change" value="N">
						<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
						<input type="hidden" name="json" value="Y">

						<div class="bx_ordercart_order_pay_center" style="display:none;">
							<noindex><a href="javascript:void();" rel="nofollow" onclick="submitForm('Y'); return false;" class="checkout"><?=GetMessage("SOA_TEMPL_BUTTON")?></a></noindex>
						</div>

						<script>
                            document.addEventListener("DOMContentLoaded", function (event) {
                                $('#startBtn').click(); //Запускаем скрипты после аякса
                            });
						</script>

					</form>
				<?
				if ($arParams["DELIVERY_NO_AJAX"] == "N")
				{
				?>
					<div style="display:none;"><? $APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null, array('HIDE_ICONS' => 'Y')); ?></div>
				<?
				}
				}
				else
				{
				?>
					<script type="text/javascript">
                        top.BX('confirmorder').value = 'Y';
                        top.BX('profile_change').value = 'N';
					</script>
					<?
					die();
				} ?>
					<?
				}
			}
			?>
		</div>
	</div>

<? if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "N" || $arResult["NEED_REDIRECT"] == "N") : ?>
	<div id="sticky" class="sticky-element">
		<div class="sticky-anchor"></div>
		<div class="col-md-3 hidden-xs hidden-sm aside sticky-content">
			<section class="white-section">
				<div class="help-btn-wrap pull-right">
					<a href="#callbackhunter" onclick="yaCounter24395911.reachGoal('ORDER_HELP')" class="help-btn pull-right"><i class="fa fa-volume-control-phone"></i></a>
					<span>Остались вопросы?<br>Свяжись со специалистом!</span>
				</div>
				<div class="clearfix"></div>

				<div class="row">
					<h3 class="title">Гарантия лучшей цены</h3>
				</div>

				<div class="text-center text-warning">
					Успей забронировать тур по самой низкой цене!
				</div>
				<div class="timer text-center">
					<div class="defaultCountdown">
					</div>
				</div>
				<br>
				<ul class="list">
					<li>
						<span class="title">Всего к оплате: </span>
						<span class="value aside_price" id="aside_price"><?=CurrencyFormat($arTour["PRICE"], 'RUB')?></span>
					</li>

					<li class="hidden" id="aside_discount_wrap">
						<span class="title">Доп. скидка:</span>
						<span class="value aside_price" id="aside_discount"><?=trim(CurrencyFormat($arTour['DISCOUNT_PRICE'], 'RUB'))?></span>
					</li>


				</ul>
				<button onclick="submitForm('Y'); return false;" class="btn btn-blue btn-lg" style="width: 100%;">
					Забронировать
				</button>
				<br><br>
				<button class="btn btn-green btn-lg" id="fast_order" data-toggle="modal" data-target=".fastOrderModal" style="width: 100%;">
					Заказать в 1 клик
				</button>
			</section>
		</div>
	</div>
	<div class="modal fade fastOrderModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="fast_order_wrap" style="padding: 35px;">
					<? $APPLICATION->IncludeComponent(
						"newtravel:fast.order.form",
						"modal",
						Array(
							"CACHE_TIME"           => "3600",
							"CACHE_TYPE"           => "A",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
							"TOUR_ID"              => $arResult["TOUR_ID"],
						)
					); ?>
				</div>
			</div>
		</div>
	</div>
<? endif; ?>