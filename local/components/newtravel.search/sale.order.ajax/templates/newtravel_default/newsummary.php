<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<section class="gray-section">
	<div class="container section-inner">
		<div class="row">
			<div class="col-xs-12 pay-data">
				<div class="summary">
					<div class="col-md-6">
						<span class="itogo">Итого к оплате:</span>
						<span
							class="price" id="price"><?=$arResult["ORDER_TOTAL_PRICE_FORMATED"]?></span> <? if (isset($arResult["BANK_MARGIN"])): ?>(в том числе <?=CurrencyFormat($arResult["BANK_MARGIN"], "RUB")?> комиссия банка)<? endif; ?>
						<br/>

						<? if ($arTour['DISCOUNT_PRICE'] > 0): ?>
							<span class="itogo">Доп. скидка:</span>
							<span class="price" id="discount_price"><?=trim(CurrencyFormat($arTour['DISCOUNT_PRICE'], 'RUB'))?></span>
						<? endif; ?>
						
						<p class="small">
							Внимание! Данный тура требует дополнительной проверки менеджером. <br/>
							Окончательная стоимость тура может быть скорректирована.
						</p>
					</div>
					<div class="col-md-6">
						<input type="hidden" id="ORDER_PROP_55" name="ORDER_PROP_55" value="<?=isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null?>">
						<input type="button" onclick="submitForm('Y'); return false;" class="btn btn-blue btn-lg pull-right col-xs-12 col-md-6" value="ЗАБРОНИРОВАТЬ"/>
						<noindex>
							<a href='javascript:void(0)' rel="nofollow"
							   class="btn btn-green btn-lg hidden-lg hidden-md hidden-sm" id="fast_order" data-toggle="modal" data-target=".fastOrderModal"
							   style="width:100%; margin-top: 5px;">
								Купить в 1 клик
							</a>
						</noindex>
						<a href="" style="display: none" id="startBtn" onclick="startScripts()">Запустить</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>