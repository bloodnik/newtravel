$(document).ready(function () {

    var StickyElement = function (node) {
        var doc = $(document),
            fixed = false,
            anchor = node.find('.sticky-anchor'),
            content = node.find('.sticky-content'),
            summary = $('.footer');


        var onScroll = function (e) {
            var docTop = doc.scrollTop(),
                anchorTop = anchor.offset().top,
                footerTop = summary.offset().top - content.height(),
                contentBottom = content.offset().top + 300;

            if (docTop > anchorTop) {
                if (!fixed) {
                    anchor.height(content.outerHeight());
                    content.addClass('fixed');
                    fixed = true;
                }
                if (contentBottom >= footerTop) {
                    if (fixed) {
                        content.addClass('disable');
                    }
                } else {
                    content.removeClass('disable');
                }

            } else {
                if (fixed) {
                    anchor.height(0);
                    content.removeClass('fixed');
                    fixed = false;
                }
            }
        };

        $(window).on('scroll', onScroll);
    };
    var demo = new StickyElement($('#sticky'));


    /*=========================================================================
     countdown
     ========================================================================== */
    var newYear = new Date();
    newYear = new Date(newYear.getFullYear() + 1, 1 - 1, 1);
    $('.defaultCountdown').countdown({
        since: '',
        compact: true,
        layout: "<span>{mnn}</span>{sep}<span>{snn}</span>"
    });

    $('#fast_order').popover({
        trigger: "hover",
        title: "УДОБНО!",
        placement: 'top',
        content: 'Оставьте только номер телефона, и наш менеджер все сделает за Вас!'
    });

    /*=========================================================================
     help-btn hover
     ========================================================================== */
    var helpbtn = $('.help-btn');
    var helpmsg = $('.help-btn-wrap>span');
    helpbtn.mouseover(function () {
        helpmsg.addClass('active');
    })
    helpbtn.mouseout(function () {
        helpmsg.removeClass('active');
    })


});