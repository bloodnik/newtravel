<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Newtravel\Search\Tourvisor;
use Newtravel\Search\Helpers;
use Bitrix\Main\Loader;
use Bitrix\Main\Application;

class CTourDetail extends CBitrixComponent {

	//Запускаем компонент
	public function executeComponent() {
		// Подключение модуля
		Loader::includeModule("iblock");
		Loader::includeModule("catalog");
		Loader::includeModule("sale");
		Loader::includeModule("newtravel.search");

		$this->arResult["TOUR_ID"] = $this->arParams["TOUR_ID"];
		$tv                        = new Tourvisor();


		if ($this->startResultCache(3600, $this->arResult["TOUR_ID"])) {

			$arTour                     = $tv->getData("actualize", array("tourid" => $this->arParams['TOUR_ID']));
			$this->arResult["TOUR"]     = $arTour["data"]['tour'];
			$arActTour                  = $tv->getData("actdetail", array("tourid" => $this->arParams['TOUR_ID']));
			$this->arResult["FLIGHTS"] = $arActTour;

			$this->arResult["TOUR"]['DISCOUNT_PRICE'] = Helpers::getDiscountPrice($this->arResult["TOUR"]['price'], 1);

			$this->includeComponentTemplate();
		}
	}
}

?>