<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>
<style>
	#tourActulizeModal .modal-header {
		padding: 5px 15px;
	}

	#tourActulizeModal .modal-title {
		padding-bottom: 0px;
		font-size: 24px;
	}

	.tourdetail .short-section-title {
		margin: 0 0 10px 0;
	}

	.tourdetail h3 {
		padding: 0 0 5px;
	}

	.tourdetail .project-OverView ul > li {
		font-size: 14px;
	}

	.tourdetail .list-label {
		font-size: 14px;
		padding: 0;
		display: inline-block;
		width: 150px;
	}

	.tourdetail .price {
		font-size: 24px !important;
		color: #00aeef !important;
		font-weight: normal !important;
		display: inline-block;
		padding: 0;
		margin: 5px 0
	}

	#checkoutBtn {
		width: 150px;
		margin-left: 10px;
	}

	#checkoutBtnDiscount {
		font-size: 18px;
		border-radius: 0;
	}

	.tourdetail .turoperator-data .col-md-4 {
		text-align: center;
	}

	.tourdetail .turoperator-data .item {
		margin-top: 15px;
		padding: 5px 10px;
		background: rgba(0, 174, 239, 0.3);
		display: block;
		overflow: hidden;
		font-size: 20px;
		position: relative;
		-webkit-box-shadow: 1px 2px 5px #ccc;
		-moz-box-shadow: 1px 2px 5px #ccc;
		box-shadow: 1px 2px 5px #ccc;
	}

	.tourdetail .turoperator-data .item:hover {
		background: rgba(0, 174, 239, 0.39);
	}

	.tourdetail .turoperator-data .item label {
		display: block;
	}

	.tourdetail .turoperator-data .item .price {
		font-size: 28px;
		color: #00AEEF;
		font-weight: bold;
		padding: 0;
	}

	.tourdetail .turoperator-data .sub-item {
		margin: 0 15px 5px 15px;
		background: #f0f0f0;
		text-align: center;
		padding: 5px 10px;
		overflow: hidden;
	}

	.tourdetail .turoperator-data .sub-item label {
		margin: 0;
	}

	.tourdetail .turoperator-data .sub-item .small > span {
		color: #a2a2a2;
	}

	.tourdetail .turoperator-data .sub-item .small > span {
		color: #a2a2a2;
	}

	#fast_order {
		margin-bottom: 5px;
	}

	@media only screen and (max-width: 479px) {
		#tourActulizeModal .modal-title {
			font-size: 18px;
		}

		#checkoutBtn {
			width: 174px;
			margin-left: 0;
		}

		.tourdetail .turoperator-data .col-md-4 {
			margin-bottom: 10px;
		}
	}
</style>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel">Тур <?=$arResult['TOUR']['tourname']?></h4>
</div>
<div class="modal-body tourdetail">
	<div class="row">
		<div class="col-md-3 col-sm-3">
			<img src="<?=$arResult['TOUR']['hotelpicturemedium']?>" alt="" width="100%"/>
		</div>

		<div class="col-md-9 col-sm-9">
			<div class="project-OverView">
				<h3>
					<?=$arResult['TOUR']['hotelname'] . " " . $arResult['TOUR']['hotelstars']?>*
					<p>
						<small><?=$arResult['TOUR']['countryname']?> / <?=$arResult['TOUR']['hotelregionname']?></small>
					</p>
				</h3>
				<ul class="list-inline">
					<li>Места в отеле: <span class="yes">Есть <i class="fa fa-check"></i></span></li>
					<li>Места на рейс: <span class="yes">Есть <i class="fa fa-check"></i></span></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<hr>
	<div class="row">
		<div class="col-md-12"><h3 class="title">Информация о туре</h3></div>
		<div class="col-md-6 col-sm-6">
			<ul class="list-unstyled">
				<li>
					<div class="list-label">Вылет:</div>
					<strong><?=$arResult['TOUR']['departurename']?> - <?=trim($arResult['TOUR']['flydate'])?></strong>
				</li>
				<li>
					<div class="list-label">Ночей:</div>
					<strong><?=$arResult['TOUR']['nights']?></strong></li>
				<li>
					<div class="list-label">Питание:</div>
					<strong><?=$arResult['TOUR']['meal']?></strong></li>
				<li>
					<div class="list-label">Размещение:</div>
					<strong><?=$arResult['TOUR']['placement']?></strong></li>
			</ul>
		</div>

		<div class="col-md-6 col-sm-6">
			<ul class="list-unstyled">
				<li>
					<div class="list-label">Комната:</div>
					<strong><?=$arResult['TOUR']['room']?></strong></li>
				<li>
					<div class="list-label">Туроператор:</div>
					<img src="https://tourvisor.ru/pics/operators/searchlogo/<?=$arResult['TOUR']['operatorcode']?>.gif" alt="<?=$arResult['TOUR']['operatorname']?>">
				</li>
				<li>
					<div class="list-label">Цена за тур:</div>
					<span class="price"><?=trim(CurrencyFormat($arResult['TOUR']['price'], 'RUB'))?></span>
				</li>
				<li>
					<small>
						* в т.ч. топливный сбор: <?=trim(CurrencyFormat($arResult['TOUR']['fuelcharge'], 'RUB'))?>
					</small>
				</li>
			</ul>
		</div>
		<div class="col-md-12 text-center">
			<div class="well well-sm">
				Забронируй сейчас on-line <strong>самостоятельно</strong>, по цене<br>
				<button id="checkoutBtnDiscount" class="btn btn-warning"><?=CurrencyFormat($arResult['TOUR']['DISCOUNT_PRICE'], 'RUB');?></button>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<? if ( ! empty($arResult["FLIGHTS"]['flights'])): ?>
				<hr>
				<h3 class="title">Информация рейсах</h3>
				<div class=" section-inner">
					<div class="row">
						<div class="col-xs-12 turoperator-data">
							<div class="col-md-4 col-sm-4 hidden-xs">
								Туроператор
							</div>
							<div class="col-md-4 col-sm-4 hidden-xs">
								Авиакомпания
							</div>
							<div class="col-md-4 col-sm-4 hidden-xs">
								Цена за тур
							</div>
							<? foreach ($arResult["FLIGHTS"]["flights"] as $key => $arFlight): ?>
								<div class="item">
									<label for="turoperator<?=$key?>" onclick="changeFlight('<?=$key?>');">
										<div class="col-md-4 col-sm-4 col-xs-12">
											<img src="https://tourvisor.ru/pics/operators/searchlogo/<?=$arResult["TOUR"]["operatorcode"]?>.gif" alt="<?=$arResult["TOURS"]["operatorname"]?>">
										</div>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<?=$arFlight["forward"][0]["company"]['name']?>
											<? if ($arFlight["forward"][0]["company"]['thumb']): ?>
												<div class="img-thumbnail"><img src="<?=$arFlight["forward"][0]["company"]['thumb']?>" height="20" alt=""></div>
											<? endif; ?>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-12 price">
											<?=CurrencyFormat($arFlight["price"]["value"], "RUB")?>
										</div>
									</label>
								</div>
								<div class="sub-item" id="sub-item<?=$key?>" <?if($key >= 1):?>style="display: none;" <?endif?>>
									<div class="col-md-6 col-sm-6 small">
										<label for="forward<?=$key?>">
											<strong style="color:#00AEEF">Туда</strong> - <?=$arFlight["dateforward"]?>
										</label>
										<br/>
										<strong><?=$arFlight["forward"][0]["departure"]["time"]?></strong> <?=$arFlight["forward"][0]["departure"]["port"]['name']?> -
										<strong><?=$arFlight["forward"][0]["arrival"]["time"]?></strong> <?=$arFlight["forward"][0]["arrival"]["port"]['name']?><br/>
										<span><?=$arFlight["forward"][0]["plane"]?> (<?=$arFlight["forward"][0]["number"]?>)</span>
										<br/>
									</div>

									<div class="col-md-6 col-sm-6 small">
										<label for="backward<?=$key?>">
											<strong style="color:#00AEEF">Обратно</strong> - <?=$arFlight["datebackward"]?>
										</label>
										<br/>
										<strong><?=$arFlight["backward"][0]["departure"]["time"]?></strong> <?=$arFlight["backward"][0]["departure"]["port"]['name']?> -
										<strong><?=$arFlight["backward"][0]["arrival"]["time"]?></strong> <?=$arFlight["backward"][0]["arrival"]["port"]['name']?><br/>
										<span><?=$arFlight["backward"][0]["plane"]?> (<?=$arFlight["backward"][0]["number"]?>)</span>
									</div>
								</div>
							<? endforeach; ?>
							<?if(count($arResult["FLIGHTS"]["flights"]) > 1):?>
								<div class="col-md-12 text-right">
									<small>* выбрать вариант рейса можно на странице оформления заказа</small>
								</div>
							<?endif;?>
						</div>
					</div>
				</div>
				<script>
					function changeFlight(key) {
						$(".sub-item").slideUp(500);
						$("#sub-item" + key).slideDown(500);
					}
				</script>
			<? endif; ?>
		</div>
	</div>
</div>

<div class="modal-footer">
	<div class="fast_order_wrap" style="display: none">
		<? $APPLICATION->IncludeComponent(
			"newtravel:fast.order.form",
			"",
			Array(
				"CACHE_TIME"           => "3600",
				"CACHE_TYPE"           => "A",
				"COMPOSITE_FRAME_MODE" => "A",
				"COMPOSITE_FRAME_TYPE" => "AUTO",
				"TOUR_ID"              => $arParams['TOUR_ID'],
			)
		); ?>
	</div>

	<div class="buttons_wrap">
		<button class="btn btn-green col-xs-12 col-sm-4 col-md-4" id="fast_order">Заказать в 1 клик</button>
		<button type="button" id="checkoutBtn" class="btn btn-blue">Забронировать</button>
		<button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>

		<a href="/personal/order/make/" style="display: none;" target="_blank" id="link">dgfdgdfgdf</a><br/>
	</div>
</div>

<script>
	$(document).ready(function () {
		$('#fast_order').popover({
			trigger: "hover",
			title: "УДОБНО!",
			placement: 'top',
			content: 'Оставьте только номер телефона, и наш менеджер все сделает за Вас!'
		});
	});


    //Нажимаем на "Оформить заказ"
    $('#checkoutBtn').on('click', function () {
        $(this).html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
        $.get('/local/tools/newtravel.search.buytour.php', {buy: "Y", tourid:<?=$arParams['TOUR_ID']?>}, function (data) {
            if (data == "success") {
                $("#checkoutBtn").text("Оформить заказ");
            }
        });
        setTimeout(function () {
            el = document.getElementById('link');
            el.click();
        }, 1000);
    });

	//Нажимаем на "Оформить заказ со скдикой"
	$("#checkoutBtnDiscount").on("click", function () {
	    var text= $(this).text();
		$(this).html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$.get('/local/tools/newtravel.search.buytour.php', {buy: "Y", tourid:<?=$arParams['TOUR_ID']?>, usediscount:"Y"}, function (data) {
			if (data == "success") {
				$("#checkoutBtnDiscount").text(text);
			}
		});
        setTimeout(function () {
            el = document.getElementById('link');
            el.click();
        }, 1000);
	});

	//Нажимаем на "Забронировать"
	$("#fast_order").on('click', function () {
		$('.buttons_wrap').slideUp(200);
		$('.fast_order_wrap').slideDown(200);
	});

	//Нажимаем на "Отмена"
	$("#cancel").on('click', function () {
		$('.buttons_wrap').slideDown(200);
		$('.fast_order_wrap').slideUp(200);
	})
</script>