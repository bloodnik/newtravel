<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use Bitrix\Main\Application;
use Newtravel\Search\Tourvisor;

class COferta extends CBitrixComponent {

	//Запускаем компонент
	public function executeComponent() {

		$request = Application::getInstance()->getContext()->getRequest();

		$tourid = $request->get('tourid');

		if(isset($tourid)) {
			\Bitrix\Main\Loader::includeModule('newtravel.search');
			\Bitrix\Main\Loader::includeModule('iblock');
			$tv = new \Newtravel\Search\Tourvisor();

			//Получаем иформацию о туре
			$data = $tv->getData('actualize', array('tourid'=>$tourid));
			$this->arResult['TOUR'] = $data['data']['tour'];

			//Получаем описание туроператора из инфоблока
			$arSelect = Array("ID", "NAME", "PREVIEW_TEXT");
			$arFilter = Array("IBLOCK_ID"=>IntVal($this->arParams['OPERATOR_IBLOCK_ID']), "CODE" => $this->arResult['TOUR']['operatorcode']);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while($arFields = $res->GetNext())
			{
				$this->arResult['OPERATOR'] = $arFields;
			}
		}

		$this->includeComponentTemplate();

	}
}

;
?>