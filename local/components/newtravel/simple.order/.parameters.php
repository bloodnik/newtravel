<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}


$arComponentParameters = array(
	"GROUPS"     => array(),
	"PARAMETERS" => array(
		"REDIRECT_URL" => array(
			"PARENT" => "BASE",
			"NAME" => "Адрес куда направить, если корзина пустая",
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"CACHE_TIME"   => array("DEFAULT" => 36000000)
	),
);