<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
use \Bitrix\Main,
	\Bitrix\Main\Localization\Loc as Loc,
	Bitrix\Main\Loader,
	Bitrix\Main\Config\Option,
	Bitrix\Sale\Delivery,
	Bitrix\Sale\PaySystem,
	Bitrix\Sale,
	Bitrix\Sale\Order,
	Bitrix\Sale\DiscountCouponsManager,
	Bitrix\Main\Context,
	Bitrix\Sale\Internals\BasketTable,
	Bitrix\Main\Application;

class CSimpleOrder extends CBitrixComponent {

	/** @var Order $order */
	private $order;
	private $siteId;
	private $paySystemId = 18;


	/**
	 * Создание объекта корзины
	 */
	protected function createOrder() {
		global $USER;

		DiscountCouponsManager::init();
		$order = Order::create($this->siteId, $USER->GetID());
		//$order = Order::create($this->siteId, \CSaleUser::GetAnonymousUserID());
		$order->setPersonTypeId(1);
		$this->order = $order;
	}

	/**
	 * Корзина
	 */
	protected function setBasket() {
		$basket = Sale\Basket::loadItemsForFUser(\CSaleBasket::GetBasketUserID(), $this->siteId)->getOrderableItems();

		//Элементы корзины
		$basketIterator                 = BasketTable::getList(array(
			'filter' => array('=FUSER_ID' => \CSaleBasket::GetBasketUserID(), '=ORDER_ID' => null)
		));
		$this->arResult['BASKET_ITEMS'] = [];
		while ($basketItem = $basketIterator->fetch()) {
			$this->arResult['BASKET_ITEMS'][] = $basketItem;
		}

		$this->order->setBasket($basket);
	}

	/**
	 * Отгрузки
	 */
	protected function setShipment() {
		$order = &$this->order;

		/** @var Sale\Shipment $shipment */
		/** @var Sale\ShipmentItem $shipmentItem */
		/** @var Sale\ShipmentCollection $shipmentCollection */
		$shipmentCollection     = $order->getShipmentCollection();
		$shipment               = $shipmentCollection->createItem();
		$shipmentItemCollection = $shipment->getShipmentItemCollection();
		$shipment->setField('CURRENCY', $order->getCurrency());
		foreach ($order->getBasket() as $item) {
			$shipmentItem = $shipmentItemCollection->createItem($item);
			$shipmentItem->setQuantity($item->getQuantity());
		}
		$arDeliveryServiceAll = Delivery\Services\Manager::getRestrictedObjectsList($shipment);
		$shipmentCollection   = $shipment->getCollection();

		if ( ! empty($arDeliveryServiceAll)) {
			reset($arDeliveryServiceAll);
			$deliveryObj = current($arDeliveryServiceAll);

			if ($deliveryObj->isProfile()) {
				$name = $deliveryObj->getNameWithParent();
			} else {
				$name = $deliveryObj->getName();
			}

			$shipment->setFields(array(
				'DELIVERY_ID'   => $deliveryObj->getId(),
				'DELIVERY_NAME' => $name,
				'CURRENCY'      => $order->getCurrency()
			));

			$shipmentCollection->calculateDelivery();
		}
	}

	/**
	 * Пл системы
	 *
	 * @param $paySystemId
	 */
	protected function setPayment($paySystemId) {
		$order = &$this->order;

		/*Payment*/
		$arPaySystemServiceAll = [];
		$paymentCollection     = $order->getPaymentCollection();

		$remainingSum = $order->getPrice() - $paymentCollection->getSum();

		/** @var Sale\Payment $extPayment */
		if ($remainingSum > 0 || $order->getPrice() == 0) {
			$extPayment          = $paymentCollection->createItem();
			$arPaySystemServices = PaySystem\Manager::getListWithRestrictions($extPayment);

			$arPaySystemServiceAll += $arPaySystemServices;

			if (array_key_exists($paySystemId, $arPaySystemServiceAll)) {
				$arPaySystem = $arPaySystemServiceAll[ $paySystemId ];
			} else {
				reset($arPaySystemServiceAll);

				$arPaySystem = current($arPaySystemServiceAll);
			}

			if ( ! empty($arPaySystem)) {
				$extPayment->setFields(array(
					'PAY_SYSTEM_ID'   => $arPaySystem["ID"],
					'PAY_SYSTEM_NAME' => $arPaySystem["NAME"]
				));

				$extPayment->setField('SUM', $order->getPrice());
			} else {
				$extPayment->delete();
			}
		}

		$this->paymentImages($arPaySystemServiceAll);

		$unusedPS = array(3, 17, 19); //Не используемые платежные системы
		foreach ($arPaySystemServiceAll as $id => $paySystem) {
			if(in_array($id, $unusedPS))
				unset($arPaySystemServiceAll[$id]);
		}
		$this->arResult['PAY_SYSTEMS'] = $arPaySystemServiceAll;


	}

	/**
	 * Сохранение заказа
	 * @var Order $order
	 */
	protected function saveOrder() {
		$order = &$this->order;

		$order->doFinalAction(true);
		$propertyCollection = $order->getPropertyCollection();

		foreach ($this->arResult['REQUEST'] as $requestName => $requestVal) {
			if ($propCode = strstr($requestName, '_ORDER_PROP', true)) {
				$emailProperty = $this->getPropertyByCode($propertyCollection, $propCode);
				$emailProperty->setValue($requestVal);
			}
		}

		$currencyCode = Option::get('sale', 'default_currency', 'RUB');
		$order->setField('CURRENCY', $currencyCode);
		$order->setField('USER_DESCRIPTION', $this->arResult['REQUEST']['USER_DESCRIPTION']);
		$order->save();

		return $order->getId();
	}

	/**
	 * Получение заказа
	 *     * @var Order $order
	 */
	protected function getOrder($orderId = 0) {
		if ($orderId > 0) {
			$order = Sale\Order::load($orderId);

			$this->arResult['ORDER']['ID']    = $order->getId();
			$this->arResult['ORDER']['PRICE'] = $order->getPrice();
			$this->arResult['ORDER']['PAYED'] = $order->isPaid();

			PR($order->getPropertyCollection()->getPhone()->getValue());

			$paymentCollection = $order->getPaymentCollection();

			/* @var Sale\Payment $payment */
			foreach ($paymentCollection as $key => $payment) {

				$arPaySystemServices = PaySystem\Manager::getListWithRestrictions($payment);
				$this->arResult['ORDER']['PAY_SYSTEM'][ $key ]['ID']      = $payment->getPaymentSystemId();
				$this->arResult['ORDER']['PAY_SYSTEM'][ $key ]['NAME']    = $payment->getField('PAY_SYSTEM_NAME');
				$this->arResult['ORDER']['PAY_SYSTEM'][ $key ]['SUM']     = $payment->getSum();
				$this->arResult['ORDER']['PAY_SYSTEM'][ $key ]['DESCRIPTION']     = $arPaySystemServices[$payment->getPaymentSystemId()]['DESCRIPTION'];
				$this->arResult['ORDER']['PAY_SYSTEM'][ $key ]['PAY_URL'] = htmlspecialcharsbx($this->arParams["PATH_TO_PAYMENT"]) . '?ORDER_ID=' . urlencode(urlencode($order->getField('ACCOUNT_NUMBER'))) . '&PAYMENT_ID=' . $payment->getField('ID');
			}
		}
	}

	/**
	 * @param $email
	 * Авторизация по EMAIL
	 *
	 * @return bool
	 */
	private function doAuthByEmail($email) {
		global $USER;
		$rsUsers = CUser::GetList($by = "", $order = "", array('=EMAIL' => $email), array('EXTERNAL_AUTH_ID'));

		if ($arUser = $rsUsers->GetNext()) {
			if (isset($arUser['EXTERNAL_AUTH_ID']) && ! empty($arUser['EXTERNAL_AUTH_ID'])) {
				return false;
			} else {
				$USER->Authorize($arUser['ID']);

				return true;
			}
		}
	}

	/**
	 * Регистрация нового пользователя
	 */
	private function registerUser() {
		global $USER;
		$email = $this->arResult['REQUEST']['EMAIL_ORDER_PROP'];

		$success_auth = $this->doAuthByEmail($email);

		if ( ! $success_auth) {
			$arEmail = explode("@", $email);
			$pass = $this->generatePassword();

			$user     = new CUser;
			$arFields = Array(
				"NAME"            => $this->arResult['REQUEST']['NAME_ORDER_PROP'],
				"EMAIL"            => $email,
				"LOGIN"            => $arEmail[0],
				"LID"              => "ru",
				"ACTIVE"           => "Y",
				"GROUP_ID"         => array(3),
				"PASSWORD"         => $pass,
				"CONFIRM_PASSWORD" => $pass,
			);

			$USER_ID = $user->Add($arFields);

			if ($USER_ID > 0) {
				$USER->Authorize($USER_ID);
				if ($USER->IsAuthorized()) {
					CUser::SendUserInfo($USER->GetID(), SITE_ID, Loc::getMessage("INFO_REQ"), true);
				}
			} else {
				$this->addError("Ошибка регистрации нового пользователя" . (strlen($user->LAST_ERROR) > 0 ? ": " . $user->LAST_ERROR : ""));
			}
		}
	}


	//Запускаем компонент
	public function executeComponent() {
		if ( ! Loader::IncludeModule('sale')) {
			die();
		}
		global $APPLICATION;
		global $USER;

		$request = Application::getInstance()->getContext()->getRequest();

		//Нажали на кнопку оформить
		$saveOrder = strlen($request->getPost('SAVE_ORDER')) > 0 && $request->getPost('SAVE_ORDER') == "Y";
		//Номер заказа из get параметра, после оформления заказа
		$orderId                           = strlen($request->get('ORDER_ID')) > 0 ? intval($request->get('ORDER_ID')) : 0;
		$this->arResult['ORDER_ID']        = $orderId;
		$this->arResult['ERRORS']          = [];
		$this->arResult['ORDER']           = [];
		$this->arParams['PATH_TO_PAYMENT'] = '/personal/order/payment/';

		if ($request->isAjaxRequest()) {
			$APPLICATION->RestartBuffer();
		}

		$this->siteId = \Bitrix\Main\Context::getCurrent()->getSite();

		$this->arResult['REQUEST'] = [];

		//Отправили форму. Назначаем платежную систему, пересчитываем заказ.
		//Если нажали "оформить", тогда проверяем на существование пользователя по email, или регистрируем нового пользователя
		if ($request->isPost()) {
			$this->paySystemId                    = $request->getPost('PAY_SYSTEM');
			$this->arResult['REQUEST']            = $request->getPostList()->toArray();
			$this->arResult['CURRENT_PAY_SYSTEM'] = intval($request->getPost('PAY_SYSTEM'));

			if ($saveOrder) {
				if ( ! $USER->IsAuthorized()) {
					if (strlen($this->arResult['REQUEST']['EMAIL_ORDER_PROP']) > 0 && check_email($this->arResult['REQUEST']['EMAIL_ORDER_PROP'])) {
						$this->registerUser();
					} else {
						$this->addError("Введите корректный email");
					}
				}
			}
		}

		//Устанавливаем параметры заказа, если заказ еще не сохранен
		if ($orderId == 0) {
			$this->createOrder();
			$this->setBasket();
			$this->setShipment();
			$this->setPayment($this->paySystemId);

			$this->arResult['CUR_PAGE']                            = $APPLICATION->GetCurPage();
			$this->arResult['CURRENT_PAY_SYSTEM']                  = $this->paySystemId;
			$this->arResult['ORDER_PROPS']                         = $this->order->getPropertyCollection()->getArray();
			$this->arResult['TOTAL_SUM']                           = $this->order->getBasket()->getPrice();
			$this->arResult['TOTAL_SUM_WITHOUT_DISCOUNTS']         = $this->order->getBasket()->getBasePrice();
			$this->arResult['DISCOUNT']                            = $this->arResult['TOTAL_SUM'] - $this->arResult['TOTAL_SUM_WITHOUT_DISCOUNTS'];
			$this->arResult['DISPLAY_TOTAL_SUM']                   = CCurrencyLang::CurrencyFormat($this->arResult['TOTAL_SUM'], 'RUB');
			$this->arResult['DISPLAY_TOTAL_SUM_WITHOUT_DISCOUNTS'] = CCurrencyLang::CurrencyFormat($this->arResult['TOTAL_SUM_WITHOUT_DISCOUNTS'], 'RUB');
			$this->arResult['DISPLAY_DISCOUNT']                    = CCurrencyLang::CurrencyFormat($this->arResult['DISCOUNT'], 'RUB');
		} else {
			$this->getOrder($orderId);
		}

		//Сохраняем заказ
		if ($request->isPost()) {
			if (empty($this->arResult['ERRORS']) && $saveOrder) {
				$orderId                    = $this->saveOrder();
				$this->arResult['ORDER_ID'] = $orderId;
				if ($orderId > 0 && ! $request->isAjaxRequest()) {
					LocalRedirect($APPLICATION->GetCurPage() . '?ORDER_ID=' . $orderId);
				}
			}
		}


		if ( ! $request->isAjaxRequest()) {
			$this->includeComponentTemplate();
		}

		if ($request->isAjaxRequest()) {

			echo json_encode($this->arResult);

			$APPLICATION->FinalActions();
			die();
		}

	}

	/**
	 * Получение свойства заказа по его коду
	 *
	 * @param Sale\PropertyValueCollection $propertyCollection
	 * @param $code
	 *
	 * @return mixed|null
	 */
	private function getPropertyByCode(Sale\PropertyValueCollection $propertyCollection, $code) {
		foreach ($propertyCollection as $property) {
			if ($property->getField('CODE') == $code) {
				return $property;
			}
		}
	}

	//Генерация логотипов пл. систем
	private function paymentImages(&$result, $scale = '') {

		foreach ($result as $key => $paySystem) {
			if ( ! empty($paySystem["LOGOTIP"])) {
				$arFileTmp                     = CFile::ResizeImageGet(
					$paySystem["LOGOTIP"],
					array("width" => "110", "height" => "110"),
					BX_RESIZE_IMAGE_PROPORTIONAL,
					true
				);
				$result[ $key ]['LOGOTIP_SRC'] = $arFileTmp["src"];
			}
		}
		unset($logotype, $paySystem);
	}

	private function generatePassword($length = 6) {
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$count = mb_strlen($chars);

		for ($i = 0, $result = ''; $i < $length; $i++) {
			$index = rand(0, $count - 1);
			$result .= mb_substr($chars, $index, 1);
		}

		return $result;
	}


	protected function addError($res) {
		$this->arResult["ERRORS"][] = $res;
	}
}

;
?>