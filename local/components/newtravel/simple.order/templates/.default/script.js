$(document).ready(function () {

    $('.pay_system__more').popover();

    $('.btn-radio').click(function (e) {
        $('.btn-radio').not(this).removeClass('active')
            .siblings('input').prop('checked', false)
            .siblings('.img-radio').css('opacity', '0.5');
        $(this).addClass('active')
            .siblings('input').prop('checked', true)
            .siblings('.img-radio').css('opacity', '1');

        $('#save-order').val('N');
        sendForm();
    });


    $('#order-form').on('submit', function (e) {
        e.preventDefault();
        $('#error-panel').hide();
        $('#error-list').empty();
        $('.preload').show();

        var url = $(this).attr('action');
        $.post(
            url,
            $(this).serialize(),
            function (data) {

                setTimeout(function () {
                    $('.preload').hide();
                }, 1000);

                console.log(data);
                if (data.ERRORS.length > 0) {
                    showErrors(data.ERRORS);
                }
                if (parseInt(data.ORDER_ID) > 0) {
                    document.location.href = data.CUR_PAGE + "?ORDER_ID=" + data.ORDER_ID;
                } else {
                    updateScreen(data);
                }
            },
            'json'
        );
    });

    $('#checkout').on('click', function () {
        $('#save-order').val('Y');
    });

});

function sendForm() {
    $('#order-form').submit();
}

function updateScreen(data) {
    var total_sum = $('#total-sum'),
        discount = $('#discount');

    total_sum.text(data.DISPLAY_TOTAL_SUM);
    discount.text(data.DISPLAY_DISCOUNT);
}

function showErrors(errors) {
    $('#error-panel').show();
    var error_list = $('#error-list');

    $.each(errors, function (i, val) {
        error_list.append('<li class="text-danger">' + val + '</li>')
    })

}