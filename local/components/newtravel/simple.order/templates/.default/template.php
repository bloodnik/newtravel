<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>
<div class="preload" style="display: none;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="loader">
					<div class="loader-inner">
						<div class="box-1"></div>
						<div class="box-2"></div>
						<div class="box-3"></div>
						<div class="box-4"></div>
					</div>
					<span class="text">ждем...</span>
				</div>
			</div>
		</div>
	</div>
</div>

<section class="order-section">
	<div class="container">
		<? if ($arResult['ORDER_ID'] == 0 && count($arResult['BASKET_ITEMS']) > 0): ?>
			<br>
			<div class="panel panel-success">
				<div class="panel-heading"><h3 class="text-center text-uppercase">Оформление заказа</h3></div>
				<div class="panel-body">
					<form action="<?=POST_FORM_ACTION_URI?>" method="post" id="order-form" name="order-form">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="CONTACT_PERSON">ФИО*</label>
									<input type="text" class="form-control" id="name" required value="<?=$arResult['REQUEST']['CONTACT_PERSON_ORDER_PROP']?>" name="CONTACT_PERSON_ORDER_PROP">
								</div>
								<div class="form-group">
									<label for="PHONE">Телефон*</label>
									<input type="text" class="form-control" placeholder="+79991234567" id="phone" required value="<?=$arResult['REQUEST']['PHONE_ORDER_PROP']?>" name="PHONE_ORDER_PROP">
								</div>
								<div class="form-group">
									<label2 for="EMAIL">E-mail*</label2>
									<input type="text" class="form-control" placeholder="your@mail.ru" id="email" required value="<?=$arResult['REQUEST']['EMAIL_ORDER_PROP']?>" name="EMAIL_ORDER_PROP">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="PASPORT">Паспортные данные(Серия, номер, кем выдан, дата выдачи)*</label>
									<textarea required name="PASPORT_ORDER_PROP" class="form-control" id="pasport" cols="30" rows="4"><?=$arResult['REQUEST']['PASPORT_ORDER_PROP']?></textarea>
								</div>
								<div class="form-group">
									<label for="USER_DESCRIPTION">Комментарий к заказу</label>
									<textarea name="USER_DESCRIPTION" class="form-control" id="comment" cols="30" rows="2"><?=$arResult['REQUEST']['USER_DESCRIPTION']?></textarea>
								</div>
							</div>
						</div>

						<hr>

						<div class="row pay_systems">
							<div class="col-md-12">
								<h3>Выберите способ оплаты</h3>
								<div class="row">
									<? foreach ($arResult['PAY_SYSTEMS'] as $key => $paySystem): ?>
										<div class="col-md-4 pay_system__item">
											<img src="<?=$paySystem['LOGOTIP_SRC']?>" class="img-responsive img-radio center-block" <? if ($paySystem['ID'] == $arResult['CURRENT_PAY_SYSTEM']): ?>style="opacity: 1" <? endif; ?>>
											<div class="desc">
												<?=TruncateText($paySystem['DESCRIPTION'], 100)?>
												<a href="javascript:void(0)" class="pay_system__more" data-placement="top" data-trigger="focus" data-toggle="popover" title="<?=$paySystem['NAME']?>"
												   data-content="<?=strip_tags($paySystem['DESCRIPTION'])?>">подробнее</a>
											</div>
											<button type="button" class="btn btn-primary btn-radio <? if ($paySystem['ID'] == $arResult['CURRENT_PAY_SYSTEM']): ?>active<? endif; ?>"><?=$paySystem['NAME']?></button>
											<input type="radio" id="left-item" value="<?=$paySystem['ID']?>" name="PAY_SYSTEM" class="hidden" <? if ($paySystem['ID'] == $arResult['CURRENT_PAY_SYSTEM']): ?>checked<? endif; ?>>
										</div>
									<? endforeach; ?>
								</div>
							</div>
						</div>

						<h3>Состав заказа</h3>
						<table class="table">
							<tr>
								<th>Название</th>
								<th>Количество туристов</th>
								<th>Цена</th>
								<th>Сумма</th>
							</tr>
							<? foreach ($arResult['BASKET_ITEMS'] as $key => $basketItem): ?>
								<tr id="item_<?=$basketItem['ID']?>">
									<td><?=$basketItem['NAME']?></td>
									<td><?=round($basketItem['QUANTITY'])?></td>
									<td><?=round($basketItem['PRICE'])?></td>
									<td><?=round($basketItem['PRICE']) * round($basketItem['QUANTITY'])?></td>
								</tr>
							<? endforeach; ?>
						</table>
						<div class="col-md-4 col-md-push-8">
							<ul class="summary">
								<li>Общая стоимость: <span id="total-sum"><?=$arResult['DISPLAY_TOTAL_SUM']?></span></li>
								<li><?=$arResult['DISCOUNT'] > 0 ? "Наценка пл/системы: " : "Доп. скидка:"?><span id="discount"><?=$arResult['DISPLAY_DISCOUNT']?></span></li>
							</ul>
						</div>

						<div class="clearfix"></div>

						<div class="panel panel-danger" id="error-panel" style="display: none;">
							<div class="panel-heading"><h3 class="panel-title">Ошибка!</h3></div>
							<div class="panel-body">
								<ul id="error-list">

								</ul>
							</div>
						</div>
						<br>

						<input type="hidden" name='SAVE_ORDER' id="save-order" value="N">
						<button type="submit" name="CHECKOUT" class="btn btn-lg btn-success pull-right" id="checkout" value="Y">Оформить заказ</button>
					</form>
				</div>
			</div>
		<? endif; ?>

		<? if ($arResult['ORDER_ID'] > 0): ?>
			<? include('finish.php') ?>
		<? endif; ?>

	</div>
</section>