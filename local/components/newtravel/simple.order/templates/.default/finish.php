<br>
<div class="panel panel-success">
	<div class="panel-heading">
		<h2 class="panel-title"><strong>Спасибо за заказ!</strong></h2>
	</div>
	<div class="panel-body">
		<ul class="summary">
			<li>Номер заказа: <span><?=$arResult['ORDER']['ID']?></span></li>
			<li class="mb40">Стоимость: <span class="price"><?=CCurrencyLang::CurrencyFormat($arResult['ORDER']['PRICE'], 'RUB')?></span></li>
			<? if ( ! $arResult['ORDER']['PAYED']): ?>
				<? foreach ($arResult['ORDER']['PAY_SYSTEM'] as $payment): ?>
					<li class="mb40">
						Способ оплаты: <?=$payment['NAME']?>
						<? if ($payment['ID'] != '1'): ?>
							<span><a class="btn btn-success" target="_blank" href="<?=$payment['PAY_URL']?>">Оплатить <?=CCurrencyLang::CurrencyFormat($payment['SUM'], 'RUB')?></a></span>
						<? endif; ?>
					</li>
					<li><?=$payment['DESCRIPTION']?></li>
				<? endforeach; ?>
			<? else: ?>
				<li><span class="text-success">Заказ оплачен</span></li>
			<? endif; ?>
		</ul>
	</div>
	<div class="panel-footer">Подробную информацию о заказе можно посмотреть в <a href="/personal/order/order_detail.php?ID=<?=$arResult['ORDER']['ID']?>">персональном разеле</a></div>
</div>

