<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
class CSpecialOffer extends CBitrixComponent {

	//Запускаем компонент
	public function executeComponent() {

		//Дата с параметров
		$originalDate = $this->arParams["DATE"];
		$newDate = date("F j, Y, H:i:s", strtotime($originalDate));

		$curDateObject = date_create(date("F j, Y, H:i s")); //Текущая дата
		$newDateObject = date_create($newDate);

		//Сравниваем даты
		$interval = date_diff($curDateObject, $newDateObject);

		//Если текущая дата больше выбранной, тогда дату не применяем, чтобы были нули
		if($interval->invert > 0) {
			$newDate = "";
		}

		$this->arResult["DATE"] = $newDate;
		
		$this->includeComponentTemplate();
	}
}

;
?>