<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);

$this->addExternalCss($templateFolder."/flipclock.css");
$this->addExternalJs($templateFolder."/flipclock.min.js");

?>
<div class="modal fade" id="speciall_offer_modal_wrap" tabindex="-1" role="dialog" aria-labelledby="Акция">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header text-center">
				<h2 class="text-danger text-uppercase">Акция!!!</h2>
			</div>
			<div class="modal-body">
				<div class="special_offer_modal text-center">


					<p>Зарегистрируйся на сайте и получи расширенную <strong>медицинскую страховку</strong> в подарок.</p>
					<p>Только сегодня до 21:00!</p>

					<div class="your-clock"></div>

					<p>Подробности акции будут отправлены на Ваш Email после регистрации</p>

					<div class="row">
						<a href="/auth/?register=yes&backurl=%2F" rel="nofollow" class="btn btn-blue" onclick="$.cookie('NEWTRAVEL_SO_CLOSED', 'Y', { path: '/' });">Зарегистрироваться</a>
						<a href="javascript:void(0)" rel="nofollow" class="btn btn-gray" id="dont_show_offer" data-dismiss="modal" onclick="$.cookie('NEWTRAVEL_SO_CLOSED', 'Y', { path: '/' });">Больше не показывать предложение</a>
					</div>
					<br>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	$(function () {
		"use strict";
		$.each($(".your-clock"), function () {
			new Clock($(this))
		})
	});
	var Clock = function (e) {
		var t = e, c = new Date("<?=$arResult["DATE"]?>"), n = new Date, o = c.getTime() / 1e3 - n.getTime() / 1e3;
		t.FlipClock(o, {clockFace: "HourlyCounter", autoStart: !0, countdown: true, language: "ru"})
	};

	$(document).ready(function () {
		var dont_open_modal = $.cookie('NEWTRAVEL_SO_CLOSED');
		if(dont_open_modal !== "Y") {
			$('#speciall_offer_modal_wrap').modal('show');
		}

	});
</script>