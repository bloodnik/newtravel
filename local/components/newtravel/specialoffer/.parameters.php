<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$date = new DateTime(date("Y-m-d H:i:s"));
$date->modify('+1 day');

$date = date_format($date, 'd.m.Y H:i:s');


$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"CACHE_TIME"  =>  array("DEFAULT"=>36000000),
		'DATE' => array(
			'NAME' => GetMessage('ELAB_DATE'),
			'TYPE' => 'CUSTOM',
			// свой подключаемый скрипт
			'JS_FILE' => '/local/components/newtravel/specialoffer/settings.js',
			// функция из подключенного скрипта JS_FILE, вызывается при отрисовке окна настроек
			'JS_EVENT' => 'OnMySettingsEdit',
			// доп. данные, передаются в функцию из JS_EVENT
			'JS_DATA' => json_encode(array('set' => GetMessage('MY_PARAM_SET'))),
			'DEFAULT' => $date,
			'PARENT' => 'BASE',
		),
	),
);