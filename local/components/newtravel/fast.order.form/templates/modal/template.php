<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>
<style>
	.fast-form .from-group {
		margin-bottom: 15px;
	}
</style>

<div class="fast-form">
	<div class="row">
		<div class="col-md-12 text-center">
			<h3 class="text-warning">Все очень просто! Закажи тур, оставив только имя и номер телефона!</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="from-group">
				<input type="text" class="form-control name_input" name="form_text_82" placeholder="Ваше имя" required/>
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="from-group">
				<input type="text"
				       class="form-control phone phone_input"
				       name="form_text_71"
				       pattern="^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))"
				       maxlength='15'
				       placeholder="Телефон 7__________"
				       title="Номер должен содержать только цифры и начинаться с либо с 8, либо с +7"
				       required/>
			</div>
		</div>

		<div class="col-md-12 col-xs-12">
			<div class="from-group">
				<input type="checkbox" name="confirm-agreement" checked id="confirm-agreement_<?=$arParams['TOUR_ID']?>"><label for='confirm-agreement_<?=$arParams['TOUR_ID']?>'>Я согласен с <a href="/about/agreement/" target="_blank">условиями</a>
					обработки персональных
					данных</label>
			</div>
		</div>

	</div>
	<br>
	<div class="row">
		<div class="col-md-12 col-xs-12 text-right">
			<div class="from-group">
				<?=bitrix_sessid_post()?>
				<button class="btn btn-green" id="fast_checkout">Заказать</button>
				<button class="btn btn-white" data-dismiss="modal">Отмена</button>
			</div>
		</div>
	</div>
</div>
<div class="fast-form-result" style="display: none;">

</div>

<script>
    $(document).ready(function () {
        var phone_input = $(".phone_input");
        var name_input = $(".name_input");
        var tourid = <?=$arParams['TOUR_ID'] ?: ""?>;
        var form = $(".fast-form");
        var form_result = $(".fast-form-result");
        var sessidval = $("#sessid").val();

        $(phone_input).on('keyup', function () {
            $(this).parent().removeClass('has-error');
        });
        $(name_input).on('keyup', function () {
            $(this).parent().removeClass('has-error');
        });

        $("#fast_checkout").on('click', function (e) {
            $("#fast_checkout").prop('disabled', true);
            BX.showWait();
            yaCounter24395911.reachGoal('FAST_ORDER_MODAL');

            if (!$('#confirm-agreement_<?=$arParams['TOUR_ID']?>').prop('checked')) {
                e.preventDefault();
                alert('Подтвердите согласие с условиями обработки персональных данных');
                BX.closeWait();
                $("#fast_checkout").prop('disabled', false);
                return false;
            }

            if (!name_input.val()) {
                BX.closeWait();
                $("#fast_checkout").prop('disabled', false);
                name_input.parent().addClass('has-error');

                return false;
            }

            if (!phone_input.val() || phone_input.val().search(/^((8|\+7|7)(9[0-9]{9,10})|(9[0-9]{6,6}))/) === -1) {
                BX.closeWait();
                $("#fast_checkout").prop('disabled', false);
                phone_input.parent().addClass('has-error');

                return false;
            }

            $.getJSON("/local/tools/newtravel.search.ajax.php", {type: "fastorder", tourid: tourid, phone: phone_input.val(), name: name_input.val(), sessid: sessidval}, function (json) {
                $("#fast_checkout").prop('disabled', false);
                BX.closeWait();
                if (json.STATUS) {
                    yaCounter24395911.reachGoal('FAST_ORDER_MODAL_SEND');
                    form.slideUp(200);
                    form_result.slideDown(200).append('<h3 class="text-success">Спасибо за заказ! В самое ближайшее время наш менеджер свяжется с Вами!</h3>');
                    form_result.append('<button class="btn btn-white" data-dismiss="modal">Закрыть</button>');
                } else {
                    form.slideUp(200);
                    form_result.slideDown(200).append('<h3 class="text-danger">' + json.ERROR + '</h3>');
                    form_result.append('<button class="btn btn-blue" onclick="$(\'.fast-form-result\').slideUp(200).empty();$(\'.fast-form\').slideDown(200);">Повторить</button>');
                }
            });
        });
    });
</script>