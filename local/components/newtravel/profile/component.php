<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//if(!isset($arParams["CACHE_TIME"]))
	//$arParams["CACHE_TIME"] = 36000000;

$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";

if($this->StartResultCache(false, array()))
{
	$this->IncludeComponentTemplate();
}
if($arParams["SET_TITLE"])
	$APPLICATION->SetTitle($arResult["NAME"]);
?>