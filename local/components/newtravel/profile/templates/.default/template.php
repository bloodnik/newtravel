<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<section class="breadcrumb-wrapper">
	<div class="container">
		<div class="row">
			<div class="page-description">
				<h3><? echo $arResult["USER"]["LAST_NAME"] ? $arResult["USER"]["LAST_NAME"] . " " : "";
					echo $arResult["USER"]["NAME"] ? $arResult["USER"]["NAME"] . "" : "";
					echo $arResult["USER"]["SECOND_NAME"] ? $arResult["USER"]["SECOND_NAME"] . "" : ""; ?>! Это вы!</h3>
			</div>
		</div>
	</div>
</section>

<section class="white-section personal-page">
	<div class="container">
		<div class="row">
			<div class="about-me-wrapper">
				<div class="col-md-6">
					<div class="about-me">
						<div class="img-cover">
							<div class="thumbnail">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/user-bg.jpg" alt="member cover"/>
							</div>
						</div>
						<div class="member-img">
							<div class="thumbnail">
								<? $w = 140;
								$h    = 140;
								$src = SimaiResizeImage($arResult["USER"]["PERSONAL_PHOTO"], $w, $h) ?>
								<img src="<?=($src ? $src : SITE_TEMPLATE_PATH . "/images/logo-big.png")?>" width="<?=$w?>" height="<?=$h?>"/>
							</div>
						</div>
						<div class="member-info">
							<h2 class="name"><? echo $arResult["USER"]["LAST_NAME"] ? $arResult["USER"]["LAST_NAME"] . " " : "";
								echo $arResult["USER"]["NAME"] ? $arResult["USER"]["NAME"] . " " : "";
								echo $arResult["USER"]["SECOND_NAME"] ? $arResult["USER"]["SECOND_NAME"] . " " : ""; ?></h2>
							<h3 class="title"><?=$arResult["USER"]["PERSONAL_PROFESSION"]?></h3>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="text-widget">
						<div class="text-widget-title">
							<h3>Персональные данные</h3>
						</div>
						<ul class="personal-details">
							<li>
								<span class="title">Логин  на сайте</span>
								<span class="value"><?=$arResult["USER"]["LOGIN"]?></span>
							</li>
							<li>
								<span class="title">ФИО</span>
								<? if ($arResult["USER"]["NAME"]): ?>
									<span class="value"><? echo $arResult["USER"]["LAST_NAME"] ? $arResult["USER"]["LAST_NAME"] . " " : "";
										echo $arResult["USER"]["NAME"] ? $arResult["USER"]["NAME"] . " " : "";
										echo $arResult["USER"]["SECOND_NAME"] ? $arResult["USER"]["SECOND_NAME"] . " " : ""; ?></span>
								<? endif; ?>
							</li>
							<li>
								<span class="title">Дата рождения</span>
								<span class="value">16.03.2014</span>
							</li>
							<li>
								<span class="title">Контакный номер</span>
								<span class="value"><?=$arResult["USER"]["PERSONAL_PHONE"] ?: "Не указан"?></span>
							</li>
							<li>
								<span class="title">Контактный @-адрес</span>
								<span class="value"><a href="mailto:<?=$arResult["USER"]["EMAIL"]?>"><?=$arResult["USER"]["EMAIL"]?></a></span>
							</li>
							<li>
								<span class="title">Страна, город</span>
								<span class="value">Россия, Уфа</span>
							</li>
						</ul>
						<a href="/personal/profile/edit.php">Редактировать свои данные</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="text-widget hidden">
						<div class="text-widget-title">
							<h3>Личный счет на сайте</h3>
						</div>
						<h5>На вашем счету:</h5>
						<div class="price">
							<? $APPLICATION->IncludeComponent("bitrix:sale.personal.account", "profile", Array(
									"SET_TITLE" => "N"
								)
							); ?>
						</div>
					</div>
					<div class="text-widget categories">
						<ul>
							<li><a href="/personal/order">Мои заказы</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>