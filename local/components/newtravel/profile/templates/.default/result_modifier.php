<?
if(!CModule::IncludeModule("sale"))
	return;
if(CModule::IncludeModule("iblock"))
{ 
if(isset($_REQUEST["ID"]) && IntVal($_REQUEST["ID"]) > 0 && CSite::InGroup(array(1)))
{
	$USER_ID = $_REQUEST["ID"];
	$ar = CSaleUser::GetList(array("USER_ID"=>$_REQUEST["ID"]));
	$FUSER_ID = $ar['ID'];
}
else
{
	$USER_ID = $USER->GetID();
	$FUSER_ID = CSaleBasket::GetBasketUserID();
}

$res = CUser::GetByID($USER_ID);
$arResult["USER"] = $res->Fetch();

$res = CIBlockElement::GetList(array("SORT"=>"ASC"), array("IBLOCK_ID"=>1), false, false, array("*"));
while($arItem = $res->GetNext())
	$arResult["TOURIST"][] = $arItem;

$res = CSaleBasket::GetList(
	array("NAME"=>"ASC", "ID"=>"ASC"),
	array("FUSER_ID"=>$FUSER_ID, "LID"=>SITE_ID, "ORDER_ID"=>"NULL"),
	false,
	false,
	array("NAME", "ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY", "PRICE", "WEIGHT")
);
while($arItems = $res->Fetch())
	$arResult["BASKET"][] = $arItems;

$res = CSaleOrderUserProps::GetList(array("DATE_UPDATE"=>"DESC"), array("USER_ID"=>$USER_ID));
while($arItems = $res->Fetch())
	$arResult["USER_PROPS"][] = $arItems;

$res = CSaleOrder::GetList(array("DATE_INSERT"=>"ASC"), array("USER_ID"=>$USER_ID));
while($arItems = $res->Fetch())
	$arResult["SALE_ORDER"][] = $arItems;
CSaleUserTransact::GetList(
	array("TRANSACT_DATE"=>"ASC"),
	array("USER_ID"=>$USER_ID),
	false,
	false,
	array()
);
while($arItems = $res->Fetch())
	$arResult["USER_TRANSACT"][] = $arItems;
}
?>