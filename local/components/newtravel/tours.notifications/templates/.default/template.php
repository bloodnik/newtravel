<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>
<? foreach ($arResult["ITEMS"] as $arItem): ?>
	<div class="alert alert-<?=$arParams['ALERT_TYPE']?> alert-dismissible notif" role="alert" style="margin-bottom: 5px;position: relative;z-index: 10; display: none;" id="alert_<?=$arItem['ID']?>">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="hideNotification(<?=$arItem['ID']?>)"><span aria-hidden="true">&times;</span>
		</button>
		<div class="container">
			<div class="row">
				<noindex>
					<strong>Внимание!</strong>
					<span class="text"><?=$arItem["PREVIEW_TEXT"]?></span>
					<? if ($arItem["PROPERTY_NOTIF_LINK_VALUE"]): ?>
						<a href="<?=$arItem["PROPERTY_NOTIF_LINK_VALUE"]?>" class="btn btn-warning" target="_blank" rel="nofollow">
							<?=$arItem["PROPERTY_NOTIF_BUT_NAME_VALUE"]?>
						</a>
					<? endif; ?>
				</noindex>
			</div>
		</div>
	</div>
<? endforeach; ?>
<script>
	function hideNotification(id) {
		$.cookie("closed_notif", id, {expires: 7, path: '/'});
	}

	$(document).ready(function () {
		var alertId = $.cookie("closed_notif");
		if (alertId == null) {
			$('.notif').show();
		} else {
			$('#alert_' + alertId).hide();
		}
	});

</script>
