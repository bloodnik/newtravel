<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("AITI_COM_NAME"),
	"DESCRIPTION" => GetMessage("AITI_COM_DESCRIPTION"),
	"ICON" => "",
	"PATH" => array(
		"ID" => "AITI",
		"SORT" => 10000,
		"NAME" => GetMessage("AITI_MAIN_SECTION"),
		"CHILD" => array(
			"ID" => "content",
			"NAME" => GetMessage("AITI_SUB_SECTION"),
			"SORT" => 10,
		)
	),
);
?>