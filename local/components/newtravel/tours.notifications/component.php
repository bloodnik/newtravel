<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

CModule::IncludeModule('iblock');

$arResult = array();

$ERROR_DETECTED = false;
if ($this->StartResultCache()) {
	$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
	if ($arParams["IBLOCK_ID"] > 0) {
		$IBLOCK_ID = $arParams["IBLOCK_ID"];

		$arSelect = Array(
			"ID",
			"NAME",
			"DATE_ACTIVE_FROM",
			"PREVIEW_TEXT",
			"PROPERTY_NOTIF_LINK",
			"PROPERTY_NOTIF_BUT_NAME"
		);
		$arFilter = Array("IBLOCK_ID" => IntVal($IBLOCK_ID), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
		$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields            = $ob->GetFields();
			$arResult["ITEMS"][] = $arFields;
		}
	} else {
		ShowError(GetMessage("AITI_ERROR_NO_IBLOCK_ID"));
		$ERROR_DETECTED = true;
	}

	$this->IncludeComponentTemplate();
}