<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule('iblock');

$arIblockTypes = CIBlockParameters::GetIBlockTypes(Array("" => GetMessage("AITI_ARDATE_VARIANT_NONE")));

$arIBlocks = array();
$arIBlocks[] = GetMessage("AITI_ARDATE_VARIANT_NONE");
$db_iblock = CIBlock::GetList(Array("SORT" => "ASC"), Array("TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = '['.$arRes["ID"].'] '.$arRes["NAME"];

$arComponentParameters = array(
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => array(
			"NAME" => GetMessage("AITI_IBTYPE_NAME"),
			"TYPE" => "LIST",
			"VALUES" => $arIblockTypes,
			"MULTIPLE" => "N",
			"PARENT" => "BASE",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => array(
			"NAME" => GetMessage("AITI_IBLOCK_NAME"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"MULTIPLE" => "N",
			"PARENT" => "BASE",
			"REFRESH" => "N",
		),
		"FILTER_LINK" => array(
			"NAME" => GetMessage("AITI_FILTER_LINK"),
			"TYPE" => "STRING",
			"PARENT" => "BASE",
			"REFRESH" => "N",
		),
		"CACHE_TIME"  => array(
			"DEFAULT" => 3600
		),
		"ALERT_TYPE" => array(
			"NAME" => GetMessage("AITI_ALERT_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => array('danger'=>'Красный', 'warning'=>'Желтый', 'info'=>'Голубой'),
			"MULTIPLE" => "N",
			"PARENT" => "BASE",
			"DEFAULT" => 'warning'
		),
	),
);
?>