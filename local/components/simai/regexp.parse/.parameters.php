<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"PARSE_URL" => array(
			"PARENT" => "BASE",
			"NAME" => "Адрес URL для парсинга (http://example.com)",
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"CODING" => array(
			"PARENT" => "BASE",
			"NAME" => "Кодировка сайта",
			"TYPE" => "LIST",
			"VALUES" => array("UTF-8", "Windwos-1251"),
			"DEFAULT" => "Windwos-1251"
		),
		"QUERY" => array(
			"PARENT" => "BASE",
			"NAME" => "Регулярное выражение",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"CACHE_TIME" => array("DEFAULT"=>36000000),
		"CACHE_GROUPS" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => "Учитывать права доступа",
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y"
		)
	)
);
?>
