<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Парсинг регулярками невалидных html и «xml» документов",
	"DESCRIPTION" => "Парсинг регулярками невалидных html и «xml» документов по аналогии с dapper (сервис) и nokogiri (ruby)",
	"ICON" => "/images/sale_profile_detail.gif",
	"PATH" => array(
		"ID" => "simai",
		"NAME" => "Симай"
	),
);
?>
