<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

if(!isset($arParams["PARSE_URL"]) || empty($arParams["PARSE_URL"]))
{
	ShowError("Не введен адрес");
	return;
}
if(!isset($arParams["MAX_LEVEL"]) || empty($arParams["MAX_LEVEL"]))
{
	$arParams["MAX_LEVEL"] = null;
}
elseif(is_numeric($arParams["MAX_LEVEL"]) && $arParams["MAX_LEVEL"] <= 0)
{
	ShowError("Не правильный параметр 'Максимальная глубина вывода массива'");
	return;
}
if(!isset($arParams["QUERY"]) || empty($arParams["QUERY"]))
{
	ShowError("Не введена строка запроса");
	return;
}
$arParams["PARSE_URL"] = trim($arParams["PARSE_URL"]);
$arParams["QUERY"] = trim($arParams["QUERY"]);
if($arParams["MAX_LEVEL"] !== null)
	$arParams["MAX_LEVEL"] = IntVal(trim($arParams["MAX_LEVEL"]));

$header = <<<EOF
Host: www.airportufa.ru
User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.1; ru; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3
Accept-Encoding: gzip,deflate
Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7
Keep-Alive: 115
Connection: close
Referer: http://www.airportufa.ru/ru/online_table/
Cookie:	hotlog=1
x-insight: activate
Cache-Control: max-age=0
EOF;
if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N" ? false : $USER->GetGroups()))))
{
	$html = stream_context_create(array(
		'http'=>array(
			'method'=>'GET',
			'header'=>$header
		)
	));
	$html = file_get_contents($arParams["PARSE_URL"]);
	//$html = mb_convert_encoding($html, 'UTF-8', mb_detect_encoding($html, 'UTF-8, Windows-1251', true));
	if($html === false)
	{
		$this->AbortResultCache();
		ShowError("Документ по адресу недоступен: ".$arParams["PARSE_URL"]);
		return;
	}
	preg_match_all('/МОСКВА(ВНУКОВО)/i', $html, $matches, PREG_SET_ORDER);
	//$arResult[] = $arParams["QUERY"];
	$arResult[] = $html;
	$arResult[] = $matches;
	$this->SetResultCacheKeys(array());
	$this->IncludeComponentTemplate();
}
?>
