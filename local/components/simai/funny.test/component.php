<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
if(strlen($arParams["IBLOCK_TYPE"])<=0)
 	$arParams["IBLOCK_TYPE"] = "funny_test";
$arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);
$arParams["PARENT_SECTION"] = intval($arParams["PARENT_SECTION"]);


$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
if(strlen($arParams["SORT_BY1"])<=0)
	$arParams["SORT_BY1"] = "SORT";
$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
if($arParams["SORT_ORDER1"]!="ASC")
	 $arParams["SORT_ORDER1"]="DESC";
if(strlen($arParams["SORT_BY2"])<=0)
	$arParams["SORT_BY2"] = "NAME";
$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
if($arParams["SORT_ORDER2"]!="DESC")
	 $arParams["SORT_ORDER2"]="ASC";

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	$arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
	if(!is_array($arrFilter))
		$arrFilter = array();
}

$arParams["CHECK_DATES"] = $arParams["CHECK_DATES"]!="N";

if(!is_array($arParams["FIELD_CODE"]))
	$arParams["FIELD_CODE"] = array();
foreach($arParams["FIELD_CODE"] as $key=>$val)
	if(!$val)
		unset($arParams["FIELD_CODE"][$key]);

$arParams["ANSWERS"] = trim($arParams["ANSWERS"]);
if(strlen($arParams["ANSWERS"])<=0)
	$arParams["ANSWERS"] = "ANSWERS";

$arParams["ANSWERS_WEIGHTS"] = trim($arParams["ANSWERS_WEIGHTS"]);
if(strlen($arParams["ANSWERS_WEIGHTS"])<=0)
	$arParams["ANSWERS_WEIGHTS"] = "ANSWERS_WEIGHTS";

$arParams["QUESTIONS_COUNT"] = intval($arParams["QUESTIONS_COUNT"]);
if($arParams["QUESTIONS_COUNT"]<=0)
	$arParams["QUESTIONS_COUNT"] = 20;

$arParams["CACHE_FILTER"] = $arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
	$arParams["CACHE_TIME"] = 0;

$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default
$arParams["INCLUDE_IBLOCK_INTO_CHAIN"] = $arParams["INCLUDE_IBLOCK_INTO_CHAIN"]!="N";

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"]=="Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"]!="N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]!="N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]!=="N";

if($arParams["DISPLAY_TOP_PAGER"] || $arParams["DISPLAY_BOTTOM_PAGER"])
{
	$arNavParams = array(
		"nPageSize" => $arParams["QUESTIONS_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
}
else
{
	$arNavParams = array(
		"nTopCount" => $arParams["QUESTIONS_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

$arParams["USE_PERMISSIONS"] = $arParams["USE_PERMISSIONS"]=="Y";
if(!is_array($arParams["GROUP_PERMISSIONS"]))
	$arParams["GROUP_PERMISSIONS"] = array(1);

$bUSER_HAVE_ACCESS = !$arParams["USE_PERMISSIONS"];
if($arParams["USE_PERMISSIONS"] && isset($GLOBALS["USER"]) && is_object($GLOBALS["USER"]))
{
	$arUserGroupArray = $GLOBALS["USER"]->GetUserGroupArray();
	foreach($arParams["GROUP_PERMISSIONS"] as $PERM)
	{
		if(in_array($PERM, $arUserGroupArray))
		{
			$bUSER_HAVE_ACCESS = true;
			break;
		}
	}
}

if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $bUSER_HAVE_ACCESS, $arNavigation, $arrFilter)))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	if(is_numeric($arParams["IBLOCK_ID"]))
	{
		$rsIBlock = CIBlock::GetList(array(), array(
			"ACTIVE" => "Y",
			"ID" => $arParams["IBLOCK_ID"],
		));
	}
	else
	{
		$rsIBlock = CIBlock::GetList(array(), array(
			"ACTIVE" => "Y",
			"CODE" => $arParams["IBLOCK_ID"],
			"SITE_ID" => SITE_ID,
		));
	}
	if($arResult = $rsIBlock->GetNext())
	{
		$arResult["USER_HAVE_ACCESS"] = $bUSER_HAVE_ACCESS;
		//SELECT
		$arSelect = array_merge($arParams["FIELD_CODE"], array(
			"ID",
			"IBLOCK_ID",
			"IBLOCK_SECTION_ID",
			"NAME",
			"ACTIVE_FROM",
			"DETAIL_PAGE_URL",
			"DETAIL_TEXT",
			"DETAIL_TEXT_TYPE",
			"PREVIEW_TEXT",
			"PREVIEW_TEXT_TYPE",
			"PREVIEW_PICTURE",
		));

		//WHERE
		$arFilter = array (
			"IBLOCK_ID" => $arResult["ID"],
			"IBLOCK_LID" => SITE_ID,
			"ACTIVE" => "Y",
			"CHECK_PERMISSIONS" => "Y",
		);

		$arParams["PARENT_SECTION"] = CIBlockFindTools::GetSectionID(
			$arParams["PARENT_SECTION"],
			$arParams["PARENT_SECTION_CODE"],
			array(
				"GLOBAL_ACTIVE" => "Y",
				"IBLOCK_ID" => $arResult["ID"],
			)
		);

		if($arParams["PARENT_SECTION"]>0)
		{
			$arFilter["SECTION_ID"] = $arParams["PARENT_SECTION"];
			$arFilter["INCLUDE_SUBSECTIONS"] = "N";
			$arResult["SECTION"]= array("PATH" => array());
			$rsPath = GetIBlockSectionPath($arResult["ID"], $arParams["PARENT_SECTION"]);
			$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
			while($arPath=$rsPath->GetNext())
			{
				$arResult["SECTION"]["PATH"][] = $arPath;
			}
		}
		else
		{
			$arResult["SECTION"]= false;
		}
		//ORDER BY
		$arSort = array(
			$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
			$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
		);
		if(!array_key_exists("ID", $arSort))
			$arSort["ID"] = "DESC";
		
		$arResult["ALL_ITEMS"] = array();
		$rsElement = CIBlockElement::GetList(
			array($arSort),
			array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "IBLOCK_TYPE"=>$arParams["IBLOCK_TYPE"], "SECTION_ID"=>$arParams["PARENT_SECTION"], "INCLUDE_SUBSECTIONS"=>"N", "ACTIVE"=>"Y"),
			false,
			false,
			array("ID")
		);
		while($obElement = $rsElement->GetNext(true, false))
		{
			$prop_values = array();
			$db_props = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $obElement["ID"], "SORT", "ASC", Array("CODE"=>"ANSWERS_WEIGHT"));
			while($prop = $db_props->GetNext())
        		$prop_values[] = $prop["VALUE"];
			$arResult["ALL_ITEMS"][] = array_merge($obElement, array("ANSWERS_WEIGHT"=>$prop_values));
		}
		$arResult["ITEMS_COUNT"] = count($arResult["ALL_ITEMS"]);

		$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, $arNavParams, $arSelect);
		$rsElement->SetUrlTemplates($arParams["DETAIL_URL"]);
		$arResult["ITEMS"] = array();
		while($obElement = $rsElement->GetNextElement())
		{
			$arItem = $obElement->GetFields();

			$arButtons = CIBlock::GetPanelButtons(
				$arItem["IBLOCK_ID"],
				$arItem["ID"],
				0,
				array("SECTION_BUTTONS"=>false, "SESSID"=>false)
			);
			$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
			$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
			$arItem["DISPLAY_PROPERTIES"] = $obElement->GetProperties();
			$arItem["ANSWERS"] = $arItem["DISPLAY_PROPERTIES"][$arParams["ANSWERS"]];
			$arItem["ANSWERS_WEIGHTS"] = $arItem["DISPLAY_PROPERTIES"][$arParams["ANSWERS_WEIGHTS"]];

			$arResult["ITEMS"][]=$arItem;
		}
		$arResult["NAV_STRING"] = $rsElement->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
		$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
		$arResult["NAV_RESULT"] = $rsElement;
		
		#############################################################################################
		# Обработка теста
		$arResult["COOKIE"] = array(
			"EXPIRES" => mktime(0, 0, 0, date("m")+1, date("d"), date("Y")),
			"PREFIX" => COption::GetOptionString("main", "cookie_name", "BITRIX_SM"),
			"NAME" => "FUNNY_TEST_".$arResult["SECTION"]["PATH"][0]["ID"],
		);
		$arResult["COOKIE"]["VALUE"] = $APPLICATION->get_cookie($arResult["COOKIE"]["NAME"]);
		if(empty($arResult["COOKIE"]["VALUE"]) || isset($_GET["reload_funny_test"]))
		{
			$arResult["COOKIE"]["VALUE"] = "";
			foreach($arResult["ALL_ITEMS"] as $arItem)
				$arResult["COOKIE"]["VALUE"] .= $arItem["ID"].":,";
			$arResult["COOKIE"]["VALUE"] = substr($arResult["COOKIE"]["VALUE"], 0, -1);
			//$APPLICATION->set_cookie($arResult["COOKIE"]["NAME"], $arResult["COOKIE"]["VALUE"], $arResult["COOKIE"]["EXPIRES"], "/", false, false, false, false);
			setcookie($arResult["COOKIE"]["PREFIX"]."_".$arResult["COOKIE"]["NAME"], $arResult["COOKIE"]["VALUE"], $arResult["COOKIE"]["EXPIRES"], "/");
		}
		$arResult["TEST_FALSE_COUNT"] = 0;
		$arResult["TEST_VALUES"] = array();
		$arCookieValues = explode(",", $arResult["COOKIE"]["VALUE"]);
		$arCookieValuesFalseCount = 0;
		foreach($arCookieValues as $arItem)
		{
			$arTemp = explode(":", $arItem);
			$arResult["TEST_VALUES"][$arTemp[0]] = $arTemp[1];
			if(!is_numeric($arTemp[1]))
				$arResult["TEST_FALSE_COUNT"]++;
		}
		if($arCookieValuesFalseCount == 1) {
			$arResult["TEST_LAST_STEP"] = true;
		}
		if($arResult["TEST_FALSE_COUNT"] == 0)
		{
			$arResult["TEST_RESULT"] = 0;
			foreach($arResult["ALL_ITEMS"] as $arItem)
				$arResult["TEST_RESULT"] += $arItem["ANSWERS_WEIGHT"][$arResult["TEST_VALUES"][$arItem["ID"]]];
			$res = CIBlockSection::GetList(Array("SORT"=>"ASC"), array("ID"=>$arParams["PARENT_SECTION"], "IBLOCK_ID"=>$arParams["IBLOCK_ID"]), false, $arSelect=Array("UF_*"));
			while($arItem = $res->GetNext())
				$arResult["SECTION_TEST"][] = $arItem;
		}
		#############################################################################################

		$this->SetResultCacheKeys(array(
			"ID",
			"IBLOCK_TYPE_ID",
			"NAV_CACHED_DATA",
			"NAME",
			"SECTION",
		));
		$this->IncludeComponentTemplate();
	}
	else
	{
		$this->AbortResultCache();
		ShowError(GetMessage("T_NEWS_NEWS_NA"));
		@define("ERROR_404", "Y");
		if($arParams["SET_STATUS_404"]==="Y")
			CHTTP::SetStatus("404 Not Found");
	}
}

if(isset($arResult["ID"]))
{

	$arTitleOptions = null;
	if($USER->IsAuthorized())
	{
		if(
			$APPLICATION->GetShowIncludeAreas()
			|| (is_object($GLOBALS["INTRANET_TOOLBAR"]) && $arParams["INTRANET_TOOLBAR"]!=="N")
			|| $arParams["SET_TITLE"]
		)
		{
			if(CModule::IncludeModule("iblock"))
			{
				$arButtons = CIBlock::GetPanelButtons(
					$arResult["ID"],
					0,
					$arParams["PARENT_SECTION"],
					array("SECTION_BUTTONS"=>false)
				);

				if($APPLICATION->GetShowIncludeAreas())
					$this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

				if(
					is_array($arButtons["intranet"])
					&& is_object($GLOBALS["INTRANET_TOOLBAR"])
					&& $arParams["INTRANET_TOOLBAR"]!=="N"
				)
				{
					foreach($arButtons["intranet"] as $arButton)
						$GLOBALS["INTRANET_TOOLBAR"]->AddButton($arButton);
				}

				if($arParams["SET_TITLE"])
				{
					$arTitleOptions = array(
						'ADMIN_EDIT_LINK' => $arButtons["submenu"]["edit_iblock"]["ACTION"],
						'PUBLIC_EDIT_LINK' => "",
						'COMPONENT_NAME' => $this->GetName(),
					);
				}
			}
		}
	}

	$this->SetTemplateCachedData($arResult["NAV_CACHED_DATA"]);

	if($arParams["SET_TITLE"])
	{
		$APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);
	}

	if($arParams["INCLUDE_IBLOCK_INTO_CHAIN"] && isset($arResult["NAME"]))
	{
		$APPLICATION->AddChainItem($arResult["NAME"]);
	}

	if($arParams["ADD_SECTIONS_CHAIN"] && is_array($arResult["SECTION"]))
	{
		foreach($arResult["SECTION"]["PATH"] as $arPath)
		{
			$APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
		}
	}
}
?>