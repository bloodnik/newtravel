<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
/*
Authorization form (for prolog)
Params:
	REGISTER_URL => path to page with authorization script (component?)
	PROFILE_URL => path to page with profile component
*/

$arParamsToDelete = array(
	"login",
	"logout",
	"register",
	"forgot_password",
	"change_password",
	"confirm_registration",
	"confirm_code",
	"confirm_user_id",
	"logout_butt",
	"auth_service_id",
);

$currentUrl = $APPLICATION->GetCurPageParam("", $arParamsToDelete);

$arResult["BACKURL"] = $currentUrl;

$arResult['ERROR'] = false;
$arResult['SHOW_ERRORS'] = (array_key_exists('SHOW_ERRORS', $arParams) && $arParams['SHOW_ERRORS'] == 'Y'? 'Y' : 'N');

if(!$USER->IsAuthorized())
{
	
	$rsET = CEventType::GetList(Array("TYPE_ID" => "SIMAI_REGISTER"));
if ($rsET->Fetch()):

else:
	$et = new CEventType;
	$et->Add(array(
		"LID" => LANGUAGE_ID,
		"EVENT_NAME" => "SIMAI_REGISTER",
		"NAME" => "Сообщение об успешной регистрации",
		"DESCRIPTION" => "#EMAIL_FROM# - Email отправителя
#EMAIL# - EMail получателя сообщения
#EMAIL_SUBJ# - Тема сообщения
#MESSAGE# - Тело сообщения",
	));
	
		$arSites = array();
	$sites = CSite::GetList(($b=""), ($o=""), Array("LANGUAGE_ID"=>LANGUAGE_ID));
	while ($site = $sites->Fetch()):
		$arSites[] = $site["LID"];
	endwhile;
	if(count($arSites) > 0):
		$emess = new CEventMessage;
		$emess->Add(array(
			"ACTIVE" => "Y",
			"EVENT_NAME" => "SIMAI_REGISTER",
			"LID" => $arSites,
			"EMAIL_FROM" => "#SITE_NAME#: #DEFAULT_EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL#",
			"SUBJECT" => "#EMAIL_SUBJ#",
			"BODY_TYPE" => "html",
			"MESSAGE" => "#MESSAGE#"
		));
	endif;
endif;
	
	if(IsModuleInstalled("subscribe") && CModule::IncludeModule("subscribe"))
	{
	$arFilter = array("ACTIVE"=>"Y", "LID"=>LANG);
	if(!$arParams["SHOW_HIDDEN"])
		$arFilterSub["VISIBLE"]="Y";
	$rsRubric = CRubric::GetList(array("SORT"=>"ASC", "NAME"=>"ASC"), $arFilterSub);
	$arRubrics = array();
	while($arRubric = $rsRubric->GetNext())
	{
		$arRubrics[]=$arRubric;
	}
	if(!empty($arRubrics)){
		foreach($arRubrics as $arRubric)
		{
			if(isset($_REQUEST["sub_rub"])){
			$bChecked = (in_array($arRubric["ID"], $_REQUEST["SUBSCRIBE"]));
			}
			else
			{
			$bChecked=true;
			}
			$arResult["RUBRICS"][]=array(
				"ID"=>$arRubric["ID"],
				"NAME"=>$arRubric["NAME"],
				"CHECKED"=>$bChecked,
			);
		}
	}	
	}
	$arResult["FORM_TYPE"] = "login";

	$arResult["STORE_PASSWORD"] = COption::GetOptionString("main", "store_password", "Y") == "Y" ? "Y" : "N";
	$arResult["NEW_USER_REGISTRATION"] = COption::GetOptionString("main", "new_user_registration", "N") == "Y" ? "Y" : "N";

	if(defined("AUTH_404"))
		$arResult["AUTH_URL"] = htmlspecialcharsback(POST_FORM_ACTION_URI);
	else
		$arResult["AUTH_URL"] = $APPLICATION->GetCurPageParam("login=yes", array_merge($arParamsToDelete, array("logout_butt", "backurl")));

	$arParams["REGISTER_URL"] = ($arParams["REGISTER_URL"] <> ''? $arParams["REGISTER_URL"] : $currentUrl);
	$arParams["FORGOT_PASSWORD_URL"] = ($arParams["FORGOT_PASSWORD_URL"] <> ''? $arParams["FORGOT_PASSWORD_URL"] : $arParams["REGISTER_URL"]);

	$url = urlencode($APPLICATION->GetCurPageParam("", array_merge($arParamsToDelete, array("backurl"))));

	$custom_reg_page = COption::GetOptionString('main', 'custom_register_page');
	$arResult["AUTH_REGISTER_URL"] = ($custom_reg_page <> ''? $custom_reg_page : $arParams["REGISTER_URL"].(strpos($arParams["REGISTER_URL"], "?") !== false? "&" : "?")."register=yes&backurl=".$url);
	$arResult["AUTH_FORGOT_PASSWORD_URL"] = $arParams["FORGOT_PASSWORD_URL"].(strpos($arParams["FORGOT_PASSWORD_URL"], "?") !== false? "&" : "?")."forgot_password=yes&backurl=".$url;

	$arRes = array();
	foreach($arResult as $key=>$value)
	{
		$arRes[$key] = htmlspecialchars($value);
		$arRes['~'.$key] = $value;
	}
	$arResult = $arRes;

	$arResult["POST"] = array();
	foreach($_POST as $vname=>$vvalue)
	{
		if($vname=="USER_LOGIN" || $vname=="backurl" || $vname == "auth_service_id" || is_array($vvalue))
			continue;
		$arResult["POST"][htmlspecialchars($vname)] = htmlspecialchars($vvalue);
	}

	if(defined("HTML_PAGES_FILE") && !defined("ERROR_404"))
		$arResult["~USER_LOGIN"] = "";
	else
		$arResult["~USER_LOGIN"] = $_COOKIE[COption::GetOptionString("main", "cookie_name", "BITRIX_SM")."_LOGIN"];

	$arResult["USER_LOGIN"] = $arResult["LAST_LOGIN"] = htmlspecialchars($arResult["~USER_LOGIN"]);
	$arResult["~LAST_LOGIN"] = $arResult["~USER_LOGIN"];
	$arResult["AUTH_SERVICES"] = false;
	$arResult["CURRENT_SERVICE"] = false;
	$arResult["AUTH_SERVICES_HTML"] = '';
	if(!$USER->IsAuthorized() && $arResult["NEW_USER_REGISTRATION"] == "Y" && CModule::IncludeModule("socialservices"))
	{
		$oAuthManager = new CSocServAuthManager();
		$arServices = $oAuthManager->GetActiveAuthServices($arResult);

		if(!empty($arServices))
		{
			$arResult["AUTH_SERVICES"] = $arServices;
			if(isset($_REQUEST["auth_service_id"]) && $_REQUEST["auth_service_id"] <> '' && isset($arResult["AUTH_SERVICES"][$_REQUEST["auth_service_id"]]))
			{
				$arResult["CURRENT_SERVICE"] = $_REQUEST["auth_service_id"];
				if(isset($_REQUEST["auth_service_error"]) && $_REQUEST["auth_service_error"] <> '')
				{
					$arResult['ERROR_MESSAGE'] = $oAuthManager->GetError($arResult["CURRENT_SERVICE"], $_REQUEST["auth_service_error"]);
				}
				elseif(!$oAuthManager->Authorize($_REQUEST["auth_service_id"]))
				{
					$ex = $APPLICATION->GetException();
					if ($ex)
						$arResult['ERROR_MESSAGE'] = $ex->GetString();
				}
			}
		}
	}

	$arResult["RND"] = mt_rand(0, 99999);
	$arResult["SECURE_AUTH"] = false;
	if(!CMain::IsHTTPS() && COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y')
	{
		$sec = new CRsaSecurity();
		if(($arKeys = $sec->LoadKeys()))
		{
			$sec->SetKeys($arKeys);
			$sec->AddToForm('system_auth_form'.$arResult["RND"], array('USER_PASSWORD'));
			$arResult["SECURE_AUTH"] = true;
		}
	}

	if(isset($APPLICATION->arAuthResult))
		$arResult['ERROR_MESSAGE'] = $APPLICATION->arAuthResult;

	if($arResult['ERROR_MESSAGE'] <> '')
		$arResult['ERROR'] = true;

	if($APPLICATION->NeedCAPTHAForLogin($arResult["USER_LOGIN"]))
		$arResult["CAPTCHA_CODE"] = $APPLICATION->CaptchaGetCode();
	else
		$arResult["CAPTCHA_CODE"] = false;
		
	$arResult["CAPTCHA_REG_CODE"] = $APPLICATION->CaptchaGetCode();		
		
	if ($_POST["REG_FORM"] == "Y")
	{
		$arResult["ERROR_MESSAGE"]="";
		if(!$_POST["REG_NAME"]){
			$arResult["ERROR_MESSAGE"] .= "Укажите Имя<br/>";
			$arResult["SHOW_ERRORS_REG"] = "Y";
		}
		//Упрощаем авторизацию
		//if(!$_POST["REG_LAST_NAME"]){
		//	$arResult["ERROR_MESSAGE"] .= "Укажите Фамилию<br/>";
		//	$arResult["SHOW_ERRORS_REG"] = "Y";
		//}
		//if(!$_POST["REG_SECOND_NAME"]){
		//	$arResult["ERROR_MESSAGE"] .= "Укажите Отчество<br/>";
		//	$arResult["SHOW_ERRORS_REG"] = "Y";
		//}
		//if(!$_POST["REG_BIRTHDAY"]){
		//	$arResult["ERROR_MESSAGE"] .= "Укажите Дату Рождения<br/>";
		//	$arResult["SHOW_ERRORS_REG"] = "Y";
		//}
		//elseif(!$DB->IsDate($_POST["REG_BIRTHDAY"], "DD.MM.YYYY")){
		//	$arResult["ERROR_MESSAGE"] .= "Неверный формат Даты Рождения<br/>";
		//	$arResult["SHOW_ERRORS_REG"] = "Y";
		//	unset($_POST["REG_BIRTHDAY"]);
		//}
		//if(!$_POST["REG_CITY"]){
		//	$arResult["ERROR_MESSAGE"] .= "Укажите Город<br/>";
		//	$arResult["SHOW_ERRORS_REG"] = "Y";
		//}
		if(!$_POST["REG_EMAIL"]){
			$arResult["ERROR_MESSAGE"] .= "Укажите E-mail<br/>";
			$arResult["SHOW_ERRORS_REG"] = "Y";
		}
		elseif(!check_email($_POST["REG_EMAIL"])){
			$arResult["ERROR_MESSAGE"] .= "Не корректный E-mail<br/>";
			$arResult["SHOW_ERRORS_REG"] = "Y";
			unset($_POST["REG_EMAIL"]);
		}
		//elseif($_POST["REG_EMAIL"]!=$_POST["CONFIRM_REG_EMAIL"]){
		//	$arResult["ERROR_MESSAGE"] .= "Не совпадают поля E-mail<br/>";
		//	$arResult["SHOW_ERRORS_REG"] = "Y";
		//}
		if(!$_POST["REG_LOGIN"] || strlen($_POST["REG_LOGIN"])<3){
			$arResult["ERROR_MESSAGE"] .= "Укажите Логин (не менее 3 символов)<br/>";
			$arResult["SHOW_ERRORS_REG"] = "Y";
		}
		if(!$_POST["REG_PASSWORD"] || strlen($_POST["REG_PASSWORD"])<6) {
			$arResult["ERROR_MESSAGE"] .= "Укажите Пароль (не менее 6 символов)<br/>";
			$arResult["SHOW_ERRORS_REG"] = "Y";
		}
		elseif($_POST["REG_PASSWORD"]!= $_POST["REG_CONF_PASSWORD"]){
			$arResult["ERROR_MESSAGE"] .= "Не совпадают Пароли<br/>";
			$arResult["SHOW_ERRORS_REG"] = "Y";
		}
		$cpt = new CCaptcha();
		if (!$cpt->CheckCode($_POST["CAPTCHA_WORD"], $_POST["CAPTCHA_SID"]))
		{
			$arResult["ERROR_MESSAGE"] .= "Неверно введен код";
			$arResult["SHOW_ERRORS_REG"] = "Y";
		}
		else
		{
		
			$arFields = Array(
				"NAME" => $_POST["REG_NAME"],
				//"LAST_NAME" => $_POST["REG_LAST_NAME"],
				//"SECOND_NAME" => $_POST["REG_SECOND_NAME"],
				//"PERSONAL_BIRTHDAY" => $_POST["REG_BIRTHDAY"],
				//"PERSONAL_CITY" => $_POST["REG_CITY"],
				"EMAIL"  => $_POST["REG_EMAIL"],
				"LOGIN" => $_POST["REG_LOGIN"],
				"LID" => SITE_ID,
				"ACTIVE" => "Y",
				"GROUP_ID" => array(
					3, 
					9, 
					10, 
				),
				"PASSWORD" => $_POST["REG_PASSWORD"],
				"CONFIRM_PASSWORD" => $_POST["REG_CONF_PASSWORD"],
			);
			
			$UID = $USER->Add($arFields);
			if (IntVal($UID) > 0)
			{
				$USER->Authorize($UID);
				if(count($_REQUEST["SUBSCRIBE"])>0)
				{
					$arFields = Array(
					"USER_ID" => $UID,
					"FORMAT" => "text",
					"EMAIL" => $_POST["REG_EMAIL"],
					"ACTIVE" => "Y",
					"SEND_CONFIRM" => "Y",
					"RUB_ID" => $_REQUEST["SUBSCRIBE"]
			);
			$subscr = new CSubscription;

			//can add without authorization
			$ID = $subscr->Add($arFields);

		}

			$html="Вы успешно зарегестрированы на сайту Умных туристов!<br/>Ваш логин для входа на сайт: ".$_POST["REG_LOGIN"]."<br/>Ваш пароль для входа на сайт: ".$_POST["REG_PASSWORD"];
			$arEventFields = array(
				"EMAIL" => $_POST["REG_EMAIL"],
				"EMAIL_SUBJ" => "Успешная регистрация",
				"MESSAGE" => $html
			);
				CEvent::Send("SIMAI_REGISTER", SITE_ID, $arEventFields);
			
				LocalRedirect($APPLICATION->GetCurPage()."?inf=ok"); die();

			}
			else
			{
				$arResult["ERROR_MESSAGE"] = $USER->LAST_ERROR;
				$arResult["SHOW_ERRORS_REG"] = "Y";
			}
		}
	}
}
else //if(!$USER->IsAuthorized())
{
	$arResult["FORM_TYPE"] = "logout";

	$arResult["AUTH_URL"] = $currentUrl;
	$arResult["PROFILE_URL"] = $arParams["PROFILE_URL"].(strpos($arParams["PROFILE_URL"], "?") !== false? "&" : "?")."backurl=".urlencode($currentUrl);

	$arRes = array();
	foreach($arResult as $key=>$value)
	{
		$arRes[$key] = htmlspecialchars($value);
		$arRes['~'.$key] = $value;
	}
	$arResult = $arRes;

	$arResult["USER_NAME"] = htmlspecialcharsEx($USER->GetFullName());
	$arResult["USER_LOGIN"] = htmlspecialcharsEx($USER->GetLogin());

	$arResult["GET"] = array();
	foreach($_GET as $vname=>$vvalue)
		if(!is_array($vvalue) && $vname!="backurl" && $vname != "login" && $vname != "auth_service_id")
			$arResult["GET"][htmlspecialchars($vname)] = htmlspecialchars($vvalue);
}
$this->IncludeComponentTemplate();
?>