<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<script type="text/javascript">
    $(function(){
        $("#reg_link_id").on("click",function(){
            authFormWindow.CloseLoginForm();
            return regWindow.ShowRegForm();
        })
    });
</script>


<?if ($arResult["FORM_TYPE"] == "login"):?>
<div id="login-form-window" class="login-form-window-auth">
<div class="login-form-window-wrap">
<div id="login-form-window-header">
<div onclick="return authFormWindow.CloseLoginForm()" id="close-form-window" title="<?=GetMessage("AUTH_CLOSE_WINDOW")?>"></div></div>
<div class="title h5"><?=GetMessage("AUTH_TITLE")?></div>
<?if ($arResult["SHOW_ERRORS"] == "Y" && $arResult["ERROR"] === true):?>
<?//echo"<pre>";print_r($arResult["ERROR_MESSAGE"]);echo"</pre>";?>
<span class="errortext"><?=(is_array($arResult["ERROR_MESSAGE"]) ? $arResult["ERROR_MESSAGE"]["MESSAGE"] : $arResult["ERROR_MESSAGE"])?></span>
<?endif?>
<?
$arResult["AUTH_SERVICES"] = false;
$arResult["CURRENT_SERVICE"] = false;
if(!$USER->IsAuthorized() && CModule::IncludeModule("socialservices"))
{
	$oAuthManager = new CSocServAuthManager();
	$arServices = $oAuthManager->GetActiveAuthServices($arResult);	
	if(!empty($arServices))
	{
		$arResult["AUTH_SERVICES"] = $arServices;
		if(isset($_REQUEST["auth_service_id"]) && $_REQUEST["auth_service_id"] <> '' && isset($arResult["AUTH_SERVICES"][$_REQUEST["auth_service_id"]]))
		{
			$arResult["CURRENT_SERVICE"] = $_REQUEST["auth_service_id"];
			if(isset($_REQUEST["auth_service_error"]) && $_REQUEST["auth_service_error"] <> '')
			{
				$arResult['ERROR_MESSAGE'] = $oAuthManager->GetError($arResult["CURRENT_SERVICE"], $_REQUEST["auth_service_error"]);
			}
			elseif(!$oAuthManager->Authorize($_REQUEST["auth_service_id"]))
			{
				$ex = $APPLICATION->GetException();
				if ($ex)
					$arResult['ERROR_MESSAGE'] = $ex->GetString();
			}
		}
	}	
}

$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "", 
	array(
		"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
		"AUTH_URL"=>$arResult["AUTH_URL"],
		"POST"=>$arResult["POST"],
		"POPUP"=>"N",
		"SUFFIX"=>"form",
	), 
	$component, 
	array("HIDE_ICONS"=>"Y")
);
?>	
<form method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
	<?if (strlen($arResult["BACKURL"]) > 0):?>
		<input type='hidden' name='backurl' value='<?=$arResult["BACKURL"]?>' />
	<?endif?>
	<?foreach ($arResult["POST"] as $key => $value):?>
		<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
	<?endforeach?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />
	
		<div class="login-form-window-text">
			<label data-text="* Логин" for="auth-user-login">* Логин</label>
			<input type="text" name="USER_LOGIN" id="auth-user-login" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" tabindex="1" />
		</div>
		<div class="login-form-window-text">
			<label data-text="* Пароль" for="auth-user-password">* Пароль</label>
			<input type="password" id="auth-user-password" name="USER_PASSWORD" maxlength="50" tabindex="2" />
		</div>
		<?if ($arResult["CAPTCHA_CODE"]):?>
		<div class="login-form-window-text">
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
		</div>
		<div class="login-form-window-text">	
			<label data-text="* Введите символы с картинки" for="captchaWord">* Введите символы с картинки</label>
			<input type="text" id="captchaWord" name="captcha_word" maxlength="50" value="" /></td>
		</div>
<?endif?>
		<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
		<div class="login-form-window-checkbox">
		<input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" tabindex="3" checked="checked" /><label class="remember-text" for="USER_REMEMBER"><?=GetMessage("AUTH_REMEMBER_ME")?></label><br />
		<a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a> | <a id="reg_link_id" href="<?=$arResult["AUTH_REGISTER_URL"]?>"><?=GetMessage("AUTH_REGISTRATION")?></a>
		</div>
		<?endif?>
		<div class="login-form-window-buttons">
				<div class="g-default-button"><input type="button" class="oform simple" value="Отмена" tabindex="4" onclick="return authFormWindow.CloseLoginForm()" /></div>
				<div class="g-default-button"><input type="submit" class="oform simple" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" tabindex="5" /></div>
				<div class="g-clear"></div>			
		</div>	
</form>
</div>
</div>


<div id="reg-form-window" class="reg-form-window">
<div class="login-form-window-wrap">
<div id="reg-form-window-header">
<div onclick="return regWindow.CloseRegForm()" id="close-form-window" title="<?=GetMessage("AUTH_CLOSE_WINDOW")?>"></div></div>
<div class="title h5"><?=GetMessage("AUTH_REGISTER")?></div>
<?if ($arResult["SHOW_ERRORS_REG"] == "Y"):?>
<span class="errortext"><?=$arResult["ERROR_MESSAGE"]?></span>
<?endif?>

<form method="post" name="REGISTER_FORM" target="_top" action="">
	<?if (strlen($arResult["BACKURL"]) > 0):?>
		<input type='hidden' name='backurl' value='<?=$arResult["BACKURL"]?>' />
	<?endif?>
	<?foreach ($arResult["POST"] as $key => $value):?>
		<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
	<?endforeach?>
	<input type="hidden" name="REG_FORM" value="Y" />
		<div class="login-form-window-text">
			<label data-text="* Имя" for="reg-name-field">* Имя</label>
			<input type="text" name="REG_NAME" value="<?=$_POST["REG_NAME"]?>" id="reg-name-field" maxlength="50" tabindex="1" />
		</div>
		<!-- div class="login-form-window-text">
			<label data-text="* Фамилия" for="reg-last-name-field">* Фамилия</label>
			<input type="text" name="REG_LAST_NAME" value="<?=$_POST["REG_LAST_NAME"]?>" id="reg-last-name-field" maxlength="50" tabindex="2" />
		</div>
		<div class="login-form-window-text">
			<label data-text="* Отчество" for="reg-second-name-field">* Отчество</label>
			<input type="text" name="REG_SECOND_NAME" value="<?=$_POST["REG_SECOND_NAME"]?>" id="reg-second-name-field" maxlength="50" tabindex="3" />
		</div>
		<div class="login-form-window-text">
			<label data-text="* День Рождения" for="datepicker">* День Рождения(dd.mm.yyyy)</label>
			<input type="text" name="REG_BIRTHDAY" class="reg_birthday" id="datepicker" maxlength="50" tabindex="3" />
		</div>
		<div class="login-form-window-text">
			<label data-text="* Город" for="reg-city-field">* Город</label>
			<input type="text" name="REG_CITY" value="<?=$_POST["REG_CITY"]?>" id="reg-city-field" maxlength="50" tabindex="4" />
		</div -->
		<div class="login-form-window-text">
			<label data-text="* E-mail" for="reg-email-field">* E-mail</label>
			<input type="text" name="REG_EMAIL" value="<?=$_POST["REG_EMAIL"]?>" id="reg-email-field" maxlength="50" tabindex="1" />
		</div>
		<!-- div class="login-form-window-text">
			<label data-text="* E-mail - подтверждение" for="reg-email_confirm-field">* E-mail - подтверждение</label>
			<input type="text" name="CONFIRM_REG_EMAIL" value="<?=$_POST["CONFIRM_REG_EMAIL"]?>" id="reg-email_confirm-field" maxlength="50" tabindex="5" />
		</div -->
		<div class="login-form-window-warning">Логин не менее трех символов</div>
		<div class="login-form-window-text">
			<label data-text="* Логин" for="auth-user-login-reg">* Логин</label>
			<input type="text" name="REG_LOGIN" value="<?=$_POST["REG_LOGIN"]?>" id="auth-user-login-reg" maxlength="50" tabindex="6" />
		</div>
		<div class="login-form-window-warning">Пароль не менее 6 символов</div>
		<div class="login-form-window-text">
			<label data-text="* Пароль" for="auth-user-password-reg">* Пароль</label>
			<input type="password" id="auth-user-password-reg" name="REG_PASSWORD" maxlength="50" tabindex="7" />
		</div>
		<div class="login-form-window-text">
			<label data-text="* Повтор пароля" for="auth-user-password2-reg">* Повтор пароля</label>
			<input type="password" name="REG_CONF_PASSWORD" id="auth-user-password2-reg" maxlength="50" tabindex="8" />
		</div>
		<?if(is_array($arResult["~RUBRICS"]) && !empty($arResult["~RUBRICS"])):?>
		
		<div class="login-form-window-subscribe">
			<div class="login-form-window-warning blue"><?=GetMessage("OUR_SUBSCRIBE")?></div>
				<?foreach($arResult["~RUBRICS"] as $itemID => $itemValue):?>
				<div class="login-form-window-subscribe">
					<label for="sf_RUB_ID_<?=$itemValue["ID"]?>">
					<input type="checkbox" name="SUBSCRIBE[]" id="sf_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>"<?if($itemValue["CHECKED"]) echo " checked"?> /> <?=$itemValue["NAME"]?>
					</label>
				</div>
				<?endforeach?>
				<input type="hidden" name="sub_rub" value="Y" />
		</div>
		<?endif?>
		<div class="login-form-window-text lfwt-captcha">
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHA_REG_CODE"])?>" width="135" height="32">
			<input type="hidden" name="CAPTCHA_SID" size="3" value="<?=htmlspecialchars($arResult["CAPTCHA_REG_CODE"])?>"><input type="text" name="CAPTCHA_WORD" maxlength="50" size="3" value="">
		</div>
		<div class="login-form-window-buttons">		
			<div class="g-default-button"><input type="button" class="oform simple" value="Отмена" tabindex="4" onclick="return regWindow.CloseRegForm()" /></div>
			<div class="g-default-button"><input type="submit" class="oform simple" name="Reg" value="<?=GetMessage("AUTH_REGISTRATION")?>" tabindex="4" /></div>
			<div class="g-clear"></div>
		</div>
		
			
</form>
</div>
</div>

    <?if ($arResult["SHOW_ERRORS"] == "Y" && $arResult["ERROR"] === true):?>
        <script type="text/javascript">authFormWindow.ShowLoginForm()</script>
    <?elseif ($arResult["SHOW_ERRORS_REG"] == "Y"):?>
        <script type="text/javascript">regWindow.ShowRegForm()</script>
    <?endif?>

    <li><span><a class="" href="/auth/" onclick="return authFormWindow.ShowLoginForm()"><?=GetMessage("AUTH_LOGIN_LINK")?></a></span></li>
<?else:

	$isNTLM = false;
	if (COption::GetOptionString("ldap", "use_ntlm", "N") == "Y")
	{
		$ntlm_varname = trim(COption::GetOptionString("ldap", "ntlm_varname", "REMOTE_USER"));
		if (array_key_exists($ntlm_varname, $_SERVER) && strlen($_SERVER[$ntlm_varname]) > 0)
			$isNTLM = true;
	}

	$params = DeleteParam(array("logout", "login", "back_url_pub"));
	$logoutUrl = $APPLICATION->GetCurPage()."?logout=yes".htmlspecialchars($params == "" ? "":"&".$params);?>
    <li>
        <div class="dropdown">
            <button class="btn btn-default" style="margin: 0 5px" type="button" id="dropdownMenu1" data-toggle="dropdown">
                <?=(strlen($arResult["USER_NAME"]) > 0 ? $arResult["USER_NAME"] : $arResult["USER_LOGIN"])?>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="/personal/profile/">Моя страница</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="/personal/profile/edit.php">Редактировать персональные данные</a></li>
                <?if ($isNTLM === false):?>
                    <li role="presentation" class="divider"></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$logoutUrl?>" style="padding-bottom: 10px;">Выйти</a></li>
                <?endif;?>
            </ul>
        </div>
    </li>
<?endif?>