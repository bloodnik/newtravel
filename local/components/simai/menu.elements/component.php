<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["ID"] = intval($arParams["ID"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["SECTION_ID"] = intval($arParams["SECTION_ID"]);

$arResult["ELEMENTS"] = array();

if($this->StartResultCache())
{
	if(!CModule::IncludeModule("iblock"))
		$this->AbortResultCache();

	$rsElements = CIBlockElement::GetList(
		array("SORT"=>"ASC"),
		array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"SECTION_ID"=>$arParams["SECTION_ID"]),
		false,
		false,
		array("ID","NAME","DETAIL_PAGE_URL")
	);
	if($arParams["IS_SEF"] !== "Y")
		$rsElements->SetUrlTemplates($arParams["DETAIL_URL"]);
	else
		$rsElements->SetUrlTemplates($arParams["SEF_BASE_URL"].$arParams["DETAIL_PAGE_URL"]);
	
	while($arElement = $rsElements->GetNext())
	{
		$arResult["ELEMENTS"][] = array(
			"ID" => $arElement["ID"],
			"~NAME" => $arElement["~NAME"],
			"~DETAIL_PAGE_URL" => $arElement["~DETAIL_PAGE_URL"],
		);
	}
	
	$this->EndResultCache();
}

//In "SEF" mode we'll try to parse URL and get ELEMENT_ID from it
if($arParams["IS_SEF"] === "Y")
{
	$componentPage = CComponentEngine::ParseComponentPath(
		$arParams["SEF_BASE_URL"],
		array("detail" => $arParams["DETAIL_PAGE_URL"]),
		$arVariables
	);
	if($componentPage === "detail")
	{
		CComponentEngine::InitComponentVariables(
			$componentPage,
			array("SECTION_ID", "ELEMENT_ID"),
			array("detail" => array("SECTION_ID" => "SECTION_ID", "ELEMENT_ID" => "ELEMENT_ID")),
			$arVariables
		);
		$arParams["ID"] = intval($arVariables["ELEMENT_ID"]);
	}
}

$aMenuLinksNew = array();
foreach($arResult["ELEMENTS"] as $arElement)
{
	$aMenuLinksNew[] = array(
		htmlspecialchars($arElement["~NAME"]),
		$arElement["~DETAIL_PAGE_URL"],
		array(),
		array(
			"FROM_IBLOCK" => true,
			"IS_PARENT" => false,
			"DEPTH_LEVEL" => 1,
		),
	);
}

return $aMenuLinksNew;
?>
