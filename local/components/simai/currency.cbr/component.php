<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$date_current_file = $_SERVER["DOCUMENT_ROOT"]."/upload/currency/".date("d-m-Y").".txt";
if (file_exists($date_current_file))
{
	$r = @fopen($date_current_file,"r");
	if ($r != false)
	{
		$arResult["USD"] = trim(fgets($r,64));
		$arResult["EUR"] = trim(fgets($r,64));
		fclose($r);
	}
}
else
{
	$dom = new DomDocument();
	$dom->load("http://www.cbr.ru/scripts/XML_daily.asp");

	$currency = array();
	foreach ($dom->documentElement->childNodes as $valutes) {
		foreach ($valutes->childNodes as $valute) {
			if ($valute->nodeType == 1 && $valute->nodeName == "CharCode") {
				$code = $valute->textContent;
			}
			if ($valute->nodeType == 1 && $valute->nodeName == "Value") {
				$currency[$code] = $valute->textContent;
			}			
		}
	}
	$arResult["USD"] = str_replace(",",".",$currency["USD"]);
	$arResult["EUR"] = str_replace(",",".",$currency["EUR"]);
	
	$w = @fopen($date_current_file,"w");	
	if ($w != false)
	{
		fwrite($w,$arResult["USD"]."\n");
		fwrite($w,$arResult["EUR"]."\n");
		fclose($w);
	}
}
$this->IncludeComponentTemplate();
?>
