<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();?>

<div class="sidebar-title"><?=$arParams["NA_PARAM_NAME"]?></div>
<div class="sidebar-block">
<ul>
<?foreach($arResult["MONTH"] as $month):?>
	<?if($month["ISSET_ELEMENTS"]=="Y"):?>
        <li><a href="/blog/?arrFilter_DATE_ACTIVE_FROM_1=01.<?=$month["DATE_FORMATED"]["MONTH"]?>.<?=$month["DATE_FORMATED"]["YEAR"]?>&arrFilter_DATE_ACTIVE_FROM_2=30.<?=$month["DATE_FORMATED"]["MONTH"]?>.<?=$month["DATE_FORMATED"]["YEAR"]?>&set_filter=Фильтр&set_filter=Y" class='f'><?=$month["DATE_FORMATED"]["MONTH_NAME"]?> <?=$month["DATE_FORMATED"]["YEAR"]?><?if($arParams["KNOW_CNT_ELEMENTS"]=="Y"):?> (<?=$month["CNT"]?>)<?endif;?></a></li>
	<?endif;?>
<?endforeach;?>
</ul>
</div>