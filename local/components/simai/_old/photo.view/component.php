<?
/*****************************************
*                                        *
*   Компонент разработан Blair           *
*   Simai Studio                         *
*   http://simai.ru/                     *
*                                        *
* Редактирование пользовательских фотографий *
*                                        *
*****************************************/


if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule("iblock");
global $USER;
$arResult = array();


// Проверяем корректность входных условий
if ($arParams["IBLOCK_ID"] == "" OR !isset($arParams["SECTION"])) {  echo "Неверно заданы настройки компонента<br>\r\n";
  var_dump($arParams);
  return false;
}


if ($USER->IsAuthorized() == false) {
  echo "Необходима авторизация";
  return false;
}




// Действия

// Удаление изображения
if (isset($_GET["del_img"])) {  $res = CIBlockElement::GetByID(intval($_GET["del_img"]));
  if($ar_res = $res->GetNext()) {    if ($ar_res["CREATED_BY"] == $USER->GetID() AND $ar_res["IBLOCK_ID"] == $arParams["IBLOCK_ID"]) {      if (CIBlockElement::Delete($ar_res["ID"])) {        $arResult["del_img"] = true;
      }
    }
  }
}

// Редактирование фотографии










//
// ОТОБРАЖЕНИЕ ИНФОРМАЦИИЫ
//


// Адрес удаления фото
if (stripos($_SERVER["REQUEST_URI"], "?") == false) {
  $url_del = $_SERVER["REQUEST_URI"]."?del_img={photo}";
}
elseif (stripos($_SERVER["REQUEST_URI"], "del_img") != false OR stripos($_SERVER["REQUEST_URI"], "change_img") != false) {
  $url_del = preg_replace("#([\s\S]+)(del_img|change_img)=([\d]+?)([\s\S]*)#Ui", "\\1del_img={photo}\\4", $_SERVER["REQUEST_URI"]);
}
else {
  $url_del = $_SERVER["REQUEST_URI"]."&del_img={photo}";
}

// Адрес редактирования фото
$url_change = str_replace("del_img=", "change_img=", $url_del);



// Сохраняем в компоненте какие там адреса
$arResult["url_del"] = $url_del;
$arResult["url_change"] = str_replace("del_img=", "change_img=", $url_del);
$arResult["url"] = preg_replace("#([\s\S]+)(del_img|change_img)=([\d]+?)([\s\S]*)#Ui", "\\1\\4", $_SERVER["REQUEST_URI"]);
if (substr($arResult["url"],-1) == "?") $arResult["url"] = substr($arResult["url"], 0, -1);






// Составляем список всех секций от текущей подсекции
$sections = get_all_sections($arParams["IBLOCK_ID"], $arParams["SECTION"]);
$arResult["sections"] = $sections;


// Инфомация о редактируемой фотографии
if (isset($_GET["change_img"]) OR isset($_POST["change_img"])) {
  if (isset($_GET["change_img"])) $img = $_GET["change_img"];
  else $img = $_POST["change_img"];
  $arFilter = Array(
    "CREATED_BY"=>$USER->GetID(),
    "ID"=>$img,
    "IBLOCK_ID"=>$arParams["IBLOCK_ID"],
   );
  $res = CIBlockElement::GetList(
    Array("SORT"=>"ASC", "PROPERTY_PRIORITY"=>"ASC"),
    $arFilter,
    false,
    false,
    Array("ID", "IBLOCK_ID", "SECTION_ID", "CREATED_BY", "PROPERTY_REAL_PICTURE", "PREVIEW_PICTURE", "PREVIEW_TEXT", "DETAIL_PICTURE", "DETAIL_TEXT", "NAME", "IBLOCK_SECTION_ID")
  );

  if ($ar_fields = $res->GetNext()) {
    $arResult["change_photo"] = Array(
      "id"=>$ar_fields["ID"],
      "section"=>$ar_fields["IBLOCK_SECTION_ID"],
      "section_name"=>$sections[$ar_fields["IBLOCK_SECTION_ID"]],
      "name"=>$ar_fields["NAME"],
      "picture"=>$ar_fields["PROPERTY_REAL_PICTURE_VALUE"],
      "preview_picture"=>$ar_fields["PREVIEW_PICTURE"],
      "detail_picture"=>$ar_fields["DETAIL_PICTURE"],
      "description"=>$ar_fields["~PREVIEW_TEXT"]
    );

    // Если переданы параметры на редактирование то
    if (isset($_POST["change"])) {
      $arLoadProductArray = Array(
        "NAME"           => $_POST["name"],
        "PREVIEW_TEXT"   => $_POST["description"],
        "DETAIL_TEXT"    => $_POST["description"],
      );
      $el = new CIBlockElement;
      $res = $el->Update($ar_fields["ID"], $arLoadProductArray);
      $arResult["change"] = true;
      unset($arResult["change_photo"]);
    }
    elseif (isset($_POST["back"])) {      unset($arResult["change_photo"]);
    }

  }

}

// Составляем список всех фотографий пользователя
$arFilter = Array(
  "IBLOCK_ID"=>$arParams["IBLOCK_ID"],
  "SECTION_ID"=>$arParams["SECTION"],
  "INCLUDE_SUBSECTIONS"=>"Y",
  "CREATED_BY"=>$USER->GetID(),
);
$arSelected = Array("ID", "IBLOCK_ID", "SECTION_ID", "CREATED_BY", "PROPERTY_REAL_PICTURE", "PREVIEW_PICTURE", "PREVIEW_TEXT", "DETAIL_PICTURE", "DETAIL_TEXT", "NAME", "IBLOCK_SECTION_ID");
$res = CIBlockElement::GetList(Array("SORT"=>"ASC", "NAME"=>"ASC"), $arFilter, false, false);
while ($ar_fields = $res->GetNext()) {
  $sec = $ar_fields["IBLOCK_SECTION_ID"];
  $arr[$sec]["picture"][] = $ar_fields;
  $arr[$sec]["name"] = $sections[$sec];
}


// Перебираем все секции и где есть фотографии отображаем их
$arResult["photo"] = $arr;






$this->IncludeComponentTemplate();








//
// ДАЛЕЕ ИДЕТ ФУНКЦИОНАЛЬНАЯ ЧАСТЬ КОМПОНЕНТА
//




// Функция для определения списка секций
function get_all_sections ($IBLOCK_ID, $parent=0, $level=0) {  $sections = array();
  if ($level == 0) $sections[0] = "--Родительская категория--";

  $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'GLOBAL_ACTIVE'=>'Y', 'SECTION_ID'=>$parent);
  $db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, false);
  while($ar_result = $db_list->GetNext())
  {
    $key = $ar_result['ID'];
    $ar_result['NAME'] = str_replace("&quot;", "\"", $ar_result['NAME']);
    if (strlen($ar_result['NAME']) > 35) $ar_result['NAME'] = substr($ar_result['NAME'], 0, 32)."...";
    $sections[$key] = str_repeat("..", $level).$ar_result['NAME'];

    $next_level = $level + 1;
    $sub_sections = get_all_sections ($IBLOCK_ID, $ar_result['ID'], $next_level);
    foreach ($sub_sections as $k => $v) {      $sections[$k] = $v;
    }
    //$sections = array_merge($sections, $sub_sections); // не работает почемуто
  }

  return $sections;
}
?>
