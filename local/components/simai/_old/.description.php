<?
/*****************************************
 *                                        *
 *   Компонент разработан Blair           *
 *   Simai Studio                         *
 *   http://simai.ru/                     *
 *                                        *
 * Добавление пользовательских фотографий *
 *                                        *
 *****************************************/

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => "Старые компоненты",
    "DESCRIPTION" => "Старые компоненты",
    "ICON" => "/images/icon.gif",
    "COMPLEX" => "N",
    "PATH" => array(
        "ID" => "Simai",
        "NAME" => "Симай"
    ),
);

?>