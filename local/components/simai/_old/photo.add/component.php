<?
/*****************************************
*                                        *
*   Компонент разработан Blair           *
*   Simai Studio                         *
*   http://simai.ru/                     *
*                                        *
* Добавление пользовательских фотографий *
*                                        *
*****************************************/

//
//
// ВНИМАНИЕ ! ОБРАБОТКА РАЗМЕРОВ ИЗОБРАЖЕНИЙ ИДЕТ В ФАЙЛЕ php_interface/init.php
//
//


if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule("iblock");
global $USER;
$arResult = array();




// Проверяем корректность входных условий
if ($arParams["IBLOCK_ID"] == "" OR !isset($arParams["SECTION"]) OR !isset($arParams["COUNT_RESTRICTION"]) OR !isset($arParams["MODERATE"]) OR $arParams["COUNT"] < 1 OR $arParams["SIZE"] < 1 OR !isset($arParams["MODE"]) OR $arParams["MODE_COUNT"] < 1 OR $arParams["ANOUNCE"] < 1 OR $arParams["DETAIL"] < 1 ) {  echo "Неверно заданы настройки компонента<br>\r\n";
  var_dump($arParams);
  return false;
}


// Проверяем авторизирован ли пользователь
if ($USER->IsAuthorized() == false) {
  echo $arParams["NEED_AUTH"];
  return false;
}



// Проверяем сколько фотографий пользователь может добавить в текущий фотоальбом
$arResult["count_add"] = $arParams["COUNT"];
if (isset($_POST["form_dropdown_section"])) {  $section = intval($_POST["form_dropdown_section"]);

  // Определяем кол-во фотографий пользователя в добавляемый фотоальбом
  $arFilter = Array(
    "IBLOCK_ID"=>$arParams["IBLOCK_ID"],
    "SECTION_ID"=>$section,
    "CREATED_BY"=>$USER->GetID(),
  );
  $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter);
  $count = 0;
  while($ar_fields = $res->GetNext()) {
    $count++;
  }

  // Определяем кол-во фоток которое можно еще добавить
  $arResult["count_add"] = $arParams["COUNT"] - $count;
  if ($arResult["count_add"] < 1) $arResult["count_add"] = 0;

  if ($arParams["COUNT_RESTRICTION"] == "Y") {    $arResult["count_add"] = 9999;
  }

}


// Обработка одиночной формы добавления
if ($arParams["MODE"] == "N" AND count($_POST) > 1) {  // Проверяем есть ли фото
  if (!isset($_FILES["foto"]) OR $_FILES["foto"]["size"] == 0) $arResult["error"] = "Необходимо указать фотографию";
  if (isset($_FILES["foto"]) AND $_FILES["foto"]["size"] > $arParams["SIZE"]*1024) $arResult["error"] = "Размер фотографии не может привышать ".$arParams["SIZE"]."Кб";
  if ($_POST["name"] == "") $arResult["error"] = "Не указано название фотографии";
  $section = intval($_POST["form_dropdown_section"]);
  if ($section < 1) $arResult["error"] = "Не указан фотоальбом";
  if ($_POST["description"] == "") $arResult["error"] = "Необходимо ввести описание";

  // Смотрим можно ли еще добавлять фотографии в этот альбом
  if ($arResult["count_add"] == 0) $arResult["error"] = "Вы не можете добавлять более ".$arParams["COUNT"]." фотографии в данный фотоальбом";
  if (!isset($arResult["error"])) {
    $el = new CIBlockElement;
    $values = array(
      "REAL_PICTURE" => $_FILES["foto"],
      "PUBLIC_ELEMENT" => $arParams["MODERATE"],
      "APPROVE_ELEMENT" => $arParams["MODERATE"],
    );
    $arFields = Array(
      "IBLOCK_SECTION"    => $section,
      "IBLOCK_ID"         => $arParams["IBLOCK_ID"],
      "NAME"              => $_POST["name"],
      "ACTIVE"            => $arParams["MODERATE"],
      "DETAIL_TEXT"       => $_POST["description"],
      "DETAIL_TEXT_TYPE"  => "html",
//      "DETAIL_PICTURE"    => $_FILES["foto"],
      "PREVIEW_TEXT"      => $_POST["description"],
      "PREVIEW_TEXT_TYPE" => "html",
//      "PREVIEW_PICTURE"   => $_FILES["foto"],
      "PROPERTY_VALUES"   => $values,
      "DETAIL_PICTURE_SIZE" => $arParams["DETAIL"],
      "PREVIEW_PICTURE_SIZE" => $arParams["ANOUNCE"],
      "REAL_PICTURE_SIZE" => $arParams["REAL"]
    );
    $add = $el->Add($arFields);

    if ($add == false) {      $arResult["error"] = "Неудается добавить фотографию";
      $arResult["add"] = false;
    }
    else $arResult["add"] = true;
  }

  // Подставляем данные для повторного заполнения формы
  if (isset($arResult["error"])) {
    $arResult["form"]["name"] = $_POST["name"];
    $arResult["form"]["section"] = $section;
    $arResult["form"]["description"] = $_POST["description"];
  }
}


// Обработка множественной формы добавления
elseif ($arParams["MODE"] == "Y" AND count($_POST) > 1) {
  // Кол-во добавленных фотографий
  $arResult["add"] = 0;
  // Перебираем все воз
  for ($i = 0; $i < $arResult["count_add"] AND $i < $arParams["COUNT"]; $i++) {
    // Флаг ошибки
    $arResult["error"] = false;

    // Проверяем входные данные    if (!isset($_FILES["foto_".$i]) OR $_FILES["foto_".$i]["size"] == 0) $arResult["error"] = true;
    if (isset($_FILES["foto_".$i]) AND $_FILES["foto_".$i]["size"] > $arParams["SIZE"]*1024) $arResult["error"] = true;
    if ($_POST["name_".$i] == "") $arResult["error"] = true;
    $section = intval($_POST["form_dropdown_section"]);
    if ($section < 1) $arResult["error"] = true;

    // Если есть ошибки то пропускаем фото
    if ($arResult["error"] == true) continue;

    // Добавляем фото
    $el = new CIBlockElement;
    $values = array(
      "REAL_PICTURE" => $_FILES["foto_".$i],
      "PUBLIC_ELEMENT" => $arParams["MODERATE"],
      "APPROVE_ELEMENT" => $arParams["MODERATE"]
    );
    $arFields = Array(
      "IBLOCK_SECTION"    => $section,
      "IBLOCK_ID"         => $arParams["IBLOCK_ID"],
      "NAME"              => $_POST["name_".$i],
      "ACTIVE"            => $arParams["MODERATE"],
      "DETAIL_TEXT_TYPE"  => "html",
//      "DETAIL_PICTURE"    => $_FILES["foto_".$i],
      "PREVIEW_TEXT_TYPE" => "html",
//      "PREVIEW_PICTURE"   => $_FILES["foto_".$i],
      "PROPERTY_VALUES"   => $values,
    );
    $add = $el->Add($arFields);

    if ($add != false) {
      $arResult["add"]++;
      $arResult["count_add"]--;
    }

  }

}








// Список подкатегорий в кототорые добавлять фото
$arResult["sections"] = get_all_sections($arParams["IBLOCK_ID"], $arParams["SECTION"]);


$arResult["sections_list"]["reference"]    = array_values($arResult["sections"]);
$arResult["sections_list"]["reference_id"] = array_keys($arResult["sections"]);


// Режим добавления
$arResult["mode"] = $arParams["MODE"];
$arResult["mode_count"] = $arParams["MODE_COUNT"];

// Если кол-во фотографий которое можно добавить менее кол-ва указанных к добавлению фоток в настройках
// То уменьшаем кол-во разрешщенных фотографий
if (isset($arResult["count_add"]) AND $arResult["count_add"] < $arResult["mode_count"]) $arResult["mode_count"] = $arResult["count_add"];

// Ограничивать ли кол-во добавляемых фотографий
$arResult["count_restriction"] == $arParams["COUNT_RESTRICTION"];







$this->IncludeComponentTemplate();








//
// ДАЛЕЕ ИДЕТ ФУНКЦИОНАЛЬНАЯ ЧАСТЬ КОМПОНЕНТА
//




// Функция для определения списка секций
function get_all_sections ($IBLOCK_ID, $parent=0, $level=0) {  $sections = array();

  $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'GLOBAL_ACTIVE'=>'Y', 'SECTION_ID'=>$parent);
  $db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, false);
  while($ar_result = $db_list->GetNext())
  {
    $key = $ar_result['ID'];
    $ar_result['NAME'] = str_replace("&quot;", "\"", $ar_result['NAME']);
    if (strlen($ar_result['NAME']) > 35) $ar_result['NAME'] = substr($ar_result['NAME'], 0, 32)."...";

    // Если есть четко указанный id секции то подставляем только эту секцию
    if (isset($_GET["section"]) AND $_GET["section"] != $key) {      // бла-бла-бла
    }
    // Если секция не указана то предоставляем пользователю право выбора
    else {
      $sections[$key] = str_repeat("..", $level).$ar_result['NAME'];
    }

    $next_level = $level + 1;
    $sub_sections = get_all_sections ($IBLOCK_ID, $ar_result['ID'], $next_level);
    foreach ($sub_sections as $k => $v) {      $sections[$k] = $v;
    }
    //$sections = array_merge($sections, $sub_sections); // не работает почемуто
  }

  return $sections;
}
?>