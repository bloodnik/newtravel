<?php

// C IIIIUU? IAIEEUIEEA E OIII?A ENI?AAE?AI OI ?OI IAEIAEEE IAACU?IU NICAAAAAOEE NENOAIO
// A ia?aaiaa ia ?onneee - nicaaiea i?aau?oae io?iiai ?acia?a a aaoaeuiii iienaiee e aiiina a oioiaaea?aa
// Caoi?aiii iia eioiiaio, id eioiaeieia i?iienaiu ?anoei
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("WWW_LENINGRAD_WWW_TOCHKA_RU", "MnogoBukov"));

class WWW_LENINGRAD_WWW_TOCHKA_RU
{
    function MnogoBukov(&$arFields)
    {
      // Caaeooea auoiaa ec i?ia?aiiu cea
      // return true;

      // Niio?ei aoiaeo ee aiaaaeyaiue yeaiaio a inu cea
      // Anee aoiaeo a inu cea oi ?anaecei ecia?a?aiea
      if (in_array($arFields["IBLOCK_ID"], array(51, 53, 49, 52, 59))) {

        // Ii?aaaeyai ana ia?aiao?u ecia?a?aiey cea
        $res = CIBlockElement::GetByID($arFields["ID"]);
        $arr = $res->GetNext();

        // Auaa?aeaaai aa?a ceua ia?aiao?u ecia?a?aiey cea
        $db_props = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields["ID"],
          Array("SORT"=>"ASC"), Array("CODE"=>"REAL_PICTURE"));
        $ar_props = $db_props->Fetch();
        $image_id = $ar_props["VALUE"];

        // Nicaaai oiaiuoaiiia cei
        $anounce = ShowImageFixedSize($image_id, $arFields["PREVIEW_PICTURE_SIZE"]);
        $detail = ShowImageFixedSize($image_id, $arFields["DETAIL_PICTURE_SIZE"]);

        // Nio?aiyai a AA ceie ?anaec ecia?a?aiey cea
        $arZlo = Array(
          "PREVIEW_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$anounce),
          "DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$detail),
        );


        // Anee iieuciaaoaeu nianai ceie e caa?ocee nianai cei, oi oiaiuoaai aai ceinou ai 1024 oi?ae cea
        $real = false; global $DB;
        $res = $DB->Query("SELECT * FROM `b_file` WHERE `id`='".$image_id."'", false, $err_mess.__LINE__);
        if ($arr_res = $res->NavNext()) {
          if ($arr_res["WIDTH"] > $arFields["REAL_PICTURE_SIZE"]) {

            // ?anaecei aeaaiia cei
            $real = ShowImageFixedSize($image_id, $arFields["REAL_PICTURE_SIZE"]);

            // Caiiieiaai aeaaiia cei
            $arZlo["PROPERTY_VALUES"] = array(
               "REAL_PICTURE"      => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$real)
               /*
               "PUBLIC_ELEMENT"    =>
               "APPROVE_ELEMENT"   =>
               "vote_count"        =>
               "vote_sum"          =>
               "rating"            =>
               "FORUM_TOPIC_ID"    =>
               "FORUM_MESSAGE_CNT" =>
               */
            );

            // A ceie nenoaia oa?yaony cei iiyoiio nio?aiyai cei ianoieuei ceui ianeieuei iii auei yoiai ceui
            $db_props = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields["ID"],
              Array("SORT"=>"ASC"), Array("CODE"=>"PUBLIC_ELEMENT"));
            $ar_props = $db_props->Fetch();
            $arZlo["PROPERTY_VALUES"]["PUBLIC_ELEMENT"] = $ar_props["VALUE"];

            $db_props = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields["ID"],
              Array("SORT"=>"ASC"), Array("CODE"=>"APPROVE_ELEMENT"));
            $ar_props = $db_props->Fetch();
            $arZlo["PROPERTY_VALUES"]["APPROVE_ELEMENT"] = $ar_props["VALUE"];

            $db_props = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields["ID"],
              Array("SORT"=>"ASC"), Array("CODE"=>"vote_count"));
            $ar_props = $db_props->Fetch();
            $arZlo["PROPERTY_VALUES"]["vote_count"] = $ar_props["VALUE"];

            $db_props = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields["ID"],
              Array("SORT"=>"ASC"), Array("CODE"=>"vote_sum"));
            $ar_props = $db_props->Fetch();
            $arZlo["PROPERTY_VALUES"]["vote_sum"] = $ar_props["VALUE"];

            $db_props = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields["ID"],
              Array("SORT"=>"ASC"), Array("CODE"=>"rating"));
            $ar_props = $db_props->Fetch();
            $arZlo["PROPERTY_VALUES"]["rating"] = $ar_props["VALUE"];

            $db_props = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields["ID"],
              Array("SORT"=>"ASC"), Array("CODE"=>"FORUM_TOPIC_ID"));
            $ar_props = $db_props->Fetch();
            $arZlo["PROPERTY_VALUES"]["FORUM_TOPIC_ID"] = $ar_props["VALUE"];

            $db_props = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields["ID"],
              Array("SORT"=>"ASC"), Array("CODE"=>"FORUM_MESSAGE_CNT"));
            $ar_props = $db_props->Fetch();
            $arZlo["PROPERTY_VALUES"]["FORUM_MESSAGE_CNT"] = $ar_props["VALUE"];

          }
        }

        // Iaiiaeyai cei
        $el = new CIBlockElement;
        $res = $el->Update($arFields["ID"], $arZlo);

        // Oaaeyai eeoiaa cei
        unlink($anounce);
        unlink($detail);
        if ($real != false) unlink($real);
      }
    }
}


?>