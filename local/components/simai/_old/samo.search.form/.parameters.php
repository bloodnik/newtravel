<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"SEARCH_PAGE" => Array(
		"PARENT"=>"BASE",
		"NAME"=>"Страница вывода результата поиска",
		"TYPE"=>"STRING",
		"DEFAULT"=>'/tours/',
		"MULTIPLE"=>"N",
	),
	"PARAMETERS" => array(
		"CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
	),
);
?>
