<?php
/**********************************
*                                 *
*    Компонент разработан Blair   *
*    Simai Studio                 *
*    http://simai.ru/             *
*                                 *
*  Голосование в фотогалерее      *
*                                 *
**********************************/

//
// Автоматический инсталятор требуемых инфоблоков
//

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// Смотрим список доступных инфоблоков и определяем нужно ли создавать
$res = CIBlock::GetList(Array("SORT"=>"ASC"), Array("TYPE"=>'photo_vote_type', "CODE"=>"photo_vote"));
$ar_res = $res->Fetch();

// Если нет такого то создаем все нужное
if ($ar_res == false) {
  // Определяем список сайтов в системе
  $sites = array();
  $rsSites = CSite::GetList();
  while ($arSite = $rsSites->Fetch()) {
    $sites[] = $arSite["LID"];
    //echo "<pre>"; print_r($arSite); echo "</pre>";
  }

  // Определяем список категорий пользователей
  $groups = array();
  $rsGroups = CGroup::GetList(
    $by = "c_sort",
    $order = "asc",
    array("ACTIVE" => "Y"),
    "N"
  );
  while ($arGroups = $rsGroups->Fetch()) {    $id = $arGroups["ID"];    $groups[$id] = "R"; // даем все группам юзерам доступ на чтение
  }


  // Создаем новый тип инфоблока
  $arFields = Array(
    'ID'=>'photo_vote_type',
    'SECTIONS'=>'Y',
    'IN_RSS'=>'N',
    'SORT'=>999,
    'LANG'=>Array()
  );
  foreach ($sites as $site) {
    $arFields['LANG'][$site] = array(
      'NAME'=>'Голосования',
      'SECTION_NAME'=>'Разделы',
      'ELEMENT_NAME'=>'Элементы'
    );
  }
  $obBlocktype = new CIBlockType;
  $obBlocktype->Add($arFields);

  // Создаем инфоблок
  $ib = new CIBlock;
  $arFields = Array(
    "ACTIVE" => "Y",
    "NAME" => "Голосование пользователей",
    "CODE" => "photo_vote",
    "IBLOCK_TYPE_ID" => "photo_vote_type",
    "SITE_ID" => $sites,
    "SORT" => "999",
    "DESCRIPTION" => "Голосования пользователей для фотогалереи",
    "DESCRIPTION_TYPE" => "text",
    "WORKFLOW" => "N",
    "INDEX_SECTION" => "N",
    "INDEX_ELEMENT" => "N",
    "GROUP_ID" => $groups
  );
  $iblock_id = $ib->Add($arFields);

  // Создаем нужные нам свойства в созданном инфоблоке

  // Привязка к фотографии
  $arFields = Array(
    "NAME" => "Фотография",
    "ACTIVE" => "Y",
    "SORT" => "600",
    "CODE" => "photo_id",
    "PROPERTY_TYPE" => "E",
    "SEARCHABLE" => "N",
    "IS_REQUIRED" => "Y",
    "IBLOCK_ID" => $iblock_id,
    "LINK_IBLOCK_ID" => $arParams["IBLOCK_ID"]
  );

  $ibp = new CIBlockProperty;
  $PropID = $ibp->Add($arFields);

  // Привязка к пользователю
  $arFields = Array(
    "NAME" => "Пользователь",
    "ACTIVE" => "Y",
    "SORT" => "600",
    "CODE" => "user_id",
    "PROPERTY_TYPE" => "S",
    "SEARCHABLE" => "N",
    "IS_REQUIRED" => "Y",
    "USER_TYPE" => "UserID",
    "IBLOCK_ID" => $iblock_id,
    );

  $ibp = new CIBlockProperty;
  $PropID = $ibp->Add($arFields);

  // Привязка к фотоальбому
  $arFields = Array(
    "NAME" => "Фотоальбом",
    "ACTIVE" => "Y",
    "SORT" => "600",
    "CODE" => "album_id",
    "PROPERTY_TYPE" => "G",
    "SEARCHABLE" => "N",
    "IS_REQUIRED" => "Y",
    "IBLOCK_ID" => $iblock_id,
    "LINK_IBLOCK_ID" => $arParams["IBLOCK_ID"]
    );

  $ibp = new CIBlockProperty;
  $arParams["IBLOCK_VOTE_ID"] = $ibp->Add($arFields);


  // IP адрес голосующего
  $arFields = Array(
    "NAME" => "IP",
    "ACTIVE" => "Y",
    "SORT" => "600",
    "CODE" => "ip",
    "PROPERTY_TYPE" => "S",
    "SEARCHABLE" => "N",
    "IS_REQUIRED" => "N",
    "IBLOCK_ID" => $iblock_id
    );

  $ibp = new CIBlockProperty;
  $arParams["IBLOCK_VOTE_ID"] = $ibp->Add($arFields);

  // IP адрес голосующего HTTP_X_FORWARDED_FOR
  $arFields = Array(
    "NAME" => "HTTP_X_FORWARDED_FOR",
    "ACTIVE" => "Y",
    "SORT" => "600",
    "CODE" => "HTTP_X_FORWARDED_FOR",
    "PROPERTY_TYPE" => "S",
    "SEARCHABLE" => "N",
    "IS_REQUIRED" => "N",
    "IBLOCK_ID" => $iblock_id
    );

  $ibp = new CIBlockProperty;
  $arParams["IBLOCK_VOTE_ID"] = $ibp->Add($arFields);

  // IP адрес голосующего X_REAL_IP
  $arFields = Array(
    "NAME" => "X_REAL_IP",
    "ACTIVE" => "Y",
    "SORT" => "600",
    "CODE" => "X_REAL_IP",
    "PROPERTY_TYPE" => "S",
    "SEARCHABLE" => "N",
    "IS_REQUIRED" => "N",
    "IBLOCK_ID" => $iblock_id
    );

  $ibp = new CIBlockProperty;
  $arParams["IBLOCK_VOTE_ID"] = $ibp->Add($arFields);

}
else {  $arParams["IBLOCK_VOTE_ID"] = $ar_res["ID"];
}


?>