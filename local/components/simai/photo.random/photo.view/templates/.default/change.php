<?
/*****************************************
*                                        *
*   Компонент разработан Blair           *
*   Simai Studio                         *
*   http://simai.ru/                     *
*                                        *
* Редактирование пользовательских фотографий *
*                                        *
*****************************************/

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule("form");

?>



<h4 style="color:green">Редактирование параметров фотографии</h4>

<?php if(isset($arResult["error"])) echo "<b>".$arResult["error"]."</b><br>"; ?>

<form action="<?php echo $arResult["url"]; ?>" method="POST" ENCTYPE="multipart/form-data">
<input type="hidden" name="change_img" value="<?php echo $arResult["change_photo"]["id"]; ?>">

Фотоальбом:
<b><? echo $arResult["change_photo"]["section_name"]; ?></b><br><br>

Название фото:<br>
<input type="text" size="25" name="name" value="<?php echo $arResult["change_photo"]["name"]; ?>"><br><br>


Описание:<br>
<?$APPLICATION->IncludeComponent("bitrix:fileman.light_editor", "", array(
  "CONTENT" => $arResult["change_photo"]["description"],
  "INPUT_NAME" => "description",
  "INPUT_ID" => "",
  "WIDTH" => "500px",
	"HEIGHT" => "200px",
	"VIDEO_ALLOW_VIDEO" => "N",
	"USE_FILE_DIALOGS" => "N",
	"FLOATING_TOOLBAR" => "N",
	"ARISING_TOOLBAR" => "Y",
	"ID" => "",
	"JS_OBJ_NAME" => ""
	),
	false
);?>


<?
    if (strlen($arResult["change_photo"]["preview_picture"])>0):
        ?><br><?echo CFile::ShowImage($arResult["change_photo"]["preview_picture"]);
    endif;
?>
<br><br>
<input type="submit" value="Изменить параметры фотографии" name="change">
<input type="submit" value="Вернуться назад" name="back">
</form>

