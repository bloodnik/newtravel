<?
/*****************************************
*                                        *
*   Компонент разработан Blair           *
*   Simai Studio                         *
*   http://simai.ru/                     *
*                                        *
* Редактирование пользовательских фотографий *
*                                        *
*****************************************/

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule("form");

// Кол-во столбцов
$count_row = 3;



//
// Далее идет сам шаблон
//


if (isset($arResult["del_img"])) echo "<b><font style=\"color:red\">Фотография была удалена</font></b><br>";
if (isset($arResult["change"])) echo "<b><font style=\"color:red\">Фотография была отредактирована</font></b><br>";





foreach ($arResult["photo"] as $section_id => $section) {  echo "<h3>".$section["name"]."</h3><br>";

  echo "<table border=\"1\" cellpadding=\"10\"><tr>\r\n";
  $i = 0;
  foreach ($section["picture"] as $photo) {    $i++;
    if ($i > $count_row) {$i=1; echo "</tr><tr>";}    echo "\t<td width=\"33%\">Название: <B>".$photo["NAME"]."</B><br>
          Описание: ".$photo["PREVIEW_TEXT"]."<br>
          Изображение: ".CFile::ShowImage($photo["PREVIEW_PICTURE"])."<br>
          <a href=\"".str_replace("{photo}", $photo["ID"], $arResult["url_change"])."\">Редактировать фотографию</a><br>
          <a href=\"".str_replace("{photo}", $photo["ID"], $arResult["url_del"])."\">Удалить фотографию</a>
          <br><br><br></td>\r\n";

  }

  // Заполняем до конца таблицу
  while ($i < $count_row && $i != 0) {    echo "\t<td>&nbsp;</td>\r\n";    $i++;
  }  echo "</tr></table><br><br>\r\n\r\n";
}


?>

