<?
/*****************************************
*                                        *
*   Компонент разработан Blair           *
*   Simai Studio                         *
*   http://simai.ru/                     *
*                                        *
* Редактирование пользовательских фотографий*
*                                        *
*****************************************/

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
  "NAME" => "Редактирование пользовательских фотографий",
  "DESCRIPTION" => "Редактирования фотографий которые были заполнены пользователем в различные фотоальбомы",
	"ICON" => "/images/icon.gif",
	"COMPLEX" => "N",
	"PATH" => array(
		"ID" => "simai",
		"NAME" => "Симай"
	),
);

?>