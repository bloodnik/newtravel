<?
/*****************************************
*                                        *
*   Компонент разработан Blair           *
*   Simai Studio                         *
*   http://simai.ru/                     *
*                                        *
* Добавление пользовательских фотографий *
*                                        *
*****************************************/

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


CModule::IncludeModule("iblock");


// многобуков - выбор блока для засылки космических сообщений с марса
$arTypesEx = Array("-"=>" ");
$db_iblock_type = CIBlockType::GetList(Array("SORT"=>"ASC"));
while($arRes = $db_iblock_type->Fetch())
  if($arIBType = CIBlockType::GetByIDLang($arRes["ID"], LANG))
    $arTypesEx[$arRes["ID"]] = $arIBType["NAME"];

$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
  $arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$arSorts = Array("ASC"=>GetMessage("T_IBLOCK_DESC_ASC"), "DESC"=>GetMessage("T_IBLOCK_DESC_DESC"));
$arSortFields = Array(
		"ID"=>GetMessage("T_IBLOCK_DESC_FID"),
		"NAME"=>GetMessage("T_IBLOCK_DESC_FNAME"),
		"ACTIVE_FROM"=>GetMessage("T_IBLOCK_DESC_FACT"),
		"SORT"=>GetMessage("T_IBLOCK_DESC_FSORT"),
		"TIMESTAMP_X"=>GetMessage("T_IBLOCK_DESC_FTSAMP")
	);

$arProperty_LNS = array();
$rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(isset($arCurrentValues["IBLOCK_ID"])?$arCurrentValues["IBLOCK_ID"]:$arCurrentValues["ID"])));
while ($arr=$rsProp->Fetch())
{
	$arProperty[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S")))
	{
		$arProperty_LNS[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	}
}
// - конец многобуков



// Список возможных сортировок
$moderate = array (
  "Y" => "Требуется предмодерация",
  "N" => "Сразу добавлять"
);


// Составляем список секций
$sections = array();
if ($arCurrentValues["IBLOCK_ID"] != "") {
  $sections = get_all_sections($arCurrentValues["IBLOCK_ID"]);
}


$arComponentParameters = array(
  "GROUPS" => array(
     "GENERAL" => array(
        "NAME" => "Общие настройки компонента"
     ),
  ),

  "PARAMETERS" => array(

      // Общие настройки
      "IBLOCK_TYPE" => Array(
        "PARENT" => "GENERAL",
        "NAME" => "Тип информационного блока",
        "TYPE" => "LIST",
        "VALUES" => $arTypesEx,
        "REFRESH" => "Y",
      ),
      "IBLOCK_ID" => Array(
        "PARENT" => "GENERAL",
        "NAME" => "Код информационного блока",
        "TYPE" => "LIST",
        "VALUES" => $arIBlocks,
        "ADDITIONAL_VALUES" => "Y",
        "REFRESH" => "Y",
      ),
      "SECTION" => Array(
        "PARENT" => "GENERAL",
        "NAME" => "Родительская секция инфоблока",
        "TYPE" => "LIST",
        "VALUES" => $sections,
      ),


  )
);



// Функция для определения списка секций
function get_all_sections ($IBLOCK_ID, $parent=0, $level=0) {

  $sections = array();
  if ($level == 0) $sections[0] = "--Родительская категория--";

  $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'GLOBAL_ACTIVE'=>'Y', 'SECTION_ID'=>$parent);
  $db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, false);
  while($ar_result = $db_list->GetNext())
  {

    $key = $ar_result['ID'];
    $ar_result['NAME'] = str_replace("&quot;", "\"", $ar_result['NAME']);
    if (strlen($ar_result['NAME']) > 35) $ar_result['NAME'] = substr($ar_result['NAME'], 0, 32)."...";
    $sections[$key] = str_repeat("..", $level).$ar_result['NAME'];

    $next_level = $level + 1;
    $sub_sections = get_all_sections ($IBLOCK_ID, $ar_result['ID'], $next_level);
    foreach ($sub_sections as $k => $v) {
      $sections[$k] = $v;
    }
    //$sections = array_merge($sections, $sub_sections); // не работает почемуто
  }

  return $sections;
}
?>