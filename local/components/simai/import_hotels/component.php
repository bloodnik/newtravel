<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/*
<?/*$APPLICATION->IncludeComponent(
	"simai:import_hotels",
	"",
	Array(
		"PARSE_URL" => "http://demotravel.ru/wc/hotels.html",
		"CODING" => "1",
		"QUERY" => ".tp_co_maintable_td a",
		"MAX_LEVEL" => "",
	),
false
);?>
*/
function quotes($s)
{
	return '"'.$s.'"';
}

if(!isset($arParams["PARSE_URL"]) || empty($arParams["PARSE_URL"]))
{
	ShowError("Не введен адрес");
	return;
}
if(!isset($arParams["MAX_LEVEL"]) || empty($arParams["MAX_LEVEL"]))
{
	$arParams["MAX_LEVEL"] = null;
}
elseif(is_numeric($arParams["MAX_LEVEL"]) && $arParams["MAX_LEVEL"] <= 0)
{
	ShowError("Не правильный параметр 'Максимальная глубина вывода массива'");
	return;
}
if(!isset($arParams["QUERY"]) || empty($arParams["QUERY"]))
{
	ShowError("Не введена строка запроса");
	return;
}
$arParams["PARSE_URL"] = trim($arParams["PARSE_URL"]);
$arParams["QUERY"] = trim($arParams["QUERY"]);
if($arParams["MAX_LEVEL"] !== null)
	$arParams["MAX_LEVEL"] = IntVal(trim($arParams["MAX_LEVEL"]));


$filename = 'import_hotels.csv';
$url = $arParams["PARSE_URL"];
$html = file_get_contents($url);
if($html === false)
{
	$this->AbortResultCache();
	ShowError("Документ по адресу недоступен: ".$arParams["PARSE_URL"]);
	return;
}
if(ini_get('safe_mode'))
{
	$this->AbortResultCache();
	ShowError("Невозможно установить set_time_limit(), PHP использует safe_mode");
	return;
}
set_time_limit(0);// бесконечное время
$saw = new nokogiri($html, $arParams["CODING"]);
$arCountries = $saw->get($arParams["QUERY"])->toArray($arParams["MAX_LEVEL"]);
$arHotels = array();
foreach($arCountries as $countryCount=>$arCountry)
{
	$arNextLink = Null;
	$arHotelsList = array();
	while(true)
	{
		if(!isset($arNextLink))
			$section_url = $url.$arCountry['href'];
		elseif(!empty($arNextLink['href']))
			$section_url = $url.$arNextLink['href'];
		else
			break;
		$html = file_get_contents($section_url);
		$saw = new nokogiri($html, $arParams["CODING"]);
		$arHotelsList[] = $saw->get('.tb_hotels_catalogue tr')->toArray();
		$arNextLink = $saw->get('.next .linkk a')->toArray();
	}
	/*foreach($arHotelsList as $hotels)
	{
		foreach($hotels as $hotel)
		{
			foreach($hotel as $tag => $row) if ($tag == 'td')
			{
				$arHotel = array(
					'COUNTRY' => quotes(trim($arCountry['#text'])),
					'ZONE' => quotes(trim($row[1]['p'][0]['#text'])),
					'NAME' => quotes(trim($row[1]['div'][0]['a']['#text'])),
					'TYPE' => quotes(trim($row[1]['p'][1]['#text'])),
					'RATING' => quotes(trim($row[2]['#text'])),
					'COMMENTS_COUNT' => trim($row[3]['a'][0]['#text']),
					'COMMENTS_URL' => quotes(trim($row[3]['a'][0]['href'])),
					'PHOTO_COUNT' => trim($row[3]['a'][1]['#text']),
					'PHOTO_URL' => quotes(trim($row[3]['a'][1]['href']))
				);
				if(!file_exists($filename))
				{
					$arCsvNames = array(
						'IC_GROUP0',  //COUNTRY
						'IC_GROUP1',  //ZONE
						'IE_NAME',    //NAME
						'IP_PROP71',  //TYPE
						'IP_PROP69',  //RATING
						'IP_PROP72',  //COMMENTS_COUNT
						'IP_PROP73',  //COMMENTS_URL
						'IP_PROP74',  //PHOTO_COUNT
						'IP_PROP75'   //PHOTO_URL
					);
					$res = CIBlockElement::GetList(array("SORT"=>"ASC"), array("NAME"), false, false, array());
					file_put_contents($filename, implode(';', $arCsvNames)."\n");
				}
				if(file_exists($filename.'.stop')) die('Interrupt script!');
				
				$hash = md5(trim($arCountry['#text'])."/".trim($row[1]['p'][0]['#text'])."/".trim($row[1]['div'][0]['a']['#text']));
				$res = CIBlockElement::GetList(array("ID"=>"ASC"), array("IBLOCK_ID"=>2, "PROPERTY_HASH"=>$hash), false, false, array());
				if(!intval($res->SelectedRowsCount())>0)
				{
					$str = implode(';', $arHotel)."\n";
					I($str);
					break;
					//file_put_contents($filename, $str, FILE_APPEND);
				}
			}
		}
	}*/
}
//file_put_contents($filename.'.ok', 'ok');
?>
