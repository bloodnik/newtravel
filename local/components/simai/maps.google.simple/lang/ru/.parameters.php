<?php

$MESS['SM_GOOGLE_MAP_DATA'] = 'Данные карты';
$MESS['SM_GOOGLE_PARAM_DATA_SET'] = 'Задать';

$MESS['SM_GOOGLE_PARAM_MAP_START_PLACE'] = 'Начальный объект карты (город)';

$MESS['SM_GOOGLE_PARAM_DATA_DEFAULT_LAT'] = '55.7383';
$MESS['SM_GOOGLE_PARAM_DATA_DEFAULT_LON'] = '37.5946';

$MESS['SM_GOOGLE_PARAM_MAP_TYPE_ROADMAP'] = 'Обычная (карта дорог)';
$MESS['SM_GOOGLE_PARAM_MAP_TYPE_SATELLITE'] = 'Спутник';
$MESS['SM_GOOGLE_PARAM_MAP_TYPE_HYBRID'] = 'Гибридная';
$MESS['SM_GOOGLE_PARAM_MAP_TYPE_TERRAIN'] = 'Рельеф';

$MESS['SM_GOOGLE_PARAM_MAP_HEIGHT'] = 'Высота карты';
$MESS['SM_GOOGLE_PARAM_MAP_WIDTH'] = 'Ширина карты';
$MESS['SM_GOOGLE_PARAM_MAP_ZOOM'] = 'Маштаб карты (от 0 до 18)';

$MESS['SM_GOOGLE_PARAM_ENABLE_ZOOM_CONTROL'] = 'Ползунок масштаба';
$MESS['SM_GOOGLE_PARAM_ENABLE_DBLCLICK_ZOOM'] = 'Приближение по двойному щелчку';
$MESS['SM_GOOGLE_PARAM_ENABLE_FULLSCREEN_BUTTON'] = 'Кнопка перехода в полноэкранный режим';
$MESS['SM_GOOGLE_PARAM_ENABLE_GEOCLICKER'] = 'Отображать сведения о гео объектах по щелчку';
$MESS['SM_GOOGLE_PARAM_ENABLE_RIGHTBUTTON_MAGNIFIER'] = 'Приближение области правой кнопкой мыши';
$MESS['SM_GOOGLE_PARAM_ENABLE_PAN_CONTROL'] = 'Панорамирование';
$MESS['SM_GOOGLE_PARAM_ENABLE_MAP_TYPE_CONTROL'] = 'Выбор типа карты';

$MESS['SM_GOOGLE_DESC_PAGER_NEWS'] = 'Настройки карт Google';

?>
