<?
$MESS['SM_GOOGLE_SET_POPUP_TITLE'] = "Настройки карты 2GIS";
$MESS['SM_GOOGLE_SET_POPUP_WINDOW_TITLE'] = "Редактирование настроек карты";
$MESS['SM_GOOGLE_SET_POPUP_WINDOW_DESCRIPTION'] = "Управление видом карты и списком отображаемых объектов";
$MESS['SM_GOOGLE_SET_START_POS'] = "Начальная позиция карты";
$MESS['SM_GOOGLE_SET_START_POS_LAT'] = "Широта";
$MESS['SM_GOOGLE_SET_START_POS_LON'] = "Долгота";
$MESS['SM_GOOGLE_SET_START_POS_SCALE'] = "Масштаб";
$MESS['SM_GOOGLE_SET_POINTS_ADD_DESCRIPTION'] = "Отметьте двойным щелчком мыши произвольное количество точек на карте.";
$MESS['SM_GOOGLE_GEO_SEARCH'] = "Поиск по карте";
$MESS['SM_GOOGLE_TYPE_CITY'] = 'Город';
$MESS['SM_GOOGLE_TYPE_SETTLEMENT'] = 'Поселение';
$MESS['SM_GOOGLE_TYPE_DISTRICT'] = 'Район';
$MESS['SM_GOOGLE_TYPE_STREET'] = 'Улица/Трасса';

$MESS['SM_GOOGLE_SET_POINTS'] = "Точки карты";
$MESS['SM_GOOGLE_SET_POINT_DEL'] = "Удалить";
$MESS['SM_GOOGLE_SET_POINT_DESC'] = "Описание";

$MESS['SM_GOOGLE_SET_SUBMIT'] = "Сохранить";

?>