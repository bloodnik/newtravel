<?
$MESS ['SM_GOOGLE_SET_POPUP_TITLE'] = "2GIS map settings";
$MESS ['SM_GOOGLE_SET_POPUP_WINDOW_TITLE'] = "Edit map settings";
$MESS ['SM_GOOGLE_SET_POPUP_WINDOW_DESCRIPTION'] = "View and displayed objects settings";
$MESS ['SM_GOOGLE_SET_START_POS'] = "Starting map position";
$MESS ['SM_GOOGLE_SET_START_POS_LAT'] = "Lattitude";
$MESS ['SM_GOOGLE_SET_START_POS_LON'] = "Longitude";
$MESS ['SM_GOOGLE_SET_START_POS_SCALE'] = "Scale";
$MESS ['SM_GOOGLE_SET_POINTS_ADD_DESCRIPTION'] = "mark any number of points by double click.";
$MESS ['SM_GOOGLE_GEO_SEARCH'] = "Map search";
$MESS ['SM_GOOGLE_TYPE_CITY'] = 'City';
$MESS ['SM_GOOGLE_TYPE_SETTLEMENT'] = 'Settlement';
$MESS ['SM_GOOGLE_TYPE_DISTRICT'] = 'District';
$MESS ['SM_GOOGLE_TYPE_STREET'] = 'Street/Road';

$MESS ['SM_GOOGLE_SET_POINTS'] = "Map placemarks";
$MESS ['SM_GOOGLE_SET_POINT_DEL'] = "Delete";
$MESS ['SM_GOOGLE_SET_POINT_DESC'] = "Description";

$MESS ['SM_GOOGLE_SET_SUBMIT'] = "Save";

?>