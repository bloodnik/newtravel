<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");

__IncludeLang($_SERVER['DOCUMENT_ROOT'].'/bitrix/components/simai/maps.google.simple/lang/'.LANGUAGE_ID.'/settings.php');

//if(!$USER->IsAdmin())
//	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$obJSPopup = new CJSPopup( '',
	array(
		'TITLE' => GetMessage('SM_GOOGLE_SET_POPUP_TITLE'),
		'SUFFIX' => 'map_2gis',
		'ARGS' => ''
	)
);

$arData = array();
if( $_REQUEST['MAP_DATA'] )
{
	CUtil::JSPostUnescape();
	if( CheckSerializedData($_REQUEST['MAP_DATA']) )
	{
		$arData = unserialize( $_REQUEST['MAP_DATA'] );
		if( is_array($arData) && is_array($arData['PLACEMARKS']) && ($cnt = count($arData['PLACEMARKS'])) )
		{
			for( $i = 0; $i < $cnt; $i++ )
			{
				$arData['PLACEMARKS'][$i]['TEXT'] = str_replace('###RN###', "\r\n", $arData['PLACEMARKS'][$i]['TEXT']);
			}
		}
	}
}

$MAP_ID = str_replace( '.', '', uniqid( 'mapg_', true ) );

?>

<script type="text/javascript">
    BX.loadCSS( '/bitrix/components/simai/maps.google.simple/settings/settings.css' );
    BX.loadScript( 'https://maps.googleapis.com/maps/api/js?v=3&sensor=false&callback=mapgoogle_createMap_<?=$MAP_ID?>',
		function() {
			//setTimeout( mapgoogle_wait4load_<?=$MAP_ID?>, 100 );
			//alert( 'BX.loadScript() OK' );
		}
	);
</script>

<script type="text/javascript" src="/bitrix/components/simai/maps.google.simple/settings/serialize_php.js"></script>

<form name="bx_popup_form_google_map">
<?php
$obJSPopup->ShowTitlebar();
$obJSPopup->StartDescription( 'bx-edit-menu' );
?>
    <p><b><?echo GetMessage('SM_GOOGLE_SET_POPUP_WINDOW_TITLE')?></b></p>
    <p class="note"><?echo GetMessage('SM_GOOGLE_SET_POPUP_WINDOW_DESCRIPTION')?></p>
<?php
$obJSPopup->StartContent();
?>

<script type="text/javascript">
    // Google maps JavaScripts
    var mapg_<?=$MAP_ID?> = null; // map itself
    //
    // map load handler
    function mapgoogle_createMap_<?=$MAP_ID?>() {
		var map_center = new google.maps.LatLng( <?=$arData['LAT']?>, <?=$arData['LON']?> );
        var map_options = {
			zoom:               <?=$arData['SCALE']?>,
			center:             map_center,
			mapTypeId:          google.maps.MapTypeId.ROADMAP,
			disableDefaultUI:   true,
			zoomControl:        true,
			scaleControl:       true,
			panControl:         false,
			streetViewControl:  false,
			overviewMapControl: false
		}; // map options
		var divka = document.getElementById( 'mapgoogle_<?=$MAP_ID?>_map' );
		mapg_<?=$MAP_ID?> = new google.maps.Map( divka, map_options );
		google.maps.event.addListener( mapg_<?=$MAP_ID?>, 'zoom_changed', mapgoogle_onZoomChanged_<?=$MAP_ID?> );
		google.maps.event.addListener( mapg_<?=$MAP_ID?>, 'dragend', mapgoogle_onDragEnd_<?=$MAP_ID?> );
		mapgoogle_geosearch_<?=$MAP_ID?>.initialize();
    }
	//
	function mapgoogle_onZoomChanged_<?=$MAP_ID?>() {
		var z = mapg_<?=$MAP_ID?>.getZoom();
		BX('map_zoom_<?=$MAP_ID?>').value = z;
	}
	//
	function mapgoogle_onDragEnd_<?=$MAP_ID?>() {
		var obPos = mapg_<?=$MAP_ID?>.getCenter();
		var z = mapg_<?=$MAP_ID?>.getZoom();
		var google_lat = obPos.lat();
		var google_lon = obPos.lng();
		BX('map_center_lat_<?=$MAP_ID?>').value = google_lat;
		BX('map_center_lon_<?=$MAP_ID?>').value = google_lon;
		BX('map_zoom_<?=$MAP_ID?>').value = z;
	}
	//
	function mapgoogle_saveChanges_<?=$MAP_ID?>() {
        // get map center & scale
        var m_lon   = BX('map_center_lon_<?=$MAP_ID?>').value;
        var m_lat   = BX('map_center_lat_<?=$MAP_ID?>').value;
        var m_scale = BX('map_zoom_<?=$MAP_ID?>').value;
        // create MAP_DATA array structure
        var to_save = {
            LON        : m_lon,
            LAT        : m_lat,
            SCALE      : m_scale
        };
        var serialized_string = serialize_php( to_save );
        // finally, save serialized data
        if( window.jsGoogleCEOpener )
            window.jsGoogleCEOpener.SaveData( serialized_string );
	}
	//
	mapgoogle_geosearch_<?=$MAP_ID?> = {
		//
		input: null,
		out_ul: null,
		ar_results: [],
		timerID: null,
		timerDelay: 1000,
		geocoder: null,
		//
		initialize: function() {
			var _this = mapgoogle_geosearch_<?=$MAP_ID?>;
			_this.input = document.getElementById( 'mapgoogle_geosearch_text<?=$MAP_ID?>' );
			if( _this.input == null )
				alert( 'Error: cannot find input :(' );
		},
		//
		onblur: function() {
			var _this = mapgoogle_geosearch_<?=$MAP_ID?>;
			_this.showResults( false );
		},
		onfocus: function() {
			var _this = mapgoogle_geosearch_<?=$MAP_ID?>;
			_this.showResults( true );
		},
		onkeypress: function( evt ) {
			var _this = mapgoogle_geosearch_<?=$MAP_ID?>;
			if( evt == null )
                evt = window.event;
            // search immediately on <Enter>
            if( evt != null )
            {
                if( evt.keyCode == 13 )
                {
                    _this.doSearch();
                    return;
                }
            }
            // cancel existing timer
            if( _this.timerID )
                clearTimeout( _this.timerID );
            // set new timer
            _this.timerID = setTimeout( mapgoogle_geosearch_<?=$MAP_ID?>.doSearch, _this.timerDelay );
		},
		//
		doSearch: function() {
			var _this = mapgoogle_geosearch_<?=$MAP_ID?>;
			var str = _this.input.value;
			if( str.length > 1 )
			{
				if( null == _this.geocoder )
					_this.geocoder = new google.maps.Geocoder();
				_this.geocoder.geocode( { address: str }, function( obResult, status ) {
					if( status != google.maps.GeocoderStatus.OK && status != google.maps.GeocoderStatus.ZERO_RESULTS )
					{
						_this.showError( status );
						return;
					}
					if( !obResult )
					{
						_this.showError();
						return;
					}
					_this.clearResults();
					var len = obResult.length;
					for( var i=0; i<len; i++ )
					{
						var position = obResult[i].geometry.location;
						var text = '';
						for( var j=0; j<obResult[i].address_components.length; j++ )
						{
							text += (text.length > 0 ? ', ' : '') + obResult[i].address_components[j].long_name;
						}
						_this.ar_results[i] = {
							name: text,
							lat: position.lat(),
							lon: position.lng()
						};
					}
					_this.generateResults();
					_this.showResults( true );
				} );
			}
		},
		//
		generateResults: function() {
			var _this = mapgoogle_geosearch_<?=$MAP_ID?>;
			if( _this.out_ul == null )
            {
                if( _this.input == null )
                    _this.input = document.getElementById( 'mapgoogle_geosearch_text<?=$MAP_ID?>' );
                var obPos = jsUtils.GetRealPos( _this.input );
                _this.out_ul = document.body.appendChild( document.createElement('UL') );
                _this.out_ul.className = 'simai-address-search-results';
                _this.out_ul.style.top = (obPos.bottom + 2) + 'px';
                _this.out_ul.style.left = obPos.left + 'px';
                _this.out_ul.style.display = 'none';
            }
            var len = _this.ar_results.length;
            for( var i=0; i<len; i++ )
            {
                var lnk_text = '' + _this.ar_results[i].name;
                _this.out_ul.appendChild( BX.create( 'LI', {
                    attrs: {className: i == 0 ? 'simai-address-search-result-first' : ''},
                    children: [
                        BX.create( 'A', {
                            attrs:  { href: "javascript:void(0)" },
                            props:  { BXSearchIndex: i },
                            events: { click: _this.onClickResult },
                            children: [
                                BX.create( 'SPAN', {
                                    text: lnk_text
                                } )
                            ]
                        } )
                    ] // children
                } ) );
            }
		},
		//
		showError: function( err ) {
			//var _this = mapgoogle_geosearch_<?=$MAP_ID?>;
			var s = 'Search error';
			if( err ) s += (': ' + err);
			alert( s );
		},
		//
		showResults: function( show ) {
			var _this = mapgoogle_geosearch_<?=$MAP_ID?>;
			if( _this.out_ul == null )
                return;
            if( show == false) {
                setTimeout( function() { mapgoogle_geosearch_<?=$MAP_ID?>.out_ul.style.display = 'none' }, 500 );
                return;
            }
            _this.out_ul.style.display = 'block';
		},
		//
		clearResults: function() {
			var _this = mapgoogle_geosearch_<?=$MAP_ID?>;
			_this.ar_result = null;
            // remove UL block
            if( _this.out_ul != null )
            {
                _this.out_ul.parentNode.removeChild( _this.out_ul );
                _this.out_ul = null;
            }
		},
		//
		onClickResult: function() {
			var _this = mapgoogle_geosearch_<?=$MAP_ID?>;
			if( this.BXSearchIndex == null )
                return;
            if( _this.ar_results == null)
                return;
            //
            var el = _this.ar_results[ this.BXSearchIndex ];
			var latLng = new google.maps.LatLng( el.lat, el.lon );
            mapg_<?=$MAP_ID?>.panTo( latLng );
		}
	};
    //
</script>

<!-- 2GIS map -->
<div id="mapgoogle_wrapper" class="mapgoogle_wrapper">
    <div id="mapgoogle_<?=$MAP_ID?>_map" style="width: 450px; height: 400px;">Google Map</div>
    <?php echo GetMessage('SM_GOOGLE_GEO_SEARCH'); ?>:
    <input type="text" id="mapgoogle_geosearch_text<?=$MAP_ID?>" value="" size="40"
           onblur="mapgoogle_geosearch_<?=$MAP_ID?>.onblur()"
           onfocus="mapgoogle_geosearch_<?=$MAP_ID?>.onfocus()"
           onkeyup="mapgoogle_geosearch_<?=$MAP_ID?>.onkeypress(event)"
           onkeypress="mapgoogle_geosearch_<?=$MAP_ID?>.onkeypress(event)" />
</div>
<div class="mapgoogle_set_row">
    <b><?=GetMessage('SM_GOOGLE_SET_START_POS')?></b>:
</div>
<div class="mapgoogle_set_row">
    <?=GetMessage('SM_GOOGLE_SET_START_POS_LAT')?>:
    <input type="text" id="map_center_lat_<?=$MAP_ID?>" value="<?=CUtil::JSEscape($arData['LAT'])?>" size="18" />
</div>
<div class="mapgoogle_set_row">
    <?=GetMessage('SM_GOOGLE_SET_START_POS_LON')?>:
    <input type="text" id="map_center_lon_<?=$MAP_ID?>" value="<?=CUtil::JSEscape($arData['LON'])?>" size="18" />
</div>
<div class="mapgoogle_set_row">
    <?=GetMessage('SM_GOOGLE_SET_START_POS_SCALE')?>:
    <input type="text" id="map_zoom_<?=$MAP_ID?>" value="<?=CUtil::JSEscape($arData['SCALE'])?>" size="5" />
</div>

<br />

<?php
$obJSPopup->StartButtons(); // has echo '</form>'."\r\n";
?>

<input type="submit" value="<?echo GetMessage('SM_GOOGLE_SET_SUBMIT')?>" onclick="return mapgoogle_saveChanges_<?=$MAP_ID?>();"/>

<?php
$obJSPopup->ShowStandardButtons( array('cancel') );
$obJSPopup->EndButtons();
?>

<?php require( $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_js.php" );?>