function OnGoogleMapSettingsEdit( arParams )
{
	if( null != window.jsGoogleCEOpener )
	{
		try { window.jsGoogleCEOpener.Close(); } catch (e) {}
		window.jsGoogleCEOpener = null;
	}
	window.jsGoogleCEOpener = new JCEditorOpener( arParams );
}

function JCEditorOpener( arParams )
{
	this.arParams = arParams;
	this.jsOptions = this.arParams.data.split('||');
	// 'JS_DATA'  => LANGUAGE_ID.'||'.GetMessage('SM_GOOGLE_PARAM_DATA_SET')
	// jsOptions[0] - language
	// jsOptions[1] - button text

	var obButton = this.arParams.oCont.appendChild( BX.create( 'BUTTON', {
		html: this.jsOptions[1]
	} ) );
	obButton.onclick = BX.delegate( this.btnClick, this );
	this.saveData = BX.delegate( this.__saveData, this );
}

JCEditorOpener.prototype.btnClick = function ()
{
	this.arElements = this.arParams.getElements();

	if (!this.arElements)
		return false;

	if (window.jsPopup_google_map == null)
	{
		var strUrl = '/bitrix/components/simai/maps.google.simple/settings/settings.php'
			+ '?lang=' + this.jsOptions[0];
		var strUrlPost = 'MAP_DATA=' + BX.util.urlencode( this.arParams.oInput.value );

		window.jsPopup_google_map = new BX.CDialog( {
			'content_url' : strUrl,
			'content_post': strUrlPost,
			'width'       : 800,
			'height'      : 600,
			'resizable'   : false
		} );
	}

	window.jsPopup_google_map.Show();
	window.jsPopup_google_map.PARAMS.content_url = '';

	return false;
}


JCEditorOpener.prototype.Close = function( e )
{
	if( false !== e ) BX.util.PreventDefault( e );
	if( null != window.jsPopup_google_map )
	{
		window.jsPopup_google_map.Close();
	}
}


JCEditorOpener.prototype.SaveData = function( strData )
{
	this.arParams.oInput.value = strData;
    // invoke onchange handler if any, on input
	if( null != this.arParams.oInput.onchange )
		this.arParams.oInput.onchange();
    // close dialog
	this.Close( false );
}
