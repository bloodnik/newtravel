﻿<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arComponentParameters = array(
	'GROUPS' => array(
	),
    // BASE SETTINGS
	'PARAMETERS' => array(
		'MAP_DATA' => array(
			'PARENT'   => 'BASE',
            'NAME'     => GetMessage('SM_GOOGLE_MAP_DATA'),
            'TYPE'     => 'CUSTOM',
            'MULTIPLE' => 'N',
			'JS_FILE'  => '/bitrix/components/simai/maps.google.simple/settings/settings.js',
			'JS_EVENT' => 'OnGoogleMapSettingsEdit',
			'JS_DATA'  => LANGUAGE_ID.'||'.GetMessage('SM_GOOGLE_PARAM_DATA_SET'),
			'DEFAULT'  => serialize( array(
				'LAT'        => 55.7383,
				'LON'        => 37.5946,
				'SCALE'      => 10,
                'PLACEMARKS' => array()
			) ),
			'PARENT'   => 'BASE',
		),
		'MAP_START_PLACE' => array(
			'PARENT' => 'BASE',
            'NAME' => GetMessage('SM_GOOGLE_PARAM_MAP_START_PLACE'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => ''
		),
		'MAP_WIDTH' => array(
			'PARENT' => 'BASE',
            'NAME' => GetMessage('SM_GOOGLE_PARAM_MAP_WIDTH'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => '500'
		),
		'MAP_HEIGHT' => array(
			'PARENT' => 'BASE',
            'NAME' => GetMessage('SM_GOOGLE_PARAM_MAP_HEIGHT'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => '400'
		),
		'MAP_ZOOM' => array(
			'PARENT' => 'BASE',
            'NAME' => GetMessage('SM_GOOGLE_PARAM_MAP_ZOOM'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => '15'
		),
		'MAP_TYPE' => array(
			'PARENT' => 'BASE',
            'NAME' => GetMessage('SM_GOOGLE_PARAM_MAP_TYPE'),
            'TYPE' => 'LIST',
            'MULTIPLE' => 'N',
            'DEFAULT' => 'ROADMAP',
			'ADDITIONAL_VALUES' => 'N',
			'VALUES' => array(
				'ROADMAP'   => GetMessage('SM_GOOGLE_PARAM_MAP_TYPE_ROADMAP'),
				'SATELLITE' => GetMessage('SM_GOOGLE_PARAM_MAP_TYPE_SATELLITE'),
				'HYBRID'    => GetMessage('SM_GOOGLE_PARAM_MAP_TYPE_HYBRID'),
				'TERRAIN'   => GetMessage('SM_GOOGLE_PARAM_MAP_TYPE_TERRAIN')
			),
		),
        // additional settings
        'MAP_CONTROL_ZOOM' => array(
            'NAME' => GetMessage('SM_GOOGLE_PARAM_ENABLE_ZOOM_CONTROL'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'Y',
            'PARENT' => 'ADDITIONAL_SETTINGS'
		),
		'MAP_CONTROL_PAN' => array(
            'NAME' => GetMessage('SM_GOOGLE_PARAM_ENABLE_PAN_CONTROL'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'N',
            'PARENT' => 'ADDITIONAL_SETTINGS'
		),
		'MAP_CONTROL_MAP_TYPE' => array(
            'NAME' => GetMessage('SM_GOOGLE_PARAM_ENABLE_MAP_TYPE_CONTROL'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'Y',
            'PARENT' => 'ADDITIONAL_SETTINGS'
		)
		//'MAP_CONTROL_DBLCLICK_ZOOM' => array(
        //    'NAME' => GetMessage('SM_GOOGLE_PARAM_ENABLE_DBLCLICK_ZOOM'),
        //    'TYPE' => 'CHECKBOX',
        //    'DEFAULT' => 'Y',
        //    'PARENT' => 'ADDITIONAL_SETTINGS'
		//),
		//'MAP_CONTROL_FULLSCREEN_BUTTON' => array(
        //    'NAME' => GetMessage('SM_GOOGLE_PARAM_ENABLE_FULLSCREEN_BUTTON'),
        //    'TYPE' => 'CHECKBOX',
        //    'DEFAULT' => 'Y',
        //    'PARENT' => 'ADDITIONAL_SETTINGS'
		//),
		//'MAP_CONTROL_GEOCLICKER' => array(
        //    'NAME' => GetMessage('SM_GOOGLE_PARAM_ENABLE_GEOCLICKER'),
        //    'TYPE' => 'CHECKBOX',
        //    'DEFAULT' => 'N',
        //    'PARENT' => 'ADDITIONAL_SETTINGS'
		//),
		//'MAP_CONTROL_RIGHTBUTTON_MAGNIFIER' => array(
        //    'NAME' => GetMessage('SM_GOOGLE_PARAM_ENABLE_RIGHTBUTTON_MAGNIFIER'),
        //    'TYPE' => 'CHECKBOX',
        //    'DEFAULT' => 'N',
        //    'PARENT' => 'ADDITIONAL_SETTINGS'
		//)
    ),
);

CIBlockParameters::AddPagerSettings( $arComponentParameters, GetMessage("SM_GOOGLE_DESC_PAGER_NEWS"), true, true );
?>
