<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<script type="text/javascript">
    // Google maps JavaScripts
    var mapg_<?=$arResult['MAP_ID']?> = null; // Google map object
	var mapg_<?=$arResult['MAP_ID']?>_start_city = '<?=CUtil::JSEscape($arResult['MAP_START_PLACE'])?>';
    //
    // page load handler:
    BX.ready( function() {
        var map_center = new google.maps.LatLng( <?=$arResult['CENTER_POINT_LAT']?>, <?=$arResult['CENTER_POINT_LON']?> );
        var map_options = {
            scrollwheel:        false,
			zoom:               <?=$arResult['MAP_ZOOM']?>,
			center:             map_center,
			mapTypeId:          google.maps.MapTypeId.<?=$arResult['MAP_TYPE']?>,
			disableDefaultUI:   true,
			zoomControl:        <?php echo ($arResult['MAP_CONTROL_ZOOM'] == 'Y' ? 'true' : 'false'); ?>,
			scaleControl:       <?php echo ($arResult['MAP_CONTROL_ZOOM'] == 'Y' ? 'true' : 'false'); ?>,
			panControl:         <?php echo ($arResult['MAP_CONTROL_PAN'] == 'Y' ? 'true' : 'false'); ?>,
			mapTypeControl:     <?php echo ($arResult['MAP_CONTROL_MAP_TYPE'] == 'Y' ? 'true' : 'false'); ?>,
			streetViewControl:  false,
			overviewMapControl: false
		}; // map options
		var divka = document.getElementById( 'map_canvas_<?=$arResult['MAP_ID']?>' );
		mapg_<?=$arResult['MAP_ID']?> = new google.maps.Map( divka, map_options );
		var ss = mapg_<?=$arResult['MAP_ID']?>_start_city;
		if( ss != '' )
		{
			//alert( 'Will search start point - ' + ss );
			var geocoder = new google.maps.Geocoder();
			geocoder.geocode( { address: ss }, function( obResult, status ) {
				if( status != google.maps.GeocoderStatus.OK && status != google.maps.GeocoderStatus.ZERO_RESULTS )
				{
					alert( 'Search error: ' + status );
					return;
				}
				if( !obResult )
				{
					alert( 'Null search results!' );
					return;
				}
				var len = obResult.length;
				if( len >= 1 )
				{
					var position = obResult[0].geometry.location;
					var text = '';
					//var first_address_type = obResult[0].address_components[j].types[0];
					//var types = '';
					//for( var j=0; j<obResult[0].types.length; j++ )
					//	types += print_r( obResult[0].types[j] );
					//alert( 'Top types: ' + types );
					for( var j=0; j<obResult[0].address_components.length; j++ ) {
						text += (text.length > 0 ? ', ' : '') + obResult[0].address_components[j].long_name;
						//types = '';
						//for( var jj=0; jj<obResult[0].address_components[j].types.length; jj++ )
						//	types += print_r( obResult[0].address_components[j].types[jj] );
						//alert( 'Address component types: ' + types );
					}
					var isCity = has_type_city( obResult[0].types );
					//alert( 'Found: ' + text + (isCity ? ' (City)' : '') );
					mapg_<?=$arResult['MAP_ID']?>.panTo( obResult[0].geometry.location );
					if( isCity )
						mapg_<?=$arResult['MAP_ID']?>.setZoom( 10 );
					var marker = new google.maps.Marker( {
						map: mapg_<?=$arResult['MAP_ID']?>,
						position: obResult[0].geometry.location
					} );
				}
				//_this.ar_results[i] = {
				//	name: text,
				//	lat: position.lat(),
				//	lon: position.lng()
				//};
			} );
		}
    } );
	
	function has_type_city( types )
	{
		var ret = false;
		if( !types ) return false;
		for( var i=0; i<types.length; i++ )
		{
			if( types[i] == 'locality' )
				ret = true;
		}
		return ret;
	}
</script>


<!-- 2GIS map -->
<div id="map_canvas_<?=$arResult['MAP_ID']?>"
     style="width: <?=$arResult['MAP_WIDTH']?>px; height: <?=$arResult['MAP_HEIGHT']?>px">
    <?php echo GetMessage('SM_GOOGLE_MAP_LOADING'); ?>
</div>

