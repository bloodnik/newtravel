<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="samo_container" id="search_tour" samo:uid="<?=$arParams['SAMO_UID']?>" samo:datasrc="<?=$arParams['SAMO_PROXY']?>?" samo:datatrg="<?=$arParams['SAMO_DATATRG']?>?">
<div id="resultset">
	<?if(isset($arResult['PRICES'][2]) && !empty($arResult['PRICES'][2])):?>
	<table class="res">
	<thead><tr>
		<th>Вылет</th>
		<th>Отель (Курорт)</th>
		<th>Номер/Размещение</th>
		<th>Питание</th>
		<th>Ночей</th>
		<th>Цена</th>
		<th>Веб-цена</th>
		<?if($USER->isAdmin()):?><th>Туроператор<br><small>(только для админ.)</small></th><?endif?>
		<th>Эконом</th>
		<th>Бизнес</th>
		<th></th>
	</tr></thead>
	<tbody>
	<?foreach(array_slice($arResult['PRICES'][2], 0, 5) as $arItem):?>
	<tr<?if($arItem[15]):?> id="red_row"<?endif?>>
		<td><?=$arItem[1]?></td>
		<td class="hotel"><?if($arItem["HOTEL_URL"]):?><a href="<?=$arItem["HOTEL_URL"]?>" target="_blank"><?endif?><?=$arItem[4]?><?if($arItem["HOTEL_URL"]):?></a><?endif?> (<?=$arItem[3]?>)</td>
		<td><?=$arItem[6]?> / <?=$arItem[7]?></td>
		<td><?=$arItem[5]?></td>
		<td><?=$arItem[2]?></td>
		<td class="cost"><span class="tour-operator" style="display:none;"><?=$arItem[10]?></span><span class="claiminc" style="display:none;"><?=$arItem[0]?></span><span class="bron" samo:id="<?=$arItem[0]?>"><?=CurrencyFormat($arItem[8], $arItem[9])?></span></td>
		<td class="cost-discount"><?=CurrencyFormat($arItem[17], $arItem[9]);?><span class="cost-discount-usd" style="display:none;"><?=$arItem[18]?></span></td>
		<?if($USER->isAdmin()):?><td><?=$arItem[10]?><?endif?>
		<td><span class="fr_place_r p<?=$arItem[11]?>">&nbsp;</span><span class="fr_place_l p<?=$arItem[12]?>">&nbsp;</span></td>
		<td><span class="fr_place_r p<?=$arItem[13]?>">&nbsp;</span><span class="fr_place_l p<?=$arItem[14]?>">&nbsp;</span></td>
		<td><a class="add_to_basket" href="#">В&nbsp;корзину</a></td>
	</tr>
	<?endforeach;?>
	</tbody>
	</table>
	<?else:?>
		<div align="center">
			<h4>По указанным критериям ничего не найдено</h4>
			<a href="/upload/instruksiya.png" class="nm">Схема инструкция поиска туров</a>
		</div>
	<?endif?>
</div><!-- resultset -->
</div><!-- samo_container -->
<div id="samo_checkin_beg_data" style="display:none;"><?=json_encode($arResult['CHECKIN_BEG'])?></div>
<div id="samo_checkin_end_data" style="display:none;"><?=json_encode($arResult['CHECKIN_END'])?></div>