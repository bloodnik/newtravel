var samo = {
	el : {},
	preloader : null,
	httpcache : {},
	cachelifetime : 900000
};
function error_msg(msg) {
	window.status = msg;
}
function I(s) {
	if(window.console) {
		console.log(s);
	}
}
$.jsonLoad = function($get_params, $callback) {
	$get_params.uid = samo.uid;
	var url = samo.datasource + $.param($get_params);
	var now = +(new Date());
	var callback = function(result) {
		if (result.error) {
			error_msg(result.error);
		} else {
			samo.httpcache[url] = {json:result, datetime:now};
			$callback(result);
		}
	};
	if (typeof samo.httpcache[url] !== 'undefined' && samo.httpcache[url].datetime + samo.cachelifetime > now) {
		$callback(samo.httpcache[url].json);
	} else {
		$.ajax({url:url, success:callback});
	}
};
Date.dayNames = [ 'Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота' ];
Date.abbrDayNames = [ 'Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб' ];
Date.monthNames = [ 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ];
Date.abbrMonthNames = [ 'Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг',	'Сен', 'Окт', 'Ноя', 'Дек' ];
$(document).ready(function() {
	$('#samo_container_search').append('<div id="preloader">Загрузка данных</div>');
	$('#preloader').bind('ajaxStart', function() {
		$(this).css('display', 'block');
	}).bind('ajaxStop', function() {
		$(this).css('display', 'none');
	});
	$.ajaxSetup({
		error : function(xhr, status, e) {
			if ('timeout' == status) {
				error_msg('Timeout error, try again later...');
			}
		},
		timeout : 60000,
		scriptCharset : 'utf-8',
		dataType : 'jsonp'
	});
	(function() {
		var $search = $('#search_tour');
		samo.result_tpl = $('#result-template').html();
		samo.notfound_tpl = $('#notfound-template').html();
		samo.uid = $search.attr('samo:uid');
		samo.datasource = $search.attr('samo:datasrc');
		samo.bronurl = $search.attr('samo:datatrg');
		//$.event.trigger('ajaxStart');
		//$('select,input,button').attr('autocomplete', 'off');
		$('#TOWNFROMINC,#STATEINC,#CHECKIN_BEG,#CHECKIN_END,#NIGHTS_FROM,#NIGHTS_TILL,#ADULT,#CHILD,#CURRENCYINC,#TOWNTO,#STARS,#HOTELS,#MEAL').each(function() {
			samo.el[this.id] = $(this);
		});
		var temp = [];
		$("#HOTELS input").each(function() {
			var item = [
				parseInt($(this).val()),
				$(this).attr("hotel"), 
				parseInt($(this).attr("town")),
				$(this).attr("stars"), 
				parseInt($(this).attr("stars")), 
				false
			];
			item.ref = $(this).parent().get(0);
			item.title = ' ' + $(this).attr("hotel").toLowerCase().replace(/[^a-z0-9]/, ' ');
			item.town = parseInt($(this).attr("town"));
			item.gs = parseInt($(this).attr("stars"));
			temp.push(item);
		});
		$.data(samo.el.HOTELS.get(0), 'items', temp);
		$.data(samo.el.HOTELS.get(0), 'any', $('#' + samo.el.HOTELS.get(0).id + '_ANY'));
		temp = [];
		$("#TOWNTO input").each(function() {temp.push([parseInt($(this).val()), $(this).parent().text(), false]);});
		$.data($("#TOWNTO").get(0), 'items', temp);
		temp = [];
		$("#STARS input").each(function() {temp.push([parseInt($(this).val()), $(this).parent().text(), false]);});
		$.data($("#STARS").get(0), 'items', temp);
		temp = [];
		$("#MEAL input").each(function() {temp.push([parseInt($(this).val()), $(this).parent().text(), false]);});
		$.data($("#MEAL").get(0), 'items', temp);
		//$.event.trigger('ajaxStop');
	})();
	samo.repaint_form = function(data) {
		for (control in data) {
			values = data[control];
			switch (control) {
			case 'TOWNFROM':
			case 'STATE':
			case 'CURRENCY':
				samo.eselect(samo.el[control + 'INC'], values);
				break;
			case 'CHECKIN_BEG':
			case 'CHECKIN_END':
				samo.datepicker(samo.el[control], values);
				break;
			case 'TOWNTO':
			case 'STARS':
			case 'MEAL':
				samo.cb(samo.el[control], values);
				break;
			case 'HOTELS':
				samo.hotels(samo.el.HOTELS, values);
				break;
			case 'NIGHTS_TILL':
			case 'NIGHTS_FROM':
			case 'ADULT':
			case 'CHILD':
				samo.el[control].val(values);
				break;
			case 'PRICES':
				samo.prices(values);
				break;
			default:
				break;
			}
		}
		samo.hotels_filter();
	};
	samo.cb = function(name, items) {
		if (items === false) {
			items = [];
		}
		var result = [], checked = '', item = [], _checked = 0;
		var i, length = items.length;
		for (i = 0; i < length; i++) {
			item = items[i];
			checked = (item.length >= 3 && item[2]) ? 'checked="checked"' : '';
			result.push('<label><input type="checkbox" value="' + item[0] + '" ' + checked + '>' + item[1] + '</label>');
			if (checked) {
				_checked++;
			}
		}
		name.html(result.join(''));
		var self = name.get(0);
		$.data(self, 'items', items);
		$.data(self, 'any', $('#' + self.id + '_ANY'));
		$($.data(self, 'any')).attr('checked', !_checked);
	};
	samo.hotels = function(name, items) {
		if (items == false) {
			items = [];
		}
		var result = $('<div/>'), checked = '', _checked = 0;
		$.each(items, function(idx, item) {
			var name = item[1] + ' ' + item[3] + ((parseInt(item[3]) == item[3]) ? '*' : '');
			var checked = (item.length >= 6 && item[5]) ? 'checked="checked"' : '';
			input = $('<label><input type="checkbox" value="' + item[0] + '" ' + checked + '>' + name + '</label>');
			items[idx].ref = input.get(0);
			items[idx].title = ' ' + item[1].toLowerCase().replace(/[^a-z0-9]/, ' ');
			items[idx].town = item[2];
			items[idx].gs = item[4];
			result.append(input);
			if (checked) {
				_checked++;
			}
		});
		name.empty().append(result.children());
		var self = name.get(0);
		$.data(self, 'items', items);
		$.data(self, 'any', $('#' + self.id + '_ANY'));
		$($.data(self, 'any')).attr('checked', !_checked);
	};
	samo.datepicker = function(name, data) {
		if (data == false) {
			name.dpSetValidDates('0');
		} else {
			name.dpSetValidDates(data[2]).dpSetStartDate(data[1]).val(data[0]).dpSetSelected(data[0]);
		}
	};
	samo.eselect = function(name, items) {
		if (items == false) {
			items = [];
		}
		var add = function(el, v, t, sO) {
			var option = document.createElement("option");
			option.value = v, option.text = t;
			var o = el.options;
			var oL = o.length;
			if (!el.cache) {
				el.cache = {};
				for ( var i = 0; i < oL; i++) {
					el.cache[o[i].value] = i;
				}
			}
			if (typeof el.cache[v] == "undefined") {
				el.cache[v] = oL;
			}
			el.options[el.cache[v]] = option;
			if (sO) {
				option.selected = true;
			}
		};
		var select = name.get(0);
		name.find('option').remove().end().get(0).cache = null;
		for ( var i = 0, length = items.length; i < length; i++) {
			var item = items[i]
			var sel = item[2] || false;
			add(select, item[0], item[1], sel);
		}
	};
	/*(function() {
		var params = {
			'action' : 'init'
		};
		if (location.search.length) {
			$(location.search.substring(1).split('&')).each(function() {
				var tmp = this.split('=');
				if (tmp.length == 2) {
					params[tmp[0]] = tmp[1];
				}
			});
			if (params.DOLOAD) {
				$([ 'FREIGHT', 'STOPSALE' ]).each(function() {
					var $cb = $('#' + this);
					if (typeof params[this] !== 'undefined') {
						$cb.attr('checked', params[this] == 1);
					} else {
						params[this] = Number($cb.is(':checked'));
					}
				});
				$([ 'COSTMIN', 'COSTMAX' ]).each(function() {
					if (typeof params[this] !== 'undefined') {
						$('#' + this).val(params[this]);
					}
				});
			}
		}
		$.jsonLoad(params, samo.repaint_form);
	})();*/
	samo.el.CHECKIN_BEG.datePicker().mask('39.19.2099');
	samo.datepicker(samo.el['CHECKIN_BEG'], $.parseJSON($('#samo_checkin_beg_data').text()));
	samo.el.CHECKIN_END.datePicker().mask('39.19.2099');
	samo.datepicker(samo.el['CHECKIN_END'], $.parseJSON($('#samo_checkin_end_data').text()));
	$('#TOWNFROMINC,#STATEINC').bind('change', function() {
		var params = {
			action : this.id.toLowerCase()
		};
		params.TOWNFROMINC = $('#TOWNFROMINC').val();
		params.STATEINC = $('#STATEINC').val();
		$.jsonLoad(params, samo.repaint_form);
	});
	samo.el.CHECKIN_BEG.bind('dpClosed', function(e, selectedDates) {
		var d = selectedDates[0];
		if (d) {
			var newDate = d.asString();
			samo.el.CHECKIN_END.dpSetSelected(newDate)
					.val(newDate);
		}
	}).bind('blur',	function() {
		var newDate = this.value;
		samo.el.CHECKIN_END.dpSetSelected(newDate).val(newDate);
	});
	samo.el.NIGHTS_FROM.bind('change', function() {
		samo.el.NIGHTS_TILL.val(this.value);
	});
	samo.el.NIGHTS_TILL.bind('change', function() {
		if (parseInt(this.value) < parseInt(samo.el.NIGHTS_FROM.val())) {
			samo.el.NIGHTS_FROM.val(this.value);
		}
	});
	samo.el.CHILD.bind('change', function() {
		if (this.value > 0) {
			$('#child_ages').css('visibility', 'visible');
			for ( var $i = 1; $i <= 3; $i++) {
				if ($i <= this.value) {
					$('#age_' + $i).css('visibility', 'visible');
				} else {
					$('#age_' + $i).css('visibility', 'hidden').val('');
				}
			}
		} else {
			$('#child_ages').css('visibility', 'hidden').find('.age').css('visibility', 'hidden').val('');
		}
	});
	$('#TOWNTO,#STARS,#MEAL,#HOTELS').delegate('input',	'click', function() {
		$('#' + $(this).parents('div:first').attr('id') + '_ANY').attr('checked', !this.checked);
	});
	$('#TOWNTO_ANY,#STARS_ANY,#HOTELS_ANY,#MEAL_ANY').bind('click',	function() {
		if (this.checked) {
			$('#' + this.id.replace('_ANY', '') + ' input:checked').removeAttr('checked');
		}
	});
	$('#hotelsearch').bind('keyup', function(e) {
		if (e.keyCode == 27) {
			$.event.trigger('clearSearch'); 
			e.stopPropagation();
			return;
		}
		$('#HOTELS_SEL').attr('checked', false);
		samo.hotels_filter();
	}).bind('clearSearch', function() {
		this.value = '';
		samo.hotels_filter();
	});
	samo.reset_hotels_filter = function() {
		$('#TOWNTO_ANY,#STARS_ANY').each(function() {
			this.checked = true;
			var name = this.name.replace(/_ANY/, '');
			$('#' + name + ' :checked').attr('checked', false);
		});
		$('#hotelsearch').val('');
	};
	samo.hotels_show = function(show_all) {
		show_all = show_all || false;
		if (show_all) {
			samo.hotels_filter();
		} else {
			samo.reset_hotels_filter();
			$('#HOTELS input').not(':checked').parent().css('display', 'none');
			$('#HOTELS input:checked').parent().css('display', 'block');
		}
	};
	$('#HOTELS_SEL').bind('click', function() {
		samo.hotels_show(!this.checked);
	});
	samo.hotels_filter = function() {
		var $grps = [];
		var $twns = [];
		var $all_stars = false;
		var $all_towns = false;
		$('#HOTELS_SEL').attr('checked', false);
		var townto_any = $('#TOWNTO_ANY').get(0);
		var grpstar_any = $('#STARS_ANY').get(0);
		var hotels = $.data(samo.el.HOTELS.get(0), 'items');
		$('#TOWNTO input:checked').each(function() {
			$twns.push(parseInt(this.value));
		});
		$all_towns = townto_any.checked = !$twns.length;
		$('#STARS input:checked').each(function() {
			$grps.push(parseInt(this.value));
		});
		$all_stars = grpstar_any.checked = !$grps.length;
		var $filter = $.trim($('#hotelsearch').val()).toLowerCase();
		var $strfilter = $filter.length;
		var hide = [];
		var show = [];
		$.each(hotels, function(idx, $hotel) {
			var found = ($all_towns || $.inArray($hotel.town, $twns) !== -1);
			if (found && !$all_stars) {
				found = $.inArray($hotel.gs, $grps) !== -1;
			}
			if (found && $strfilter) {
				found = $hotel.title.indexOf($filter) > 0;
			}
			$hotel.ref.style.display = (found) ? 'block' : 'none';
		});
	};
	$('#STARS,#TOWNTO').delegate('input', 'click', samo.hotels_filter);
	$('#STARS_ANY,#TOWNTO_ANY').bind('click', samo.hotels_filter);
	samo.add_to_basket = function() {
		td = $(this).parents('tr').children();
		$.post(
			samo.bronurl,
			{
				TOWNFROMINC:$('#TOWNFROMINC :selected').text(),
				STATEINC:$('#STATEINC :selected').text(),
				CHECKIN:td.eq(0).text(),
				HOTEL:td.eq(1).text(),
				ZONE:td.eq(1).text().replace(/^.*\(/, '').replace(')', ''),
				ROOM:td.eq(2).text().replace(/ \/.*$/, ''),
				HTPLACE:td.eq(2).text().replace(/^.*\/ /, ''),
				NIGHTS:td.eq(4).text(),
				MEAL:td.eq(3).text(),
				CLAIMINC:td.eq(5).find('.claiminc').text(),
				PRICE:td.eq(6).find('.cost-discount-usd').text(),
				TOUR_OPERATOR:td.eq(5).find('.tour-operator').text()
			},
			function (data) {
				$.event.trigger('ajaxStop');
				var sidebar_product_count = $('#sidebar_product_count');
				sidebar_product_count.text(parseInt(sidebar_product_count.text()) + 1);
				alert(data.message);
			},
			"jsonp"
		);
		return false;
	};
	/*samo.add_to_basket = function() {
		td = $(this).parents('tr').children();
		$.post(
			samo.bronurl,
			{CLAIMINC:td.eq(5).children().attr('samo:id'),},
			function (data) {
				$.event.trigger('ajaxStop');
				var sidebar_product_count = $('#sidebar_product_count');
				sidebar_product_count.text(parseInt(sidebar_product_count.text()) + 1);
				alert(data.message);
			},
			"jsonp"
		);
		return false;
	};*/
	$('#resultset').delegate('.add_to_basket', 'click', samo.add_to_basket);
	//$('#resultset').delegate('.page', 'click', function() {samo.search($(this).text());});
	samo.prices = function(data) {
		var pages = data[1], page = data[0];
		data = data[2];
		var $result = '';
		if (data.length) {
			var result = document.createElement('div')
			$(result).html(samo.result_tpl);
			var template = $('#eachrow', result).removeAttr('id');
			var container = template.parent();
			$.each(data, function(idx, e) {
				var row = template.clone();
				var tmp = row.html();
				tmp = tmp.replace('{%checkin%}', e[1])
					.replace('{%hotel%}', e[4])
					.replace('{%town%}', e[3])
					.replace('{%nights%}', e[2])
					.replace('{%meal%}', e[5])
					.replace('{%room%}', e[6])
					.replace('{%htplace%}', e[7])
					.replace('{%price%}', e[17])
					.replace('{%price_discount%}', e[8])
					.replace('{%currency%}', e[9])
					.replace('{%touroperator%}', e[10])
					.replace('{%econom%}', '<span class="fr_place_r p' + e[11] + '">&nbsp;</span><span class="fr_place_l p' + e[12] + '">&nbsp;</span>')
					.replace('{%business%}', '<span class="fr_place_r p' + e[13] + '">&nbsp;</span><span class="fr_place_l p' + e[14] + '">&nbsp;</span>');
				row.html(tmp);
				if (e[15]) {
					row.addClass('red_row');
				}
				if (e[0]) {
					var $price = $('.cost', row);
					$price.replaceWith('<td><span class="bron" samo:id="' + e[0] + '">' + $price.text() + '</span></td>');
				}
				container.append(row);
			});
			template.remove();
			if (pages.length > 1) {
				var pager = '<div id="pager">';
				$.each(pages,
					function(idx, i) {
						pager += '<span class="' + ((i == page) ? 'current' : 'page') + '">' + i + '</span>&nbsp;';
					});
				pager += '<div>';
				$(result).append(pager);
			}
			$result = result.innerHTML;
		} else {
			$result = samo.notfound_tpl;
		}
		$('#resultset').html($result);
	};
	samo.search = function() {
		var page = arguments.length ? arguments[0] : 1;
		var params = {
			//action : 'prices',
			PAGE : page
		};
		$('#TOWNFROMINC,#STATEINC,#NIGHTS_TILL,#NIGHTS_FROM,#ADULT,#CHILD,#CURRENCYINC,#COSTMIN,#COSTMAX').each(function() {
			params[this.id] = this.value;
		});
		$('#CHECKIN_BEG,#CHECKIN_END').each(function() {
			var tmp = this.value.split('.');
			params[this.id] = tmp[2] + tmp[1] + tmp[0];
		});
		$.each([ 'HOTELS', 'MEAL', 'TOWNTO', 'STARS' ],	function() {
			if ((this == 'TOWNTO' || this == 'STARS') && typeof params.HOTELS != 'undefined') {
				return;
			}
			var tmp = [];
			$('#' + this + ' input:checked').each(function() {
				tmp.push(this.value);
			});
			if (tmp.length) {
				params[this] = tmp.join(',');
			}
			$('#FREIGHT:checked,#STOPSALE:checked').each(function() {
				params[this.id] = 1;
			});
		});
		/*$.jsonLoad(params, function(data) {
			samo.prices(data);
		});*/
		var query = '/tours/search/?DOLOAD=1&';
		for(key in params) {
			query += key+'='+escape(params[key])+'&';
		}
		query = query.slice(0, query.length - 1);
		window.location.href = query
	};
	$('#load').bind('click', function() {
		samo.search();
	}).bind('ajaxStart', function() {
		this.disabled = true;
	}).bind('ajaxStop', function() {
		this.disabled = false;
	}).removeAttr('disabled');
});