<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?//I($arResult)?>
<div class="samo_container" id="search_tour" samo:uid="<?=$arParams['SAMO_UID']?>" samo:datasrc="<?=$arParams['SAMO_PROXY']?>?" samo:datatrg="<?=$arParams['SAMO_DATATRG']?>?">
<div id="samo_container_search">
	<table class="std container">
		<tr>
			<td colspan="2" class="panel">
				<table>
					<tr>
						<td width="5%" rowspan="2" class="npp">1</td>
						<td width="20%">Город отправления</td>
						<td width="32%">
							<select name="TOWNFROMINC" id="TOWNFROMINC" autocomplete="off">
							<?foreach($arResult['TOWNFROM'] as $arItem):?>
								<option value="<?=$arItem[0]?>"<?=($arItem[2]==1?' selected="selected"':'')?>><?=$arItem[1]?></option>
							<?endforeach;?>
							</select>
						</td>
						<td>Страна назначения</td>
						<td>
							<select name="STATEINC" id="STATEINC" autocomplete="off" >
							<?foreach($arResult['STATE'] as $arItem):?>
								<option value="<?=$arItem[0]?>"<?=($arItem[2]==1?' selected="selected"':'')?>><?=$arItem[1]?></option>
							<?endforeach;?>
							</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="50%" class="panel n2">
				<table>
					<tr>
						<td rowspan="2" class="npp">2</td>
						<td width="30%">Вылет от</td>
						<td><input type="text" name="CHECKIN_BEG" class="date" id="CHECKIN_BEG" autocomplete="off" value="<?=$arResult['CHECKIN_BEG'][0]?>" /></td>
						<td width="30%">Ночей от</td>
						<td>
							<select name="NIGHTS_FROM" id="NIGHTS_FROM" class="spin" autocomplete="off">
							<?foreach(range(1, 29) as $i):?>
								<option value="<?=$i?>"<?=($i==$arResult['NIGHTS_FROM']?' selected="selected"':'')?>><?=$i?></option>
							<?endforeach;?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Вылет до</td>
						<td><input type="text" class="date" id="CHECKIN_END" autocomplete="off" value="<?=$arResult['CHECKIN_END'][0]?>" /></td>
						<td style="padding-bottom: 5px;">Ночей до</td>
						<td>
							<select name="NIGHTS_TILL" id="NIGHTS_TILL" class="spin" autocomplete="off" >
							<?foreach(range(1, 29) as $i):?>
								<option value="<?=$i?>"<?=($i==$arResult['NIGHTS_TILL']?' selected="selected"':'')?>><?=$i?></option>
							<?endforeach;?>
							</select>
						</td>
					</tr>
				</table>
			</td>
			<td class="panel n2">
				<table>
					<tr>
						<td width="5%" rowspan="2" class="npp">3</td>
						<td width="20%">Взрослых</td>
						<td width="15%">
							<select name="ADULT" class="spin" id="ADULT" autocomplete="off">
							<?foreach(range(1, 4) as $i):?>
								<option value="<?=$i?>"<?=($i==$arResult['ADULT']?' selected="selected"':'')?>><?=$i?></option>
							<?endforeach;?>
							</select>
						</td>
						<td colspan="2" width="60%" style="padding-bottom: 5px;">
							<span class="currency_label">Цена</span>
							<select id="CURRENCYINC" name="CURRENCY" autocomplete="off" >
							<?foreach($arResult['CURRENCY'] as $arItem):?>
								<?if(in_array($arItem[0], $arParams["ALLOWED_CURRENCIES"])):?>
								<option value="<?=$arItem[0]?>"<?=($arItem[2]==1?' selected="selected"':'')?>><?=$arItem[1]?></option>
								<?endif?>
							<?endforeach;?>
							</select>
							 от <input type="text" id="COSTMIN" name="COSTMAX" value="" class="price" autocomplete="off" size="4">
							 до <input type="text" id="COSTMAX" name="COSTMAX" autocomplete="off" value="" class="price" size="4">
						</td>
					</tr>
					<tr>
						<td>Детей</td>
						<td>
							<select name="CHILD" class="spin" id="CHILD" autocomplete="off" >
							<?foreach(range(0, 2) as $i):?>
								<option value="<?=$i?>"<?=($i==$arResult['CHILD']?' selected="selected"':'')?>><?=$i?></option>
							<?endforeach;?>
							</select>
						</td>
						<td id="child_ages">
							<input type="text" maxlength="2" class="age" id="age_1" autocomplete="off" name="AGE1">
							<input type="text" maxlength="2" class="age" id="age_2" autocomplete="off" name="AGE2">
							<input maxlength="2" type="text" class="age" id="age_3" autocomplete="off" name="AGE3">
						</td>
						<td></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="panel n4">
				<div class="div_hotel_param" id="HOTELSCONTAINER">
					<table style="clear: both;">
						<thead>
							<tr>
								<td class="l tdl">Город</td>
								<td><label><input type="checkbox" name="TOWNTO_ANY" id="TOWNTO_ANY"<?=(!isset($_GET["TOWNTO"])?' checked="checked"':'')?>>любой</label></td>
								<td class="l tdl">Категория</td>
								<td><label><input type="checkbox" name="STARS_ANY" id="STARS_ANY"<?=(!isset($_GET["STARS"])?' checked="checked"':'')?>>любая</label></td>
								<td class="l tdl">Гостиница <input type="text" name="hotelsearch" autocomplete="off" id="hotelsearch"> <label><input type="checkbox" name="HOTELS_SEL" id="HOTELS_SEL" autocomplete="off"> выбранные</label></td>
								<td><label><input type="checkbox" name="HOTELS_ANY" id="HOTELS_ANY"<?=(!isset($_GET["HOTELS"])?' checked="checked"':'')?>>любая</label></td>
								<td class="l tdl">Питание</td>
								<td class="tdl"><label><input type="checkbox" name="MEAL_ANY" id="MEAL_ANY"<?=(!isset($_GET["MEAL"])?' checked="checked"':'')?>>любое</label></td>
							</tr>
						</thead>
						<tr>
							<td width="25%" colspan="2">
								<div class="checklistbox" id="TOWNTO" name="TOWNTO">
								<?foreach($arResult['TOWNTO'] as $arItem):?>
									<label><input type="checkbox" value="<?=$arItem[0]?>"<?=($arItem[2]==1?' checked="checked"':'')?>><?=$arItem[1]?></label>
								<?endforeach;?>
								</div>
							</td>
							<td width="10%" colspan="2">
								<div class="checklistbox" id="STARS" name="STARS">
								<?foreach($arResult['STARS'] as $arItem):?>
									<label><input type="checkbox" value="<?=$arItem[0]?>"<?=($arItem[2]==1?' checked="checked"':'')?>><?=$arItem[1]?></label>
								<?endforeach;?>
								</div>
							</td>
							<td width="50%" colspan="2">
								<div class="checklistbox" id="HOTELS" name="HOTELS">
								<?foreach($arResult['HOTELS'] as $arItem):?>
									<label style="display:block;"><input type="checkbox" value="<?=$arItem[0]?>"<?=(in_array($arItem[0], explode(",", $_GET["HOTELS"]))?' checked="checked"':'')?> town="<?=$arItem[2]?>" hotel="<?=$arItem[1]?>" stars="<?=$arItem[3]?>"><?=$arItem[1]?> <?=$arItem[3]?>*</label>
								<?endforeach;?>
								</div>

							</td>
							<td width="15%" colspan="2" class="tdl">
								<div class="checklistbox" id="MEAL" name="MEAL">
								<?foreach($arResult['MEAL'] as $arItem):?>
									<label><input type="checkbox" value="<?=$arItem[0]?>"<?=(strpos($arResult['MEAL_SELECTED'], strval($arItem[0])) === false?'':' checked="checked"')?>><?=$arItem[1]?></label>
								<?endforeach;?>
								<div>
							</td>
						</tr>
					</table>
				</div>
		<table>
			<tr>
			 <td style="border: none"><label><input type="checkbox" id="FREIGHT" name="FREIGHT" autocomplete="off"<?if(isset($_GET["FREIGHT"])):?> checked="checked"<?endif;?>/> есть места на рейсы </label></td>
			 <td style="border: none"><label><input type="checkbox" id="STOPSALE" name="STOPSALE" autocomplete="off"<?if(isset($_GET["STOPSALE"])):?> checked="checked"<?endif;?>/> нет остановки продаж</label></td>
			 <td width="30%" class="r"><button id="load" disabled="disabled">Найти</button></td>
			</tr>
		</table>
			</td>
		</tr>
	</table>
	<table class="price_legend">
		<tr>
			<td width="63px"><img src="public/f_right.gif"></td>
			<td> - места на рейсы (есть/нет/по запросу)</td>
		<tr>
			<td class="confirm_now"></td>
			<td> - мгновенное подтверждение</td>
		</tr>
		<tr>
			<td class="stopsale"></td>
			<td> - остановка продаж</td>
		</tr>
	</table>
</div><!-- samo_container_search -->
<div id="resultset">
	<?if(isset($arResult['PRICES'][2]) && !empty($arResult['PRICES'][2])):?>
	<div align="center">
		<small>* Цена указана за номер.</small><br> <!--<a style="color:red;" href="/faq/3099/"><b>ОБРАТИТЕ&nbsp;ВНИМАНИЕ&nbsp;НА&nbsp;ДОПЛАТЫ</b></a>-->
	</div>
	<table class="res">
	<thead><tr>
		<th>Вылет</th>
		<th>Отель (Курорт)</th>
		<th>Номер/Размещение</th>
		<th>Питание</th>
		<th>Ночей</th>
		<th>Цена</th>
		<th>Веб-цена</th>
		<?if($USER->isAdmin()):?><th>Туроператор<br><small>(только для админ.)</small></th><?endif?>
		<th>Эконом</th>
		<th>Бизнес</th>
		<th></th>
	</tr></thead>
	<tbody>
	<?foreach($arResult['PRICES'][2] as $arItem):?>
	<tr<?if($arItem[15]):?> id="red_row"<?endif?>>
		<td><?=$arItem[1]?></td>
		<td class="hotel"><?if($arItem["HOTEL_URL"]):?><a href="<?=$arItem["HOTEL_URL"]?>" target="_blank"><?endif?><?=$arItem[4]?> (<?=$arItem[3]?>)<?if($arItem["HOTEL_URL"]):?></a><?endif?></td>
		<td><?=$arItem[6]?> / <span class="htplace"><?=$arItem[7]?></span></td>
		<td><?=$arItem[5]?></td>
		<td><?=$arItem[2]?></td>
		<td class="cost"><span class="tour-operator" style="display:none;"><?=$arItem[10]?></span><span class="claiminc" style="display:none;"><?=$arItem[0]?></span><span class="bron" samo:id="<?=$arItem[0]?>"><?=CurrencyFormat($arItem[8], $arItem[9])?></span></td>
		<td class="cost-discount"><?=CurrencyFormat($arItem[17], $arItem[9]);?><span class="cost-discount-usd" style="display:none;"><?=$arItem[18]?></span></td>
		<?if($USER->isAdmin()):?><td><?=$arItem[10]?><?endif?>
		<td><span class="fr_place_r p<?=$arItem[11]?>">&nbsp;</span><span class="fr_place_l p<?=$arItem[12]?>">&nbsp;</span></td>
		<td><span class="fr_place_r p<?=$arItem[13]?>">&nbsp;</span><span class="fr_place_l p<?=$arItem[14]?>">&nbsp;</span></td>
		<td><a class="add_to_basket" href="#">В&nbsp;корзину</a></td>
	</tr>
	<?endforeach;?>
	</tbody>
	</table>
	<div id="pager">
	<?foreach($arResult['PRICES'][1] as $arItem):?>
		<span class="<?=(($arResult['PRICES'][0] == $arItem)?'current':'page')?>">
			<a href="<?=CMain::GetCurPageParam('PAGE='.$arItem,array('action', 'page'))?>"><?=$arItem?></a>
		</span>
	<?endforeach;?>
	</div><br>
	<?else:?>
		<div align="center">
			<h4>По указанным критериям ничего не найдено</h4>
			
			<h4><a href="/upload/instruksiya.png" class="nm">Схема инструкции поиска туров</a></h4><br>
			<img src="/upload/instruksiya.png" alt="Схема инструкция поиска туров" ><br>
		</div>
	<?endif?>
	<?if($USER->isAdmin()):?>
	<p align="center">
		<small><b>Время запроса к серверу samo.ru:</b>
		<?=$arResult["SAMO_SEARCH_REQUEST_TIME"]?> сек. (только для админ.)</small>
	</p>
	<?endif;?>
<?/*	<script type="text/html" id="result-template">
		<table class="res">
		<thead><tr>
			<th>Вылет</th>
			<th>Гостиница</th>
			<th>Ночей</th>
			<th>Питание</th>
			<th>Номер/Размещение</th>
			<th>Цена</th>
			<th>Веб-цена</th>
			<th>Туроператор</th>
			<th>Эконом</th>
			<th>Бизнес</th>
			<th><th>
		</tr></thead>
		<tbody>
		<tr id="eachrow">
			<td>{%checkin%}</td>
			<td>{%hotel%} ({%town%})</td>
			<td>{%nights%}</td>
			<td>{%meal%}</td>
			<td>{%room%} / {%htplace%}</td>
			<td class="cost">{%price%} {%currency%}</td>
			<td class="cost-discount">{%price_discount%} {%currency%}</td>
			<td>{%touroperator%}</td>
			<td>{%econom%}</td>
			<td>{%business%}</td>
			<td><a class="add_to_basket" href="#">В&nbsp;корзину</a></td>
		</tr>
		</tbody>
		</table>
	</script>
<!-- без туроператора, размещение и номер в разных колонках
<script type="text/html" id="result-template">
<table class="res">
<thead><tr><th>Вылет</th><th>Гостиница</th><th>Ночей</th><th>Питание</th><th>Номер</th><th>Размещение</th><th>Цена</th><th>Эконом</th><th>Бизнес</th></tr></thead>
<tbody><tr id="eachrow"><td>{%checkin%}</td><td>{%hotel%} ({%town%})</td><td>{%nights%}</td><td>{%meal%}</td><td>{%room%}</th><td>{%htplace%}</td><td class="cost">{%price%} {%currency%}</td><td>{%econom%}</td><td>{%business%}</td></tr></tbody>
</table>
</script>
-->
	<script type="text/html" id="notfound-template">
		<h4 aling="center">По указанным критериям ничего не найдено</h4>
		<a href="/upload/instruksiya.png" class="nm">Схема инструкция поиска туров</a>
	</script>
*/?>
</div><!-- resultset -->
</div><!-- samo_container -->
<div id="samo_checkin_beg_data" style="display:none;"><?=json_encode($arResult['CHECKIN_BEG'])?></div>
<div id="samo_checkin_end_data" style="display:none;"><?=json_encode($arResult['CHECKIN_END'])?></div>