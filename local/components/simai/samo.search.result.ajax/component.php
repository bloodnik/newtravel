<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
error_reporting(E_ALL);


if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["DISPLAY_HOTEL_LINK"] = true;

$arParams["ALLOWED_CURRENCIES"] = array(643, 840, 978); # 643 => RUB, 840 => USD, 978 => EUR
$arParams["DEFAULT_CURRENCY"] = 978; # Валюта по умолчанию запрашиваемая на сервере samo.ru
$arParams["DISPLAY_CURRENCY"] = 643; # Валюта по умолчанию показываемая на странице результатов поиска

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

if(!isset($_GET['DOLOAD']))
{
	$_GET["FREIGHT"] = 1;
	$_GET["STOPSALE"] = 1;
}


if($this->StartResultCache(false, (isset($_GET)?http_build_query($_GET):false)))
{

	if(!CModule::IncludeModule("iblock")) {ShowError("Module not found: iblock");$this->AbortResultCache();}
	if(!CModule::IncludeModule("catalog")) {ShowError("Module not found: catalog");$this->AbortResultCache();}
	if(!CModule::IncludeModule("sale")) {ShowError("Module not found: sale");$this->AbortResultCache();}
	if(empty($arParams["SAMO_DATASRC"])) {ShowError("Not initilized: SAMO_DATASRC");$this->AbortResultCache();}
	if(empty($arParams["SAMO_UID"])) {ShowError("Not initilized: SAMO_UID");$this->AbortResultCache();}

	$query = array('action'=>'init', 'uid'=>$arParams["SAMO_UID"], 'callback'=>'jsonp'.rand(1000000000000,9997323126099));
	if(isset($_GET['DOLOAD']))
	{
		# если цена в RUB берем в USD и конвертируем по внутреннему по курсу...
		$CURRENCY_ID = IntVal($_GET["CURRENCYINC"]);
		if($_GET["CURRENCYINC"] != $arParams["DEFAULT_CURRENCY"])
			$_GET["CURRENCYINC"] = $arParams["DEFAULT_CURRENCY"]; # цену всегда берем в USD/EUR
		$_GET["COSTMIN"] = CCurrencyRates::ConvertCurrency($_GET["COSTMIN"],
			$CURRENCY[$CURRENCY_ID],
			$CURRENCY[$arParams["DEFAULT_CURRENCY"]]
		);
		$_GET["COSTMAX"] = CCurrencyRates::ConvertCurrency($_GET["COSTMAX"],
			$CURRENCY[$CURRENCY_ID],
			$CURRENCY[$arParams["DEFAULT_CURRENCY"]]
		);
		$query = array_merge($query, $_GET);
		$_GET["CURRENCYINC"] = $CURRENCY_ID;
	}
	else
	{
		$query = array_merge($query, array(
			"DOLOAD"=>1,
			"PAGE"=>1,
			"TOWNFROMINC"=>150,
			"STATEINC"=>3,
			"NIGHTS_FROM"=>1,
			"NIGHTS_TILL"=>14,
			"ADULT"=>2,
			"CURRENCYINC"=>$arParams["DEFAULT_CURRENCY"],
			"COSTMIN"=>"",
			"COSTMAX"=>"",
			"CHILD"=>0,
			"CHECKIN_BEG"=>date("Ymd"),
			"CHECKIN_END"=>date("Ymd", mktime(0,0,0,date("m"),date("d"),date("Y")+1)),
			"FREIGHT"=>1,
			"STOPSALE"=>1
		));
		$CURRENCY_ID = $arParams["DISPLAY_CURRENCY"];
	}
	$time_start = microtime(true);
	$arResult = SamoSearchFormAjaxGetJson($arParams["SAMO_DATASRC"], $query);
	$time_end =  microtime(true);

	if(!$arResult = SamoSearchFormAjaxDecodeJson($arResult))
	{
		ShowError("Error connecting to samo.ru server");
		$this->AbortResultCache();
	}
	$arResult["SAMO_SEARCH_REQUEST_TIME"] = round($time_end - $time_start, 2);
	
	# Получаем ссылки
	$res = CIBlockElement::GetList(
		array(),
		array('IBLOCK_ID'=>$arParams["TOUR_OPERATOR_IBLOCK_ID"], 'IBLOCK_TYPE'=>$arParams["TOUR_OPERATOR_IBLOCK_TYPE"]),
		false,
		false,
		array('NAME', 'PROPERTY_DISCOUNT')
	);
	while($arItem = $res->GetNext(true, false))
		$arDiscount[trim(strtolower($arItem['NAME']))] = $arItem['PROPERTY_DISCOUNT_VALUE'];

	# Вычисляем процент скидки заданные в инфоблоке Туроператоры из цены путевки
	foreach($arResult['CURRENCY'] as $arItem)
		$CURRENCY[$arItem[0]] = $arItem[1];

	# Текущая страна
	foreach($arResult['STATE'] as $arState)
		if($arState[2] == 1)
			$arResult['CURRENT_STATE'] = $arState[1];

	foreach($arResult["PRICES"][2] as $i=>$arItem)
	{
		# Расчитываем скидки, цены и тд
		$arResult["PRICES"][2][$i][9] = $CURRENCY[$CURRENCY_ID];
		$arResult["PRICES"][2][$i][18] = ceil($arItem[8]*(1 - $arDiscount[trim(strtolower($arItem[10]))]/100.));
		$arResult["PRICES"][2][$i][8] = floor(CCurrencyRates::ConvertCurrency(
			$arItem[8],
			$CURRENCY[$arParams["DEFAULT_CURRENCY"]],
			$CURRENCY[$CURRENCY_ID]
		));
		if(is_numeric($arItem[8]) && $arDiscount[trim(strtolower($arItem[10]))] > 0)
		{
			$arResult["PRICES"][2][$i][17] = floor(CCurrencyRates::ConvertCurrency(
				$arResult["PRICES"][2][$i][18],
				$CURRENCY[$arParams["DEFAULT_CURRENCY"]],
				$CURRENCY[$CURRENCY_ID]
			));
		}
		else
		{
			# скидок нет
			$arResult["PRICES"][2][$i][17] = $arResult["PRICES"][2][$i][8];
		}
		$arResult["PRICES"][2][$i]["HOTEL_URL"] = "/tours/search/hotel.php?hotel=".urlencode($arItem[4])."&state=".urlencode($arResult['CURRENT_STATE']);
	}

	if($CURRENCY_ID != $arParams["DEFAULT_CURRENCY"])
	{
		foreach($arResult['CURRENCY'] as $key=>$arItem)
			if($arItem[0] == $arParams["DEFAULT_CURRENCY"])
				$arResult['CURRENCY'][$key][2] = 0;
			elseif($arItem[0] == $CURRENCY_ID)
				$arResult['CURRENCY'][$key][2] = 1;
	}

	if(!isset($_GET['DOLOAD']))
	{
		$arResult['NIGHTS_FROM'] = 1;
		$arResult['NIGHTS_TILL'] = 14;
	}
	
	# Объедененная жратва
	$arMeal = array();
	foreach($arResult['MEAL'] as $arItem)
		if($arItem[2] == 1)
			$arMeal[] = $arItem[0];

	$arResult['MEAL_SELECTED'] = implode(',', $arMeal);
	$arResult['MEAL'] = array(
		array(5,'AI',false,),
		array("1,2000000009",'BB',false,),
		array(2000000010,'DINNER',false,),
		array("4,2000000011",'FB',false,),
		array("3,2000000012",'HB',false,),
		array(8,'OB',false,),
		array(2000000008,'OTHER',false,),
		array(2000000007,'PROGRAM',false,),
		array(7,'UAI',false,)
	);

	if($this->InitComponentTemplate())
	{
		$template = &$this->GetTemplate();
		$arResult['TEMPLATE_PATH'] = $template->GetFolder();
		$this->ShowComponentTemplate();
	}
}
$APPLICATION->AddHeadScript($arResult['TEMPLATE_PATH']."/search.js");
$APPLICATION->AddHeadScript($arResult['TEMPLATE_PATH']."/calendar.js");
?>