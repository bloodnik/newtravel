<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-"=>" "));

$arIBlocks=Array();
foreach(array('TOUR_OPERATOR', 'HOTEL') as $iblock_name)
{
	$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"),	Array("SITE_ID"=>$_REQUEST["site"], "TYPE"=>($arCurrentValues[$iblock_name."_IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
	while($arRes = $db_iblock->Fetch())
		$arIBlocks[$iblock_name][$arRes["ID"]] = $arRes["NAME"];
}

$arComponentParameters = array(
	"GROUPS"=>array(
	),
	"PARAMETERS"=>array(
		"TOUR_OPERATOR_IBLOCK_TYPE" => Array(
			"PARENT" => "BASE",
			"NAME" => "Тип инфоблока",
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "permits",
			"REFRESH" => "Y",
		),
		"TOUR_OPERATOR_IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "Туроператор",
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks["TOUR_OPERATOR"],
			"DEFAULT" => 15,
			"REFRESH" => "Y",
		),
		"HOTEL_IBLOCK_TYPE" => Array(
			"PARENT" => "BASE",
			"NAME" => "Тип инфоблока",
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "permits",
			"REFRESH" => "Y",
		),
		"HOTEL_IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "Отель",
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks["HOTEL"],
			"DEFAULT" => 2,
			"REFRESH" => "Y",
		),
		"SAMO_DATASRC"=>Array(
			"PARENT"=>"BASE",
			"NAME"=>"Путь к серверу Андромеда (samo.ru)",
			"TYPE"=>"STRING",
			"DEFAULT"=>'http://search.samo.ru/search/index.php',
			"MULTIPLE"=>"N",
		),
		"SAMO_UID"=>Array(
			"PARENT" =>"BASE",
			"NAME"=>"ID в системе Андромеда (samo.ru)",
			"TYPE"=>"STRING",
			"DEFAULT"=>'',
			"MULTIPLE"=>"N",
		),
		"SAMO_DATATRG"=>Array(
			"PARENT"=>"BASE",
			"NAME"=>"Страница заказа",
			"TYPE"=>"STRING",
			"DEFAULT"=>'/personal/order/basket/',
			"MULTIPLE"=>"N",
		),
		"SAMO_PROXY"=>Array(
			"PARENT"=>"BASE",
			"NAME"=>"Страница проксирующая данные с (samo.ru)",
			"TYPE"=>"STRING",
			"DEFAULT"=>'/tours/search/ajax.php',
			"MULTIPLE"=>"N",
		),
		"CACHE_TIME" => Array("DEFAULT"=>36000000),
	),
);
?>
