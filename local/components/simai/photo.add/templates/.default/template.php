<?
/*****************************************
*                                        *
*   Компонент разработан Blair           *
*   Simai Studio                         *
*   http://simai.ru/                     *
*                                        *
* Добавление пользовательских фотографий *
*                                        *
*****************************************/


if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule("form");


// Режим одиночного добавления
if ($arResult["mode"] == "N") {  require_once("one_add.php");
}

// Режим множественного добавления
elseif ($arResult["mode"] == "Y") {  require_once("many_add.php");
}


?>

