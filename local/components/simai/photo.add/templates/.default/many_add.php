<?
/*****************************************
*                                        *
*   Компонент разработан Blair           *
*   Simai Studio                         *
*   http://simai.ru/                     *
*                                        *
* Добавление пользовательских фотографий *
*                                        *
*****************************************/

//
// Добавление списка фотографий
//

// $arResult["add"] -> содержит кол-во добавленных фотографий
// $arResult["count_add"] -> кол-во фотографий которое может добавить пользователь


if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule("form");


?>


<h4 style="color:green">Добавление фотографий</h4>

<?php if(isset($arResult["add"])) echo "<b>Добавленно: ".$arResult["add"]." фотографий</b><br>"; ?>
<?php
if ($arResult["count_add"] == 0) echo "Вы больше не можете добавлять фотографии в данный фотоальбом<br>";
elseif ($arResult["count_restriction"] == "N") echo "Вы можете еще добавить в этот альбом <b>".$arResult["count_add"]."</b> фотографий<br>";
?>

<form action="" method="POST" ENCTYPE="multipart/form-data">

Выберите фотоальбом:<br>
<? echo CForm::GetDropDownField(
  "section",
  $arResult["sections_list"],
  $arResult["form"]["section"]
);
?><br><br>


<?

for ($i = 0; $i < $arResult["mode_count"]; $i++) {  echo 'Название фото:<br>
  <input type="text" size="25" name="name_'.$i.'">
  <input type="file" name="foto_'.$i.'"><br><br>';
}


?>

<br><br>
<input type="submit" value="Добавить фото">
</form>
