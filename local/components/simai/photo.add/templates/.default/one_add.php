<?
/*****************************************
*                                        *
*   Компонент разработан Blair           *
*   Simai Studio                         *
*   http://simai.ru/                     *
*                                        *
* Добавление пользовательских фотографий *
*                                        *
*****************************************/

//
// Добавление одной фотографии
//


if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule("form");



//
// ФОРМА ДОБАВЛЕНИЯ ФОТОГРАФИИ
//
if (!isset($arResult["add"]) OR $arResult["add"] == false) {
?>


<h4 style="color:green">Добавление фотографий</h4>

<?php if(isset($arResult["error"])) echo "<b>".$arResult["error"]."</b><br>"; ?>

<form action="" method="POST" ENCTYPE="multipart/form-data">

Название фото:<br>
<input type="text" size="25" name="name" value="<?php echo $arResult["form"]["name"]; ?>"><br><br>

Выберите фотоальбом:<br>
<? echo CForm::GetDropDownField(
  "section",
  $arResult["sections_list"],
  $arResult["form"]["section"]
);
?><br><br>

Описание:<br>
<?$APPLICATION->IncludeComponent("bitrix:fileman.light_editor", "", array(
	"CONTENT" => $arResult["form"]["description"],
	"INPUT_NAME" => "description",
	"INPUT_ID" => "",
	"WIDTH" => "470",
	"HEIGHT" => "200px",
	"VIDEO_ALLOW_VIDEO" => "N",
	"USE_FILE_DIALOGS" => "N",
	"FLOATING_TOOLBAR" => "N",
	"ARISING_TOOLBAR" => "Y",
	"ID" => "",
	"JS_OBJ_NAME" => ""
	),
	false
);?>


<?
    echo "Фото: <br>".CFile::InputFile("foto", 20, $arResult["form"]["foto"]);
    if (strlen($arResult["form"]["foto"])>0):
        ?><br><?echo CFile::ShowImage($arResult["form"]["foto"], 200, 200, "border=0", "", true);
    endif;
?>
<br><br>
<input type="submit" value="Добавить фото">
</form>




<?php
} // -конец формы добавления фотографии

//
// Показываем что фотография добавилась
//
else {
?>


<b>Фотография была успешно добавленна</b>

<?php
} // -конец формы показа что фото добавилось
?>

