<?
$MESS ['CMDESKTOP_PARAMS_ID'] = "Идентификатор";
$MESS ['CMDESKTOP_PARAMS_CAN_EDIT'] = "Разрешить настраивать рабочий стол всем авторизованным пользователям";
$MESS ['CMDESKTOP_PARAMS_COLUMNS'] = "Количество столбцов";
$MESS ['CMDESKTOP_PARAMS_COLUMN_WITH'] = "Размер столбца (px или %)";
$MESS ['CMDESKTOP_PARAMS_GADGETS'] = "Доступные гаджеты";
$MESS ['CMDESKTOP_PARAMS_GADGETS_ALL'] = "(все имеющиеся)";
$MESS ['CMDESKTOP_PARAMS_GADGET_PAR'] = "Настройки по умолчанию параметров пользователя для гаджета";
$MESS ['CMDESKTOP_PARAMS_GADGET_SET'] = "Настройки гаджета";
?>