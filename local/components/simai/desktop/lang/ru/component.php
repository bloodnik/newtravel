<?
$MESS ['CMDESKTOP_AUTH_ERR'] = "Ошибка авторизации";
$MESS ['CMDESKTOP_GROUP_PERSONAL'] = "Личное";
$MESS ['CMDESKTOP_GROUP_PERSONAL_DESCR'] = "Персональные инструменты";
$MESS ['CMDESKTOP_GROUP_SONET'] = "Социальная сеть";
$MESS ['CMDESKTOP_GROUP_SONET_DESCR'] = "Информация пользователя";
$MESS ['CMDESKTOP_GROUP_CONTENT'] = "Умный папа";
$MESS ['CMDESKTOP_GROUP_CONTENT_DESCR'] = "Новое и актуальное";
$MESS ['CMDESKTOP_GROUP_COMMUN'] = "Общение";
$MESS ['CMDESKTOP_GROUP_COMMUN_DESCR'] = "Блоги, форумы";
$MESS ['CMDESKTOP_GROUP_BUY'] = "Купилка";
$MESS ['CMDESKTOP_GROUP_BUY_DESCR'] = "Все о покупках и продажах";
$MESS ['CMDESKTOP_GROUP_SERVICES'] = "Внешние сервисы";
$MESS ['CMDESKTOP_GROUP_SERVICES_DESCR'] = "Сервисы получения информации из Интернет";
$MESS ['CMDESKTOP_GROUP_OTHER'] = "Прочие";
$MESS ['CMDESKTOP_GROUP_OTHER_DESCR'] = "Разные гаджеты";
?>