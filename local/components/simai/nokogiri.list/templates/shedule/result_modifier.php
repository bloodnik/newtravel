<?
$coding = "utf-8";//"Windows-1251";
foreach ($arResult["tr"] as &$tr)
{
	if(isset($tr["td"]))
	{
		foreach($tr["td"] as $pos => &$td)
		{
			$td = trim($td);

			if($td == '-')
			{
				$td = '&mdash';
			}
			elseif($pos == 0)
			{
				//$td = mb_convert_case($td, MB_CASE_LOWER, $coding);
				$td = mb_strtolower($td, $coding);
				$td = preg_replace('/^.|(?<=\().|(?<= ).|(?<=-)./e', "mb_strtoupper('\\0', '$coding')", $td);
				$td = preg_replace('/(?<=\S)\(/', '&nbsp;(', $td);
			}
			elseif(in_array($pos, array(3, 8)))
			{
				preg_match('/[\d\.]{7,7}/', $td, $m, PREG_OFFSET_CAPTURE);
				if(!empty($m[0]))
				{
					$s = str_replace('.', '', $m[0][0]);
					$s = str_replace(array(1,2,3,4,5,6,7), array('пн, ', 'вт, ', 'ср, ', 'чт, ', 'пт, ', 'сб, ', 'вс, '), $s);
					$s = rtrim($s, ', ');
					$s = str_replace('пн, вт, ср, чт, пт, сб, вс', 'по всем дням', $s);
					$td = substr($td, 0, $m[0][1]).$s.substr($td, $m[0][1]+strlen($m[0][0]));
				}
				$td = preg_replace('/(Января|Февраля|Марта|Апреля|Мая|Июня|Июля|Августа|Сентября|Октября|Ноября|Декабря)/','$1<br/>', $td);
				$td = preg_replace('/,(?=\S)/', ',&nbsp;', $td);
			}
			elseif(in_array($pos, array(4, 5, 6, 9, 10)))
			{
				$td = preg_replace('/(\d\d)\.(\d\d)/','$1:$2', $td);
			}
		}
	}
}

?>
