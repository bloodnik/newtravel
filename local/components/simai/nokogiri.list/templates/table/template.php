<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? //I($arResult)?>
<table class="data-table">
<?foreach ($arResult["tr"] as $tr):?>
	<?if(isset($tr["td"])):?>
	<tr>
		<?foreach($tr["td"] as $td):?>
		<td><?=$td?></td>
		<?endforeach;?>
	</tr>
	<?elseif(isset($tr["th"])):?>
	<tr>
		<?foreach($tr["th"] as $td):?>
		<th><?=$td?></th>
		<?endforeach;?>
	</tr>
	<?endif;?>
<?endforeach;?>
</table>
