<?
$coding = 'Windows-1251';
foreach ($arResult["tr"] as $i => &$tr)
{
	if($i == 0)
	{
		unset($arResult["tr"][$i]);
	}
	if($i == 1)
	{
		$arResult["tr"][$i]["th"] = $arResult["tr"][$i]["td"];
		unset($arResult["tr"][$i]["td"]);
		foreach($tr["th"] as $j => &$th)
			$th = trim(strip_tags($th));
	}
	elseif(isset($tr["td"]))
	{
		if(is_array($tr["td"]))
		{
			foreach($tr["td"] as $j => &$td)
			{
				$td = trim(strip_tags($td));
	
				if($td == '-')
				{
					$td = '&mdash';
				}
				elseif($j == 5)
				{
					$td = mb_strtolower($td, $coding);
					$td = preg_replace('/^.|(?<=\().|(?<= ).|(?<=-)./e',"mb_strtoupper('\\0', '$coding')", $td);
					$td = preg_replace('/(?<=\S)\(/', '&nbsp;(', $td);
				}
			}
		}
		else
		{
			$tr["td"] = trim(strip_tags($tr["td"]));
		}
	}
}

?>
