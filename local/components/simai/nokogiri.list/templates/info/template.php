<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?// I($arResult)?>
<table class="data-table" width="100%">
<?foreach ($arResult["tr"] as $tr):?>
	<tr>
	<?if (isset($tr["td"])):?>
		<?if (count($tr["td"]) == 1):?>
			<td colspan="7"><h2><?=strip_tags($td)?></h2></td>
		<?else:?>
			<?foreach($tr["td"] as $td):?>
				<td><?=strip_tags($td)?></td>
			<?endforeach;?>
		<?endif;?>
	<?elseif(isset($tr["th"])):?>
		<?foreach($tr["th"] as $td):?>
			<th><?=strip_tags($td)?></th>
		<?endforeach;?>
	<?endif;?>
	</tr>
<?endforeach;?>
</table>