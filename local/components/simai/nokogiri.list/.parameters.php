<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"PARSE_URL" => array(
			"PARENT" => "BASE",
			"NAME" => "Адрес URL для парсинга (http://example.com)",
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"CODING" => array(
			"PARENT" => "BASE",
			"NAME" => "Кодировка сайта",
			"TYPE" => "LIST",
			"VALUES" => array("UTF-8"=>"UTF-8", "CP1251"=>"CP1251"),
			"DEFAULT" => "CP1251"
		),
		"QUERY" => array(
			"PARENT" => "BASE",
			"NAME" => "Строка запроса (распознаются вложеные теги (через пробел), а также конструкции вида .class, #id и [attr=value])",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"MAX_LEVEL" => array(
			"PARENT" => "BASE",
			"NAME" => "Максимальная глубина вывода массива (от 1 до N)",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"CACHE_TIME" => array("DEFAULT"=>36000000),
		"CACHE_GROUPS" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => "Учитывать права доступа",
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y"
		)
	)
);
?>
