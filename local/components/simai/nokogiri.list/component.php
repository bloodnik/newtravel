<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

if(!isset($arParams["PARSE_URL"]) || empty($arParams["PARSE_URL"]))
{
	ShowError("Не введен адрес");
	return;
}
if(!isset($arParams["MAX_LEVEL"]) || empty($arParams["MAX_LEVEL"]))
{
	$arParams["MAX_LEVEL"] = null;
}
elseif(is_numeric($arParams["MAX_LEVEL"]) && $arParams["MAX_LEVEL"] <= 0)
{
	ShowError("Не правильный параметр 'Максимальная глубина вывода массива'");
	return;
}
if(!isset($arParams["QUERY"]) || empty($arParams["QUERY"]))
{
	ShowError("Не введена строка запроса");
	return;
}
$arParams["PARSE_URL"] = trim($arParams["PARSE_URL"]);
$arParams["QUERY"] = trim($arParams["QUERY"]);
if($arParams["MAX_LEVEL"] !== null)
	$arParams["MAX_LEVEL"] = IntVal(trim($arParams["MAX_LEVEL"]));

if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N" ? false : $USER->GetGroups()))))
{
	$html = file_get_contents($arParams["PARSE_URL"]);
	if($html === false)
	{
		$this->AbortResultCache();
		ShowError("Документ по адресу недоступен: ".$arParams["PARSE_URL"]);
		return;
	}
 	$saw = new nokogiri($html, $arParams["CODING"]);
 	$arResult = $saw->get($arParams["QUERY"])->toArray($arParams["MAX_LEVEL"]);
	$this->IncludeComponentTemplate();
}
?>
