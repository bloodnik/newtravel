<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
# .*?,(.*?),(.*?),(.*?),(.*?),.*?\n
# $2 => $4,\n
$arResult = array();
$errorMessage = "";

$host = "http://testapi.anywayanyday.com/api"; # хост, присвоенный партнёру
$Partner = 'testapid'; # код, присвоенный партнёру
$L = 'RU'; # язык отображение ответа: RU – русский (по умолчанию), EN – английский.

if ($_SERVER["REQUEST_METHOD"] == "GET" && check_bitrix_sessid()):
/*
 * Инициализация запроса
 */
if (!isset($R))
{
	# Маршрут, содержащий пункты вылета, прилёта, дату и месяц вылета
	# Маршрут может содержать несколько участков, каждый из которых представляет набор последовательных десяти символов:
	# два символа  день вылета с лидирующим нулём; два символа  месяц вылета с лидирующим нулём;
	# три символа – IATA код города либо аэропорта вылета; три символа – IATA код города либо аэропорта прилёта.
	$Route = '3011MOWLON1212LONMOW';
	//$Route = Trim($_GET['flight_from']).Trim($_GET['flight_from_date']).Trim($_GET['flight_to']).Trim($_GET['flight_to_date']);
	# Количество взрослых пассажиров. Число от 1 до 6. Значение по умолчанию 1;
	$AD = (isset($_GET['AD'])?(IntVal(Trim($_GET['AD']))<1?1:(IntVal(Trim($_GET['AD']))>6?6:Trim($_GET['AD']))):1);
	# Количество детей в возрасте от 2-х до 12 лет. Число от 0 до 4. Значение по умолчанию 0;
	$CN = (isset($_GET['CN'])?(IntVal(Trim($_GET['AD']))<0?0:(IntVal(Trim($_GET['AD']))>4?4:Trim($_GET['CN']))):0);
	# Класс обслуживания. Принимаемые значения: E (эконом), B (бизнес), A(все). Значение по умолчанию: E.
	$SC = (isset($_GET['SC'])?(!in_array(Trim($_GET['SC']), array('E','B','A'))?'E':Trim($_GET['SC'])):'E');
	# Partner - код, присвоенный партнёру
	$url = "$host/NewRequest/?Route=$Route&AD=$AD&CN=$CN&SC=$SC&Partner=$Partner";
	$str = file_get_contents($url);
	$xml = simplexml_load_string($str);
	$arResult[] = $url;
	if ((bool)$xml['Id'])
	{
		$R = (string)$xml['Id']; # ключ запроса;
		$mode = 'state';
	}
	elseif ((bool)$xml['Error'])
	{
		$errorMessage.= 'NewRequest: ';
		switch((string)$xml['Error'])
		{
			case 'WrongRouteParams': $errorMessage.= 'Параметры маршрута не указаны, либо указаны не корректно'; break;
			case 'WrongPassengersCount': $errorMessage.= 'Не верное количество пассажиров'; break;
			case 'SearchNotAvailable': $errorMessage.= 'Поиск временно не доступен'; break;
			case 'UnknownPartner': $errorMessage.= 'Код партнёра не опознан, либо для этого партнёра запрещён запрос с IP адреса, с которого поступил запрос';break;
			case 'SystemError': $errorMessage.= 'Системная ошибка'; break;
			default: $errorMessage.= 'Неизвестная ошибка';
	    }
	    $errorMessage.= '<br>';
	}
}
/*
 * Получение статуса поискового запроса
 */
function get_request_state($mode, $host, $R, &$arResult, &$errorMessage)
{
	# R – ключ запроса;
	$url = "$host/RequestState/?R=$R";
	$arResult[] = $url;
	while (true)
	{
		$str = file_get_contents($url);
		$xml = simplexml_load_string($str);
		if ((bool)$xml['Completed'])
		{
			$completed = (string)$xml['Completed']; # статус завершения поиска в процентом выражении (от 0 до 100).
			$arResult[] = $completed;
			if ((int)$completed == 100)
			{
				return $mode;
			}
		}
		elseif ((bool)$xml['Error'])
		{
			$errorMessage.= 'RequestState: ';
			switch((string)$xml['Error'])
			{
				case 'InvalidRequest':$errorMessage.= 'Ключ запроса не указан  либо не существует или указан неверно';break;
				case 'RequestExpired':$errorMessage.= 'Истекло время жизни результатов поиска';break;
				default:$errorMessage.= 'Неизвестная ошибка';
			}
			$errorMessage.= '<br>';
		    return false;
		}
		sleep(1); # ждать 1 секунду
	}
}

if ($R && $mode == 'state')
if ($R && $mode == 'state')
{
	$mode = get_request_state('info', $host, $R, $arResult, $errorMessage);
}
/*
 * Получение деталей поискового запроса.
 */
if ($R && $mode == 'info')
{
	# R – ключ запроса;
	$L = (isset($_GET['L'])?Trim($_GET['L']):$L); # язык отображение ответа: RU – русский (по умолчанию); EN – английский.
	$url = "$host/RequestInfo/?R=$R&L=$L";
	$str = file_get_contents($url);
	$xml = simplexml_load_string($str);
	$arResult[] = var_export((bool)$xml['AD'],(bool)$xml['CN'],(bool)$xml['IN'],(bool)$xml['CS'], true);
	if (!(bool)$xml['Error']) //(bool)$xml['AD'] || (bool)$xml['CN'] || (bool)$xml['IN'] || (bool)$xml['CS']
	{
		foreach ($xml->Direction as $direction) # direction – участок маршрута
		{
			$arResult[] = array(
				(string)$direction['Route'], # IATA код города либо аэропорта вылета и прилёта
				(string)$direction['DC'], # страна вылета
				(string)$direction['DP'], # город/аэропорт вылета
				(string)$direction['AC'], # страна прилёта
				(string)$direction['AP'], # город/аэропорт прилёта
				(string)$direction['DD'] # дата вылета в формате 2009-10-05
			);
		}
		$mode = 'search';
	}
	else
	{
		$errorMessage.= 'RequestInfo: ';
		switch((string)$xml['Error'])
		{
			case 'InvalidRequest':$errorMessage.= 'Ключ запроса не указан  либо не существует или указан неверно';break;
			case 'RequestExpired':$errorMessage.= 'Истекло время жизни результатов поиска';break;
			default:$errorMessage.= 'Неизвестная ошибка';
	    }
	    $errorMessage.= '<br>';
	}
}
/*
 * Получение результата поиска
 */
if ($R && $mode == 'search')
{
	# R – ключ запроса;
	$V = 'Matrix'; # метод выдачи: Matrix – матричный (по умолчанию); List – списком (доступен при число участков маршрута не более 2-х).
	# L – язык отображение ответа: RU – русский (по умолчанию); EN – английский.
	$C = 'RUB'; # код валюты просмотра стоимости: RUB – российские рубли (по умолчанию); USD – доллары США;EUR – евро.
	$S = 'Price'; # сортировка: Price – по цене (по умолчанию); Time – по времени в пути.
	$VB = 'false'; # метод отображения информации о тарифе: false – краткий (по умолчанию); true – подробный.

	# Не обязательные параметры
	$BC1 = ''; # фильтр по количеству пересадок
	$DT1 = ''; # фильтр по времени вылета: N – ночь; M – утро; D – день; E – вечер.
	$AT1 = ''; # фильтр по времени прилёта: N – ночь; M – утро; D – день; E – вечер.
	$DA1 = ''; # фильтр по аэропорту вылета, IATA код аэропорта;
	$AA1 = ''; # фильтр по аэропорту прилёта, IATA код аэропорта.
	$PS = ''; # размер страницы результатов поиска. (только для метода List)
	$PN = ''; # номер страницы результатов поиска. (только для метода List)
	$CT = 'All'; # connections type: All – все варианты (по умолчанию); Direct – только прямые перелёты
	$PT = 'All'; # Тип цен: All – цена за всех пассажиров (по умолчанию); Adult – цена на одного взрослого;

	$url = "$host/Fares/?R=$R&V=$V&L=$L&C=$C&S=$S&VB=$VB";
	$str = file_get_contents($url);
	$xml = simplexml_load_string($str);
	if (!(bool)$xml['Error']) //(bool)$xml['AD'] || (bool)$xml['CN'] || (bool)$xml['IN'] || (bool)$xml['CS']
	{
		if(get_request_state(true, $host, $R, $arResult, $errorMessage))
		{
			$arResult[] = $xml;
		}
	}
	else
	{
		$errorMessage.= 'Fares: ';
		switch((string)$xml['Error'])
		{
			case 'InvalidRequest':$errorMessage.= 'Ключ запроса не указан  либо не существует или указан неверно';break;
			case 'RequestExpired':$errorMessage.= 'Истекло время жизни результатов поиска';break;
			case 'SearchNotComplete':$errorMessage.= 'Процесс поиска не завершен';break;
			case 'NoFaresFound':$errorMessage.= 'По данному запросу нет предложений';break;
			default:$errorMessage.= 'Неизвестная ошибка';
	    }
	    $errorMessage.= '<br>';
	}
}
if($errorMessage)
	$arResult['ERROR'] = $errorMessage;
$this->IncludeComponentTemplate();
else:
if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"?false:$USER->GetGroups()))))
{
	$this->SetResultCacheKeys(array());
	$this->IncludeComponentTemplate();
}
endif; # beginif: $_SERVER["REQUEST_METHOD"] == "GET" && check_bitrix_sessid()

?>
