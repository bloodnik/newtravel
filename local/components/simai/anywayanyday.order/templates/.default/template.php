<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/jquery-ui.css");
$APPLICATION->AddHeadScript('https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js');
$APPLICATION->AddHeadScript('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.7/jquery-ui.min.js');
?>
<? I($arResult);?>
<form method="get" name="search_flight" id="search_flight">
<?=bitrix_sessid_post()?>
<div id="progressbar"></div>
<label for="flight_from">Пункт вылета: </label><input id="flight_from" name="flight_from" type="text">
<label for="flight_from_date">Дата и месяц вылета: </label><input id="flight_from_date" name="flight_from_date" type="text" /><br>
<label for="flight_to">Пункт прилета: </label><input id="flight_to" name="flight_to" type="text">
<label for="flight_to_date">Дата и месяц прилета: </label><input id="flight_to_date" name="flight_to_date" type="text" />
<?$APPLICATION->IncludeComponent(
	"bitrix:main.calendar",
	"",
	Array(
		"SHOW_INPUT" => "N",
		"FORM_NAME" => "c1234",
		"INPUT_NAME" => "date_fld",
		"INPUT_NAME_FINISH" => "date_fld_finish",
		"INPUT_VALUE" => "",
		"INPUT_VALUE_FINISH" => "",
		"SHOW_TIME" => "N",
		"HIDE_TIMEBAR" => "Y"
	),
	$component
);?>
<label for="AD">Количество взрослых пассажиров:</label>
<select id="AD" name="AD">
	<option>1</option>
	<option>2</option>
	<option>3</option>
	<option>4</option>
	<option>5</option>
	<option>6</option>
</select>
<br/>
<label for="CN">Количество детей в возрасте от 2-х до 12 лет</label>
<select id="CN" name="CN">
	<option>0</option>
	<option>1</option>
	<option>2</option>
	<option>3</option>
	<option>4</option>
</select>
<br/>
<label for="SC">Класс обслуживания</label
<select id="SC" name="SC">
	<option value="E">эконом</option>
	<option value="B">бизнес</option>
	<option value="A">все</option>
</select>
<br/>
<input type="submit" value="Искать" />
</form>
