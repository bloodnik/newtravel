<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="currency-container">
<?if($arResult["USD"]):?><div id="usd-container"><?=round($arResult["USD"], 2)?></div><?endif?>
<?if($arResult["EUR"]):?><div id="eur-container"><?=round($arResult["EUR"], 2)?></div><?endif?>
</div>
