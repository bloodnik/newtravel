<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="curs">
<div class="usd"><span class="l-usd">USD</span><span class="l-sd">&nbsp;</span><span class="l-eur"><?=number_format($arResult["USD"], 2, '.', ' ');?></span></div>
<div class="eur"><span class="l-usd">EUR</span><span class="l-sd">&nbsp;</span><span class="l-eur"><?=number_format($arResult["EUR"], 2, '.', ' ');?></span> </div>
</div>
