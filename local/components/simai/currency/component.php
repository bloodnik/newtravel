<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

if($this->StartResultCache(false, array()))
{
	$by = "date";
	$order = "desc";

	if(!CModule::IncludeModule("currency"))
		return;
	$res = CCurrencyRates::GetList($by, $order, array("CURRENCY"=>"USD"));
	if($ar_rate = $res->Fetch())
	    $arResult["USD"] = $ar_rate["RATE"];

	$res = CCurrencyRates::GetList($by, $order, array("CURRENCY"=>"EUR"));
	if($ar_rate = $res->Fetch())
	    $arResult["EUR"] = $ar_rate["RATE"];
	
	$this->IncludeComponentTemplate();
}
?>
