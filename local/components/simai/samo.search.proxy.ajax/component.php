<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$coding = "Windows-1251";

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["DEFAULT_CURRENCY"] = "EUR"; # Валюта по умолчанию запрашиваемая на сервере samo.ru
$arParams["DISPLAY_CURRENCY"] = "RUB"; # Валюта по умолчанию показываемая на странице результатов поиска

if(isset($_POST) && !empty($_POST))
{
	if(!$USER->IsAuthorized())
	{
		echo $_REQUEST['callback'].'({message:"Пожалуйста, зарегестрируйтесь"})';
		return;
	}

	if(!(CModule::IncludeModule("iblock") && CModule::IncludeModule("catalog") && CModule::IncludeModule("sale"))) {
		echo $_REQUEST['callback'].'({message:"Ошибка загрузки модулей 1С-Битрикс",error:true})';
		return;
	}

	foreach($_POST as $k=>$v)
		$_POST[$k] = trim(htmlspecialcharsEx(iconv("UTF-8", "Windows-1251", $_POST[$k])));

	$arProps = array();
	foreach(array("TOUR_OPERATOR", "TOWNFROMINC", "HOTEL", "ROOM", "MEAL", "HTPLACE") as $v)
	{
		$res = CIBlockElement::GetList(
			array("SORT"=>"ASC"),
			array(array("LOGIC"=>"OR", array("%NAME"=>$_POST[$v]), array("%PROPERTY_ALIAS"=>$_POST[$v])), "IBLOCK_ID"=>$arParams[$v."_IBLOCK_ID"], "IBLOCK_TYPE"=>$arParams["IBLOCK_TYPE"]),
			false,
			false,
			array("ID")
		);
		if($arItem = $res->GetNext())
			$arProps[$v] = $arItem["ID"];

	}
	foreach(array("STATEINC", "ZONE") as $v)
	{
		$res = CIBlockSection::GetList(array("SORT"=>"ASC"), array("%NAME"=>$_POST[$v], "IBLOCK_ID"=>$arParams["HOTEL_IBLOCK_ID"], "IBLOCK_TYPE"=>$arParams["IBLOCK_TYPE"]), false);
		if($arItem = $res->GetNext())
			$arProps[$v] = $arItem["ID"];
	}

	L($arProps);

	# Получаем рейсы
	$html = file_get_contents("http://search.samo.ru/search/index.php?action=broninfo&claiminc=".$_POST["CLAIMINC"]);
	if($html && iconv("UTF-8", "Windows-1251", $html) != 'Туроператор не найден')
	{
		$parse_error = false;
		$saw = new nokogiri($html, $coding);

		# FLIGHT
		if($arItem = $saw->get("#FREIGHTSINFO .direction-0 option")->toArray())
		{
			if(isset($arItem[0]))
				$arItem = $arItem[0];
			$arProps["FLIGHT"] = iconv("UTF-8", "Windows-1251", trim(strip_tags(($arItem["#text"]))));
		}
		else
			$parse_error = true;

		# FLIGHT_BACK
		if($arItem = $saw->get("#FREIGHTSINFO .direction-1 option")->toArray())
		{
			if(isset($arItem[0]))
				$arItem = $arItem[0];
			$FLIGHT_BACK_DATE = preg_match_1('@^\d+\|\d+\|\d+\|(.*?)\|@', $arItem["value"]);
			$arProps["FLIGHT_BACK"] = trim($FLIGHT_BACK_DATE." ".iconv("UTF-8", "Windows-1251", trim(strip_tags($arItem["#text"]))));
		}
		else
			$parse_error = true;

		# TODO добавление всех остальных параметров таких как отель размещение и тд (защита от подделки данных на странице результатов поиска)..
		# c пересчетом цены из POST запроса http://search.samo.ru/search/index.php?action=calc&claiminc=201326001311967206660008497&kcode=
		# ответ jsonp1299320654595({"price":"1014","currency":"EUR","currencyKey":"4","priceRub":"40692","currencyRub":"RUB"})
		/*$options = array('http'=>array(
			'method'=>"POST",
			'header'=>
				"Accept-language: en\r\n".
				"Content-type: application/x-www-form-urlencoded; charset=UTF-8\r\n".
				"X-Requested-With: XMLHttpRequest\r\n".
				"Cookie: SAMO_SOFT_GUEST_ID=293002; SAMO_SOFT_LAST_VISIT=08.06.2011+15%3A22%3A40; __utma=55906345.722575800.1307529813.1307529813.1307529813.1; __utmz=55906345.1307529813.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); ANDROMEDA=ae452eeac4a224d61f8733e1b14a6c21\r\n",
			'content'=>http_build_query(array('action'=>'calc','claiminc'=>$_POST["CLAIMINC"],'kcode'=>''))
		));
		$context = stream_context_create($options);
		$html = file_get_contents("http://search.samo.ru/search/index.php", false, $options);
		L($html);*/

		if(!$parse_error)
		{
			# Название
			$arResult["NAME"] = implode(' / ', array(
				$_POST["TOWNFROMINC"],
				$_POST["STATEINC"],
				$_POST["ZONE"],
				$_POST["HOTEL"],
				$_POST["HTPLACE"],
				$_POST["ROOM"],
				$_POST["MEAL"],
				showNums($_POST["NIGHTS"], 'ночь', 'ночи', 'ночей'),
				showNums(IntVal($_POST["NIGHTS"]) + 1, 'день', 'дня', 'дней'),
				floor(CCurrencyRates::ConvertCurrency($_POST["PRICE"], $arParams["DEFAULT_CURRENCY"], $arParams["DISPLAY_CURRENCY"]))." RUB (".$_POST["PRICE"]." ".$arParams["DEFAULT_CURRENCY"].")",
			));
			# Проверяем если такая путевка
			$res = CIBlockElement::GetList(
				array("SORT"=>"ASC"),
				array("PROPERTY_CLAIMINC"=>$_POST["CLAIMINC"], "IBLOCK_ID"=>$arParams["IBLOCK_ID"], "IBLOCK_TYPE"=>$arParams["IBLOCK_TYPE"]),
				false,
				false,
				array("ID")
			);
			if($arItem = $res->GetNext())
			{
				$PRODUCT_ID = intval($arItem["ID"]);
			}
			else
			{
				# Добавляем путевку в инфоблок
				$el = new CIBlockElement;
				$arFields = array(
					"IBLOCK_TYPE"=>$arParams["IBLOCK_TYPE"],
					"IBLOCK_ID"=> $arParams["IBLOCK_ID"],
					"MODIFIED_BY"=>$USER->GetID(),
					"IBLOCK_SECTION_ID"=>false,
					"PROPERTY_VALUES"=> array_merge(array(
						"NIGHTS_TILL"=>IntVal($_POST["NIGHTS"]),
						"CHECKIN_BEG"=>$_POST["CHECKIN"],
						"CLAIMINC"=>$_POST["CLAIMINC"]),
						$arProps
					),
					"NAME"=>$arResult["NAME"],
					"ACTIVE"=>"N",
					"DATE_ACTIVE_FROM"=>date("d.m.Y"),
					"DATE_ACTIVE_TO"=>$_POST["CHECKIN"],
				);
				if($ID = $el->Add($arFields))
				{
					$PRODUCT_ID = intval($ID);
				}
				else
				{
					echo $_REQUEST['callback'].'({message:"Ошибка добавления путевки в инфоблок",error:true})';
					return;
				}
			}
			if($PRODUCT_ID>0)
			{
				if($arFlyFee = GetFlyFee($PRODUCT_ID))
				{
					# Расчитываем кол-во людей в путевке
					$rsHtplace = CIBlockElement::GetByID($arProps["HTPLACE"]);
					if($arHtplace = $rsHtplace->GetNext())
					{
						# кол-во взрослых
						$adultCount = 0;
						$infantCount = 0;
						if(strpos($arHtplace["NAME"], "Single") === 0
						|| strpos($arHtplace["NAME"], "SNG") === 0
						|| strpos($arHtplace["NAME"], "1 Pax") === 0)
							$adultCount = 1;
						elseif(strpos($arHtplace["NAME"], "Double") === 0
						|| strpos($arHtplace["NAME"], "DBL") === 0
						|| strpos($arHtplace["NAME"], "Twin") === 0
						|| strpos($arHtplace["NAME"], "2 Pax") === 0)
							$adultCount = 2;
						elseif(strpos($arHtplace["NAME"], "Triple") === 0
						|| strpos($arHtplace["NAME"], "TRP") === 0
						|| strpos($arHtplace["NAME"], "3 Pax") === 0)
							$adultCount = 3;
						elseif(strpos($arHtplace["NAME"], "Quadruple") === 0
						|| strpos($arHtplace["NAME"], "QDP") === 0
						|| strpos($arHtplace["NAME"], "4 Pax") === 0)
							$adultCount = 4;
						elseif(strpos($arHtplace["NAME"], "5 Pax") === 0)
							$adultCount = 5;
						# кол-во детей
						if(strpos($arHtplace["NAME"], "+CHD") > 0)
							$adultCount = 1;
						elseif(strpos($arHtplace["NAME"], "+2 CHD") > 0)
							$adultCount = 2;
						elseif(strpos($arHtplace["NAME"], "+3 CHD") > 0)
							$adultCount = 3;
					}
					if(isset($_REQUEST["infant"]) && IntVal($_REQUEST["infant"])>0)
					{
						$infantCount += IntVal($_REQUEST["infant"]);
					}
					$fuel = CCurrencyRates::ConvertCurrency($arFlyFee["FUEL"], $arFlyFee["FUEL_CURRENCY"], $arParams["DEFAULT_CURRENCY"]);
					$infant = CCurrencyRates::ConvertCurrency($arFlyFee["INFANT"], $arFlyFee["INFANT_CURRENCY"], $arParams["DEFAULT_CURRENCY"]);
					$_POST["PRICE"] = $_POST["PRICE"] + $adultCount*$fuel + $infantCount*$infant;
				}
				CPrice::SetBasePrice($PRODUCT_ID, floatval($_POST["PRICE"]), $arParams["DEFAULT_CURRENCY"]);
				# Добавляем товар в корзину
				$arFields = array(
					"PRODUCT_ID" => $PRODUCT_ID,
					//"PRODUCT_PRICE_ID" => 0,
					"PRICE" => $_POST["PRICE"],
					"CURRENCY" => $arParams["DEFAULT_CURRENCY"], # в samo.search.result.ajax/components.php --> $arParams["DEFAULT_CURRENCY"] = 3
					//"WEIGHT" => 0,
					"QUANTITY" => 1,
					"LID" => SITE_ID,
					"DELAY" => "Y",
					//"CAN_BUY" => "Y",
					"NAME" => $arResult["NAME"],
					//"NOTES" => "",
					//"FUSER_ID" => $USER->GetID(),
					//"MODULE" => "catalog",
					//"CALLBACK_FUNC" => "CatalogBasketCallback",
					//"ORDER_CALLBACK_FUNC" => "CatalogBasketOrderCallback",
					//"CANCEL_CALLBACK_FUNC" => "CatalogBasketCancelCallback",
					//"PAY_CALLBACK_FUNC" => "CatalogPayOrderCallback",
					//"DETAIL_PAGE_URL" => "",
				);
				if(CSaleBasket::Add($arFields))
					echo $_REQUEST['callback'].'({message:"Путевка добавлена в корзину",error:false})';
				else
					echo $_REQUEST['callback'].'({message:"Ошибка добавления путевки в корзину",error:true})';
			}
			else
				echo $_REQUEST['callback'].'({message:"Ошибка: путевка не найдена",error:true})';

		}
		else
			echo $_REQUEST['callback'].'({message:"Ошибка загрузки авиарейсов для путевки",error:true})';
	}
	else
		echo $_REQUEST['callback'].'({message:"Ошибка доступа к процессинговому серверу",error:true})';
}
else
{
	# ajax запросы из samo.search.form.ajax и samo.search.result.ajax
	if($this->StartResultCache(false, (isset($_GET)?$_GET:false)))
	{
		if(!CModule::IncludeModule("iblock") || empty($arParams["SAMO_DATASRC"]) || empty($arParams["SAMO_UID"]))
			$this->AbortResultCache();

		if(isset($_GET))
		{
			if(!isset($_GET['uid']))
				$_GET['uid'] = $arParams["SAMO_UID"];
			if(!$arResult = SamoSearchFormAjaxGetJson($arParams["SAMO_DATASRC"], $_GET))
				$this->AbortResultCache();
		}
		else
			$this->AbortResultCache();
		$this->IncludeComponentTemplate();
	}
}
?>