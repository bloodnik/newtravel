<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("all"=>" "));

$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE"=>($arCurrentValues["IBLOCK_TYPE"]!="all"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];
                                  
$arComponentParameters = array(
	"GROUPS"=>array(
	),
	"PARAMETERS"=>array(
		"IBLOCK_TYPE"=>Array(
			"PARENT"=>"BASE",
			"NAME"=>GetMessage("CP_BMS_IBLOCK_TYPE"),
			"TYPE"=>"LIST",
			"VALUES"=>$arTypesEx,
			"DEFAULT"=>"catalog",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH"=>"Y",
		),
		"IBLOCK_ID"=>Array(
			"PARENT"=>"BASE",
			"NAME"=>"Путевки (из модуля)",
			"TYPE"=>"LIST",
			"VALUES"=>$arIBlocks,
			"DEFAULT"=>'1',
			"MULTIPLE"=>"N",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH"=>"Y",
		),
		"HOTEL_IBLOCK_ID"=>Array(
			"PARENT"=>"BASE",
			"NAME"=>"Отели по странам",
			"TYPE"=>"LIST",
			"VALUES"=>$arIBlocks,
			"DEFAULT"=>'1',
			"MULTIPLE"=>"N",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH"=>"Y",
		),
		"MEAL_IBLOCK_ID"=>Array(
			"PARENT"=>"BASE",
			"NAME"=>"Типы питания",
			"TYPE"=>"LIST",
			"VALUES"=>$arIBlocks,
			"DEFAULT"=>'1',
			"MULTIPLE"=>"N",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH"=>"Y",
		),
		"ROOM_IBLOCK_ID"=>Array(
			"PARENT"=>"BASE",
			"NAME"=>"Типы номеров",
			"TYPE"=>"LIST",
			"VALUES"=>$arIBlocks,
			"DEFAULT"=>'1',
			"MULTIPLE"=>"N",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH"=>"Y",
		),
		"HTPLACE_IBLOCK_ID"=>Array(
			"PARENT"=>"BASE",
			"NAME"=>"Типы размещения",
			"TYPE"=>"LIST",
			"VALUES"=>$arIBlocks,
			"DEFAULT"=>'1',
			"MULTIPLE"=>"N",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH"=>"Y",
		),
		"TOUR_OPERATOR_IBLOCK_ID"=>Array(
			"PARENT"=>"BASE",
			"NAME"=>"Туроператоры",
			"TYPE"=>"LIST",
			"VALUES"=>$arIBlocks,
			"DEFAULT"=>'1',
			"MULTIPLE"=>"N",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH"=>"Y",
		),
		"SAMO_DATASRC"=>Array(
			"PARENT"=>"BASE",
			"NAME"=>"Путь к серверу Андромеда (samo.ru)",
			"TYPE"=>"STRING",
			"DEFAULT"=>'http://search.samo.ru/search/index.php',
			"MULTIPLE"=>"N",
		),
		"SAMO_UID"=>Array(
			"PARENT" =>"BASE",
			"NAME"=>"ID в системе Андромеда (samo.ru)",
			"TYPE"=>"STRING",
			"DEFAULT"=>'',
			"MULTIPLE"=>"N",
		),
		"CACHE_TIME"=>Array("DEFAULT"=>36000000),
	),
);
?>
