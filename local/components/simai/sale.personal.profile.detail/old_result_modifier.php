<?
$errorMessage = "";
if (isset($arParams["ID"]) && IntVal($arParams["ID"]) > 0)
	$UserPropsID = IntVal($arParams["ID"]);
else
	$UserPropsID = IntVal($arResult["USER_VALS"]["PROFILE_ID"]);
if (!CModule::IncludeModule("sale"))
{
	ShowError(GetMessage("SALE_MODULE_NOT_INSTALL"));
	return;
}
if (!$USER->IsAuthorized())
{
	$APPLICATION->AuthForm(GetMessage("SALE_ACCESS_DENIED"));
}
$dbOrderUserProps = CSaleOrderUserProps::GetList(
	array("DATE_UPDATE" => "DESC"),
	array(
		"ID" => $UserPropsID,
		"USER_ID" => IntVal($USER->GetID())
	),
	false,
	false,
	array("PERSON_TYPE_ID", "ID")
);
if (!$arOrderUserPropVals = $dbOrderUserProps->Fetch())
{
	$errorMessage .= GetMessage("SALE_NO_PROFILE")."<br />";
}
if (strlen($errorMessage) <= 0)
{
	$dbOrderProps = CSaleOrderProps::GetList(
		array(),
		array(
			"PERSON_TYPE_ID" => $arOrderUserPropVals["PERSON_TYPE_ID"],
			"CODE" => array("F_PASS_ZG","F_PASS_RF")
		),
		false,
		false,
		array("CODE", "ID", "NAME")
	);
	while ($arOrderPropsVals = $dbOrderProps->Fetch())
	{
		$dbOrderUserPropsValue = CSaleOrderUserPropsValue::GetList(
			array(),
			array(
				"USER_PROPS_ID"=>$arOrderUserPropVals["ID"],
				"ORDER_PROPS_ID"=>$arOrderPropsVals["ID"]
			),
			false,
			false,
			array("ID", "NAME")
		);
		if ($arOrderUserPropsValueVals = $dbOrderUserPropsValue->Fetch())
		{
			d("arOrderUserPropsValueVals found");
			$arFileOldIDs = explode(",", $arOrderUserPropsValueVals["VALUE"]);
			if (isset($_FILES["F_PASS_RF"]) && isset($_FILES["F_PASS_ZG"]) && check_bitrix_sessid())
			{
				d("files send found");
				$arImagesTmp = array();
				foreach ($_FILES[$arOrderPropsVals["CODE"]] as $arKey => $arValues)
				{
					foreach ($arValues as $key => $value)
					{
						$arImagesTmp[$key][$arKey] = $value;
					}
				}
				foreach ($arImagesTmp as $key => $arImage)
				{
					if (!empty($arImage["name"]))
					{
						$arImagesTmp[$key]["del"] = "Y";
					}
				}
				$arFileIDs = array();
				foreach ($arImagesTmp as $key => $arImage)
				{
					$res = CFile::CheckImageFile($arImage);
					if (strlen($res) == 0)
					{
						if(strlen($arImage["name"]) > 0 && !empty($arFileOldIDs[$key]))
						{
							CFile::Delete($arFileOldIDs[$key]);
							$arFileOldIDs[$key] = '';
						}
						if(empty($arFileOldIDs[$key]))
						{
							$fid = CFile::SaveFile($arImage, "passport_scans");
							if (intval($fid) > 0 && !empty($arImage["name"]))
							{
								$arFileIDs[] = $fid;
							}
							else
							{
								$arFileIDs[] = '';
								$errorMessage.= $fid."<br>";
							}
						}
						else
						{
							if($_POST[$arOrderPropsVals["CODE"]."_DEL_".$key] == "on")
							{
								CFile::Delete($arFileOldIDs[$key]);
								$arFileIDs[$key] = '';
							}
							else
							{
								$arFileIDs[$key] = $arFileOldIDs[$key];
							}
						}
					}
					else
					{
						$errorMessage.= $res."<br>";
					}
				}
				CSaleOrderUserPropsValue::Delete($arOrderUserPropsValueVals["ID"]);
				CSaleOrderUserPropsValue::Add(array(
				   "USER_PROPS_ID" => $ID,
				   "ORDER_PROPS_ID" => $arOrderPropsVals["ID"],
				   "NAME" => $arOrderUserPropsValueVals["NAME"],
				   "VALUE" => implode(",",$arFileIDs)
				));
			}
			$arResult[$arOrderPropsVals["CODE"]] = (!empty($arFileIDs)?$arFileIDs:(empty($arFileOldIDs)?$arFileOldIDs:array()));
		}
		else
		{
			d("no");
			CSaleOrderUserPropsValue::Add(array(
			   "USER_PROPS_ID" => $arOrderUserPropVals["ID"],
			   "ORDER_PROPS_ID" => $arOrderPropsVals["ID"],
			   "NAME" => $arOrderPropsVals["NAME"],
			   "VALUE" => ",,"
			));
			d(array(
			   "USER_PROPS_ID" => $arOrderUserPropVals["ID"],
			   "ORDER_PROPS_ID" => $arOrderPropsVals["ID"],
			   "NAME" => $arOrderPropsVals["NAME"],
			   "VALUE" => ",,"
			));
		}
	}
}
?>