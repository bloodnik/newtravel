var samo = {
	el : {},
	preloader : null,
	httpcache : {},
	cachelifetime : 900000
};
function error_msg(msg) {
	window.status = msg;
}
$.jsonLoad = function($get_params, $callback) {
	$get_params.uid = samo.uid;
	var url = samo.datasource + $.param($get_params);
	var now = +(new Date());
	var callback = function(result) {
		if (result.error) {
			error_msg(result.error);
		} else {
			samo.httpcache[url] = {json: result, datetime: now};
			$callback(result);
		}
	}
	if (typeof samo.httpcache[url] !== 'undefined' && samo.httpcache[url].datetime + samo.cachelifetime > now) {
		$callback(samo.httpcache[url].json);
	} else {
		$.ajax({url: url, success: callback});
	}
}
Date.dayNames = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота' ];
Date.abbrDayNames = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб' ];
Date.monthNames = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ];
Date.abbrMonthNames = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек' ];
$(document).ready(function() {
	$('#fast_search').append('<div id="preloader">Загрузка данных</div>');
	$('#preloader').bind('ajaxStart', function() {
		$(this).css('display', 'block');
	}).bind('ajaxStop', function() {
		$(this).css('display', 'none');
	});
	$.ajaxSetup({
		error : function(xhr, status, e) {
			if ('timeout' == status) {
				error_msg('Timeout error, try again later...');
			}
		},
		timeout : 60000,
		scriptCharset : 'utf-8',
		dataType : 'jsonp'
	});
	(function() {
		var $search = $('#fast_search');
		samo.uid = $search.attr('samo:uid');
		samo.datasource = $search.attr('samo:datasrc');
		samo.search_page = $search.attr('samo:search_page');
		//$.event.trigger('ajaxStart');
		//$('select,input,button').attr('autocomplete', 'off');
		$('#TOWNFROMINC,#STATEINC,#CHECKIN_BEG,#CHECKIN_END,#NIGHTS_FROM,#NIGHTS_TILL,#ADULT,#CHILD,#CURRENCYINC,#TOWNTO,#STARS,#HOTELS,#MEAL').each(function() {
			samo.el[this.id] = $(this);
		});
		//$.event.trigger('ajaxStop');
	})();
	samo.repaint_form = function(data) {
		for (control in data) {
			values = data[control]
			switch (control) {
			case 'TOWNFROM':
			case 'STATE':
			case 'CURRENCY':
				samo.eselect(samo.el[control + 'INC'], values);
				break;
			case 'HOTELS':
				samo.hotels(samo.el[control], values);
				break;
			case 'CHECKIN_BEG':
				samo.datepicker(samo.el[control], values);
				break;
			case 'NIGHTS_TILL':
			case 'ADULT':
			case 'CHILD':
				samo.el[control].val(values);
				break;
			default:
				break;
			}
		}
	};
	samo.datepicker = function(name, data) {
		if (data == false) {
			name.dpSetValidDates('0');
		} else {
			name.dpSetValidDates(data[2]).dpSetStartDate(data[1]).val(data[0]).dpSetSelected(data[0]);
		}
	};
	samo.hotels = function(name, items) {
		var _options = [[ 0, '- - - - - - - - - - ', 0 ]];
		if (items == false) {
			items = [];
		}
		$(items).each(function() {
			var item = this;
			_options.push([item[0],item[1]+' '+item[3]+(parseInt(item[3])?'*':''),item.length >= 6 && item[5] ]);
		});
		return samo.eselect(name, _options);
	};
	samo.eselect = function(name, items) {
		if (items == false) {
			items = [];
		}
		var add = function(el, v, t, sO) {
			var option = document.createElement("option");
			option.value = v, option.text = t;
			var o = el.options;
			var oL = o.length;
			if (!el.cache) {
				el.cache = {};
				for ( var i = 0; i < oL; i++) {
					el.cache[o[i].value] = i;
				}
			}
			if (typeof el.cache[v] == "undefined") {
				el.cache[v] = oL;
			}
			el.options[el.cache[v]] = option;
			if (sO) {
				option.selected = true;
			}
		};
		var select = name.get(0);
		name.find('option').remove().end().get(0).cache = null;
		for ( var i = 0, length = items.length; i < length; i++) {
			var item = items[i];
			var sel = item[2] || false;
			add(select, item[0], item[1], sel);
		}
	};
	/*(function() {
		var params = {};
		if (location.search.length) {
			$(location.search.substring(1).split('&')).each(function() {
				var tmp = this.split('=');
				if (tmp.length == 2) {
					params[tmp[0]] = tmp[1];
				}
			});
			if (typeof params.COSTMAX !== 'undefined') {
				$('#' + this).val(params[this]);
			}
		}
		params.action = 'init';
		$.jsonLoad(params, samo.repaint_form);
	})();*/
	$('#CHECKIN_BEG').datePicker().mask('39.19.2099');
	samo.datepicker(samo.el['CHECKIN_BEG'], $.parseJSON($('#samo_checkin_beg_data').text()));
	$('#TOWNFROMINC,#STATEINC').bind('change', function() {
		var params = {
			action : this.id.toLowerCase()
		};
		params.TOWNFROMINC = $('#TOWNFROMINC').val();
		params.STATEINC = $('#STATEINC').val();
		$.jsonLoad(params, samo.repaint_form);
	});
	samo.search = function() {
		var page = arguments.length ? arguments[0] : 1;
		var params = {
			DOLOAD : 1
		};
		$('#TOWNFROMINC,#STATEINC,#NIGHTS_TILL,#ADULT,#CHILD,#CURRENCYINC,#HOTELS,#COSTMAX').each(function() {
			params[this.id] = this.value;
		});
		params.NIGHTS_FROM = 1; //params.NIGHTS_TILL;
		params.FREIGHT = 1;
		params.STOPSALE = 1;
		$('#CHECKIN_BEG,#CHECKIN_END').each(function() {
			var tmp = this.value.split('.');
			params[this.id] = tmp[2] + tmp[1] + tmp[0];
		});
		var w = (window.top != self) ? window.top : self;
		w.location.href = samo.search_page + $.param(params);
	};
	$('#load').bind('click', function() {
		samo.search();
	}).bind('ajaxStart', function() {
		this.disabled = true;
	}).bind('ajaxStop', function() {
		this.disabled = false;
	}).removeAttr('disabled');
});
