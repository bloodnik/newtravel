<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arResult['NIGHTS_TILL'] = 14;
?>
<div class="samo_container" id="fast_search" samo:uid="<?=$arParams['SAMO_UID']?>" samo:datasrc="<?=$arParams['SAMO_PROXY']?>?" samo:search_page="<?=$arParams['SAMO_SEARCH_PAGE']?>?">
<table>
	<tbody>
	<tr>
		<td class="panel n1">
		<table>
			<tbody>
			<tr>
				<td width="20%">Вылет из</td>
				<td width="80%" colspan="3">
					<select autocomplete="off" name="TOWNFROMINC" id="TOWNFROMINC">
					<?foreach($arResult['TOWNFROM'] as $arItem):?>
						<option value="<?=$arItem[0]?>"<?=($arItem[2]==1?' selected="selected"':'')?>><?=$arItem[1]?></option>
					<?endforeach;?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Страна</td>
				<td colspan="3">
					<select autocomplete="off" name="STATEINC" id="STATEINC">
					<?foreach($arResult['STATE'] as $arItem):?>
						<option value="<?=$arItem[0]?>"<?=($arItem[2]==1?' selected="selected"':'')?>><?=$arItem[1]?></option>
					<?endforeach;?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Гостиница</td>
				<td colspan="3">
					<select autocomplete="off" name="HOTELS" id="HOTELS" style="">
						<option>- - - - - - - - - - </option>
					<?foreach($arResult['HOTELS'] as $arItem):?>
						<option value="<?=$arItem[0]?>"><?=trim(preg_replace('/\([^)]*\)/', '',$arItem[1]))?> <?=$arItem[3]?>*</option>
					<?endforeach;?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Начало поездки</td>
				<td class="l"><input autocomplete="off" type="text" class="date" id="CHECKIN_BEG" name="CHECKIN_BEG" value="<?=$arResult['CHECKIN_BEG'][0]?>"></td>
				<td class="r">Ночей</td>
				<td class="r">
					<select name="NIGHTS_TILL" class="spin" id="NIGHTS_TILL" autocomplete="off">
					<?foreach(range(1, 29) as $i):?>
						<option value="<?=$i?>"<?=($i==$arResult['NIGHTS_TILL']?' selected="selected"':'')?>><?=$i?></option>
					<?endforeach;?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Взрослых</td>
				<td class="l">
					<select name="ADULT" class="spin" id="ADULT" autocomplete="off">
					<?foreach(range(1, 9) as $i):?>
						<option value="<?=$i?>"<?=($i==$arResult['ADULT']?' selected="selected"':'')?>><?=$i?></option>
					<?endforeach;?>
					</select>
					</td>
				<td class="r">Детей</td>
				<td class="r"><select name="CHILD" class="spin" id="CHILD" autocomplete="off">
					<?foreach(range(0, 9) as $i):?>
						<option value="<?=$i?>"<?=($i==$arResult['CHILD']?' selected="selected"':'')?>><?=$i?></option>
					<?endforeach;?>
				</select></td>
			</tr>
			<tr>
				<td>Цена до</td>
				<td class="l"><input autocomplete="off" type="text" class="price" name="COSTMAX" id="COSTMAX" value="" /></td>
				<td class="r">Валюта</td>
				<td class="r">
					<select class="spin" id="CURRENCYINC" name="CURRENCYINC" autocomplete="off">
					<?foreach($arResult['CURRENCY'] as $arItem):?>
						<?if(in_array($arItem[0], $arParams["ALLOWED_CURRENCIES"])):?>
						<option value="<?=$arItem[0]?>"<?=($arItem[2]==1?' selected="selected"':'')?>><?=$arItem[1]?></option>
						<?endif;?>
					<?endforeach;?>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="4" class="submit"><button id="load">Искать</button></td>
			</tr>
			</tbody>
		</table>
		</td>
	</tr>
	</tbody>
</table>
<div id="samo_checkin_beg_data" style="display:none;"><?=json_encode($arResult['CHECKIN_BEG']);?></div>
<div id="samo_checkin_beg_data" style="display:none;"><?=$arResult['CHECKIN_BEG'];?></div>
</div>
<?//I($arResult)?>