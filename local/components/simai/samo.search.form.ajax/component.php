<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["ALLOWED_CURRENCIES"] = array(643, 840, 978); # 643 => RUB, 840 => USD, 978 => EUR

if($this->StartResultCache())
{
	if(!CModule::IncludeModule("iblock") || empty($arParams["SAMO_DATASRC"]) || empty($arParams["SAMO_UID"]))
		$this->AbortResultCache();

	$json = SamoSearchFormAjaxGetJson($arParams["SAMO_DATASRC"], array('action'=>'init', 'uid'=>$arParams["SAMO_UID"], 'callback'=>'jsonp'.rand(1000000000000,9997323126099)));
	if(!$arResult = SamoSearchFormAjaxDecodeJson($json))
		$this->AbortResultCache();
	
	# поиск страны прилета (если нету из Уфы, ищем в Москве)
	if(isset($arParams["HOTEL_ID"]))
	{
		$res = CIBlockElement::GetElementGroups($arParams["HOTEL_ID"], false);
		if($arItem = $res->GetNext(true, false))
		{
			$res = CIBlockSection::GetList(
				array("SORT"=>"ASC"),
				array(
					"IBLOCK_ID" => $arItem["IBLOCK_ID"],
					"<=LEFT_BORDER" => $arItem["LEFT_MARGIN"],
					">=RIGHT_BORDER" => $arItem["RIGHT_MARGIN"],
					"DEPTH_LEVEL" => 1
				),
				false,
				false,
				array("NAME")
			);
			if($arItem = $res->GetNext(true, false))
				$COUNTRY_NAME = $arItem["NAME"];
		}
	}
	elseif(isset($arParams["COUNTRY_NAME"]))
	{
		$COUNTRY_NAME = $arParams["COUNTRY_NAME"];
	}
	
	if(isset($arParams["HOTEL_ID"]) || isset($arParams["COUNTRY_NAME"]))
	{
		$bCountryFound = false;
		do {
			foreach($arResult["STATE"] as &$arItem)
				$arItem[2] = 0;
			foreach($arResult["STATE"] as &$arItem)
			{
				if($arItem[1] == $COUNTRY_NAME)
				{
					$arItem[2] = 1;
					$bCountryFound = true;
					break;
				}
			}
			if(!$bCountryFound)
			{
				$townFromID = 1; // Москва
				$arTownFrom = $arResult["TOWNFROM"];
				$json = SamoSearchFormAjaxGetJson($arParams["SAMO_DATASRC"], array('action'=>'townfrominc', 'TOWNFROMINC'=>$townFromID, 'uid'=>$arParams["SAMO_UID"], 'callback'=>'jsonp'.rand(1000000000000,9997323126099)));
				if(!$arResult = SamoSearchFormAjaxDecodeJson($json))
					$this->AbortResultCache();
				$arResult["TOWNFROM"] = $arTownFrom;
				foreach($arResult["STATE"] as &$arItem)
					$arItem[2] = 0;
				foreach($arResult["STATE"] as &$arItem)
					if($arItem[1] == $COUNTRY_NAME)
						$arItem[2] = 1;
				foreach($arResult["TOWNFROM"] as &$arItem)
					$arItem[2] = 0;
				foreach($arResult["TOWNFROM"] as &$arItem)
					if($arItem[0] == $townFromID)
						$arItem[2] = 1;
				$bCountryFound = true;
			}
		}
		while(!$bCountryFound);
	}

	if($this->InitComponentTemplate())
	{
		$template = &$this->GetTemplate();
		$arResult['TEMPLATE_PATH'] = $template->GetFolder();
		$this->ShowComponentTemplate();
	}
}
$APPLICATION->AddHeadScript($arResult['TEMPLATE_PATH']."/fast.js");
$APPLICATION->AddHeadScript($arResult['TEMPLATE_PATH']."/calendar.js");
?>
