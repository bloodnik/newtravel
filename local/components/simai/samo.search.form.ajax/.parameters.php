<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arComponentParameters = array(
	"GROUPS"=>array(
	),
	"PARAMETERS"=>array(
		"SAMO_DATASRC"=>Array(
			"PARENT"=>"BASE",
			"NAME"=>"Путь к серверу Андромеда (samo.ru)",
			"TYPE"=>"STRING",
			"DEFAULT"=>'http://search.samo.ru/search/index.php',
			"MULTIPLE"=>"N",
		),
		"SAMO_UID"=>Array(
			"PARENT" =>"BASE",
			"NAME"=>"ID в системе Андромеда (samo.ru)",
			"TYPE"=>"STRING",
			"DEFAULT"=>'',
			"MULTIPLE"=>"N",
		),
		"SAMO_SEARCH_PAGE"=>Array(
			"PARENT"=>"BASE",
			"NAME"=>"Страница вывода результата поиска",
			"TYPE"=>"STRING",
			"DEFAULT"=>'/tours/',
			"MULTIPLE"=>"N",
		),
		"SAMO_PROXY"=>Array(
			"PARENT"=>"BASE",
			"NAME"=>"Страница проксирующая данные с (samo.ru)",
			"TYPE"=>"STRING",
			"DEFAULT"=>'/samo.php',
			"MULTIPLE"=>"N",
		),
		"CACHE_TIME"=>Array("DEFAULT"=>36000000),
	),
);
?>
