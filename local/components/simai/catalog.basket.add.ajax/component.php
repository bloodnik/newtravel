<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arParams["ACTION_VARIABLE"]=trim($arParams["ACTION_VARIABLE"]);
if(strlen($arParams["ACTION_VARIABLE"])<=0|| !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["ACTION_VARIABLE"]))
	$arParams["ACTION_VARIABLE"] = "action";

$arParams["PRODUCT_ID_VARIABLE"]=trim($arParams["PRODUCT_ID_VARIABLE"]);
if(strlen($arParams["PRODUCT_ID_VARIABLE"])<=0|| !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_ID_VARIABLE"]))
	$arParams["PRODUCT_ID_VARIABLE"] = "id";

$arParams["PRODUCT_QUANTITY_VARIABLE"]=trim($arParams["PRODUCT_QUANTITY_VARIABLE"]);
if(strlen($arParams["PRODUCT_QUANTITY_VARIABLE"])<=0|| !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_QUANTITY_VARIABLE"]))
	$arParams["PRODUCT_QUANTITY_VARIABLE"] = "quantity";

$arParams["PRODUCT_PROPS_VARIABLE"]=trim($arParams["PRODUCT_PROPS_VARIABLE"]);
if(strlen($arParams["PRODUCT_PROPS_VARIABLE"])<=0|| !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_PROPS_VARIABLE"]))
	$arParams["PRODUCT_PROPS_VARIABLE"] = "prop";

if($arParams["AUTHORIZED_USERS_ONLY"] == "Y" && !$USER->IsAuthorized())
{
	echo $_REQUEST['callback'].'({message:"Только для авторизованных пользователей!",error:true})';
	return;
}

$strError = "";
if (array_key_exists($arParams["ACTION_VARIABLE"], $_REQUEST) && array_key_exists($arParams["PRODUCT_ID_VARIABLE"], $_REQUEST))
{
	if(array_key_exists($arParams["ACTION_VARIABLE"]."BUY", $_REQUEST))
		$action = "BUY";
	elseif(array_key_exists($arParams["ACTION_VARIABLE"]."ADD2BASKET", $_REQUEST))
		$action = "ADD2BASKET";
	else
		$action = strtoupper($_REQUEST[$arParams["ACTION_VARIABLE"]]);

	$productID = intval($_REQUEST[$arParams["PRODUCT_ID_VARIABLE"]]);
	if(($action == "ADD2BASKET" || $action == "BUY") && $productID > 0)
	{
		if(CModule::IncludeModule("iblock") && CModule::IncludeModule("sale") && CModule::IncludeModule("catalog"))
		{
			if($arParams["USE_PRODUCT_QUANTITY"])
				$QUANTITY = intval($_POST[$arParams["PRODUCT_QUANTITY_VARIABLE"]]);
			if($QUANTITY <= 1)
				$QUANTITY = 1;

			$product_properties = array();
			if(count($arParams["PRODUCT_PROPERTIES"]))
			{
				if(is_array($_POST[$arParams["PRODUCT_PROPS_VARIABLE"]]))
				{
					$product_properties = CIBlockPriceTools::CheckProductProperties(
						$arParams["IBLOCK_ID"],
						$productID,
						$arParams["PRODUCT_PROPERTIES"],
						$_POST[$arParams["PRODUCT_PROPS_VARIABLE"]]
					);
					if(!is_array($product_properties))
						$strError = GetMessage("CATALOG_ERROR2BASKET").".";
				}
				else
					$strError = GetMessage("CATALOG_ERROR2BASKET").".";
			}
			$res = CPrice::GetList(array(), array(
				"PRODUCT_ID" => $productID,
				"CATALOG_GROUP_ID" => 1, # BASE
			));
			if($arProduct = $res->Fetch())
			{
				$res = CIBlockElement::GetList(
					array(),
					array("ID"=>$productID, "IBLOCK_ID"=>17),
					false,
					false,
					array("*","PROPERTY_HTPLACE")
				);
				if($arElement = $res->GetNext(false, true))
				{
					$res = CSaleBasket::GetList(
						array("NAME"=>"ASC","ID"=>"ASC"),
						array(
							"FUSER_ID"=>CSaleBasket::GetBasketUserID(),
							"LID"=>SITE_ID,
							"ORDER_ID"=>"NULL",
							"PRODUCT_ID"=>$productID,
						),
						false,
						false,
						array("ID")
					);
					if(!$res->Fetch())
					{
						if($arFlyFee = GetFlyFee($productID))
						{
							# Расчитываем кол-во людей в путевке
							$rsHtplace = CIBlockElement::GetByID($arElement["PROPERTY_HTPLACE_VALUE"]);
							if($arHtplace = $rsHtplace->GetNext())
							{
								# кол-во взрослых
								$adultCount = 0;
								$infantCount = 0;
								if(strpos($arHtplace["NAME"], "SNGL") === 0)
									$adultCount = 1;
								elseif(strpos($arHtplace["NAME"], "DBL") === 0 || strpos($arHtplace["NAME"], "TWN") === 0)
									$adultCount = 2;
								elseif(strpos($arHtplace["NAME"], "TRPL") === 0)
									$adultCount = 3;
								elseif(strpos($arHtplace["NAME"], "QDPL") === 0)
									$adultCount = 4;
								# кол-во детей
								if(strpos($arHtplace["NAME"], "+CHD") > 0)
									$adultCount += 1;
								elseif(strpos($arHtplace["NAME"], "+2CHD") > 0)
									$adultCount += 2;
								elseif(strpos($arHtplace["NAME"], "+3CHD") > 0)
									$adultCount += 3;
								# допкравать
								elseif(strpos($arHtplace["NAME"], "+EXB") > 0)
								{
									if(isset($_REQUEST["exb"]))
									{
										if($_REQUEST["exb"] == "infant")
											$infantCount++;
										elseif($_REQUEST["exb"] == "adult")
											$adultCount++;
									}
									else
									{
										echo $_REQUEST['callback'].'({message:"Дополнительная кровать предназначена для ребенка до 2-x лет?",error:false,exb:true,infant:'.IntVal($_REQUEST["infant"]).'})';
										return;
									}
								}
								if(isset($_REQUEST["infant"]) && IntVal($_REQUEST["infant"])>0)
								{
									$infantCount += IntVal($_REQUEST["infant"]);
								}
							}
							$fuel = CCurrencyRates::ConvertCurrency($arFlyFee["FUEL"], $arFlyFee["FUEL_CURRENCY"], $arProduct["CURRENCY"]);
							$infant = CCurrencyRates::ConvertCurrency($arFlyFee["INFANT"], $arFlyFee["INFANT_CURRENCY"], $arProduct["CURRENCY"]);
							$arProduct["PRICE"] = $arProduct["PRICE"] + $adultCount*$fuel + $infantCount*$infant;
						}
						$arFields = array(
							"PRODUCT_ID"=>$productID,
							"PRICE"=>$arProduct["PRICE"],
							"CURRENCY"=>$arProduct["CURRENCY"],
							"QUANTITY"=>$QUANTITY,
							"LID"=>SITE_ID,
							"DELAY"=>"Y",
							"NAME"=>$arElement["NAME"],
							"PROPS"=>$product_properties,
						);
						if(!CSaleBasket::Add($arFields))
							$strError = "Ошибка: не могу добавить путевку в корзину.";
					}
					else
						$strError = "Вы уже добавили эту путевку в корзину.";
				}
				else
					$strError = "Ошибка: путевка не найдена в каталоге.";
			}
			else
				$strError = "Ошибка: цена путевки не найдена.";

			if(!strlen($strError)>0)
			{
				echo $_REQUEST['callback'].'({message:"Путевка добавлена в корзину!",error:false})';
				return;
			}
			elseif($ex = $GLOBALS["APPLICATION"]->GetException())
				$strError .= "\n".str_replace(array('Product is run out'), array('Путевка уже продана'),$ex->GetString());
		}
	}
	else
	{
		if(!$productID > 0)
			$strError = "Ошибка: ID путевки где-то потерялся.";
	}

}
if(strlen($strError)>0)
{
	echo $_REQUEST['callback'].'({message:"'.$strError.'",error:true})';
	return;
}
?>
