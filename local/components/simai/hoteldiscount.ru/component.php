<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

if(!strlen($arParams["PARSE_URL"])>0)
	return;

$url = trim(rtrim($arParams["PARSE_URL"], ' /'));
$uri = $APPLICATION->GetCurUri();
if(strpos($uri, '/hotels/qr.aspx') === 0)
	$url .= substr($uri, strlen('/hotels'));

if($this->StartResultCache(false, array($url)))
{
	$html = file_get_contents($url);
	if($html)
	{
		$str = iconv($arParams["CODING"], SITE_CHARSET, $html);
		$arResult = preg_replace('/.*?<body.*?>|<\/body>.*?/', '', $str);
	}
	$this->IncludeComponentTemplate();
}
?>
