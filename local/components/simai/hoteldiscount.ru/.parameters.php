<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"PARSE_URL" => array(
			"PARENT" => "BASE",
			"NAME" => "Адрес URL для парсинга (http://8177.hoteldiscount.ru)",
			"TYPE" => "STRING",
			"DEFAULT" => 'http://8177.hoteldiscount.ru',
		),
		"CODING" => array(
			"PARENT" => "BASE",
			"NAME" => "Кодировка сайта",
			"TYPE" => "LIST",
			"VALUES" => array("UTF-8"=>"UTF-8", "Windwos-1251"=>"Windwos-1251"),
			"DEFAULT" => "Windwos-1251"
		),
		"CACHE_TIME" => array("DEFAULT"=>3600),
	)
);
?>
