<?
/**********************************
*                                 *
*    Компонент разработан Blair   *
*    Simai Studio                 *
*    http://simai.ru/             *
*                                 *
*  Голосование в фотогалерее      *
*                                 *
**********************************/

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
  "NAME" => "Голосование в фотоальбомах",
  "DESCRIPTION" => "Голосование пользователями за участников фотоконкурса",
	"ICON" => "/images/icon.gif",
	"COMPLEX" => "N",
	"PATH" => array(
		"ID" => "Студия «Симай»",
		"CHILD" => array(
			"ID" => "photo.view",
			"NAME" => "Голосование в фотоконкурсах"
		)
	),
);

?>