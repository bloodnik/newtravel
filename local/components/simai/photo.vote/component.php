<?
/**********************************
*                                 *
*    Компонент разработан Blair   *
*    Simai Studio                 *
*    http://simai.ru/             *
*                                 *
*  Голосование в фотогалерее      *
*                                 *
**********************************/

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule("iblock");
CModule::IncludeModule("forum");
global $USER;
$arResult = array();



// НАЧАЛО ПЕРЕОПРЕДЕЛЕНИЯ ПАРАМЕТРОВ ДЛЯ ИНФОМАМЫ

// Определяем id фотоальбома
//$url = explode("/", getenv("REQUEST_URI"));
//if ($url[3] == "") return false; // если неправильный url то вырубаемся
//$arParams["SECTION"] = $url[3];

// Все знают что я думаю про битрикс, поэтому просто сделаем так
$arParams["SECTION"] = $_GET["section"];

// Определяем название фотоальбома
$res = CIBlockSection::GetByID($arParams["SECTION"]);
if($ar_res = $res->GetNext())
  $album_name = $ar_res['NAME'];
else
  $album_name = "";

// print_r($arParams);
  
// Проверяем что этот фотоальбом находится в "текущих голосованиях"
$res = CIBlockSection::GetByID($arParams["SECTION"]);
if($ar_res = $res->GetNext() AND $ar_res["IBLOCK_SECTION_ID"] == "22") {  // Теперь проверяем дату
  $arUserFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_".$ar_res["IBLOCK_ID"]."_SECTION", $arParams["SECTION"]);

  $cur_date = date("Ymd");
  $UF_DATE = preg_replace("|([\d]+)\.([\d]+)\.([\d]+|)", "\\3\\2\\1", $arUserFields["UF_DATE"]["VALUE"]);
  $UF_PRIEM = preg_replace("|([\d]+)\.([\d]+)\.([\d]+)|", "\\3\\2\\1", $arUserFields["UF_PRIEM"]["VALUE"]);
  $UF_CLOSE = preg_replace("|([\d]+)\.([\d]+)\.([\d]+)|", "\\3\\2\\1", $arUserFields["UF_CLOSE"]["VALUE"]);

  // Если какая-то дата не задана то вырубаемся
  if ($arUserFields["UF_DATE"]["VALUE"] == "" OR $arUserFields["UF_PRIEM"]["VALUE"] == ""
    OR $arUserFields["UF_CLOSE"]["VALUE"] == "") return false;

  // Если еще идет прием голосов то не разрешаем голосование и запрещаем показ результатов
  if ($cur_date < $UF_PRIEM) {    $arParams["VOTE_END"] = "Y";
    $arParams["SHOW"] = "never";
    $APPLICATION->SetTitle(str_ireplace("{ALBUM_NAME}", $album_name, $arParams["VOTE_PRIEM"]));
    return false;
  }
  elseif ($cur_date < $UF_CLOSE) {    $APPLICATION->SetTitle(str_ireplace("{ALBUM_NAME}", $album_name, $arParams["VOTE_VOTE"]));    $arParams["VOTE_END"] = "N";
  }
  // Если время вышло то показывает результаты
  elseif ($cur_date >= $UF_CLOSE) {    $APPLICATION->SetTitle(str_ireplace("{ALBUM_NAME}", $album_name, $arParams["VOTE_CLOSE"]));    $arParams["VOTE_END"] = "Y";
  }
  // Если что-то где-то глючит
  // Значит все пришел звездец
  else return false;



}
else {
  $APPLICATION->SetTitle(str_ireplace("{ALBUM_NAME}", $album_name, $arParams["VOTE_ALBUM_ERROR"]));  return false;
}




// -КОНЕЦ ПЕРЕОПРЕДЕЛЕНИЯ ПАРАМЕТРОВ ДЛЯ ИНФОМАМЫ










// Проверяем корректность входных условий
if ($arParams["IBLOCK_ID"] == "" OR !isset($arParams["SECTION"]) OR count($arParams["ACCOUNT"]) < 1 OR count($arParams["FORUM"]) < 1 OR count($arParams["COUNT"]) < 1 OR !isset($arParams["VOTE_END"]) OR !isset($arParams["REVOTE"]) OR !isset($arParams["SHOW"]) OR !isset($arParams["COUNT_ALL"])) {  echo "Неверно заданы настройки компонента<br>\r\n";
  var_dump($arParams);
  return false;
}


// Автоматическое создание инфоблока с нужной конфигурацией
if (is_file(dirname(__FILE__)."/auto_install.php")) require_once(dirname(__FILE__)."/auto_install.php");
// ОТТУДА ПОЯВЛЯЕТСЯ ПЕРЕМЕННАЯ $arParams["IBLOCK_VOTE_ID"] СОДЕРЖАЩАЯ ИДЕНТИФИКАТОР ИНФОБЛОКА C ГОЛОСОВАНИЕМ

// Инфоблок голосования
//$arParams["IBLOCK_VOTE_ID"] = 66;





if ($USER->IsAuthorized() == false) {
  echo "Необходима авторизация";
  return false;
}



// Определяем за кого голосовал текущий пользователь
$arResult["user_vote"] = cur_user_vote ($arParams["IBLOCK_VOTE_ID"], $arParams["SECTION"], $USER->GetID());


// Проверка может ли пользователь голосовать
if ($arParams["VOTE_END"] == "Y" OR (count($arResult["user_vote"]) > 0 AND $arParams["REVOTE"] == "N")) {  $arResult["can_vote"] = false;
}
else {  $arResult["can_vote"] = true;
}

// Проверяем время жизни аккаунта
$add_field = CUser::GetByID($USER->GetID());
$add_field = $add_field->GetNext();
preg_match("#([\d]+)\.([\d]+)\.([\d]+) ([\d]+):([\d]+):([\d]+)#", $add_field["DATE_REGISTER"], $date);

$time = mktime ($date[4] , $date[5], $date[6], $date[2], $date[1], $date[3]);
$day = (time() - $time)/60/60/24;
if ($day < $arParams["ACCOUNT"]) $arResult["can_vote"] = false;

// Проверяем кол-во сообщений на форуме
if ($arParams["FORUM"] > 0) {
  $forum_count = 0;
  $db_res = CForumMessage::GetList(array("ID"=>"ASC"), array("AUTHOR_ID"=>$USER->GetID()));
  while ($ar_res = $db_res->Fetch())
  {    $forum_count++;    if ($forum_count > $arParams["FORUM"]) break;
  }

  // Если сообщений на форуме меньше нужного то неразрешаем юзеру голосовать
  if ($forum_count < $arParams["FORUM"]) $arResult["can_vote"] = false;
}


// Проверка можно ли показывать результаты
$arResult["show_vote"] = true;
if ($arParams["SHOW"] == "never") {  $arResult["show_vote"] = false;
}
if ($arParams["SHOW"] == "vote" AND count($arResult["user_vote"]) == 0  AND $arParams["VOTE_END"] == "N") {  $arResult["show_vote"] = false;
}
if ($arParams["SHOW"] == "close" AND $arParams["VOTE_END"] == "N") {  $arResult["show_vote"] = false;
}







// Реализация голосования
if (isset($_POST["vote"]) AND $arResult["can_vote"] == true) {  $arr_vote = array(); // массив id с голосованиями
  // Перебираем все переданные данные  foreach ($_POST as $key => $value) {    // Определяем является ли это галочкой в голосовании    if (strtolower($value) == "on" AND intval($key) == $key) {      $arr_vote[] = intval($key);
    }
  }

  // Проверяем с кол-вом нужных голосов
  if (count($arr_vote) == $arParams["COUNT"] OR (count($arr_vote) < $arParams["COUNT"] AND $arParams["COUNT_ALL"] == "N" AND count($arr_vote) > 0)) {    // Удаляем его голоса из этого фотоальбома
    foreach ($arResult["user_vote"] as $el_id) {      CIBlockElement::Delete($el_id);
    }

    // Вставляем голосование
    foreach ($arr_vote as $photo_id) {
      // Определяем название фотоальбома
      $res = CIBlockSection::GetByID($arParams["SECTION"]);
      if($ar_res = $res->GetNext())
        $album_name = $ar_res['NAME'];
      $el = new CIBlockElement;

      $PROP = array();
      $PROP["user_id"] = $USER->GetID();
      $PROP["album_id"] = $arParams["SECTION"];
      $PROP["photo_id"] = $photo_id;
      $PROP["ip"] = $_SERVER["REMOTE_ADDR"];
      $PROP["HTTP_X_FORWARDED_FOR"] = isset($_SERVER["HTTP_X_FORWARDED_FOR"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : getenv("HTTP_X_FORWARDED_FOR");
      $PROP["X_REAL_IP"] = isset($_SERVER["X_REAL_IP"]) ? $_SERVER["X_REAL_IP"] : getenv("X_REAL_IP");

      $arLoadProductArray = Array(
        "MODIFIED_BY"    => $USER->GetID(),
        "IBLOCK_SECTION" => false,
        "IBLOCK_ID"      => $arParams["IBLOCK_VOTE_ID"],
        "PROPERTY_VALUES"=> $PROP,
        "NAME"           => $album_name." - ".$USER->GetLogin(),
        "ACTIVE"         => "Y"
      );

      $el = $el->Add($arLoadProductArray);
    }

    // Заного определяем за кого проголосовал пользователь
    $arResult["user_vote"] = cur_user_vote ($arParams["IBLOCK_VOTE_ID"], $arParams["SECTION"], $USER->GetID());

    // Если юзер проголосовал и нельзя переголосовывать/нужно показать результаты то меняем эти параметры
    if ($arParams["REVOTE"] == "N") $arResult["can_vote"] = false;
    if ($arParams["SHOW"] == "vote") $arResult["show_vote"] = true;
    $arResult["message"] = $arParams["VOTE_USER_VOTE"];


  }
  elseif ($arParams["COUNT_ALL"] == "Y") {    $arResult["error"] = "Вы должны выбрать ".$arParams["COUNT"]." участника для голосования.";
  }
  else {    $arResult["error"] = "Вы должны выбрать от 1 до ".$arParams["COUNT"]." участников для голосования.";
  }



}







//
// ОТОБРАЖЕНИЕ ИНФОРМАЦИИЫ
//


// Определяем название фотоальбома
$res = CIBlockSection::GetByID($arParams["SECTION"]);
$ar_res = $res->GetNext();
$arResult["section"] = $ar_res["NAME"];


// Определяем все голоса пользователей в этих фотоальбомах
$vote = array(); // Список фотографий
$arFilter = Array(
  "IBLOCK_ID"         => $arParams["IBLOCK_VOTE_ID"],
  "PROPERTY_album_id" => $arParams["SECTION"]
);
$arSelected = Array("IBLOCK_ID", "PROPERTY_PHOTO_ID", "SECTION");
//$res = CIBlockElement::GetList(Array("SORT"=>"ASC", "NAME"=>"ASC"), $arFilter, Array("PROPERTY_PHOTO_ID"), false, $arSelected);
$res = CIBlockElement::GetList(Array("SORT"=>"ASC", "NAME"=>"ASC"), $arFilter, false, false, $arSelected);
while ($ar_fields = $res->GetNext()) {//  $vote[$ar_fields["PROPERTY_PHOTO_ID_VALUE"]] = $ar_fields["CNT"];
  if (isset($vote[$ar_fields["PROPERTY_PHOTO_ID_VALUE"]]))  $vote[$ar_fields["PROPERTY_PHOTO_ID_VALUE"]]++;
  else  $vote[$ar_fields["PROPERTY_PHOTO_ID_VALUE"]] = 1;
}




// Составляем список всех фотографий пользователя
$arResult["photo"] = array(); // Список фотографий
$arFilter = Array(
  "IBLOCK_ID"=>$arParams["IBLOCK_ID"],
  "SECTION_ID"=>$arParams["SECTION"],
  "INCLUDE_SUBSECTIONS"=>"N",
);
$arSelected = Array("ID", "SORT", "IBLOCK_ID", "SECTION_ID", "CREATED_BY", "PROPERTY_REAL_PICTURE", "PREVIEW_PICTURE", "PREVIEW_TEXT", "DETAIL_PICTURE", "DETAIL_TEXT", "NAME", "IBLOCK_SECTION_ID", "DATE_CREATE");
$res = CIBlockElement::GetList(Array("SORT"=>"ASC", "DATE_CREATE"=>"DESC", "NAME"=>"ASC"), $arFilter, false, false, $arSelected);
while ($ar_fields = $res->GetNext()) {
  // Кол-во голосов за этого участника  if (isset($vote[$ar_fields["ID"]])) $cur_vote = $vote[$ar_fields["ID"]];
  else $cur_vote = 0;

  // Определяем голосовал ли за этого участиника пользователь
  if (isset($arResult["user_vote"][$ar_fields["ID"]])) $user_vote = 1;
  else $user_vote = 0;

  $arResult["photo"][] = Array(
    "id" => $ar_fields["ID"],
    "name" => $ar_fields["NAME"],
    "preview" => $ar_fields["PREVIEW_PICTURE"],
    "picture" => $ar_fields["PROPERTY_REAL_PICTURE_VALUE"],
    "text" => $ar_fields["~PREVIEW_TEXT"],
    "vote" => $cur_vote, // Кол-во голосов за этого участника
    "user_vote" => $user_vote, // Показатель что текущий юзер голосовал за этого участника
    "created_by" => $ar_fields["CREATED_BY"]
  );
}



// Если юзеру надо показывать результаты голосования то выдаем их в соответствии с кол-вом голосов
if ($arResult["show_vote"] == true) {  $arr = $arResult["photo"];  $size = count($arResult["photo"])-1;
  for ($i = $size; $i>=0; $i--) {
    for ($j = 0; $j<=($i-1); $j++)
      if ($arr[$j]["vote"]<$arr[$j+1]["vote"]) {
        $k = $arr[$j];
        $arr[$j] = $arr[$j+1];
        $arr[$j+1] = $k;
    }
  }
  $arResult["photo"] = $arr;
}


// Передаем в шаблон кол-во фоток которые надо выбирать
$arResult["count"] = $arParams["COUNT"];
$arResult["count_all"] = $arParams["COUNT_ALL"];




$this->IncludeComponentTemplate();





// ФУНКЦИОНАЛЬНАЯ ЧАСТЬ

// Определяем за кого голосовал текущий пользовательfunction cur_user_vote ($iblock_id, $section_id, $user_id) {  $arResult["user_vote"] = array();  $arFilter = Array(
    "IBLOCK_ID"=>$iblock_id,
    "PROPERTY_USER_ID"=>$user_id,
    "PROPERTY_ALBUM_ID"=>$section_id
  );
  $arSelected = Array("ID", "SORT", "IBLOCK_ID", "SECTION_ID", "CREATED_BY", "PROPERTY_ALBUM_ID",
  "PROPERTY_USER_ID", "PROPERTY_PHOTO_ID", "PREVIEW_PICTURE", "PREVIEW_TEXT", "DETAIL_PICTURE",
  "DETAIL_TEXT", "NAME", "IBLOCK_SECTION_ID");
  $res = CIBlockElement::GetList(Array("SORT"=>"ASC", "DATE_CREATE"=>"DESC", "NAME"=>"ASC"), $arFilter, false, false, $arSelected);
  while ($ar_fields = $res->GetNext()) {
    $arResult["user_vote"][$ar_fields["PROPERTY_PHOTO_ID_VALUE"]] = $ar_fields["ID"];
  }
  return $arResult["user_vote"];
}


?>
