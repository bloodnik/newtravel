<?
/**********************************
*                                 *
*    Компонент разработан Blair   *
*    Simai Studio                 *
*    http://simai.ru/             *
*                                 *
*  Голосование в фотогалерее      *
*                                 *
**********************************/

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


CModule::IncludeModule("iblock");


// многобуков - выбор блока для засылки космических сообщений с марса
$arTypesEx = Array("-"=>" ");
$db_iblock_type = CIBlockType::GetList(Array("SORT"=>"ASC"));
while($arRes = $db_iblock_type->Fetch())
  if($arIBType = CIBlockType::GetByIDLang($arRes["ID"], LANG))
    $arTypesEx[$arRes["ID"]] = $arIBType["NAME"];

$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
  $arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$arSorts = Array("ASC"=>GetMessage("T_IBLOCK_DESC_ASC"), "DESC"=>GetMessage("T_IBLOCK_DESC_DESC"));
$arSortFields = Array(
		"ID"=>GetMessage("T_IBLOCK_DESC_FID"),
		"NAME"=>GetMessage("T_IBLOCK_DESC_FNAME"),
		"ACTIVE_FROM"=>GetMessage("T_IBLOCK_DESC_FACT"),
		"SORT"=>GetMessage("T_IBLOCK_DESC_FSORT"),
		"TIMESTAMP_X"=>GetMessage("T_IBLOCK_DESC_FTSAMP")
	);

$arProperty_LNS = array();
$rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(isset($arCurrentValues["IBLOCK_ID"])?$arCurrentValues["IBLOCK_ID"]:$arCurrentValues["ID"])));
while ($arr=$rsProp->Fetch())
{
	$arProperty[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S")))
	{
		$arProperty_LNS[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	}
}
// - конец многобуков



// Список возможных сортировок
$show = array (
  "never" => "Никогда не показывать",
  "vote"  => "Показывать после того как проголосовал",
  "close" => "При завершении голосования",
  "ever"  => "Всегда показывать",
);


// Составляем список секций
$sections = array();
if ($arCurrentValues["IBLOCK_ID"] != "") {
  $sections = get_all_sections($arCurrentValues["IBLOCK_ID"]);
}


$arComponentParameters = array(
  "GROUPS" => array(
     "GENERAL" => array(
        "NAME" => "Общие настройки компонента"
     ),
     "VOTE" => array(
        "NAME" => "Настройки голосования"
     ),
     "MESSAGE" => array(
        "NAME" => "Текстовые сообщения"
     ),
  ),

  "PARAMETERS" => array(

      // Общие настройки
      "IBLOCK_TYPE" => Array(
        "PARENT" => "GENERAL",
        "NAME" => "Тип информационного блока",
        "TYPE" => "LIST",
        "VALUES" => $arTypesEx,
        "REFRESH" => "Y",
      ),
      "IBLOCK_ID" => Array(
        "PARENT" => "GENERAL",
        "NAME" => "Код информационного блока",
        "TYPE" => "LIST",
        "VALUES" => $arIBlocks,
        "ADDITIONAL_VALUES" => "Y",
        "REFRESH" => "Y",
      ),
      "SECTION" => Array(
        "PARENT" => "GENERAL",
        "NAME" => "Фотоальбом для голосования",
        "TYPE" => "LIST",
        "VALUES" => $sections,
      ),


      "ACCOUNT" => Array(
        "PARENT" => "VOTE",
        "NAME" => "Срок аккаунта для голосования (дней)",
        "TYPE" => "INTEGER",
        "DEFAULT" => 15,
      ),
      "FORUM" => Array(
        "PARENT" => "VOTE",
        "NAME" => "Кол-во сообщений на форуме для голосования",
        "TYPE" => "INTEGER",
        "DEFAULT" => 15,
      ),
      "COUNT" => Array(
        "PARENT" => "VOTE",
        "NAME" => "Кол-во выбираемых фотогорафий для голосования",
        "TYPE" => "INTEGER",
        "DEFAULT" => 3,
      ),
      "COUNT_ALL" => Array(
        "PARENT" => "VOTE",
        "NAME" => "Обязательно выбирать указанное кол-во фоток",
        "TYPE" => "checkbox",
        "DEFAULT" => "N",
      ),
      "VOTE_END" => Array(
        "PARENT" => "VOTE",
        "NAME" => "Завершить голосование",
        "TYPE" => "checkbox",
        "DEFAULT" => "N",
      ),
      "REVOTE" => Array(
        "PARENT" => "VOTE",
        "NAME" => "Может ли пользователь переголосовывать",
        "TYPE" => "checkbox",
        "DEFAULT" => "N",
      ),
      "SHOW" => Array(
        "PARENT" => "VOTE",
        "NAME" => "Когда показывать результаты",
        "TYPE" => "LIST",
        "DEFAULT" => "vote",
        "VALUES" => $show,
      ),


      "VOTE_ALBUM_ERROR" => Array(
        "PARENT" => "MESSAGE",
        "NAME" => "Выбран несуществующий фотоконкурс для голосования",
        "TYPE" => "STRING",
        "DEFAULT" => "Выбран несуществующий фотоконкурс.",
      ),
      "VOTE_USER_VOTE" => Array(
        "PARENT" => "MESSAGE",
        "NAME" => "Пользователь успешно проголосовал",
        "TYPE" => "STRING",
        "DEFAULT" => "Ваш голос был учтен",
      ),
      "VOTE_PRIEM" => Array(
        "PARENT" => "MESSAGE",
        "NAME" => "Статус фтоконкурса - прием фотографий",
        "TYPE" => "STRING",
        "DEFAULT" => "Голосовать нельзя, идет прием фотографий",
      ),
      "VOTE_VOTE" => Array(
        "PARENT" => "MESSAGE",
        "NAME" => "Статус фотоконкурса - прием голосов",
        "TYPE" => "STRING",
        "DEFAULT" => "Голосование в фотоконкурсе",
      ),
      "VOTE_CLOSE" => Array(
        "PARENT" => "MESSAGE",
        "NAME" => "Статус фотоконкурса - голосование завершено",
        "TYPE" => "STRING",
        "DEFAULT" => "Просмотр результатов голосования",
      ),



  )
);



// Функция для определения списка секций
function get_all_sections ($IBLOCK_ID, $parent=0, $level=0) {

  $sections = array();
  if ($level == 0) $sections[0] = "--Родительская категория--";

  $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'GLOBAL_ACTIVE'=>'Y', 'SECTION_ID'=>$parent);
  $db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, false);
  while($ar_result = $db_list->GetNext())
  {

    $key = $ar_result['ID'];
    $ar_result['NAME'] = str_replace("&quot;", "\"", $ar_result['NAME']);
    if (strlen($ar_result['NAME']) > 35) $ar_result['NAME'] = substr($ar_result['NAME'], 0, 32)."...";
    $sections[$key] = str_repeat("..", $level).$ar_result['NAME'];

    $next_level = $level + 1;
    $sub_sections = get_all_sections ($IBLOCK_ID, $ar_result['ID'], $next_level);
    foreach ($sub_sections as $k => $v) {
      $sections[$k] = $v;
    }
    //$sections = array_merge($sections, $sub_sections); // не работает почемуто
  }

  return $sections;
}
?>