<?
/**********************************
*                                 *
*    Компонент разработан Blair   *
*    Simai Studio                 *
*    http://simai.ru/             *
*                                 *
*  Голосование в фотогалерее      *
*                                 *
**********************************/

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule("form");

//
// Описание переменных результата
//
//  $arResult["count"] - кол-во сообщений для голосования
//  $arResult["count_all"] - [Y/N] показывает обязательно ли выбирать все фотки
//
//  $arResult["album"] - название фотоальбома
//
//  $arResult["error"] - ошибка при голосовании (например, не все выбрано)
//  $arResult["message"] - сообщение что пользователь проголосовал
//
//  $arResult["can_vote"] - [true/false] показатель что пользователь может голосовать
//  $arResult["show_vote"] - [true/false] показатель что пользователю надо показывать результаты голосования
//
//  $arResult["photo"] - массив фотографий
//    $photo["user_vote"] - 0 юзер не голосовал за эту фотку, 1 голосовал
//    $photo["vote"]      - кол-во голосов за эту фотографию
//    $photo["name"]      - название фотки
//    $photo["text"]      - описание фотки
//    $photo["preview"]   - фотография превьюшки фотки
//    $photo["picture"]   - исходная фотография

// Кол-во столбцов
$count_row = 5;



//
// Далее идет сам шаблон
//

if (isset($arResult["message"])) echo "<b><font style=\"color:green\">".$arResult["message"]."</font></b><br>";
if (isset($arResult["error"])) echo "<b><font style=\"color:red\">".$arResult["error"]."</font></b><br>";

global $USER;

/*
foreach ($arResult["photo"] as $key => $value) {

}
*/

?>
<script type="text/javascript">
function vote_js (id, value) {
  if (value == false) {
    document.getElementById(id).style.background = "#FFFFFF";
  }
  else {
    document.getElementById(id).style.background = "#C7FFA0";
  }

}


function get_elements_from_form() {
        form = document.getElementById('vote');
        var elements = [];
        var current_el;
        var count = 0;
        var radio_groups = {}; // Эта переменная служит для учета групп радиокнопок
        for (var i = 0; i < form.length; i++) {
                var temp_el = form[i];
                switch(temp_el.type) {
                        case "radio" :
                                if (temp_el.checked) {
                                        radio_groups[temp_el.name] = '[filled]'; // помечаем группу
                                        current_el = {name: temp_el.name, type: 'radio', value: temp_el.value};
                                } else {
                                        if (!radio_groups[temp_el.name]) { // если группа еще не встречалась
                                                radio_groups[temp_el.name] = '[no elelements checked]';
                                        }
                                        continue;
                                }
                                break;
                        case 'checkbox' :
                                if (temp_el.checked) {
                                        count++;
                                        current_el = {name: temp_el.name, type: 'checkbox', value: temp_el.value};
                                } else {
                                        current_el = {name: temp_el.name, type: 'checkbox', value: '[not checked]'};
                                }
                                break;
                        default:
                                current_el = {name: temp_el.name,  type:temp_el.type, value: temp_el.value};
                                break;
                }
                elements.push(current_el);
        }
        // Отдельная обработка всех radio groups
        for (var rg in radio_groups) {
                if (radio_groups[rg] != '[filled]') { //добавляем группу, в которой нет выбранных кнопок
                        elements.push({name: rg, type: 'radio', value: radio_groups[rg]});
                }
        }
        return count;
}

function check_vote() {
  count = get_elements_from_form();
  var need_count = <?php echo $arResult["count"]; ?>;

<?php if ($arResult["count_all"] == "Y") {?>
  if (count != need_count) {
    alert('Необходимо выбрать ' + need_count + ' фотографий');
    return false;
  }
<?php } else { ?>
  if (count > need_count || count == 0) {
    alert('Необходимо выбрать от 1 до ' + need_count + ' фотографий');
    return false;
  }
<?php } ?>
  return true;
}

function check_max() {
  count = get_elements_from_form();
  var need_count = <?php echo $arResult["count"]; ?>;

  if (count > need_count) {
    alert('Не более ' + need_count + ' фотографий ');
    return false;
  }

  return true;

}


// Проверяет что пользователь голосует не за свои фотографии
function check_self(photo) {
  var arr_photo = new Array(<?php
$listSelfPhoto = "0, ";
foreach ($arResult["photo"] as $key => $photo) {
  if ($photo["created_by"] == $USER->GetID()) {
    $listSelfPhoto .= $photo["id"].", ";
  }
}
if (strlen($listSelfPhoto) > 0) $listSelfPhoto = substr($listSelfPhoto, 0, -2);
echo $listSelfPhoto; ?>);
  var length = arr_photo.length;
  for(var i=0; i<length; i++) {
    if (arr_photo[i] == photo) {
      alert('Нельзя голосовать за свои фотографии');
      return false;
    }
  }

  return true;
}



</script>
<?php

  if ($arResult["show_vote"] == true) {
   echo "<p>[!] Цветом обозначены фотографии за которые вы проголосовали.</p>";
  }
  echo "<form action=\"\" method=\"POST\" name=\"vote\" id=\"vote\">\r\n";
  echo "<input type=\"hidden\" name=\"vote\" value=\"vote\">";
  echo "<table border=\"0\" cellpadding=\"5\" cellspacing=\"2\" width='100%'><tr>\r\n";

  $i = 0; //$j =0;
//var_dump($arResult["photo"]);
  foreach ($arResult["photo"] as $photo) {
    //var_dump($photo);
   // $j++; if ($i > 4) die();
    $i++;
    if ($i > $count_row) {$i=1; echo "</tr><tr>";}

    echo "\t<td class='vote_element' valign='top' align='center' width=\"20%\" id=\"vote_".$photo["id"]."\" ";
//    ".$photo["id"]."

    // Определяем стиль ячейки если юзер проголосовал за фотку внутри ее
    if ($photo["user_vote"] > 0) echo " bgcolor=\"#C7FFA0\" ";
    else echo "  bgcolor=\"white\" ";

    echo ">
    <table cellspacing='3' cellpadding=\"3\" ><tr><td class='vote_photo' align='center'><img src=\"".SimaiImageFixedSize($photo["picture"],100,100)."\" border=\"0\"></td></tr><tr><td class='vote_checkbox'>";
    if ($arResult["can_vote"] == true) {
      echo "<input type=\"checkbox\" name=\"".$photo["id"]."\" onclick=\"if (check_max() == false) return false; if (check_self('".$photo["id"]."') == false) return false; vote_js('vote_".$photo["id"]."', this.checked);\" ";
      if ($photo["user_vote"] > 0) echo "CHECKED";
      echo " id=\"photo_".$photo["id"]."\"><label for=\"photo_".$photo["id"]."\">Отдать голос</label>";
    }

    if ($arResult["show_vote"] == true) {
      echo "Голосов: <b>".$photo["vote"]."</b><br>";
    }
    /*
    var_dump($photo["picture"]);
    var_dump(ShowImageFixedSize($photo["picture"],100,100));
    */
    echo  "</td></tr><tr><td><small>".$photo["name"]."</small></td></tr></table></td>\r\n";
  }


  // Заполняем до конца таблицу
  while ($i < $count_row && $i != 0) {
    echo "\t<td>&nbsp;</td>\r\n";
    $i++;
  }


  echo "</tr></table><br><br>\r\n\r\n";
  if ($arResult["can_vote"] == true) echo "<input type=\"submit\" onclick=\"return check_vote();\" value=\"Проголосовать\">";

  echo "</form>\r\n"


?>