<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="cityTour"></div>

<script type="text/javascript">
if(window.jQuery==undefined) {
	alert('Необходимо подключить библиотеку jQuery');
}
$(document).ready(function() {
	CityTourStatus (<?=$arResult["TOUR"]?>, <?=$arParams["COUNT"]?>);
});
</script>
