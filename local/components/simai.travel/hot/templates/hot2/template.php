<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["SALE"]=intval($arParams["SALE_PERCENT"]);
?>
<script>
// Ожидание статуса обработки
function HotelTourStatus (RequestResult, count, num) {
	// Делаем запрос статуса
	$.get("/bitrix/tools/simai.travel_ajax.php", { id: RequestResult, type: "searchStatus" }, function(status) {
		if(status != "load") {
			setTimeout(function() {
				HotelTourStatus(RequestResult, count, num);
			}, 2000);
		}
		else {
			HotelTourData(RequestResult, count, num);
		}
	});
}

// Отображение результата данных
function HotelTourData (RequestResult, count, num) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, template: 'hot2', sale: num }, function(data) {
		$('#hotelTour').html(data);
	});
}

// Покупка тура
function HotelTourBuy (RequestResult, count, tour, num) {
if(num=="undefind")num=0;
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, tourBuy: tour, template: 'hot2',sale: num }, function(data) {
		$('#hotelTour').html(data);
		$.get("/personal/cart/system.php", {ajax: "Y"}, function(data){$('#cartLine').html(data);});
	});
}
</script>
<div class="idea-title">Еще немного идей для отдыха</div>
<div id="hotelTour">Ждите, идет загрузка.</div>

<script type="text/javascript">
if(window.jQuery==undefined) {
	alert('Необходимо подключить библиотеку jQuery');
}
$(document).ready(function() {
if (typeof(HotelTourStatus)=='function'){
	HotelTourStatus (<?=$arResult["TOUR"]?>, <?=$arParams["COUNT"]?>, <?=$arParams["SALE"]?>);
	}
});
</script>