<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["SALE_PERCENT"]=intval($arParams["SALE_PERCENT"]);
?>
<script>

	// Ожидание статуса обработки
	function HotelTourStatus (RequestResult, count, num) {
		// Делаем запрос статуса
		$.get("/bitrix/tools/simai.travel_ajax.php", { id: RequestResult, type: "searchStatus" }, function(searchStatus) {

            searchStatus = JSON.parse(searchStatus);

            var status;
            if (searchStatus.status) {
                status = "load";
            }else {
                status = "wait";
            }

			if(status != "load") {
				setTimeout(function() {
					HotelTourStatus(RequestResult, count, num);
				}, 2000);
			}
			else {
				HotelTourData(RequestResult, count, num);
			}
		});
	}

	// Отображение результата данных
	function HotelTourData (RequestResult, count, num) {
		$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, template: 'adapt_hot', sale: num}, function(data) {
			$('#hotelTour').html(data);
			$('.tours-count').html(count);	
		});;
	}

	// Покупка тура
	function HotelTourBuy (RequestResult, count, tour, num) {
	if(num=="undefind")num=0;
		$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, tourBuy: tour, template: 'adapt_hot',sale: num }, function(data) {
			$('#hotelTour').html(data);
            $(".load-section").css("display", "none");
            //window.location = '/personal/cart/';

			//$.get("/personal/cart/system.php", {ajax: "Y"}, function(data){$('#cartLine').html(data);});
		});
        window.open('/personal/cart/','_blank');
	}


	function ChangeCountry(count, num, countryId) {
        $("#DateCountdown").TimeCircles().destroy();

		$(".hot-menu-list ul li").removeClass("active-hot-menu-item");

		$(this).addClass('active-hot-menu-item');

		$("#countryId").val(countryId);

		HotelTourStatusReload(count, num);

	}



	// Отправка формы поиска туров
	function HotelTourStatusReload(count, num) {
		// Показываем что ищем туры
		//('#hotelTour').html('Ждите, идет загрузка.');

		$.post("/bitrix/tools/simai.travel_ajax.php", $("#searchTourHot").serialize(), function(RequestResult) {
			//alert("!");
			HotelTourStatus(RequestResult, count, num);			

		});
	}
</script>




<div class="load-section" style="display: none"><img src="<?=SITE_TEMPLATE_PATH?>/images/ajax-load-blue.gif" alt=""/></div>
<!-- =========================================
Результаты
========================================== -->
<section class="gray-section">

<div class="container">

<div class="row page-header">
    <div class="col-md-8 col-md-push-3">
        <div>Горячие Предложения от Умных Туристов</div>
    </div>
    <div class="col-md-4 col-md-push-1">
        <!-- div class="btn btn-warning">Изменить условия поиска</div -->
    </div>
</div>

<!-- row -->
<div class="row">

<!-- col-md-4 -->
<div class="col-md-2 left-col">
    <div class="hot-menu-header">
        <b>Выберите страну:</b>
    </div>

    <div class="hot-menu-list">
        <ul>
        	<?foreach ($arResult["COUNTRIES"] as $code=>$country):?>
	            <li onclick="ChangeCountry(<?=$arParams["COUNT"]?>,<?=$arParams["SALE_PERCENT"]?>, <?=$code?>)" <?//if ($arParams["COUNTRY"] == $code):?> <?//endif?>>
	                <?=$country["NAME"]?>  <br>
	                <span>от <?=$country["MIN_PRICE"]?> руб/чел</span>
	                <div class="hot-menu-arrow"> > </div>
	            </li>
            <?endforeach?>
        </ul>
    </div>

</div><!-- /col-md-4 -->
<!-- col-md-2 -->
<div class="col-md-7">
	<!--Результаты поиска туров-->
	<form action="" name="searchTourHot" id="searchTourHot" method="GET">

		<input type="hidden" name="type" value="search">
		<input type="hidden" name="requestId" value="0">
		<input type="hidden" name="pageSize" value="30">
		<input type="hidden" name="pageNumber" value="1">
		<?/*<input type="hidden" name="countryId" value="<?=$arParams["COUNTRY"]?>">*/?>
		<input type="hidden" name="cityFromId" value="<?=$arParams["CITY"]?>">
		<?foreach ($arParams["STARS"] as $i=>$stars):?>
			<input type="hidden" name="starsList[<?=$i?>]" value="<?=$stars?>">
		<?endforeach?>
		<input type="hidden" name="countryId" id="countryId"  value="">
		<input type="hidden" name="adults" value="2">
		<input type="hidden" name="kids" value="0">
		<input type="hidden" name="nightsMin" value="<?=$arParams["NIGHT_FROM"]?>">
		<input type="hidden" name="nightsMax" value="<?=$arParams["NIGHT_TO"]?>">
		<input type="hidden" name="priceMin" value="">
		<input type="hidden" name="priceMax" value="">
		<input type="hidden" name="currencyAlias" value="<?=$arParams["CURRENCY"]?>">
		<input type="hidden" name="departFrom" value="<?=$arParams["DAY_FROM"]?>">
		<input type="hidden" name="departTo" value="<?=$arParams["DAY_TO"]?>">

		<?/*
		<input type="hidden" name="hotelIsNotInStop" value="1">
		<input type="hidden" name="hasTickets" value="0">
		<input type="hidden" name="ticketsIncluded" value="0">
		<input type="hidden" name="updateResult" value="1">
		<input type="hidden" name="useFilter" value="0">
		<input type="hidden" name="useTree" value="0">
		<input type="hidden" name="groupBy" value="0">
		<input type="hidden" name="includeDescriptions" value="1">
		*/?>
	</form>

	<div id="hotelTour">
      <div class="form-res-in">
	  <div id="angLoading" style="text-align:center"><img align="center"  src="http://ui.sletat.ru/gfx/ld2.gif"></div>
	  <div id="angContainer">
        <div class="error-txt"> Наш портал ищет для Вас лучшие горячие предложения.<br>Пожалуйста, ожидайте...</div>
		</div>
       </div>
	</div>

</div><!-- /col-md-7 -->


    <div class="col-md-3 right-col">
        <h5>Мы нашли для вас <span class="tours-count"></span> предложений!</h5>
        <div class="tour-search-time">
            <div id="DateCountdown" style="width: 100%;"></div>
            <span></span>
        </div>
        <h5>В стоимость каждого тура входит:</h5>
        <ul class="information-list">
            <li><i class="fa fa-plane fa-3x"></i><span>Авиаперелет, туда-обратно</span></li>
            <li><i class="fa fa-arrow-circle-right fa-3x"></i><span>Трансфер, Аэропорт-Отель-Аэропорт</span></li>
            <li><i class="fa fa-home fa-3x"></i><span>Проживание в отеле</span></li>
            <li><i class="fa fa-list fa-3x"></i><span>Питание в отеле (на выбор)</span></li>
            <li><i class="fa fa-plus fa-3x"></i><span>Медицинская страховка</span></li>
        </ul>
        <h4 style="text-align: right; color: #74C14A">Обозначения:</h4>
        <h5>Места на рейсе:</h5>
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x" style="color:#74C14A"></i>
                  <i class="fa fa-plane fa-stack-1x"></i>
                </span>			     - Есть </br>
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x" style="color:#E33933"></i>
                  <i class="fa fa-plane fa-stack-1x"></i>
                </span>			     - Нет </br>
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x" style="color:#FF9D40"></i>
                  <i class="fa fa-plane fa-stack-1x"></i>
                </span>			     - По запросу
        <h5>Места в отеле:</h5>
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x" style="color:#74C14A"></i>
                  <i class="fa fa-home fa-stack-1x"></i>
                </span>             - Есть
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x" style="color:#FF9D40"></i>
                  <i class="fa fa-home fa-stack-1x"></i>
                </span>- По запросу
        <h5>Рекомендованные отели для:</h5>
        <ul class="information-list-2">
            <li>
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x" style="color:#B9E0A5"></i>
                          <i class="fa fa-lightbulb-o fa-stack-1x"></i>
                        </span>
                <span> Семейного отдыха / Отдыха с детьми </span>
            </li>
            <li>
                       <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x" style="color:#7EA6E0"></i>
                          <i class="fa fa-lightbulb-o fa-stack-1x"></i>
                       </span>
                <span> Молодежного / Активного отдыха </span>
            </li>
            <li>
                       <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x" style="color:#F8B5CC"></i>
                          <i class="fa fa-lightbulb-o fa-stack-1x"></i>
                       </span>
                <span> Романтичного отдыха (молодоженам и парам)</span>
            </li>
        </ul>
    </div><!-- /col-md-3 -->

		</div><!-- /row -->
	</div><!-- /container -->
</section><!-- /section -->






<script type="text/javascript">
    //Начинаем поиск после загрузки страницы (по стране указанной в параметрах компонента или если перешли из страницы страны)
    var countryId = <?=isset($_REQUEST["countryId"])?$_REQUEST["countryId"]:$arParams["COUNTRY"]?>;
	ChangeCountry(<?=$arParams["COUNT"]?>,<?=$arParams["SALE_PERCENT"]?>, countryId);
</script>