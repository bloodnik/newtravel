<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["SALE_PERCENT"]=intval($arParams["SALE_PERCENT"]);
?>
<script>
// Ожидание статуса обработки
function HotelTourStatus (RequestResult, count, num) {
	// Делаем запрос статуса
	$.get("/bitrix/tools/simai.travel_ajax.php", { id: RequestResult, type: "searchStatus" }, function(status) {
		if(status != "load") {
			setTimeout(function() {
				HotelTourStatus(RequestResult, count, num);
			}, 2000);
		}
		else {
			HotelTourData(RequestResult, count, num);
		}
	});
}

// Отображение результата данных
function HotelTourData (RequestResult, count, num) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, template: 'hot', sale: num}, function(data) {
		$('#hotelTour').html(data);
	});
}

// Покупка тура
function HotelTourBuy (RequestResult, count, tour, num) {
if(num=="undefind")num=0;
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, tourBuy: tour, template: 'hot',sale: num }, function(data) {
		$('#hotelTour').html(data);
		$.get("/personal/cart/system.php", {ajax: "Y"}, function(data){$('#cartLine').html(data);});
	});
}

// Отправка формы поиска туров
function HotelTourStatusReload(count, num) {
	// Показываем что ищем туры
	$('#hotelTour').html('Ждите, идет загрузка.');

	$.post("/bitrix/tools/simai.travel_ajax.php", $("#searchTourHot").serialize(), function(RequestResult) {
		//alert("!");
		HotelTourStatus(RequestResult, count, num);
	});
}

</script>
<div class="hot-tours">ГОРЯЩИЕ ТУРЫ
            <div class="sort-buttons"></div>
</div>

<form class="jClever" action="" name="searchTourHot" id="searchTourHot" method="GET">

<input type="hidden" name="type" value="search">
<input type="hidden" name="requestId" value="0">
<input type="hidden" name="pageSize" value="30">
<input type="hidden" name="pageNumber" value="1">
<?/*<input type="hidden" name="countryId" value="<?=$arParams["COUNTRY"]?>">*/?>
<input type="hidden" name="cityFromId" value="<?=$arParams["CITY"]?>">
<?foreach ($arParams["STARS"] as $i=>$stars):?>
<input type="hidden" name="starsList[<?=$i?>]" value="<?=$stars?>">
<?endforeach?>
<input type="hidden" name="adults" value="2">
<input type="hidden" name="kids" value="0">
<input type="hidden" name="nightsMin" value="<?=$arParams["NIGHT_FROM"]?>">
<input type="hidden" name="nightsMax" value="<?=$arParams["NIGHT_TO"]?>">
<input type="hidden" name="priceMin" value="">
<input type="hidden" name="priceMax" value="">
<input type="hidden" name="currencyAlias" value="<?=$arParams["CURRENCY"]?>">
<input type="hidden" name="departFrom" value="<?=$arParams["DAY_FROM"]?>">
<input type="hidden" name="departTo" value="<?=$arParams["DAY_TO"]?>">

<div class="select-box">	
<select name="countryId" onchange="HotelTourStatusReload(<?=$arParams["COUNT"]?>, <?=$arParams["SALE_PERCENT"]?>)">
<?foreach ($arResult["COUNTRIES"] as $code=>$name):?>
<option value="<?=$code?>"<?if ($arParams["COUNTRY"] == $code):?> selected<?endif?>><?=$name?></option>
<?endforeach?>
</select>
</div>
<?/*
<input type="hidden" name="hotelIsNotInStop" value="1">
<input type="hidden" name="hasTickets" value="0">
<input type="hidden" name="ticketsIncluded" value="0">
<input type="hidden" name="updateResult" value="1">
<input type="hidden" name="useFilter" value="0">
<input type="hidden" name="useTree" value="0">
<input type="hidden" name="groupBy" value="0">
<input type="hidden" name="includeDescriptions" value="1">
*/?>
</form>

<div id="hotelTour">Ждите, идет загрузка.</div>

<script type="text/javascript">
if(window.jQuery==undefined) {
	alert('Необходимо подключить библиотеку jQuery');
}
$(function() {
if (typeof(HotelTourStatus)=='function'){
	HotelTourStatus (<?=$arResult["TOUR"]?>, <?=$arParams["COUNT"]?>, <?=$arParams["SALE_PERCENT"]?>);
}
      /*$(".tour-item.item-right").each(function(){
		var height=$(this).outerHeight();
		$(this).next(".palm").css({"height":height});
      });*/
});
</script>