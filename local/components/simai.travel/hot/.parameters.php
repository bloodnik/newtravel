<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


// Подключение модуля
CModule::IncludeModule("simai.travel");

// Откуда
$city = CSimaiTravelSletat::GetDepartCities();
$listCity = array("0" => "Нет");
foreach($city as $arr) {
	$listCity[$arr["Id"]] = $arr["Name"];
}


// Куда
$country = CSimaiTravelSletat::GetCountries($arCurrentValues["CITY"]);
$listCountry = array("0" => "Нет");
foreach($country as $arr) {
	$listCountry[$arr["Id"]] = $arr["Name"];
}




$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"CITY" => Array(
			"PARENT" => "BASE",
			"NAME" => "Город вылета",
			"TYPE" => "LIST",
			"VALUES" => $listCity,
			"REFRESH" => "Y",
		),
		"COUNTRY_IBLOCK" => Array(
			"PARENT" => "BASE",
			"NAME" => "Инфоблок стран назначения",
			"TYPE" => "STRING",
		),
		"STARS_IBLOCK" => Array(
			"PARENT" => "BASE",
			"NAME" => "Инфоблок звездности",
			"TYPE" => "STRING",
		),
		"DAY_FROM" => Array(
			"PARENT" => "BASE",
			"NAME" => "Дней до вылета (c)",
			"TYPE" => "STRING",
			"DEFAULT" => "1",
		),
		"DAY_TO" => Array(
			"PARENT" => "BASE",
			"NAME" => "Дней до вылета (по)",
			"TYPE" => "STRING",
			"DEFAULT" => "30",
		),
		"NIGHT_FROM" => Array(
			"PARENT" => "BASE",
			"NAME" => "Ночей (от)",
			"TYPE" => "STRING",
			"DEFAULT" => "7",
		),
		"NIGHT_TO" => Array(
			"PARENT" => "BASE",
			"NAME" => "Ночей (до)",
			"TYPE" => "STRING",
			"DEFAULT" => "12",
		),
		"COUNT" => Array(
			"PARENT" => "BASE",
			"NAME" => "Количество результатов",
			"TYPE" => "STRING",
			"DEFAULT" => "10",
		),
		"CURRENCY" => Array(
			"PARENT" => "BASE",
			"NAME" => "Валюта",
			"TYPE" => "STRING",
			"DEFAULT" => 'RUB',
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
	),
);
?>