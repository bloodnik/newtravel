<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// Преобразовываем дату в нужный вид
$arParams["DAY_FROM"] = date("d.m.Y", time()+60*60*24*$arParams["DAY_FROM"]);
$arParams["DAY_TO"] = date("d.m.Y", time()+60*60*24*$arParams["DAY_TO"]);

$arResult = array();

// Кеширование
//$obCache = new CPageCache;
//if($obCache->StartDataCache($arParams["CACHE_TIME"], serialize($arParams),  "/")):

	// Подключение модуля
	CModule::IncludeModule("simai.travel");
	CModule::IncludeModule("iblock");

	// Выборка активных стран
	$arResult["COUNTRIES"] = Array();


    $arSelect = Array("NAME", "CODE", "PROPERTY_MIN_PRICE");
    $arFilter = Array("IBLOCK_ID"=>IntVal($arParams["COUNTRY_IBLOCK"]), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
    while($ob = $res->GetNextElement()){
        $arCountries = $ob->GetFields();
        $arResult["COUNTRIES"][$arCountries["CODE"]] = array("NAME" => $arCountries["NAME"], "MIN_PRICE" => $arCountries["PROPERTY_MIN_PRICE_VALUE"]);
        if (!isset($_REQUEST["countryId"])) {
            $arParams["COUNTRY"] = $arCountries["CODE"];
        }
    }

	// Выборка активных стран
	$arParams["STARS"] = Array();
	$res = $DB->Query("select `NAME`,`CODE` from `b_iblock_element` where `IBLOCK_ID`=".IntVal($arParams["STARS_IBLOCK"])." and `ACTIVE`='Y'");
	$i = 0;
	while($arr = $res->GetNext()):
		$i++;
		if (IntVal($arr["CODE"])):
			$arParams["STARS"][] = $arr["CODE"];
		endif;
	endwhile;

	// Список туров в отель
	$params = array(
		'requestId'=>0,
		'pageSize'=>30,
		'pageNumber'=>1,
		"countryId"=>isset($_REQUEST["countryId"])?$_REQUEST["countryId"]:$arParams["COUNTRY"],
		"cityFromId"=>$arParams["CITY"],
		"cities"=>array(),
		"meals"=> array(),
		"stars"=> $arParams["STARS"],
		"hotels"=> array(),
		"adults"=>2,
		"kids"=>0,
		"kidsAges"=>array(),
		"nightsMin" => $arParams["NIGHT_FROM"],
		"nightsMax" => $arParams["NIGHT_TO"],
		"priceMin" => "",
		"priceMax" => "",
		"currencyAlias" => $arParams["CURRENCY"],
		"departFrom" => $arParams["DAY_FROM"],
		"departTo" => $arParams["DAY_TO"],
		'hotelIsNotInStop'=>true,
		'hasTickets'=>false,
		'ticketsIncluded'=>false,
		'updateResult'=>true,
		'useFilter'=>false,
		'f_to_id'=>array(),
		'useTree'=>false,
		'groupBy'=>false,
		'includeDescriptions'=>true
	);
	//$arResult["TOUR"] = CSimaiTravelSletat::CreateRequest($params);

	$this->IncludeComponentTemplate();

    // записываем предварительно буферизированный вывод в файл кеша
    //$obCache->EndDataCache();

//endif;
?>