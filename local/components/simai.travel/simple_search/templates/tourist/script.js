if(window.jQuery==undefined) {
	alert('Необходимо подключить библиотеку jQuery');
}
$(document).ready(function() {
	$('#travelCityFrom').change(function() {
		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "country", from: $('#travelCityFrom').val()}, function(json){
			$('#travelCountryTo').empty();
			$('#travelCountryTo option').remove();
			$('#travelCountryTo').append( $('<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>') );
			$.each(json.results,function(i,country){
				$('#travelCountryTo').append( $('<option value="'+country.Id+'">'+country.Name+'</option>') );
			});
		});
	});
});