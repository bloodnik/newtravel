<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$countries_temp = $arResult["to"];
$arResult["to"] = Array();
$top_countries = Array(
	"Болгария",
	"Вьетнам",
	"Греция",
	"Египет",
	"Израиль",
	"Испания",
	"Италия",
	"Кипр",
	"ОАЭ",
	"Индия",
	"Таиланд",
	"Тунис",
	"Турция",
	"Хорватия",
	"Черногория",
	"Чехия",
	"Шри-Ланка"
);	
foreach ($top_countries as $cname):
	foreach($countries_temp as $i=>$country):	
		if ($country["Name"] == $cname):
			$arResult["to"][] = $country;
			unset($countries_temp[$i]);
		endif;
	endforeach;
endforeach;
foreach($countries_temp as $i=>$country):
	$arResult["to"][] = $country;	
endforeach;
?>
<form action="<?=$arParams["ACTION"]?>" name="searchTour" method="GET" class="jClever" <?if($arParams["NEW"] == "Y"):?>target="new"<?endif;?>>
<div class="filter-main">
              <div class="filter-wrap">
<input type="hidden" name="start" value="Y">
<input type="hidden" name="count" value="<?=$arParams["COUNT"]?>">
<input type="hidden" name="date2" value="<?=date("d.m.Y",(86400*12)+time())?>">

 <div class="f-lcol">
 <div class="select-box">
					<select name="cityFromId" id="travelCityFrom">
						<option value="0">Откуда</option>
						<?foreach($arResult["from"] as $city):?>
							<option value="<?=$city["Id"]?>" <?if($arParams["CITY"] == $city["Id"]):?>SELECTED<?endif;?>><?=$city["Name"]?></option>
						<?endforeach?>
					</select>
					<select name="countryId" id="travelCountryTo">
						<option value="0">Куда</option>
						<optgroup label="Топовые страны">
						<?foreach($arResult["to"] as $i=>$country):?>
							<option value="<?=$country["Id"]?>" <?if($arParams["COUNTRY"] == $country["Id"]):?>SELECTED<?endif;?>><?=$country["Name"]?></option>
							<?if ($i == count($top_countries) - 1):?>
						</optgroup><optgroup label="Популярные страны">
							<?endif;?>
						<?endforeach?>
						</optgroup>
					</select>
</div>
					<div class="select-box">
						<select id="i-nightlist" class="sCombo" name="night">
							<option value="6-6">на 6 ночей</option>
							<option value="10-10">на 10 ночей</option>
							<option value="14-14">на 14 ночей</option>
							<option value="2-6">на 2-6 ночей</option>
							<option selected="" value="6-14">на 6-14 ночей</option>
							<option value="7-10">на 7-10 ночей</option>
							<option value="11-14">на 11-14 ночей</option>
							<option value="15-30">на 14 ночей</option>
						</select>
					</div>
				
</div>
<div class="f-rcol">
			<div class="date-pick simple">
				<input placeholder="Вылет с" id="dateAir" value="<?=date("d.m.Y")?>" class="from" name="date" />
			</div>
			<!--<div class="c-row simple"><input type="checkbox" id="i-3day" name="3day" CHECKED><span>±3 дня</span></div>-->
					
		<input class="button" type="submit" value="ИСКАТЬ ТУР">
</div>
<div class="big-form"><a href="/tours/">Перейти на расширенную форму поиска</a></div>
</div>
</div>
</form>