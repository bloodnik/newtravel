<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


// Подключение модуля
CModule::IncludeModule("simai.travel");

// Откуда
$city = CSimaiTravelSletat::GetDepartCities();
$listCity = array("0" => "Нет");
foreach($city as $arr) {
	$listCity[$arr["Id"]] = $arr["Name"];
}


// Куда
$country = CSimaiTravelSletat::GetCountries($arCurrentValues["CITY"]);
$listCountry = array("0" => "Нет");
foreach($country as $arr) {
	$listCountry[$arr["Id"]] = $arr["Name"];
}


$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"CITY" => Array(
			"PARENT" => "BASE",
			"NAME" => "Город вылета",
			"TYPE" => "LIST",
			"VALUES" => $listCity,
			"REFRESH" => "Y",
		),
		"COUNTRY" => Array(
			"PARENT" => "BASE",
			"NAME" => "Страна назначения",
			"TYPE" => "LIST",
			"VALUES" => $listCountry,
			"REFRESH" => "N",
		),
		"DAY" => Array(
			"PARENT" => "BASE",
			"NAME" => "Дней до вылета",
			"TYPE" => "STRING",
			"DEFAULT" => "1",
		),
		"ACTION" => Array(
			"PARENT" => "BASE",
			"NAME" => "Назначение формы",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"NEW" => array(
			"PARENT" => "BASE",
			"NAME" => "Открывать форму в новом окне",
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		"COUNT" => Array(
			"PARENT" => "BASE",
			"NAME" => "Результатов на странице",
			"TYPE" => "STRING",
			"DEFAULT" => "10",
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
	),
);
?>