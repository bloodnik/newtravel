<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// Преобразовываем дату в нужный вид
$arParams["DAY_FROM"] = date("d.m.Y", time()+60*60*24*$arParams["DAY_FROM"]);
$arParams["DAY_TO"] = date("d.m.Y", time()+60*60*24*$arParams["DAY_TO"]);

// Если есть переданные данные из маленькой формы, то обрабатываем их
if(!empty($_REQUEST["START"])) {

        $arr = explode("-", $_REQUEST["night"]);
        $arParams["NIGHT_FROM"] =  isset($_REQUEST["night"])?$arr[0]:$_REQUEST["nightsMin"];
        $arParams["NIGHT_TO"] =  isset($_REQUEST["night"])?$arr[1]:$_REQUEST["nightsMax"];


	$arParams["COUNTRY"] = $_REQUEST["countryId"];
	$arParams["CITY"] = $_REQUEST["cityFromId"];

	$arParams["DAY_FROM"] = $arParams["DAY_TO"] = $_REQUEST["departFrom"];
	if(!empty($_REQUEST["3day"])) {
		$date = explode(".", $_REQUEST["date"]);
		$time = mktime(0, 0, 0, $date[1], $date[0], $date[2]);
		$arParams["DAY_FROM"] = date("d.m.Y", $time);
		$arParams["DAY_TO"] = date("d.m.Y", $time+60*60*24*3);
	}
	elseif(!empty($_REQUEST["departTo"])) {
		$arParams["DAY_TO"] = $_REQUEST["departTo"];
	}
}

// Для ссылки формируем параметры при запуске
$arParams["ADULTS"] = (!empty($_REQUEST["adults"])) ? $_REQUEST["adults"] : 2;
$arParams["KIDS"] = (!empty($_REQUEST["kids"])) ? $_REQUEST["kids"] : 0;
$arParams["KID1"] = (!empty($_REQUEST["kid1"])) ? $_REQUEST["kid1"] : "";
$arParams["KID2"] = (!empty($_REQUEST["kid2"])) ? $_REQUEST["kid2"] : "";
$arParams["KID3"] = (!empty($_REQUEST["kid3"])) ? $_REQUEST["kid3"] : "";

$arParams["PRICE_MIN"] = (!empty($_REQUEST["priceMin"])) ? $_REQUEST["priceMin"] : "";
$arParams["PRICE_MAX"] = (!empty($_REQUEST["priceMax"])) ? $_REQUEST["priceMax"] : "";
$arParams["PRICE_CURRENCY"] = (!empty($_REQUEST["currencyAlias"]) and in_array($_REQUEST["currencyAlias"], array('RUB', 'USD', 'EUR'))) ? $_REQUEST["currencyAlias"] : "RUB";

$arParams["RESORTS"] = (!empty($_REQUEST["curortsList"])) ? explode(",", $_REQUEST["curortsList"]) : array(); // курорты (города)
$arParams["STARS"] = (!empty($_REQUEST["stars"])) ? explode(",", $_REQUEST["stars"]) : array(); // звезды
$arParams["MEALS"] = (!empty($_REQUEST["meals"])) ? explode(",", $_REQUEST["meals"]) : array(); // питание
$arParams["HOTELS"] = (!empty($_REQUEST["hotels"])) ? explode(",", $_REQUEST["hotels"]) : array(); // отели



// Запуск поиска туров
$arParams["START"] = ($_REQUEST["START"] == "Y") ? true : false;

$arResult = array();

// Кеширование
$obCache = new CPageCache;
if($obCache->StartDataCache($arParams["CACHE_TIME"], serialize($arParams),  "/")):

	// Подключение модуля
//CModule::IncludeModule("simai.travel");

	// Откуда
//$arResult["from"] = CSimaiTravelSletat::GetDepartCities();

	// Список стран
//$arResult["to"] = CSimaiTravelSletat::GetCountries($arParams["CITY"]);

	// Список курортов (не отдает если не указана страна назначения)
//$arResult["resorts"] = CSimaiTravelSletat::GetCities($arParams["COUNTRY"]);

	// Список отелей
	//$arResult["hotels"] = CSimaiTravelSletat::GetHotels($arParams["COUNTRY"], $arParams["RESORTS"], $arParams["STARS"]);

	// Список категорий отелей
//$arResult["stars"] = CSimaiTravelSletat::GetHotelStars($arParams["COUNTRY"], $arParams["RESORTS"]);

	// Список категорий питания
//$arResult["meals"] = CSimaiTravelSletat::GetMeals();


    /** Группировка типов питания*/
/*$arSortMeals = array();
    $FB = array(112);
    $HB = array(113);
    $AI = array(115);
    $EXCEPTION_MEALS = array(121, 122, 116);
    $i=0;
    foreach ($arResult["meals"] as $meals){

        if (in_array($meals["Id"], $EXCEPTION_MEALS)){
            continue;
        }

        if (in_array($meals["Id"], $FB)) {
            $arSortMeals[$i]["Name"] = "FB/FB+";
            $arSortMeals[$i]["Id"] = "112,121";
        }
        elseif (in_array($meals["Id"], $HB)) {
            $arSortMeals[$i]["Name"] = "HB/HB+";
            $arSortMeals[$i]["Id"] = "113,122";
        }
        elseif (in_array($meals["Id"], $AI)) {
            $arSortMeals[$i]["Name"] = "AI/UAI+";
            $arSortMeals[$i]["Id"] = "115,116";
        }
        else {
            $arSortMeals[$i]["Name"] = $meals["Name"];
            $arSortMeals[$i]["Id"] = $meals["Id"];
        }
        $i++;
    }
    $arResult["arSortMeals"] = $arSortMeals;

    if (isset($arParams["MEALS"]) && is_array($arParams["MEALS"]) ){
        $checkedMeals = array();
        foreach($arParams["MEALS"] as $meals) {
            if (in_array($meals, $FB)) {
                $checkedMeals[] = "112,121";
            } elseif (in_array($meals, $HB)) {
                $checkedMeals[] = "113,122";
            } elseif (in_array($meals, $AI)) {
                $checkedMeals[] = "115,116";
            }
        }
        $arResult["checkedMeals"] = $checkedMeals;
}*/


	$this->IncludeComponentTemplate();

    // записываем предварительно буферизированный вывод в файл кеша
    $obCache->EndDataCache();

endif;
?>