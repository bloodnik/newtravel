<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["SALE"]=intval($arParams["SALE_PERCENT"]);
?>
<script type="text/javascript">
if(window.jQuery==undefined) {
	//alert('Необходимо подключить библиотеку jQuery');
}

jQuery.fn.live = function (types, data, fn) {
    jQuery(this.context).on(types,this.selector,data,fn);
    return this;
};

//Масив загруженных отелей
var hotelList = new Array();

$(function(){

    //Первоначальная загрузка отелей
    GetHotelList();

    $('.input-group.date').datepicker({
        format: "dd.mm.yyyy",
        language: "ru",
        autoclose: true
    });

	// Изменение города отправления
	$('#travelCityFrom').change(function() {
		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "country", from: $('#travelCityFrom').val()}, function(json){
			$('#travelCountryTo').empty();
			$('#travelCountryTo option').remove();
			$('#travelCountryTo').append( $('<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>') );
			$.each(json.results,function(i,country){
				$('#travelCountryTo').append( $('<option value="'+country.Id+'">'+country.Name+'</option>') );
			});

            //Устанавливаем тултип на выбор страны.
            $('[data-toggle="tooltip"]').tooltip('show');
            $("#travelCountryTo").css("background", "rgba(0, 174, 239, 0.16)");

		});

		// Сбрасываем все остальные настройки
		$('#resortsList').empty();
		$('#resortsList option').remove();
		$("#resortsAny").attr("checked","checked");

		$('#starsList').empty();
		$('#starsList option').remove();
		$("#starsAny").attr("checked","checked");

		$('#hotelsList').empty();
		$('#hotelsList option').remove();
		$("#hotelsAny").attr("checked","checked");
	});

	// Изменение страны назначения
	$('#travelCountryTo').change(function() {
		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "resort", country: $('#travelCountryTo').val()}, function(json){
			$('#resortsList').empty();
			$('#resortsList option').remove();
			//Заполняем курорты по приоритету - 01.07.2014			
			$('#resortsList').append( $('<li class="scrolable-group-header">Популярные курорты</li>') );
			$.each(json.results.topCities,function(i,resort){
				$('#resortsList').append( $('<li><input type="checkbox" name="resortsList[]" onchange="createResortList()" value="'+resort.Id+'"><label>'+resort.Name+'</label></li>') ); //Популярные курорты
			});
			$('#resortsList').append( $('<li class="scrolable-group-header">Остальные курорты</li>') );
			$.each(json.results.cities,function(i,resort){
				$('#resortsList').append( $('<li><input type="checkbox" name="resortsList[]" onchange="createResortList()" value="'+resort.Id+'"><label>'+resort.Name+'</label></li>') ); //Остальные курорты
			});			

			// Устанавливаем галочки по умолчанию
			$("#resortsAny").attr("checked","checked");
			$("#hotelsAny").attr("checked","checked");

            // Изменяем доступные категории отелей
            GetHotelStars();

			// Снятие/установка галочки "Любой курорт"
			$('#resortsList input:checkbox').change(function() {
				$("#resortsAny").attr("checked","checked");
				$("#resortsList input:checkbox:checked").each(function() {
					$("#resortsAny").removeAttr("checked");
				});
				// Изменение категорий отелей при смене списка курортов
				GetHotelStars();
			});

		});

        //Убираем тултип и фон выбора страны
        $('[data-toggle="tooltip"]').tooltip('destroy');
        $("#travelCountryTo").css("background", "#fff");

	});
	//
	// Зависимые изменения
	//

	// Получаем список всех доступных категорий отелей
	function GetHotelStars() {
		// Составляем список курортов
		var resorts = "";
		if($("#resortsAny").attr("checked") !== "checked") {
			$("#resortsList input:checkbox:checked").each(function(i,resort) {
				resorts = resorts+resort.value+",";
			});
		}

		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "stars", country: $('#travelCountryTo').val(), resorts: resorts}, function(json){
			$('#starsList').empty();
			$('#starsList option').remove();
			$("#starsAny").attr("checked","checked");
			$.each(json.results,function(i,star){
				$('#starsList').append( $('<div class="checklist"><input type="checkbox" name="starsList[]" onchange="createStarList()" value="'+star.Id+'"><span>'+star.Name+'</span></div>') );
			});

			// Снятие/установка галочки "Любая категория отеля"
			$('#starsList input:checkbox').change(function() {
				$("#starsAny").attr("checked","checked");
				$("#starsList input:checkbox:checked").each(function() {
					$("#starsAny").removeAttr("checked");
				});
				GetHotelList();
			});


			// Изменение списка отелей
			GetHotelList();
		});
	}


	// Получаем список всех доступных отелей
	function GetHotelList() {

		// Составляем список курортов
		var resorts = "";
		if($("#resortsAny").attr("checked") !== "checked") {
			$("#resortsList input:checkbox:checked").each(function(i,resort) {
				resorts = resorts+resort.value+",";
			});
		}

		// Составляем список категорий
		var stars = "";
		if($("#starsAny").attr("checked") !== "checked") {
			$("#starsList input:checkbox:checked").each(function(i,star) {
				stars = stars+star.value+",";
			});
		}
		// Если введен фрагмент имени
		var filter = "";
		if($("#hotelsName").val() !== "") {

			filter = $("#hotelsName").val();
		}

		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "hotels", country: $('#travelCountryTo').val(), resorts: resorts, stars: stars, filter: filter}, function(json){

			var checked_hotels = new Array();
			var j = 0;
			$("#hotelsList input:checkbox:checked").each(function(i,hotel) {
				checked_hotels[j] = '<li><input type="checkbox" onchange="createHotelList()" checked name="hotelsList[]" alt="'+hotel.alt+'" value="'+hotel.value+'"><label>'+hotel.alt+'</label></li>';
				j++;
			});
			
			$('#hotelsList').empty();
			$('#hotelsList option').remove();
			$('#hotels_list_autocomplete').empty();
			$('#hotels_list_autocomplete option').remove();

			if (checked_hotels[0]==undefined) {				
				$("#hotelsAny").attr("checked","checked");				
			}
			
			$.each(checked_hotels,function(i,hotel){
				$('#hotelsList').append( $(hotel) );
			});


			$.each(json.results,function(i,hotel){

                //заполняем массив отелей для фильтрации
                var hotelInfo  = new Array();
                hotelInfo[0] = hotel.Id;
                hotelInfo[1] = hotel.Name;
                hotelList[i] = hotelInfo;

				$('#hotelsList').append( $('<li><input type="checkbox" onchange="createHotelList()" name="hotelsList[]" alt="'+hotel.Name+'" value="'+hotel.Id+'"><label>'+hotel.Name+' '+hotel.StarName+'</label></li>') );
				$('#hotels_list_autocomplete').append( $('<option value="'+hotel.Name+'">') );
			});

			// Снятие/установка галочки "Любой отель"
            $('#hotelsList input:checkbox').change(function() {
                $("#hotelsAny").attr("checked","checked");
                $("#hotelsList input:checkbox:checked").each(function() {
                    $("#hotelsAny").removeAttr("checked");
                });
            });

		});
	}	
	
	// Поиск отелей
	$('#hotelsSearch').click(function() {
		GetHotelList();
	});

	//
	// Обработчики галочек
	//
	// Галочка "Все курорты"
	$("#resortsAny").live("change", function(){
      GetHotelStars();
    });
	// Снятие/установка галочки "Любой курорт"
	$("#resortsAny").live("change", function(){
		if ($(this).attr("checked") == "checked") {
			$("#resortsList input:checkbox:checked").each(function() {
				$(this).removeAttr("checked");
			});		
		}
	});
	$("#resortsList input:checkbox").live("change", function(){
      $("#resortsAny").attr("checked","checked");
		$("#resortsList input:checkbox:checked").each(function() {
			$("#resortsAny").removeAttr("checked");
		});
		// Изменение категорий отелей при смене списка курортов
		GetHotelStars();
    });




	/*$('#resortsList input:checkbox').change(function() {
		$("#resortsAny").attr("checked","checked");
		$("#resortsList input:checkbox:checked").each(function() {
			$("#resortsAny").removeAttr("checked");
		});
		// Изменение категорий отелей при смене списка курортов
		GetHotelStars();
	});*/



	// Снятие/установка галочки "Любая категория отеля"
	$("#starsList input:checkbox").live("change", function(){
      $("#starsAny").attr("checked","checked");
		$("#starsList input:checkbox:checked").each(function() {
			$("#starsAny").removeAttr("checked");
		});
		GetHotelList();
    });
	/*$('#starsList input:checkbox').change(function() {
		$("#starsAny").attr("checked","checked");
		$("#starsList input:checkbox:checked").each(function() {
			$("#starsAny").removeAttr("checked");
		});
		GetHotelList();
	});*/

	// Галочка "Все категории отелей"
	$("#starsAny").live("change", function(){
      GetHotelStars();
    });
	// Снятие/установка галочки "Любой отель"
	$("#hotelsAny").live("change", function(){
		if ($(this).attr("checked") == "checked") {
			$("#hotelsList input:checkbox:checked").each(function() {
				$(this).removeAttr("checked");
			});
			GetHotelList();
		}			
	});	
	$("#hotelsList input:checkbox").live("change", function(){
      $("#hotelsAny").attr("checked","checked");
		$("#hotelsList input:checkbox:checked").each(function() {
			$("#hotelsAny").removeAttr("checked");
		});
    });

	// Снятие/установка галочки "Любая категория питания"
	$("#mealsList input:checkbox").live("change", function(){
      $("#mealsAny").attr("checked","checked");
		$("#mealsList input:checkbox:checked").each(function() {
			$("#mealsAny").removeAttr("checked");
		});
    });
});

// 
// Разбор GET
//
function parseGetParams() {
   var $_GET = {};
   var __GET = window.location.search.substring(1).split("&");
   for(var i=0; i<__GET.length; i++) {
      var getVar = __GET[i].split("=");
      $_GET[getVar[0]] = typeof(getVar[1])=="undefined" ? "" : getVar[1];
   }
   return $_GET;
}


//фильтрация отелей
function filterHotel(self) {
    var filterString = $("#hotelsName").val();
    if (filterString.length >= 3) {
        $("#hotelsList").empty();
        var hotelListClone = hotelList;
        var filteredList = 	hotelListClone.filter(filter);
        $.each(filteredList, function(index, hotel){
            $('#hotelsList').append( $('<li><input type="checkbox" onchange="createHotelList()" name="hotelsList[]" alt="'+hotel[1]+'" value="'+hotel[0]+'"><label>'+hotel[1]+' </label></li>') );
        });
    }else{
        $("#hotelsList").empty();
        $.each(hotelList, function(index, hotel){
            $('#hotelsList').append( $('<li><input type="checkbox" onchange="createHotelList()" name="hotelsList[]" alt="'+hotel[1]+'" value="'+hotel[0]+'"><label>'+hotel[1]+' </label></li>') );

        })
    }
}

function filter(item) {
    var filterString = $("#hotelsName").val();
    var hotelName = item[1];
    hotelName = hotelName.toLowerCase();
    filterString = filterString.toLowerCase();
    pos = hotelName.indexOf(filterString);
    if (-1 < pos) {
        return true;
    }else {
        return false;
    }
}


//
// Отправка формы поиска туров
//
function SendFormSearchTour(page, count, num) {

	// Показываем что ищем туры
	$("#searchTourStatus").css('display', 'block');
	$("#request").css('display', 'none');

	$.post("/bitrix/tools/simai.travel_ajax.php", $("#searchTour").serialize(), function(RequestResult) {
		$("#request").html(RequestResult+'<br>');

		CheckFormSearchTourStatus(RequestResult, 5, page, count, num);
	});
}



//
// Ожидание статуса обработки
//
function CheckFormSearchTourStatus (RequestResult, time, page, count, num) {
	// Делаем запрос статуса
	$.get("/bitrix/tools/simai.travel_ajax.php", { id: RequestResult, type: "searchStatus" }, function(status) {
		//$("#request").append(status+'<br>');
		if(status != "load") {
			setTimeout(function() {
				//$('#request').append('Проверка статуса, прошло '+time+' сек<br>');
				time += 5;
				CheckFormSearchTourStatus(RequestResult, time, page, count, num);
			}, 5000);
		}
		else {
			//$('#request').append('Данные обработаны за '+time+' сек<br>');
			SearchTourData(RequestResult, page, count, num);
		}
	});
}

// Отображение результата данных
function SearchTourData (RequestResult, page, count, num) {
	/*var curGetVars = parseGetParams();
	var captcha_word = curGetVars.captcha_word;
	var captcha_sid = curGetVars.captcha_sid;
	if (captcha_word == "")	{
		captcha_word = $('#captcha_word').val();
	}
	if (captcha_sid == "") {
		captcha_sid = $('#captcha_sid').val();
	}*/
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: page, count: count, sale: num/*, captcha_word: captcha_word,  captcha_sid: captcha_sid*/}, function(data) {
		$("#searchTourStatus").css('display', 'none');
		$("#request").css('display', 'block');
		$('#request').html(data);
	});
}

// Покупка тура
function SearchTourDataBuy (RequestResult, page, count, tour, num) {
if(num=="undefind")num=0;
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: page, count: count, tourBuy: tour, sale: num }, function(data) {
		$('#request').html(data);

		// Плюсуем кол-во элементов в корзине
		var count = ($('#sidebar_product_count').html()*1)+1;
		$('#sidebar_product_count').html(count);

		// Показываем человеку что тур куплен
		var r=confirm("Перейти в корзину для оформления тура?")
		if (r==true)
		{
			window.location = '/personal/cart/';
		}
		else
		{

		}
		$.get("/personal/cart/system.php", {ajax: "Y"}, function(data){$('#cartLine').html(data);});
	});
}



//
// Если переданы параметры то сразу ищем туры
//

$(document).ready(function() {
	colKid=$("#kids").val();
	if(typeof(colKid)!="undefined" && colKid==0){
		$("#kid1,#kid2,#kid3").css("display","none").parent().prev().css("display","none");
	}
	$("#kids").change(function(){
		colKid=$(this).val();
		if(typeof(colKid)!="undefined"){
			if(colKid==0){
				$("#kid1,#kid2,#kid3").css("display","none").parent().prev().css("display","none");
			}
			else if(colKid==1){
				$("#kid1").css("display","inline-block").parent().prev().css("display","block");
				$("#kid2,#kid2").css("display","none");
			}
			else if(colKid==2){
				$("#kid1,#kid2").css("display","inline-block").parent().prev().css("display","block");
				$("#kid3").css("display","none");
			}
			else if(colKid==3){
				$("#kid1,#kid2,#kid3").css("display","inline-block").parent().prev().css("display","block");
			}
		}
		});
});
//
// Формирование ссылки на страницу поиска
//
function getLink() {
	var link = 'http://' + window.location.host + document.location.pathname + 'tours/?START=Y&count=10';
	link += '&departFrom=' +  $('#air_from').val() + '&departTo=' +  $('#air_to').val();
	link += '&night=' +  $('#nightsMin').val() + '-' +  $('#nightsMax').val();
	link += '&adults=' +  $('#adults').val() + '&kids=' +  $('#kids').val() + '&kid1=' +  $('#kid1').val() + '&kid2=' +  $('#kid2').val() + '&kid3=' +  $('#kid3').val();
	link += '&priceMin=' +  $('#priceMin').val() + '&priceMax=' +  $('#priceMax').val() + '&currencyAlias=' + $('input:radio[name=currencyAlias]:checked').val();

	link += '&cityFromId=' +  $('#travelCityFrom').val() + '&countryId=' +  $('#travelCountryTo').val();

	// Список курортов
	var resortsList = "";
	$('#resortsList li input:checkbox:checked').each(function(nf, inputData)
	{
		if (resortsList !== "") resortsList += ',';
		resortsList += $(inputData).val();
	});
	link += '&curortsList=' +  resortsList;

	// Список питания
	var mealsList = "";
	$('#mealsList div input:checkbox:checked').each(function(nf, inputData)
	{
		if (mealsList !== "") mealsList += ',';
		mealsList += $(inputData).val();
	});
	link += '&meals=' +  mealsList;

	// Список звезд
	var starsList = "";
	$('#starsList div input:checkbox:checked').each(function(nf, inputData)
	{
		if (starsList !== "") starsList += ',';
		starsList += $(inputData).val();
	});
	link += '&stars=' +  starsList;

    // Отели
    var hotelsList = "";
    $('#hotelsList li input:checkbox:checked').each(function(nf, inputData)
    {
        if (hotelsList !== "") hotelsList += '%2C';
        hotelsList += $(inputData).val();
    });
    link += '&hotels=' +  hotelsList;


	//$('#getLink').html(link);
	prompt('Ссылка на страницу поиска:', link);
}

function createResortList() {

    // Список курортов
    var resortsList = "";
    $('#resortsList li input:checkbox:checked').each(function(nf, inputData)
    {
        if (resortsList !== "") resortsList += ',';
        resortsList += $(inputData).val();
    });
    $("#curortsList").val(resortsList);
}
function createHotelList() {

    // Отели
    var hotelsList = "";
    $('#hotelsList input:checkbox:checked').each(function(nf, inputData)
    {
        if (hotelsList !== "") hotelsList += ',';
        hotelsList += $(inputData).val();
    });
    $("#hotel").val(hotelsList);
}
function createMealList() {

    // Список питания
    var mealsList = "";
    $('#mealsList div input:checkbox:checked').each(function(nf, inputData)
    {
        if (mealsList !== "") mealsList += ',';
        mealsList += $(inputData).val();
    });
    $("#meals").val(mealsList);
}
function createStarList() {

    // Список звезд
    var starsList = "";
    $('#starsList div input:checkbox:checked').each(function(nf, inputData)
    {
        if (starsList !== "") starsList += ',';
        starsList += $(inputData).val();
    });
    $("#stars").val(starsList);
}

var sale=<?=$arParams["SALE"]?>
</script>

<?
$countries_temp = $arResult["to"];
$arResult["to"] = Array();
$top_countries = Array(
	"Болгария",
	"Вьетнам",
	"Греция",
	"Египет",
	"Израиль",
	"Испания",
	"Италия",
	"Кипр",
	"ОАЭ",
	"Индия",
	"Таиланд",
	"Тунис",
	"Турция",
	"Хорватия",
	"Черногория",
	"Чехия",
	"Шри-Ланка"
);	
foreach ($top_countries as $cname):
	foreach($countries_temp as $i=>$country):	
		if ($country["Name"] == $cname):
			$arResult["to"][] = $country;
			unset($countries_temp[$i]);
		endif;
	endforeach;
endforeach;
foreach($countries_temp as $i=>$country):
	$arResult["to"][] = $country;	
endforeach;
//
//Начало | Фильтрация приоритеов по курортам при открытии из малой формы| 01.07.2014
//
$resortCustomList = Array(
    "119" => Array(
        "34" => "Аланья",
        "72" => "Анталья",
        "149" => "Белек",
        "363" => "Даламан",
        "566" => "Кемер",
        "4268" => "Манавгат",
        "765" => "Мармарис",
        "1334" => "Сиде",
        "1365" => "Стамбул",
        "1531" => "Фетхие",
    ),
    "40" => Array (
        "746" => "Макади",
        "767" => "Марса Алам, Эль Кусейр",
        "893" => "Нувейба",
        "1298" => "Сафага",
        "1355" => "Сома Бей",
        "1394" => "Таба",
        "1592" => "Хургада",
        "1642" => "Шарм-Эль-Шейх",
        "1662" => "Эль Гуна",
    ),
);

if(array_key_exists($_REQUEST["countryId"],$resortCustomList)) {
	$customResorts = $resortCustomList[$_REQUEST["countryId"]];
}

if(!isset($_REQUEST["countryId"])) {
	$customResorts = $resortCustomList[$arParams["COUNTRY"]];	
}	

if (is_array($customResorts)) {
	$topCities = Array();
	$cities = Array();
	foreach ($arResult["resorts"] as $arr) {
		if (array_key_exists($arr["Id"],$customResorts)) {
			$topCities[] = $arr;
		} 
		else {
			$cities[] = $arr;
		}		
	}
}
else{
	$topCities = Array();
	$cities = Array();
	foreach ($arResult["resorts"] as $arr) {
		$topCities[] = $arr;
	}
}		
	$arResult["resorts"]["topCities"] = $topCities;
	$arResult["resorts"]["cities"] = $cities;
//
//Конец | Фильтрация приоритеов по курортам при открытии из малой формы | 01.07.2014
//

?>



<!-- =========================================
Tour Section ( Slider )
========================================== -->
<!-- tour-section -->
<section id="tour-section" class="tour-section parallax">
    <div class="overlay-grid">
        <div class="container">

            <div class="row">
            <div class="col-md-12 col-xs-12">
                <form class="form-horizontal tour-search-form" action="/tours" name="searchTour" id="searchTour" method="GET" <?if($arParams["NEW"] == "Y"):?>target="new"<?endif;?>>
                    <input type="hidden" name="type" value="search" />
                    <input type="hidden" name="START" value="Y" />
                    <div class="">
                        <!-- Город вылета -->
                        <div class="select-box form-group col-md-10">
                            <label class="col-md-4 control-label" for="travelCityFrom" id="from-label">Подбор путешествий с вылетом из </label>
                            <div class="col-md-4">
                                <select name="cityFromId" id="travelCityFrom" class="form-control">
                                    <option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>
                                    <?foreach($arResult["from"] as $city):?>
                                        <option value="<?=$city["Id"]?>" <?if($arParams["CITY"] == $city["Id"]):?>SELECTED<?endif;?>><?=$city["Name"]?></option>
                                    <?endforeach?>
                                </select>
                            </div>
                        </div>
                        <!-- Ссылка -->
                        <div class="button-link col-md-2">
                            <a class="skidko btn btn-primary" href="javascript:getLink();"  title="Получить ссылку на список туров"><i class="fa fa-link"></i></a>
                        </div>

                        <div class="col-md-3 col-xs-12" style="margin-bottom: 20px; overflow: visible">

                            <!-- Страна -->
                            <label for="travelCountryTo" class="col-md-12 col-xs-12 form-label p0">Пункт назначения:</label>
                            <div class="select-box col-md-12 col-xs-12 p0" data-toggle="tooltip" data-placement="top" title="Выберите пункт назначения!">
                                <select name="countryId" id="travelCountryTo" class="form-control">
                                    <option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>
                                    <optgroup label="Топовые страны">
                                        <?foreach($arResult["to"] as $i=>$country):?>
                                        <option value="<?=$country["Id"]?>" <?if($arParams["COUNTRY"] == $country["Id"]):?>SELECTED<?endif;?>><?=$country["Name"]?></option>
                                        <?if ($i == count($top_countries) - 1):?>
                                    </optgroup><optgroup label="Популярные страны">
                                        <?endif;?>
                                        <?endforeach?>
                                    </optgroup>
                                </select>
                            </div>

                            <!-- Курорт -->
                            <input type="hidden" name="curortsList" id="curortsList" value="" />
                            <label for="mycustomscroll1" class="col-md-12 col-xs-12 form-label p0">Уточнить курорт:</label>
                            <div class="curort cur-position col-md-12 col-xs-12 p0">
                                <div class="cur form-control" id="curort-button">Курорт</div>
                                <div id="resorts">
                                    <div id='mycustomscroll1' class='scrolable'>
                                        <ul>
                                            <li><input type="checkbox" name="resortsAny" id="resortsAny" <?if(count($arParams["RESORTS"]) == 0):?>checked=""<?endif;?> /><label>Любой</label></li>
                                        </ul>
                                        <ul id="resortsList">
                                            <li class="scrolable-group-header">Популярные курорты</li>
                                            <?foreach($arResult["resorts"]["topCities"] as $resorts):?>
                                                <li><input type="checkbox" name="resortsList" onchange="createResortList()" value="<?=$resorts["Id"]?>" <?if(in_array($resorts["Id"], $arParams["RESORTS"])):?>checked<?endif;?> /><label><?=$resorts["Name"]?></label></li>
                                            <?endforeach?>
                                            <li class="scrolable-group-header">Остальные курорты</li>
                                            <?foreach($arResult["resorts"]["cities"] as $resorts):?>
                                                <li><input type="checkbox" name="resortsList" onchange="createResortList()" value="<?=$resorts["Id"]?>" <?if(in_array($resorts["Id"], $arParams["RESORTS"])):?>checked<?endif;?> /><label><?=$resorts["Name"]?></label></li>
                                            <?endforeach?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3 col-xs-12">
                            <label for="" class="col-md-12 form-label p0">Дата поездки:</label>
                            <div class="input-group date col-xs-6 col-md-6" id="" style="float: left">
                                <!-- Дата с -->
                                <input type="text" id="air_from"  value="<?=$arParams["DAY_FROM"]?>" placeholder="Вылет с" name="departFrom" class="form-control">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <div class="input-group date col-xs-6 col-md-6" id="">
                                <!-- Дата по -->
                                <input type="text" id="air_to" name="departTo" placeholder="Вылет по" value="<?=$arParams["DAY_TO"]?>" class="form-control" name="end">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>

                            <!-- Отели -->
                            <input type="hidden" name="hotels" id="hotel" value="" />
                            <label for="mycustomscroll2" class="col-md-12 col-xs-12 form-label p0">Уточнить отель:</label>
                            <div class="curort hotel-position col-md-12 col-xs-12 p0">
                                <div class="cur" style="padding-left: 2px">
                                    <input type="text" name="hotelsName"  value="<?if (trim($arParams["HOTELS_NAME"])):?><?=$arParams["HOTELS_NAME"]?><?endif?>" id="hotelsName"  class="placeholdered" placeholder="Введите назавание отеля" list="hotels_list_autocomplete" onkeyup="filterHotel(this)" />
                                    <datalist id="hotels_list_autocomplete">
                                    <?
                                        foreach($arResult["hotels"] as $hotels):
                                            ?><option value="<?=$hotels["Name"]?>"><?
                                        endforeach?></datalist>
                                    <!-- input type="button" class="hotelButton" value="Искать" id="hotelsSearch" -->
                                </div>
                                <div id="hotels">
                                    <div id='mycustomscroll2' class='scrolable'>
                                        <ul>
                                            <li><input type="checkbox" value="-" name="hotelsAny" id="hotelsAny" <?if(count($arParams["HOTELS"]) == 0):?>checked=""<?endif;?> /><label>Любой</label></li>
                                        </ul>
                                        <?$hotelsChecked = Array();?>
                                        <ul id="hotelsList">
                                            <?foreach($arResult["hotels"] as $hotels):?>
                                                <li><input type="checkbox" name="hotelsList[]" onchange="createHotelList()" value="<?=$hotels["Id"]?>" alt="<?=$hotels["Name"]?>" <?if(in_array($hotels["Id"], $arParams["HOTELS"])):	$hotelsChecked[] = $hotels["Id"];?>checked<?endif;?> /><label><?=$hotels["Name"]?> <?=$hotels["StarName"]?></label></li>
                                            <?endforeach?>
                                            <?foreach ($arParams["HOTELS"]  as $hotelId):
                                                if (!in_array($hotelId, $hotelsChecked)):?>
                                                    <input type="hidden" name="hotelsList[]" value="<?=$hotelId?>" />
                                               <?endif;
                                            endforeach?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 col-xs-12 hotelCat">
                            <!-- Категория отеля -->
                            <label for="" class="form-label">Категория отеля:</label>
                            <div class="form-block">
                                <input type="hidden" name="stars" id="stars" value="" />
                                <div class="checklist"><input type="checkbox" name="starsAny" id="starsAny" <?if(count($arParams["STARS"]) == 0):?>CHECKED<?endif;?>><span>Любое</span></div>
                                <div id="starsList">
                                    <?$nonUsedStars = array(400,410,411)?>
                                    <?foreach($arResult["stars"] as $stars):?>
                                        <?if (!in_array($stars["Id"], $nonUsedStars)):?>
                                            <div class="checklist"><input type="checkbox" name="starsList[]" onchange="createStarList()" value="<?=$stars["Id"]?>" <?if(in_array($stars["Id"], $arParams["STARS"])):?>CHECKED<?endif;?> /><span><?=$stars["Name"]?></span></div>
                                        <?endif?>
                                    <?endforeach?>
                                </div>
                            </div>

                            <!-- Питание -->
                            <label for="" class="form-label" style="margin-top: 5px">Тип питания:</label>
                            <div class="form-block">
                                <input type="hidden" name="meals" id="meals" value="" />
                                <div class="checklist"><input type="checkbox" name="mealsAny" id="mealsAny" <?if(count($arParams["MEALS"]) == 0):?>CHECKED<?endif;?> /><span>Любое</span></div>
                                <div id="mealsList">
                                    <?foreach($arResult["arSortMeals"] as $meals):?>
                                    <div class="checklist"><input type="checkbox" onchange="createMealList()" name="mealsList[]" value="<?=$meals["Id"]?>" <?if(in_array($meals["Id"], $arResult["checkedMeals"])):?>CHECKED<?endif;?> /><span><?=$meals["Name"]?></span></div>
                                    <?endforeach?>
                                </div>
                            </div>
                        </div>

                        <div class="select-box col-md-3 col-xs-12">
                            <label for="" class="col-md-12 form-label">Ночей:</label>
                            <div class="select-box">
                                <!-- Количество дней ночей -->
                                <div class="col-md-6 col-xs-6">
                                    <select class="nightfrom form-control" name="nightsMin" id="nightsMin" name="night-from">
                                        <?for($i=1; $i<=30; $i++){?>
                                            <option value="<?=$i?>" <?if($arParams["NIGHT_FROM"] == $i):?>selected<?endif;?>><?=$i?></option>
                                        <?}?>
                                    </select>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <select class="nightfrom form-control" name="nightsMax" id="nightsMax" name="night-to">
                                        <?for($i=1; $i<=30; $i++){?>
                                            <option value="<?=$i?>" <?if($arParams["NIGHT_TO"] == $i):?>selected<?endif;?>><?=$i?></option>
                                        <?}?>
                                    </select>
                                </div>
                            </div>

                            <!-- Количество взрослых -->
                            <div class="col-md-6 col-xs-6">
                                <label for="" class="form-label">Взрослых:</label>
                                <select name="adults" id="adults" class="sCombo form-control">
                                    <?for($i=0; $i<=4; $i++){?>
                                        <option value="<?=$i?>"<?if($arParams["ADULTS"] == $i):?> selected<?endif;?>><?=$i?></option>
                                    <?}?>
                                </select>
                            </div>

                            <!-- Количество детей -->

                            <div class="col-md-6 col-xs-6">
                                <label for="" class="form-label">Детей:</label>
                                <select name="kids" id="kids" class="sCombo form-control">
                                    <?for($i=0; $i<=3; $i++){?>
                                        <option value="<?=$i?>"<?if($arParams["KIDS"] == $i):?> selected<?endif;?>><?=$i?></option>
                                    <?}?>
                                </select>
                            </div>

                            <!-- Возраст детей -->
                            <label class="col-md-12 form-label">Возраст детей</label>
                            <div class="col-md-12  selectChild" id="i-kidages-caption" style="">
                                <select name="kid1" id="kid1" class="sCombo form-control">
                                    <?for($i=0; $i<=15; $i++){?>
                                        <option value="<?=$i?>"<?if($arParams["KID1"] == $i):?> selected<?endif;?>><?=$i?></option>
                                    <?}?>
                                </select>
                                <select name="kid2" id="kid2" class="sCombo form-control" >
                                    <?for($i=0; $i<=15; $i++){?>
                                        <option value="<?=$i?>"<?if($arParams["KID2"] == $i):?> selected<?endif;?>><?=$i?></option>
                                    <?}?>
                                </select>
                                <select name="kid3" id="kid3" class="sCombo form-control">
                                    <?for($i=0; $i<=15; $i++){?>
                                        <option value="<?=$i?>"<?if($arParams["KID3"] == $i):?> selected<?endif;?>><?=$i?></option>
                                    <?}?>
                                </select>
                            </div>

                            <!-- Цены от и до -->
                            <label for="" class="col-md-12 form-label">Цена:</label>
                            <div class="price-pick">
                                <div class="col-md-6 col-xs-6">
                                    <input name="priceMin" id="priceMin"  class="form-control input-md" type="text" placeholder='от руб' value="<?=$arParams["PRICE_MIN"]?>"/>
                                </div>

                                <div class="col-md-6 col-xs-6">
                                    <input name="priceMax" id="priceMax" class="form-control input-md" type="text" placeholder='до руб' value="<?=$arParams["PRICE_MAX"]?>"/>
                                </div>
                            </div>
                            <div class="form-bottom col-md-12 col-xs-12">
                                <input type="submit" class="search-tour btn btn-success" style="width:100%" value="Поехали!" /> <!--onClick="SendFormSearchTour(1,<?=$arParams["COUNT"]?>,<?=$arParams["SALE"]?>);"-->
                            </div>
                            <!-- Валюта -->
                            <div class="col-md-6" style="visibility: hidden">
                                <div id="i-currency col-md-12">
                                    <input type="radio" <?if($arParams["PRICE_CURRENCY"] == "RUB"):?>checked="checked"<?endif;?> value="RUB" name="currencyAlias" /><label><span>RUB</span></label>
                                    <input type="radio" <?if($arParams["PRICE_CURRENCY"] == "USD"):?>checked="checked"<?endif;?> value="USD" name="currencyAlias" /><label><span>USD</span></label>
                                    <input type="radio" <?if($arParams["PRICE_CURRENCY"] == "EUR"):?>checked="checked"<?endif;?> value="EUR" name="currencyAlias" /><label><span>EUR</span></label>
                                </div>
                            </div>


                        </div>

                    </div>
                </form>
            </div>
        </div> <!-- /row -->
    </div><!-- /container -->

    </div>
</section><!-- /home-section -->