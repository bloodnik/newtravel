<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["SALE"]=intval($arParams["SALE_PERCENT"]);?>
<script type="text/javascript">
if(window.jQuery==undefined) {
	alert('Необходимо подключить библиотеку jQuery');
}
$(function(){
	$("#air_from").datepicker({
		dateFormat:"dd.mm.yy",
		monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
		dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
		dayNamesShort: ['Вск', 'Пнд', 'Втр', 'Срд', 'Чтв', 'Птн', 'Сбт'],
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
		minDate: "+1d",
		maxDate: "+1y",
		changeYear: true,
		onSelect: function(){$(this).prev("label").text('');}
	    });
	$("#air_to").datepicker({
		dateFormat:"dd.mm.yy",
		monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
		dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
		dayNamesShort: ['Вск', 'Пнд', 'Втр', 'Срд', 'Чтв', 'Птн', 'Сбт'],
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
		minDate: "+3d",
		maxDate: "+1y",
		changeYear: true,
		onSelect: function(){$(this).prev("label").text('');}
	    });
	// Изменение города отправления
	$('#travelCityFrom').change(function() {
		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "country", from: $('#travelCityFrom').val()}, function(json){
			$('#travelCountryTo').empty();
			$('#travelCountryTo option').remove();
			$('#travelCountryTo').append( $('<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>') );
			$.each(json.results,function(i,country){
				$('#travelCountryTo').append( $('<option value="'+country.Id+'">'+country.Name+'</option>') );
			});
			$(".select-box select").selectBox("destroy");
			$(".select-box select").selectBox();
		});

		// Сбрасываем все остальные настройки
		$('#resortsList').empty();
		$('#resortsList option').remove();
		$("#resortsAny").attr("checked","checked");

		$('#starsList').empty();
		$('#starsList option').remove();
		$("#starsAny").attr("checked","checked");

		$('#hotelsList').empty();
		$('#hotelsList option').remove();
		$("#hotelsAny").attr("checked","checked");
	});

	// Изменение страны назначения
	$('#travelCountryTo').change(function() {
		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "resort", country: $('#travelCountryTo').val()}, function(json){
			$('#resortsList').empty();
			$('#resortsList option').remove();
			//Заполняем курорты по приоритету - 01.07.2014			
			$('#resortsList').append( $('<li class="scrolable-group-header">Популярные курорты</li>') );
			$.each(json.results.topCities,function(i,resort){
				$('#resortsList').append( $('<li><input type="checkbox" name="resortsList[]" value="'+resort.Id+'"><label>'+resort.Name+'</label></li>') ); //Популярные курорты
			});
			$('#resortsList').append( $('<li class="scrolable-group-header">Остальные курорты</li>') );
			$.each(json.results.cities,function(i,resort){
				$('#resortsList').append( $('<li><input type="checkbox" name="resortsList[]" value="'+resort.Id+'"><label>'+resort.Name+'</label></li>') ); //Остальные курорты
			});			
      
      // reload flexcroll
      //fleXenv.fleXcrollMain("mycustomscroll");

			// Устанавливаем галочки по умолчанию
			$("#resortsAny").attr("checked","checked");
			$("#hotelsAny").attr("checked","checked");

            // Изменяем доступные категории отелей
            GetHotelStars();

			// Снятие/установка галочки "Любой курорт"
			$('#resortsList input:checkbox').change(function() {
				$("#resortsAny").attr("checked","checked");
				$("#resortsList input:checkbox:checked").each(function() {
					$("#resortsAny").removeAttr("checked");
				});
				// Изменение категорий отелей при смене списка курортов
				GetHotelStars();
			});

		});
	});
	//
	// Зависимые изменения
	//

	// Получаем список всех доступных категорий отелей
	function GetHotelStars() {
		// Составляем список курортов
		var resorts = "";
		if($("#resortsAny").attr("checked") !== "checked") {
			$("#resortsList input:checkbox:checked").each(function(i,resort) {
				resorts = resorts+resort.value+",";
			});
		}

		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "stars", country: $('#travelCountryTo').val(), resorts: resorts}, function(json){
			$('#starsList').empty();
			$('#starsList option').remove();
			$("#starsAny").attr("checked","checked");
			$.each(json.results,function(i,star){
				$('#starsList').append( $('<div class="c-row"><input type="checkbox" name="starsList[]" value="'+star.Id+'"><span>'+star.Name+'</span></div>') );
			});

			// Снятие/установка галочки "Любая категория отеля"
			$('#starsList input:checkbox').change(function() {
				$("#starsAny").attr("checked","checked");
				$("#starsList input:checkbox:checked").each(function() {
					$("#starsAny").removeAttr("checked");
				});
				GetHotelList();
			});


			// Изменение списка отелей
			GetHotelList();
		});
	}


	// Получаем список всех доступных отелей
	function GetHotelList() {

		// Составляем список курортов
		var resorts = "";
		if($("#resortsAny").attr("checked") !== "checked") {
			$("#resortsList input:checkbox:checked").each(function(i,resort) {
				resorts = resorts+resort.value+",";
			});
		}

		// Составляем список категорий
		var stars = "";
		if($("#starsAny").attr("checked") !== "checked") {
			$("#starsList input:checkbox:checked").each(function(i,star) {
				stars = stars+star.value+",";
			});
		}
		// Если введен фрагмент имени
		var filter = "";
		if($("#hotelsName").attr("value") !== "") {
			filter = $("#hotelsName").attr("value");
		}

		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "hotels", country: $('#travelCountryTo').val(), resorts: resorts, stars: stars, filter: filter}, function(json){
			
			var checked_hotels = new Array();
			var j = 0;
			$("#hotelsList input:checkbox:checked").each(function(i,hotel) {
				checked_hotels[j] = '<li><input type="checkbox" checked name="hotelsList[]" alt="'+hotel.alt+'" value="'+hotel.value+'"><label>'+hotel.alt+'</label></li>';
				j++;
			});
			
			$('#hotelsList').empty();
			$('#hotelsList option').remove();
			$('#hotels_list_autocomplete').empty();
			$('#hotels_list_autocomplete option').remove();

			if (checked_hotels[0]==undefined) {				
				$("#hotelsAny").attr("checked","checked");				
			}
			
			$.each(checked_hotels,function(i,hotel){
				$('#hotelsList').append( $(hotel) );
			});
			
			$.each(json.results,function(i,hotel){
				$('#hotelsList').append( $('<li><input type="checkbox" name="hotelsList[]" alt="'+hotel.Name+'" value="'+hotel.Id+'"><label>'+hotel.Name+' '+hotel.StarName+'</label></li>') );			
				$('#hotels_list_autocomplete').append( $('<option value="'+hotel.Name+'">') );
			});

			// Снятие/установка галочки "Любой отель"
			$('#hotelsList input:checkbox').change(function() {
				$("#hotelsAny").attr("checked","checked");
				$("#hotelsList input:checkbox:checked").each(function() {
					$("#hotelsAny").removeAttr("checked");
				});
			});
			
			$('.jClever').jCleverAPI("destroy");
			$('.jClever').jClever({
				applyTo:{
				checkbox: true,
				radio: true,
				button: false,
				file: false,
				input: false,
				textarea: false
					}
					}
				);

		});
	}	
	
	// Поиск отелей
	$('#hotelsSearch').click(function() {
		GetHotelList();		
	});

	//
	// Обработчики галочек
	//
	// Галочка "Все курорты"
	$("#resortsAny").live("change", function(){
      GetHotelStars();
    });
	// Снятие/установка галочки "Любой курорт"
	$("#resortsAny").live("change", function(){
		if ($(this).attr("checked") == "checked") {
			$("#resortsList input:checkbox:checked").each(function() {
				$(this).removeAttr("checked");
			});		
		}
	});
	$("#resortsList input:checkbox").live("change", function(){
      $("#resortsAny").attr("checked","checked");
		$("#resortsList input:checkbox:checked").each(function() {
			$("#resortsAny").removeAttr("checked");
		});
		// Изменение категорий отелей при смене списка курортов
		GetHotelStars();
    });




	/*$('#resortsList input:checkbox').change(function() {
		$("#resortsAny").attr("checked","checked");
		$("#resortsList input:checkbox:checked").each(function() {
			$("#resortsAny").removeAttr("checked");
		});
		// Изменение категорий отелей при смене списка курортов
		GetHotelStars();
	});*/



	// Снятие/установка галочки "Любая категория отеля"
	$("#starsList input:checkbox").live("change", function(){
      $("#starsAny").attr("checked","checked");
		$("#starsList input:checkbox:checked").each(function() {
			$("#starsAny").removeAttr("checked");
		});
		GetHotelList();
    });
	/*$('#starsList input:checkbox').change(function() {
		$("#starsAny").attr("checked","checked");
		$("#starsList input:checkbox:checked").each(function() {
			$("#starsAny").removeAttr("checked");
		});
		GetHotelList();
	});*/

	// Галочка "Все категории отелей"
	$("#starsAny").live("change", function(){
      GetHotelStars();
    });
	// Снятие/установка галочки "Любой отель"
	$("#hotelsAny").live("change", function(){
		if ($(this).attr("checked") == "checked") {
			$("#hotelsList input:checkbox:checked").each(function() {
				$(this).removeAttr("checked");
			});
			GetHotelList();
		}			
	});	
	$("#hotelsList input:checkbox").live("change", function(){
      $("#hotelsAny").attr("checked","checked");
		$("#hotelsList input:checkbox:checked").each(function() {
			$("#hotelsAny").removeAttr("checked");
		});
		$('.jClever').jCleverAPI("destroy");
			$('.jClever').jClever({
				applyTo:{
				checkbox: true,
				radio: true,
				button: false,
				file: false,
				input: false,
				textarea: false
					}
					}
				);
    });

	// Снятие/установка галочки "Любая категория питания"
	$("#mealsList input:checkbox").live("change", function(){
      $("#mealsAny").attr("checked","checked");
		$("#mealsList input:checkbox:checked").each(function() {
			$("#mealsAny").removeAttr("checked");
		});
		$('.jClever').jCleverAPI("destroy");
			$('.jClever').jClever({
				applyTo:{
				checkbox: true,
				radio: true,
				button: false,
				file: false,
				input: false,
				textarea: false
					}
					}
				);
    });
});

// 
// Разбор GET
//
function parseGetParams() {
   var $_GET = {};
   var __GET = window.location.search.substring(1).split("&");
   for(var i=0; i<__GET.length; i++) {
      var getVar = __GET[i].split("=");
      $_GET[getVar[0]] = typeof(getVar[1])=="undefined" ? "" : getVar[1];
   }
   return $_GET;
}


//
// Отправка формы поиска туров
//
function SendFormSearchTour(page, count, num) {
	var emailVal = $("#email").val();
	
	if (emailVal.length > 0) {
		$(".error").html("<div>Вы бот</div>");
		return;
	}
	
	// Показываем что ищем туры
	$("#searchTourStatus").css('display', 'block');
	$("#request").css('display', 'none');

	$.post("/bitrix/tools/simai.travel_ajax.php", $("#searchTour").serialize(), function(RequestResult) {
		$("#request").html(RequestResult+'<br>');

		CheckFormSearchTourStatus(RequestResult, 5, page, count, num);
	});
}



//
// Ожидание статуса обработки
//
function CheckFormSearchTourStatus (RequestResult, time, page, count, num) {
	// Делаем запрос статуса
	$.get("/bitrix/tools/simai.travel_ajax.php", { id: RequestResult, type: "searchStatus" }, function(status) {
		//$("#request").append(status+'<br>');
		if(status != "load") {
			setTimeout(function() {
				//$('#request').append('Проверка статуса, прошло '+time+' сек<br>');
				time += 5;
				CheckFormSearchTourStatus(RequestResult, time, page, count, num);
			}, 5000);
		}
		else {
			//$('#request').append('Данные обработаны за '+time+' сек<br>');
			SearchTourData(RequestResult, page, count, num);
		}
	});
}

// Отображение результата данных
function SearchTourData (RequestResult, page, count, num) {
	/*var curGetVars = parseGetParams();
	var captcha_word = curGetVars.captcha_word;
	var captcha_sid = curGetVars.captcha_sid;
	if (captcha_word == "")	{
		captcha_word = $('#captcha_word').val();
	}
	if (captcha_sid == "") {
		captcha_sid = $('#captcha_sid').val();
	}*/
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: page, count: count, sale: num/*, captcha_word: captcha_word,  captcha_sid: captcha_sid*/}, function(data) {
		$("#searchTourStatus").css('display', 'none');
		$("#request").css('display', 'block');
		$('#request').html(data);
	});
}

// Покупка тура
function SearchTourDataBuy (RequestResult, page, count, tour, num) {
if(num=="undefind")num=0;
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: page, count: count, tourBuy: tour, sale: num }, function(data) {
		$('#request').html(data);

		// Плюсуем кол-во элементов в корзине
		var count = ($('#sidebar_product_count').html()*1)+1;
		$('#sidebar_product_count').html(count);

		// Показываем человеку что тур куплен
		var r=confirm("Перейти в корзину для оформления тура?")
		if (r==true)
		{
			window.location = '/personal/cart/';
		}
		else
		{

		}
		$.get("/personal/cart/system.php", {ajax: "Y"}, function(data){$('#cartLine').html(data);});
	});
}



//
// Если переданы параметры то сразу ищем туры
//

$(document).ready(function() {
	var curGet = parseGetParams();
	if(curGet.start == "Y") {		
		SendFormSearchTour(1, curGet.count, sale);
	}
	colKid=$("#kids").val();
	if(typeof(colKid)!="undefined" && colKid==0){
		$("#kid1+a,#kid2+a,#kid3+a").css("display","none").parent().prev().css("display","none");
	}
	$("#kids").change(function(){
		colKid=$(this).val();
		if(typeof(colKid)!="undefined"){
			if(colKid==0){
				$("#kid1+a,#kid2+a,#kid3+a").css("display","none").parent().prev().css("display","none");
				
			}
			else if(colKid==1){
				$("#kid1+a").css("display","inline-block").parent().prev().css("display","block");
				$("#kid2+a,#kid2+a").css("display","none");
			}
			else if(colKid==2){
				$("#kid1+a,#kid2+a").css("display","inline-block").parent().prev().css("display","block");
				$("#kid3+a").css("display","none");
			}
			else if(colKid==3){
				$("#kid1+a,#kid2+a,#kid3+a").css("display","inline-block").parent().prev().css("display","block");
			}
		}
		});
});
//
// Формирование ссылки на страницу поиска
//
function getLink() {
	var link = 'http://' + window.location.host + document.location.pathname + '?start=Y&count=10';
	link += '&date=' +  $('#air_from').val() + '&date2=' +  $('#air_to').val();
	link += '&night=' +  $('#nightsMin').val() + '-' +  $('#nightsMax').val();
	link += '&adults=' +  $('#adults').val() + '&kids=' +  $('#kids').val() + '&kid1=' +  $('#kid1').val() + '&kid2=' +  $('#kid2').val() + '&kid3=' +  $('#kid3').val();
	link += '&priceMin=' +  $('#priceMin').val() + '&priceMax=' +  $('#priceMax').val() + '&currencyAlias=' + $('input:radio[name=currencyAlias]:checked').val();

	link += '&cityFromId=' +  $('#travelCityFrom').val() + '&countryId=' +  $('#travelCountryTo').val();

	// Список курортов
	var resortsList = "";
	$('#resortsList li input:checkbox:checked').each(function(nf, inputData)
	{
		if (resortsList !== "") resortsList += ',';
		resortsList += $(inputData).val();
	});
	link += '&resortsList=' +  resortsList;

	// Список питания
	var mealsList = "";
	$('#mealsList div input:checkbox:checked').each(function(nf, inputData)
	{
		if (mealsList !== "") mealsList += ',';
		mealsList += $(inputData).val();
	});
	link += '&mealsList=' +  mealsList;

	// Список звезд
	var starsList = "";
	$('#starsList div input:checkbox:checked').each(function(nf, inputData)
	{
		if (starsList !== "") starsList += ',';
		starsList += $(inputData).val();
	});
	link += '&starsList=' +  starsList;

	// Отели
	var hotelsList = "";
	$('#hotelsList li input:checkbox:checked').each(function(nf, inputData)
	{
		if (hotelsList !== "") hotelsList += ',';
		hotelsList += $(inputData).val();
	});
	link += '&hotelsList=' +  hotelsList;


	//$('#getLink').html(link);
	prompt('Ссылка на страницу поиска:', link);
}
var sale=<?=$arParams["SALE"]?>
</script>

<?
$countries_temp = $arResult["to"];
$arResult["to"] = Array();
$top_countries = Array(
	"Болгария",
	"Вьетнам",
	"Греция",
	"Египет",
	"Израиль",
	"Испания",
	"Италия",
	"Кипр",
	"ОАЭ",
	"Индия",
	"Таиланд",
	"Тунис",
	"Турция",
	"Хорватия",
	"Черногория",
	"Чехия",
	"Шри-Ланка"
);	
foreach ($top_countries as $cname):
	foreach($countries_temp as $i=>$country):	
		if ($country["Name"] == $cname):
			$arResult["to"][] = $country;
			unset($countries_temp[$i]);
		endif;
	endforeach;
endforeach;
foreach($countries_temp as $i=>$country):
	$arResult["to"][] = $country;	
endforeach;
//
//Начало | Фильтрация приоритеов по курортам при открытии из малой формы| 01.07.2014
//
$resortCustomList = Array(
	"119" => Array(
		"34" => "Аланья",
		"72" => "Анталья", 
		"149" => "Белек",
		"363" => "Даламан", 
		"566" => "Кемер", 
		"4268" => "Манавгат", 
		"765" => "Мармарис",
		"1334" => "Сиде",
		"1365" => "Стамбул",
		"1531" => "Фетхие",
	),
	"40" => Array (
		"746" => "Макади",	
		"767" => "Марса Алам, Эль Кусейр",	
		"893" => "Нувейба",	
		"1298" => "Сафага",	
		"1355" => "Сома Бей",	
		"1394" => "Таба",	
		"1592" => "Хургада",	
		"1642" => "Шарм-Эль-Шейх",	
		"1662" => "Эль Гуна",
	),
);

if(array_key_exists($_REQUEST["countryId"],$resortCustomList)) {
	$customResorts = $resortCustomList[$_REQUEST["countryId"]];
}

if(!isset($_REQUEST["countryId"])) {
	$customResorts = $resortCustomList[$arParams["COUNTRY"]];	
}	

if (is_array($customResorts)) {
	$topCities = Array();
	$cities = Array();
	foreach ($arResult["resorts"] as $arr) {
		if (array_key_exists($arr["Id"],$customResorts)) {
			$topCities[] = $arr;
		} 
		else {
			$cities[] = $arr;
		}		
	}
}
else{
	$topCities = Array();
	$cities = Array();
	foreach ($arResult["resorts"] as $arr) {
		$topCities[] = $arr;
	}
}		
	$arResult["resorts"]["topCities"] = $topCities;
	$arResult["resorts"]["cities"] = $cities;
//
//Конец | Фильтрация приоритеов по курортам при открытии из малой формы | 01.07.2014
//

?>

<div class="fast-search-tour">
<h1>БЫСТРЫЙ ПОИСК ТУРА</h1>
<p>Хотите хорошо отдохнуть? Легко! <b>УМНЫЕ ТУРИСТЫ</b> - это ЕДИНАЯ база всех СПЕЦпредложений и ГОРЯЩИХ ТУРОВ по ИНТЕРНЕТ-ТАРИФАМ</p>
<div class="tour-form">
 <div class="tour-form-in">
<form class="" action="<?=$arParams["ACTION"]?>" name="searchTour" id="searchTour" method="GET" <?if($arParams["NEW"] == "Y"):?>target="new"<?endif;?>>
<input type="hidden" name="type" value="search">
<div class="form-col first">
<div class="select-box">
			<select name="cityFromId" id="travelCityFrom">
				<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>
				<?foreach($arResult["from"] as $city):?>
					<option value="<?=$city["Id"]?>" <?if($arParams["CITY"] == $city["Id"]):?>SELECTED<?endif;?>><?=$city["Name"]?></option>
				<?endforeach?>
			</select>
	</div>
	<div class="curort">
			<div class="cur">Курорт</div>
			<div id="resorts">
			<div id='mycustomscroll' class='scrolable'>
			<ul>
			<li><input type="checkbox" name="resortsAny" id="resortsAny" <?if(count($arParams["RESORTS"]) == 0):?>checked=""<?endif;?>><label>Любой</label></li>
			</ul>
				<ul id="resortsList">
					<li class="scrolable-group-header">Популярные курорты</li>					
					<?foreach($arResult["resorts"]["topCities"] as $resorts):?>
						<li><input type="checkbox" name="resortsList[]" value="<?=$resorts["Id"]?>" <?if(in_array($resorts["Id"], $arParams["RESORTS"])):?>checked<?endif;?>><label><?=$resorts["Name"]?></label></li>
					<?endforeach?>
					<li class="scrolable-group-header">Остальные курорты</li>
					<?foreach($arResult["resorts"]["cities"] as $resorts):?>
						<li><input type="checkbox" name="resortsList[]" value="<?=$resorts["Id"]?>" <?if(in_array($resorts["Id"], $arParams["RESORTS"])):?>checked<?endif;?>><label><?=$resorts["Name"]?></label></li>
					<?endforeach?>					
				</ul>
			</div>
			</div>
</div>
</div>
<div class="form-col">
<div class="select-box">
			<select name="countryId" id="travelCountryTo">
				<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>
				<optgroup label="Топовые страны">
				<?foreach($arResult["to"] as $i=>$country):?>
					<option value="<?=$country["Id"]?>" <?if($arParams["COUNTRY"] == $country["Id"]):?>SELECTED<?endif;?>><?=$country["Name"]?></option>
					<?if ($i == count($top_countries) - 1):?>
				</optgroup><optgroup label="Популярные страны">
					<?endif;?>
				<?endforeach?>
				</optgroup>
			</select>
			</div>
	<div class="curort">
	
	<div class="cur">
	<input type="text" name="hotelsName" value="<?if (trim($arParams["HOTELS_NAME"])):?><?=$arParams["HOTELS_NAME"]?><?endif?>" id="hotelsName"  class="placeholdered" list="hotels_list_autocomplete">
	<datalist id="hotels_list_autocomplete"><?
					foreach($arResult["hotels"] as $hotels):
						?><option value="<?=$hotels["Name"]?>"><?
					endforeach?></datalist>	
	<input type="button" class="hotelButton" value="Искать" id="hotelsSearch">
	</div>
	
	<div class="cur">Отель</div>
			<div id="hotels">
				<div id='mycustomscroll' class='scrolable'>
				<ul>
					<li><input type="checkbox" value="-" name="hotelsAny" id="hotelsAny" <?if(count($arParams["HOTELS"]) == 0):?>checked=""<?endif;?>><label>Любой</label></li>
				</ul>
					<?$hotelsChecked = Array();?>				
					<ul id="hotelsList">
						<?foreach($arResult["hotels"] as $hotels):?>
							<li><input type="checkbox" name="hotelsList[]" value="<?=$hotels["Id"]?>" alt="<?=$hotels["Name"]?>" <?if(in_array($hotels["Id"], $arParams["HOTELS"])):	$hotelsChecked[] = $hotels["Id"];?>checked<?endif;?>><label><?=$hotels["Name"]?> <?=$hotels["StarName"]?></label></li>
						<?endforeach?>
						<?foreach ($arParams["HOTELS"]  as $hotelId):
							if (!in_array($hotelId, $hotelsChecked)):?>
							<input type="hidden" name="hotelsList[]" value="<?=$hotelId?>">
							<?endif;
						endforeach?>
					</ul>
				</div>
			</div>
	</div>
</div>
<div class="form-col froms">
 <div class="date-pick">
     <input id="air_from" value="<?=$arParams["DAY_FROM"]?>" placeholder="Вылет с" type="text" name="departFrom" class="from" />
 </div>
				<label class="cat-label select"><b>Ночей с</b></label>
				<label class="cat-label select right"><b>Ночей до</b></label>
				<div class="select-box">
				<select class="nightfrom" name="nightsMin" id="nightsMin" name="night-from">
					<?for($i=1; $i<=30; $i++){?>
						<option value="<?=$i?>" <?if($arParams["NIGHT_FROM"] == $i):?>selected<?endif;?>><?=$i?></option>
					<?}?>
				</select>
				<select class="nightfrom" name="nightsMax" id="nightsMax" name="night-to">
					<?for($i=1; $i<=30; $i++){?>
						<option value="<?=$i?>" <?if($arParams["NIGHT_TO"] == $i):?>selected<?endif;?>><?=$i?></option>
					<?}?>
				</select>
				</div>
				<div class="fl">
				 <label class="cat-label"><b>Категория:</b></label>
				  <div class="c-row"><input type="checkbox" name="starsAny" id="starsAny" <?if(count($arParams["STARS"]) == 0):?>CHECKED<?endif;?>><span>Любое</span></div>
						  <div id="starsList">
							<?foreach($arResult["stars"] as $stars):?>
								 <div class="c-row"><input type="checkbox" name="starsList[]" value="<?=$stars["Id"]?>" <?if(in_array($stars["Id"], $arParams["STARS"])):?>CHECKED<?endif;?>><span><?=$stars["Name"]?></span></div>
							<?endforeach?>
						</div>
				</div>
				<div class="fr">

				 <label class="cat-label"><b>Питание:</b></label>

                          <div class="c-row"><input type="checkbox" name="mealsAny" id="mealsAny" <?if(count($arParams["MEALS"]) == 0):?>CHECKED<?endif;?>><span>Любое</span></div>
						  <div id="mealsList">
							<?foreach($arResult["meals"] as $meals):?>
								 <div class="c-row" title="<?=getMessage("MEAL_TYPE_".toUpper($meals["Name"]));?>"><input type="checkbox" name="mealsList[]" value="<?=$meals["Id"]?>" <?if(in_array($meals["Id"], $arParams["MEALS"])):?>CHECKED<?endif;?>><span><?=$meals["Name"]?></span></div>
							<?endforeach?>
						</div>

				</div>
</div>
<div class="form-col last-col">
<div class="row">
      <div class="date-pick">
           <input value="<?=$arParams["DAY_TO"]?>"  placeholder="Вылет по" id="air_to" type="text" name="departTo" class="from" />
      </div>
	</div>
	 <div class="select-box">
			<label class="child-age">Кол-во взрослых и детей</label>
					<select style="width:58px" name="adults" id="adults" class="sCombo">
						<?for($i=0; $i<=4; $i++){?>
							<option value="<?=$i?>"<?if($arParams["ADULTS"] == $i):?> selected<?endif;?>><?=$i?></option>
						<?}?>
					</select>

					<select style="width:58px" name="kids" id="kids" class="sCombo">
						<?for($i=0; $i<=3; $i++){?>
							<option value="<?=$i?>"<?if($arParams["KIDS"] == $i):?> selected<?endif;?>><?=$i?></option>
						<?}?>
					</select>

     </div>
	 <label class="child-age">Возраст детей</label>
			<div class="select-box selectChild" id="i-kidages-caption" style="padding-top:10px;padding-bottom:20px">
				<select name="kid1" id="kid1">
					<?for($i=0; $i<=15; $i++){?>
						<option value="<?=$i?>"<?if($arParams["KID1"] == $i):?> selected<?endif;?>><?=$i?></option>
					<?}?>
				</select>
				<select name="kid2" id="kid2">
					<?for($i=0; $i<=15; $i++){?>
						<option value="<?=$i?>"<?if($arParams["KID2"] == $i):?> selected<?endif;?>><?=$i?></option>
					<?}?>
				</select>
				<select name="kid3" id="kid3">
					<?for($i=0; $i<=15; $i++){?>
						<option value="<?=$i?>"<?if($arParams["KID3"] == $i):?> selected<?endif;?>><?=$i?></option>
					<?}?>
				</select>

</div>
<div style="clear:both;"></div>
         <div class="price-pick">
              <label class="all-price">Цена от :</label>
              <input name="priceMin" id="priceMin" type="text" value="<?=$arParams["PRICE_MIN"]?>"/>
               <label>До</label>
              <input name="priceMax" id="priceMax" type="text" value="<?=$arParams["PRICE_MAX"]?>"/>
	</div>

</div>
	<div style="clear:both;"></div>
	<div class="form-bottom">
	<div class="form-bot-in">
	<label><b>Валюта:</b></label>
	<div id="i-currency">
				<input type="radio" <?if($arParams["PRICE_CURRENCY"] == "RUB"):?>checked="checked"<?endif;?> value="RUB" name="currencyAlias"><label><span>RUB</span></label>
				<input type="radio" <?if($arParams["PRICE_CURRENCY"] == "USD"):?>checked="checked"<?endif;?> value="USD" name="currencyAlias"><label><span>USD</span></label>
				<input type="radio" <?if($arParams["PRICE_CURRENCY"] == "EUR"):?>checked="checked"<?endif;?> value="EUR" name="currencyAlias"><label><span>EUR</span></label>
			</div>
	</div>
	
	<?/*<div class="form-bot-in">
	<label><b>Введите код:</b></label>
	<?
	include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
	$capCode = $GLOBALS["APPLICATION"]->CaptchaGetCode();
	echo '<img src="/bitrix/tools/captcha.php?captcha_sid='.htmlspecialchars($capCode).'" style="margin-top:-10px; width:120px; padding-left:5px">';
	echo '<input type="hidden" id="captcha_sid" name="captcha_sid" value="'.htmlspecialchars($capCode).'">';
	?>
	<input type="text" id="captcha_word" name="captcha_word" size="6" maxlength="20" value="">
	</div>*/?>
	 <input name="email" id="email" type="text" value="" style="position: absolute;background: none;border: 0;right: 0;">
	 <div class="error"></div>
	<a href="#request" class="search-tour button button-large" onClick="SendFormSearchTour(1,<?=$arParams["COUNT"]?>,<?=$arParams["SALE"]?>);">Искать Туры!</a>
  </div>
  <div class="button-link" style="float: left;">
  <a class="skidko" href="javascript:getLink();" title="Получить ссылку на список туров"></a>
Получить ссылку:
</div>
</form>
</div>
</div>
</div>
<div style="clear:both;"></div>

<div id="request" style="display: none;"></div>
 <div style="clear:both;"></div>
            <div id="searchTourStatus" class="form-result" style="display: none;">
              <div class="form-res-in">
			  <div id="angLoading" style="text-align:center"><img align="center"  src="http://ui.sletat.ru/gfx/ld2.gif"></div>
			  <div id="angContainer">
                <div class="error-txt"> Наш портал ищет для Вас лучшие предложения.<br>Пожалуйста, ожидайте...</div>
				</div>
               </div>
            </div>
