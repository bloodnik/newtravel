<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<!-- =========================================
Tour Section ( Slider )
========================================== -->
<!-- tour-section -->
<section id="tour-section" class="tour-section parallax">
    <div class="overlay-grid">
        <div class="tabs-box">

            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-tour-search" data-toggle="tab"></i>Поиск туров</a></li>
                            <li><a href="#tab-avia" data-toggle="tab"></i>Авиабилеты</a></li>
                            <li><a href="#tab-hotels" data-toggle="tab"></i>Отели</a></li>
                        </ul>
                    </div><!-- /col-md-12 -->

                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab-tour-search">
                            <div class="container">
                                <div class="tv-search-form" tv-moduleid="100408"></div>
                                <script type="text/javascript" src="http://tourvisor.ru/module/init.js"></script>
                            </div>
                        </div><!-- /tab-pane -->

                        <div class="tab-pane fade in" id="tab-avia">
                            <div class="container">
                                <iframe scrolling="no" width="940" height="177" frameborder="0" src="//www.travelpayouts.com/widgets/611fc99ac1ed5a38305750df9db9b1c7.html?v=380"></iframe>
                            </div>
                        </div><!-- /tab-pane -->

                        <div class="tab-pane fade in" id="tab-hotels">
                            <div class="container">
                                <iframe scrolling="no" width="940" height="177" frameborder="0" src="//www.travelpayouts.com/widgets/63043538e09fdc61ac368fc239c18128.html?v=386"></iframe>
                            </div>
                        </div><!-- /tab-pane -->

                    </div><!-- /tab-content -->

                </div><!-- /row -->
            </div><!-- /container -->
        </div><!-- /tabs-box -->

    </div>
</section><!-- /home-section -->