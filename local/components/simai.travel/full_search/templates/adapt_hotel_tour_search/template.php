<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["SALE"]=intval($arParams["SALE_PERCENT"]);?>
<script type="text/javascript">

if(window.jQuery==undefined) {
	//alert('Необходимо подключить библиотеку jQuery');
}

jQuery.fn.live = function (types, data, fn) {
    jQuery(this.context).on(types,this.selector,data,fn);
    return this;
};

$(function(){

    $('.input-group.date').datepicker({
        format: "dd.mm.yyyy",
        language: "ru",
        autoclose: true
    });

    <?if (IntVal($_REQUEST["resId"]) > 0):?>
    //Переход со страницы тура
    $(document).ready(function(){
        if(typeof(HotelTourData)!="undefined"){
            HotelTourData (<?=IntVal($_REQUEST["resId"])?>, 50, <?=$arParams["SALE"]?>, <?=$_REQUEST["offerId"]?>);  //09.07.2014 Добавлен параметр OFFER_ID для поиска предложения по отелю
        }
    });

    // Отображение результата данных
    function HotelTourData (RequestResult, count, num, offerId) {
        $.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, template: 'adapt_hotels_current', sale: num, offerId: offerId }, function(data) { //добавлен параметр offerId 09.07.2014
            $('.tour-from').html(data);
        });
    }
    <?endif;?>


	// Изменение города отправления
	$('#travelCityFrom').change(function() {

        $(".cityFrom").html($('#travelCityFrom option:selected').html());

		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "country", from: $('#travelCityFrom').val()}, function(json){
			$('#travelCountryTo').empty();
			$('#travelCountryTo option').remove();
			$('#travelCountryTo').append( $('<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>') );
			$.each(json.results,function(i,country){
				$('#travelCountryTo').append( $('<option value="'+country.Id+'">'+country.Name+'</option>') );
			});
			$(".select-box select").selectBox("destroy");
			$(".select-box select").selectBox();
		});

		// Сбрасываем все остальные настройки
		$('#resortsList').empty();
		$('#resortsList option').remove();
		$("#resortsAny").attr("checked","checked");

		$('#starsList').empty();
		$('#starsList option').remove();
		$("#starsAny").attr("checked","checked");

		$('#hotelsList').empty();
		$('#hotelsList option').remove();
		$("#hotelsAny").attr("checked","checked");
	});


});

//
// Отправка формы поиска туров
//
function SendFormSearchTour(page, count, num) {

    $("#nightsMax").val(parseInt($("#nightsMin").val()) + 1);

	// Показываем что ищем туры
	$("#searchTourStatus").css('display', 'block');
	$("#request").css('display', 'none');

	$.post("/bitrix/tools/simai.travel_ajax.php", $("#searchTour").serialize(), function(RequestResult) {
		$("#request").html(RequestResult+'<br>');
        //alert(RequestResult);
		CheckFormSearchTourStatus(RequestResult, 2, page, count, num);
	});
}



//
// Ожидание статуса обработки
//
function CheckFormSearchTourStatus (RequestResult, time, page, count, num) {
	// Делаем запрос статуса

	$.get("/bitrix/tools/simai.travel_ajax.php", { id: RequestResult, type: "searchStatus" }, function(searchStatus) {
		//$("#request").append(status+'<br>');

        searchStatus = JSON.parse(searchStatus);

        //Строим прогресс бар

        var countOperators = parseInt(searchStatus.count); //Общее количетво туроператоров
        var countLoad = parseInt(searchStatus.load); //Количество загруженных туроператоров


        var status;
        if (searchStatus.status) {
            status = "load";
        }else {
            status = "wait";
        }

		if(status != "load") {
			setTimeout(function() {
				//$('#request').append('Проверка статуса, прошло '+time+' сек<br>');
				time += 5;
				CheckFormSearchTourStatus(RequestResult, time, page, count, num);
			}, 5000);
		}
		else {
			//$('#request').append('Данные обработаны за '+time+' сек<br>');
			SearchTourData(RequestResult, page, count, num);
		}
	});
}

// Отображение результата данных
function SearchTourData (RequestResult, page, count, num) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, template: 'adapt_hotels', page: page, count: count, sale: num/*, captcha_word: captcha_word,  captcha_sid: captcha_sid*/}, function(data) {
		$("#searchTourStatus").css('display', 'none');
		$("#request").css('display', 'block');
		$('#request').html(data);
	});
}

// Покупка тура
function SearchTourDataBuy (RequestResult, page, count, tour, num) {
if(num=="undefind")num=0;
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, template: 'adapt_hotels', page: page, count: count, tourBuy: tour, sale: num }, function(data) {
		$('#request').html(data);

        window.location = '/personal/cart/';

	});
}



var sale=<?=$arParams["SALE"]?>
</script>

<!-- Список туров в отель -->
<div class="row hotel-tours">
    <div class="col-md-12">
        <form class="form" name="searchTour" id="searchTour">
            <!-- Скрытые поля для поиска -->
            <input type="hidden" name="type" value="search">
            <input type="hidden" name="countryId" id="countryId" value="<?=$arParams["COUNTRY"]?>" />
            <input type="hidden" name="cities" id="cities" value="<?=$arParams["RESORT"]?>" />
            <input type="hidden" name="hotels" id="hotels" value="<?=$arParams["HOTEL"]?>" />
            <input type="hidden" name="nightsMax" id="nightsMax" value="" />

            <fieldset>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label from-city-label" for="city">Туры в отель с вылетом из </label>
                        <select  name="cityFromId" id="travelCityFrom" class="form-control">
                            <?foreach($arResult["from"] as $city):?>
                                <option value="<?=$city["Id"]?>" <?if($arParams["CITY"] == $city["Id"]):?>SELECTED<?endif;?>><?=$city["Name"]?></option>
                            <?endforeach?>
                        </select>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="input-group date col-md-2">
                    <span class="input-group-addon">Вылет</span>
                    <input type="text" id="air_from"  value="<?=$arParams["DAY_FROM"]?>" placeholder="Вылет с" name="departFrom" class="form-control">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <div class="input-group date col-md-1">
                    <!-- Дата по -->
                    <input type="text" id="air_to" name="departTo" placeholder="Вылет по" value="<?=$arParams["DAY_TO"]?>" class="form-control">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <div class="input-group col-md-2">
                    <span class="input-group-addon">на</span>
                    <select class="nightfrom form-control" name="nightsMin" id="nightsMin" name="night-to">
                        <?for($i=1; $i<=30; $i++){?>
                            <option value="<?=$i?>" <?if($arParams["NIGHT_FROM"] == $i):?>selected<?endif;?>><?=$i?></option>
                        <?}?>
                    </select>
                    <span class="input-group-addon">ночей</span>
                </div>
                <div class="input-group col-md-2">
                    <span class="input-group-addon">взрослых</span>
                    <select name="adults" id="adults" class="sCombo form-control">
                        <?for($i=0; $i<=4; $i++){?>
                            <option value="<?=$i?>"<?if($arParams["ADULTS"] == $i):?> selected<?endif;?>><?=$i?></option>
                        <?}?>
                    </select>
                </div>
                <div class="input-group col-md-2">
                    <span class="input-group-addon">детей</span>
                    <select name="kids" id="kids" class="sCombo form-control">
                        <?for($i=0; $i<=3; $i++){?>
                            <option value="<?=$i?>"<?if($arParams["KIDS"] == $i):?> selected<?endif;?>><?=$i?></option>
                        <?}?>
                    </select>
                </div>
                <div class="input-group col-md-2">
                    <span class="input-group-addon">питание</span>
                    <select id="meals" name="meals" class="form-control">
                        <?foreach($arResult["meals"] as $meals):?>
                            <option value="<?=$meals["Id"]?>" <?if(in_array($meals["Id"], $arParams["MEALS"])):?>SELECTED<?endif;?>><?=$meals["Name"]?></option>
                        <?endforeach;?>
                    </select>
                </div>
                <div class="col-xs-12 col-md-1">
                    <div class="form-group">
                        <a class="btn btn-warning" href="#request" onClick="SendFormSearchTour(1,2,<?=$arParams["SALE"]?>)">Поиск</a>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
    <div class="hotel-tour-list col-md-12">
        <ul>
            <?if (IntVal($_REQUEST["resId"]) > 0):?>
                <li class="tour-from">

                </li>
            <?endif?>
            <!--Результаты поиска туров-->
            <div id="request" style="display: none;"></div>
            <div style="clear:both;"></div>
            <div id="searchTourStatus" class="form-result" style="display: none;">
                <div class="form-res-in">
                    <div id="angLoading" style="text-align:center"><img align="center"  src="http://ui.sletat.ru/gfx/ld2.gif"></div>
                    <div id="angContainer">
                        <div class="error-txt"> Наш портал ищет для Вас лучшие предложения.<br>Пожалуйста, ожидайте...</div>
                    </div>
                </div>
            </div>

        </ul>
    </div>
</div><!-- /Список туров в отель -->
