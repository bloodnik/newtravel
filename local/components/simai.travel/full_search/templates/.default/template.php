<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
if(window.jQuery==undefined) {
	alert('Необходимо подключить библиотеку jQuery');
}
$(document).ready(function() {

	// Изменение города отправления
	$('#travelCityFrom').change(function() {
		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "country", from: $('#travelCityFrom').val()}, function(json){
			$('#travelCountryTo').empty();
			$('#travelCountryTo option').remove();
			$('#travelCountryTo').append( $('<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>') );
			$.each(json.results,function(i,country){
				$('#travelCountryTo').append( $('<option value="'+country.Id+'">'+country.Name+'</option>') );
			});
		});

		// Сбрасываем все остальные настройки
		$('#resortsList').empty();
		$('#resortsList option').remove();
		$("#resortsAny").attr("checked","checked");

		$('#starsList').empty();
		$('#starsList option').remove();
		$("#starsAny").attr("checked","checked");

		$('#hotelsList').empty();
		$('#hotelsList option').remove();
		$("#hotelsAny").attr("checked","checked");
	});

	// Изменение страны назначения
	$('#travelCountryTo').change(function() {
		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "resort", country: $('#travelCountryTo').val()}, function(json){
			$('#resortsList').empty();
			$('#resortsList option').remove();
			$.each(json.results,function(i,resort){
				$('#resortsList').append( $('<div class="chk"><input type="checkbox" name="resortsList[]" value="'+resort.Id+'"><label>'+resort.Name+'</label></div>') );
			});

			// Устанавливаем галочки по умолчанию
			$("#resortsAny").attr("checked","checked");
			$("#hotelsAny").attr("checked","checked");

            // Изменяем доступные категории отелей
            GetHotelStars();

			// Снятие/установка галочки "Любой курорт"
			$('#resortsList input:checkbox').change(function() {
				$("#resortsAny").attr("checked","checked");
				$("#resortsList input:checkbox:checked").each(function() {
					$("#resortsAny").removeAttr("checked");
				});
				// Изменение категорий отелей при смене списка курортов
				GetHotelStars();
			});
		});
	});







	//
	// Зависимые изменения
	//

	// Получаем список всех доступных категорий отелей
	function GetHotelStars() {
		// Составляем список курортов
		var resorts = "";
		if($("#resortsAny").attr("checked") !== "checked") {
			$("#resortsList input:checkbox:checked").each(function(i,resort) {
				resorts = resorts+resort.value+",";
			});
		}

		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "stars", country: $('#travelCountryTo').val(), resorts: resorts}, function(json){
			$('#starsList').empty();
			$('#starsList option').remove();
			$("#starsAny").attr("checked","checked");
			$.each(json.results,function(i,star){
				$('#starsList').append( $('<div class="chk"><input type="checkbox" name="starsList[]" value="'+star.Id+'"><label>'+star.Name+'</label></div>') );
			});

			// Снятие/установка галочки "Любая категория отеля"
			$('#starsList input:checkbox').change(function() {
				$("#starsAny").attr("checked","checked");
				$("#starsList input:checkbox:checked").each(function() {
					$("#starsAny").removeAttr("checked");
				});
				GetHotelList();
			});


			// Изменение списка отелей
			GetHotelList();
		});
	}


	// Получаем список всех доступных отелей
	function GetHotelList() {
		// Составляем список курортов
		var resorts = "";
		if($("#resortsAny").attr("checked") !== "checked") {
			$("#resortsList input:checkbox:checked").each(function(i,resort) {
				resorts = resorts+resort.value+",";
			});
		}

		// Составляем список категорий
		var stars = "";
		if($("#starsAny").attr("checked") !== "checked") {
			$("#starsList input:checkbox:checked").each(function(i,star) {
				stars = stars+star.value+",";
			});
		}

		$.getJSON('/bitrix/tools/simai.travel_ajax.php', {type: "hotels", country: $('#travelCountryTo').val(), resorts: resorts, stars: stars}, function(json){
			$('#hotelsList').empty();
			$('#hotelsList option').remove();
			$("#hotelsAny").attr("checked","checked");
			$.each(json.results,function(i,hotel){
				$('#hotelsList').append( $('<div class="chk"><input type="checkbox" name="hotelsList[]" value="'+hotel.Id+'"><label>'+hotel.Name+'</label></div>') );
			});

			// Снятие/установка галочки "Любой отель"
			$('#hotelsList input:checkbox').change(function() {
				$("#hotelsAny").attr("checked","checked");
				$("#hotelsList input:checkbox:checked").each(function() {
					$("#hotelsAny").removeAttr("checked");
				});
			});
		});
	}



	//
	// Обработчики галочек
	//

	// Галочка "Все курорты"
	$('#resortsAny').change(function() {
		// Изменение категорий отелей при смене списка курортов
		GetHotelStars();
	});

	// Снятие/установка галочки "Любой курорт"
	$('#resortsList input:checkbox').change(function() {
		$("#resortsAny").attr("checked","checked");
		$("#resortsList input:checkbox:checked").each(function() {
			$("#resortsAny").removeAttr("checked");
		});
		// Изменение категорий отелей при смене списка курортов
		GetHotelStars();
	});



	// Снятие/установка галочки "Любая категория отеля"
	$('#starsList input:checkbox').change(function() {
		$("#starsAny").attr("checked","checked");
		$("#starsList input:checkbox:checked").each(function() {
			$("#starsAny").removeAttr("checked");
		});
		GetHotelList();
	});

	// Галочка "Все категории отелей"
	$('#starsAny').change(function() {
		// Изменение категорий отелей при смене списка курортов
		GetHotelList();
	});



	// Снятие/установка галочки "Любой отель"
	$('#hotelsList input:checkbox').change(function() {
		$("#hotelsAny").attr("checked","checked");
		$("#hotelsList input:checkbox:checked").each(function() {
			$("#hotelsAny").removeAttr("checked");
		});
	});



	// Снятие/установка галочки "Любая категория питания"
	$('#mealsList input:checkbox').change(function() {
		$("#mealsAny").attr("checked","checked");
		$("#mealsList input:checkbox:checked").each(function() {
			$("#mealsAny").removeAttr("checked");
		});
	});
});



//
// Отправка формы поиска туров
//
function SendFormSearchTour(page, count) {

	// Показываем что ищем туры
	$("#searchTourStatus").css('display', 'block');
	$("#request").css('display', 'none');

	$.post("/bitrix/tools/simai.travel_ajax.php", $("#searchTour").serialize(), function(RequestResult) {
		$("#request").html(RequestResult+'<br>');
		CheckFormSearchTourStatus(RequestResult, 5, page, count);
	});
}



//
// Ожидание статуса обработки
//
function CheckFormSearchTourStatus (RequestResult, time, page, count) {
	// Делаем запрос статуса
	$.get("/bitrix/tools/simai.travel_ajax.php", { id: RequestResult, type: "searchStatus" }, function(status) {
		//$("#request").append(status+'<br>');
		if(status != "load") {
			setTimeout(function() {
				//$('#request').append('Проверка статуса, прошло '+time+' сек<br>');
				time += 5;
				CheckFormSearchTourStatus(RequestResult, time, page, count);
			}, 5000);
		}
		else {
			//$('#request').append('Данные обработаны за '+time+' сек<br>');
			SearchTourData(RequestResult, page, count);
		}
	});
}



// Отображение результата данных
function SearchTourData (RequestResult, page, count) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: page, count: count }, function(data) {
		$("#searchTourStatus").css('display', 'none');
		$("#request").css('display','block');
		$('#request').html(data);
	});
}

// Покупка тура
function SearchTourDataBuy (RequestResult, page, count, tour) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: page, count: count, tourBuy: tour }, function(data) {
		$('#request').html(data);

		// Плюсуем кол-во элементов в корзине
		var count = ($('#sidebar_product_count').html()*1)+1;
		$('#sidebar_product_count').html(count);

		// Показываем человеку что тур куплен
		var r=confirm("Перейти в корзину для оформления тура?")
		if (r==true)
		{
			window.location = '/personal/cart/';
		}
		else
		{

		}
	});
}



//
// Если переданы параметры то сразу ищем туры
//
function parseGetParams() {
   var $_GET = {};
   var __GET = window.location.search.substring(1).split("&");
   for(var i=0; i<__GET.length; i++) {
      var getVar = __GET[i].split("=");
      $_GET[getVar[0]] = typeof(getVar[1])=="undefined" ? "" : getVar[1];
   }
   return $_GET;
}

$(document).ready(function() {
	var curGet = parseGetParams();
	if(curGet.start == "Y") {
		SendFormSearchTour(1, curGet.count);
	}
});
</script>
<div class="full-search">
<form action="<?=$arParams["ACTION"]?>" name="searchTour" id="searchTour" method="GET" <?if($arParams["NEW"] == "Y"):?>target="new"<?endif;?>>
<input type="hidden" name="type" value="search">
<table width="100%" style="margin:0 auto">
	<tbody><tr>
		<td>
			<p class="caption">Откуда</p>
			<select name="cityFromId" id="travelCityFrom">
				<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>
				<?foreach($arResult["from"] as $city):?>
					<option value="<?=$city["Id"]?>" <?if($arParams["CITY"] == $city["Id"]):?>SELECTED<?endif;?>><?=$city["Name"]?></option>
				<?endforeach?>
			</select>
		</td>
		<td>
			<p class="caption">Куда</p>
			<select name="countryId" id="travelCountryTo">
				<option value="0">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</option>
				<?foreach($arResult["to"] as $country):?>
					<option value="<?=$country["Id"]?>" <?if($arParams["COUNTRY"] == $country["Id"]):?>SELECTED<?endif;?>><?=$country["Name"]?></option>
				<?endforeach?>
			</select>
		</td>
		<td colspan="3" class="input-wid">
<p class="caption">Вылет с ... по</p>
<?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
     "SHOW_INPUT" => "Y",
     "FORM_NAME" => "searchTour",
     "INPUT_NAME" => "departFrom",
     "INPUT_NAME_FINISH" => "departTo",
     "INPUT_VALUE" => $arParams["DAY_FROM"],
     "INPUT_VALUE_FINISH" => $arParams["DAY_TO"],
     "SHOW_TIME" => "N",
     "HIDE_TIMEBAR" => "Y"
	)
);?>
		</td>
	</tr>
	<tr>
		<td style="vertical-align: top;">
			<p class="caption">Курорт</p>
			<div id="resorts">
			<div class="chk firstf"><input type="checkbox" name="resortsAny" id="resortsAny" CHECKED><label>Любой</label></div>
				<div id="resortsList">
					<?foreach($arResult["resorts"] as $resorts):?>
						<div class="chk"><input type="checkbox" name="resortsList[]" value="<?=$resorts["Id"]?>"><label><?=$resorts["Name"]?></label></div>
					<?endforeach?>
				</div>
			</div>
		</td>
		<td style="vertical-align: top;">
			<p class="caption">Отель</p>
			<div id="hotels">
				<div class="chk firstf"><input type="checkbox" name="hotelsAny" id="hotelsAny" CHECKED><label>Любой</label></div>
				<div id="hotelsList">
					<?foreach($arResult["hotels"] as $hotels):?>
						<div class="chk"><input type="checkbox" name="hotelsList[]" value="<?=$hotels["Id"]?>"><label><?=$hotels["Name"]?></label></div>
					<?endforeach?>
				</div>
			</div>
		</td>
		<td style="vertical-align: top;width: 97px;">
				<p class="caption">Ночей от</p>
				<select class="nightfrom" name="nightsMin" name="night-from">
					<?for($i=1; $i<=30; $i++){?>
						<option value="<?=$i?>" <?if($arParams["NIGHT_FROM"] == $i):?>SELECTED<?endif;?>><?=$i?></option>
					<?}?>
				</select>
				<div>
					<p class="caption" style="margin-top:6px">Категория</p>
					<div id="stars">
						<div class="chk"><input type="checkbox" name="starsAny" id="starsAny" CHECKED><label>Любая</label></div>
						<div id="starsList">
							<?foreach($arResult["stars"] as $stars):?>
								<div class="chk"><input type="checkbox" name="starsList[]" value="<?=$stars["Id"]?>"><label><?=$stars["Name"]?></label></div>
							<?endforeach?>
						</div>
					</div>
				</div>
		</td>
		<td style="vertical-align: top;">
				<p class="caption">Ночей до</p>
				<select class="nightfrom" name="nightsMax" name="night-to">
					<?for($i=1; $i<=30; $i++){?>
						<option value="<?=$i?>" <?if($arParams["NIGHT_TO"] == $i):?>SELECTED<?endif;?>><?=$i?></option>
					<?}?>
				</select>
				<div>
					<p class="caption" style="margin-top:6px">Питание</p>
					<div id="stars">
						<div class="chk"><input type="checkbox" name="mealsAny" id="mealsAny" CHECKED><label>Любое</label></div>

						<div id="mealsList">
							<?foreach($arResult["meals"] as $meals):?>
								<div class="chk"><input type="checkbox" name="mealsList[]" value="<?=$meals["Id"]?>"><label><?=$meals["Name"]?></label></div>
							<?endforeach?>
						</div>
					</div>
				</div>
		</td>
		<td style="vertical-align: top;">
			<div>
				<div style="float:left;padding-right:10px">
					<p class="caption">Взрослых</p>
					<select style="width:58px" name="adults" class="sCombo">
						<option value="1">1</option>
						<option value="2" selected>2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
				</div>
				<div>
					<p id="i-kid-caption" class="caption">Детей</p>
					<select style="width:58px" name="kids" class="sCombo">
						<option value="0" selected>0</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
					</select>
				</div>
			</div>
			<div id="i-kidages-caption" style="padding-top:10px;padding-bottom:20px">
				<p class="caption">Возраст детей</p>
				<select name="kid1">
					<?for($i=0; $i<=15; $i++){?>
						<option value="<?=$i?>"><?=$i?></option>
					<?}?>
				</select>
				<select name="kid2">
					<?for($i=0; $i<=15; $i++){?>
						<option value="<?=$i?>"><?=$i?></option>
					<?}?>
				</select>
				<select name="kid3">
					<?for($i=0; $i<=15; $i++){?>
						<option value="<?=$i?>"><?=$i?></option>
					<?}?>
				</select>
			</div>
			<div>
				<div style="float:left; padding-right:8px">
					<p class="caption">Цена от</p>
					<input type="text" name="priceMin" class="sText" style="width:70px;">
				</div>
				<div>
					<p class="caption">до</p>
					<input type="text" name="priceMax" class="sText" style="width:70px;">
				</div>
			</div>
			<div id="i-currency">
				<div class="chks"><input type="radio" checked="checked" value="RUB" name="currencyAlias"><label><span>RUB</span></label></div>
				<div class="chks"><input type="radio" value="USD" name="currencyAlias"><label><span>USD</span></label></div>
				<div class="chks"><input type="radio" value="EUR" name="currencyAlias"><label><span>EUR</span></label></div>
			</div>
		</td>
	</tr>
</tbody></table>

</form>
</div>




<a name="tourList"></a>
<a href="#tourList" onClick="SendFormSearchTour(1, <?=$arParams["COUNT"]?>);">Искать туры</a><br /><br />
<div id="request" style="display: none;"></div>

<div id="searchTourStatus" style="border: 1px solid #E6E6E6; display: none;">
	<div id="angLoading" style="text-align:center"><img align="center"  src="http://ui.sletat.ru/gfx/ld2.gif"></div>
	<div id="angContainer">
		<div style="text-align:center;padding:5px;font:16px Arial;line-height:130%">Наше агентство ищет для Вас лучшие предложения.<br>Пожалуйста, ожидайте...</div>
	</div>
</div>
