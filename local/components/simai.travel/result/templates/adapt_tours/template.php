<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$toursCount = $arResult["count"];
$days = array("Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб");
if($arResult["count"]>0):
$arParams["SALE_PERCENT"]=intval($arParams["SALE_PERCENT"]);
$arParams["OFFICE_PERCENT"]=intval($arParams["OFFICE_PERCENT"]);
if($arResult["BUY"] == true):?><font color="#BB3333">Куплен тур: <?=$_REQUEST["tourBuy"]?></font><br><?endif;?>
	<div class="tour-list-wrap">
		<ul class="tour-list">
			<?foreach($arResult["rows"] as $key=>$row):?>
				<?//PR($row)?>
				<li class="tour-item">
				    <!--Шапка элемента тура-->
				    <div class="col-md-12 tour-item-header">
				        <div class="col-md-6 col-xs-12" style="padding: 0">
				            <div class="item-hotel-name col-md-7"><a target="_blank" href="/hotels/<?=$row["HotelId"]?>.html?from=tours&resId=<?=$arParams["ID"]?>&offerId=<?=$row["OfferId"]?>&id=<?=$row["HotelId"]?>"><?=$row["HotelName"]?></a></div>
				            <div class="item-hotel-stars">  
                               <?
                                    switch($row['StarName']){
                                        case "1*":
                                            echo '<i class="fa fa-star"></i>';
                                            break;
                                        case "2*":
                                            echo '<i class="fa fa-star"></i><i class="fa fa-star"></i>';
                                            break;
                                        case "3*":
                                            echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
                                            break;
                                        case "4*":
                                            echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
                                            break;
                                        case "5*":
                                            echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
                                            break;
                                        case 'Apts':
                                            echo "Аппартаменты";
                                            break;
                                        case 'Villas':
                                            echo "Вилла";
                                            break;
                                        default:
                                            echo "";
                                            break;
                                    }
                               ?>
				            </div>
				            <span class="tour-item-resort"><?=$row["CountryName"]?>, <?=$row["ResortName"]?></span>
				        </div>
                        <?
                            $arSelect = Array("ID", "NAME", "PROPERTY_HotelType");
                            $arFilter = Array("IBLOCK_ID"=>58, "CODE"=>$row["HotelId"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
                            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
                        ?>
                        <div class="col-md-6 col-xs-12"  style="padding: 0">
				            <div class="item-rating">
                                <span class="fa-stack fa-lg">
                                  <i class="fa fa-circle fa-stack-2x" style="color:#B9E0A5"></i>
                                  <i class="fa fa-thumbs-up fa-stack-1x"></i>
                                </span>
                                Рейтинг <?=$row["HotelRating"]?>/10
                            </div>
				            <div class="item-category">
                                <?while($ob = $res->GetNextElement()){
                                    $HotelTypeId = $ob->GetFields();
                                    switch ($HotelTypeId["PROPERTY_HOTELTYPE_ENUM_ID"]) {
                                        case 105:
                                            echo '<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" style="color:#B9E0A5"></i><i class="fa fa-lightbulb-o fa-stack-1x"></i></span>';
                                            break;
                                        case 104:
                                            echo '<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" style="color:#7EA6E0"></i><i class="fa fa-lightbulb-o fa-stack-1x"></i></span>';
                                            break;
                                        case 107:
                                            echo '<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" style="color:#F8B5CC"></i><i class="fa fa-lightbulb-o fa-stack-1x"></i></span>';
                                            break;
                                    }
                                }
                                ?>
				            </div>
				        </div>
				    </div><!--/Шапка элемента тура-->

				    <!--Тело элемента тура-->
				    <div class="col-md-12 col-xs-12 tour-item-body">
				        <div class="col-md-3 col-xs-12 tour-item-img">
				            <a href="/hotels/<?=$row["HotelId"]?>.html?id=<?=$row["HotelId"]?>&from=tours&resId=<?=$arParams["ID"]?>&offerId=<?=$row["OfferId"]?>&id=<?=$row["HotelId"]?>" target="_blank"><img src="<?=$row["HotelTitleImageUrl"]?>" alt="<?=$row["TourName"]?>" /></a>
				        </div>
				        <div class="col-md-6 col-xs-12 tour-item-info">
				            <ul>
				                <li>Вылет: <span><?=$row["CheckInDate"]?> (<?=getMessage("CALENDAR_DATE_".strtoupper(date("D",MakeTimeStamp($row["CheckInDate"],"DD.MM.YYYY"))))?>.) ночей: <?=$row["Nights"]?></span></li>
				                <li>Номер: <span><?=$row["RoomName"]?> <?if($row["HtPlaceDescription"]):?><?=$row["HtPlaceName"]?>(<?=$row["HtPlaceDescription"]?>)<?endif?> </span></li>
				                <li>Питание: <span><?if($row["MealDescription"]):?><?=$row["MealName"]?> (<?=$row["MealDescription"]?>)<?endif?></span></li>
				            </ul>
				        </div>
				        <div class="col-md-3 col-xs-6 tour-item-old-price">
				            <p class="item-price"><?if($arParams["SALE_PERCENT"]>0):?><?=intval($row["Price"]/100*$arParams["SALE_PERCENT"])+$row["Price"]?> <?=($row["Currency"]=="RUB"?"руб.":$row["Currency"])?><?endif?></p>
				            <p class="item-old-price-descr">Это цена без скидки!</p>
				        </div>
                        <div class="col-md-3 col-xs-6 tour-item-office-price">
                            <p class="item-price"><?if($arParams["OFFICE_PERCENT"]>0):?><?=intval($row["Price"]/100*$arParams["OFFICE_PERCENT"])+$row["Price"]?> <?=($row["Currency"]=="RUB"?"руб.":$row["Currency"])?><?endif?></p>
                            <p class="item-old-price-descr">Это цена при покупке в офисе</p>
                        </div>
				    </div><!--/Тело элемента тура-->
				    <!--Подвал элемента тура-->
				    <div class="col-md-12 tour-item-footer col-xs-12">
				        <div class="col-md-9 tour-item-additional">
                            Места в отеле:
                            <a href="javascript:void()" id="hotelPlace" rel="tooltip" data-placement="right" data-original-title="Места в отеле">
                                <?switch ($row["HotelIsInStop"]):
                                    case "Stop":?><span style="color:#b00">нет</span><?break;
                                    case "Available":?>
                                        <span class="fa-stack fa-2x">
                                          <i class="fa fa-circle fa-stack-2x" style="color:#74C14A"></i>
                                          <i class="fa fa-home fa-stack-1x"></i>
                                        </span>
                                        <?break;
                                    case "Request":?>
                                        <span class="fa-stack fa-2x">
                                          <i class="fa fa-circle fa-stack-2x" style="color:#FF9D40"></i>
                                          <i class="fa fa-home fa-stack-1x"></i>
                                        </span>
                                        <?break;
                                    case "Unknown":?><span style="color:#666">нет данных</span><?break;
                                endswitch;?>
                            </a>
                            Билеты в эконом:
                            <a href="javascript:void()" id="economTicket" rel="tooltip" data-placement="right" data-original-title="Билеты в эконом туда">
                                <!--Эконом билеты туда-->
                                <?switch ($row["EconomTicketsDpt"]):
                                    case "Stop":?>
                                        <span class="fa-stack fa-2x">
                                          <i class="fa fa-circle fa-stack-2x" style="color:#74C14A"></i>
                                          <i class="fa fa-plane fa-stack-1x"></i>
                                        </span>
                                        <?break;
                                    case "Available":?>
                                        <span class="fa-stack fa-2x">
                                          <i class="fa fa-circle fa-stack-2x" style="color:#74C14A"></i>
                                          <i class="fa fa-plane fa-stack-1x"></i>
                                        </span>
                                        <?break;
                                    case "Request":?>
                                        <span class="fa-stack fa-2x">
                                          <i class="fa fa-circle fa-stack-2x" style="color:#FF9D40"></i>
                                          <i class="fa fa-plane fa-stack-1x"></i>
                                        </span>
                                        <?break;
                                    case "Unknown":?><span style="color:#666">нет данных &raquo;</span><?break;
                                endswitch;?>
                            </a>
                            <a href="javascript:void()" id="economTicket" style="position: relative; left: -18px" rel="tooltip" data-placement="right" data-original-title="Билеты в эконом обратно">
                                <!--//Эконом билеты обратно-->
                                <?switch ($row["EconomTicketsRtn"]):
                                    case "Stop":?>
                                        <span class="fa-stack fa-2x">
                                          <i class="fa fa-circle fa-stack-2x" style="color:#74C14A"></i>
                                          <i class="fa fa-plane fa-stack-1x"></i>
                                        </span>
                                        <?break;
                                    case "Available":?>
                                        <span class="fa-stack fa-2x">
                                          <i class="fa fa-circle fa-stack-2x" style="color:#74C14A"></i>
                                          <i class="fa fa-plane fa-stack-1x"></i>
                                        </span>
                                        <?break;
                                    case "Request":?>
                                        <span class="fa-stack fa-2x">
                                          <i class="fa fa-circle fa-stack-2x" style="color:#FF9D40"></i>
                                          <i class="fa fa-plane fa-stack-1x"></i>
                                        </span>
                                        <?break;
                                    case "Unknown":?><span style="color:#666">нет данных &raquo;</span><?break;
                                endswitch;?>
                            </a>
                            Билеты в бизнес:
                            <a href="javascript:void()" id="businesTicket" rel="tooltip" data-placement="right" data-original-title="Билеты в бизнес туда">
                                <!--//Бизнес билеты туда-->
                                <?switch ($row["BusinessTicketsDpt"]):
                                    case "Stop":?>
                                        <span class="fa-stack fa-2x">
                                          <i class="fa fa-circle fa-stack-2x" style="color:#E33933"></i>
                                          <i class="fa fa-plane fa-stack-1x"></i>
                                        </span>
                                    <?break;
                                    case "Available":?>
                                        <span class="fa-stack fa-2x">
                                          <i class="fa fa-circle fa-stack-2x" style="color:#74C14A"></i>
                                          <i class="fa fa-plane fa-stack-1x"></i>
                                        </span>
                                        <?break;
                                    case "Request":?>
                                        <span class="fa-stack fa-2x">
                                          <i class="fa fa-circle fa-stack-2x" style="color:#FF9D40"></i>
                                          <i class="fa fa-plane fa-stack-1x"></i>
                                        </span>
                                        <?break;
                                    case "Unknown":?><span style="color:#666">нет данных &raquo;</span><?break;
                                endswitch;?>
                            </a>
                            <a href="javascript:void()" id="businesTicket" rel="tooltip" style="position: relative; left: -18px" data-placement="right" data-original-title="Билеты в бизнес обратно">
                                <!--//Бизнес билеты обратно-->
                                <?switch ($row["BusinessTicketsRtn"]):
                                    case "Stop":?>
                                        <span class="fa-stack fa-2x">
                                          <i class="fa fa-circle fa-stack-2x" style="color:#E33933"></i>
                                          <i class="fa fa-plane fa-stack-1x"></i>
                                        </span>
                                        <?break;
                                    case "Available":?>
                                        <span class="fa-stack fa-2x">
                                          <i class="fa fa-circle fa-stack-2x" style="color:#74C14A"></i>
                                          <i class="fa fa-plane fa-stack-1x"></i>
                                        </span>
                                        <?break;
                                    case "Request":?>
                                        <span class="fa-stack fa-2x">
                                          <i class="fa fa-circle fa-stack-2x" style="color:#FF9D40"></i>
                                          <i class="fa fa-plane fa-stack-1x"></i>
                                        </span>
                                        <?break;
                                    case "Unknown":?><span style="color:#666">нет данных &raquo;</span><?break;
                                endswitch;?>
                            </a>
				             
				        </div>
				        <div class="col-md-3 tour-item-price" onClick="SearchTourDataBuy (<?=$arParams["ID"]?>, <?=$arParams["PAGE"]?>, <?=$arParams["COUNT"]?>, <?=$row["OfferId"]?>, <?=$arParams["SALE_PERCENT"]?>);">
				            <div class="btn btn-primary tour-buy-btn col-xs-12">
                                <p><?=$row["Price"]?>
                                <?=($row["Currency"]=="RUB"?"РУБ":$row["Currency"])?>
                                </p>
				                <span>Ваша цена со скидкой!</span>
				            </div>
				        </div>
				    </div><!--/Подвал элемента тура-->
				</li>
			<?endforeach;?>
		</ul>
	</div>

<? /* постраничная навигация */ ?>
<?if($arResult["pageTotal"] > 0):
?>
<div class="pagination">
<ul>
<?if($arResult["pageTotal"]<10):?>
	<?if($arResult["pageNumber"] > 1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]-1)?>, <?=$arResult["pageCount"]?>);">Предыдущая</a></li><?endif;?>
	<?for($i=1;$i<=($arResult["pageTotal"]+1);$i++):?>
		<?if($arResult["pageNumber"] == $i):?>
			<li><span><?=$i?></span></li>
		<?else:?>
			<li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=$i?>, <?=$arResult["pageCount"]?>);"><?=$i?></a></li>
		<?endif;?>
	<?endfor;?>
	<?if($arResult["pageNumber"] < $arResult["pageTotal"]+1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]+1)?>, <?=$arResult["pageCount"]?>);">Следующая</a></li><?endif;?>
<?else:?>
	<?$varFirst=((($arResult["pageNumber"]-2)<1)?1:$arResult["pageNumber"]-2);$varLast=(($arResult["pageNumber"]+2)>($arResult["pageTotal"]+1)?$arResult["pageTotal"]+1:$arResult["pageNumber"]+2);?>
	<?if($varFirst>1):?>
	<li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, 1, <?=$arResult["pageCount"]?>);">Начало</a></li>
	<?endif?>
	<?if($arResult["pageNumber"] > 1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]-1)?>, <?=$arResult["pageCount"]?>);"><</a></li><?endif;?>
	<?for($i=$varFirst;$i<=($varLast);$i++):?>
		<?if($arResult["pageNumber"] == $i):?>
			<li><span><?=$i?></span></li>
		<?else:?>
			<li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=$i?>, <?=$arResult["pageCount"]?>);"><?=$i?></a></li>
		<?endif;?>
	<?endfor;?>
	<?if($arResult["pageNumber"] < $arResult["pageTotal"]+1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]+1)?>, <?=$arResult["pageCount"]?>);">></a></li><?endif;?>
	<?if($varLast<$arResult["pageTotal"]+1):?>
	<li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=$arResult["pageTotal"]+1?>, <?=$arResult["pageCount"]?>);">Конец</a></li>
	<?endif;?>
<?endif?>
</ul>
</div>
<?endif;?>
<?endif?>

<script>

    $('#economTicket, #green_light, #blue_light, #red_light,#businesTicket,#hotelPlace').tooltip();

    var toursCount = "<?=$toursCount?>";
    $(".tours-count").html(toursCount);
    $(".tour-search-time span").html("назад");

    $("#DateCountdown").TimeCircles({
        "animation": "smooth",
        "bg_width": 1.3,
        "fg_width": 0.06333333333333334,
        "circle_bg_color": "#60686F",
        "text_size": 0.1,
        "time": {
            "Days": {
                "text": "Дней",
                "color": "#FFCC66",
                "show": false
            },
            "Hours": {
                "text": "Часов",
                "color": "#99CCFF",
                "show": true
            },
            "Minutes": {
                "text": "Минут",
                "color": "#BBFFBB",
                "show": true
            },
            "Seconds": {
                "text": "Секунд",
                "color": "#FF9999",
                "show": true
            }
        }
    });
</script>
