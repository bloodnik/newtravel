<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arParams["SALE"]=intval($arParams["SALE"]);?>
<script type="text/javascript">
$(function() {
	  $(".oform.no-user").click(function(){authFormWindow.ShowLoginForm()});
});
</script>
<div class='hot-tours-info'>* цена указана за номер </div>
<?if($arResult["BUY"] == true):?><font color="#BB3333">Куплен тур: <?=$_REQUEST["tourBuy"]?></font><br><?endif;?>
	<?foreach($arResult["rows"] as $row):?>	
		<div class="tour-list">
            <div class="tour-list-in">
              <div class="tour-item item-right">
                <div class="t-name" style="width:305px;"><a class="tname" target="_blank" href="/hotels/<?=$row["HotelId"]?>.html?from=tours&resId=<?=$arParams["ID"]?>&offerId=<?=$row["OfferId"]?>"><?=$row["HotelName"]?></a>
                  <div class="rating">
                    <ul>
					<?if($row["StarName"]){echo str_repeat("<li class='rate'></li>", intval($row["StarName"]));}?>
                      <li class="rating-s">Рейтинг <b><?=$row["HotelRating"]?></b></li>
                    </ul>
                  </div>
                  <div class="where"><?=$row["ResortName"]?></div>
                </div>
                <div class="t-i-l">
                  <div class="img-n"><a href="/hotels/<?=$row["HotelId"]?>.html?from=tours&resId=<?=$arParams["ID"]?>&offerId=<?=$row["OfferId"]?>" target="_blank"><img src="<?=$row["HotelTitleImageUrl"]?>" alt="<?=$row["TourName"]?>" /></a></div>
                  <div class="n-name-txt">
                    <div class="n-txt">
                      <div><b>Вылет:</b> <?=$row["CheckInDate"]?> (<?=getMessage("CALENDAR_DATE_".strtoupper(date("D",MakeTimeStamp($row["CheckInDate"],"DD.MM.YYYY"))))?>.) ночей: <?=$row["Nights"]?></div>
                      <div><b>Номер:</b> <?=$row["RoomName"]?></div>
                      <div><b><?=$row["HtPlaceName"]?></b> (<?=$row["HtPlaceDescription"]?>)</div>
                      <div><b><?=$row["MealName"]?></b> (<?=$row["MealDescription"]?>)</div>

                      <?if($arResult["hotel"][$row["HotelId"]]["type"][0] != false):
					  $numberHotel=(count($arResult["hotel"][$row["HotelId"]]["type"])-1)?>
					  <div><b>Тип отеля:</b> 
						<?foreach($arResult["hotel"][$row["HotelId"]]["type"] as $num=>$type):?>
						<?=$type;if($num!=$numberHotel)echo", "?>
						<?endforeach;?>
						</div>
                      <?endif;?>
					  
						<div><b>Места в отеле:</b> <?
						switch ($row["HotelIsInStop"]):
							case "Stop":?><span style="color:#b00">нет</span><?break;
							case "Available":?><span style="color:#0a0">есть</span><?break;
							case "Request":?><span style="color:#aa0">под запрос</span><?break;
							case "Unknown":?><span style="color:#666">нет данных</span><?break;
						endswitch;
						?></div>
						<div><b>Билеты экон.-класса:</b> <?
						switch ($row["EconomTicketsDpt"]):
							case "Stop":?><span style="color:#b00">нет &raquo;</span><?break;
							case "Available":?><span style="color:#0a0">есть &raquo;</span><?break;
							case "Request":?><span style="color:#aa0">под запрос &raquo;</span><?break;
							case "Unknown":?><span style="color:#666">нет данных &raquo;</span><?break;
						endswitch;
						?> - <?
						switch ($row["EconomTicketsRtn"]):
							case "Stop":?><span style="color:#b00">&laquo; нет</span><?break;
							case "Available":?><span style="color:#0a0">&laquo; есть</span><?break;
							case "Request":?><span style="color:#aa0">&laquo; под запрос</span><?break;
							case "Unknown":?><span style="color:#666">&laquo; нет данных</span><?break;
						endswitch;
						?></div>
						<div><b>Билеты биз.-класса:</b> <?
						switch ($row["BusinessTicketsDpt"]):
							case "Stop":?><span style="color:#b00">нет &raquo;</span><?break;
							case "Available":?><span style="color:#0a0">есть &raquo;</span><?break;
							case "Request":?><span style="color:#aa0">под запрос &raquo;</span><?break;
							case "Unknown":?><span style="color:#666">нет данных &raquo;</span><?break;
						endswitch;
						?> - <?
						switch ($row["BusinessTicketsRtn"]):
							case "Stop":?><span style="color:#b00">&laquo; нет</span><?break;
							case "Available":?><span style="color:#0a0">&laquo; есть</span><?break;
							case "Request":?><span style="color:#aa0">&laquo; под запрос</span><?break;
							case "Unknown":?><span style="color:#666">&laquo; нет данных</span><?break;
						endswitch;
						?></div>

                    </div>
                  </div>
                </div>
              </div>
              <div class="palm">
               <?if($arParams["SALE"]>0):?><div class="old-price"><s><?=intval($row["Price"]/100*$arParams["SALE"])+$row["Price"]?> <?=($row["Currency"]=="RUB"?"руб.":$row["Currency"])?></s></div><?endif?>
                <div class="new-price"><?=$row["Price"]?> <?=($row["Currency"]=="RUB"?"руб.":$row["Currency"])?></div>
                <!--<div class="doplata">доплаты:</div>-->

                <?if($row["Tax_oil"] > 0 ):?>
				  <div class="doplata">топливный сбор - <?=$row["Tax_oil"]?> <?=$row["Tax_oil_currency"]?>/чел</div>
	            <?else:?>
					<div class="doplata">топливного сбора нет</div>
                <?endif;?>

                <?if($row["Tax_visa"] > 0 ):?>
	                <div class="vissbor">визовый сбор - <?=$row["Tax_visa"]?> <?=$row["Tax_visa_currency"]?></div>
	                <div class="vissbor">визовый сбор - <?=$row["Tax_visa_rub"]?> RUB</div>
	            <?else:?>
					<div class="vissbor">визового сбора нет</div>
                <?endif;?>

				<?if($USER->IsAuthorized()):?>
				<?if(in_array($row["OfferId"], $arResult["BASKET"])):?>
					<span class="in-basket">В корзине</span>
				<?else:?>
                <a href="javascript:void(0);" class="oform" onClick="HotelTourBuy(<?=$arParams["ID"]?>,<?=$arParams["COUNT"]?>,<?=$row["OfferId"]?>, <?=$arParams["SALE"]?>);">Оформить</a>
				<?endif?>
				<?else:?>
				<a class="oform no-user" href="javascript:void(0);">Оформить</a>
				<?endif;?>
				</div>
            </div>
          </div>
<?endforeach;?>

