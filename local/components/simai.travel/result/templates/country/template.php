<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arCount=count($arResult["rows"]);
$arParams["SALE"]=intval($arParams["SALE"]);
?>
<script type="text/javascript">
$(function(){
	  $(".oform.no-user").click(function(){authFormWindow.ShowLoginForm()});
});
</script>
 <div style="clear:both; height:10px;"></div>
 <?//echo"<pre>";print_r($arResult);echo"<pre>";?>
   <div class="search-block">
   <?if($arResult["BUY"] == true):?><font color="#BB3333">Куплен тур: <?=$_REQUEST["tourBuy"]?></font><br><?endif;?>
   <h2 style="float: left;">Результаты поиска</h2>
   <h1><a style="margin-bottom: 10px; font-size: 16px; cursor:pointer; display:inline;border-bottom:1px dashed;padding-bottom:0px; margin-bottom: 10px;display: block;margin-left: 151px;margin-top: 10px;color:#BDD50E; float:left;text-decoration: none;" href="/tours/">ПЕРЕЙТИ К ФОРМЕ ПОИСКА</a></h1>
   <div style="clear:both;"></div>
   <?if(empty($arResult["rows"])):?>
    Не найдено
   <?else:?>
              <div class="slide-search">
			  <div class="tour-list">
            <div class="tour-list-in">
			<?foreach($arResult["rows"] as $key=>$row):?>
				<div class="t-i-w">
                          <div class="tour-item">
                            <div class="t-name"><a class="tname" href="/hotels/<?=$row["HotelId"]?>.html"><?=$row["HotelName"]?></a></div>
                            <div class="t-i-l">
                              <div class="img-n"><a href="/hotels/<?=$row["HotelId"]?>.html"><img src="<?=$row["HotelTitleImageUrl"]?>" alt="<?=$row["TourName"]?>" /></a></div>
                              <div class="n-name-txt">
                                <div class="n-txt">
									<div><b>Вылет:</b> <?=$row["CheckInDate"]?> (<?=getMessage("CALENDAR_DATE_".strtoupper(date("D",MakeTimeStamp($row["CheckInDate"],"DD.MM.YYYY"))))?>.) ночей: <?=$row["Nights"]?></div>
									<div><b>Номер:</b> <?=$row["RoomName"]?></div>
									<div><b><?=$row["HtPlaceName"]?></b> (<?=$row["HtPlaceDescription"]?>)</div>
									<div><b><?=$row["MealName"]?></b> (<?=$row["MealDescription"]?>)</div>
								</div>
                              </div>
                            </div>
                          </div>
                           <div class="where"><?=$row["ResortName"]?></div>
                          <div class="rating">
                            <div class="rates">
                                <?if(intVal($row["StarName"])){echo str_repeat("<div class=\"rate\"></div>", intval($row["StarName"]));}?>
                              </div>
                              <div class="rating-s">Рейтинг <b><?=$row["HotelRating"]?></b></div>
                          </div>
						  <?if($arResult["hotel"][$row["HotelId"]]["type"][0] != false):?>
										<div class="where">
												<?foreach($arResult["hotel"][$row["HotelId"]]["type"] as $num=>$type):?>
														<span><?=$type;?></span>
												<?endforeach;?>
										</div>
						<?endif;?>
                          <div class="palm">
                <?if($arParams["SALE"]>0):?><div class="old-price"><s><?=intval($row["Price"]/100*$arParams["SALE"])+$row["Price"]?> <?=($row["Currency"]=="RUB"?"руб.":$row["Currency"])?></s></div><?endif?>
                <div class="new-price"><?=$row["Price"]?> <?=($row["Currency"]=="RUB"?"руб.":$row["Currency"])?></div>
                <!--<div class="doplata">доплаты:</div>
                <div class="vissbor">визовый сбор - нет</div>-->
				<?if($row["Tax_oil"] > 0 ):?>
	                <div class="doplata">топливный сбор - <?=$row["Tax_oil"]?> <?=$row["Tax_oil_currency"]?></div>
	                <div class="doplata">топливный сбор - <?=$row["Tax_oil_rub"]?> RUB</div>
	            <?else:?>
					<div class="doplata">топливного сбора нет</div>
                <?endif;?>

                <?if($row["Tax_visa"] > 0 ):?>
	                <div class="vissbor">визовый сбор - <?=$row["Tax_visa"]?> <?=$row["Tax_visa_currency"]?></div>
	                <div class="vissbor">визовый сбор - <?=$row["Tax_visa_rub"]?> RUB</div>
	            <?else:?>
					<div class="vissbor">визового сбора нет</div>
                <?endif;?>
				<?if($USER->IsAuthorized()):?>
				<?if(in_array($row["OfferId"], $arResult["BASKET"])):?>
					<span class="in-basket">В корзине</span>
				<?else:?>
                <a href="javascript:void(0);" class="oform" onClick="CountryTourBuy(<?=$arParams["ID"]?>,<?=$arParams["COUNT"]?>,<?=$row["OfferId"]?>, <?=$arParams["SALE"]?>);">Оформить</a>
				<?endif?>
				<?else:?>
				<a class="oform no-user" href="javascript:void(0);">Оформить</a>
				<?endif;?>
				</div>
               </div>
			<?
			if(($key+1)%4==0 && ($key+1)!=$arCount)echo"</div></div><div class=\"tour-list\"><div class=\"tour-list-in\">";
			endforeach?>
            </div>
          </div>
</div>
<?endif?>
</div>
