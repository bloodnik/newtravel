<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arCount=count($arResult["rows"]);
$arParams["SALE"]=intval($arParams["SALE"]);
?>

<?if(empty($arResult["rows"])):?>
    Не найдено
<?else:?>

    <?foreach($arResult["rows"] as $key=>$row):?>
        <li>
            <div class="tour-period"><?=$row["CheckInDate"]?> (<?=getMessage("CALENDAR_DATE_".strtoupper(date("D",MakeTimeStamp($row["CheckInDate"],"DD.MM.YYYY"))))?>.)-></div>
            <div class="tour-nights"><?=$row["Nights"]?> ночей</div>
            <div class="tour-meals"><?=$row["MealDescription"]?></div>
            <div class="tour-room-type"><?=$row["RoomName"]?> - <?=$row["HtPlaceName"]?></div>
            <div class="tour-item-old-price" style="text-decoration: line-through">
                <?if($arParams["OFFICE_PERCENT"]>0):?><?=intval($row["Price"]/100*$arParams["OFFICE_PERCENT"])+$row["Price"]?> <?=($row["Currency"]=="RUB"?"Р":$row["Currency"])?><?endif?>
            </div>
            <div class="btn btn-warning tour-buy-btn" onClick="SearchTourDataBuy(<?=$arParams["ID"]?>, <?=$arParams["PAGE"]?>, <?=$arParams["COUNT"]?>,<?=$row["OfferId"]?>, <?=$arParams["SALE"]?>);"><?=$row["Price"]?> Р</div>
        </li>
    <?endforeach?>

<?endif?>
