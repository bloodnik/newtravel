<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["SALE"]=intVal($arParams["SALE"]);
?>
<script type="text/javascript">
$(function() {
	$(".t-slide").jCarouselLite({
        btnNext: ".nexts",
		visible: 2,
		scroll: 1,
		circular:false,
        btnPrev: ".prevs"
});
$(".tour-list-in").each(function(){
		var height=$(this).outerHeight();
		$(this).children(".palm").css({"min-height":height});
});
$(".oform.no-user").click(function(){authFormWindow.ShowLoginForm()});
});
</script>
<div class="idea">
<?if($arResult["BUY"] == true):?><font color="#BB3333">Куплен тур: <?=$_REQUEST["tourBuy"]?></font><br><?endif;?>
<div class="t-slide">
<ul>
	<?foreach($arResult["rows"] as $row):?>
	<li class="autoH">
		<div class="tour-list" style="margin-left:0;">
            <div class="tour-list-in">
              <div class="tour-item">
                <div class="t-name"><a class="tname" href="/hotels/<?=$row["HotelId"]?>.html"><?=$row["HotelName"]?></a>
                  <div class="rating">
               
					<?if($row["StarName"]){echo str_repeat("<div class='rate'></div>", intval($row["StarName"]));}?>
                      <div class="rating-s">Рейтинг <b><?=$row["HotelRating"]?></b></div>
                 
                  </div>
                  <div class="where"><?=$row["ResortName"]?></div>
                </div>
                <div class="t-i-l">
                  <div class="img-n"><a href="/hotels/<?=$row["HotelId"]?>.html"><img src="<?=$row["HotelTitleImageUrl"]?>" alt="<?=$row["TourName"]?>" /></a></div>
                  <div class="n-name-txt">
                    <div class="n-txt">
                      <div><b>Вылет:</b> <?=$row["CheckInDate"]?> (<?=getMessage("CALENDAR_DATE_".strtoupper(date("D",MakeTimeStamp($row["CheckInDate"],"DD.MM.YYYY"))))?>.) ночей: <?=$row["Nights"]?></div>
                      <div><b>Номер:</b> <?=$row["RoomName"]?></div>
                      <div><b><?=$row["HtPlaceName"]?></b> (<?=$row["HtPlaceDescription"]?>)</div>
                      <div><b><?=$row["MealName"]?></b> (<?=$row["MealDescription"]?>)</div>
					  <?if($arResult["hotel"][$row["HotelId"]]["type"][0] != false):
											$numberHotel=(count($arResult["hotel"][$row["HotelId"]]["type"])-1)?>
											<div><b>Тип отеля:</b> 
												<?foreach($arResult["hotel"][$row["HotelId"]]["type"] as $num=>$type):?>
														<?=$type;if($num!=$numberHotel)echo", "?>
												<?endforeach;?>
											</div>
									<?endif;?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="palm">
                <?if($arParams["SALE"]>0):?><div class="old-price"><s><?=intval($row["Price"]/100*$arParams["SALE"])+$row["Price"]?> <?=($row["Currency"]=="RUB"?"руб.":$row["Currency"])?></s></div><?endif?>
                <div class="new-price"><?=$row["Price"]?> <?=($row["Currency"]=="RUB"?"руб.":$row["Currency"])?></div>
                <!--<div class="doplata">доплаты:</div>-->
				<?if($row["Tax_oil"] > 0 ):?>
	                <div class="doplata">топливный сбор - <?=$row["Tax_oil"]?> <?=$row["Tax_oil_currency"]?></div>
	            <?else:?>
					<div class="doplata">топливного сбора нет</div>
                <?endif;?>

                <?if($row["Tax_visa"] > 0 ):?>
	                <div class="vissbor">визовый сбор - <?=$row["Tax_visa"]?> <?=$row["Tax_visa_currency"]?></div>
	                <div class="vissbor">визовый сбор - <?=$row["Tax_visa_rub"]?> RUB</div>
	            <?else:?>
					<div class="vissbor">визового сбора нет</div>
                <?endif;?>
				<?if($USER->IsAuthorized()):?>
				<?if(in_array($row["OfferId"], $arResult["BASKET"])):?>
					<span class="in-basket">В корзине</span>
				<?else:?>
                <a href="javascript:void(0);" class="oform" onClick="HotelTourBuy(<?=$arParams["ID"]?>,<?=$arParams["COUNT"]?>,<?=$row["OfferId"]?>, <?=$arParams["SALE"]?>);">Оформить</a>
				<?endif?>
				<?else:?>
				<a class="oform no-user" href="javascript:void(0);">Оформить</a>
				<?endif;?>
				</div>
            </div>
          </div>
		</li>
<?endforeach;?>
</ul>
</div>
<a class="prevs"></a> <a class="nexts"></a>
</div>