<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$days = array("Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб");
if($arResult["count"]>0):
$arParams["SALE"]=intval($arParams["SALE"]);
if($arResult["BUY"] == true):?><font color="#BB3333">Куплен тур: <?=$_REQUEST["tourBuy"]?></font><br><?endif;?>
<div class="hot-tours-info">* цена указана за номер </div>
<div class="slide-search">
			  <div class="tour-list">
            <div class="tour-list-in">
			<?foreach($arResult["rows"] as $key=>$row):?>
				<div class="t-i-w">
                          <div class="tour-item">
                            <div class="t-name">
								<a class="tname" target="_blank" href="/hotels/<?=$row["HotelId"]?>.html?from=tours&resId=<?=$arParams["ID"]?>&offerId=<?=$row["OfferId"]?>"><?=$row["HotelName"]?></a>
							  <div class="rating">
								<div class="rates">
									<?if(intVal($row["StarName"])){echo str_repeat("<div class=\"rate\"></div>", intval($row["StarName"]));}?>
								  </div>
								  <div class="rating-s">Рейтинг <b><?=$row["HotelRating"]?></b></div>
							  </div>							
							</div>
                            <div class="t-i-l">
                              <div class="img-n"><a href="/hotels/<?=$row["HotelId"]?>.html?from=tours&resId=<?=$arParams["ID"]?>&offerId=<?=$row["OfferId"]?>" target="_blank"><img src="<?=$row["HotelTitleImageUrl"]?>" alt="<?=$row["TourName"]?>" /></a></div>
                              <div class="n-name-txt">
                                <div class="n-txt">
									<div><b>Вылет:</b> <?=$row["CheckInDate"]?> (<?=getMessage("CALENDAR_DATE_".strtoupper(date("D",MakeTimeStamp($row["CheckInDate"],"DD.MM.YYYY"))))?>.) ночей: <?=$row["Nights"]?></div>
									<div><b>Номер:</b> <?=$row["RoomName"]?></div>
									<?if($row["HtPlaceDescription"]):?><div><b><?=$row["HtPlaceName"]?></b> (<?=$row["HtPlaceDescription"]?>)</div><?endif?>
									<?if($row["MealDescription"]):?><div><b><?=$row["MealName"]?></b> (<?=$row["MealDescription"]?>)</div><?endif?>
									<div><b>Места в отеле:</b> <?
									switch ($row["HotelIsInStop"]):
										case "Stop":?><span style="color:#b00">нет</span><?break;
										case "Available":?><span style="color:#0a0">есть</span><?break;
										case "Request":?><span style="color:#aa0">под запрос</span><?break;
										case "Unknown":?><span style="color:#666">нет данных</span><?break;
									endswitch;
									?></div>
									<div><b>Билеты эконом-класса:</b> <?
									switch ($row["EconomTicketsDpt"]):
										case "Stop":?><span style="color:#b00">нет &raquo;</span><?break;
										case "Available":?><span style="color:#0a0">есть &raquo;</span><?break;
										case "Request":?><span style="color:#aa0">под запрос &raquo;</span><?break;
										case "Unknown":?><span style="color:#666">нет данных &raquo;</span><?break;
									endswitch;
									?> - <?
									switch ($row["EconomTicketsRtn"]):
										case "Stop":?><span style="color:#b00">&laquo; нет</span><?break;
										case "Available":?><span style="color:#0a0">&laquo; есть</span><?break;
										case "Request":?><span style="color:#aa0">&laquo; под запрос</span><?break;
										case "Unknown":?><span style="color:#666">&laquo; нет данных</span><?break;
									endswitch;
									?></div>
									<div><b>Билеты бизнес-класса:</b> <?
									switch ($row["BusinessTicketsDpt"]):
										case "Stop":?><span style="color:#b00">нет &raquo;</span><?break;
										case "Available":?><span style="color:#0a0">есть &raquo;</span><?break;
										case "Request":?><span style="color:#aa0">под запрос &raquo;</span><?break;
										case "Unknown":?><span style="color:#666">нет данных &raquo;</span><?break;
									endswitch;
									?> - <?
									switch ($row["BusinessTicketsRtn"]):
										case "Stop":?><span style="color:#b00">&laquo; нет</span><?break;
										case "Available":?><span style="color:#0a0">&laquo; есть</span><?break;
										case "Request":?><span style="color:#aa0">&laquo; под запрос</span><?break;
										case "Unknown":?><span style="color:#666">&laquo; нет данных</span><?break;
									endswitch;
									?></div>
					            </div>
                              </div>
                            </div>
                          </div>
                           <div class="where"><?=$row["ResortName"]?></div>
									<?if($arResult["hotel"][$row["HotelId"]]["type"][0] != false):?>
										<div class="where">
												<?foreach($arResult["hotel"][$row["HotelId"]]["type"] as $num=>$type):?>
														<span><?=$type;?></span>
												<?endforeach;?>
										</div>
									<?endif;?>
                          <div class="palm">
                <?if($arParams["SALE"]>0):?><div class="old-price"><s><?=intval($row["Price"]/100*$arParams["SALE"])+$row["Price"]?> <?=($row["Currency"]=="RUB"?"руб.":$row["Currency"])?></s></div><?endif?>
                <div class="new-price"><?=$row["Price"]?> <?=($row["Currency"]=="RUB"?"руб.":$row["Currency"])?></div>
				<!-- 02.07.2014 добавлены доп параметры(топливный сбор и т.п.) -->
                <!--<div class="doplata">доплаты:</div>-->
                <?if($row["Tax_oil"] > 0 ):?>
	                <div class="doplata">топливный сбор - <?=$row["Tax_oil"]?> <?=$row["Tax_oil_currency"]?>/чел</div>
	            <?else:?>
					<div class="doplata">топливного сбора нет</div>
                <?endif;?>

                <?if($row["Tax_visa"] > 0 ):?>
	                <div class="vissbor">визовый сбор - <?=$row["Tax_visa"]?> <?=$row["Tax_visa_currency"]?></div>
	                <div class="vissbor">визовый сбор - <?=$row["Tax_visa_rub"]?> RUB</div>
	            <?else:?>
					<div class="vissbor">визового сбора нет</div>
                <?endif;?>                
				<?if($USER->IsAuthorized()):?>
				<?if(in_array($row["OfferId"], $arResult["BASKET"])):?>
					<span class="in-basket">В корзине</span>
				<?else:?>
                <a href="javascript:void(0);" class="oform" onClick="SearchTourDataBuy (<?=$arParams["ID"]?>, <?=$arParams["PAGE"]?>, <?=$arParams["COUNT"]?>, <?=$row["OfferId"]?>, <?=$arParams["SALE"]?>);">Оформить</a>
				<?endif?>
				<?else:?>
				<a class="oform no-user" onClick="authFormWindow.ShowLoginForm();" href="javascript:void(0);">Оформить</a>
				<?endif;?>
				</div>
               </div>
			<?
			if(($key+1)%4==0 && ($key+1)!=$arCount)echo"</div></div><div class=\"tour-list\"><div class=\"tour-list-in\">";
			endforeach?>
            </div>
          </div>
		  <a class="tname" style="font-size: 15px;" target="_blank" href="/faq/3099/">Доплаты при заказе тура</a>
</div>


<? /* постраничная навигация */ ?>
<?/*if($arResult["pageTotal"] > 0):?>
<div class="pagination">
<ul>
	<?if($arResult["pageNumber"] > 1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]-1)?>, <?=$arResult["pageCount"]?>);">Предыдущая</a></li><?endif;?>
	<?for($i=1;$i<=($arResult["pageTotal"]+1);$i++):?>
		<?if($arResult["pageNumber"] == $i):?>
			<li><span><?=$i?></span></li>
		<?else:?>
			<li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=$i?>, <?=$arResult["pageCount"]?>);"><?=$i?></a></li>
		<?endif;?>
	<?endfor;?>
	<?if($arResult["pageNumber"] < $arResult["pageTotal"]+1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]+1)?>, <?=$arResult["pageCount"]?>);">Следующая</a></li><?endif;?>
</ul>
</div>
<?endif;*/?>
<? /* постраничная навигация */ ?>
<?if($arResult["pageTotal"] > 0):
?>
<div class="pagination">
<ul>
<?if($arResult["pageTotal"]<10):?>
	<?if($arResult["pageNumber"] > 1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]-1)?>, <?=$arResult["pageCount"]?>);">Предыдущая</a></li><?endif;?>
	<?for($i=1;$i<=($arResult["pageTotal"]+1);$i++):?>
		<?if($arResult["pageNumber"] == $i):?>
			<li><span><?=$i?></span></li>
		<?else:?>
			<li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=$i?>, <?=$arResult["pageCount"]?>);"><?=$i?></a></li>
		<?endif;?>
	<?endfor;?>
	<?if($arResult["pageNumber"] < $arResult["pageTotal"]+1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]+1)?>, <?=$arResult["pageCount"]?>);">Следующая</a></li><?endif;?>
<?else:?>
	<?$varFirst=((($arResult["pageNumber"]-2)<1)?1:$arResult["pageNumber"]-2);$varLast=(($arResult["pageNumber"]+2)>($arResult["pageTotal"]+1)?$arResult["pageTotal"]+1:$arResult["pageNumber"]+2);?>
	<?if($varFirst>1):?>
	<li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, 1, <?=$arResult["pageCount"]?>);">Начало</a></li>
	<?endif?>
	<?if($arResult["pageNumber"] > 1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]-1)?>, <?=$arResult["pageCount"]?>);"><</a></li><?endif;?>
	<?for($i=$varFirst;$i<=($varLast);$i++):?>
		<?if($arResult["pageNumber"] == $i):?>
			<li><span><?=$i?></span></li>
		<?else:?>
			<li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=$i?>, <?=$arResult["pageCount"]?>);"><?=$i?></a></li>
		<?endif;?>
	<?endfor;?>
	<?if($arResult["pageNumber"] < $arResult["pageTotal"]+1):?><li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=($arResult["pageNumber"]+1)?>, <?=$arResult["pageCount"]?>);">></a></li><?endif;?>
	<?if($varLast<$arResult["pageTotal"]+1):?>
	<li><a href="#tourList" onClick="SearchTourData(<?=$arParams["ID"]?>, <?=$arResult["pageTotal"]+1?>, <?=$arResult["pageCount"]?>);">Конец</a></li>
	<?endif;?>
<?endif?>
</ul>
</div>
<?endif;?>
<?endif?>