<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "Идентификатор запроса",
			"TYPE" => "STRING",
			"DEFAULT" => "1",
		),
		"COUNT" => Array(
			"PARENT" => "BASE",
			"NAME" => "Кол-во туров на странице",
			"TYPE" => "STRING",
			"DEFAULT" => "30",
		),
		"PAGE" => Array(
			"PARENT" => "BASE",
			"NAME" => "Номер запрашиваемой страницы",
			"TYPE" => "STRING",
			"DEFAULT" => "1",
		),
		"OFFER_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "Идентификатор предложения",
			"TYPE" => "STRING",
			"DEFAULT" => "1",
		),		
	),
);
?>