<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// Подключение модуля
CModule::IncludeModule("simai.travel");
CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
global $DB;

/*
if (isset($_REQUEST['captcha_sid']))
{
	include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
	$cpt = new CCaptcha();
	if (!$cpt->CheckCode($_REQUEST['captcha_word'], $_REQUEST['captcha_sid']))
	{
		echo '<span style="color:red">Неверный код</span>';
		return false;
	}
}
*/

$arResult["data"] = CSimaiTravelSletat::GetRequestResult($arParams["ID"]);
// Список всех выдаваемых полей
//echo "\"".implode("\", \"", array_keys($arResult["data"]["GetRequestResultResult"]["Rows"]["XmlTourRecord"][0]))."\"";
//echo "<pre>";
//var_dump($arResult["data"]);
//echo "</pre>";


// Кол-во найденных туров
$arResult["count"] = count($arResult["data"]["GetRequestResultResult"]["Rows"]["XmlTourRecord"]);
$arResult["countStr"] = wform(substr($arResult["count"], -2), "тур", "тура", "туров");

// Топливные сборы определяются по ["SourceId"]
$arResult["oil"] = (is_array($arResult["data"]["GetRequestResultResult"]["OilTaxes"]["XmlTourOilTax"])) ? $arResult["data"]["GetRequestResultResult"]["OilTaxes"]["XmlTourOilTax"] : false;

// Визовые сборы в страну
$arResult["visa"] = (is_array($arResult["data"]["GetRequestResultResult"]["Visa"])) ? $arResult["data"]["GetRequestResultResult"]["Visa"] : false;

// Для постраничной навигации
$arResult["pageCount"] = $arParams["COUNT"];
$arResult["pageNumber"] = $arParams["PAGE"];
$arResult["pageTotal"] = floor($arResult["count"] / $arParams["COUNT"]);

// Определяем инфоблок туров
$res = CIBlock::GetList(Array(), Array('TYPE'=>'simai', 'CODE'=>'travel_tour'), true);
if($ar_res = $res->Fetch()) {
	$iblock_id = $ar_res['ID'];
}

// Используемые отели
$arResult["hotel"] = array();

// Определяем инфоблок отелей
$res = CIBlock::GetList(Array(), Array('TYPE'=>'simai', 'CODE'=>'travel_hotel'), true);
if($ar_res = $res->Fetch()) {
	$iblock_hotel = $ar_res['ID'];
}

// Составляем список туров в нужном виде
$arResult["rows"] = array();
$i = -1;
    foreach($arResult["data"]["GetRequestResultResult"]["Rows"]["XmlTourRecord"] as $row) {

    //Количесво взрослых и детей
    $adults = $row["Adults"];
    $kids = $row["Kids"];

    // Определяем максимальный топливный сбор
	$oil = 0; $oilCurrency = "RUB"; $oilRub = 0;
	foreach($arResult["data"]["GetRequestResultResult"]["OilTaxes"]["XmlTourOilTax"] as $curOil) {
		if($curOil["SourceId"] == $row["SourceId"] and CCurrencyRates::ConvertCurrency($curOil["Tax"], $curOil["CurrencyName"], "RUB") > $oilRub) {
			$oilRub = CCurrencyRates::ConvertCurrency($curOil["Tax"], $curOil["CurrencyName"], "RUB");
			$oil = $curOil["Tax"];
			$oilCurrency = $curOil["CurrencyName"];
		}
	}

	// Сохраняем установленные данные по сборам
	$row["Tax_oil"] = $oil;
	$row["Tax_oil_currency"] = $oilCurrency;
	$row["Tax_oil_rub"] = $oilRub;
	$row["Tax_visa"] = $arResult["visa"]["Price"];
	$row["Tax_visa_currency"] = $arResult["visa"]["CurrencyName"];
	$row["Tax_visa_rub"] = CCurrencyRates::ConvertCurrency($arResult["visa"]["Price"], $arResult["visa"]["CurrencyName"], "RUB");
	$row["Tax_price"] = $row["Price"];
	$row["Tax_currency"] = $row["Currency"];

    //Стоимость доплат за всех туристов и младенца (если есть)
    $row["Final_oil_tax"] = ($adults + $kids) * $row["Tax_oil_rub"];

	// Формируем реальную цену в рублях
	$row["Price"] = intval(CCurrencyRates::ConvertCurrency($row["Price"], $row["Currency"], "RUB") + $row["Final_oil_tax"]);

	$row["Currency"] = "RUB";

	// Покупка тура
	if(!empty($_REQUEST["tourBuy"]) and $_REQUEST["tourBuy"] == $row["OfferId"]) {

		global $USER;

        if (CModule::IncludeModule("sale"))
        {
            CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
        }

		// Добавляем тур в базу
		$el = new CIBlockElement;
		$PROP = $row;
		$PROP["USER_ID"] = $USER->GetID();
		$PROP["requestId"] = $arParams["ID"];
		$arLoadProductArray = Array(
			"MODIFIED_BY"    => $USER->GetID(),
			"IBLOCK_SECTION_ID" => false,
			"IBLOCK_ID"      => $iblock_id,
			"PROPERTY_VALUES"=> $PROP,
			//"NAME"           => "Куплен ".date("d.m.Y")." пользователем ".$USER->GetLogin()." (".$USER->GetFullName().")",
			"NAME"           => $row["TourName"],
			"ACTIVE"         => "Y",
		);
		$PRODUCT_ID = $el->Add($arLoadProductArray);

		// Проставляем количество
		$arFields = array(
			"ID" => $PRODUCT_ID,
			"QUANTITY" => 1,
			"QUANTITY_TRACE" => "N"
		);
		CCatalogProduct::Add($arFields);

		// Назначаем цену
		$arFields = Array(
		    "PRODUCT_ID" => $PRODUCT_ID,
		    "CATALOG_GROUP_ID" => 1,
		    "PRICE" => $row["Price"],
		    "CURRENCY" => $row["Currency"],
		    "QUANTITY_FROM" => false,
		    "QUANTITY_TO" => false
		);
		$PRODUCT_PRICE = CPrice::Add($arFields);

		// Добавляем тур в корзину
		$arFields = array(
			"PRODUCT_ID" => $PRODUCT_ID,
			"PRODUCT_PRICE_ID" => $PRODUCT_PRICE,
			"PRICE" => $row["Price"],
			"CURRENCY" => $row["Currency"],
			"WEIGHT" => 0,
			"QUANTITY" => 1,
			"LID" => LANG,
			"DELAY" => "N",
			"CAN_BUY" => "Y",
			"NAME" => $row["TourName"],
			//"CALLBACK_FUNC" => "MyBasketCallback",
			//"MODULE" => "simai.travel",
			"CALLBACK_FUNC" => "CatalogBasketCallback",
			"MODULE" => "catalog",
			"NOTES" => "",
			"ORDER_CALLBACK_FUNC" => "MyBasketOrderCallback",
			"DETAIL_PAGE_URL" => ""
		);
		$arFields["PROPS"]= array(
		0=>array(
			"NAME" => "Количество звезд",
			"CODE" => "STAR_NAME",
			"VALUE" => $row["StarName"]
		),
		1=>array(
			"NAME" => "Количество взрослых",
			"CODE" => "ADULTS",
			"VALUE" => $row["Adults"]
		),
		2=>array(
			"NAME" => "Дата вылета",
			"CODE" => "CHECK_IN_DATE",
			"VALUE" => $row["CheckInDate"],
			"SORT"=>"10"
		),
		3=>array(
			"NAME" => "Ночей",
			"CODE" => "NIGHTS",
			"VALUE" => $row["Nights"],
			"SORT"=>"20"
		),
		4=>array(
			"NAME" => "Страна",
			"CODE" => "COUNTRY_NAME",
			"VALUE" => $row["CountryName"]
		),
		5=>array(
			"NAME" => "Курорт",
			"CODE" => "RESORT_NAME",
			"VALUE" => $row["ResortName"],
			"SORT"=>"30"
		),
		6=>array(
			"NAME" => "Id отеля",
			"CODE" => "HOTEL_ID",
			"VALUE" => $row["HotelId"]
		),
		7=>array(
			"NAME" => "Отель",
			"CODE" => "HOTEL_NAME",
			"VALUE" => $row["HotelName"]
		),
		8=>array(
			"NAME" => "Рейтинг отеля",
			"CODE" => "HOTEL_RATING",
			"VALUE" => $row["HotelRating"]
		),
		9=>array(
			"NAME" => "Номер",
			"CODE" => "HT_PLACE_DESCRIPTION",
			"VALUE" => $row["HtPlaceDescription"]
		),
		10=>array(
			"NAME" => "Питание",
			"CODE" => "MEAL_DESCRIPTION",
			"VALUE" => $row["MealDescription"]
		),
		11=>array(
			"NAME" => "Стоимость",
			"CODE" => "PRICE",
			"VALUE" => "Оригинальная стоимость: ".$row["Tax_price"]." ".$row["Tax_currency"]."          \r\nВизовый сбор: ".$row["Tax_visa"]." ".$row["Tax_visa_currency"]."          \r\nТопливный сбор: ".$row["Tax_oil"]." ".$row["Tax_oil_currency"]
		),
		12=>array(
			"NAME" => "Город вылета",
			"CODE" => "CITY_FROM_NAME",
			"VALUE" => $row["CityFromName"]
		),
		13=>array(
            "NAME" => "Количество детей",
            "CODE" => "KIDS",
            "VALUE" => $row["Kids"]
        ),
		14=>array(
            "NAME" => "Общая сумма топливных доплат",
            "CODE" => "FINAL_OIL_TAX",
            "VALUE" => $row["Final_oil_tax"]
        ),
		15=>array(
            "NAME" => "Цена без доплат",
            "CODE" => "CLEAR_PRICE",
            "VALUE" => $row["Tax_price"]
        )

		);
		CSaleBasket::Add($arFields);


		//Add2BasketByProductID($PRODUCT_ID, 1, $arFields, $arFields["PROPS"]);
		//Add2Basket($PRODUCT_PRICE, 1, array(), $arFields["PROPS"]);
		$arResult["BUY"] = true;
	}


	// Постраничная навигация
	$i++;
	if($i < $arResult["pageCount"]*($arResult["pageNumber"]-1) or $i >= $arResult["pageCount"]*$arResult["pageNumber"]) continue;

	// Определяем день недели вылета и время до вылета
	$date = explode(".", $row["CheckInDate"]);
	$time = mktime(0, 0, 0, intval($date[1]), intval($date[0]), $date[2]);
	$row["CheckInDateDayOfWeek"] = date("w", $time);
	$row["CheckInDateStringTo"] = date_diff_str($time);

	$row["NightsStr"] = wform($row["Nights"], "ночь", "ночи", "ночей");


	// Определяем категорийность отелей
	if(!isset($arResult["hotel"][$row["HotelId"]])) {
		$arSelect = Array("ID", "NAME", "PROPERTY_HotelType");
		$arFilter = Array("IBLOCK_ID"=>$iblock_hotel, "CODE"=>$row["HotelId"]);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($hotel = $res->GetNext()) {
			$arResult["hotel"][$row["HotelId"]]["type"][] = $hotel["PROPERTY_HOTELTYPE_VALUE"];
			$arResult["hotel"][$row["HotelId"]]["type_id"][] = $hotel["PROPERTY_HOTELTYPE_ENUM_ID"];
		}
	}

	$arResult["rows"][] = $row;
}


// Отладка
if(!empty($_SESSION["travel"]["request"][$arParams["ID"]]) and true == false) {
	echo "<pre>Запрос:\r\n";
	var_dump($_SESSION["travel"]["request"][$arParams["ID"]]);
	echo "\r\n\r\nОтвет:\r\n";
	var_dump($arResult["data"]);
	echo "</pre>";
}


// Определяем какие туры куплены пользователем
$arResult["BASKET"] = array();
$dbBasketItems = CSaleBasket::GetList(
	array(
		"NAME" => "ASC",
		"ID" => "ASC"
	),
	array(
		"FUSER_ID" => CSaleBasket::GetBasketUserID(),
		"LID" => SITE_ID,
		"ORDER_ID" => "NULL"
	),
	false,
	false,
	array(
		"ID",
		"CALLBACK_FUNC",
		"MODULE",
		"PRODUCT_ID",
		"QUANTITY",
		"DELAY",
		"CAN_BUY",
		"PRICE",
		"WEIGHT"
	)
);
while ($arItems = $dbBasketItems->Fetch()) {
	$q = $DB->Query("SELECT `VALUE` FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".$arItems["PRODUCT_ID"]."' AND `IBLOCK_PROPERTY_ID`= (
		SELECT `ID` FROM `b_iblock_property` WHERE `IBLOCK_ID` = '".$iblock_id."' AND `CODE`='OfferId'
	)");
	if ($row = $q->GetNext()) {
		$arResult["BASKET"][] = $row["VALUE"];
	}
}

/*
echo "<pre>";
var_dump($arResult);
echo "</pre>";
*/
$this->IncludeComponentTemplate();


//
// Импортируем новые отели в систему
//
if(empty($_SESSION["HOTEL_IMPORT"][$arParams["ID"]])) {

	// Запускаем перебор отелей только 1 раз
	$_SESSION["HOTEL_IMPORT"][$arParams["ID"]] = true;

	// Определяем инфоблок отелей
	$res = CIBlock::GetList(Array(), Array('TYPE'=>'simai', 'CODE'=>'travel_hotel'), true);
	if($ar_res = $res->Fetch()) {
		$iblock_hotel = $ar_res['ID'];
	}

	// Определяем инфоблок отзывов
	$res = CIBlock::GetList(Array(), Array('TYPE'=>'simai', 'CODE'=>'travel_reviews'), true);
	if($ar_res = $res->Fetch()) {
		$iblock_reviews = $ar_res['ID'];
	}

	$hotelList = array();
	foreach($arResult["data"]["GetRequestResultResult"]["Rows"]["XmlTourRecord"] as $hotel) {

		if($hotel["HotelId"] < 1) continue;

		if(in_array($hotel["HotelId"], $hotelList)) continue;
		$hotelList[] = $hotel["HotelId"];

		// Проверяем есть ли такой отель в базе
		$q = $DB->Query("SELECT * FROM `b_iblock_element` WHERE `IBLOCK_ID` = '".$iblock_hotel."' AND `CODE`='".$hotel["HotelId"]."'");
		if ($row = $q->GetNext()) {
			continue;
		}

		// Категория первого уровня
		$arFilter = Array('IBLOCK_ID'=>$iblock_hotel, 'NAME'=>$hotel["CountryName"]);
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);
		if($ar_result = $db_list->GetNext()){
			$section_1 = $ar_result['ID'];
		}
		else {
			$bs = new CIBlockSection;
			$arFields = Array(
				"ACTIVE" => "Y",
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID" => $iblock_hotel,
				"NAME" => $hotel["CountryName"],
				"CODE" => $hotel["CountryId"],
			);
			$section_1 = $bs->Add($arFields);
		}

		// Категория второго уровня
		$arFilter = Array('IBLOCK_ID'=>$iblock_hotel, 'SECTION_ID'=>$section_1, 'NAME'=>$hotel["ResortName"]);
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);
		if($ar_result = $db_list->GetNext()){
			$section_2 = $ar_result['ID'];
		}
		else {
			$bs = new CIBlockSection;
			$arFields = Array(
				"ACTIVE" => "Y",
				"IBLOCK_SECTION_ID" => $section_1,
				"IBLOCK_ID" => $iblock_hotel,
				"NAME" => $hotel["ResortName"],
				"CODE" => $hotel["ResortId"],
			);
			$section_2 = $bs->Add($arFields);
		}

		// Добавляем отель в систему
		$el = new CIBlockElement;
		$PROP = $hotel;
		$arLoadProductArray = Array(
			"IBLOCK_SECTION_ID" => $section_2,
			"IBLOCK_ID"      => $iblock_hotel,
			"PROPERTY_VALUES"=> $PROP,
			"NAME"           => $hotel["HotelName"],
			"CODE"           => $hotel["HotelId"],
			"ACTIVE"         => "Y",
		);
		$curHotel = $el->Add($arLoadProductArray);



		//
		// ИМПОРТ ОТЗЫВОВ СО СТАРОГО САЙТА
		//

		$iblock_old_hotel = 2;
		$iblock_old_reviews = 10;

		// Категория первого уровня
		$arFilter = Array('IBLOCK_ID'=>$iblock_old_hotel, 'NAME'=>$hotel["CountryName"]);
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);
		if($ar_result = $db_list->GetNext()){
			$section_1 = $ar_result['ID'];
			// Категория второго уровня
			$arFilter = Array('IBLOCK_ID'=>$iblock_old_hotel, 'SECTION_ID'=>$section_1, 'NAME'=>$hotel["ResortName"]);
			$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);
			if($ar_result = $db_list->GetNext()){
				$section_2 = $ar_result['ID'];
				// Отель
				$arFilter = Array("IBLOCK_ID"=>$iblock_old_hotel, "%NAME"=>$hotel["HotelName"], "ACTIVE"=>"Y");
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array());
				if($old_hotel = $res->GetNext()){
					// Перебираем все доступные отзывы и дублируем их
					$arFilter = Array("IBLOCK_ID"=>$iblock_old_reviews, "PROPERTY_HOTEL"=>$old_hotel["ID"], "ACTIVE"=>"Y");
					$res = CIBlockElement::GetList(Array("active_from"=>"asc"), $arFilter, false, false, array());
					while($ob = $res->GetNextElement()){
						$arFields = $ob->GetFields();
						$arProps = $ob->GetProperties();
						// Добавляем новый отзыв в систему
						$el = new CIBlockElement;
						$PROP = array();
						foreach($arProps as $nameProp => $valueProp) {
							$PROP[$nameProp] = $valueProp["VALUE"];
						}
						$PROP["HOTEL"] = $curHotel;
						$PROP["HOTEL_CODE"] = $hotel["HotelId"];
						$arLoadProductArray = Array(
							"IBLOCK_SECTION_ID" => false,
							"IBLOCK_ID"         => $iblock_reviews,
							"PROPERTY_VALUES"   => $PROP,
							"NAME"              => $arFields["NAME"],
							"DATE_CREATE"       => $arFields["DATE_CREATE"],
							"PREVIEW_TEXT"      => $arFields["PREVIEW_TEXT"],
							"ACTIVE_FROM"       => $arFields["ACTIVE_FROM"],
							"SORT"              => $arFields["SORT"],
							"PREVIEW_TEXT_TYPE" => $arFields["PREVIEW_TEXT_TYPE"]
						);
						$el->Add($arLoadProductArray);
					}
				}
			}
		}

		//
		// ---ИМПОРТ ОТЗЫВОВ СО СТАРОГО САЙТА
		//

	} // ---Перебор списка отелей
}
//
// ---Импортируем новые отели в систему
//

function wform($count, $single, $partitive, $plural) {
	return in_array($count,
		array(1, 21, 31, 41, 51)) ? $single : (
			in_array($count, array(2, 3, 4, 22, 23, 24, 32, 33, 34, 42, 43, 44, 52, 53, 54)) ? $partitive : $plural);
}

function date_diff_str($time) {
	$diff = floor(($time - time()) / (60*60*24));
	if ($diff < 8)
		return sprintf("%d %s", $diff, wform($diff, "день", "дня", "дней"));
	else
		return sprintf("%d %s %d %s", floor($diff / 7), wform(floor($diff / 7), "неделя", "недели", "неделей"), floor($diff % 7), wform(($diff % 7), "день", "дня", "дней"));
}
?>