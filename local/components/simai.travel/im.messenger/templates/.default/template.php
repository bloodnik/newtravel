<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<style>
.bx-messanger {background: none !important;cursor: pointer;padding: 0 !important;}
</style>
<div id="bx-notifier-panel" style="position: static;background: none !important;" class="bx-notifier-panel">
<ul class="myinfo">
	<li><span class="bx-notifier-message bx-messanger" title="<?=GetMessage('IM_MESSENGER_OPEN_MESSENGER');?>"><?=GetMessage('IM_MESSENGER_OPEN_MESSENGER');?> <span style="color:#D22A37">(<span id="info_about_mess" class="bx-notifier-indicator-count"></span>)</span></span></li>
		
		<a style="display: none;" href="javascript:void(0)" class="bx-notifier-indicator bx-notifier-notify" title="<?=GetMessage('IM_MESSENGER_OPEN_NOTIFY');?>"><span class="bx-notifier-indicator-text"></span><span class="bx-notifier-indicator-icon"></span><span class="bx-notifier-indicator-count">0</span>
		</a>
		
		
		<a style="display: none;" class="bx-notifier-indicator bx-notifier-mail" href="#mail" title="<?=GetMessage('IM_MESSENGER_OPEN_EMAIL');?>" target="_blank"><span class="bx-notifier-indicator-icon"></span><span class="bx-notifier-indicator-count">0</span></a>
		
	
		<li>
		<a onClick="BXIM.openMessenger();return false;" href="javascript:void(0)" title="<?=GetMessage('IM_MESSENGER_OPEN_CONTACT_LIST');?>"><?=GetMessage('IM_MESSENGER_OPEN_CONTACT_LIST');?></a></li>
</ul>
</div>
<script type="text/javascript">
<?=CIMMessenger::GetTemplateJS(Array(), $arResult)?>
</script>