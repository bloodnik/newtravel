<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (isset($_REQUEST['AJAX_CALL']) && $_REQUEST['AJAX_CALL'] == 'Y')
	return;

if (defined('IM_COMPONENT_INIT'))
	return;
else
	define("IM_COMPONENT_INIT", true);

if (intval($USER->GetID()) <= 0)
	return;

if (!CModule::IncludeModule('im'))
	return;

if (!CModule::IncludeModule("socialnetwork"))
	return;

$arParams["DESKTOP"] = isset($arParams['DESKTOP']) && $arParams['DESKTOP'] == 'Y'? 'Y': 'N';


$arResult = Array();

// Counters
$arResult["COUNTERS"] = Array();
if (CModule::IncludeModule("socialnetwork"))
	$arResult["COUNTERS"] = CSocNetLogCounter::GetCodeValuesByUserID($USER->GetID(), SITE_ID);

CIMContactList::SetOnline(null, true);

if ($arParams['DESKTOP'] == 'Y')
{
	$GLOBALS["APPLICATION"]->SetPageProperty("BodyClass", "im-desktop");
	CIMMessenger::SetDesktopStatusOnline();
}

$arParams["INIT"] = 'Y';
$arParams["DESKTOP_LINK_OPEN"] = 'N';

// Exchange
$arResult["PATH_TO_USER_MAIL"] = "";
$arResult["MAIL_COUNTER"] = 0;
if ($arParams["INIT"] == 'Y' && CModule::IncludeModule("dav"))
{
	$ar = CDavExchangeMail::GetTicker($GLOBALS["USER"]);
	if ($ar !== null)
	{
		$arResult["PATH_TO_USER_MAIL"] = $ar["exchangeMailboxPath"];
		$arResult["MAIL_COUNTER"] = intval($ar["numberOfUnreadMessages"]);
	}
}

// Message & Notify
if ($arParams["INIT"] == 'Y')
{
	$arSettings = CIMMessenger::GetSettings();

	$CIMContactList = new CIMContactList();
	$arResult['CONTACT_LIST'] = $CIMContactList->GetList();

// Модификация
global $USER;
if(!CSite::InGroup(array(8)) or true == true) {

	// Создаем группу пользователей
	$arResult['CONTACT_LIST']["groups"]["consult"] = array(
		"id" => "consult",
		"status" => "open",
		"name" => "Менеджеры онлайн"
	);

	$arResult['CONTACT_LIST']["woGroups"]["consult"] = array(
		"id" => "consult",
		"status" => "open",
		"name" => "Менеджеры онлайн"
	);

	// Список пользователей-консультантов
	$listUser = array();
	$filter = Array (
	    "ACTIVE"              => "Y",
	    "GROUPS_ID"           => Array(8)
	);
	$rsUsers = CUser::GetList(($by="name"), ($order="desc"), $filter);
	//$is_filtered = $rsUsers->is_filtered; // отфильтрована ли выборка ?
	//$rsUsers->NavStart(50); // разбиваем постранично по 50 записей
	//while($rsUsers->NavNext(true, "f_")) :
	while($users = $rsUsers->GetNext()) :

		// Самого себя не добавляем в контакт-лист
		if($users["ID"] == $USER->GetID()) continue;

		// Сохраняем пользователей в контакт-листе

		$status = CSocNetUser::IsOnLine($users["ID"]) == true ? "online" : "offline";
		$arResult['CONTACT_LIST']["users"][$users["ID"]] = array(
			"id" => $users["ID"],
			"name" => $users["NAME"]." ".$users["LAST_NAME"],
			"avatar" => "/bitrix/js/im/images/blank.gif",
			"status" => $status,
			"profile" => "/people/user/".$users["ID"]."/"
		);

		$listUser[] = $users["ID"];

	endwhile;

	// Добавляем пользователей в группу
	$arResult['CONTACT_LIST']["userInGroup"]["consult"] = array(
		"id" => "consult",
		"users" => $listUser
	);

	$arResult['CONTACT_LIST']["woUserInGroup"]["consult"] = array(
		"id" => "consult",
		"users" => $listUser
	);

}
// -- Модификация

	$CIMNotify = new CIMNotify();
	$arResult['NOTIFY'] = $CIMNotify->GetUnreadNotify();
	$arResult['NOTIFY']['flashNotify'] = CIMNotify::GetFlashNotify($arResult['NOTIFY']['unreadNotify']);
	$arResult['NOTIFY']['countNotify'] = $CIMNotify->GetNotifyCounter($arResult['NOTIFY']);
	$arResult["NOTIFY_COUNTER"] = $arResult['NOTIFY']['countNotify']; // legacy

	$CIMMessage = new CIMMessage();
	$arResult['MESSAGE'] = $CIMMessage->GetUnreadMessage();
	$arResult['MESSAGE']['flashMessage'] = CIMMessage::GetFlashMessage($arResult['MESSAGE']['unreadMessage']);
	$arResult['MESSAGE']['countMessage'] = $CIMMessage->GetMessageCounter($arResult['MESSAGE']);
	$arResult["MESSAGE_COUNTER"] = $arResult['MESSAGE']['countMessage']; // legacy

	$arRecent = CIMContactList::GetRecentList(Array(
		'LOAD_USERS' => 'N',
		'LOAD_LAST_MESSAGE' => 'Y',
		'USE_TIME_ZONE' => 'N'
	));
	$arResult['RECENT'] = Array();
	foreach ($arRecent as $userId => $value)
	{
		if (!isset($arResult['MESSAGE']['message'][$value['MESSAGE']['id']]))
		{
			$arResult['MESSAGE']['message'][$value['MESSAGE']['id']] = $value['MESSAGE'];
			$arResult['MESSAGE']['usersMessage'][$userId][] = $value['MESSAGE']['id'];
		}
		$value['MESSAGE']['userId'] = $userId;
		$arResult['RECENT'][] = $value['MESSAGE'];
	}
	// Merge message users with contact list
	if (isset($arResult['MESSAGE']['users']) && !empty($arResult['MESSAGE']['users']))
	{
		foreach ($arResult['MESSAGE']['users'] as $arUser)
			$arResult['CONTACT_LIST']['users'][$arUser['id']] = $arUser;

		if (isset($arResult['MESSAGE']['userInGroup']))
		{
			foreach ($arResult['MESSAGE']['userInGroup'] as $arUserInGroup)
			{
				if (isset($arResult['CONTACT_LIST']['userInGroup'][$arUserInGroup['id']]['users']))
					$arResult['CONTACT_LIST']['userInGroup'][$arUserInGroup['id']]['users'] = array_unique(array_merge($arResult['CONTACT_LIST']['userInGroup'][$arUserInGroup['id']]['users'], $arUserInGroup['users']));
				else
				{
					if (isset($arResult['CONTACT_LIST']['userInGroup']['other']['users']))
						$arResult['CONTACT_LIST']['userInGroup']['other']['users'] = array_unique(array_merge($arResult['CONTACT_LIST']['userInGroup']['other']['users'], $arUserInGroup['users']));
					else
					{
						$arUserInGroup['id'] = 'other';
						$arResult['CONTACT_LIST']['userInGroup']['other'] = $arUserInGroup;
					}
				}
			}
		}
		if (isset($arResult['MESSAGE']['woUserInGroup']))
		{
			foreach ($arResult['MESSAGE']['woUserInGroup'] as $arWoUserInGroup)
			{
				if (isset($arResult['CONTACT_LIST']['woUserInGroup'][$arWoUserInGroup['id']]['users']))
					$arResult['CONTACT_LIST']['woUserInGroup'][$arWoUserInGroup['id']]['users'] = array_merge($arResult['CONTACT_LIST']['woUserInGroup'][$arWoUserInGroup['id']]['users'], $arWoUserInGroup['users']);
				else
				{
					if (isset($arResult['CONTACT_LIST']['woUserInGroup']['other']['users']))
						$arResult['CONTACT_LIST']['woUserInGroup']['other']['users'] = array_merge($arResult['CONTACT_LIST']['woUserInGroup']['other']['users'], $arWoUserInGroup['users']);
					else
					{
						$arWoUserInGroup['id'] = 'other';
						$arResult['CONTACT_LIST']['woUserInGroup']['other'] = $arWoUserInGroup;
					}
				}
			}
		}
	}
	$arResult['OPEN_TAB'] = CIMMessenger::GetOpenTabs();
	$arResult['CURRENT_TAB'] = CIMMessenger::GetCurrentTab();
	$arResult['STATUS'] = isset($arSettings['status'])? $arSettings['status']: 'online';
	$arResult['VIEW_OFFLINE'] = isset($arSettings['viewOffline']) && $arSettings['viewOffline'] == 'N'? 'false': 'true';
	$arResult['VIEW_GROUP'] = isset($arSettings['viewGroup']) && $arSettings['viewGroup'] == 'N'? 'false': 'true';
	$arResult['ENABLE_SOUND'] = isset($arSettings['enableSound']) && $arSettings['enableSound'] == 'N'? 'false': 'true';
	$arResult['SEND_BY_ENTER'] = isset($arSettings['sendByEnter']) && $arSettings['sendByEnter'] == 'Y'? 'true': 'false';
	$arResult['PANEL_POSTION_HORIZONTAL'] = isset($arSettings['panelPositionHorizontal']) && in_array($arSettings['panelPositionHorizontal'], Array('left', 'center', 'right'))? $arSettings['panelPositionHorizontal']: 'right';
	$arResult['PANEL_POSTION_VERTICAL'] = isset($arSettings['panelPositionVertical']) && in_array($arSettings['panelPositionVertical'], Array('top', 'bottom'))? $arSettings['panelPositionVertical']: 'bottom';
}
else
{
	$arResult['STATUS'] = 'online';
	$arResult['ENABLE_SOUND'] = 'false';
}
$arResult['DESKTOP'] = $arParams['DESKTOP'] == 'Y'? 'true': 'false';
$arResult["INIT"] = $arParams['INIT'];
$arResult['DESKTOP_LINK_OPEN'] = $arParams['DESKTOP_LINK_OPEN'] == 'Y'? 'true': 'false';
$arResult['PATH_TO_USER_PROFILE_TEMPLATE'] = COption::GetOptionString('im', 'path_to_user_profile', "", CModule::IncludeModule('extranet') && !CExtranet::IsIntranetUser()? "ex": false);
$arResult['PATH_TO_USER_PROFILE'] = CComponentEngine::MakePathFromTemplate($arResult['PATH_TO_USER_PROFILE_TEMPLATE'], array('user_id' => $USER->GetId()));

CJSCore::Init(array('im'));
if (!(isset($arParams['TEMPLATE_HIDE']) && $arParams['TEMPLATE_HIDE'] == 'Y'))
	$this->IncludeComponentTemplate();

return $arResult;

?>