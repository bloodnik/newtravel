<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// Преобразовываем дату в нужный вид
$arParams["DAY_FROM"] = date("d.m.Y", time()+60*60*24*$arParams["DAY_FROM"]);
$arParams["DAY_TO"] = date("d.m.Y", time()+60*60*24*$arParams["DAY_TO"]);


$arResult = array();

// Кеширование
$obCache = new CPageCache;
if($obCache->StartDataCache($arParams["CACHE_TIME"], serialize($arParams),  "/")):

	// Подключение модуля
	CModule::IncludeModule("simai.travel");
	CModule::IncludeModule("iblock");


	// Информация о стране
	$arFilter = Array('IBLOCK_CODE'=>'travel_hotel', 'GLOBAL_ACTIVE'=>'Y', 'CODE'=>$arParams["COUNTRY"]);
	$res = CIBlockSection::GetList(Array("sort"=>"asc"), $arFilter, false);
	if($ar_res = $res->GetNext()) {
		$arResult["COUNTRY"] = $ar_res;
	}
	else {
		echo '<font color="#bb3333">Страна не найдена</font>';
		return false;
	}

	// Список туров в страну
	$params = array(
		'requestId'=>0,
		'pageSize'=>30,
		'pageNumber'=>1,
		"countryId"=>$arParams["COUNTRY"],
		"cityFromId"=>$arParams["CITY"],
		"cities"=>array(),
		"meals"=> array(),
		"stars"=> array(),
		"hotels"=> array(),
		"adults"=>2,
		"kids"=>0,
		"kidsAges"=>array(),
		"nightsMin" => $arParams["NIGHT_FROM"],
		"nightsMax" => $arParams["NIGHT_TO"],
		"priceMin" => "",
		"priceMax" => "",
		"currencyAlias" => $arParams["CURRENCY"],
		"departFrom" => $arParams["DAY_FROM"],
		"departTo" => $arParams["DAY_TO"],
		'hotelIsNotInStop'=>true,
		'hasTickets'=>true,
		'ticketsIncluded'=>true,
		'updateResult'=>true,
		'useFilter'=>false,
		'f_to_id'=>array(),
		'useTree'=>false,
		'groupBy'=>false,
		'includeDescriptions'=>true
	);
	//$arResult["TOUR"] = CSimaiTravelSletat::CreateRequest($params);

	$this->IncludeComponentTemplate();

    // записываем предварительно буферизированный вывод в файл кеша
    $obCache->EndDataCache();

endif;
?>