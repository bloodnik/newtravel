<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div id="countryTour"></div>

<script type="text/javascript">
if(window.jQuery==undefined) {
	alert('Необходимо подключить библиотеку jQuery');
}
$(document).ready(function() {
	CountryTourStatus (<?=$arResult["TOUR"]?>, <?=$arParams["COUNT"]?>);
});
</script>
