<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["SALE"]=intval($arParams["SALE_PERCENT"]);?>
<script type="text/javascript">
$(function(){
	$("#social_block_id").click(function(){
		$(this).next().toggleClass("visible-light-box");
	})
});
</script>
<div class="left-col">
<div id="hotelTour" style="clear: both;"></div>
<div class="details">
<div class="detail-in">
                  <div class="detail-top">
                    <div  style="margin-left:10px;" class="predloj"><a href="#hotelTour" class="predl"><?=getMessage("HOTEL_LIST")?></a><?=getMessage("ACTUALLY_LIST")?></div>
                    <div class="predloj"><a href="/faq/application.php?country=<?=urlencode($arResult["HOTEL"]["PROP"]["CountryName"]["VALUE"])?>&hotel=<?=urlencode($arResult["HOTEL"]["PROP"]["HotelName"]["VALUE"])?>" target="_blank" id="hotel_whant_id" class="predl"><?=getMessage("WHANT_HOTEL")?></a><?=getMessage("MANAGER_HELP")?></div>
									  <div id="social_block_id" class="predloj" style="float: right;margin: 0 10px 0 0;"><a href="javascript:void(0);" class="predl ppp">Рассказать друзьям</a></div>
<div class="social-light-box">
<?
$APPLICATION->IncludeComponent("simai:asd.share.buttons","", Array(
        "ASD_ID" => "",	// ID элемента (обязательно для списка)
        "ASD_TITLE" => $APPLICATION->GetTitle(),	// Заголовок
        "ASD_URL" => $APPLICATION->GetCurPage(),	// Детальный URL (относительный)
        "ASD_PICTURE" => "",	// Адрес изображения (относительный)
        "ASD_TEXT" =>"",	// Текст-комментарий
        "ASD_LINK_TITLE" => "Расшарить в #SERVICE#",	// Шаблон всплывающих подсказок
        "ASD_INCLUDE_SCRIPTS" => array(	// Включить подгружаемые скрипты
            0 => "FB_LIKE",
            //1 => "VK_LIKE",
            2 => "TWITTER",
            3 => "GOOGLE",
        ),
        "LIKE_TYPE" => "LIKE",	// Тип "Мне нравится"
        "VK_API_ID" => "3035730",	// API ID Вконтакте
        "VK_LIKE_VIEW" => "mini",	// Вид "Мне нравится" для ВКонтакте
        "TW_DATA_VIA" => "",	// Twitter data-via (в твит добавляет @tratata)
        "SCRIPT_IN_HEAD" => "N",	// Добавлять скрипты в секцию head (Важно! компонент должен находиться вне области кеширования)
    ),
    false
);?>
</div>
                  
<?if(!empty($arResult["HOTEL"]["PROP"]["HotelType"]["VALUE"])):?>
<div class="detail-top-bottom">
<ul>
<?foreach($arResult["HOTEL"]["PROP"]["HotelType"]["VALUE"] as $key=>$arValue):
?>
<li><?=$arValue?></li>
<?
endforeach?>
</ul>
</div>
<?endif?>				  
</div>
<iframe id="hotelData" name="hotelData" src="<?=$arResult["HOTEL"]["PROP"]["HotelDescriptionUrl"]["VALUE"]?>" width="100%" height="815px;" scrolling="auto" frameborder="no">
Ваш браузер не поддерживает плавающие фреймы!
</iframe>
</div>
</div>
</div>
    <?if (IntVal($arParams["RES_ID"]) <= 0):?>
        <div id="hotelTour" style="clear: both;">Идет поиск туров...</div>
        <script type="text/javascript">
        $(document).ready(function(){
            if(typeof(HotelTourStatus)!="undefined"){
                HotelTourStatus (<?=$arResult["TOUR"]?>, <?=$arParams["COUNT"]?>, <?=$arParams["SALE"]?>);
            }
        });
        </script>
    <?else:?>
        <div style="clear: both;"></div>
        <!-- a href="javascript:SearchByClick()" class="predl ppp" style="width:180px">Искать туры для отеля</a -->

        <script type="text/javascript">
        $(document).ready(function(){
                if(typeof(HotelTourData)!="undefined"){
                    HotelTourData (<?=IntVal($arParams["RES_ID"])?>, <?=$arParams["COUNT"]?>, <?=$arParams["SALE"]?>, <?=$arParams["OFFER_ID"]?>);  //09.07.2014 Добавлен параметр OFFER_ID для поиска предложения по отелю
                }
        });
        function SearchByClick(){
            $("#hotelTour").html("Идет поиск туров...");
            if(typeof(HotelTourStatus)!="undefined"){
                HotelTourStatus (<?=$arResult["TOUR"]?>, <?=$arParams["COUNT"]?>, <?=$arParams["SALE"]?>);
            }
        }
        </script>
    <?endif;?>