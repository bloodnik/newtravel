<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

////////Отладка/////////////////
//PR($arResult);
/////////////////////////////////

$arParams["SALE"]=intval($arParams["SALE_PERCENT"]);?>
<div id="hotelTour" style="clear: both;"></div>
<?
$this->AddEditAction($arResult["HOTEL"]['ID'], $arResult['EDIT_LINK'], CIBlock::GetArrayByID($arResult["HOTEL"]["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arResult["HOTEL"]['ID'], $arResult['DELETE_LINK'], CIBlock::GetArrayByID($arResult["HOTEL"]["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
<section class="gray-section detail-hotel" id="<?=$this->GetEditAreaId($arResult["HOTEL"]['ID']);?>">
    <!-- container -->
    <div class="container">
            <?if(is_array($arResult["ITEM"]) and empty($arResult["ITEM"]["HotelId"])):?>
                <p class="text-error">Информация об отеле не найдена</p>
            <?else:?>
                <!--Шапка страницы отеля-->
                <div class="row page-header">
                    <div class="col-md-2 col-xs-12">
                        <div class="hotel-name"><?=$arResult["ITEM"]["Name"]?></div>
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <div class="detail-hotel-stars">
                            <?switch ($arResult["ITEM"]["StarName"]) {
                                case "1*":
                                    echo '<i class="fa fa-star"></i>';
                                    break;
                                case "2*":
                                    echo '<i class="fa fa-star"></i><i class="fa fa-star"></i>';
                                    break;
                                case "3*":
                                    echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
                                    break;
                                case "4*":
                                    echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
                                    break;
                                case "5*":
                                    echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
                                    break;
                                case 'Apts':
                                    echo "Аппартаменты";
                                    break;
                                case 'Villas':
                                    echo "Вилла";
                                    break;
                                default:
                                    echo "";
                                    break;
                            }?>

                        </div>
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <div class="detail-item-resort"><a href="/hotels/<?=$arResult["COUNTRY_IBLOCK_ID"]?>/" target="_blank"><?=$arResult["ITEM"]["CountryName"]?></a>, <a href="/hotels/<?=$arResult["RESORT_IBLOCK_ID"]?>/" target="_blank"><?=$arResult["ITEM"]["Resort"]?></a></div>
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <div class="detail-hotel-reviews-count">Отзывов <a href="#reviews"><span class="review-count">0</span></a></div>
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <div class="hotel-additional">
                            <?foreach ($arResult["HOTEL"]["PROP"]["HotelType"]["VALUE_ENUM_ID"] as $HotelTypeId) {
                                switch ($HotelTypeId) {
                                    case 105:
                                        echo '<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" style="color:#B9E0A5"></i><i class="fa fa-lightbulb-o fa-stack-1x"></i></span>';
                                        break;
                                    case 104:
                                        echo '<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" style="color:#7EA6E0"></i><i class="fa fa-lightbulb-o fa-stack-1x"></i></span>';
                                        break;
                                    case 107:
                                        echo '<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" style="color:#F8B5CC"></i><i class="fa fa-lightbulb-o fa-stack-1x"></i></span>';
                                        break;
                                }
                            };?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="item-rating">
                            <span class="fa-stack fa-lg">
                              <i class="fa fa-circle fa-stack-2x" style="color:#B9E0A5"></i>
                              <i class="fa fa-thumbs-up fa-stack-1x"></i>
                            </span>
                            <span style="font-size: 18px">
                            Рейтинг <?=$arResult["ITEM"]["HotelRate"]?>/10
                            </span>
                        </div>
                    </div>
                </div> <!--/Шапка страницы отеля-->
                <div class="tab-buttons">
                    <ul class="nav ">
                        <li class="active"><a href="#slider" data-toggle="tab">Фотографии</a></li>
                        <li><a href="#hotel-map" data-toggle="tab">Карта</a></li>
                    </ul>
                </div>                
    </div><!-- /container -->

                    <!-- Карта и фотослайдер -->
                <div class="tab-section">
                    <div class="col-md-12">
                        <div id="hotel-tab-content" class="tab-content">
                            <div class="tab-pane active" id="slider">


                                <div class="jcarousel-wrapper">

                                    <!-- Carousel -->
                                    <div class="jcarousel" data-jcarousel="true">
                                        <ul style="left: 0px; top: 0px;">
                                            <?foreach ($arResult["ITEM"]["ImageUrls"]["string"] as $value):?>
                                                <li><a href="<?=$value?>" class="fancybox" data-fancybox-group="gallery3"><img src="<?=$value?>" width="600" height="400" alt=""></a></li>
                                            <?endforeach?>
                                        </ul>
                                    </div>

                                    <!-- Prev/next controls -->
                                    <a href="#" class="jcarousel-control-prev inactive" data-jcarouselcontrol="true"><i class="fa fa-arrow-circle-left fa-2x"></i></a>
                                    <a href="#" class="jcarousel-control-next" data-jcarouselcontrol="true"><i class="fa fa-arrow-circle-right fa-2x"></i></a>

                                </div>

                            </div>
                            <div class="tab-pane" id="hotel-map">
                                <div id="map" style="width: 100%; height: 400px"></div>
                            </div>
                        </div>
                    </div>
                </div><!-- /row -->

                <div class="container">
                    <div class="row" style="padding: 20px 0 20px 0">
                        <div class="col-md-4 col-xs-12 hotel-info">
                            <i class="fa fa-mobile fa-4x"></i>
                            <ul class="contact-info-list">
                                <li><b>Email:</b>  <?=isset($arResult["ITEM"]["Email"])?$arResult["ITEM"]["Email"]:"-"?></li>
                                <li><b>Телефон:</b>  <?=isset($arResult["ITEM"]["Phone"])?$arResult["ITEM"]["Phone"]:"-"?></li>
                                <li><b>Факс:</b>  <?=isset($arResult["ITEM"]["Fax"])?$arResult["ITEM"]["Fax"]:"-"?></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-xs-12 hotel-info">
                            <i class="fa fa-home fa-4x"></i>
                            <ul class="hotel-info-list">
                                <li><b>Количество номеров:</b><?=isset($arResult["ITEM"]["RoomsCount"])?$arResult["ITEM"]["RoomsCount"]:"-"?></li>
                                <li><b>Растояние до аэропорта:</b> <?=isset($arResult["ITEM"]["AirportDistance"])?$arResult["ITEM"]["AirportDistance"]."км":"-"?></li>
                                <li><b>Растояние до центра города:</b>  <?=isset($arResult["ITEM"]["CityCenterDistance"])?$arResult["ITEM"]["CityCenterDistance"]."км":"-"?></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-xs-12 hotel-info">
                            <i class="fa fa-map-marker fa-4x"></i>
                            <ul class="hotel-info-list" style="padding-top: 20px">
                                <li><b>Удаленность от пляжа(моря):</b><?=isset($arResult["ITEM"]["BEACH_LINE"])?$arResult["ITEM"]["BEACH_LINE"]:"-"?></li>
                            </ul>
                        </div>
                    </div>
                </div>    

                <div class="container">

                    <?$APPLICATION->IncludeComponent(
                        "simai.travel:full_search",
                        "adapt_hotel_tour_search",
                        Array(
                            "SALE_PERCENT" => "10",
                            "CITY" => "1274",
                            "COUNTRY" => $arResult["ITEM"]["CountryId"],
                            "RESORT" => $arResult["ITEM"]["ResortId"],
                            "HOTEL" => $arResult["ITEM"]["HotelId"],
                            "STARS" => $arResult["ITEM"]["StarId"],
                            "DAY_FROM" => "1",
                            "DAY_TO" => "12",
                            "NIGHT_FROM" => "6",
                            "NIGHT_TO" => "14",
                            "COUNT" => "2",
                            "CACHE_TYPE" => "N",
                            "CACHE_TIME" => "0",
                            "CACHE_NOTES" => ""
                        ),
                        false,
                        Array(
                            'ACTIVE_COMPONENT' => 'Y'
                        )
                    );?>

                    <!-- Описание отеля -->
                    <div class="row hotel-description">
                        <div class="tab-buttons">
                            <ul class="nav">
                                <li class="active"><a href="#service" data-toggle="tab">Услуги отеля</a></li>
                                <li><a href="#description" style="width: 185px;" data-toggle="tab">Подробное описание</a></li>
                            </ul>
                        </div>
                        <div class="tab-section">
                            <div class="col-md-12">
                                <div id="hotel-tab-content" class="tab-content">
                                    <div class="tab-pane active" id="service">
                                        <?if (isset($arResult["ITEM"]["ROOM_COMFORT"])):?>
                                            <div class="col-md-3">
                                                Услуги отеля:
                                                <ul>
                                                    <?foreach ($arResult["ITEM"]["ROOM_COMFORT"] as $value) {?>
                                                        <li>
                                                            <span class="value"><?=$value["Name"]?></span>
                                                        </li>
                                                    <?}?>
                                                </ul>
                                            </div>
                                        <?endif?>
                                        <?if (isset($arResult["ITEM"]["MEAL"])):?>
                                            <div class="col-md-3">
                                                Питание:
                                                <ul>
                                                    <?if (is_array($arResult["ITEM"]["MEAL"][0])):?>
                                                    <?foreach ($arResult["ITEM"]["MEAL"] as $value) {?>
                                                        <li>
                                                            <span class="value"><?=$value["Name"]?></span>
                                                        </li>
                                                    <?}?>
                                                    <?else:?>
                                                        <li>
                                                            <span class="value"><?=$arResult["ITEM"]["MEAL"]["Name"]?></span>
                                                        </li>
                                                    <?endif;?>
                                                </ul>
                                            </div>
                                        <?endif?>
                                        <?if (isset($arResult["ITEM"]["BATH"])):?>
                                            <div class="col-md-3">
                                                Отдых на воде:
                                                <ul>
                                                    <?if (is_array($arResult["ITEM"]["BATH"][0])):?>
                                                        <?foreach ($arResult["ITEM"]["BATH"] as $value) {?>
                                                            <li>
                                                                <span class="value"><?=$value["Name"]?></span>
                                                            </li>
                                                        <?}?>
                                                    <?else:?>
                                                        <li>
                                                            <span class="value"><?=$arResult["ITEM"]["BATH"]["Name"]?></span>
                                                        </li>
                                                    <?endif;?>
                                                </ul>
                                            </div>
                                        <?endif?>
                                        <?if (isset($arResult["ITEM"]["BEACH_TYPE"])):?>
                                            <div class="col-md-3">
                                                Тип пляжа:
                                                <ul>
                                                    <?if (is_array($arResult["ITEM"]["BEACH_TYPE"][0])):?>
                                                        <?foreach ($arResult["ITEM"]["BEACH_TYPE"] as $value) {?>
                                                            <li>
                                                                <span class="value"><?=$value["Name"]?></span>
                                                            </li>
                                                        <?}?>
                                                    <?else:?>
                                                        <li>
                                                            <span class="value"><?=$arResult["ITEM"]["BEACH_TYPE"]["Name"]?></span>
                                                        </li>
                                                    <?endif;?>
                                                </ul>
                                            </div>
                                        <?endif;?>
                                    </div>
                                    <div class="tab-pane" id="description">
                                        <noindex rel="nofollow"><?=$arResult["ITEM"]["Description"]?></noindex>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /Описание отеля -->
            <?endif?>

        </div><!-- /row -->

    </div>
</section><!-- /section -->

<?
//Устанавливаем значение фильтра для отзывов
global $arrFilter;
$arrFilter = Array("PROPERTY_HOTEL_CODE"=>$arResult["ITEM"]["HotelId"], "!PREVIEW_TEXT"=>false);
?>

<script type="text/javascript">


    $(window).load(function(){

        $('.jcarousel').jcarousel({
            // Configuration goes here
        });

    });

    var lon = "<?=$arResult["ITEM"]["Longitude"]?>"
    var lat = "<?=$arResult["ITEM"]["Latitude"]?>"


    function initialize() {
        var myLatlng = new google.maps.LatLng(lat,lon);
        var mapOptions = {
            zoom: 13,
            center: myLatlng,
            scrollwheel: false
        }
        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Hello World!'
        });
    }


    google.maps.event.addDomListener(window, 'load', initialize);

</script>
