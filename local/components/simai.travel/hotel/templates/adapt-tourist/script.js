// Ожидание статуса обработки
function HotelTourStatus (RequestResult, count, num) {
	// Делаем запрос статуса
	$.get("/bitrix/tools/simai.travel_ajax.php", { id: RequestResult, type: "searchStatus" }, function(searchStatus) {

        searchStatus = JSON.parse(searchStatus);

        var status;
        if (searchStatus.status) {
            status = "load";
        }else {
            status = "wait";
        }

		if(status != "load") {
			setTimeout(function() {
				HotelTourStatus(RequestResult, count, num);
			}, 2000);
		}
		else {
			HotelTourData(RequestResult, count, num);
		}
	});
}

// Отображение результата данных
function HotelTourData (RequestResult, count, num, offerId) {
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, template: 'hotel', sale: num, offerId: offerId }, function(data) { //добавлен параметр offerId 09.07.2014
		$('#hotelTour').html(data);
	});
}

// Покупка тура
function HotelTourBuy (RequestResult, count, tour, num) {
	if(!num)num=0;
	$.get("/bitrix/tools/simai.travel_result.php", { id: RequestResult, page: '1', count: count, tourBuy: tour, template: 'hotel',sale: num }, function(data) {
		$('#hotelTour').html(data);
		$.get("/personal/cart/system.php", {ajax: "Y"}, function(data){$('#cartLine').html(data);});
	});
	$.get("/personal/cart/system.php", {ajax: "Y"}, function(data){$('#cartLine').html(data);});
}