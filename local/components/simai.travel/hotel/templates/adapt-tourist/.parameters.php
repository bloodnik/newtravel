<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"SALE_PERCENT" => Array(
		"NAME" => "Процент скидки(на сколько увеличить старую цену)",
		"TYPE" => "string",
		"DEFAULT" => "0",
	),
	"RES_ID" => Array(
		"NAME" => "id результата поиска",
		"TYPE" => "string",
		"DEFAULT" => $_REQUEST["resId"],
	),
	"OFFER_ID" => Array(
		"NAME" => "id предложения поиска",
		"TYPE" => "string",
		"DEFAULT" => $_REQUEST["offerId"],
	),	
);
?>
