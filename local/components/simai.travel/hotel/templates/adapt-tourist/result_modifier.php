<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?

//Получаем id инфоблока страны и курорта на основе id курорта и страны отеля

$countryId = $arResult["ITEM"]["CountryId"];
$resortId = $arResult["ITEM"]["ResortId"];

CModule::IncludeModule("iblock");
$obCountry = CIBlockSection::GetList(array(), array("IBLOCK"=> "travel_hotel", "CODE"=>$countryId), false, array("NAME", "ID"), false);

if ($resCountry = $obCountry->GetNext()) {
    $arResult["COUNTRY_IBLOCK_ID"] = $resCountry["ID"];
}

$obResort = CIBlockSection::GetList(array(), array("IBLOCK"=> "travel_hotel", "CODE"=>$resortId), false, array("NAME", "ID"), false);

if ($resResort = $obResort->GetNext()) {
    $arResult["RESORT_IBLOCK_ID"] = $resResort["ID"];
}
?>