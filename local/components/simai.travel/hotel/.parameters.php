<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// Подключение модуля
CModule::IncludeModule("simai.travel");

// Откуда
$city = CSimaiTravelSletat::GetDepartCities();
$listCity = array("0" => "Нет");
foreach($city as $arr) {
	$listCity[$arr["Id"]] = $arr["Name"];
}

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"HOTEL" => Array(
			"PARENT" => "BASE",
			"NAME" => "Идентификатор отеля",
			"TYPE" => "STRING",
			"DEFAULT" => '={$_REQUEST["CODE"]}',
		),
		"CURRENCY" => Array(
			"PARENT" => "BASE",
			"NAME" => "Валюта",
			"TYPE" => "STRING",
			"DEFAULT" => 'RUB',
		),
		"CITY" => Array(
			"PARENT" => "BASE",
			"NAME" => "Город вылета",
			"TYPE" => "LIST",
			"VALUES" => $listCity,
			"REFRESH" => "Y",
		),
		"DAY_FROM" => Array(
			"PARENT" => "BASE",
			"NAME" => "Дней до вылета (c)",
			"TYPE" => "STRING",
			"DEFAULT" => "1",
		),
		"DAY_TO" => Array(
			"PARENT" => "BASE",
			"NAME" => "Дней до вылета (по)",
			"TYPE" => "STRING",
			"DEFAULT" => "3",
		),
		"NIGHT_FROM" => Array(
			"PARENT" => "BASE",
			"NAME" => "Ночей (от)",
			"TYPE" => "STRING",
			"DEFAULT" => "7",
		),
		"NIGHT_TO" => Array(
			"PARENT" => "BASE",
			"NAME" => "Ночей (до)",
			"TYPE" => "STRING",
			"DEFAULT" => "7",
		),
		"COUNT" => Array(
			"PARENT" => "BASE",
			"NAME" => "Туров в отель",
			"TYPE" => "STRING",
			"DEFAULT" => "5",
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
        "SET_TITLE" => array(
            "PARENT" => "BASE",
            "NAME" => "Устанавливать заголовок",
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
	),
);
?>