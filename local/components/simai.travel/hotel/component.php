<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $APPLICATION;

// Преобразовываем дату в нужный вид
$arParams["DAY_FROM"] = date("d.m.Y", time()+60*60*24*$arParams["DAY_FROM"]);
$arParams["DAY_TO"] = date("d.m.Y", time()+60*60*24*$arParams["DAY_TO"]);


$arResult = array();

// Кеширование
//$obCache = new CPageCache;
//if($obCache->StartDataCache($arParams["CACHE_TIME"], serialize($arParams),  "/")):

	// Подключение модуля
	CModule::IncludeModule("simai.travel");
	CModule::IncludeModule("iblock");


    // Определяем инфоблок отелей
    $res = CIBlock::GetList(Array(), Array('TYPE'=>'simai', 'CODE'=>'travel_hotel'), true);
    if($ar_res = $res->Fetch()) {
        $iblock_hotel = $ar_res['ID'];
    }


	// Информация о отеле
	$arFilter = Array("IBLOCK_ID"=>$iblock_hotel, "CODE"=>$arParams["HOTEL"], "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array());

	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arProps = $ob->GetProperties();
		$arFields["PROP"] = $arProps;
		$arResult["HOTEL"] = $arFields;

		$arResult["ITEM"] = CSimaiTravelSletat::GetHotelInformation($arResult["HOTEL"]["CODE"]);		


		foreach ($arResult["ITEM"]["HotelFacilities"]["HotelInfoFacilityGroup"] as $key => $value) {
			if($value["Name"] == "Пляжная линия") {
				$beachLine = $value["Facilities"]["HotelInfoFacility"]["Name"];
			}
			if($value["Id"] == 8) { //Удобства в номерах
				$arResult["ITEM"]["ROOM_COMFORT"] = $value["Facilities"]["HotelInfoFacility"];
			}
			if($value["Id"] == 10) { //ПИТАНИЕ
				$arResult["ITEM"]["MEAL"] = $value["Facilities"]["HotelInfoFacility"];
			}
			if($value["Id"] == 14) { //Отдых на воде
				$arResult["ITEM"]["BATH"] = $value["Facilities"]["HotelInfoFacility"];
			}
			if($value["Id"] == 20) { //Тип пляжа
				$arResult["ITEM"]["BEACH_TYPE"] = $value["Facilities"]["HotelInfoFacility"];
			}

		}	

		$arResult["ITEM"]["BEACH_LINE"] = $beachLine;
	}
	else {
		echo '<font color="#bb3333">Отель не найден</font>';
		return false;
	}


    $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($iblock_hotel ,$arFields["ID"]);
    $arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();

    if ($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != "")
        $APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]);
    else
        $APPLICATION->SetTitle($arResult["ITEM"]["Name"]);

    $APPLICATION->SetPageProperty("title", $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]);
    $APPLICATION->SetPageProperty("keywords", $arResult["IPROPERTY_VALUES"]["ELEMENT_META_KEYWORDS"]);
    $APPLICATION->SetPageProperty("description", $arResult["IPROPERTY_VALUES"]["ELEMENT_META_DESCRIPTION"]);

	// Список туров в отель
	$params = array(
		'requestId'=>0,
		'pageSize'=>30,
		'pageNumber'=>1,
		"countryId"=>$arResult["HOTEL"]["PROP"]["CountryId"]["VALUE"],
		"cityFromId"=>$arParams["CITY"],
		"cities"=>array($arResult["HOTEL"]["PROP"]["ResortId"]["VALUE"]),
		"meals"=> array(),
		"stars"=> array(),
		"hotels"=> array($arParams["HOTEL"]),
		"adults"=>2,
		"kids"=>0,
		"kidsAges"=>array(),
		"nightsMin" => $arParams["NIGHT_FROM"],
		"nightsMax" => $arParams["NIGHT_TO"],
		"priceMin" => "",
		"priceMax" => "",
		"currencyAlias" => $arParams["CURRENCY"],
		"departFrom" => $arParams["DAY_FROM"],
		"departTo" => $arParams["DAY_TO"],
		'hotelIsNotInStop'=>true,
		'hasTickets'=>true,
		'ticketsIncluded'=>true,
		'updateResult'=>true,
		'useFilter'=>false,
		'f_to_id'=>array(),
		'useTree'=>false,
		'groupBy'=>false,
		'includeDescriptions'=>true
	);
	if($arParams['OFFER_ID'] > 0) {
		$arResult["TOUR"] = CSimaiTravelSletat::CreateRequest($params);
	}

    $arButtons = CIBlock::GetPanelButtons(
        $arResult["HOTEL"]["IBLOCK_ID"],
        $arResult["HOTEL"]["ID"],
        0,
        array("SECTION_BUTTONS"=>false, "SESSID"=>false)
    );
    $arResult["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
    $arResult["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];


    $this->IncludeComponentTemplate();

    // записываем предварительно буферизированный вывод в файл кеша
    //$obCache->EndDataCache();

//endif;
?>