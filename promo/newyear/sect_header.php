<header class="header d-flex flex-column pb-1" style="background: url('<?=SITE_TEMPLATE_PATH?>/assets/i/bg/newyear.jpg') no-repeat center; background-size: cover; position: relative; overflow: hidden;">
	<img src="/promo/newyear/img/sneg.png" style="position: absolute;top: 0;left: 0;right: 0;margin: 0 auto;opacity: .3;">
	<img src="/promo/newyear/img/newyearball.png" style="position: absolute;height: 100px;top: 18%;left: 63%;right: 0;z-index: 0;">
	<img src="/promo/newyear/img/newyearball_red.png" style="position: absolute;height: 100px;top: 22%;right: 62%;">
	<div class="header-search-container order-2 order-lg-1">
		<div class="header-search-container__logo-line pt-3 pt-lg-0" style="background: none; border-bottom: 1px solid #ffffff38; margin: 0 15px;">
			<div class="container">
				<div class="row row align-items-center">
					<div class="col-6 col-sm-6 col-md-4"><a href="/"><img class="img-fluid" src="<?=SITE_TEMPLATE_PATH?>/assets/i/logo.png" alt=""></a></div>
					<div class="col-6 col-sm-6 col-md-4 text-right order-md-3 text-sm-center"><a href="#" data-toggle="modal" data-target="#callbackModal" class="btn btn-danger">Заказать звонок</a></div>
					<div class="col-12 col-sm-12 col-md-4 order-md-2 text-center display-6 pb-2 text-white"><a class="text-white" href="tel:+73472163061">+7(347) 216-30-71</a></div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="header-search-container__search-line row pt-3 mt-5 mt-md-2">
				<div class=" col-12 mb-3 d-flex flex-column text-center">
					<div class="h3 text-uppercase text-dark-blue">до нового года осталось</div>
					<div class="mt-5">
						<div class="d-flex justify-content-center" id="to_new_yaer_time"></div>
					</div>
				</div>
				<script type="text/javascript">
                    var date = new Date(2017, 12, 1);
                    var now = new Date();
                    var diff = (date.getTime() / 1000) - (now.getTime() / 1000);

                    var clock = $('#to_new_yaer_time').FlipClock(diff, {
                        clockFace: 'DailyCounter',
                        //showSeconds: false,
                        countdown: true,
                        language: 'ru'
                    });
				</script>

				<div class="header-search-container__title col-12 text-center my-3">
					<h2 class="display-3 text-white" style="font-weight: 400;">Выбери направление прямо сейчас</h2>
				</div>
			</div>

			<? $APPLICATION->IncludeComponent(
				"newtravel.search:simple.search.v2",
				"",
				Array(
					"ADULTS"               => "2",
					"CACHE_TIME"           => "36000000",
					"CACHE_TYPE"           => "A",
					"CITY"                 => "4",
					"COUNTRY"              => "4",
					"DATE_FROM"            => "25.12.2017",
					"DATE_TO"              => "01.01.2018",
					"NIGHTSFROM"           => "6",
					"NIGHTSTO"             => "10",
					"USE_COOKIE_DEPARTURE" => "Y",
					"USE_GEO_DEFINE"       => "N"
				)
			); ?>

		</div>

		<div style="background: rgba(0,0,0,0.5)">
			<div class="container pt-3">
				<div class="header-search-container__search-line row mb-3">
					<div class="col-12 text-center">
						<span class="h2 text-white text-uppercase">Оформи заявку</span>
					</div>
				</div>

				<div class="row pb-3">
					<div class="col text-center">
						<a href="#" class="btn btn-orange btn-lg" data-toggle="modal" data-target="#pickMeTourModal">Оставить заявку</a>
					</div>
				</div>

				<div class="row text-white text-center pb-3">
					<div class="col-12 text-center">
						<span class="h3 text-white">Готовься к новогоднему путешествию</span>
					</div>
				</div>

				<div class="row text-white text-center pb-3 mt-3">
					<div class="col-12"><i class="fa fa-chevron-down fa-2x"></i></div>
				</div>
			</div>
		</div>

	</div>
</header>