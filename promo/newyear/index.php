<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Туры на Новый год 2018");


define("SHOW_FOOTER", false);
?>

	<style>
		.flip-clock-wrapper {
			margin: 0;
		}

		.flip-clock-divider .flip-clock-label {
			color: #0a4468;
			font-size: 16px;
			z-index: 1;
			font-weight: bold;
		}

		.flip-clock-wrapper ul li a div div.inn {
			background-color: #fafafa;
			color: #353535;
		}

		@media (max-width: 576px) {
			.flip-clock-wrapper {
				zoom: 55%;
			}

			.flip-clock-divider .flip-clock-label {
				font-size: 26px;
			}
		}

	</style>


<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW"      => "sect",
		"AREA_FILE_SUFFIX"    => "countries_ufa",
		"EDIT_TEMPLATE"       => ""
	)
); ?>


<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW"      => "sect",
		"AREA_FILE_SUFFIX"    => "countries_moscow",
		"EDIT_TEMPLATE"       => ""
	)
); ?>


<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW"      => "sect",
		"AREA_FILE_SUFFIX"    => "quotes",
		"EDIT_TEMPLATE"       => ""
	)
); ?>

<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW"      => "sect",
		"AREA_FILE_SUFFIX"    => "bottom_line",
		"EDIT_TEMPLATE"       => ""
	)
); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>