<section class="py-5 bg-white">
	<div class="container">
		<div class="col-12 mx-auto text-center">
			<h2 class=" mb-4 text-uppercase">Туры из Москвы</h2>
		</div>


		<? $APPLICATION->IncludeComponent(
			"bitrix:news.list",
			"specialoffers_list",
			Array(
				"ACTIVE_DATE_FORMAT"              => "d.m.Y",
				"ADD_SECTIONS_CHAIN"              => "N",
				"AJAX_MODE"                       => "N",
				"AJAX_OPTION_ADDITIONAL"          => "",
				"AJAX_OPTION_HISTORY"             => "N",
				"AJAX_OPTION_JUMP"                => "N",
				"AJAX_OPTION_STYLE"               => "Y",
				"CACHE_FILTER"                    => "N",
				"CACHE_GROUPS"                    => "Y",
				"CACHE_TIME"                      => "86400",
				"CACHE_TYPE"                      => "A",
				"CHECK_DATES"                     => "Y",
				"COMPONENT_TEMPLATE"              => "specialoffers_list",
				"DETAIL_URL"                      => "",
				"DISPLAY_BOTTOM_PAGER"            => "N",
				"DISPLAY_DATE"                    => "Y",
				"DISPLAY_NAME"                    => "Y",
				"DISPLAY_PICTURE"                 => "Y",
				"DISPLAY_PREVIEW_TEXT"            => "Y",
				"DISPLAY_TOP_PAGER"               => "N",
				"FIELD_CODE"                      => array(),
				"FILTER_NAME"                     => "",
				"HIDE_LINK_WHEN_NO_DETAIL"        => "N",
				"IBLOCK_ID"                       => "91",
				"IBLOCK_TYPE"                     => "specialoffers",
				"INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
				"INCLUDE_SUBSECTIONS"             => "Y",
				"MESSAGE_404"                     => "",
				"NEWS_COUNT"                      => "8",
				"PAGER_BASE_LINK_ENABLE"          => "N",
				"PAGER_DESC_NUMBERING"            => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL"                  => "N",
				"PAGER_SHOW_ALWAYS"               => "N",
				"PAGER_TEMPLATE"                  => ".default",
				"PAGER_TITLE"                     => "",
				"PARENT_SECTION"                  => "4831",
				"PARENT_SECTION_CODE"             => "",
				"PREVIEW_TRUNCATE_LEN"            => "",
				"PROPERTY_CODE"                   => array(0 => "URL", 1 => "PRICE"),
				"SET_BROWSER_TITLE"               => "N",
				"SET_LAST_MODIFIED"               => "N",
				"SET_META_DESCRIPTION"            => "N",
				"SET_META_KEYWORDS"               => "N",
				"SET_STATUS_404"                  => "N",
				"SET_TITLE"                       => "N",
				"SHOW_404"                        => "N",
				"SORT_BY1"                        => "ACTIVE_FROM",
				"SORT_BY2"                        => "SORT",
				"SORT_ORDER1"                     => "DESC",
				"SORT_ORDER2"                     => "ASC",
				"TITLE_DESCRIPTION"               => "",
				"XL_ITEMS_COUNT"                  => '4'
			)
		); ?>
	</div>
</section>