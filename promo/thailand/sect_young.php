<div class="container mt-3 mb-3 bg-light" id="young">
	<div class="col-12 mx-auto text-center">
		<h2 class="display-5 mb-4">ТОП лучших туров для молодежи</h2>
	</div>

	<div class="col-12 mx-auto text-center ">
		<h2 class="display-6 mb-4">Отели для молодежи в Паттайе рядом с «Walking Street»</h2>
	</div>
	<div class="row">
		<? $APPLICATION->IncludeComponent(
			"newtravel.search:tours.offers",
			"",
			Array(
				"ADULTS"        => "2",
				"CACHE_TIME"    => "3600",
				"CACHE_TYPE"    => "A",
				"CHILD"         => "0",
				"COUNTRY_ID"    => "2",
				"DATE_FROM"     => "",
				"DATE_TO"       => "29.12.2017",
				"DEPARTURE_ID"  => "4",
				"HOTELS_ID"     => "",
				"IBLOCK_ID"     => "",
				"IBLOCK_TYPE"   => "content",
				"ITEM_STYLE"    => "card",
				"ONPAGE"        => "1",
				"PRICE_FROM"    => "",
				"PRICE_PER_ONE" => "N",
				"PRICE_TO"      => "",
				"CARD_LG_COUNT" => "4",
				"SUBREGIONS"    => "69",
				"STARS"         => "2",
				"STARSBETTER"   => "0",
				"SINGLE_ITEM"   => "Y",
				"OPERATORS"     => "12,13,16",
			)
		); ?>
		<? $APPLICATION->IncludeComponent(
			"newtravel.search:tours.offers",
			"",
			Array(
				"ADULTS"        => "2",
				"CACHE_TIME"    => "3600",
				"CACHE_TYPE"    => "A",
				"CHILD"         => "0",
				"CHILDAGE1"     => "",
				"COUNTRY_ID"    => "2",
				"DATE_FROM"     => "",
				"DATE_TO"       => "29.12.2017",
				"DEPARTURE_ID"  => "4",
				"IBLOCK_ID"     => "",
				"IBLOCK_TYPE"   => "content",
				"ITEM_STYLE"    => "card",
				"ONPAGE"        => "1",
				"PRICE_FROM"    => "",
				"PRICE_PER_ONE" => "N",
				"PRICE_TO"      => "",
				"CARD_LG_COUNT" => "4",
				"SUBREGIONS"    => "69",
				"STARS"         => "3",
				"STARSBETTER"   => "0",
				"SINGLE_ITEM"   => "Y",
				"OPERATORS"     => "12,13,16",
			)
		); ?>
		<? $APPLICATION->IncludeComponent(
			"newtravel.search:tours.offers",
			"",
			Array(
				"ADULTS"        => "2",
				"CACHE_TIME"    => "3600",
				"CACHE_TYPE"    => "A",
				"CHILD"         => "0",
				"COUNTRY_ID"    => "2",
				"DATE_FROM"     => "",
				"DATE_TO"       => "29.12.2017",
				"DEPARTURE_ID"  => "4",
				"HOTELS_ID"     => "",
				"IBLOCK_ID"     => "",
				"IBLOCK_TYPE"   => "content",
				"ITEM_STYLE"    => "card",
				"ONPAGE"        => "1",
				"PRICE_FROM"    => "",
				"PRICE_PER_ONE" => "N",
				"PRICE_TO"      => "",
				"CARD_LG_COUNT" => "4",
				"SUBREGIONS"    => "69",
				"STARS"         => "4",
				"STARSBETTER"   => "0",
				"SINGLE_ITEM"   => "Y",
				"OPERATORS"     => "12,13,16",
			)
		); ?>
	</div>
	<div class="col-12 mx-auto text-center">
		<h2 class="display-6 mb-4">Отели для молодежи на Пхукете, Патонг рядом с «Bangla Road»</h2>
	</div>
	<div class="row">
		<? $APPLICATION->IncludeComponent(
			"newtravel.search:tours.offers",
			"",
			Array(
				"ADULTS"        => "2",
				"CACHE_TIME"    => "3600",
				"CACHE_TYPE"    => "A",
				"CHILD"         => "0",
				"CHILDAGE1"     => "",
				"CHILDAGE2"     => "",
				"CHILDAGE3"     => "",
				"COUNTRY_ID"    => "2",
				"DATE_FROM"     => "",
				"DATE_TO"       => "29.12.2017",
				"DEPARTURE_ID"  => "4",
				"HOTELS_ID"     => "",
				"IBLOCK_ID"     => "",
				"IBLOCK_TYPE"   => "content",
				"ITEM_STYLE"    => "card",
				"ONPAGE"        => "1",
				"PRICE_FROM"    => "",
				"PRICE_PER_ONE" => "N",
				"PRICE_TO"      => "",
				"CARD_LG_COUNT" => "4",
				"SUBREGIONS"    => "75",
				"STARS"         => "2",
				"STARSBETTER"   => "0",
				"SINGLE_ITEM"   => "Y",
				"OPERATORS"     => "12,13,16",
			)
		); ?>
		<? $APPLICATION->IncludeComponent(
			"newtravel.search:tours.offers",
			"",
			Array(
				"ADULTS"        => "2",
				"CACHE_TIME"    => "3600",
				"CACHE_TYPE"    => "A",
				"CHILD"         => "0",
				"CHILDAGE1"     => "",
				"CHILDAGE2"     => "",
				"CHILDAGE3"     => "",
				"COUNTRY_ID"    => "2",
				"DATE_FROM"     => "",
				"DATE_TO"       => "29.12.2017",
				"DEPARTURE_ID"  => "4",
				"HOTELS_ID"     => "",
				"IBLOCK_ID"     => "",
				"IBLOCK_TYPE"   => "content",
				"ITEM_STYLE"    => "card",
				"ONPAGE"        => "1",
				"PRICE_FROM"    => "",
				"PRICE_PER_ONE" => "N",
				"PRICE_TO"      => "",
				"CARD_LG_COUNT" => "4",
				"SUBREGIONS"    => "75",
				"STARS"         => "3",
				"STARSBETTER"   => "0",
				"SINGLE_ITEM"   => "Y",
				"OPERATORS"     => "12,13,16",
			)
		); ?>
		<? $APPLICATION->IncludeComponent(
			"newtravel.search:tours.offers",
			"",
			Array(
				"ADULTS"        => "2",
				"CACHE_TIME"    => "3600",
				"CACHE_TYPE"    => "A",
				"CHILD"         => "0",
				"CHILDAGE1"     => "",
				"CHILDAGE2"     => "",
				"CHILDAGE3"     => "",
				"COUNTRY_ID"    => "2",
				"DATE_FROM"     => "",
				"DATE_TO"       => "29.12.2017",
				"DEPARTURE_ID"  => "4",
				"HOTELS_ID"     => "",
				"IBLOCK_ID"     => "",
				"IBLOCK_TYPE"   => "content",
				"ITEM_STYLE"    => "card",
				"ONPAGE"        => "1",
				"PRICE_FROM"    => "",
				"PRICE_PER_ONE" => "N",
				"PRICE_TO"      => "",
				"CARD_LG_COUNT" => "4",
				"SUBREGIONS"    => "75",
				"STARS"         => "4",
				"STARSBETTER"   => "0",
				"SINGLE_ITEM"   => "Y",
				"OPERATORS"     => "12,13,16",
			)
		); ?>
	</div>

	<div class="text-center">
		<a class="btn btn-lg btn-orange mt-2 mb-3" data-toggle="modal" data-target="#pickMeTourModal" href="javascript:void(0)">Заявка на другие варианты</a>
	</div>
</div>
<br>