<section class="bg-light pb-5 pt-5" id="blog">
	<div class="container">
		<h2 class="display-5 mb-4 text-center">Общая информация</h2>
		<div class="row d-flex align-content-center">
			<div class="col-12 col-md-6">
				<ul class="list-unstyled">
					<li class="media mb-5">
						<img class="mr-3 align-self-center" src="<?=SITE_TEMPLATE_PATH?>/assets/i/icons/airplane.svg" alt="" width="100px">
						<div class="media-body align-self-center">
							<h5 class="mt-0 mb-1">Прямые рейсы Уфа-Бангкок и Уфа-Пхукет</h5>
						</div>
					</li>
					<li class="media mb-5">
						<img class="mr-3 align-self-center" src="<?=SITE_TEMPLATE_PATH?>/assets/i/icons/around_earth.svg" alt="" width="100px">
						<div class="media-body align-self-center">
							<h5 class="mt-0 mb-1">Время в полете составляет около 8 часов</h5>
						</div>
					</li>
				</ul>
			</div>
			<div class="col-12 col-md-6">
				<ul class="list-unstyled">
					<li class="media mb-5">
						<img class="mr-3 align-self-center" src="<?=SITE_TEMPLATE_PATH?>/assets/i/icons/compass.svg" alt="" width="100px">
						<div class="media-body align-self-center">
							<h5 class="mt-0 mb-1">Разница во времени с г. Уфа +2 часа</h5>
						</div>
					</li>
					<li class="media mb-5">
						<img class="mr-3 align-self-center" src="<?=SITE_TEMPLATE_PATH?>/assets/i/icons/passport.svg" alt="" width="100px">
						<div class="media-body align-self-center">
							<h5 class="mt-0 mb-1">Виза не требуется (сроком пребывания до 30 дней)</h5>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>