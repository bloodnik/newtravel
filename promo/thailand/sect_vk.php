<section class="bg-orange pb-5 pt-5">
	<div class="container">
		<div class="col-12 mx-auto text-center text-white">
			<h2 class="display-5 mb-4">Будь ближе к нам в VK</h2>
			<h2 class="display-8 mb-4">Подпишись на нашу страницу в ВК и участвуй в ежемесячных розыгрышах призов.</h2>

			<script type="text/javascript" src="//vk.com/js/api/openapi.js?150"></script>

			<!-- VK Widget -->
			<div id="vk_groups" style="width: 100%;"></div>
			<script type="text/javascript">
                VK.Widgets.Group("vk_groups", {mode: 4, no_cover: 1, width: 'auto', height: "400", color3: '37CAF3'}, 7500134);
			</script>
		</div>

	</div>
</section>