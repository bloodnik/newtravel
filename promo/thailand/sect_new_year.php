<div class="bg-primary" id="new_year">
	<div class="container pt-5 pb-5">
		<div class="col-12 mx-auto text-center text-white">
			<h2 class="mb-4 text-uppercase">Встречай Новый 2018 год в Таиланде</h2>
			<h2 class="display-6 mb-4">Успей забронировать отель, пока есть места.</h2>
		</div>

		<div class="col-12 mx-auto text-center text-white">
			<h2 class="display-6 mb-4">Паттайя</h2>
		</div>
		<div class="row">
			<? $APPLICATION->IncludeComponent(
				"newtravel.search:tours.offers",
				"",
				Array(
					"ADULTS"        => "2",
					"CACHE_TIME"    => "3600",
					"CACHE_TYPE"    => "A",
					"CHILD"         => "0",
					"COUNTRY_ID"    => "2",
					"DATE_FROM"     => "24.12.2017",
					"DATE_TO"       => "29.12.2017",
					"DEPARTURE_ID"  => "4",
					"HOTELS_ID"     => "",
					"IBLOCK_ID"     => "",
					"IBLOCK_TYPE"   => "content",
					"ITEM_STYLE"    => "card",
					"ONPAGE"        => "1",
					"PRICE_FROM"    => "",
					"PRICE_PER_ONE" => "N",
					"PRICE_TO"      => "",
					"CARD_LG_COUNT" => "4",
					"REGIONS"       => "7",
					"STARS"         => "3",
					"SINGLE_ITEM"   => "Y",
					"OPERATORS"     => "12, 13, 16",
				)
			); ?>
			<? $APPLICATION->IncludeComponent(
				"newtravel.search:tours.offers",
				"",
				Array(
					"ADULTS"        => "2",
					"CACHE_TIME"    => "3600",
					"CACHE_TYPE"    => "A",
					"CHILD"         => "0",
					"CHILDAGE1"     => "",
					"COUNTRY_ID"    => "2",
					"DATE_FROM"     => "24.12.2017",
					"DATE_TO"       => "29.12.2017",
					"DEPARTURE_ID"  => "4",
					"IBLOCK_ID"     => "",
					"IBLOCK_TYPE"   => "content",
					"ITEM_STYLE"    => "card",
					"ONPAGE"        => "1",
					"PRICE_FROM"    => "",
					"PRICE_PER_ONE" => "N",
					"PRICE_TO"      => "",
					"CARD_LG_COUNT" => "4",
					"REGIONS"       => "7",
					"STARS"         => "4",
					"SINGLE_ITEM"   => "Y",
					"OPERATORS"     => "12,13,16",
				)
			); ?>
			<? $APPLICATION->IncludeComponent(
				"newtravel.search:tours.offers",
				"",
				Array(
					"ADULTS"        => "2",
					"CACHE_TIME"    => "3600",
					"CACHE_TYPE"    => "A",
					"CHILD"         => "0",
					"COUNTRY_ID"    => "2",
					"DATE_FROM"     => "24.12.2017",
					"DATE_TO"       => "29.12.2017",
					"DEPARTURE_ID"  => "4",
					"HOTELS_ID"     => "",
					"IBLOCK_ID"     => "",
					"IBLOCK_TYPE"   => "content",
					"ITEM_STYLE"    => "card",
					"ONPAGE"        => "1",
					"PRICE_FROM"    => "",
					"PRICE_PER_ONE" => "N",
					"PRICE_TO"      => "",
					"CARD_LG_COUNT" => "4",
					"REGIONS"       => "7",
					"STARS"         => "5",
					"SINGLE_ITEM"   => "Y",
					"OPERATORS"     => "12,13,16",
				)
			); ?>
		</div>
		<div class="col-12 mx-auto text-center text-white">
			<h2 class="display-6 mb-4">Пхукет</h2>
		</div>
		<div class="row">
			<? $APPLICATION->IncludeComponent(
				"newtravel.search:tours.offers",
				"",
				Array(
					"ADULTS"        => "2",
					"CACHE_TIME"    => "3600",
					"CACHE_TYPE"    => "A",
					"CHILD"         => "0",
					"CHILDAGE1"     => "",
					"CHILDAGE2"     => "",
					"CHILDAGE3"     => "",
					"COUNTRY_ID"    => "2",
					"DATE_FROM"     => "25.12.2017",
					"DATE_TO"       => "29.12.2017",
					"DEPARTURE_ID"  => "4",
					"HOTELS_ID"     => "",
					"IBLOCK_ID"     => "",
					"IBLOCK_TYPE"   => "content",
					"ITEM_STYLE"    => "card",
					"ONPAGE"        => "1",
					"PRICE_FROM"    => "",
					"PRICE_PER_ONE" => "N",
					"PRICE_TO"      => "",
					"CARD_LG_COUNT" => "4",
					"REGIONS"       => "8",
					"STARS"         => "3",
					"SINGLE_ITEM"   => "Y",
					"OPERATORS"     => "12,13,16",
				)
			); ?>
			<? $APPLICATION->IncludeComponent(
				"newtravel.search:tours.offers",
				"",
				Array(
					"ADULTS"        => "2",
					"CACHE_TIME"    => "3600",
					"CACHE_TYPE"    => "A",
					"CHILD"         => "0",
					"CHILDAGE1"     => "",
					"CHILDAGE2"     => "",
					"CHILDAGE3"     => "",
					"COUNTRY_ID"    => "2",
					"DATE_FROM"     => "25.12.2017",
					"DATE_TO"       => "29.12.2017",
					"DEPARTURE_ID"  => "4",
					"HOTELS_ID"     => "",
					"IBLOCK_ID"     => "",
					"IBLOCK_TYPE"   => "content",
					"ITEM_STYLE"    => "card",
					"ONPAGE"        => "1",
					"PRICE_FROM"    => "",
					"PRICE_PER_ONE" => "N",
					"PRICE_TO"      => "",
					"CARD_LG_COUNT" => "4",
					"REGIONS"       => "8",
					"STARS"         => "4",
					"SINGLE_ITEM"   => "Y",
					"OPERATORS"     => "12,13,16",
				)
			); ?>
			<? $APPLICATION->IncludeComponent(
				"newtravel.search:tours.offers",
				"",
				Array(
					"ADULTS"        => "2",
					"CACHE_TIME"    => "3600",
					"CACHE_TYPE"    => "A",
					"CHILD"         => "0",
					"CHILDAGE1"     => "",
					"CHILDAGE2"     => "",
					"CHILDAGE3"     => "",
					"COUNTRY_ID"    => "2",
					"DATE_FROM"     => "25.12.2017",
					"DATE_TO"       => "29.12.2017",
					"DEPARTURE_ID"  => "4",
					"HOTELS_ID"     => "",
					"IBLOCK_ID"     => "",
					"IBLOCK_TYPE"   => "content",
					"ITEM_STYLE"    => "card",
					"ONPAGE"        => "1",
					"PRICE_FROM"    => "",
					"PRICE_PER_ONE" => "N",
					"PRICE_TO"      => "",
					"CARD_LG_COUNT" => "4",
					"REGIONS"       => "8",
					"STARS"         => "5",
					"SINGLE_ITEM"   => "Y",
					"OPERATORS"     => "12,13,16",
				)
			); ?>
		</div>

		<div class="text-center text-white">
			*новогодние ужины оплачиваются дополнительно.
		</div>
	</div>
</div>
