<div class="container" id="hot">
	<div class="col-12 mx-auto text-center">
		<h2 class=" mb-4 text-uppercase">Горящие туры в Таиланд из Уфы.</h2>
		<h2 class="display-6 mb-4">Успей купить по выгодной цене последние места!</h2>
	</div>
	<div class="row w">
		<? $APPLICATION->IncludeComponent(
			"newtravel.search:tours.offers",
			"",
			Array(
				"ADULTS"        => "2",
				"CACHE_TIME"    => "3602",
				"CACHE_TYPE"    => "A",
				"CHILD"         => "0",
				"CHILDAGE1"     => "",
				"CHILDAGE2"     => "",
				"CHILDAGE3"     => "",
				"COUNTRY_ID"    => "2",
				"DATE_FROM"     => "",
				"DATE_TO"       => "",
				"DEPARTURE_ID"  => "4",
				"HOTELS_ID"     => "",
				"IBLOCK_TYPE"   => "content",
				"ITEM_STYLE"    => "card",
				"ONPAGE"        => "1",
				"PRICE_FROM"    => "",
				"PRICE_PER_ONE" => "N",
				"PRICE_TO"      => "",
				"CARD_LG_COUNT" => "4",
				"REGIONS"       => "7",
				"STARS"         => "2",
				"SINGLE_ITEM"   => "Y",
				"OPERATORS"     => "12,13,16",
			)
		); ?>
		<? $APPLICATION->IncludeComponent(
			"newtravel.search:tours.offers",
			"",
			Array(
				"ADULTS"        => "2",
				"CACHE_TIME"    => "3600",
				"CACHE_TYPE"    => "A",
				"CHILD"         => "0",
				"CHILDAGE1"     => "",
				"CHILDAGE2"     => "",
				"CHILDAGE3"     => "",
				"COUNTRY_ID"    => "2",
				"DATE_FROM"     => "",
				"DATE_TO"       => "",
				"DEPARTURE_ID"  => "4",
				"HOTELS_ID"     => "",
				"IBLOCK_TYPE"   => "content",
				"ITEM_STYLE"    => "card",
				"ONPAGE"        => "1",
				"PRICE_FROM"    => "",
				"PRICE_PER_ONE" => "N",
				"PRICE_TO"      => "",
				"CARD_LG_COUNT" => "4",
				"REGIONS"       => "8",
				"STARS"         => "2",
				"SINGLE_ITEM"   => "Y",
				"OPERATORS"     => "12,13,16",
			)
		); ?>
		<? $APPLICATION->IncludeComponent(
			"newtravel.search:tours.offers",
			"",
			Array(
				"ADULTS"        => "2",
				"CACHE_TIME"    => "3600",
				"CACHE_TYPE"    => "A",
				"CHILD"         => "0",
				"CHILDAGE1"     => "",
				"CHILDAGE2"     => "",
				"CHILDAGE3"     => "",
				"COUNTRY_ID"    => "2",
				"DATE_FROM"     => "",
				"DATE_TO"       => "",
				"DEPARTURE_ID"  => "4",
				"HOTELS_ID"     => "",
				"IBLOCK_TYPE"   => "content",
				"ITEM_STYLE"    => "card",
				"ONPAGE"        => "1",
				"PRICE_FROM"    => "",
				"PRICE_PER_ONE" => "N",
				"PRICE_TO"      => "",
				"CARD_LG_COUNT" => "4",
				"REGIONS"       => "7,8",
				"STARS"         => "4",
				"SINGLE_ITEM"   => "Y",
				"OPERATORS"     => "12,13,16",
			)
		); ?>

	</div>
	<div class="text-center">
		<a class="btn btn-lg btn-orange mt-2 mb-3" data-toggle="modal" data-target="#hotToursModal" href="javascript:void(0)">Заявка на горящий тур</a>
	</div>
</div>
<br>