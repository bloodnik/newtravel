<div class="container" id="search-block">
	<div class="col-12 mx-auto text-center">
		<h2 class="display-5 mb-4">Все операторы на одном сайте</h2>
	</div>
	<?$APPLICATION->IncludeComponent(
		"newtravel.search:full.search.v2",
		"",
		Array(
			"ADULTS" => "2",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"CITY" => "4",
			"COUNTRY" => "4",
			"DATE_TO" => "10",
			"NIGHTSFROM" => "6",
			"NIGHTSTO" => "10",
			"STARS" => "3",
			"USE_COOKIE_DEPARTURE" => "Y",
			"USE_GEO_DEFINE" => "N"
		)
	);?>
</div>