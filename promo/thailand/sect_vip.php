<div class="container mt-3 mb-3 bg-light" id="vip">
	<div class="col-12 mx-auto text-center">
		<h2 class="display-5 mb-4">VIP отдых</h2>
	</div>
	<? $APPLICATION->IncludeComponent(
		"newtravel.search:tours.offers",
		"",
		Array(
			"ADULTS"        => "2",
			"CACHE_TIME"    => "3600",
			"CACHE_TYPE"    => "A",
			"CHILD"         => "0",
			"CHILDAGE1"     => "",
			"CHILDAGE2"     => "",
			"CHILDAGE3"     => "",
			"COUNTRY_ID"    => "2",
			"DATE_FROM"     => "15.11.2017",
			"DATE_TO"       => "15.12.2017",
			"DEPARTURE_ID"  => "4",
			"HOTELS_ID"     => "547, 4270, 599, 574",
			"IBLOCK_ID"     => "",
			"IBLOCK_TYPE"   => "content",
			"ITEM_STYLE"    => "card",
			"ONPAGE"        => "3",
			"PRICE_FROM"    => "",
			"PRICE_PER_ONE" => "N",
			"PRICE_TO"      => "",
			"CARD_LG_COUNT"      => "4",
			"SINGLE_ITEM" => "N",
			"OPERATORS"     => "12,13,16",
		)
	); ?>
	<div class="text-center">
		<div class="display-7 mb-2">Предлагаем дополнительные услуги:</div>
		<ul class="list-inline">
			<li class="list-inline-item">Перелет бизнес класс</li>
			<li class="list-inline-item">Индивидуальный VIP трансфер</li>
			<li class="list-inline-item">Аренда самолета, яхты и многое другое</li>
		</ul>
	</div>
</div>
<br>