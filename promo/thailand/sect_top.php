<div class="bg-light" id="child">
	<div class="container pt-5 pb-5">
		<div class="col-12 mx-auto text-center">
			<h2 class="mb-4 text-uppercase">Топ отелей для отдыха с детьми с водными горками</h2>
		</div>

		<? $APPLICATION->IncludeComponent(
			"newtravel.search:tours.offers",
			"",
			Array(
				"ADULTS"        => "2",
				"CACHE_TIME"    => "3600",
				"CACHE_TYPE"    => "A",
				"CHILD"         => "0",
				"CHILDAGE1"     => "",
				"CHILDAGE2"     => "",
				"CHILDAGE3"     => "",
				"COUNTRY_ID"    => "2",
				"DATE_FROM"     => "01.11.2017",
				"DATE_TO"       => "30.11.2017",
				"DEPARTURE_ID"  => "4",
				"HOTELS_ID"     => "744, 3036, 752, 601, 597, 599",
				"IBLOCK_ID"     => "",
				"IBLOCK_TYPE"   => "content",
				"ITEM_STYLE"    => "card",
				"ONPAGE"        => "6",
				"PRICE_FROM"    => "",
				"PRICE_PER_ONE" => "N",
				"PRICE_TO"      => "",
				"SINGLE_ITEM" => "N",
				"OPERATORS"     => "12,13,16",
			)
		); ?>
	</div>
</div>
