<section class="pb-5 pt-5 bg-light" id="advantages">
	<div class="container">
		<div class="row">
			<div class="col-12 mx-auto text-center">
				<h2 class="display-5 mb-4">Преимущества отдыха в Таиланде </h2>
			</div>
		</div>

		<div class="row" id="advantage">
			<div class="col-12 col-md-4 mb-2">
				<div class="card border-primary text-white text-center">
					<img class="card-img" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/thailand/6.jpg" alt="Card image">
					<div class="card-img-overlay d-flex">
						<div class="card-text align-self-center">Знаменитый тайский массаж</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-4 mb-2">
				<div class="card border-primary text-white text-center">
					<img class="card-img" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/thailand/1.jpg" alt="Card image">
					<div class="card-img-overlay d-flex">
						<div class="card-text align-self-center">Безвизовый въезд в страну</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-4 mb-2">
				<div class="card border-primary text-white text-center">
					<img class="card-img" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/thailand/3.jpg" alt="Card image">
					<div class="card-img-overlay d-flex">
						<div class="card-text align-self-center">Райские острова и прекрасные пляжи</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-4 mb-2">
				<div class="card border-primary text-white text-center">
					<img class="card-img" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/thailand/4.jpg" alt="Card image">
					<div class="card-img-overlay d-flex">
						<div class="card-text align-self-center">Разнообразная экскурсионная программа</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-4 mb-2">
				<div class="card border-primary text-white text-center">
					<img class="card-img" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/thailand/5.jpg" alt="Card image">
					<div class="card-img-overlay d-flex">
						<div class="card-text align-self-center">Экзотическая природа</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-4 mb-2">
				<div class="card border-primary text-white text-center">
					<img class="card-img" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/thailand/2.jpg" alt="Card image">
					<div class="card-img-overlay d-flex">
						<div class="card-text align-self-center">Невысокие цены и шопинг</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
