<div class="bg-light" id="month">
	<div class="container pt-5 pb-5">
		<div class="col-12 mx-auto text-center">
			<h2 class=" mb-4 text-uppercase">Ранее бронирование!</h2>
			<h2 class="display-6 mb-4">Торопитесь! Цены уже растут!</h2>
		</div>

		<ul class="nav nav-pills nav-fill" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" href="#november" role="tab" data-toggle="tab">Январь</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#december" role="tab" data-toggle="tab">Февраль</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#january" role="tab" data-toggle="tab">Март</a>
			</li>
		</ul>

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active show" id="november">
				<? $APPLICATION->IncludeComponent(
					"newtravel.search:tours.offers",
					"",
					Array(
						"ADULTS"        => "2",
						"CACHE_TIME"    => "3600",
						"CACHE_TYPE"    => "A",
						"CHILD"         => "0",
						"CHILDAGE1"     => "",
						"CHILDAGE2"     => "",
						"CHILDAGE3"     => "",
						"COUNTRY_ID"    => "2",
						"DATE_FROM"     => "01.01.2018",
						"DATE_TO"       => "31.01.2018",
						"DEPARTURE_ID"  => "4",
						"HOTELS_ID"     => "",
						"IBLOCK_ID"     => "",
						"IBLOCK_TYPE"   => "content",
						"ITEM_STYLE"    => "card",
						"ONPAGE"        => "4",
						"PRICE_FROM"    => "",
						"PRICE_PER_ONE" => "N",
						"PRICE_TO"      => "",
						"SINGLE_ITEM" => "N",
						"OPERATORS"     => "12,13,16",
						"CARD_LG_COUNT" => "3",
					)
				); ?>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="december">
				<? $APPLICATION->IncludeComponent(
					"newtravel.search:tours.offers",
					"",
					Array(
						"ADULTS"        => "2",
						"CACHE_TIME"    => "3600",
						"CACHE_TYPE"    => "A",
						"CHILD"         => "0",
						"CHILDAGE1"     => "",
						"CHILDAGE2"     => "",
						"CHILDAGE3"     => "",
						"COUNTRY_ID"    => "2",
						"DATE_FROM"     => "01.02.2018",
						"DATE_TO"       => "28.02.2018",
						"DEPARTURE_ID"  => "4",
						"HOTELS_ID"     => "",
						"IBLOCK_ID"     => "",
						"IBLOCK_TYPE"   => "content",
						"ITEM_STYLE"    => "card",
						"ONPAGE"        => "4",
						"PRICE_FROM"    => "",
						"PRICE_PER_ONE" => "N",
						"PRICE_TO"      => "",
						"SINGLE_ITEM" => "N",
						"OPERATORS"     => "12,13,16",
						"CARD_LG_COUNT" => "3",
					)
				); ?>
			</div>

			<div role="tabpanel" class="tab-pane fade" id="january">
				<? $APPLICATION->IncludeComponent(
					"newtravel.search:tours.offers",
					"",
					Array(
						"ADULTS"        => "2",
						"CACHE_TIME"    => "3600",
						"CACHE_TYPE"    => "A",
						"CHILD"         => "0",
						"CHILDAGE1"     => "",
						"CHILDAGE2"     => "",
						"CHILDAGE3"     => "",
						"COUNTRY_ID"    => "2",
						"DATE_FROM"     => "01.03.2018",
						"DATE_TO"       => "30.03.2018",
						"DEPARTURE_ID"  => "4",
						"HOTELS_ID"     => "",
						"IBLOCK_ID"     => "",
						"IBLOCK_TYPE"   => "content",
						"ITEM_STYLE"    => "card",
						"ONPAGE"        => "4",
						"PRICE_FROM"    => "",
						"PRICE_PER_ONE" => "N",
						"PRICE_TO"      => "",
						"SINGLE_ITEM" => "N",
						"OPERATORS"     => "12,13,16",
						"CARD_LG_COUNT" => "3",
					)
				); ?>
			</div>
		</div>
	</div>
</div>
