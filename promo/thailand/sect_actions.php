<div class="">
	<div class="container pt-5 pb-5">
		<div class="col-12 mx-auto text-center">
			<h2 class=" mb-4 text-uppercase">Акция!!! 4 экскусии со скидкой 100 %</h2>
		</div>

		<p>При бронировании туров с вылетом из любого города РФ в Таиланд на курорты Паттайя, Пхукет, Самуи Вы получаете <span style="font-weight: bold;">экскурсии в подарок:</span></p>


		<ul>
			<li>Городской тур по Паттайе (полдня)</li>
			<li>Городской тур по Пхукету</li>
			<li>Городской тур по Самуи <span class="text-muted">(соответственно вашему отелю проживания)</span></li>
		</ul>


		<div class="col-12 mx-auto text-center">
			<h2 class="display-7 mb-4">Туры с бесплатной экскурсией</h2>
		</div>

		<? $APPLICATION->IncludeComponent(
			"newtravel.search:tours.offers",
			"",
			Array(
				"ADULTS"        => "2",
				"CACHE_TIME"    => "3600",
				"CACHE_TYPE"    => "A",
				"CHILD"         => "0",
				"CHILDAGE1"     => "",
				"CHILDAGE2"     => "",
				"CHILDAGE3"     => "",
				"COUNTRY_ID"    => "2",
				"DATE_FROM"     => "",
				"DATE_TO"       => "",
				"DEPARTURE_ID"  => "4",
				"HOTELS_ID"     => "",
				"IBLOCK_ID"     => "",
				"IBLOCK_TYPE"   => "content",
				"ITEM_STYLE"    => "card",
				"ONPAGE"        => "4",
				"PRICE_FROM"    => "",
				"PRICE_PER_ONE" => "N",
				"PRICE_TO"      => "",
				"CARD_LG_COUNT" => "3",
				"OPERATORS" => "12",
				"SINGLE_ITEM" => "N"
			)
		); ?>
	</div>
</div>
