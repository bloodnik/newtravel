<footer class="bg-dark pt-3 pb-3 text-light">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-12 col-md-4 small">
				Умныетуристы.рф © 2008 - 2017 <span class="text-secondary">Все права защищены</span>
			</div>
			<div class="col-12 col-md-5">
				|</span> ул. пр. Октября, д 52  <span class="text-secondary">|</span>  <a href="/tours/">Поиск туров онлайн</a>
			</div>
			<div class="col-12 col-md-3 text-md-right">
				<ul class="list-inline mb-0">
					<li class="list-inline-item"><a href="https://vk.com/umnie_turisti"><i class="fa fa-vk"></i></a></li>
					<li class="list-inline-item"><a href="https://www.facebook.com/groups/umnyetouristy/"><i class="fa fa-facebook-official"></i></a></li>
					<li class="list-inline-item"><a href="https://www.instagram.com/umnye__turisty/"><i class="fa fa-instagram"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>