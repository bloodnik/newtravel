<header class="header d-flex flex-column pb-5" style="background: url('<?=SITE_TEMPLATE_PATH?>/assets/i/photos/maldives/bg.jpg') no-repeat center; background-size: cover; background-position-y: 0%;">
	<div class="header-search-container order-2 order-lg-1">
		<div class="header-search-container__logo-line pt-3 pt-lg-0">
			<div class="container">
				<div class="row row align-items-center">
					<div class="col-6 col-sm-6 col-md-4"><a href="/"><img class="img-fluid" src="<?=SITE_TEMPLATE_PATH?>/assets/i/logo.png" alt=""></a></div>
					<div class="col-6 col-sm-6 col-md-4 text-right order-md-3 text-sm-center"><a href="#" data-toggle="modal" data-target="#callbackModal" class="btn btn-danger">Заказать звонок</a></div>
					<div class="col-12 col-sm-12 col-md-4 order-md-2 text-center display-6 pb-2 text-white"><a class="text-white" href="tel:+73472163061">8 800 505 47 61</a></div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="header-search-container__search-line row mt-5">
				<div class="header-search-container__title col-12">
					<h1 class="text-uppercase text-white text-center text-md-left">Прямые вылеты из Москвы на Мальдивы!</h1>
				</div>
			</div>


			<h3 class="pt-3 text-white text-center text-md-left">Оставь заявку на подбор тура прямо сейчас и получи 3 лучших варианта через 15 минут</h3>

			<? $APPLICATION->IncludeComponent(
				"bitrix:form.result.new",
				"pick_me_tour",
				Array(
					"CACHE_TIME"             => "3600",
					"CACHE_TYPE"             => "A",
					"CHAIN_ITEM_LINK"        => "",
					"CHAIN_ITEM_TEXT"        => "",
					"EDIT_URL"               => "",
					"IGNORE_CUSTOM_TEMPLATE" => "N",
					"LIST_URL"               => "",
					"SEF_MODE"               => "N",
					"SUCCESS_URL"            => "",
					"USE_EXTENDED_ERRORS"    => "N",
					"VARIABLE_ALIASES"       => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
					"WEB_FORM_ID"            => "4",
					"COUNTRY_NAME"           => defined(PROMO_COUNTRY_NAME) ? PROMO_COUNTRY_NAME : ""
				)
			); ?>

		</div>
	</div>
</header>


<script>
    setTimeout(function () {
        if (!$('#pickMeTourModal').is(':visible')) {
            //$('#pickMeTourModal').modal('show');
        }
    }, 7000)
</script>