<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Вьетнам");

/**
 * ID старны для подстановки в поиск
 */
define("PROMO_COUNTRY_NAME", "Вьетнам");
define("SHOW_FOOTER", false);
?>

<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW"      => "sect",
		"AREA_FILE_SUFFIX"    => "bottom_line",
		"EDIT_TEMPLATE"       => ""
	)
); ?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>