<div class="pt-3 pb-3 bg-light" id="best">
	<div class="container">
		<div class="col-12 mx-auto text-center">
			<h2 class=" mb-4 text-uppercase">Лучшие предложения на ближайшие даты.</h2>
			<h2 class="display-6 mb-4">Надо брать!</h2>
		</div>
		<div class="row">
			<? $APPLICATION->IncludeComponent(
				"newtravel.search:tours.offers",
				"",
				Array(
					"ADULTS"        => "2",
					"CACHE_TIME"    => "3600",
					"CACHE_TYPE"    => "A",
					"CHILD"         => "0",
					"CHILDAGE1"     => "",
					"CHILDAGE2"     => "",
					"CHILDAGE3"     => "",
					"COUNTRY_ID"    => "9",
					"DATE_FROM"     => "",
					"DATE_TO"       => "14.11.2017",
					"DEPARTURE_ID"  => "4",
					"HOTELS_ID"     => "",
					"IBLOCK_ID"     => "",
					"IBLOCK_TYPE"   => "content",
					"ITEM_STYLE"    => "card",
					"ONPAGE"        => "1",
					"PRICE_FROM"    => "",
					"PRICE_PER_ONE" => "N",
					"PRICE_TO"      => "",
					"CARD_LG_COUNT" => "4",
					"RATING"        => "4",
					"STARS"         => "3",
					"SINGLE_ITEM"   => "Y",
					"OPERATORS"     => "12,13,16",
				)
			); ?>
			<? $APPLICATION->IncludeComponent(
				"newtravel.search:tours.offers",
				"",
				Array(
					"ADULTS"        => "2",
					"CACHE_TIME"    => "3600",
					"CACHE_TYPE"    => "A",
					"CHILD"         => "0",
					"CHILDAGE1"     => "",
					"CHILDAGE2"     => "",
					"CHILDAGE3"     => "",
					"COUNTRY_ID"    => "9",
					"DATE_FROM"     => "",
					"DATE_TO"       => "14.11.2017",
					"DEPARTURE_ID"  => "4",
					"HOTELS_ID"     => "",
					"IBLOCK_ID"     => "",
					"IBLOCK_TYPE"   => "content",
					"ITEM_STYLE"    => "card",
					"ONPAGE"        => "1",
					"PRICE_FROM"    => "",
					"PRICE_PER_ONE" => "N",
					"PRICE_TO"      => "",
					"CARD_LG_COUNT" => "4",
					"RATING"        => "4",
					"STARS"         => "4",
					"SINGLE_ITEM"   => "Y",
					"OPERATORS"     => "12,13,16",
				)
			); ?>
			<? $APPLICATION->IncludeComponent(
				"newtravel.search:tours.offers",
				"",
				Array(
					"ADULTS"        => "2",
					"CACHE_TIME"    => "3600",
					"CACHE_TYPE"    => "A",
					"CHILD"         => "0",
					"CHILDAGE1"     => "",
					"CHILDAGE2"     => "",
					"CHILDAGE3"     => "",
					"COUNTRY_ID"    => "9",
					"DATE_FROM"     => "",
					"DATE_TO"       => "14.11.2017",
					"DEPARTURE_ID"  => "4",
					"HOTELS_ID"     => "",
					"IBLOCK_ID"     => "",
					"IBLOCK_TYPE"   => "content",
					"ITEM_STYLE"    => "card",
					"ONPAGE"        => "1",
					"PRICE_FROM"    => "",
					"PRICE_PER_ONE" => "N",
					"PRICE_TO"      => "",
					"CARD_LG_COUNT" => "4",
					"RATING"        => "4",
					"STARS"         => "5",
					"SINGLE_ITEM"   => "Y",
					"OPERATORS"     => "12,13,16",
				)
			); ?>
		</div>
		<div class="text-center">
			<a class="btn btn-lg btn-orange mt-2 mb-3" data-toggle="modal" data-target="#pickMeTourModal" href="javascript:void(0)">Заявка на другие варианты</a>
		</div>
	</div>
</div>
