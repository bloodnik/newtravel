<section class="bg-primary pt-3 pb-2">
	<div class="container">
		<? $APPLICATION->IncludeComponent(
			"bitrix:form.result.new",
			"callback_inline",
			Array(
				"CACHE_TIME"             => "3600",
				"CACHE_TYPE"             => "A",
				"CHAIN_ITEM_LINK"        => "",
				"CHAIN_ITEM_TEXT"        => "",
				"EDIT_URL"               => "",
				"IGNORE_CUSTOM_TEMPLATE" => "N",
				"LIST_URL"               => "",
				"SEF_MODE"               => "N",
				"SUCCESS_URL"            => "",
				"USE_EXTENDED_ERRORS"    => "N",
				"VARIABLE_ALIASES"       => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
				"WEB_FORM_ID"            => "9",
				"COUNTRY_NAME"            => PROMO_COUNTRY_NAME,
				"AJAX_MODE" => "Y",  // режим AJAX
				"AJAX_OPTION_SHADOW" => "N", // затемнять область
				"AJAX_OPTION_JUMP" => "N", // скроллить страницу до компонента
				"AJAX_OPTION_STYLE" => "Y", // подключать стили
				"AJAX_OPTION_HISTORY" => "N",
			)
		); ?>
	</div>
</section>