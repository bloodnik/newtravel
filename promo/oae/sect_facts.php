<section class="bg-light pt-5 pb-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-heading text-center">
					<h2 class="display-5">Давай познакомимся?</h2>
					<p>Дорогой турист! Хотим представить тебе несколько достоверных фактов о нашей компании. <br> Кто такие УмныеТуристы.рф?</p>
				</div>

				<div class="row">
					<div class="col-12 col-sm-6  col-md-6 col-lg-3">
						<div class="skill-block">
							<div class="sk-border">
								<h2>10 лет</h2>
							</div>
							<div class="sk-text">
								<h4>Опыт работы</h4>
								<p>Наша компания на рынке туризма уже более 10 лет, что сделало из нас экспертов в своей области</p>
							</div>
						</div>
					</div>

					<div class="col-12 col-sm-6 col-md-6 col-lg-3">
						<div class="skill-block">
							<div class="sk-border">
								<h2>5</h2>
							</div>
							<div class="sk-text">
								<h4>Экспертов</h4>
								<p>В нашем штате работают 5 профессионалов своего дела, средний опыт в области туризма 7 лет</p>
							</div>
						</div>
					</div>

					<div class="col-12 col-sm-6  col-md-6 col-lg-3">
						<div class="skill-block">
							<div class="sk-border">
								<h2>4000</h2>
							</div>

							<div class="sk-text">
								<h4>Довольных клиентов </h4>
								<p>За все время мы уже осчастливили более 4000 клиентов, многие из них уже много лет с нами</p>
							</div>
						</div>
					</div>

					<div class="col-12 col-sm-6  col-md-6 col-lg-3">
						<div class="skill-block">
							<div class="sk-border">
								<h2>350</h2>
							</div>

							<div class="sk-text">
								<h4>Отзывов</h4>
								<p>На нашем сайте, клиенты оставили более 350 <a href="/reviews/" target="_blank">отзывов</a>, о том как и где они отдыхали с нашей помощью</p>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>