<section class="pb-5 pt-5 bg-light" id="about">
	<div class="container">
		<div class="row">
			<div class="col-12 mx-auto text-center">
				<h2 class="display-5 mb-4">Преимущества отдыха в ОАЭ </h2>
			</div>
		</div>

		<div class="row" id="advantage">
			<div class="col-12 col-md-3 mb-2">
				<div class="card border-primary text-white text-center">
					<img class="card-img" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/oae/2.jpg" alt="Card image">
					<div class="card-img-overlay d-flex">
						<div class="card-text align-self-center">
							Прямой вылет из Уфы. <br>
							6 рейсов в неделю <br>
							3 авиакомпании:<br>
							(Fly Dubay, Azur Air, АК Россия)
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-3 mb-2">
				<div class="card border-primary text-white text-center">
					<img class="card-img" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/oae/3.jpg" alt="Card image">
					<div class="card-img-overlay d-flex">
						<div class="card-text align-self-center">Самая безопасная страна для отдыха</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-3 mb-2">
				<div class="card border-primary text-white text-center">
					<img class="card-img" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/oae/4.jpg" alt="Card image">
					<div class="card-img-overlay d-flex">
						<div class="card-text align-self-center">Виза не нужна</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-3 mb-2">
				<div class="card border-primary text-white text-center">
					<img class="card-img" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/oae/5.jpg" alt="Card image">
					<div class="card-img-overlay d-flex">
						<div class="card-text align-self-center">Теплое море и хорошие пляжи</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-3 mb-2">
				<div class="card border-primary text-white text-center">
					<img class="card-img" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/oae/6.jpg" alt="Card image">
					<div class="card-img-overlay d-flex">
						<div class="card-text align-self-center">
							Короткий перелет. <br>
							Всего 5 часов. <br>
							Удобно для поездки с детьми.
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-3 mb-2">
				<div class="card border-primary text-white text-center">
					<img class="card-img" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/oae/7.jpg" alt="Card image">
					<div class="card-img-overlay d-flex">
						<div class="card-text align-self-center">Большой выбор развлечений для детей и взрослых.</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-3 mb-2">
				<div class="card border-primary text-white text-center">
					<img class="card-img" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/oae/8.jpg" alt="Card image">
					<div class="card-img-overlay d-flex">
						<div class="card-text align-self-center">Широкий выбор туров от 3-30 дней</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-3 mb-2">
				<div class="card border-primary text-white text-center">
					<img class="card-img" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/oae/9.jpg" alt="Card image">
					<div class="card-img-overlay d-flex">
						<div class="card-text align-self-center">Выгодный шопинг</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
