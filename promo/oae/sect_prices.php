<section class="bg-light  pt-3 pb-3" id="prices">
	<div class="container">
		<div class="row">
			<div class="col-12 mx-auto text-center">
				<h2 class="display-5 mb-4">Минимальные цены в ОАЭ</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-12 col-md-6 col-lg-4">
				<div class="card mb-3">
					<img class="card-img-top" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/oae/ArabianPark3.jpg" alt="Arabian Park, 3*">
					<div class="card-body">
						<h4 class="card-title">Arabian Park, 3*</h4>
						<p class="card-text text-success display-6">
							от 29 000 Р/Чел
						</p>
						<p class="card-text">Удобно расположен в центре Дубая, рядом метро. Большой открытый бассейн и бесплатный Wi-Fi, 4 ресторана и бара, терраса для загара и тренажерный зал, бесплатный трансфер на пляж и торговый центр Dubai
							Mall.</p>
						<p class="card-text">
							<small class="text-muted">Последнее обновление 30.09.2017</small>
						</p>
					</div>
					<div class="card-footer">
						<a class='btn btn-primary btn-block' target="_blank" href="/tours/?start=Y&type=search&departure=4&country=9&datefrom=01.11.2017&dateto=11.11.2017&nightsfrom=6&nightsto=14&stars[0]=3&hotels[0]=2516">Забронировать сейчас</a>
						<a class='btn btn-outline-success btn-block' href="javascript:void(0)" data-toggle="modal" data-target="#callbackModal">Заказать в 1 клик</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-4">
				<div class="card mb-3">
					<img class="card-img-top" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/oae/CopthorneHotelSharjah4.jpg" alt="Copthorne Hotel Sharjah, 4*">
					<div class="card-body">
						<h4 class="card-title">Copthorne Hotel Sharjah, 4*</h4>
						<p class="card-text text-success display-6">
							от 30 000 Р/чел
						</p>
						<p class="card-text">Отель в стиле Хай-тек, удобно расположен в центре Шарджи, 5 мин. от моря, номера с видом на лагуну. Бесплатный Wi-Fi, 2 ресторана, СПА центр. В 5 минутах ходьбы от отеля находятся Музеи Шарджи, аквариум
							Шарджи, амфитеатр Аль-Махаз, аквапарк Аль-Джазира.</p>
						<p class="card-text">
							<small class="text-muted">На 6 ночей, Завтрак + Ужин. Вылет 01/11</small>
							<small class="text-muted">Последнее обновление 30.09.2017</small>
						</p>
					</div>
					<div class="card-footer">
						<a class='btn btn-primary btn-block' target="_blank" href="/tours/?start=Y&type=search&departure=4&country=9&datefrom=01.11.2017&dateto=14.11.2017&nightsfrom=6&nightsto=14&stars[0]=3&hotels[0]=22943">Забронировать сейчас</a>
						<a class='btn btn-outline-success btn-block' href="javascript:void(0)" data-toggle="modal" data-target="#callbackModal">Заказать в 1 клик</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-4">
				<div class="card mb-3">
					<img class="card-img-top" src="<?=SITE_TEMPLATE_PATH?>/assets/i/photos/oae/GhayaGrandHotel5.jpg" alt="Card image cap">
					<div class="card-body">
						<h4 class="card-tite">Ghaya Grand Hotel, 5*</h4>
						<p class="card-text text-success display-6">
							от 40 000 Р/чел
						</p>
						<p class="card-text">Новый, современный отель-высотка расположен в центре Дубай и в 10 минутах от района Джумейра Бич и торгового центра Mall of the Emirates. Шикарные номера и вышколенный сервис. В отеле 5 ресторанов,
							бесплатный Wi-Fi, круглосуточный лобби-лаундж, бар у бассейна и кальянная. В гранд-отеле Ghaya можно воспользоваться теннисным кортом, тренажерным залом, детской игровой площадкой и спа-салоном.</p>
						<p class="card-text">
							<small class="text-muted">На 6 ночей, Завтрак. Вылет 01/11</small>
							<small class="text-muted">Последнее обновление 30.09.2017</small>
						</p>
					</div>
					<div class="card-footer">
						<a class='btn btn-primary btn-block' target="_blank" href="/tours/?start=Y&type=search&departure=4&country=9&datefrom=01.11.2017&dateto=14.11.2017&nightsfrom=6&nightsto=14&stars[0]=3&hotels[0]=35135">Забронировать сейчас</a>
						<a class='btn btn-outline-success btn-block' href="javascript:void(0)" data-toggle="modal" data-target="#callbackModal">Заказать в 1 клик</a>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>