<header class="header d-flex flex-column" style="background: url('<?=SITE_TEMPLATE_PATH?>/assets/i/bg/thai-bg.jpg') no-repeat center; background-size: cover">
	<div class="header-search-container order-2 order-lg-1">
		<div class="header-search-container__logo-line pt-3 pt-lg-0">
			<div class="container">
				<div class="row row align-items-center">
					<div class="col-6 col-sm-6 col-md-4"><a href="/"><img class="img-fluid" src="<?=SITE_TEMPLATE_PATH?>/assets/i/logo.png" alt=""></a></div>
					<div class="col-6 col-sm-6 col-md-4 text-right order-md-3 text-sm-center"><a href="#" data-toggle="modal" data-target="#callbackModal" class="btn btn-danger">Заказать звонок</a></div>
					<div class="col-12 col-sm-12 col-md-4 order-md-2 text-center display-5 pb-2 text-white">+7(347) 216-30-71</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="header-search-container__search-line row pb-5 mt-5">
				<div class="header-search-container__title col-12 text-center">
					<h1 class="text-white text-uppercase">Поиск туров</h1>
				</div>
			</div>
		</div>
	</div>
	<div class="top-line order-1 order-lg-2">
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
			<div class="container d-flex d-lg-block flex-column">
				<div class="align-self-end">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
				</div>

				<div class="align-self-start collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav d-flex justify-content-between" style="width: 100%;">
						<li class="nav-item active">
							<a class="nav-link text-orange" href="/promotions/">АКЦИИ</a>
						</li>
						<li class="nav-item active">
							<a class="nav-link" href="/blog/">Блог</a>
						</li>
						<li class="nav-item active">
							<a class="nav-link" href="/faq/how_to_pay/">Способы оплаты</a>
						</li>
						<li class="nav-item active">
							<a class="nav-link" href="/reviews/">Отзывы</a>
						</li>
						<li class="nav-item active">
							<a class="nav-link" href="/faq/">Вопросы-ответы</a>
						</li>
						<li class="nav-item active">
							<a class="nav-link" href="/about/contacts.php">Контакты</a>
						</li>

					</ul>
				</div>
			</div>
		</nav>
	</div>
</header>