<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/mobileapp/public/.mobile_menu.php");

$arMobileMenuItems = array(
	array(
		"type" => "section",
		"text" => GetMessage("MOBILE_MENU_HEADER"),
		"sort" => "100",
		"items" => array(
			array(
				"text" => 'Спецпредложения',
				"data-url" => SITE_DIR . "newtravel_app/index.php",
				"class" => "menu-item",
				"id" => "main",

			),
			array(
				"text" => 'Наш блог',
				"data-url" => SITE_DIR . "newtravel_app/blog/blog.php",
				"class" => "menu-item",
				"id" => "blog",
			),
			array(
				"text" => 'Страны',
				"data-url" => SITE_DIR . "newtravel_app/countries/index.php",
				"class" => "menu-item",
				"id" => "countries",
			)
			
		)
	)
);
?>