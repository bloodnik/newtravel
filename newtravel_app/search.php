<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>
	<br>

<? $APPLICATION->IncludeComponent(
	"newtravel.mobile:search.main",
	"",
	Array(
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A"
	)
); ?>


	<script>
		//Добавляем кнопку в Тоp Bar
		var params = {
			callback: function () {
				NewtravelMobileApp.search.openFilter();
			},
			type: "filter"
		};
		BXMPage.TopBar.addRightButton(params);
	</script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>