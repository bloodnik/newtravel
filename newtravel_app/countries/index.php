<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>
<? $APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"",
	Array(
		"ADD_SECTIONS_CHAIN"  => "Y",
		"CACHE_GROUPS"        => "Y",
		"CACHE_TIME"          => "36000000",
		"CACHE_TYPE"          => "A",
		"COUNT_ELEMENTS"      => "N",
		"EL_IN_COL"           => "4",
		"IBLOCK_ID"           => "58",
		"IBLOCK_TYPE"         => "simai",
		"SECTION_CODE"        => "",
		"SECTION_FIELDS"      => array("", ""),
		"SECTION_ID"          => $_REQUEST["SECTION_ID"],
		"SECTION_URL"         => "/newtravel_app/countries/detail.php?SECTION_ID=#SECTION_ID#",
		"SECTION_USER_FIELDS" => array("UF_FLIGHT_HOURS", "UF_CURRENCY", ""),
		"SHOW_PARENT_NAME"    => "Y",
		"TOP_DEPTH"           => "1",
		"VIEW_MODE"           => "LINE"
	)
); ?>

	<script>
		BXMobileApp.UI.Page.TopBar.title.setText('Описания стран');
		BXMobileApp.UI.Page.TopBar.title.setDetailText('');
	</script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>