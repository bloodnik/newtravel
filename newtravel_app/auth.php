<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");?>

<? $APPLICATION->IncludeComponent(
	"bitrix:system.auth.form",
	"newtravel",
	Array(
		"COMPONENT_TEMPLATE"   => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"FORGOT_PASSWORD_URL"  => "",
		"PATH_TO_MYPORTAL"     => "desktop.php",
		"PROFILE_URL"          => "",
		"REGISTER_URL"         => "",
		"SHOW_ERRORS"          => "Y"
	)
); ?>

	<script>
		BXMobileApp.UI.Page.TopBar.title.setText('Авторизация');
		BXMobileApp.UI.Page.TopBar.title.setDetailText('');
	</script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>