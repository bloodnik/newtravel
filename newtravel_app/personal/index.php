<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>


	<div class="page-content">
		Вы попали в персональный раздел
	</div>

<? if ($USER->IsAuthorized()): ?>
	<? $APPLICATION->IncludeComponent(
		"bitrix:sale.personal.order.list",
		"",
		Array(
			"ACTIVE_DATE_FORMAT"            => "d.m.Y",
			"CACHE_GROUPS"                  => "Y",
			"CACHE_TIME"                    => "3600",
			"CACHE_TYPE"                    => "A",
			"HISTORIC_STATUSES"             => array("F"),
			"ID"                            => $ID,
			"NAV_TEMPLATE"                  => "",
			"ORDERS_PER_PAGE"               => "20",
			"PATH_TO_BASKET"                => "",
			"PATH_TO_CANCEL"                => "",
			"PATH_TO_COPY"                  => "",
			"PATH_TO_DETAIL"                => "/newtravel_app/personal/detail.php",
			"PATH_TO_PAYMENT"               => "payment.php",
			"SAVE_IN_SESSION"               => "Y",
			"SET_TITLE"                     => "Y",
			"STATUS_COLOR_AD"               => "gray",
			"STATUS_COLOR_B"                => "gray",
			"STATUS_COLOR_BS"               => "gray",
			"STATUS_COLOR_DR"               => "gray",
			"STATUS_COLOR_F"                => "gray",
			"STATUS_COLOR_GO"               => "gray",
			"STATUS_COLOR_N"                => "green",
			"STATUS_COLOR_P"                => "yellow",
			"STATUS_COLOR_PD"               => "gray",
			"STATUS_COLOR_PSEUDO_CANCELLED" => "red",
			"STATUS_COLOR_VD"               => "gray"
		)
	); ?>
<? endif; ?>

	<script>
		BXMobileApp.UI.Page.TopBar.title.setText('Персональный раздел');
		BXMobileApp.UI.Page.TopBar.title.setDetailText('');
	</script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>