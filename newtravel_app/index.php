<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>


<? $APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"specialoffers_list",
	array(
		"ACTIVE_DATE_FORMAT"              => "d.m.Y",
		"ADD_SECTIONS_CHAIN"              => "N",
		"AJAX_MODE"                       => "N",
		"AJAX_OPTION_ADDITIONAL"          => "",
		"AJAX_OPTION_HISTORY"             => "N",
		"AJAX_OPTION_JUMP"                => "N",
		"AJAX_OPTION_STYLE"               => "Y",
		"CACHE_FILTER"                    => "N",
		"CACHE_GROUPS"                    => "Y",
		"CACHE_TIME"                      => "86400",
		"CACHE_TYPE"                      => "A",
		"CHECK_DATES"                     => "Y",
		"DETAIL_URL"                      => "",
		"DISPLAY_BOTTOM_PAGER"            => "N",
		"DISPLAY_DATE"                    => "Y",
		"DISPLAY_NAME"                    => "Y",
		"DISPLAY_PICTURE"                 => "Y",
		"DISPLAY_PREVIEW_TEXT"            => "Y",
		"DISPLAY_TOP_PAGER"               => "N",
		"FIELD_CODE"                      => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME"                     => "",
		"HIDE_LINK_WHEN_NO_DETAIL"        => "N",
		"IBLOCK_ID"                       => "84",
		"IBLOCK_TYPE"                     => "specialoffers",
		"INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
		"INCLUDE_SUBSECTIONS"             => "Y",
		"MESSAGE_404"                     => "",
		"NEWS_COUNT"                      => "6",
		"PAGER_BASE_LINK_ENABLE"          => "N",
		"PAGER_DESC_NUMBERING"            => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL"                  => "N",
		"PAGER_SHOW_ALWAYS"               => "N",
		"PAGER_TEMPLATE"                  => ".default",
		"PAGER_TITLE"                     => "Хиты продаж. Куда полететь?",
		"PARENT_SECTION"                  => "",
		"PARENT_SECTION_CODE"             => "",
		"PREVIEW_TRUNCATE_LEN"            => "",
		"PROPERTY_CODE"                   => array(
			0 => "DATE",
			1 => "NIGHTS",
			2 => "URL",
			3 => "PRICE",
			4 => "",
		),
		"SET_BROWSER_TITLE"               => "N",
		"SET_LAST_MODIFIED"               => "N",
		"SET_META_DESCRIPTION"            => "N",
		"SET_META_KEYWORDS"               => "N",
		"SET_STATUS_404"                  => "N",
		"SET_TITLE"                       => "N",
		"SHOW_404"                        => "N",
		"SORT_BY1"                        => "ACTIVE_FROM",
		"SORT_BY2"                        => "SORT",
		"SORT_ORDER1"                     => "DESC",
		"SORT_ORDER2"                     => "ASC",
		"TITLE_DESCRIPTION"               => "Подборка отелей с высоким рейтингом",
		"COMPONENT_TEMPLATE"              => "specialoffers_list"
	),
	false
); ?>


	<script>
		//Добавляем кнопку в Тоp Bar
		var params = {
			callback: function () {
				NewtravelMobileApp.search.openFilter();
			},
			type: "filter"
		};
		BXMPage.TopBar.addRightButton(params);

		BXMobileApp.UI.Page.TopBar.title.setText('Умныетуристы.рф');
		BXMobileApp.UI.Page.TopBar.title.setDetailText('Поиск и бронирование туров');
	</script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>