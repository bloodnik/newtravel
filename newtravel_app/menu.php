<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>
	<script type="text/javascript">
		app.enableSliderMenu(true);
	</script>

	<div class="side-logo">
		<img src="<?=SITE_TEMPLATE_PATH?>/images/logo-big.png" alt="">
		<div class="auth-wrap">
			<? if (!$USER->IsAuthorized()): ?>
				<a href="#" onclick="NewtravelMobileApp.goToThePage('/newtravel_app/auth.php')">Войти</a>
			<? else: ?>
				<a href="#" onclick="NewtravelMobileApp.goToThePage('/newtravel_app/personal/')"><?=$USER->GetFirstName() . " " .$USER->GetLastName()?></a>
			<? endif ?>
		</div>
	</div>

<?
$APPLICATION->IncludeComponent(
	'bitrix:mobileapp.menu',
	'mobile',
	array("MENU_FILE_PATH" => "/newtravel_app/.mobile_menu.php"),
	false,
	Array('HIDE_ICONS' => 'Y'));
?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>