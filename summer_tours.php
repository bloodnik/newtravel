<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Туры на лето в Турцию, Грецию, Кипр, Болгарию");
$APPLICATION->SetPageProperty("keywords", "туры в Турцию, туры в Грецию, туры на Кипр");
$APPLICATION->SetPageProperty("description", "Туры на Лето на самые популярные пляжи Турции, Греции, Кипра, Болгарии");
use Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/plugins/gmaps.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/plugins/jquery.backstretch.min.js");
Asset::getInstance()->addJs("https://maps.googleapis.com/maps/api/js?key=AIzaSyAKYVzO5rPM1Gqq8crmt9Bku0uvKhr7t4g&sensor=true");
?>
	<style>
		.simple-search-section {
			background: url(/upload/lending_img/summer_bg.jpg) 0px -395px;
			/*background-size: cover;*/
		}

		.latest-works li .title-caption {
			position: absolute;
			bottom: 0;
			left: 4px;
			top: 8px;
			height: 60px;
		}

		.latest-works li .title-caption span:last-child {
			float: left;
		}

		.price {
			font-size: 28px;
			color: #00AEEF;
			line-height: 1.3;
			padding: 20px;
			margin-left: 37px;
		}

		.old-price {
			font-size: 24px;
			color: #898990;
			line-height: 1.3;
			text-decoration: line-through;
			padding: 20px;
		}

		.description {
			text-align: justify;
			padding: 10px;
		}

		.description a {
			position: relative;
			display: block;
		}

		.description .btn {
			position: absolute;
			top: 10px;
			right: 0;
			margin-bottom: 10px;
		}

		.menu-links a {
			font-size: 20px;
		}

		.table-striped > tbody > tr:nth-of-type(odd) {
			background-color: #f1f1f1;
		}

		.table-striped > tbody > tr:nth-child(odd) > td, .table-striped > tbody > tr:nth-child(odd) > th {
			background-color: #f1f1f1;
		}

		.offers-item {
			height: auto;
		}

		.latest-works.curorts li {
			height: 425px;
		}

	</style>

	<!-- БАННЕР --->
	<section id="" class="gray-section simple-search-section">
		<div class="text-center text-uppercase">
			<div class="col-md-12">
				<h1 class="index-title">Туры на лето в Турцию, Грецию, Кипр, Болгарию</h1>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<? $APPLICATION->IncludeComponent(
						"newtravel.search:simple.search",
						".default",
						array(
							"CITY"                 => "4",
							"COUNTRY"              => "6",
							"NIGHTSFROM"           => "6",
							"NIGHTSTO"             => "14",
							"ADULTS"               => "2",
							"STARS"                => "1",
							"ACTION"               => "",
							"NEW"                  => "N",
							"COUNT"                => "10",
							"CACHE_TYPE"           => "A",
							"CACHE_TIME"           => "3600",
							"CACHE_NOTES"          => "",
							"DATE_TO"              => "60",
							"COMPONENT_TEMPLATE"   => ".default",
							"USE_GEO_DEFINE"       => "N",
							"COMPOSITE_FRAME_MODE" => "Y",
							"COMPOSITE_FRAME_TYPE" => "DYNAMIC_WITH_STUB_LOADING"
						),
						false
					); ?>
				</div>
				<div class="col-md-6 hidden-xs">
					<? $APPLICATION->IncludeComponent(
						"bitrix:main.include",
						".default",
						array(
							"AREA_FILE_SHOW"     => "file",
							"AREA_FILE_SUFFIX"   => "inc_why_we",
							"EDIT_TEMPLATE"      => "",
							"COMPONENT_TEMPLATE" => ".default",
							"PATH"               => "/index_inc_why_we.php"
						),
						false
					); ?>
				</div>
			</div>
		</div>
	</section>
	<!-- БАННЕР --->

	<br>
	<div class="container hidden-xs">
		<div class="row">
			<div class="col-md-12">
				<div class="menu-links">
					<a href="#greece">Халкидики, Салоники(Греция, материковая часть)</a>
					<a href="#greece_island">Родос, Крит (Греция, острова)</a>
					<a href="#kipr">Кипр</a>
					<a href="#turkey">Турция</a>
					<a href="#bolgaria">Болгария</a>
					<a href="#spain">Испания</a>
				</div>
			</div>
		</div>
	</div>

	<hr>

	<div class="container">
		<div class="row">
			<div class="attention-box text-center" style="background-color: #aeedff;font-weight: bold; font-size: 1.3em;line-height: 1.6;">
				<p>Какие планы на лето 2017? В самом разгаре раннее бронирование по самым выгодным ценам. Умные Туристы предлагают Вам отдохнуть по-настоящему! Мы подготовили для Вас отличную подборку вариантов отдыха с прямым вылетом
					из Уфы.</p>
			</div>
		</div>
	</div>

	<!-- Греция --->
	<section class="white-section" id="greece">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="flexslider project-flexslider">
						<div class="flex-viewport" style="overflow: hidden; position: relative;">
							<ul class="slides" style="width: 1200%; margin-left: -545px;">
								<li class="clone" aria-hidden="true" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/greece_land/1.jpg" title="Фото Греция" class="fancybox">
										<img src="/upload/lending_img/greece_land/1.jpg" alt="Фото Греция" draggable="false"/>
									</a>
								</li>
								<li class="flex-active-slide" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/greece_land/2.jpg" title="Фото Греция" class="fancybox">
										<img src="/upload/lending_img/greece_land/2.jpg" alt="Фото Греция" draggable="false"/>
									</a>
								</li>
								<li class="" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/greece_land/3.jpg" title="Фото Греция" class="fancybox">
										<img src="/upload/lending_img/greece_land/3.jpg" alt="Фото Греция" draggable="false"/>
									</a>
								</li>
							</ul>
						</div>

						<ul class="flex-direction-nav">
							<li><a class="flex-prev" href="#">Назад</a></li>
							<li><a class="flex-next" href="#">Вперед</a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-6">
					<div class="project-OverView">
						<div class="short-section-title">
							<h3>Халкидики, Салоники, Пиерия (Греция, материковая часть)</h3>
						</div>

						<p>«Пришло время упомянуть Колыбель Цивилизации», – именно с этих слов начинается описание Греции во всех информационных источниках. Действительно, экскурсионная программа этого курорта очень обширна. Вам не хватит и недели,
							чтобы объехать все интересные места, начиная с Афин и заканчивая мужским монастырем на Афоне.</p>
						<p>Но не думайте, будто всё, что Вы будете делать - это в бешеном темпе бегать по достопримечательностям; ведь на материке этой чудесной страны есть много других приятных вещей, которыми совершенно необходимо успеть
							насладиться. Например, прекрасные пляжи с мелким белым песком, где вода нагревается уже в середине мая. А ещё Халкидики – самая зелёная часть страны с чистейшим воздухом – здесь очень много сосен.</p>
						<p>Стоит отметить, что перелёт к Вашему месту отдыха будет осуществляться на большом самолете, где коленки не упираются в спинку кресла, а во время обеда на борту кроме закусок предлагают попробовать настоящее греческое
							вино!</p>
						<table class="table table-striped">
							<thead>
							<tr>
								<th class="text-success">Плюсы</th>
								<th class="text-danger">Минусы</th>
							</tr>
							</thead>
							<tr>
								<td>+ Экскурсии! Там не просто руины древних городов, это выглядит впечатляюще!</td>
								<td>- Отели дороже, чем на островах</td>
							</tr>
							<tr>
								<td>+ Мягкий песок на пляжах</td>
								<td></td>
							</tr>
							<tr>
								<td>+ Воздух и море нагреваются раньше, чем на островах</td>
								<td></td>
							</tr>
							<tr>
								<td>+ Комфортный перелёт</td>
								<td></td>
							</tr>
							<tr>
								<td>+ Греческая кухня в местных тавернах</td>
								<td></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Греция --->

	<!-- Греция ТУРЫ --->
	<section class="gray-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="short-section-title">
						<h3>Туры в Грецию</h3>
					</div>
				</div>
				<div class="col-md-12">
					<table class="table table-striped">
						<thead>
						<tr>
							<th>Отель</th>
							<th>Май</th>
							<th>Июнь-Июль</th>
							<th>Август</th>
						</tr>
						</thead>
						<tr>
							<td><a href="/hotel/6687/" target="_blank">ROMANZA MARE 3*</a></td>
							<td><a href="/hotel/6687/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">82 016</a></td>
							<td><a href="/hotel/6687/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">91 445</a></td>
							<td><a href="/hotel/6687/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">97 385</a></td>
						</tr>
						<tr>
							<td><a href="/hotel/6621/" target="_blank">OLYMPIC PALACE 4*</a></td>
							<td><a href="/hotel/6621/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">105 065</a></td>
							<td><a href="/hotel/6621/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">119 025</a></td>
							<td><a href="/hotel/6621/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">139 864</a></td>
						</tr>
						<tr>
							<td><a href="/hotel/6519/" target="_blank">DESSOLE LIPPIA RESORT4*</a></td>
							<td><a href="/hotel/6519/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">115 061</a></td>
							<td><a href="/hotel/6519/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">128 785</a></td>
							<td><a href="/hotel/6519/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">151 622</a></td>
						</tr>
					</table>
					Цены указаны на размещение 2+1 (5 лет), актуальны на 23.02.2017.
				</div>
			</div>
		</div>
	</section>
	<!-- Греция ТУРЫ --->

	<!-- Греция острова --->
	<section class="white-section" id="greece_island">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="flexslider project-flexslider">
						<div class="flex-viewport" style="overflow: hidden; position: relative;">
							<ul class="slides" style="width: 1200%; margin-left: -545px;">
								<li class="clone" aria-hidden="true" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/greece_island/1.jpg" title="Фото Греция" class="fancybox">
										<img src="/upload/lending_img/greece_island/1.jpg" alt="Фото Греция" draggable="false"/>
									</a>
								</li>
								<li class="flex-active-slide" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/greece_island/2.jpg" title="Фото Греция" class="fancybox">
										<img src="/upload/lending_img/greece_island/2.jpg" alt="Фото Греция" draggable="false"/>
									</a>
								</li>
								<li class="" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/greece_island/3.jpg" title="Фото Греция" class="fancybox">
										<img src="/upload/lending_img/greece_island/3.jpg" alt="Фото Греция" draggable="false"/>
									</a>
								</li>
							</ul>
						</div>

						<ul class="flex-direction-nav">
							<li><a class="flex-prev" href="#">Назад</a></li>

							<li><a class="flex-next" href="#">Вперед</a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-6">
					<div class="project-OverView">
						<div class="short-section-title">
							<h3>Родос, Крит (Греция, острова)</h3>
						</div>

						<p>Может, полететь в Грецию? Там мягкий климат и потрясающая природа! А шенгенскую визу получить проще и дешевле, чем в другие страны Евросоюза, тем более что в последнее время греческое консульство более лояльно к россиянам и
							визы выдаются сразу на 3-5 лет. На обоих островах есть чем заняться. Например, сходить с детьми в аквапарк, или погулять по старому городу. Ах, эта атмосфера вечернего ужина в греческом ресторане под Сиртаки с бокалом
							настоящего красного вина!</p>
						<p>Кстати говоря, цены на продукты, сувениры, одежду и аксессуары достаточно демократичны, поэтому не стоит отказывать себе в удовольствии заняться шопингом.</p>
						<table class="table table-striped">
							<thead>
							<tr>
								<th class="text-success">Плюсы</th>
								<th class="text-danger">Минусы</th>
							</tr>
							</thead>
							<tr>
								<td>+ Вы получаете возможность путешествовать по всей Европе в течение 3-х лет без головоломки с визой</td>
								<td>- Проблематично будет туристам, не говорящим по-английски</td>
							</tr>
							<tr>
								<td>+ Греки очень милые :)</td>
								<td>- Для молодежи островная Греция может показаться скучной</td>
							</tr>
							<tr>
								<td>+ Множество отелей работает по системе «всё включено» с детским питанием</td>
								<td></td>
							</tr>
							<tr>
								<td>+ На острове Родос вы сможете искупаться сразу в двух морях: эгейском и средиземном</td>
								<td></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Греция острова--->

	<!-- Греция ТУРЫ --->
	<section class="gray-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="short-section-title">
						<h3>Туры в Грецию</h3>
					</div>
				</div>
				<div class="col-md-12">
					<table class="table table-striped">
						<thead>
						<tr>
							<th>Отель</th>
							<th>Май</th>
							<th>Июнь-Июль</th>
							<th>Август</th>
						</tr>
						</thead>
						<tr>
							<td><a href="/hotel/3616/" target="_blank">AQUA MARE BOMO CLUB 3*</a></td>
							<td><a href="/hotel/3616/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">121 390</a></td>
							<td><a href="/hotel/3616/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">121 036</a></td>
							<td><a href="/hotel/3616/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">127 658</a></td>
						</tr>
						<tr>
							<td><a href="/hotel/3902/" target="_blank">OLYMPIC KOSMA HOTEL 3*</a></td>
							<td><a href="/hotel/3902/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">124 215</a></td>
							<td><a href="/hotel/3902/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">125 295</a></td>
							<td><a href="/hotel/3902/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">133 700</a></td>
						</tr>
						<tr>
							<td><a href="/hotel/3639/" target="_blank">ATHOS PALACE HOTEL 4*</a></td>
							<td><a href="/hotel/3639/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">127 247</a></td>
							<td><a href="/hotel/3639/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">130 086</a></td>
							<td><a href="/hotel/3639/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">136 712</a></td>
						</tr>
						<tr>
							<td><a href="/hotel/3783/" target="_blank">ISTION CLUB & SPA 5*</a></td>
							<td><a href="/hotel/3783/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">128 016</a></td>
							<td></td>
							<td><a href="/hotel/3783/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">161 440</a></td>
						</tr>
						<tr>
							<td><a href="/hotel/3596/" target="_blank">ALEXANDROS PALACE HOTEL & SUITES 5*</a></td>
							<td><a href="/hotel/3596/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">133 459</a></td>
							<td><a href="/hotel/3596/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">122 455</a></td>
							<td><a href="/hotel/3596/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">134 997</a></td>
						</tr>
						<tr>
							<td><a href="/hotel/15987/" target="_blank">ASSA MARIS BOMO CLUB 3*</a></td>
							<td><a href="/hotel/15987/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">144 048</a></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><a href="/hotel/16234/" target="_blank">RAHONI CRONWELL PARK 5*</a></td>
							<td><a href="/hotel/16234/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">145 290</a></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><a href="/hotel/5308/" target="_blank">CRONWELL PLATAMON RESORT 5*</a></td>
							<td><a href="/hotel/5308/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">147 183</a></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><a href="/hotel/3581/" target="_blank">AEGEAN MELATHRON HOTEL 5*</a></td>
							<td><a href="/hotel/3581 /?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">178 951</a></td>
							<td></td>
							<td></td>
						</tr>
					</table>
					Цены указаны на размещение 2+1 (5 лет), актуальны на 23.02.2017.
				</div>
			</div>
		</div>
	</section>
	<!-- Греция ТУРЫ --->

	<!-- Кипр --->
	<section class="white-section" id="kipr">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="flexslider project-flexslider">
						<div class="flex-viewport" style="overflow: hidden; position: relative;">
							<ul class="slides" style="width: 1200%; margin-left: -545px;">
								<li class="clone" aria-hidden="true" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/kipr/1.jpg" title="Фото Кипр" class="fancybox">
										<img src="/upload/lending_img/kipr/1.jpg" alt="Фото Кипр" draggable="false"/>
									</a>
								</li>
								<li class="flex-active-slide" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/kipr/2.jpg" title="Фото Кипр" class="fancybox">
										<img src="/upload/lending_img/kipr/2.jpg" alt="Фото Кипр" draggable="false"/>
									</a>
								</li>
								<li class="" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/kipr/3.jpg" title="Фото Кипр" class="fancybox">
										<img src="/upload/lending_img/kipr/3.jpg" alt="Фото Кипр" draggable="false"/>
									</a>
								</li>
							</ul>
						</div>

						<ul class="flex-direction-nav">
							<li><a class="flex-prev" href="#">Назад</a></li>
							<li><a class="flex-next" href="#">Вперед</a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-6">
					<div class="project-OverView">
						<div class="short-section-title">
							<h3>Кипр</h3>
						</div>
						<p>Остров, на котором каждый найдет отдых по душе. Популярность Кипра для туристов растет с каждым годом, и на это есть множество причин. Одной из них является то, что это европейский курорт, где Вам не понадобится шенгенская
							виза. На Кипре прекрасные песчаные пляжи сочетаются с городскими развлечениями и богатой экскурсионной программой. И в каком бы городе вы не остановились, всегда можно взять напрокат автомобиль и доехать до любой точки
							менее чем за 4 часа, ведь «длина» острова составляет всего 240 км.</p>
						<table class="table table-striped">
							<thead>
							<tr>
								<th class="text-success">Плюсы</th>
								<th class="text-danger">Минусы</th>
							</tr>
							</thead>
							<tr>
								<td>+ Кипрская виза бесплатная и оформляется за 10 минут</td>
								<td>- Достаточно высокие цены на проезд и питание, в евро</td>
							</tr>
							<tr>
								<td>+ Пляжи на любой вкус, от мелкого песочка до крупной гальки</td>
								<td>- Почти все пляжи общественные, придется доплачивать за зонтики</td>
							</tr>
							<tr>
								<td>+ Один из самых тусовочных курортов для молодежи: Айя-Напа</td>
								<td></td>
							</tr>
							<tr>
								<td>+ Уже тепло в мае, еще не холодно в октябре</td>
								<td></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Кипр --->

	<!-- КИПР КУРОРТЫ --->
	<section class="gray-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="short-section-title">
						<h3>Курорты Кипра</h3>
					</div>
				</div>

				<ul class="latest-works curorts">
					<li>
						<div class="img-figure">
							<div class="img-figure-overlayer">
								<a class="fancybox" href="/upload/lending_img/aia_napa.jpg" title="Увеличить"> <i class="fa fa-search"></i> </a>
							</div>
							<img src="/upload/lending_img/aia_napa.jpg" alt="Айя-Напа"/>
						</div>

						<div class="title-caption"><span>Айя-Напа</span></div>

						<div class="description">
							<p>Супер для молодежи. В центре города много клубов, баров ресторанов – просто бесконечная тусовка! Пляжи песчаные. В самом центре для семей громко будет, в пригороде хорошо. Пляж ПЕСОК отличный.</p>
							<p>Недалеко от аэропорта, ехать где-то полчаса. Самый дорогой.</p>
						</div>
					</li>
					<li>
						<div class="img-figure">
							<div class="img-figure-overlayer">
								<a class="fancybox" href="/upload/lending_img/pafos.jpg" title="Увеличить"> <i class="fa fa-search"></i> </a>
							</div>
							<img src="/upload/lending_img/pafos.jpg" alt="Пафос"/>
						</div>
						<div class="title-caption"><span>Пафос</span></span></div>
						<div class="description">
							<p>Далековато от аэропорта, ехать около 2х часов. Все пляжи галечные, есть один маленький с песочком, но его не так просто найти. Близко к историческим местам, руинам древних городов. И именно здесь Афродита вышла из пены
								морской.</p>
						</div>
					</li>
					<li>
						<div class="img-figure">
							<div class="img-figure-overlayer">
								<a class="fancybox" href="/upload/lending_img/larnaka.jpg" title="Увеличить"> <i class="fa fa-search"></i> </a>
							</div>

							<img src="/upload/lending_img/larnaka.jpg" alt="Ларнака"/>
						</div>

						<div class="title-caption"><span>Ларнака</span></div>
						<div class="description">
							<p>Город, в котором расположен аэропорт. Пляжи – песок. Есть, где погулять. Но развлечений не много. Зато можно доехать до Айя-Напы.</p>
						</div>
					</li>
					<li>
						<div class="img-figure">
							<div class="img-figure-overlayer">
								<a class="fancybox" href="/upload/lending_img/protaras.jpg" title="Увеличить"> <i class="fa fa-search"></i> </a>
							</div>

							<img src="/upload/lending_img/protaras.jpg" alt="Протарас"/>
						</div>

						<div class="title-caption"><span>Протарас</span></div>
						<div class="description">
							<p>Хорошо и для молодежи, и для семей. Пляж песчаный. Не центр: нет шума и музыки по ночам, и можно найти неплохой отель по нормальной стоимости недалеко от моря. Близко к аэропорту и Айя-Напе.</p>
						</div>
					</li>
					<li>
						<div class="img-figure">
							<div class="img-figure-overlayer">
								<a class="fancybox" href="/upload/lending_img/limasol.jpg" title="Увеличить"> <i class="fa fa-search"></i> </a>
							</div>

							<img src="/upload/lending_img/limasol.jpg" alt="Лимассол"/>
						</div>

						<div class="title-caption"><span>Лимассол</span></div>
						<div class="description">
							<p>От аэропорта час. Средний город – он так и переводится. Имеется в виду, что тут и до экскурсий близко, и пляж нормальный, и до Айя-Напы можно быстро добраться.</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</section>
	<!-- КИПР КУРОРТЫ --->

	<!-- Кипр ТУРЫ --->
	<section class="gray-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="short-section-title">
						<h3>Туры на Кипр</h3>
					</div>
				</div>
				<div class="col-md-12">
					<table class="table table-striped">
						<thead>
						<tr>
							<th>Отель</th>
							<th>Май</th>
							<th>Июнь-Июль</th>
							<th>Август</th>
						</tr>
						</thead>
						<tr>
							<td><a href="/hotel/3534/" target="_blank">KAPETANIOS 3*</a></td>
							<td><a href="/hotel/3534/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">97 200</a></td>
							<td><a href="/hotel/3534/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">112 125</a></td>
							<td><a href="/hotel/3534/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">111 240</a></td>
						</tr>
						<tr>
							<td><a href="/hotel/5418/" target="_blank">ANASTASIA BEACH 4*</a></td>
							<td><a href="/hotel/5418/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">109 406</a></td>
							<td><a href="/hotel/5418/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">129 516</a></td>
							<td><a href="/hotel/5418/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">123 824</a></td>
						</tr>
						<tr>
							<td><a href="/hotel/5412/" target="_blank">ADAMS BEACH 5*</a></td>
							<td><a href="/hotel/5412/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">126 417</a></td>
							<td><a href="/hotel/5412/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">153 674</a></td>
							<td><a href="/hotel/5412/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">154 559</a></td>
						</tr>
					</table>
					Цены указаны на размещение 2+1 (5 лет), актуальны на 23.02.2017.
				</div>
			</div>
		</div>
	</section>
	<!-- Кипр ТУРЫ --->

	<!-- ТУРЦИЯ --->
	<section class="white-section" id="turkey">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="flexslider project-flexslider">
						<div class="flex-viewport" style="overflow: hidden; position: relative;">
							<ul class="slides" style="width: 1200%; margin-left: -545px;">
								<li class="flex-active-slide" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/turkey/1.jpg" title="Турция фото - 2" class="fancybox">
										<img src="/upload/lending_img/turkey/1.jpg" alt="Турция фото" draggable="false"/>
									</a>
								</li>
								<li class="" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/turkey/2.jpg" title="Турция фото" class="fancybox">
										<img src="/upload/lending_img/turkey/2.jpg" alt="Турция фото" draggable="false"/>
									</a>
								</li>
								<li class="" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/turkey/3.jpg" title="Турция фото" class="fancybox">
										<img src="/upload/lending_img/turkey/3.jpg" alt="Турция фото" draggable="false"/>
									</a>
								</li>
							</ul>
						</div>
						<ul class="flex-direction-nav">
							<li><a class="flex-prev" href="#">Назад</a></li>
							<li><a class="flex-next" href="#">Вперед</a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-6">
					<div class="project-OverView">
						<div class="short-section-title">
							<h3>Турция</h3>
						</div>

						<p>Турция остаётся самым комфортным курортом для семейного отдыха, отели подстроены под наши пожелания, еда, алкоголь и даже анимация соответствуют запросам, любимых Турцией, русских туристов.</p>
						<p>Кстати говоря, даже если вы были в Турции несколько раз, всё равно найдется 100 причин посетить её снова, потому что это не только тюлений отдых на пляже, но и активные виды спорта, интереснейшие исторические экскурсии,
							морские прогулки и самые зажигательные вечеринки в клубах и барах на побережье.</p>
						<table class="table table-striped">
							<thead>
							<tr>
								<th class="text-success" width="50%">Плюсы</th>
								<th class="text-danger">Минусы</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>+ Это дешево! Нет, вы видели цены? Невероятно дешево!</td>
								<td>- Нестабильная политическая ситуация</td>
							</tr>
							<tr>
								<td>+ Отели для самых притязательных туристов с самыми притязательными детьми</td>
								<td>- Сложно выбрать отель, так как под ваш бюджет их 20 штук</td>
							</tr>
							<tr>
								<td>+ Персонал и анимация на русском языке</td>
								<td>- Переполненность отелей в разгар сезона</td>
							</tr>
							<tr>
								<td>+ Не нужна виза</td>
								<td>- Автор планирует отдых в Турции этим летом, поэтому не настроена искать минусы :)</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- ТУРЦИЯ --->

	<!-- ТУРЦИЯ КУРОРТЫ --->
	<section class="gray-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="short-section-title">
						<h3>Курорты Турции</h3>
					</div>
				</div>

				<ul class="latest-works curorts">
					<li>
						<div class="img-figure">
							<div class="img-figure-overlayer">
								<a class="fancybox" href="/upload/lending_img/alania.jpg" title="Увеличить"> <i class="fa fa-search"></i> </a>
							</div>
							<img src="/upload/lending_img/alania.jpg" alt="Алания"/>
						</div>

						<div class="title-caption"><span>Алания</span></div>

						<div class="description">
							<p>Самый тёплый курорт, можно лететь в мае или в октябре. Пляжи разные, есть и песок, и галька. Далеко от аэропорта.</p>
						</div>
					</li>
					<li>
						<div class="img-figure">
							<div class="img-figure-overlayer">
								<a class="fancybox" href="/upload/lending_img/antalia.jpg" title="Увеличить"> <i class="fa fa-search"></i> </a>
							</div>
							<img src="/upload/lending_img/antalia.jpg" alt="Анталия"/>
						</div>
						<div class="title-caption"><span>Анталия</span></span></div>
						<div class="description">
							<p>Ближе всего к аэропорту. Это курортная столица, много городских отелей в центре и поблизости. В пригороде семейные, с территорией. Из классных районов – Лара.</p>
						</div>
					</li>
					<li>
						<div class="img-figure">
							<div class="img-figure-overlayer">
								<a class="fancybox" href="/upload/lending_img/side.jpg" title="Увеличить"> <i class="fa fa-search"></i> </a>
							</div>

							<img src="/upload/lending_img/side.jpg" alt="Сиде"/>
						</div>

						<div class="title-caption"><span>Сиде</span></div>
						<div class="description">
							<p>Песчаные пляжи, разного уровня отели. По погоде почти так же, как в Алании.</p>
						</div>
					</li>
					<li>
						<div class="img-figure">
							<div class="img-figure-overlayer">
								<a class="fancybox" href="/upload/lending_img/kemer.jpg" title="Увеличить"> <i class="fa fa-search"></i> </a>
							</div>

							<img src="/upload/lending_img/kemer.jpg" alt="Кемер"/>
						</div>

						<div class="title-caption"><span>Кемер</span></div>
						<div class="description">
							<p>Горы, красивые виды, пляжи галечные. Здесь свежее, то есть в мае и в октябре ехать купаться не надо. Зато в июне, например, самая оптимальная температура для взрослых и детей.</p>
						</div>
					</li>
					<li>
						<div class="img-figure">
							<div class="img-figure-overlayer">
								<a class="fancybox" href="/upload/lending_img/belek.jpg" title="Увеличить"> <i class="fa fa-search"></i> </a>
							</div>

							<img src="/upload/lending_img/belek.jpg" alt="Белек"/>
						</div>

						<div class="title-caption"><span>Белек</span></div>
						<div class="description">
							<p>Самый крутой район, много вип отелей. Погода, климат примерно как в Алании, Сиде. СОСНЫ, отели зеленые, воздух чистый.</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</section>
	<!-- ТУРЦИЯ КУРОРТЫ --->

	<!-- ТУРЦИЯ ТУРЫ --->
	<section class="gray-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="short-section-title">
						<h3>Туры в Турцию</h3>
					</div>
				</div>
				<div class="col-md-12">
					<table class="table table-striped">
						<thead>
						<tr>
							<th>Отель</th>
							<th>Май</th>
							<th>Июнь-Июль</th>
							<th>Август</th>
						</tr>
						</thead>
						<tr>
							<td><a href="/hotel/15875/" target="_blank">RAYMAR RESORT 5*</a></td>
							<td><a href="/hotel/15875/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">63 703</a></td>
							<td><a href="/hotel/15875/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">89 939</a></td>
							<td><a href="/hotel/15875/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">100 029</a></td>
						</tr>
						<tr>
							<td><a href="/hotel/1433/" target="_blank">PIRATE'S BEACH CLUB 5*</a></td>
							<td><a href="/hotel/1433/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">81 303</a></td>
							<td><a href="/hotel/1433/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">112 591</a></td>
							<td><a href="/hotel/1433/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">124 206</a></td>
						</tr>
						<tr>
							<td><a href="/hotel/46670/" target="_blank">TRENDY LARA 5*</a></td>
							<td><a href="/hotel/46670/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">94 265</a></td>
							<td><a href="/hotel/46670/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">119 011</a></td>
							<td><a href="/hotel/46670/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">134 542</a></td>
						</tr>
						<tr>
							<td><a href="/hotel/1337/" target="_blank">LIMAK ATLANTIS DE LUXE 5*</a></td>
							<td><a href="/hotel/1337/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">115 791</a></td>
							<td><a href="/hotel/1337/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">146 636</a></td>
							<td><a href="/hotel/1337/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">151 515</a></td>
						</tr>
					</table>
					Цены указаны на размещение 2+1 (5 лет), актуальны на 23.02.2017.
				</div>
			</div>
		</div>
	</section>
	<!-- ТУРЦИЯ ТУРЫ --->

	<!-- БОЛГАРИЯ --->
	<section class="white-section" id="bolgaria">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="flexslider project-flexslider">
						<div class="flex-viewport" style="overflow: hidden; position: relative;">
							<ul class="slides" style="width: 1200%; margin-left: -545px;">
								<li class="flex-active-slide" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/bolgaria/1.jpg" title="Болгария фото - 2" class="fancybox">
										<img src="/upload/lending_img/bolgaria/1.jpg" alt="Болгария фото" draggable="false"/>
									</a>
								</li>
								<li class="" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/bolgaria/2.jpg" title="Болгария фото" class="fancybox">
										<img src="/upload/lending_img/bolgaria/2.jpg" alt="Болгария фото" draggable="false"/>
									</a>
								</li>
								<li class="" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/bolgaria/3.jpg" title="Болгария фото" class="fancybox">
										<img src="/upload/lending_img/bolgaria/3.jpg" alt="Болгария фото" draggable="false"/>
									</a>
								</li>
							</ul>
						</div>
						<ul class="flex-direction-nav">
							<li><a class="flex-prev" href="#">Назад</a></li>
							<li><a class="flex-next" href="#">Вперед</a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-6">
					<div class="project-OverView">
						<div class="short-section-title">
							<h3>Болгария</h3>
						</div>

						<p>Великолепный выбор для бюджетного отдыха! И если Вам не комфортно общаться на неродном языке. Есть, где погулять в Несебре. Есть тусовочный молодёжный курорт Солнечные пески. Есть тихие спокойные деревушки Елените, Святой
							Влас и другие. Есть бальнеокурорт Поморие.</p>
						<p>Солнечный берег не спит всю ночь, много европейской молодёжи, куча ресторанчиков, клубов... На окраинах, разумеется, тише.</p>
						<p>Если Вас интересует исключительно пляжный отдых, то лучше выбрать небольшие городки-деревни подальше от Бургаса.</p>
						<p>Питание почти везде отличное, кухня знакомая (есть даже гречка в детских меню), фрукты и овощи вкусные, не резиновые.</p>
						<p>Отели нужно смотреть новые, либо с реновацией, иначе будет сервис и номера с мебелью а-ля СССР.</p>
						<p>Чаще всего едут семьи с маленькими детьми, так как вход в море почти везде очень пологий, песочек мелкий, есть много аквапарков (почти в каждом регионе)</p>
						<table class="table table-striped">
							<thead>
							<tr>
								<th class="text-success" width="50%">Плюсы</th>
								<th class="text-danger">Минусы</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>+ цены очень доступные</td>
								<td>- Нужна виза</td>
							</tr>
							<tr>
								<td>+ трансфер почти везде не более часа</td>
								<td>- сервис на уровне СССР</td>
							</tr>
							<tr>
								<td>+ есть что посмотреть любознательным в Несебре</td>
								<td>- отельный фонд очень старый, номера маленькие, инфраструктура слабая – 1 ресторан, 1 бассейн</td>
							</tr>
							<tr>
								<td>+ питание знакомое и недорогое</td>
								<td>- пляжи муниципальные</td>
							</tr>
							<tr>
								<td>+ во многих регионах есть аквапарки</td>
								<td></td>
							</tr>
							<tr>
								<td>+ идеален для молодежи (Солнечный Берег) и семей с маленькими детьми</td>
								<td></td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- БОЛГАРИЯ --->

	<!-- БОЛГАРИЯ ТУРЫ --->
	<section class="gray-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="short-section-title">
						<h3>Туры в Болгарию</h3>
					</div>
				</div>

				<table class="table table-striped">
					<thead>
					<tr>
						<th>Отель</th>
						<th>Май</th>
						<th>Июнь-Июль</th>
						<th>Август</th>
					</tr>
					</thead>
					<tr>
						<td><a href="/hotel/7307/" target="_blank">ROYAL BAY 4* в Еленити на олл</a></td>
						<td><a href="/hotel/7307/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">90 800</a></td>
						<td><a href="/hotel/7307/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">116 700</a></td>
						<td><a href="/hotel/7307/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">103 300</a></td>
					</tr>
					<tr>
						<td><a href="/hotel/9038/" target="_blank">EVRIKA BEACH CLUB HOTEL 4* СОЛН БЕРЕГ</a></td>
						<td><a href="/hotel/9038/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">111 200</a></td>
						<td><a href="/hotel/9038/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">119 100</a></td>
						<td><a href="/hotel/9038/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">147 700</a></td>
					</tr>
					<tr>
						<td><a href="/hotel/7341/" target="_blank">SOL Hotel Nessebar Mare (Соль Несеба Мэйр) 4*</a></td>
						<td><a href="/hotel/7341/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">100 100</a></td>
						<td><a href="/hotel/7341/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">116 400</a></td>
						<td><a href="/hotel/7341/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">164 400</a></td>
					</tr>
				</table>
				Цены указаны на размещение 2+1 (5 лет), всё включено, 11 ночей, актуальны на 28.02.2017.
			</div>
		</div>
	</section>
	<!-- БОЛГАРИЯ ТУРЫ --->


	<!-- ИСПАНИЯ --->
	<section class="white-section" id="spain">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="flexslider project-flexslider">
						<div class="flex-viewport" style="overflow: hidden; position: relative;">
							<ul class="slides" style="width: 1200%; margin-left: -545px;">
								<li class="flex-active-slide" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/spain/1.jpg" title="Испания фото - 2" class="fancybox">
										<img src="/upload/lending_img/spain/1.jpg" alt="Испания фото" draggable="false"/>
									</a>
								</li>
								<li class="" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/spain/2.jpg" title="Испания фото" class="fancybox">
										<img src="/upload/lending_img/spain/2.jpg" alt="Испания фото" draggable="false"/>
									</a>
								</li>
								<li class="" style="float: left; display: block; width: 545px;">
									<a href="/upload/lending_img/spain/3.jpg" title="Испания фото" class="fancybox">
										<img src="/upload/lending_img/spain/3.jpg" alt="Испания фото" draggable="false"/>
									</a>
								</li>
							</ul>
						</div>
						<ul class="flex-direction-nav">
							<li><a class="flex-prev" href="#">Назад</a></li>
							<li><a class="flex-next" href="#">Вперед</a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-6">
					<div class="project-OverView">
						<div class="short-section-title">
							<h3>Испания</h3>
						</div>

						<p>Прекрасная страна, где можно совместить пляжный отдых с экскурсионными программами и шоппингом. И в Испании действительно нужно совмещать и пляжный отдых, и экскурсии, и шоппинг! Только ради пляжа сюда ехать не стоит!</p>
						<p>В Испании есть два основных туристических региона: Коста- Брава и Коста-Дорада.</p>
						<p>Коста-Брава находится поближе к Барселоне (аэропорту), но пляжи каменистые, с детьми будет некомфортно купаться, подросткам и хорошо плавающим нормально. Природа более богатая, жара в августе переносится легче. Крупнейший
							туристический центр на Коста-Браве - это Ллорет. Здесь много дискотек, баров, ресторанов. На окраинах города, разумеется, не так шумно в ночное время. Ездить в Барселону на экскурсии удобнее именно отсюда.</p>
						<p>Коста-Дорада (рекомендуем регион Салоу) – здесь песок помельче, глубина не такая как в Коста-Браве.</p>
						<table class="table table-striped">
							<thead>
							<tr>
								<th class="text-success" width="50%">Плюсы</th>
								<th class="text-danger">Минусы</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>+ Великолепная экскурсионная программа в Барселоне!</td>
								<td>- Нужна виза</td>
							</tr>
							<tr>
								<td>+ Порт Авентура – аналог Диснейленда для подростков и взрослых (детям страшно)</td>
								<td>- Ценник выше, чем в Греции</td>
							</tr>
							<tr>
								<td>+ Цены ниже, чем в Италии</td>
								<td>- Отели маленькие, инфраструктура слабая – 1 ресторан, 1 бассейн</td>
							</tr>
							<tr>
								<td>+ Идеален для молодежи и семей с подростками</td>
								<td>- Возможен языковой барьер (в отличие от Болгарии и Турции)</td>
							</tr>
							<tr>
								<td></td>
								<td>- пляжи муниципальные</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- ИСПАНИЯ --->

	<!-- ИСПАНИЯ ТУРЫ --->
	<!-- section class="gray-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="short-section-title">
						<h3>Туры в Испанию</h3>
					</div>
				</div>

				<table class="table table-striped">
					<thead>
					<tr>
						<th>Отель</th>
						<th>Май</th>
						<th>Июнь-Июль</th>
						<th>Август</th>
					</tr>
					</thead>
					<tr>
						<td><a href="/hotel/15875/" target="_blank">RAYMAR RESORT 5*</a></td>
						<td><a href="/hotel/15875/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">63 703</a></td>
						<td><a href="/hotel/15875/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">89 939</a></td>
						<td><a href="/hotel/15875/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">100 029</a></td>
					</tr>
					<tr>
						<td><a href="/hotel/1433/" target="_blank">PIRATE'S BEACH CLUB 5*</a></td>
						<td><a href="/hotel/1433/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">81 303</a></td>
						<td><a href="/hotel/1433/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">112 591</a></td>
						<td><a href="/hotel/1433/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">124 206</a></td>
					</tr>
					<tr>
						<td><a href="/hotel/46670/" target="_blank">TRENDY LARA 5*</a></td>
						<td><a href="/hotel/46670/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">94 265</a></td>
						<td><a href="/hotel/46670/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">119 011</a></td>
						<td><a href="/hotel/46670/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">134 542</a></td>
					</tr>
					<tr>
						<td><a href="/hotel/1337/" target="_blank">LIMAK ATLANTIS DE LUXE 5*</a></td>
						<td><a href="/hotel/1337/?start=Y&type=search&departure=4&datefrom=01.05.2017&dateto=31.05.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">115 791</a></td>
						<td><a href="/hotel/1337/?start=Y&type=search&departure=4&datefrom=01.06.2017&dateto=31.07.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">146 636</a></td>
						<td><a href="/hotel/1337/?start=Y&type=search&departure=4&datefrom=01.08.2017&dateto=28.08.2017&nightsfrom=9&child=1&childage1=5" rel="nofollow" target="_blank">151 515</a></td>
					</tr>
				</table>
			</div>
		</div>
	</section -->
	<!-- ИСПАНИЯ ТУРЫ --->


	<!-- ПРЕИМУЩЕСТВА --->
	<section class="gray-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="short-section-title">
						<h3>Почему стоит купить у нас?</h3>
					</div>

					<div class="col-md-3">
						<div class="feature-box-style6">
							<div class="feature-box-title"><i class="fa fa-users"></i> <a href="http://xn--e1agkeqhedbe9fh.xn--p1ai/tours/" title="Все туроператоры">Все туроператоры</a></div>

							<div class="feature-box-containt">
								<p>"Умные Туристы.РФ" - Сервис онлайн поиска по ВСЕМ туроператорам на одном сайте.</p>
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="feature-box-style6">
							<div class="feature-box-title"><i class="fa fa-pencil"></i> <a href="http://xn--e1agkeqhedbe9fh.xn--p1ai/tours/" title="Сравнение цен">Сравнение цен</a></div>

							<div class="feature-box-containt">
								<p>БЫСТРО - За 1 минуту сравните цены на туры.</p>
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="feature-box-style6">
							<div class="feature-box-title"><i class="fa fa-star"></i> <a href="http://xn--e1agkeqhedbe9fh.xn--p1ai/tours/" title="Скидки на туры">Скидки на туры</a></div>

							<div class="feature-box-containt">
								<p>ВЫГОДНО - СКИДКИ есть, ДОПЛАТ нет.</p>
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="feature-box-style6">
							<div class="feature-box-title"><i class="fa fa-dollar"></i> <a href="http://xn--e1agkeqhedbe9fh.xn--p1ai/tours/" title="Покупай онлайн">Покупай онлайн</a></div>

							<div class="feature-box-containt">
								<p>УДОБНО - покупай онлайн, в офисе или закажи курьера домой.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- ПРЕИМУЩЕСТВА --->

	<div class="container">
		<br>
		<? $APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include/managers.php"),
			Array(),
			Array("MODE" => "html", "SHOW_BORDER" => true)
		);
		?>
	</div>
	<div id="map1"></div>

	<div class="modal fade" id="question" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Задайте ваш вопрос эксперту</h4>
				</div>
				<div class="modal-body">
					<? $APPLICATION->IncludeComponent(
						"bitrix:form.result.new",
						"expert_question",
						Array(
							"CACHE_TIME"             => "3600",
							"CACHE_TYPE"             => "A",
							"CHAIN_ITEM_LINK"        => "",
							"CHAIN_ITEM_TEXT"        => "",
							"EDIT_URL"               => "",
							"IGNORE_CUSTOM_TEMPLATE" => "N",
							"LIST_URL"               => "",
							"SEF_MODE"               => "N",
							"SUCCESS_URL"            => "",
							"USE_EXTENDED_ERRORS"    => "N",
							"VARIABLE_ALIASES"       => Array(
								"RESULT_ID"   => "RESULT_ID",
								"WEB_FORM_ID" => "WEB_FORM_ID"
							),
							"WEB_FORM_ID"            => "7"
						)
					); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="give_more" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Хочу получить больше предложений по турам</h4>
				</div>
				<div class="modal-body">
					<? $APPLICATION->IncludeComponent(
						"bitrix:form.result.new",
						"tour_help",
						Array(
							"CACHE_TIME"             => "3600",
							"CACHE_TYPE"             => "A",
							"CHAIN_ITEM_LINK"        => "",
							"CHAIN_ITEM_TEXT"        => "",
							"EDIT_URL"               => "",
							"IGNORE_CUSTOM_TEMPLATE" => "N",
							"LIST_URL"               => "",
							"SEF_MODE"               => "N",
							"SUCCESS_URL"            => "",
							"USE_EXTENDED_ERRORS"    => "N",
							"VARIABLE_ALIASES"       => Array(
								"RESULT_ID"   => "RESULT_ID",
								"WEB_FORM_ID" => "WEB_FORM_ID"
							),
							"WEB_FORM_ID"            => "6"
						)
					); ?>
				</div>
			</div>
		</div>
	</div>

<? if ((isset($_GET['WEB_FORM_ID']) && $_GET['WEB_FORM_ID'] == "7") && (isset($_GET['formresult']) && $_GET['formresult'] == "addok")): ?>
	<script>
		$(document).ready(function () {
			$('#question').modal('show');
		});
	</script>
<? endif; ?>
<? if ((isset($_GET['WEB_FORM_ID']) && $_GET['WEB_FORM_ID'] == "6") && (isset($_GET['formresult']) && $_GET['formresult'] == "addok")): ?>
	<script>
		$(document).ready(function () {
			$('#give_more').modal('show');
		});
	</script>
<? endif; ?>

	<script>
		$(document).ready(function () {

			$('#question').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget) // Button that triggered the modal
				var recipient = button.data('country') // Extract info from data-* attributes
				// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
				// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
				var modal = $(this);
				modal.find('.modal-body input[name=form_text_67]').val(recipient)
			});
			$('#give_more').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget) // Button that triggered the modal
				var recipient = button.data('country'); // Extract info from data-* attributes
				var text = "Хочу " + recipient + ". \n";

				var modal = $(this);
				modal.find('.modal-body textarea[name=form_textarea_60]').val(text)
			});

			var map;
			map = new GMaps({
				el: '#map1',
				zoom: 17,
				scrollwheel: false,
				lat: 54.730145,
				lng: 55.930815
			});
			map.addMarker({
				lat: 54.730145,
				lng: 55.930815,
				icon: "images/map_marker.png"
			});

			/* Project Slider
			 -------------------------------------------------------------------------- */
			$('.project-flexslider').flexslider({
				animation: "slide",
				pauseOnHover: true,
				controlNav: false,
				directionNav: true,
				useCSS: false,
				slideshowSpeed: 10000
			});

			$('#home-section').backstretch([
				"/upload/lending_img/moscow_bg.jpg",
				"/upload/lending_img/moscow_bg.jpg",
			], {
				fade: 750,
				duration: 25000
			});


		});
	</script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>