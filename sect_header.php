<div class="header d-flex flex-column pb-5"
     style="background: url('<?= SITE_TEMPLATE_PATH ?>/assets/i/bg/bg_index.jpg') no-repeat center; background-size: cover;">
    <div class="header-search-container order-2 order-lg-1">
        <div class="header-search-container__logo-line pt-3 pt-lg-0">
            <div class="container">
                <div class="row row align-items-center">
                    <div class="col-6 col-sm-6 col-md-4">
                        <a href="/"><img src="/local/templates/promo_pages/assets/i/logo.png" class="img-fluid"
                                         alt="logo" title="logo"></a>
                    </div>
                    <div class="col-6 col-sm-6 col-md-4 text-right order-md-3 text-sm-center">
                        <a href="#" data-toggle="modal" data-target="#callbackModal" class="btn btn-danger">Заказать
                            звонок</a>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 order-md-2 text-center display-6 pb-2 text-white">
                        <a class="text-white" href="tel:+73472163061"><?=PHONE?></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="header-search-container__search-line row pb-5 pt-5">
                <div class="header-search-container__title col-12 text-center">
                    <h1 class="display-3">Удобный поиск самых выгодных цен на туры</h1>
                </div>
            </div>
            <? $APPLICATION->IncludeComponent(
                "newtravel.search:simple.search.v2",
                "",
                Array(
                    "ADULTS" => "2",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CITY" => "4",
                    "COUNTRY" => "4",
                    "NIGHTSFROM" => "6",
                    "NIGHTSTO" => "10",
                    "USE_COOKIE_DEPARTURE" => "Y",
                    "USE_GEO_DEFINE" => "N"
                )
            ); ?>
            <div class="header-search-container__search-line row ">
                <div class="col-12 text-center">
                    <span class="display-6 text-white">Оставь заявку на подбор тура прямо сейчас</span>
                </div>
            </div>
            <div class="row text-white text-center pb-3">
                <div class="col-12">
                    <i class="fa fa-chevron-down fa-2x"></i>
                </div>
            </div>
            <div class="row pb-3">
                <div class="col text-center">
                    <a href="#" class="btn btn-orange btn-lg" data-toggle="modal" data-target="#pickMeTourModal">Оставить
                        заявку</a>
                </div>
            </div>
            <div class="row text-white text-center pb-3">
                <div class="col-12 text-center">
                    <span class="display-7 text-white">и получи 3 лучших варианта в течение 15 минут</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="text-center" style="background: #f8f8f8">
    <div class="container">
        <div class="row">
            <div class="col">
                <a href="/blog/vazhnye-opoveshcheniya/ura-u-nas-eshche-odin-ofis.php"><img
                            src="/local/templates/promo_pages/assets/i/banner/banner_new_office.jpg" style="width: 75%"
                            alt=""></a>
            </div>
        </div>
    </div>
</div>
<br>