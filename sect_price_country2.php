<section class="bg-white py-5">
	<div class="container">
		<div class="col-12 mx-auto text-center">
			<h2 class=" mb-4 text-uppercase">Тунис</h2>
			<h2 class="display-6 mb-4"></h2>
		</div>
		<div class="row">
			<? $APPLICATION->IncludeComponent(
				"newtravel.search:tours.offers",
				"",
				Array(
					"ADULTS"        => "2",
					"CACHE_TIME"    => "3602",
					"CACHE_TYPE"    => "A",
					"CHILD"         => "0",
					"CHILDAGE1"     => "",
					"CHILDAGE2"     => "",
					"CHILDAGE3"     => "",
					"COUNTRY_ID"    => "5",
					"DATE_FROM"     => "01.06.2019",
					"DATE_TO"       => "30.06.2019",
					"DEPARTURE_ID"  => "4",
					"HOTELS_ID"     => "",
					"IBLOCK_TYPE"   => "content",
					"ITEM_STYLE"    => "card",
					"ONPAGE"        => "4",
					"PRICE_FROM"    => "",
					"PRICE_PER_ONE" => "N",
					"PRICE_TO"      => "",
					"CARD_LG_COUNT" => "3",
					"REGIONS"       => "",
					"STARS"         => "3",
					"SINGLE_ITEM"   => "N",
					"RATING"        => "4",
					//"OPERATORS"     => "12,13,16",
				)
			); ?>
		</div>
	</div>
</section>