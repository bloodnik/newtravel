<header id="blogHeader" class="header d-flex flex-column" style="background: url('<?=SITE_TEMPLATE_PATH?>/assets/i/bg/blog_bg.jpg') no-repeat bottom; background-size: cover">
	<div class="header-search-container order-2 order-lg-1">
		<div class="header-search-container__logo-line pt-3 pt-lg-0">
			<div class="container">
				<div class="row row align-items-center">
					<div class="col-6 col-sm-6 col-md-4"><a href="/"><img class="img-fluid" src="<?=SITE_TEMPLATE_PATH?>/assets/i/logo.png" alt=""></a></div>
					<div class="col-6 col-sm-6 col-md-4 text-right order-md-3 text-sm-center"><a href="#" data-toggle="modal" data-target="#callbackModal" class="btn btn-danger">Заказать звонок</a></div>
					<div class="col-12 col-sm-12 col-md-4 order-md-2 text-center display-6 pb-2 text-white" style="font-weight: 400;"><a href="tel:+73472163061"></a><?=PHONE?></div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="header-search-container__search-line row pb-3 mt-3">
				<div class="header-search-container__title col-12 text-center">
					<h1 class="h2 text-white text-uppercase"><?=$APPLICATION->ShowTitle(false)?></h1>
				</div>
			</div>
		</div>
	</div>
</header>