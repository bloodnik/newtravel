<section class="bg-info pt-3 pb-3">
	<div class="container">
		<? $APPLICATION->IncludeComponent(
			"bitrix:form.result.new",
			"budget_inline",
			Array(
				"CACHE_TIME"             => "3600",
				"CACHE_TYPE"             => "A",
				"CHAIN_ITEM_LINK"        => "",
				"CHAIN_ITEM_TEXT"        => "",
				"EDIT_URL"               => "",
				"IGNORE_CUSTOM_TEMPLATE" => "N",
				"LIST_URL"               => "",
				"SEF_MODE"               => "N",
				"SUCCESS_URL"            => "",
				"USE_EXTENDED_ERRORS"    => "N",
				"VARIABLE_ALIASES"       => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
				"WEB_FORM_ID"            => "3",
				"AJAX_MODE" => "Y",  // режим AJAX
				"AJAX_OPTION_JUMP" => "N", // скроллить страницу до компонента
				"AJAX_OPTION_STYLE" => "Y", // подключать стили
				"AJAX_OPTION_HISTORY" => "N",
			)
		); ?>
	</div>
</section>